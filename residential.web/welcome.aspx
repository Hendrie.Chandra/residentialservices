﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="welcome.aspx.cs" Inherits="residential.web.welcome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="jumbotron" style="margin-top:50px;">
                <h1>Welcome!</h1>
                <p class="lead"><strong>Residential Service v2.0</strong></p>  
                <p>*strongly recommended to use Browser that supports HTML5</p>                
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
