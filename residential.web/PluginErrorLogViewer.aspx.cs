﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web
{
    public partial class PluginErrorLogViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                bound_pluginErrorLog();
        }

        protected void bound_pluginErrorLog()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_PluginErrorLog();
            gvw_data.DataSource = dt;
            gvw_data.DataBind();
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bound_pluginErrorLog();
        }

        protected void btn_Refresh_Click(object sender, EventArgs e)
        {
            bound_pluginErrorLog();
        }
    }
}