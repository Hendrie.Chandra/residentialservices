﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.Data.SqlClient;
using residential.libs.Models;

namespace residential.libs
{
    public class DBclass
    {
        private String connectionstring;

        public DBclass(String _connectionstring)                                                                                                                                                                                                                                                                                                                                                                                                                                            
        {
            connectionstring = _connectionstring;
        }

        #region role

        public DataTable retrieve_role()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Role", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_role(Int32 _roleid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Role", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleID", _roleid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_role(String _rolename)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Role", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleName", _rolename);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_role(Int32 _roleid, String _rolename, String _roleaccess, String _roledescription, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Role", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleID", _roleid);
            cmd.Parameters.AddWithValue("@RoleName", _rolename);
            cmd.Parameters.AddWithValue("@RoleDescription", _roledescription);
            cmd.Parameters.AddWithValue("@RoleAccess", _roleaccess);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_role(Int32 _roleid)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_Role", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleID", _roleid);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public DataTable retrieve_roleaccess(Int32 _RoleID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_RoleAccess", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleID", _RoleID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        #endregion

        #region user

        public DataTable retrieve_user(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_user(Int32 _userid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", _userid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_user(String _username)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserName", _username);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_user(Int32 _OrgID, Int32 _SiteID, String _Username)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@UserName", _Username);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_user(Int32 _userid, String _username, String _fullname, String _mobilenumber, String _emailaddress, Int32 _departmentid,
            Int32 _roleid, Int32 _orgid, Int32 _siteid, Int32 _defaultsiteid, String _savedby, string lstSubLocation)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", _userid);
            cmd.Parameters.AddWithValue("@UserName", _username);
            cmd.Parameters.AddWithValue("@FullName", _fullname);
            cmd.Parameters.AddWithValue("@MobileNumber", _mobilenumber);
            cmd.Parameters.AddWithValue("@EmailAddress", _emailaddress);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@RoleID", _roleid);
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@DefaultSiteID", _defaultsiteid);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);
            cmd.Parameters.AddWithValue("@SubLocationList", lstSubLocation);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_user(Int32 _userid)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", _userid);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();
            return returnvalue;
        }

        public Int32 save_defaultusersite(Int32 _userid, Int32 _defaultsiteid, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_DefaultUserSite", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserID", _userid);
            cmd.Parameters.AddWithValue("@DefaultSiteID", _defaultsiteid);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public DataTable retrieve_userAuthorization(String _username)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_UserAuthorization", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@username", _username);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        #endregion

        #region site

        public DataTable retrieve_site()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Site", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_site(Int32 _siteid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Site", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SiteID", _siteid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_site(Int32 _parentid, Int32 _siteid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Site", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ParentID", _parentid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_site(Int32 _siteid, String _sitecode, String _sitename, int _orgid, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Site", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@SiteCode", _sitecode);
            cmd.Parameters.AddWithValue("@SiteName", _sitename);
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_site(Int32 _siteid)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_Site", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SiteID", _siteid);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region department

        public DataTable retrieve_department()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Department", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_department(Int32 _departmentid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Department", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_department(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Department", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_department(Int32 _departmentid, String _departmentcode, String _departmentname, Int32 _siteid, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Department", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@DepartmentCode", _departmentcode);
            cmd.Parameters.AddWithValue("@DepartmentName", _departmentname);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_department(Int32 _departmentid)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_Department", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region help

        public DataTable retrieve_help()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Help", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_help(Int32 _helpid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Help", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpID", _helpid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_help(Int32 _helpid, Int32 _parenthelpid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Help", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ParentHelpID", _parenthelpid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_help(Int32 _helpid, Int32 _parenthelpid, Int32 _helpcategoryid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Help", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpCategoryID", _helpcategoryid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_help(Int32 _helpid, String _helpname, Int32 _parenthelpid, Int32 _departmentid, Int16 _treelevel, Int16 _overduedays, Int32 _teamid, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Help", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpID", _helpid);
            cmd.Parameters.AddWithValue("@HelpName", _helpname);
            cmd.Parameters.AddWithValue("@ParentHelpID", _parenthelpid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@TreeLevel", _treelevel);
            cmd.Parameters.AddWithValue("@OverdueDays", _overduedays);
            cmd.Parameters.AddWithValue("@TeamID", _teamid);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_help(Int32 _helpid)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_Help", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpID", _helpid);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public DataTable retrieve_helpID(Int32 _OrgID, Int32 _SiteID, String _HelpName)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_HelpID", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@HelpName", _HelpName);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        #endregion

        #region lookup personal & reference number

        public DataTable lookup_referencenumber(String _casenumber, String _name, String _date, int _sitecode, int _orgcode)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Lookup_ReferenceNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@casenumber", _casenumber);
            cmd.Parameters.AddWithValue("@name", _name);
            DateTime date;
            if (!(DateTime.TryParse(_date, out date)))
            {
                cmd.Parameters.AddWithValue("@date", "");
            }
            else
            {
                cmd.Parameters.AddWithValue("@date", _date);
            }
            cmd.Parameters.AddWithValue("@site", _sitecode);
            cmd.Parameters.AddWithValue("@org", _orgcode);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable lookup_personal(Int32 _customertype, Int32 _orgid, Int32 _siteid, String _personal, String _unitcodecluster, String _unitno)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Lookup_Personal", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CustomerType", _customertype);
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@Personal", _personal);
            cmd.Parameters.AddWithValue("@UnitCodeCluster", _unitcodecluster);
            cmd.Parameters.AddWithValue("@UnitNo", _unitno);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_personal(String _pscode)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Personal", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PSCode", _pscode);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }


        #endregion

        #region bcd_transaction

        public DataTable retrieve_BCD_HelpName(Int32 _HelpID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BCD_HelpName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpID", _HelpID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_BCD_HelpName(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BCD_HelpName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_bcd_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            DateTime? _StartDate, DateTime? _EndDate, Int32 _Quantity, Int32 _Price, Int32 _TotalAmount, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_BCD", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@StartDate", _StartDate);
                cmd.Parameters.AddWithValue("@EndDate", _EndDate);
                cmd.Parameters.AddWithValue("@Quantity", _Quantity);
                cmd.Parameters.AddWithValue("@Price", _Price);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public DataTable retrieve_BCD(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BCD", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_BCD(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, String _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BCD", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        //public Int32 update_status_bcd_transaction(String _CaseNumber, Int32 _CaseStatusID, String _savedby)
        //{
        //    SqlConnection conn = new SqlConnection(connectionstring);
        //    SqlCommand cmd = new SqlCommand("SP_Save_Case", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
        //    cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
        //    cmd.Parameters.AddWithValue("@SavedBy", _savedby);

        //    SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
        //    returnparameter.Direction = ParameterDirection.ReturnValue;
        //    cmd.Parameters.Add(returnparameter);

        //    conn.Open();
        //    cmd.ExecuteNonQuery();
        //    conn.Close();

        //    Int32 returnvalue = (Int32)returnparameter.Value;

        //    return returnvalue;
        //}

        public DataTable retrieve_bcdprice(Int32 _helpid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BCDPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpID", _helpid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public void Save_BCDPrice(Int32 _helpID, Int32 _price, String _savedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_BCDPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpID", _helpID);
            cmd.Parameters.AddWithValue("@Price", _price);
            cmd.Parameters.AddWithValue("@Username", _savedBy);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public DataTable retrieve_BCDSpecialRequest(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BCDSpecialRequest", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_BCDPermitTaking(Int32 _OrgID, Int32 _SiteID, String _Search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BCDPermitTaking", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@Search", _Search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_BCDPermitTaking(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BCDPermitTaking", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public void Save_BCDPermitTaking(String _CaseNumber, DateTime _StartDate, DateTime _EndDate, String _SavedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_BCDPermitTaking", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@StartDate", _StartDate);
            cmd.Parameters.AddWithValue("@EndDate", _EndDate);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        #endregion

        #region General_Case

        public DataTable retrieve_Case(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Case", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_CaseByCustomer(string _caseNumber, string _psCode, string _unitCode, string _unitNo)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CaseByCustomer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);
            cmd.Parameters.AddWithValue("@PSCode", _psCode);
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_caseorigin()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CaseOrigin", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_paymentSchemeNonIPKL()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("spSelectPaymentSchemeNonIPKL", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_unitownership(Int32 _OrgID, Int32 _SiteID, String _pscode, int _customerType)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_UnitOwnership", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@PSCode", _pscode);
            cmd.Parameters.AddWithValue("@CustomerType", _customerType);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_TransactionCase(String _searchquery)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Search_Tasklist", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@searchquery", _searchquery);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_casestatus(Int32 _casestatusid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CaseStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_casestatus()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CaseStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable retrieve_casestatus(Int32 _roleID, Int32 _currentCaseStatusID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CaseStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleID", _roleID);
            cmd.Parameters.AddWithValue("@CurrentCaseStatusID", _currentCaseStatusID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable retrieve_pic(Int32 _OrgID, Int32 _SiteID, Int32 _DepartmentID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PIC", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@DepartmentID", _DepartmentID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_satisfaction()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Satisfaction", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_tasklist(Int32 _OrgID, Int32 _SiteID, Int32 _UserID, Int32 _CaseStatusID, String _Search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Tasklist", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@UserID", _UserID);
            cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
            cmd.Parameters.AddWithValue("@Search", _Search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_tasklist(Int32 _DepartmentID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Tasklist", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DepartmentID", _DepartmentID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_ViewFormName(String _CaseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ViewFormName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String assign_case(Int32 _UserID, String _CaseNumber, String _SavedBy)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Assign_Case", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@UserID", _UserID);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public DataTable retrieve_Currency(int orgID, int siteID, string currencyID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Currency", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", orgID);
            cmd.Parameters.AddWithValue("@SiteID", siteID);
            cmd.Parameters.AddWithValue("@CurrencyID", currencyID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        #endregion

        #region TicketHistory

        public Int32 save_tickethistory(String _CaseNumber, Int32 _CaseStatusID, String _FullName, String _Remarks, String _SavedBy, String _Attachment)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_TicketHistory", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
            cmd.Parameters.AddWithValue("@FullName", _FullName);
            cmd.Parameters.AddWithValue("@Remarks", _Remarks);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
            cmd.Parameters.AddWithValue("@Attachment", _Attachment);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public Int32 save_satisfaction(String _CaseNumber, Int32 _SatisfactionID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Satisfaction", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@SatisfactionID", _SatisfactionID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return (Int32)returnparameter.Value;
        }

        public DataTable retrieve_tickethistory(String _CaseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_TicketHistory", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }

        #endregion

        #region SMS_Blasting

        public DataTable retrieve_SubscriptionType()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SubscriptionType", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SubscriptionTypeDetail(Int32 _SubscriptionTypeID, Int32 _SMSGroupID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SubscriptionTypeDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubscriptionTypeID", _SubscriptionTypeID);
            cmd.Parameters.AddWithValue("@SMSGroupID", _SMSGroupID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_SubscriptionType(Int32 _SubscriptionTypeID, String _SubscriptionTypeName, String _SavedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_SubscriptionType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubscriptionTypeID", _SubscriptionTypeID);
            cmd.Parameters.AddWithValue("@SubscriptionTypeName", _SubscriptionTypeName);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public Int32 save_SubscriptionTypeDetail(Int32 _SubscriptionTypeID, Int32 _SMSGroupID, String _SavedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_SubscriptionTypeDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubscriptionTypeID", _SubscriptionTypeID);
            cmd.Parameters.AddWithValue("@SMSGroupID", _SMSGroupID);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public Int32 delete_SubscriptionTypeDetail(Int32 _SubscriptionTypeID, Int32 _SMSGroupID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_SubscriptionTypeDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubscriptionTypeID", _SubscriptionTypeID);
            cmd.Parameters.AddWithValue("@SMSGroupID", _SMSGroupID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public DataTable retrieve_SMSGroup(Int32 _SubscriptionTypeID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSGroup", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SubscriptionTypeID", _SubscriptionTypeID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SMSGroup()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSGroup", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SMSPriority()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSPriority", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_smsblast(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, String _MessageDetail,
            Int32 _SMSPriorityID, DateTime _BlastSchedule, Int32 _SenderID, Int32 _Price, Int32 _Quantity, Int32 _TotalAmount, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_SMSBlast", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@MessageDetail", _MessageDetail);
                cmd.Parameters.AddWithValue("@SMSPriorityID", _SMSPriorityID);
                cmd.Parameters.AddWithValue("@BlastSchedule", _BlastSchedule);
                cmd.Parameters.AddWithValue("@SenderID", _SenderID);
                cmd.Parameters.AddWithValue("@Quantity", _Quantity);
                cmd.Parameters.AddWithValue("@Price", _Price);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;
                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public Int32 save_SMSBlastNumberDetail(Int32 _OrgID, Int32 _SiteID, Int32 _SMSBlastID, String _SMSNumber, String _SMSName, String _SavedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_SMSBlastNumberDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@SMSBlastID", _SMSBlastID);
            cmd.Parameters.AddWithValue("@SMSNumber", _SMSNumber);
            cmd.Parameters.AddWithValue("@SMSName", _SMSName);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public Int32 delete_SMSBlastNumberDetail(Int32 _SMSBlastID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_SMSBlastNumberDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SMSBlastID", _SMSBlastID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public DataTable retrieve_SMSGroupNumberDetail(Int32 _SMSGroupID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSGroupNumberDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SMSGroupID", _SMSGroupID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SMSBlast(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSBlast", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SMSBlast(Int32 _orgID, Int32 _siteID, Int32 _caseStatusID, String _Search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSBlast", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CaseStatusID", _caseStatusID);
            cmd.Parameters.AddWithValue("@Search", _Search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SMSBlastNumberDetail(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSBlastNumberDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SMSSender(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSSender", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SMSSender(Int32 _SMSSenderID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSSender", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SMSSenderID", _SMSSenderID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public void Save_SMSSender(Int32 _orgID, Int32 _siteID, Int32 _SMSSenderID, String _smsSender, String _savedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_SMSSender", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SMSSenderID", _SMSSenderID);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Delete_SMSSender(Int32 _SMSSenderID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_SMSSender", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SMSSenderID", _SMSSenderID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            conn.Close();
        }

        public DataTable retrieve_SMSBlastPrice(Int32 _orgID, Int32 _siteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSBlastPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 Update_SMSStatus(String _CaseNumber, Int32 _SMSStatusID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Update_SMSStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@SMSStatusID", _SMSStatusID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public DataTable retrieve_SMSPhoneNumber(Int32 _orgID, Int32 _siteID, Int32 _level, String _code)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PhoneNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@Level", _level);
            cmd.Parameters.AddWithValue("@Code", _code);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        #endregion

        #region Update
        public DataTable retrieve_booked_banner(String _bannerLocationID, String _startDate)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Booked_Banner", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerLocationID", _bannerLocationID);
            cmd.Parameters.AddWithValue("@StartDate", _startDate);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_booked_banner_point(String _PointID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Booked_Banner", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PointID", _PointID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_banner_price(Int32 _pointId)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Banner_Price", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LocationId", _pointId);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_Banner(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, String _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Banner", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_banner_by_casenumber(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Banner", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 delete_banner_detail_transaction(Int32 _BannerID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_Banner_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerID", _BannerID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public String save_banner_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description,
            DateTime? _OverdueDate, Int32 _TotalAmount, Int32 _InstallationFee, Double _TaxPercentage, Int32 _TaxAmount, Int32 _TotalAmountAfterTax,
             Int32 _DiscountTypeID, Int32 _TotalDiscount, Int32 _ActualAmount, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Banner", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@InstallationFee", _InstallationFee);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@ActualAmount", _ActualAmount);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;
                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public void Update_Banner_Adjustment(String _CaseNumber, Decimal _Adjustment)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Update_BannerAdjustment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@Adjustment", _Adjustment);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public Int32 save_banner_detail_transaction(
            Int32 _OrgID, Int32 _SiteID, Int32 _BannerID, Int32 _BannerTypeID, Int32 _LocationID, Int32 _PointID,
            DateTime _StartDate, DateTime _EndDate, Int32 _Price, String _SavedBy
            )
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Banner_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@BannerID", _BannerID);
            cmd.Parameters.AddWithValue("@BannerTypeID", _BannerTypeID);
            cmd.Parameters.AddWithValue("@LocationID", _LocationID);
            cmd.Parameters.AddWithValue("@PointID", _PointID);
            cmd.Parameters.AddWithValue("@StartDate", _StartDate);
            cmd.Parameters.AddWithValue("@EndDate", _EndDate);
            cmd.Parameters.AddWithValue("@Price", _Price);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }
        public Int32 delete_rebanner_detail_transaction(Int32 _ReBannerID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_ReBanner_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ReBannerID", _ReBannerID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }
        public DataTable retrieve_banner_detail(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Banner_Detail_By_CaseNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_rebanner_by_casenumber(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ReBanner", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_ReBanner(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, string _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ReBanner", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_rebanner_detail(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ReBanner_Detail_By_CaseNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public String save_rebanner_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description,
            DateTime _OverdueDate, String _CaseReferenceID, Int32 _TotalAmount, Int32 _DiscountTypeID, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, Int32 _TaxPercentage, Int32 _TaxAmount, Int32 _TotalAmountAfterTax, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_ReBanner", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@CaseReference", _CaseReferenceID);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;
                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public Int32 save_rebanner_detail_transaction(
            Int32 _OrgID, Int32 _SiteID, Int32 _ReBannerID, Int32 _LocationID, Int32 _PointID,
            DateTime _ChangeDate, String _SavedBy
            )
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_ReBanner_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@ReBannerID", _ReBannerID);
            cmd.Parameters.AddWithValue("@LocationID", _LocationID);
            cmd.Parameters.AddWithValue("@PointID", _PointID);
            cmd.Parameters.AddWithValue("@ChangeDate", _ChangeDate);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public DataTable retrieve_flyerPrice(Int32 _orgID, Int32 _siteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_FlyerPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_flyer(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_Flyer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_flyer(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, String _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Flyer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_flyer_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            Int32 _Quantity, Double _Price, Double _TotalAmount, Double _TaxPercentage, Double _TaxAmount,
            Double _TotalAmountAfterTax, Int32 _DiscountTypeID, Double _TotalDiscount, Int32 _ActualAmount, Int32 _PeriodNo,
            Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Flyer", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@NoOfFlyer", _Quantity);
                cmd.Parameters.AddWithValue("@Price", _Price);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@ActualAmount", _ActualAmount);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public DataTable retrieve_magazine_by_casenumber(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Magazine", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_magazine(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, string _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Magazine", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_magazine_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description,
            DateTime _OverdueDate, Int32 _TotalAmount, Int32 _DiscountTypeID, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, Int32 _TaxPercentage, Int32 _TaxAmount, Int32 _TotalAmountAfterTax, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Magazine", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;
                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public DataTable retrieve_magazine_detail(Int32 _siteID, Int32 _magazineTrxTypeID, String _magazineEdition)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Magazine_Detail_By_TrxTypeAndEdition", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@MagazineTrxTypeID", _magazineTrxTypeID);
            cmd.Parameters.AddWithValue("@MagazineEdition", _magazineEdition);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_magazine_detail(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Magazine_Detail_By_CaseNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public Int32 save_magazine_detail_transaction(Int32 _MagazineDetailID, Int32 _MagazineID, Int32 _MagazineTrxTypeID, String _MagazineEdition, Int32 _Qty,
    Int32 _Price, Int32 _Amount, String _SavedBy
            )
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Magazine_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MagazineDetailID", _MagazineDetailID);
            cmd.Parameters.AddWithValue("@MagazineID", _MagazineID);
            cmd.Parameters.AddWithValue("@MagazineTrxTypeID", _MagazineTrxTypeID);
            cmd.Parameters.AddWithValue("@MagazineEdition", _MagazineEdition);
            cmd.Parameters.AddWithValue("@Qty", _Qty);
            cmd.Parameters.AddWithValue("@Price", _Price);
            cmd.Parameters.AddWithValue("@Amount", _Amount);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }
        public Int32 delete_magazine_detail_transaction(Int32 _MagazineID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_Magazine_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MagazineID", _MagazineID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }
        public DataTable retrieve_personal_identification(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, string _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Personal_Identification", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_personal_identification_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            Int32 _ZoneID, Int32 _Qty_KTP, Int32 _Qty_KK, Int32 _TotalAmount, Int32 _DiscountTypeID, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, Double _TaxPercentage, Int32 _TaxAmount, Int32 _TotalAmountAfterTax, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PersonalIdentification", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@ZoneID", _ZoneID);
                cmd.Parameters.AddWithValue("@Qty_KTP", _Qty_KTP);
                cmd.Parameters.AddWithValue("@Qty_KK", _Qty_KK);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public Int32 save_personal_identification_detail_transaction(
            Int32 _OrgID, Int32 _SiteID, Int32 _PersonalIdentificationID, Int32 _PersonalIdentificationDetailID,
            String _Address, String _Kelurahan, String _Kecamatan, String _Name, Int32 _GenderID, Int32 _RelationID,
            String _BirthPlace, DateTime _BirthDate, String _StateCountry, Int32 _MaritalStatusID, Int32 _BloodTypeID,
            Int32 _ReligionID, Int32 _LastEducationID, Int32 _OccupationID, String _ParentName,
            String _SBKRINotes, String _Notes
            )
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_PersonalIdentification_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@PersonalIdentificationID", _PersonalIdentificationID);
            cmd.Parameters.AddWithValue("@PersonalIdentificationDetailID", _PersonalIdentificationDetailID);
            cmd.Parameters.AddWithValue("@Address", _Address);
            cmd.Parameters.AddWithValue("@Kelurahan", _Kelurahan);
            cmd.Parameters.AddWithValue("@Kecamatan", _Kecamatan);
            cmd.Parameters.AddWithValue("@Name", _Name);
            cmd.Parameters.AddWithValue("@GenderID", _GenderID);
            cmd.Parameters.AddWithValue("@RelationID", _RelationID);
            cmd.Parameters.AddWithValue("@BirthPlace", _BirthPlace);
            cmd.Parameters.AddWithValue("@BirthDate", _BirthDate);
            cmd.Parameters.AddWithValue("@StateCountry", _StateCountry);
            cmd.Parameters.AddWithValue("@MaritalStatusID", _MaritalStatusID);
            cmd.Parameters.AddWithValue("@BloodTypeID", _BloodTypeID);
            cmd.Parameters.AddWithValue("@ReligionID", _ReligionID);
            cmd.Parameters.AddWithValue("@LastEducationID", _LastEducationID);
            cmd.Parameters.AddWithValue("@OccupationID", _OccupationID);
            cmd.Parameters.AddWithValue("@ParentName", _ParentName);
            cmd.Parameters.AddWithValue("@SBKRINotes", _SBKRINotes);
            cmd.Parameters.AddWithValue("@Notes", _Notes);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public String save_re_personal_identification_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            Int32 _ZoneID, Int32 _Qty_KTP, Int32 _Qty_KK, Int32 _TotalAmount, Int32 _DiscountTypeID, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, Double _TaxPercentage, Int32 _TaxAmount, Int32 _TotalAmountAfterTax, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_RePersonalIdentification", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@ZoneID", _ZoneID);
                cmd.Parameters.AddWithValue("@Qty_KTP", _Qty_KTP);
                cmd.Parameters.AddWithValue("@Qty_KK", _Qty_KK);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;
                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public DataTable retrieve_repersonal_identification_by_casenumber(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_RePersonal_Identification", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_re_personal_identification(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, string _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_RePersonal_Identification", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_CarRent(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            //SqlCommand cmd = new SqlCommand("SP_Retrieve_Flyer_By_CaseNumber", conn);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarRent", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_CarRent(Int32 CarLicensePlateID, DateTime Date)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarRent_ByDate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CarLicensePlateID", CarLicensePlateID);
            cmd.Parameters.AddWithValue("@Date", Date);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }

        public DataTable retrieve_CarRent(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, string _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarRent", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_car_rent_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description,
            DateTime _OverdueDate, Int32 _CarLicensePlateID, Int32 _CarRentPackageID, DateTime _Date, String _StartPickupTime,
            String _EndPickupTime, String _PickupLocation, String _Destination, Int32 _CarRentTariffID, Int32 _TotalAmount, Int32 _DiscountTypeID, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, Double _TaxPercentage, Int32 _TaxAmount, Int32 _TotalAmountAfterTax, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_CarRent", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);

                cmd.Parameters.AddWithValue("@CarLicensePlateID", _CarLicensePlateID);
                cmd.Parameters.AddWithValue("@CarRentPackageID", _CarRentPackageID);
                cmd.Parameters.AddWithValue("@Date", _Date);
                cmd.Parameters.AddWithValue("@StartPickupTime", _StartPickupTime);
                cmd.Parameters.AddWithValue("@EndPickupTime", _EndPickupTime);
                cmd.Parameters.AddWithValue("@PickupLocation", _PickupLocation);
                cmd.Parameters.AddWithValue("@Destination", _Destination);
                cmd.Parameters.AddWithValue("@CarRentTariffID", _CarRentTariffID);

                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;
                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public DataTable retrieve_wtp(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_WTP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_wtp(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, string _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_WTP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_wtp_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description,
            DateTime _OverdueDate, Int32 _WaterPlantCategoryID, string _CurrencyID, Int32? _Rate, Double _TotalAmount, Int32 _DiscountTypeID, Int32 _TotalDiscount,
            Double _TotalAmountAfterDiscount, Int32 _TaxPercentage, Int32 _TaxAmount, Double _TotalAmountAfterTax, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_WTP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@WaterPlantCategoryID", _WaterPlantCategoryID);
                cmd.Parameters.AddWithValue("@CurrencyID", _CurrencyID);
                cmd.Parameters.AddWithValue("@Rate", _Rate);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public DataTable retrieve_rewtp(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, string _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ReWTP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_generalprice(String _code, Int32 _siteId, Int32 _zoneId)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_GeneralPrice2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Code", _code);
            cmd.Parameters.AddWithValue("@SiteID", _siteId);
            cmd.Parameters.AddWithValue("@ZoneID", _zoneId);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_generalprice(String _code, Int32 _siteId)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_GeneralPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Code", _code);
            cmd.Parameters.AddWithValue("@SiteID", _siteId);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_discountype(string _category)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_DiscountType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DiscountCategory", _category);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_magazine_edition()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_MagazineEdition", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_car_license_plate()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarLicensePlate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_car_rent_package()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarRentPackage", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_car_rent_tariff()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarRentTariff", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_carrenttariff(Int32 _carRentTariifId)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarRentTariffById", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CarRentTariffId", _carRentTariifId);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_zone(Int32 _orgID, Int32 _siteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Zone", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_magazine_trx_type()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_MagazineTrxType", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_magazineprice(Int32 _magazineTrxTypeid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_MagazinePrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MagazineTrxTypeId", _magazineTrxTypeid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_waterplantcategory(Int32 site)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_WaterPlantCategory", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SiteId", site);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_waterplantcategory(Int32 _waterPlantCategoryId, Int32 _siteId)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_WaterPlantCategoryPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@WaterPlantCategoryId", _waterPlantCategoryId);
            cmd.Parameters.AddWithValue("@SiteId", _siteId);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_parameter_help(String param)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ParameterHelp", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Param", param);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }
        public DataTable retrieve_personal_identification_by_casenumber(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Personal_Identification", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_personal_identification_detail(Int32 _personalIdentificationID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PersonalIdentificationDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PersonalIdentificationID", _personalIdentificationID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }


        public DataTable retrieve_personal_identification_detail(String _CaseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PersonalIdentificationDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 delete_personal_identification_detail(Int32 _PersonalIdentificationDetailID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_PersonalIdentification_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PersonalIdentificationDetailID", _PersonalIdentificationDetailID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }
        public DataTable retrieve_location(Int32 _OrgID, Int32 _SiteID, Int32 _BannerTypeID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BannerLocation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@BannerTypeID", _BannerTypeID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_point(Int32 _locationId)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Point", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerLocationID", _locationId);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_point()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SELECT * FROM MSPoint", conn);
            cmd.CommandType = CommandType.Text;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_pipeSizeWTP(String _unitCode, String _unitNo, Int32 _siteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PipeSizeWTP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_ReWTPPrice(Int32 _LamaPemutusan)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ReWTPPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LamaPemutusan", _LamaPemutusan);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        public DataTable retrieve_cutoffdateWTP(int orgID, String _unitCode, String _unitNo, Int32 _siteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CutOffDateWTP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_rewtp(String _CaseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ReWTP", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_rewtp_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description,
            DateTime _OverdueDate, Int32 _WaterPlantCategoryID, DateTime _CutOffDate, Int32 _CutOffDay, Int32 _TotalAmount, Int32 _DiscountTypeID, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, Int32 _TaxPercentage, Int32 _TaxAmount, Int32 _TotalAmountAfterTax, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_ReWTP", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@WaterPlantCategoryID", _WaterPlantCategoryID);
                cmd.Parameters.AddWithValue("@CutOffDate", _CutOffDate);
                cmd.Parameters.AddWithValue("@CutOffDay", _CutOffDay);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@DiscountTypeID", _DiscountTypeID);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@TaxPercentage", _TaxPercentage);
                cmd.Parameters.AddWithValue("@TaxAmount", _TaxAmount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterTax", _TotalAmountAfterTax);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;
                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public DataTable retrieve_personal_identification_detail_by_lineid(Int32 _PersonalIdentificationID, Int32 _lineID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PersonalIdentificationDetail_ByLineID", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PersonalIdentificationID", _PersonalIdentificationID);
            cmd.Parameters.AddWithValue("@LineID", _lineID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 delete_personal_identification_detail_transaction(Int32 _PersonalIdentificationID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_PersonalIdentification_Detail2", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PersonalIdentificationID", _PersonalIdentificationID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region banner_type

        public DataTable retrieve_banner_type()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Banner_Type", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_banner_type(Int32 _bannertype)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Banner_Type", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerTypeID", _bannertype);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_banner_type(int _bannertypeid, String _bannertypename, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_BannerType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerTypeID", _bannertypeid);
            cmd.Parameters.AddWithValue("@BannerTypeName", _bannertypename);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_banner_type(Int32 _bannerid, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_BannerType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerTypeID", _bannerid);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region banner_location

        public DataTable retrieve_banner_location(Int32 _SiteID, Int32 _OrgID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BannerLocation", conn);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_banner_location(Int32 _BannerLocationID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BannerLocation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerLocationID", _BannerLocationID);

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_banner_location(Int32 _bannertype, Int32 _SiteID, Int32 _OrgID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BannerLocation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@BannerTypeID", _bannertype);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_banner_location(int _bannertypeid, String _bannertypename, String _bannertypeCode, Int32 _BannerTypeID, Int32 _price, String _savedby, Int32 _SiteID, Int32 _OrgID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_BannerLocation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerLocationID", _bannertypeid);
            cmd.Parameters.AddWithValue("@BannerLocationName", _bannertypename);
            cmd.Parameters.AddWithValue("@BannerLocationCode", _bannertypeCode);
            cmd.Parameters.AddWithValue("@BannerTypeID", _BannerTypeID);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@Price", _price);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_banner_location(Int32 _bannerid, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_BannerLocation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerLocationID", _bannerid);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region magazinetrxtype

        public DataTable retrieve_magazinetrxtype()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_magazinetrxtype", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_magazinetrxtype(Int32 _MSMagazineTrxTypeID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_magazinetrxtype", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MSMagazineTrxTypeID", _MSMagazineTrxTypeID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_magazinetrxtype(int _MSMagazineTrxTypeID, String _MSMagazineTrxTypeName, int _price, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_MagazineTrxType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MSMagazineTrxTypeID", _MSMagazineTrxTypeID);
            cmd.Parameters.AddWithValue("@MSMagazineTrxTypeName", _MSMagazineTrxTypeName);
            cmd.Parameters.AddWithValue("@Price", _price);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_magazinetrxtype(Int32 _MSMagazineTrxTypeID, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_MagazineTrxType", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@MSMagazineTrxTypeID", _MSMagazineTrxTypeID);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region point

        //public DataTable retrieve_pointmstMax()
        //{
        //    DataTable dt = new DataTable();

        //    SqlConnection conn = new SqlConnection(connectionstring);
        //    SqlCommand cmd = new SqlCommand("SP_Retrieve_PointMax", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //    adapter.Fill(dt);

        //    return dt;
        //}

        public DataTable retrieve_pointmst()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Point", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_pointmst(Int32 _PointID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Point", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PointID", _PointID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_point(String _PointName, int _BannerLocationID, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Point", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BannerLocationID", _BannerLocationID);
            cmd.Parameters.AddWithValue("@PointName", _PointName);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 Delete_BannerPoint(Int32 _pointID, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_BannerPoint", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PointID", _pointID);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region CarLicensePlate

        public DataTable retrieve_carlicenseplate()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarLicensePlate", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_carlicenseplate(Int32 _CarLicensePlateID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarLicensePlate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CarLicensePlateID", _CarLicensePlateID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_carlicenseplate(int _CarLicensePlateID, String _CarLicensePlateName, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_CarLicensePlate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CarLicensePlateID", _CarLicensePlateID);
            cmd.Parameters.AddWithValue("@CarLicensePlateName", _CarLicensePlateName);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_carlicenseplate(Int32 _CarLicensePlateID, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_CarLicensePlate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CarLicensePlateID", _CarLicensePlateID);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region CarRentTariff

        public DataTable retrieve_CarRentTariff()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarRentTariffMst", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_CarRentTariff(Int32 _CarRentTariffID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_CarRentTariffMst", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CarRentTariffID", _CarRentTariffID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_CarRentTariff(int _CarLicensePlateID, String _CarLicensePlateName, int _price, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_CarRentTariff", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CarRentTariffID", _CarLicensePlateID);
            cmd.Parameters.AddWithValue("@CarRentTariffName", _CarLicensePlateName);
            cmd.Parameters.AddWithValue("@Price", _price);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 delete_CarRentTariff(Int32 _CarLicensePlateID, String _savedby)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Delete_CarRentTariff", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CarRentTariffID", _CarLicensePlateID);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        #endregion

        #region PersonalIdentification

        public DataTable retrieve_Gender()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Gender", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_Religion()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Religion", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_Relation()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Relation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_Occupation()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Occupation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_LastEducation()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_LastEducation", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_MaritalStatus()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_MaritalStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        public DataTable retrieve_BloodType()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BloodType", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);

            return dt;
        }

        #endregion

        #region Environment

        public DataTable retrieve_Environment_HelpName(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Environment_HelpName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_Environment_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            DateTime? _StartDate, DateTime? _EndDate, Int32 _Quantity, Int32 _Price, Int32 _TotalAmount, Int32 _TotalDiscount,
            Int32 _TotalAmountAfterDiscount, Int32 _PeriodNo, Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Environment", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@Quantity", _Quantity);
                cmd.Parameters.AddWithValue("@Price", _Price);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@TotalDiscount", _TotalDiscount);
                cmd.Parameters.AddWithValue("@TotalAmountAfterDiscount", _TotalAmountAfterDiscount);
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //Int32 returnvalue = (Int32)returnparameter.Value;
                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public DataTable retrieve_Environment(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Environment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_Environment(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, String _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Environment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_Environment_Price(Int32 _helpid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_EnvironmentPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpID", _helpid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        #endregion

        #region Unit

        public DataTable retrieve_UnitArea(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_UnitArea", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_UnitCluster(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_UnitCluster", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_UnitCode(Int32 _OrgID, Int32 _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_UnitCode", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        #endregion

        #region ErrorLogging

        public void Save_ErrorLog(Exception _exc, String _initiatingUser)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_ErrorLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
            cmd.Parameters.AddWithValue("@Message", _exc.Message);
            cmd.Parameters.AddWithValue("@Source", _exc.Source);
            cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
            cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException);
            cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException);
            cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
            cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public DataTable retrieve_ErrorLog()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_ErrorLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_PluginErrorLog()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PluginErrorLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        #endregion

        #region Security Deposit

        public DataTable retrieve_SecurityDeposit(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SecurityDeposit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_SecurityDeposit(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, String _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SecurityDeposit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_SecurityDeposit_transaction(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime? _OverdueDate,
            String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_SecurityDeposit", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string update_relatedCaseNumberSecurityDeposit(String _CaseNumber, String _RelatedCaseNumber)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Update_RelatedCaseSecurityDeposit", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@RelatedCaseNumber", _RelatedCaseNumber);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return null;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        #endregion

        #region NewCodeByLK
        public DataTable GetMSWaterPaskem(int orgID, int siteID, string unitCode, string unitNo, int duration)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SPMSWaterPaskem_Get", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("OrgID", orgID);
            cmd.Parameters.AddWithValue("SiteID", siteID);
            cmd.Parameters.AddWithValue("UnitCode", unitCode);
            cmd.Parameters.AddWithValue("UnitNo", unitNo);
            cmd.Parameters.AddWithValue("Duration", duration);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        #endregion

        #region smartcard

        //start new request DEC 2016
        public DataTable retrieve_smartcard_unit_incase(int _orgID, int _siteID, String _unitCode, string _unitNo)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_Unit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_auto_unregister(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_AutoUnregister", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_history_byunit(int _orgID, int _siteID, String _unitCode, string _unitNo)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_History_ByUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_existing_byunit(int _orgID, int _siteID, String _unitCode, string _unitNo)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_Existing_ByUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_deactive_bysystem(int _orgID, int _siteID, String _cardNumber, string _createdBy)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_InactiveBySystem", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CardNumber", _cardNumber);
            cmd.Parameters.AddWithValue("@CreatedBy", _createdBy);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_msboomgate_byunit(int _orgID, int _siteID, int _customerType, String _psCode, string _unitCode, string _unitNo)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_MSBoomGate_ByUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CustomerType", _customerType);
            cmd.Parameters.AddWithValue("@PSCode", _psCode);
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_msboomgate_bypscode(int _orgID, int _siteID, int _customerType, String _psCode)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_MSBoomGate_ByPSCode", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CustomerType", _customerType);
            cmd.Parameters.AddWithValue("@PSCode", _psCode);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_msboomgate_bypscode_unit_filter(int _orgID, int _siteID, int _customerType, String _psCode, string _unitCode, string _unitNo)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_MSBoomGate_ByPSCode_UnitFilter", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CustomerType", _customerType);
            cmd.Parameters.AddWithValue("@PSCode", _psCode);
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_unregister_detail(int _orgID, int _siteID, string _cardNumber, String _boomGateCode)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_Unregister_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CardNumber", _cardNumber);
            cmd.Parameters.AddWithValue("@BoomGateCode", _boomGateCode);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_unregister_boomgate_byunit(int _orgID, int _siteID, string _unitCode, String _unitNo)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_Unregister_BoomGate_ByUnit", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_autounregister_byboomgate(int _orgID, int _siteID, string _psCode, String _boomGateCode)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_AutoUnregister_ByBoomGate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@PSCode", _psCode);
            cmd.Parameters.AddWithValue("@BoomGateCode", _boomGateCode);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        //public DataTable retrieve_smartcard_viewer(int _orgID, int _siteID, string _unitCode, String _unitNo)
        //{
        //    DataTable dt = new DataTable();

        //    SqlConnection conn = new SqlConnection(connectionstring);

        //    SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_Viewer", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@OrgID", _orgID);
        //    cmd.Parameters.AddWithValue("@SiteID", _siteID);
        //    cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
        //    cmd.Parameters.AddWithValue("@UnitNo", _unitNo);

        //    conn.Open();
        //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //    adapter.Fill(dt);
        //    conn.Close();

        //    return dt;
        //}

        public DataTable retrieve_additional_access_gate(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, String _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Additional_Access_Gate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_additional_access_gate_transaction(
            Int32 _OrgID, Int32 _SiteID, int _TransactionType, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            Int32 _Quantity, string _ResidentStatus, DateTime? _RentPeriodedate, int _CardTypeID,
            string _email, string _phone, string _mobilephone, string _fax, int _price,
            int _TotalAmount,
            bool IsIdentitasDiri, string IsIdentitasDiri_fileName, string IsIdentitasDiri_fileType, byte[] IsIdentitasDiri_fileData,
            bool IsKK, string IsKK_fileName, string IsKK_fileType, byte[] IsKK_fileData,
            bool IsSTNK, string IsSTNK_fileName, string IsSTNK_fileType, byte[] IsSTNK_fileData,
            bool IsKepemilikanUnit, string IsKepemilikanUnit_fileName, string IsKepemilikanUnit_fileType, byte[] IsKepemilikanUnit_fileData,
            bool IsPerjanjianSewaMenyewaUnit, string IsPerjanjianSewaMenyewaUnit_fileName, string IsPerjanjianSewaMenyewaUnit_fileType, byte[] IsPerjanjianSewaMenyewaUnit_fileData,
            bool IsSuratKeteranganKaryawan, string IsSuratKeteranganKaryawan_fileName, string IsSuratKeteranganKaryawan_fileType, byte[] IsSuratKeteranganKaryawan_fileData,
            bool IsBuktiBayarUtilitas, string IsBuktiBayarUtilitas_fileName, string IsBuktiBayarUtilitas_fileType, byte[] IsBuktiBayarUtilitas_fileData,
            bool IsSuratKuasa, string IsSuratKuasa_fileName, string IsSuratKuasa_fileType, byte[] IsSuratKuasa_fileData,
            bool IsKTPPemberiPenerimaKuasa, string IsKTPPemberiPenerimaKuasa_fileName, string IsKTPPemberiPenerimaKuasa_fileType, byte[] IsKTPPemberiPenerimaKuasa_fileData,
            string lainlain, string lainlain_fileName, string lainlain_fileType, byte[] lainlain_fileData,
            Int32 _PeriodNo,
            Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_SmartCard_Additional_Access", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@TransactionType", _TransactionType);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@Quantity", _Quantity);
                cmd.Parameters.AddWithValue("@ResidentStatus", _ResidentStatus);
                cmd.Parameters.AddWithValue("@RentPeriodeDate", _RentPeriodedate);
                cmd.Parameters.AddWithValue("@CardTypeID", _CardTypeID);
                cmd.Parameters.AddWithValue("@Email", _email);
                cmd.Parameters.AddWithValue("@Phone", _phone);
                cmd.Parameters.AddWithValue("@MobilePhone", _mobilephone);
                cmd.Parameters.AddWithValue("@Fax", _fax);
                cmd.Parameters.AddWithValue("@Price", _price);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);

                cmd.Parameters.AddWithValue("@IsIdentitasDiri", IsIdentitasDiri);
                cmd.Parameters.AddWithValue("@IdentitasDiriFileName", IsIdentitasDiri_fileName);
                cmd.Parameters.AddWithValue("@IdentitasDiriFileType", IsIdentitasDiri_fileType);
                cmd.Parameters.AddWithValue("@IdentitasDiriFileUpload", IsIdentitasDiri_fileData);

                cmd.Parameters.AddWithValue("@IsKK", IsKK);
                cmd.Parameters.AddWithValue("@KKFileName", IsKK_fileName);
                cmd.Parameters.AddWithValue("@KKFileType", IsKK_fileType);
                cmd.Parameters.AddWithValue("@KKFileUpload", IsKK_fileData);

                cmd.Parameters.AddWithValue("@IsSTNK", IsSTNK);
                cmd.Parameters.AddWithValue("@STNKFileName", IsSTNK_fileName);
                cmd.Parameters.AddWithValue("@STNKFileType", IsSTNK_fileType);
                cmd.Parameters.AddWithValue("@STNKFileUpload", IsSTNK_fileData);

                cmd.Parameters.AddWithValue("@IsKepemilikanUnit", IsKepemilikanUnit);
                cmd.Parameters.AddWithValue("@KepemilikanUnitFileName", IsKepemilikanUnit_fileName);
                cmd.Parameters.AddWithValue("@KepemilikanUnitFileType", IsKepemilikanUnit_fileType);
                cmd.Parameters.AddWithValue("@KepemilikanUnitFileUpload", IsKepemilikanUnit_fileData);

                cmd.Parameters.AddWithValue("@IsPerjanjianSewaMenyewaUnit", IsPerjanjianSewaMenyewaUnit);
                cmd.Parameters.AddWithValue("@PerjanjianSewaMenyewaUnitFileName", IsPerjanjianSewaMenyewaUnit_fileName);
                cmd.Parameters.AddWithValue("@PerjanjianSewaMenyewaUnitFileType", IsPerjanjianSewaMenyewaUnit_fileType);
                cmd.Parameters.AddWithValue("@PerjanjianSewaMenyewaUnitFileUpload", IsPerjanjianSewaMenyewaUnit_fileData);

                cmd.Parameters.AddWithValue("@IsSuratKeteranganKaryawan", IsSuratKeteranganKaryawan);
                cmd.Parameters.AddWithValue("@SuratKeteranganKaryawanFileName", IsSuratKeteranganKaryawan_fileName);
                cmd.Parameters.AddWithValue("@SuratKeteranganKaryawanFileType", IsSuratKeteranganKaryawan_fileType);
                cmd.Parameters.AddWithValue("@SuratKeteranganKaryawanFileUpload", IsSuratKeteranganKaryawan_fileData);

                cmd.Parameters.AddWithValue("@IsBuktiBayarUtilitas", IsBuktiBayarUtilitas);
                cmd.Parameters.AddWithValue("@BuktiBayarUtilitasFileName", IsBuktiBayarUtilitas_fileName);
                cmd.Parameters.AddWithValue("@BuktiBayarUtilitasFileType", IsBuktiBayarUtilitas_fileType);
                cmd.Parameters.AddWithValue("@BuktiBayarUtilitasFileUpload", IsBuktiBayarUtilitas_fileData);

                cmd.Parameters.AddWithValue("@IsSuratKuasa", IsSuratKuasa);
                cmd.Parameters.AddWithValue("@SuratKuasaFileName", IsSuratKuasa_fileName);
                cmd.Parameters.AddWithValue("@SuratKuasaFileType", IsSuratKuasa_fileType);
                cmd.Parameters.AddWithValue("@SuratKuasaFileUpload", IsSuratKuasa_fileData);

                cmd.Parameters.AddWithValue("@IsKTPPemberiPenerimaKuasa", IsKTPPemberiPenerimaKuasa);
                cmd.Parameters.AddWithValue("@KTPPemberiPenerimaKuasaFileName", IsKTPPemberiPenerimaKuasa_fileName);
                cmd.Parameters.AddWithValue("@KTPPemberiPenerimaKuasaFileType", IsKTPPemberiPenerimaKuasa_fileType);
                cmd.Parameters.AddWithValue("@KTPPemberiPenerimaKuasaFileUpload", IsKTPPemberiPenerimaKuasa_fileData);

                cmd.Parameters.AddWithValue("@lainlain", lainlain);
                cmd.Parameters.AddWithValue("@lainlainFileName", lainlain_fileName);
                cmd.Parameters.AddWithValue("@lainlainFileType", lainlain_fileType);
                cmd.Parameters.AddWithValue("@lainlainFileUpload", lainlain_fileData);

                cmd.Parameters.AddWithValue("@ActualAmount", _TotalAmount);// actial amount = total {{smartcard}}
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public DataTable retrieve_additional_access_gate_price(int _SmartCardTypeID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Additional_Access_Gate_Price", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SmartCardTypeID", _SmartCardTypeID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_access_gate_registration(Int32 _OrgID, Int32 _SiteID, int? _CaseStatusID, String _Search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Access_Gate_Registration", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
            cmd.Parameters.AddWithValue("@Search", _Search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_id(Int32 _OrgID, Int32 _SiteID, String _CaseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_ID", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_additional_access_card_detail(Int32 _OrgID, Int32 _SiteID, string _CardNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Additional_Access_Card_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CardNumber", _CardNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        //end new request dec 2016

        //start new request 29 dec 2016
        public DataTable retrieve_smartcard_viewer(Int32 _OrgID, Int32 _SiteID, string _CardDetailData, string _UnitCoode, string _UnitNo)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_SmartCard_Viewer", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CardDetailData", _CardDetailData);
            cmd.Parameters.AddWithValue("@UnitCode", _UnitCoode);
            cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);


            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
        //end new request 29 dec 2016



        public DataTable retrieve_smartcard_history(String _PSCode)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Smartcard_History", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PSCode", _PSCode);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_membershipcard_ownership(String _PSCode)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_MembershipCard", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PSCode", _PSCode);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_file_upload(String _CaseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_File_Upload", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_uplsmartcard(int _OrgId, int _SiteID, String _Search, string _cardstatus)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("RetrieveUplSmartcard", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgId);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@searchText", _Search);
            cmd.Parameters.AddWithValue("@CardStatus", _cardstatus);//add by bili


            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_registration(Int32 _OrgID, Int32 _SiteID, int? _CaseStatusID, String _Search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Smartcard_Registration", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
            cmd.Parameters.AddWithValue("@Search", _Search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_registration(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Smartcard_Registration", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }


        public DataTable retrieve_smartcard(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, String _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_unregister_smartcard(Int32 _orgid, Int32 _siteid, Int32 _casestatusid, Int32 _departmentid, String _search)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_Unregistration", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CaseStatusID", _casestatusid);
            cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@Search", _search);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);
            cmd.CommandTimeout = 120;
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_card_detail(int _SmartCardID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_Card_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SmartCardID", _SmartCardID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_transaction_bycase(string casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_Transaction_byCase", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_pscode(String _casenumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PSCode", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_boomgatecode(int BoomgateID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_BoomGateCode", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BoomgateID", BoomgateID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        //public DataTable retrieve_boomgate(String _PSCode)
        //{
        //    DataTable dt = new DataTable();

        //    SqlConnection conn = new SqlConnection(connectionstring);
        //    SqlCommand cmd = new SqlCommand("SP_Retrieve_Boom_Gate_v3", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@PSCode", _PSCode);

        //    conn.Open();
        //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //    adapter.Fill(dt);
        //    conn.Close();

        //    return dt;
        //}

        public DataTable retrieve_msboomgate(int _OrgID, int _SiteID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_MSBoomGate", conn);
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String save_smartcard_transaction(
            Int32 _OrgID, Int32 _SiteID, int _TransactionType, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            Int32 _Quantity, string _ResidentStatus, DateTime? _RentPeriodedate, int _CardTypeID,
            string _email, string _phone, string _mobilephone, string _fax, int _price,
            int _TotalAmount,
            bool IsIdentitasDiri, string IsIdentitasDiri_fileName, string IsIdentitasDiri_fileType, byte[] IsIdentitasDiri_fileData,
            bool IsKK, string IsKK_fileName, string IsKK_fileType, byte[] IsKK_fileData,
            bool IsSTNK, string IsSTNK_fileName, string IsSTNK_fileType, byte[] IsSTNK_fileData,
            bool IsKepemilikanUnit, string IsKepemilikanUnit_fileName, string IsKepemilikanUnit_fileType, byte[] IsKepemilikanUnit_fileData,
            bool IsPerjanjianSewaMenyewaUnit, string IsPerjanjianSewaMenyewaUnit_fileName, string IsPerjanjianSewaMenyewaUnit_fileType, byte[] IsPerjanjianSewaMenyewaUnit_fileData,
            bool IsSuratKeteranganKaryawan, string IsSuratKeteranganKaryawan_fileName, string IsSuratKeteranganKaryawan_fileType, byte[] IsSuratKeteranganKaryawan_fileData,
            bool IsBuktiBayarUtilitas, string IsBuktiBayarUtilitas_fileName, string IsBuktiBayarUtilitas_fileType, byte[] IsBuktiBayarUtilitas_fileData,
            bool IsSuratKuasa, string IsSuratKuasa_fileName, string IsSuratKuasa_fileType, byte[] IsSuratKuasa_fileData,
            bool IsKTPPemberiPenerimaKuasa, string IsKTPPemberiPenerimaKuasa_fileName, string IsKTPPemberiPenerimaKuasa_fileType, byte[] IsKTPPemberiPenerimaKuasa_fileData,
            string lainlain, string lainlain_fileName, string lainlain_fileType, byte[] lainlain_fileData,
            Int32 _PeriodNo,
            Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_SmartCard", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@TransactionType", _TransactionType);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@Quantity", _Quantity);
                cmd.Parameters.AddWithValue("@ResidentStatus", _ResidentStatus);
                cmd.Parameters.AddWithValue("@RentPeriodeDate", _RentPeriodedate);
                cmd.Parameters.AddWithValue("@CardTypeID", _CardTypeID);
                cmd.Parameters.AddWithValue("@Email", _email);
                cmd.Parameters.AddWithValue("@Phone", _phone);
                cmd.Parameters.AddWithValue("@MobilePhone", _mobilephone);
                cmd.Parameters.AddWithValue("@Fax", _fax);
                cmd.Parameters.AddWithValue("@Price", _price);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);

                cmd.Parameters.AddWithValue("@IsIdentitasDiri", IsIdentitasDiri);
                cmd.Parameters.AddWithValue("@IdentitasDiriFileName", IsIdentitasDiri_fileName);
                cmd.Parameters.AddWithValue("@IdentitasDiriFileType", IsIdentitasDiri_fileType);
                cmd.Parameters.AddWithValue("@IdentitasDiriFileUpload", IsIdentitasDiri_fileData);

                cmd.Parameters.AddWithValue("@IsKK", IsKK);
                cmd.Parameters.AddWithValue("@KKFileName", IsKK_fileName);
                cmd.Parameters.AddWithValue("@KKFileType", IsKK_fileType);
                cmd.Parameters.AddWithValue("@KKFileUpload", IsKK_fileData);

                cmd.Parameters.AddWithValue("@IsSTNK", IsSTNK);
                cmd.Parameters.AddWithValue("@STNKFileName", IsSTNK_fileName);
                cmd.Parameters.AddWithValue("@STNKFileType", IsSTNK_fileType);
                cmd.Parameters.AddWithValue("@STNKFileUpload", IsSTNK_fileData);

                cmd.Parameters.AddWithValue("@IsKepemilikanUnit", IsKepemilikanUnit);
                cmd.Parameters.AddWithValue("@KepemilikanUnitFileName", IsKepemilikanUnit_fileName);
                cmd.Parameters.AddWithValue("@KepemilikanUnitFileType", IsKepemilikanUnit_fileType);
                cmd.Parameters.AddWithValue("@KepemilikanUnitFileUpload", IsKepemilikanUnit_fileData);

                cmd.Parameters.AddWithValue("@IsPerjanjianSewaMenyewaUnit", IsPerjanjianSewaMenyewaUnit);
                cmd.Parameters.AddWithValue("@PerjanjianSewaMenyewaUnitFileName", IsPerjanjianSewaMenyewaUnit_fileName);
                cmd.Parameters.AddWithValue("@PerjanjianSewaMenyewaUnitFileType", IsPerjanjianSewaMenyewaUnit_fileType);
                cmd.Parameters.AddWithValue("@PerjanjianSewaMenyewaUnitFileUpload", IsPerjanjianSewaMenyewaUnit_fileData);

                cmd.Parameters.AddWithValue("@IsSuratKeteranganKaryawan", IsSuratKeteranganKaryawan);
                cmd.Parameters.AddWithValue("@SuratKeteranganKaryawanFileName", IsSuratKeteranganKaryawan_fileName);
                cmd.Parameters.AddWithValue("@SuratKeteranganKaryawanFileType", IsSuratKeteranganKaryawan_fileType);
                cmd.Parameters.AddWithValue("@SuratKeteranganKaryawanFileUpload", IsSuratKeteranganKaryawan_fileData);

                cmd.Parameters.AddWithValue("@IsBuktiBayarUtilitas", IsBuktiBayarUtilitas);
                cmd.Parameters.AddWithValue("@BuktiBayarUtilitasFileName", IsBuktiBayarUtilitas_fileName);
                cmd.Parameters.AddWithValue("@BuktiBayarUtilitasFileType", IsBuktiBayarUtilitas_fileType);
                cmd.Parameters.AddWithValue("@BuktiBayarUtilitasFileUpload", IsBuktiBayarUtilitas_fileData);

                cmd.Parameters.AddWithValue("@IsSuratKuasa", IsSuratKuasa);
                cmd.Parameters.AddWithValue("@SuratKuasaFileName", IsSuratKuasa_fileName);
                cmd.Parameters.AddWithValue("@SuratKuasaFileType", IsSuratKuasa_fileType);
                cmd.Parameters.AddWithValue("@SuratKuasaFileUpload", IsSuratKuasa_fileData);

                cmd.Parameters.AddWithValue("@IsKTPPemberiPenerimaKuasa", IsKTPPemberiPenerimaKuasa);
                cmd.Parameters.AddWithValue("@KTPPemberiPenerimaKuasaFileName", IsKTPPemberiPenerimaKuasa_fileName);
                cmd.Parameters.AddWithValue("@KTPPemberiPenerimaKuasaFileType", IsKTPPemberiPenerimaKuasa_fileType);
                cmd.Parameters.AddWithValue("@KTPPemberiPenerimaKuasaFileUpload", IsKTPPemberiPenerimaKuasa_fileData);

                cmd.Parameters.AddWithValue("@lainlain", lainlain);
                cmd.Parameters.AddWithValue("@lainlainFileName", lainlain_fileName);
                cmd.Parameters.AddWithValue("@lainlainFileType", lainlain_fileType);
                cmd.Parameters.AddWithValue("@lainlainFileUpload", lainlain_fileData);

                cmd.Parameters.AddWithValue("@ActualAmount", _TotalAmount);// actial amount = total {{smartcard}}
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public String save_smartcard_replacement_transaction(
            Int32 _OrgID, Int32 _SiteID, int _TransactionType, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            Int32 _Quantity, string _ResidentStatus, DateTime? _RentPeriodeDate, int _CardTypeID,
            string _email, string _phone, string _mobilephone, string _fax, int _price,
            int _TotalAmount,
            bool IsIdentitasDiri, bool IsKK, bool IsSTNK, bool IsKepemilikanUnit, bool IsPerjanjianSewaMenyewaUnit, bool IsSuratKeteranganKaryawan, bool IsBuktiBayarUtilitas, bool IsSuratKuasa, bool IsKTPPemberiPenerimaKuasa, string lainlain,
            Int32 _PeriodNo,
            Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_SmartCard_Replacement", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@TransactionType", _TransactionType);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@Quantity", _Quantity);
                cmd.Parameters.AddWithValue("@ResidentStatus", _ResidentStatus);
                cmd.Parameters.AddWithValue("@RentPeriodeDate", _RentPeriodeDate);
                cmd.Parameters.AddWithValue("@CardTypeID", _CardTypeID);
                cmd.Parameters.AddWithValue("@Email", _email);
                cmd.Parameters.AddWithValue("@Phone", _phone);
                cmd.Parameters.AddWithValue("@MobilePhone", _mobilephone);
                cmd.Parameters.AddWithValue("@Fax", _fax);
                cmd.Parameters.AddWithValue("@Price", _price);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                cmd.Parameters.AddWithValue("@IsIdentitasDiri", IsIdentitasDiri);
                cmd.Parameters.AddWithValue("@IsKK", IsKK);
                cmd.Parameters.AddWithValue("@IsSTNK", IsSTNK);
                cmd.Parameters.AddWithValue("@IsKepemilikanUnit", IsKepemilikanUnit);
                cmd.Parameters.AddWithValue("@IsPerjanjianSewaMenyewaUnit", IsPerjanjianSewaMenyewaUnit);
                cmd.Parameters.AddWithValue("@IsSuratKeteranganKaryawan", IsSuratKeteranganKaryawan);
                cmd.Parameters.AddWithValue("@IsBuktiBayarUtilitas", IsBuktiBayarUtilitas);
                cmd.Parameters.AddWithValue("@IsSuratKuasa", IsSuratKuasa);
                cmd.Parameters.AddWithValue("@IsKTPPemberiPenerimaKuasa", IsKTPPemberiPenerimaKuasa);
                cmd.Parameters.AddWithValue("@lainlain", lainlain);
                cmd.Parameters.AddWithValue("@ActualAmount", _TotalAmount);// actial amount = total {{smartcard}}
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        //Int32 _OrgID, Int32 _SiteID, int _TransactionType, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
        //   Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
        //   String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
        //   Int32? _Quantity, string _ResidentStatus, int? _CardTypeID,
        //   string _email, string _phone, string _mobilephone, string _fax, int _price,
        //   int _TotalAmount,
        //   bool IsIdentitasDiri, bool IsKK, bool IsSTNK, bool IsKepemilikanUnit, bool IsPerjanjianSewaMenyewaUnit, bool IsSuratKeteranganKaryawan, bool IsBuktiBayarUtilitas, bool IsSuratKuasa, bool IsKTPPemberiPenerimaKuasa, string lainlain,
        //   Int32 _PeriodNo,
        //   Boolean _IsSettled, String _SavedBy, String _FullName

        public String[] save_smartcard_unregister_transaction(
            Int32 _OrgID, Int32 _SiteID, int _TransactionType, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime _OverdueDate,
            Int32? _Quantity, string _ResidentStatus, int? _CardTypeID,
            string _email, string _phone, string _mobilephone, string _fax, int _price,
            int _TotalAmount,
            Int32 _PeriodNo,
            Boolean _IsSettled, String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_SmartCard_Unregister", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@TransactionType", _TransactionType);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@Quantity", _Quantity);
                cmd.Parameters.AddWithValue("@ResidentStatus", _ResidentStatus);
                cmd.Parameters.AddWithValue("@CardTypeID", _CardTypeID);
                cmd.Parameters.AddWithValue("@Email", _email);
                cmd.Parameters.AddWithValue("@Phone", _phone);
                cmd.Parameters.AddWithValue("@MobilePhone", _mobilephone);
                cmd.Parameters.AddWithValue("@Fax", _fax);
                cmd.Parameters.AddWithValue("@Price", _price);
                cmd.Parameters.AddWithValue("@TotalAmount", _TotalAmount);
                //cmd.Parameters.AddWithValue("@IsIdentitasDiri", IsIdentitasDiri);
                //cmd.Parameters.AddWithValue("@IsKK", IsKK);
                //cmd.Parameters.AddWithValue("@IsSTNK", IsSTNK);
                //cmd.Parameters.AddWithValue("@IsKepemilikanUnit", IsKepemilikanUnit);
                //cmd.Parameters.AddWithValue("@IsPerjanjianSewaMenyewaUnit", IsPerjanjianSewaMenyewaUnit);
                //cmd.Parameters.AddWithValue("@IsSuratKeteranganKaryawan", IsSuratKeteranganKaryawan);
                //cmd.Parameters.AddWithValue("@IsBuktiBayarUtilitas", IsBuktiBayarUtilitas);
                //cmd.Parameters.AddWithValue("@IsSuratKuasa", IsSuratKuasa);
                //cmd.Parameters.AddWithValue("@IsKTPPemberiPenerimaKuasa", IsKTPPemberiPenerimaKuasa);
                //cmd.Parameters.AddWithValue("@lainlain", lainlain);
                cmd.Parameters.AddWithValue("@ActualAmount", _TotalAmount);// actial amount = total {{smartcard}}
                cmd.Parameters.AddWithValue("@PeriodNo", _PeriodNo);
                cmd.Parameters.AddWithValue("@IsSettled", _IsSettled);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                cmd.Parameters.Add("@NewID", SqlDbType.Int).Direction = ParameterDirection.Output;

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                //return returnparameter.Value.ToString();
                int New_Smartcard_ID = Convert.ToInt32(cmd.Parameters["@NewID"].Value);
                string return_parameter = returnparameter.Value.ToString();
                string new_id = cmd.Parameters["@NewID"].Value.ToString();
                string[] returnvalue = new string[] { return_parameter, new_id };

                return returnvalue;
            }
            catch (Exception e)
            {
                //return e.Message;
                return new string[] { e.Message, "" };
            }
        }

        public String[] save_card_detail(int OrgID, int SiteID, int SmartCardID, string CardNumber, string ChipNumber, string HolderName, string IDCardNumber, string OfficeAddress, string MobilePhone, String VehicleNumber, int VehicleType, string VehicleBrand, DateTime? EffectiveDate, DateTime? ExpiredDate, string CreatedBy)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Card_Detail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", OrgID);
                cmd.Parameters.AddWithValue("@SiteID", SiteID);
                cmd.Parameters.AddWithValue("@SmartCardID", SmartCardID);
                cmd.Parameters.AddWithValue("@CardNumber", CardNumber);
                cmd.Parameters.AddWithValue("@ChipNumber", ChipNumber);
                cmd.Parameters.AddWithValue("@HolderName", HolderName);
                cmd.Parameters.AddWithValue("@IDCardNumber", IDCardNumber);
                cmd.Parameters.AddWithValue("@OfficeAddress", OfficeAddress);
                cmd.Parameters.AddWithValue("@MobilePhone", MobilePhone);
                cmd.Parameters.AddWithValue("@VehicleNumber", VehicleNumber);
                cmd.Parameters.AddWithValue("@VehicleType", VehicleType);
                cmd.Parameters.AddWithValue("@VehicleBrand", VehicleBrand);
                cmd.Parameters.AddWithValue("@EffectiveDate", EffectiveDate);
                cmd.Parameters.AddWithValue("@ExpiredDate", ExpiredDate);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);

                cmd.Parameters.Add("@NewID", SqlDbType.Int).Direction = ParameterDirection.Output;

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                int New_card_detail_ID = Convert.ToInt32(cmd.Parameters["@NewID"].Value);
                ////Int32 returnvalue = (Int32)returnparameter.Value;

                //return returnparameter.Value.ToString();
                string return_parameter = returnparameter.Value.ToString();
                string new_id = cmd.Parameters["@NewID"].Value.ToString();
                string[] returnvalue = new string[] { return_parameter, new_id };

                return returnvalue;
            }
            catch (Exception e)
            {
                return new string[] { e.Message, "" };
            }
        }

        public String[] save_card_detail_unregister(int OrgID, int SiteID, int SmartCardID, string CardNumber, string ChipNumber, string CreatedBy)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Card_Detail_Unregister", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", OrgID);
                cmd.Parameters.AddWithValue("@SiteID", SiteID);
                cmd.Parameters.AddWithValue("@SmartCardID", SmartCardID);
                cmd.Parameters.AddWithValue("@CardNumber", CardNumber);
                cmd.Parameters.AddWithValue("@ChipNumber", ChipNumber);
                //cmd.Parameters.AddWithValue("@HolderName", HolderName);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);

                cmd.Parameters.Add("@NewID", SqlDbType.Int).Direction = ParameterDirection.Output;

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                int New_card_detail_ID = Convert.ToInt32(cmd.Parameters["@NewID"].Value);
                ////Int32 returnvalue = (Int32)returnparameter.Value;

                //return returnparameter.Value.ToString();
                string return_parameter = returnparameter.Value.ToString();
                string new_id = cmd.Parameters["@NewID"].Value.ToString();
                string[] returnvalue = new string[] { return_parameter, new_id };

                return returnvalue;
            }
            catch (Exception e)
            {
                return new string[] { e.Message, "" };
            }
        }

        public String insert_transaction_cluster_access(int OrgID, int SiteID, int SmartCardDetailID, int BoomGateID, int Sync)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Insert_TRClusterAccess", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", OrgID);
                cmd.Parameters.AddWithValue("@SiteID", SiteID);
                cmd.Parameters.AddWithValue("@SmartCardDetailID", SmartCardDetailID);
                cmd.Parameters.AddWithValue("@BoomGateID", BoomGateID);
                cmd.Parameters.AddWithValue("@Sync", Sync);


                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                ////Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public String insert_membership_transaction(int MembershipID, int BoomGateID, int Status, int Sync)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Insert_Membership_Transaction", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MembershipID", MembershipID);
                cmd.Parameters.AddWithValue("@BoomGateID", BoomGateID);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@Sync", Sync);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                ////Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }


        public DataTable retrieve_master_card_type()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCardType", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_card_price(int _cardtypeid)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCardPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SmartCardTypeID", _cardtypeid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        //public DataTable retrieve_validity_periode()
        //{
        //    DataTable dt = new DataTable();

        //    SqlConnection conn = new SqlConnection(connectionstring);
        //    SqlCommand cmd = new SqlCommand("SP_Retrieve_MSValidityPeriode", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    conn.Open();
        //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //    adapter.Fill(dt);
        //    conn.Close();

        //    return dt;
        //}

        //public DataTable retrieve_resident_status()
        //{
        //    DataTable dt = new DataTable();

        //    SqlConnection conn = new SqlConnection(connectionstring);
        //    SqlCommand cmd = new SqlCommand("SP_Retrieve_Resident_Status_v3", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    conn.Open();
        //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //    adapter.Fill(dt);
        //    conn.Close();

        //    return dt;
        //}

        //public DataTable retrieve_quantity()
        //{
        //    DataTable dt = new DataTable();

        //    SqlConnection conn = new SqlConnection(connectionstring);
        //    SqlCommand cmd = new SqlCommand("SP_Retrieve_Quantity_v3", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    conn.Open();
        //    SqlDataAdapter adapter = new SqlDataAdapter(cmd);
        //    adapter.Fill(dt);
        //    conn.Close();

        //    return dt;
        //}

        //retrieve_access_card
        public DataTable retrieve_membership_card(int _OrgID, int _SiteID, string _serachnumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            //SqlCommand cmd = new SqlCommand("SP_Retrieve_Access_Card", conn);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_MSMembershipCard", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CardNumber", _serachnumber);
            cmd.Parameters.AddWithValue("@ChipNumber", _serachnumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_selected_cluster(int SmartcardDetailID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_selected_cluster", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SmartCardDetailID", SmartcardDetailID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_smartcard_price(int _SmartCardTypeID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCardPrice", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SmartCardTypeID", _SmartCardTypeID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_membershipcard_detail(string CardNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_MembershipCard_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CardNumber", CardNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String update_issued_to_member(string CardNumber, string PSCode)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Update_Membership_Status", conn);//SP_Update_MSSmartCard
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CardNumber", CardNumber);
                cmd.Parameters.AddWithValue("@PSCode", PSCode);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                ////Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public String update_unregister_card(int MembershipID, int BoomGateID, int Sync)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Deactive_SmartCard", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MembershipID", MembershipID);
                cmd.Parameters.AddWithValue("@BoomGateID", BoomGateID);
                cmd.Parameters.AddWithValue("@Sync", Sync);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                ////Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public Int32 save_uplmssmartcard(int _OrgID, int _SiteID, String _CardNumber, String _ChipNumber, String _CardStatus, bool _IsActive, string _SavedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_UplMSSmartCard", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CardNumberParam", _CardNumber);
            cmd.Parameters.AddWithValue("@ChipNumberParam", _ChipNumber);
            cmd.Parameters.AddWithValue("@CardStatusParam", _CardStatus);
            cmd.Parameters.AddWithValue("@IsActParam", _IsActive);
            cmd.Parameters.AddWithValue("@CreatedBy", _SavedBy);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        public Int32 save_uplmssmartcard_visitor(int _OrgID, int _SiteID, String _CardNumber, String _ChipNumber, string _PSCode, String _CardStatus, bool _IsActive, bool _IsVisitor, string _savedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_UplMSSmartCardVisitor", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CardNumber", _CardNumber);
            cmd.Parameters.AddWithValue("@ChipNumber", _ChipNumber);
            cmd.Parameters.AddWithValue("@PSCode", _PSCode);
            cmd.Parameters.AddWithValue("@CardStatus", _CardStatus);
            cmd.Parameters.AddWithValue("@IsActive", _IsActive);
            cmd.Parameters.AddWithValue("@IsVisitor", _IsVisitor);
            cmd.Parameters.AddWithValue("@CreatedBy", _savedBy);

            cmd.Parameters.Add("@NewID", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)cmd.Parameters["@NewID"].Value;

            return returnvalue;
        }

        public DataTable retrieve_boomgateID(int _OrgID, int _SiteID, string _BoomGateCode)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Retrieve_BoomGateID", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@BoomGateCode", _BoomGateCode);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 validate_upload_cardnumber(String _CardNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Validate_Upload_CardNumber", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CardNumber", _CardNumber);

            conn.Open();
            Int32 returnvalue = (int)cmd.ExecuteScalar();
            conn.Close();

            return returnvalue;
        }

        public Int32 save_pushdata_error(String Boom, String Mifare, String Nama, string Alamat, string Status, string ErrorMessage)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_PushDataError", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Boom", Boom);
            cmd.Parameters.AddWithValue("@Mifare", Mifare);
            cmd.Parameters.AddWithValue("@Nama", Nama);
            cmd.Parameters.AddWithValue("@Alamat", Alamat);
            cmd.Parameters.AddWithValue("@Status", Status);
            cmd.Parameters.AddWithValue("@ErrorMessage", ErrorMessage);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        //add by bili 31 maret 2017
        //public Int32 save_pushdata_log(string _sourcePush, string _boom, string _mifare, string _nama, string _alamat, string _jenis, string _cardno, string _status, DateTime? _kadaluarsa, string _psCode, DateTime _issued, string _msgPush, int _flag)
        //{
        //    SqlConnection conn = new SqlConnection(connectionstring);
        //    SqlCommand cmd = new SqlCommand("SP_Save_PushDataLog", conn);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("@SourcePush", _sourcePush);
        //    cmd.Parameters.AddWithValue("@Boom", _boom);
        //    cmd.Parameters.AddWithValue("@Mifare", _mifare);
        //    cmd.Parameters.AddWithValue("@Nama", _nama);
        //    cmd.Parameters.AddWithValue("@Alamat", _alamat);
        //    cmd.Parameters.AddWithValue("@CardNo", _cardno);
        //    cmd.Parameters.AddWithValue("@Status", _status);
        //    cmd.Parameters.AddWithValue("@Kadaluarsa", _kadaluarsa);
        //    cmd.Parameters.AddWithValue("@PsCode", _psCode);
        //    cmd.Parameters.AddWithValue("@Issued", _issued);
        //    cmd.Parameters.AddWithValue("@MessagePush", _msgPush);
        //    cmd.Parameters.AddWithValue("@Flag", _flag);

        //    conn.Open();
        //    Int32 returnvalue = cmd.ExecuteNonQuery();
        //    conn.Close();

        //    return returnvalue;
        //}

        public String validate_vehicle_number(string VehicleNumber)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_VehicleNumber_validation", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@VehicleNumber", VehicleNumber);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                ////Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public String validate_card_number(string CardNumber)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_CardNumber_Validation", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CardNumber", CardNumber);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                ////Int32 returnvalue = (Int32)returnparameter.Value;

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        #endregion smartcard

        #region complain and information in rs

        public DataTable retrieve_helpname(int _orgID, int _siteID, int _helpCategoryID)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("splk_retrieve_helpname", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@HelpCategoryID", _helpCategoryID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_location(int _orgID, int _siteID)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("splk_retrieve_location", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_location_byid(int _locationID)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("splk_retrieve_location_byID", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@LocationID", _locationID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_sublocation(int _orgID, int _siteID, int _locationID)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("splk_retrieve_sublocation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@LocationID", _locationID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_sublocation_byid(int _sublocationID)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("splk_retrieve_sublocation_byID", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SublocationID", _sublocationID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_helpcategory(string _crmHelpCategoryID)
        {
            DataTable dt = new DataTable();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("splk_retrieve_helpcategory", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CRMHelpCategoryID", _crmHelpCategoryID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public String saveComplaintAndInfo(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID, Int32? _LocationID, Int32? _SublocationID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime? _overdueDate,
            String _SavedBy, String _FullName
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);

                SqlCommand cmd = new SqlCommand("splk_save_complaintandinformation", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@LocationID", _LocationID);
                cmd.Parameters.AddWithValue("@SublocationID", _SublocationID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _overdueDate);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName); ;


                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public DataTable retrieve_pichead_bycase(string _caseNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SPPICHeadByCaseNumber_GET", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataSet retrieve_case(int _orgID, int _siteID, int _helpCategoryID, int _caseStatusID, string _search)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("splk_retrieve_case", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CaseStatusID", _caseStatusID);
            cmd.Parameters.AddWithValue("@Search", _search);
            cmd.Parameters.AddWithValue("@HelpCategoryID", _helpCategoryID);

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(ds);
            oAdapter.Dispose();

            return ds;
        }

        public DataTable retrieve_unit_owner_byunit(int _orgID, int _siteID, string _unitCode, string _unitNo)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_unitowner", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_resident_card(int _residentCardID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_resident_card", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ResidentCardID", _residentCardID);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_resident_design_card(string _designTypeCode)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_design_card", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DesignTypeCode", _designTypeCode);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable cek_unit_exist(string _unitCode, string _unitNo)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_check_resident_header", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public string save_resident_header(int _orgID, int _siteID, string _unitCode, string _unitNo, string _ownerPsCode, string _tenantPsCode, string _savedby)
        {

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_save_resident_header", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgId", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@UnitCode", _unitCode);
            cmd.Parameters.AddWithValue("@UnitNo", _unitNo);
            cmd.Parameters.AddWithValue("@OwnerPsCode", _ownerPsCode);
            cmd.Parameters.AddWithValue("@TenantPsCode", _tenantPsCode);
            cmd.Parameters.AddWithValue("@UserName", _savedby);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt.Rows[0]["Result"].ToString();
        }

        /*
         * remove crm
         */
        public string SaveToTMDDB(Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32? _LocationID, Int32? _SubLocationID, Int32 _CaseOriginID, bool _CustomerType, String _PSName, String _PSCode, String _UnitCode, String _UnitNo,
            String _NamaPelapor, String _TeleponPelapor, String _EmailPelapor, String _Description, DateTime? _OverdueDate, String _SavedBy)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Case", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);  //null
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@LocationID", _LocationID);
                cmd.Parameters.AddWithValue("@SubLocationID", _SubLocationID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public string save_email_request_log(string _CaseNumber ,int _CaseStatusID, string _EmailTo, string _EmailBody)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("splk_insert_email_request", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@EmailTo", _EmailTo);
                cmd.Parameters.AddWithValue("@EmailBody", _EmailBody);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public DataTable retrieve_help_duration(int _orgID, int _siteID, int _helpID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_HelpName_Duration", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@HelpID", _helpID);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_pic_head(string _caseNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_PICHead", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_numbering(int _orgID, int _siteID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_numbering", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public string save_satisfaction_access(string _SatisfactionAccessID, string _CaseNumber, string _SatisfactionUrl)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("splk_insert_satisfaction_access", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SatisfactionAccessID", _SatisfactionAccessID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@SatisfactionUrl", _SatisfactionUrl);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }


        #endregion complain and information in rs

        #region move from crm class

        public static string RandomString(int size)
        {
            string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random rand = new Random();
            char[] chars = new char[size];
            for (int i = 0; i < size; i++)
            {
                chars[i] = Alphabet[rand.Next(Alphabet.Length)];
            }
            return new string(chars);
        }

        public string GenerateCaseNumber(int siteID)
        {
            //DataTable dt = this.retrieve_numbering(1, siteID);

            //int numbering = 0;
            //if (dt.Rows.Count > 0)
            //{
            //    numbering = int.Parse(dt.Rows[0]["NewNumber"].ToString());
            //}

            //CAS-07275-F6Y7X5
            string randomString = RandomString(4);
            return (siteID.ToString("00") + DateTime.Now.ToString("yy") + randomString).ToUpper();

        }

        private Boolean IsWeekend(DateTime _date)
        {
            return _date.DayOfWeek == DayOfWeek.Saturday || _date.DayOfWeek == DayOfWeek.Sunday;
        }

        public DateTime GetOverdueDate(int _Duration)
        {
            DateTime StartDate = DateTime.Now;
            DateTime CurrentDate = StartDate;
            DateTime EndDate = DateTime.Now.AddDays(_Duration);

            for (int i = 1; i <= _Duration; i++)
            {
                if (IsWeekend(CurrentDate))
                {
                    EndDate = EndDate.AddDays(1);
                    _Duration++;
                }
                CurrentDate = CurrentDate.AddDays(1);
            }

            return EndDate;
        }

        public DataTable cek_case_number_exist(string _CaseNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_cek_case_number", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_case_detail(string _CaseNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_case_detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_email_config(string _SiteID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_email_config", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public Int32 save_ticket_attachment(String _CaseNumber, Int32 _CaseStatusID, String _FileName, string _FileType, byte[] _Attachment, string _SavedBy)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_save_ticket_attachment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
            cmd.Parameters.AddWithValue("@FileName", _FileName);
            cmd.Parameters.AddWithValue("@FileType", _FileType);
            cmd.Parameters.AddWithValue("@Attachment", _Attachment);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }


        public DataTable retrieve_attachment(string _CaseNumber, int? _Attachmentid)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_attachment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@AttachmentID", _Attachmentid);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public string delete_attachment(string _CaseNumber, int _AttachmentID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_delete_attachment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@AttachmentID", _AttachmentID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return returnparameter.Value.ToString();
        }

        public string delete_case_record(string _CaseNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_delete_case_record", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return returnparameter.Value.ToString();
        }

        public DataTable retrieve_personal_email(string _pscode)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_personal_email", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PSCode", _pscode);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }


        public DataTable retrieve_email_history(string _CaseNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SPCRMEmail_GetByCaseNo", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNo", _CaseNumber);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_email_history_byID(string _EmailID)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SPCRMEmail_GetByEmailID", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmailID", _EmailID);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }


        //move from crm class
        public InsertCaseResponse InsertCase(InsertCaseRequest _request)
        {
            InsertCaseResponse response = new InsertCaseResponse();
            response.CaseID = Guid.Empty;
            Guid CaseID = Guid.Empty;
            Guid tickethistoryid = Guid.Empty;

            if (string.IsNullOrEmpty(_request.CaseNumber))
            {
                try
                {
                    //get duration from helpname                
                    int helpDuration = -1;
                    DataTable dt = this.retrieve_help_duration(_request.OrgID, _request.SiteID, _request.HelpID);
                    if (dt.Rows.Count > 0)
                        helpDuration = int.Parse(dt.Rows[0]["Duration"].ToString());

                    //get overdue
                    DateTime? OverdueDate = null;
                    if (helpDuration >= 0)
                        OverdueDate = this.GetOverdueDate(helpDuration);

                    response.OverdueDate = OverdueDate;

                    string NewCaseNumber = this.GenerateCaseNumber(_request.SiteID);
                    DataTable dt_case = this.cek_case_number_exist(NewCaseNumber);
                    if (dt_case.Rows.Count > 0)
                    {
                        response.ExceptionMessage = "case number alrady exist, please try again !";
                    }
                    else
                    {
                        response.CaseNumber = NewCaseNumber;
                    }
                }
                catch (Exception e)
                {
                    response.ExceptionMessage = e.Message;
                }
            }
            else
            {
                response.CaseNumber = _request.CaseNumber;
            }
            return response;
        }

        public void Delete_Case_Record(string _CaseNumber)
        {
            this.delete_case_record(_CaseNumber);
        }

        #endregion


        #region operationcenter
        
        //July 2018 - by Fadel Muhammad

        public DataTable allcase(int _orgID, int _siteID, DateTime? StartDate, DateTime? EndDate, string DeptName)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Chart_AllCaseSummary", conn);
            //if (StartDate != null && EndDate != null && DeptName != null)
            //{
                cmd.Parameters.AddWithValue("@OrgID", _orgID);
                cmd.Parameters.AddWithValue("@SiteID", _siteID);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@DeptName", DeptName);
            //}
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable duecase(int _orgID, int _siteID, DateTime? StartDate, DateTime? EndDate, string DeptName)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Chart_DueSummary", conn);
            //if (StartDate != null && EndDate != null)
            //{
                cmd.Parameters.AddWithValue("@OrgID", _orgID);
                cmd.Parameters.AddWithValue("@SiteID", _siteID);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@DeptName", DeptName);
            //}
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable overduecase(int _orgID, int _siteID, DateTime? StartDate, DateTime? EndDate, string DeptName)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Chart_OverDueSummary", conn);
            //if (StartDate != null && EndDate != null)
            //{
                cmd.Parameters.AddWithValue("@OrgID", _orgID);
                cmd.Parameters.AddWithValue("@SiteID", _siteID);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@DeptName", DeptName);
            //}
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable closecase(int _orgID, int _siteID, DateTime? StartDate, DateTime? EndDate)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Chart_CaseClosePerDept", conn);
            if (StartDate != null && EndDate != null)
            {
                cmd.Parameters.AddWithValue("@OrgID", _orgID);
                cmd.Parameters.AddWithValue("@SiteID", _siteID);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
            }
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable closeSLA(int _orgID, int _siteID, DateTime? StartDate, DateTime? EndDate, string DeptName)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_CloseSLA_Monitoring", conn);
            //if (StartDate != null && EndDate != null)
            //{
                cmd.Parameters.AddWithValue("@OrgID", _orgID);
                cmd.Parameters.AddWithValue("@SiteID", _siteID);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@DeptName", DeptName);
            //}
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable DeptCloseSLA(int _orgID, int _siteID, DateTime? StartDate, DateTime? EndDate, string DeptName)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_DeptSLA_Monitoring", conn);
            //if (StartDate != null && EndDate != null)
            //{
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Parameters.AddWithValue("@DeptName", DeptName);
            //}
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();

            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }


        public DataSet retrieve_case_report(int _orgID, int _siteID, int _caseStatusID, string _search, DateTime? StartDate, DateTime? EndDate)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_RetrieveReport", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CaseStatusID", _caseStatusID);
            cmd.Parameters.AddWithValue("@Search", _search);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(ds);
            oAdapter.Dispose();

            return ds;
        }

        public DataTable deptmonitoring(int _orgID, int _siteID, DateTime? StartDate, DateTime? EndDate)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_CaseMonitoring_PerDept", conn);
                cmd.Parameters.AddWithValue("@OrgID", _orgID);
                cmd.Parameters.AddWithValue("SiteID", _siteID);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);

            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(dt);
            oAdapter.Dispose();

            return dt;
        }

        public DataTable retrieve_deptlist()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Dept", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();
            return dt;
        }

        public DataSet chartdetail(int OrgID, int SiteID, DateTime? StartDate, DateTime? EndDate, string DeptName, int CSID)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_Chart_Detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", OrgID);
            cmd.Parameters.AddWithValue("@SiteID", SiteID);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Parameters.AddWithValue("@DeptName", DeptName);
            cmd.Parameters.AddWithValue("@CSID", CSID);

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(ds);
            oAdapter.Dispose();

            return ds;
        }

        public DataSet case_tobe_validate(int _orgID, int _siteID, int _caseStatusID, string _search, DateTime? StartDate, DateTime? EndDate)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_RetrieveReport", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@SiteID", _siteID);
            cmd.Parameters.AddWithValue("@CaseStatusID", _caseStatusID);
            cmd.Parameters.AddWithValue("@Search", _search);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(ds);
            oAdapter.Dispose();

            return ds;
        }

        public Int32 save_notehistory(String _CaseNumber, String _FullName, String _Remarks, String _SavedBy, String _Attachment)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Note4Ticket", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@FullName", _FullName);
            cmd.Parameters.AddWithValue("@Remarks", _Remarks);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
            cmd.Parameters.AddWithValue("@Attachment", _Attachment);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)returnparameter.Value;

            return returnvalue;
        }

        public DataSet cdallcase(int OrgID, int SiteID, DateTime? StartDate, DateTime? EndDate, string DeptName, string CSID)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_CD_AllCase", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", OrgID);
            cmd.Parameters.AddWithValue("@SiteID", SiteID);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Parameters.AddWithValue("@DeptName", DeptName);
            cmd.Parameters.AddWithValue("@CSID", CSID);

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(ds);
            oAdapter.Dispose();

            return ds;
        }

        public DataSet cdduecase(int OrgID, int SiteID, DateTime? StartDate, DateTime? EndDate, string DeptName, string CSID)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_CD_DueCase", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", OrgID);
            cmd.Parameters.AddWithValue("@SiteID", SiteID);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Parameters.AddWithValue("@DeptName", DeptName);
            cmd.Parameters.AddWithValue("@CSID", CSID);

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(ds);
            oAdapter.Dispose();

            return ds;
        }

        public DataSet cdoverduecase(int OrgID, int SiteID, DateTime? StartDate, DateTime? EndDate, string DeptName, string CSID)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_CD_OverDueCase", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", OrgID);
            cmd.Parameters.AddWithValue("@SiteID", SiteID);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Parameters.AddWithValue("@DeptName", DeptName);
            cmd.Parameters.AddWithValue("@CSID", CSID);

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(ds);
            oAdapter.Dispose();

            return ds;
        }

        public DataSet cdcaseclose(int OrgID, int SiteID, DateTime? StartDate, DateTime? EndDate, string DeptCode)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(connectionstring);

            SqlCommand cmd = new SqlCommand("SP_CD_CaseClosePerDept", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", OrgID);
            cmd.Parameters.AddWithValue("@SiteID", SiteID);
            cmd.Parameters.AddWithValue("@StartDate", StartDate);
            cmd.Parameters.AddWithValue("@EndDate", EndDate);
            cmd.Parameters.AddWithValue("@DeptCode", DeptCode);

            conn.Open();
            SqlDataAdapter oAdapter = new SqlDataAdapter();
            oAdapter.SelectCommand = cmd;

            oAdapter.Fill(ds);
            oAdapter.Dispose();

            return ds;
        }

        #endregion operationcenter
    }
}
