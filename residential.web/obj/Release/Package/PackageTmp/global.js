﻿function openWindow(url, w, h) {
    var ileft = (screen.width - w) / 2;
    var itop = (screen.height - h) / 2;
    var pos = 'left=' + ileft + ',top=' + itop + ',width=' + w + ',height=' + h;
    var x = window.open(url, '', pos + ',toolbar=0,status=0,location=0,menubar=0,directories=0,resizable=1,scrollbars=1');
    x.focus();
}

function openWindowModal(url, w, h) {
    var pos = "center:yes;resizable:no;dialogHeight:" + h + "px;dialogWidth=" + w + "px";
    var x = window.showModalDialog(url, "", pos);
}


function openLookupWindow(url, w, h, objectname) {
    var pos = "center:yes;resizable:no;dialogHeight:" + h + "px;dialogWidth=" + w + "px";
    var myobject = document.getElementById(objectname);
    var argobject = new Object();
    argobject.param = myobject.value;
    argobject.pscode = '';
    argobject.psname = '';
    argobject.customertype = '';
    var x = window.showModalDialog(url, argobject, pos);

    document.getElementById('ContentPlaceHolder1_entryheader_txt_pscode').value = argobject.pscode;
    document.getElementById('ContentPlaceHolder1_entryheader_txt_customername').value = argobject.psname;
    document.getElementById('ContentPlaceHolder1_entryheader_txt_customertype').value = argobject.customertype;
}

function openLookupWindowRefNumber(url, w, h, objectname) {
    var pos = "center:yes;resizable:no;dialogHeight:" + h + "px;dialogWidth=" + w + "px";
    var myobject = document.getElementById(objectname);
    var argobject = new Object();
    argobject.param = myobject.value;
    argobject.pscode = '';
    argobject.psname = '';
    var x = window.showModalDialog(url, argobject, pos);

    document.getElementById('ContentPlaceHolder1_txtCaseNumber').value = argobject.refnumber;
}

function returnlookup(x) {
    var opener = null;

    if (window.dialogArguments) // Internet Explorer supports window.dialogArguments
    {
        opener = window.dialogArguments;
    }
//    else // Firefox, Safari, Google Chrome and Opera supports window.opener
//    {
//        if (window.opener) {
//            opener = window.opener;
//        }
    //    }

    var pscode = document.getElementById('lbl_pscode').innerText;
    var psname = document.getElementById('lbl_psname').innerText;
    var customertype = document.getElementById('lbl_customertype').innerText;    
        opener.pscode = pscode;
        opener.psname = psname;
        opener.customertype = customertype;
    x.returnValue = opener;
    x.close();
}

function returnlookuprefnumber(x) {
    var opener = null;

    if (window.dialogArguments) // Internet Explorer supports window.dialogArguments
    {
        opener = window.dialogArguments;
    }
    //    else // Firefox, Safari, Google Chrome and Opera supports window.opener
    //    {
    //        if (window.opener) {
    //            opener = window.opener;
    //        }
    //    }


    var refnumber = document.getElementById('lbl_refnumber').innerText;

    opener.refnumber = refnumber
    x.returnValue = opener;
    x.close();
}

function returnlookupuserid(x) {
    var opener = null;

    if (window.dialogArguments) // Internet Explorer supports window.dialogArguments
    {
        opener = window.dialogArguments;
    }    

    opener.userid = document.getElementById('userid').value;    

    x.returnValue = opener;
    x.close();
}

function closeWindow(x) {
    x.close();
}

function numericOnly() {
    if (!(event.keyCode >= 48 && event.keyCode <= 57))
    { event.returnValue = false; }
}

function alphanumericOnly() {
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90) || (event.keyCode >= 97 && event.keyCode <= 122)) {
        if (event.keyCode >= 97 && event.keyCode <= 122)
        { event.keyCode = event.keyCode - 32; }
        else
        { event.keyCode = event.keyCode; }
    } else {
        event.returnValue = false;
    }
}

function all_alphanumeric(str) {
    var newstring = str.value;
    newstring = newstring.replace(/[^a-zA-Z0-9]+/g, "");
    str.value = newstring;
}

function findspp(e, buttonid) {
    var evt = e ? e : window.event;  
    var bt = document.getElementById(buttonid);
    if (bt) {  
        if (evt.keyCode == 13){   
            bt.click();   
            return false;     
        }     
    }
}
function confirmDelete() {
    if (GetCheckedCheckBox()) {
        return confirm("Are you sure want to delete?");
    } else {
        alert("Please select one or more items to delete!");
        return false;
    }
}

function GetCheckedCheckBox() {
    var inputList = document.getElementsByTagName("input");

    for (var i = 0; i < inputList.length; i++) {
        if (inputList[i].type == "checkbox" && inputList[i].checked && inputList[i].name != "ctl00$ContentPlaceHolder1$gvw_data$ctl01$chk_selectall") {
            return true;
        }
    }  
    return false;
}

function ConvertToCurrency(val) {
    var price = val.toString();
    var div = Math.ceil(price.length / 3);
    var mod = price.length % 3;
    var res = "Rp";

    if (div > 1) {
        if (mod == 0) mod = 3;        
        for (var i = 0; i < div - 1; i++) {
            if (i == 0) res = res + price.substr(0, mod);
            res = res + "." + price.substr(i * 3 + mod, 3);
        }
    }
    else
        res = res + price;
    return res;
}

function UpdateTotalPrice() {
    var quantity = document.getElementById("txt_jumlah").value;
    if (quantity == 0) quantity = 1;
    var pricecurrency = document.getElementById("lbl_price").innerHTML;
    var price = pricecurrency.replace(/[^0-9\,]+/g, "");
    var discount = document.getElementById("txt_discount").value;
    if (discount == '') discount = 0;

    var subtotal = parseInt(quantity) * price;
    var total = subtotal - parseInt(discount);

    //document.getElementById("txt_jumlah").value = quantity;
    document.getElementById("lbl_subtotal").innerHTML = ConvertToCurrency(subtotal);
    document.getElementById("lbl_discount").innerHTML = ConvertToCurrency(discount);
    document.getElementById("lbl_total").innerHTML = ConvertToCurrency(total);
}

function MessageCharacterLeft() {
    var messages = document.getElementById("ContentPlaceHolder1_txt_messageContent");
    var content = parseInt(messages.value.length);    
    if (content > 160)
        messages.value = messages.value.substring(0, 160);
    var charLeft = 160 - parseInt(messages.value.length);
    document.getElementById("ContentPlaceHolder1_lbl_charLength").innerHTML = charLeft;    
}

function openLookupWindowWTP(url, w, h, objectname) {
    var pos = "center:yes;resizable:no;dialogHeight:" + h + "px;dialogWidth=" + w + "px";
    var myobject = document.getElementById(objectname);
    var argobject = new Object();
    argobject.param = myobject.value;
    argobject.pscode = '';
    argobject.psname = '';
    argobject.customertype = '';
    var x = window.showModalDialog(url, argobject, pos);

    document.getElementById('ContentPlaceHolder1_txt_pscode').value = argobject.pscode;
    document.getElementById('ContentPlaceHolder1_txt_customername').value = argobject.psname;
    document.getElementById('ContentPlaceHolder1_txt_customertype').value = argobject.customertype;
}

function buttonSaving(ctrl) {
    ctrl.disabled = true;
    ctrl.value = "SAVING...";
//    __doPostBack(ctrl.id, '');
}

function buttonSaving2(ctrl) {
    ctrl.disabled = true;
    ctrl.value = "SAVING...";
    __doPostBack(ctrl.id, '');
}

function buttonUpdating(ctrl) {
    ctrl.disabled = true;
    ctrl.value = "UPDATING...";
//    __doPostBack(ctrl.id, '');
}

function buttonUpdating2(ctrl) {
    ctrl.disabled = true;
    ctrl.value = "UPDATING...";
    __doPostBack(ctrl.id, '');
}

function buttonAssigning(ctrl) {
    ctrl.disabled = true;
    ctrl.value = "ASSIGNING..";
//    __doPostBack(ctrl.id, '');
}

function setHeartbeat() {
    setTimeout("heartbeat()", 300000); // every 5 min
}

function heartbeat() {
    $.get(
        "KeepSessionAlive.ashx",
        null,
        function (data) {            
            setHeartbeat();
        },
        "json"
    );
}

function hide_alert() {
    setTimeout(function () { $('#ContentPlaceHolder1_alert_success, #ContentPlaceHolder1_alert_danger, #alert_success, #alert_danger').fadeOut("slow") }, 10000);
}