﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrymagazine.aspx.cs"
    Inherits="residential.web.forms.entrymagazine" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%--<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>--%>
<%@ Register Src="~/forms/emailviewer.ascx" TagName="EmailViewer" TagPrefix="aspuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form Iklan Majalah</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <div id="updaterequest" runat="server">
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
<%--        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>--%>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_email">Email Viewer</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade in active">
            <aspuc:UpdateCase ID="updatecase" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
<%--        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>--%>
        <div id="tab_email" class="tab-pane fade">
            <aspuc:EmailViewer ID="emailviewer" runat="server" />
        </div>
    </div>
    </div>    
    <hr />    
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Magazine Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">                        
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Magazine Type</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboType" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="cboType_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Edition</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboEdition" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Quantity x Price</label>
                            <div class="col-sm-2">    
                                <asp:TextBox ID="txt_jumlah" runat="server" CssClass="form-control" Type="Number">1</asp:TextBox>                            
                            </div>                            
                            <div class="col-sm-2">
                                <p class="form-control-static">X&nbsp;<asp:Label ID="lbPriceDetail" runat="server">0</asp:Label></p>
                            </div>                                                        
                        </div>                  
                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <asp:Button ID="btn_add_detail" runat="server" Text="Add Detail" CssClass="btn btn-primary"
                                OnClick="btn_add_detail_Click" UseSubmitBehavior="false" />
                            <asp:Button ID="btnDelete" runat="server" Text="Delete Detail" CssClass="btn btn-default"
                                OnClick="btnDelete_Click" UseSubmitBehavior="false" OnClientClick="return confirmDelete();" />
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Magazine Detail List</label>
                            <div class="col-sm-10">
                                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                    AllowPaging="True" ShowHeaderWhenEmpty="true"
                                    DataKeyNames="MagazineDetailID" OnRowDataBound="gvw_data_RowDataBound">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                                                    AutoPostBack="true" />
                                            </HeaderTemplate>
                                            <HeaderStyle CssClass="gridview_header" Width="30px" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk_select" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NO">
                                            <HeaderStyle CssClass="gridview_header" Width="30px" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_no" runat="server" Text="Label"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="TYPE" DataField="MagazineType" SortExpression="MagazineType">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="EDITION" DataField="MagazineEdition" SortExpression="MagazineEdition">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="QTY" DataField="Qty" SortExpression="Qty">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_right" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="PRICE" DataField="Price" SortExpression="Price">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_right" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="AMOUNT" DataField="Amount" SortExpression="Amount">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Adjustment</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_discount" Text="0" AutoPostBack="True" OnTextChanged="txt_discount_TextChanged"
                                    runat="server" CssClass="form-control" Type="Number">0</asp:TextBox>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>            
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" runat="server"></asp:Label></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_adjustment" runat="server"></asp:Label></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" runat="server"></asp:Label></p>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
