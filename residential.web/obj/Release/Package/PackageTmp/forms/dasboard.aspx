﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="dasboard.aspx.cs" Inherits="residential.web.forms.dasboard" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/Chart.min.js"></script>
    <script src="../scripts/dashboard/pikaday.js"></script>
    <link href="../scripts/dashboard/pikaday.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
                <h1>Dashboard <asp:Label runat="server" ID="lblTitle" /></h1>
            </div>
    <div class="container">
        <div class="row">
        <div class="col-md-2">
            <asp:Label ID="Label3" runat="server" Text="Created Date" Font-Bold="True" Font-Size="Medium"></asp:Label>
        </div>
    </div><br />


        <div class="row form-inline">
            <div class="form-group">
                <div class="col-md-2">From</div>
                <div class="col-md-3">
                    <asp:TextBox ID="TxtStartDate" runat="server" ClientIDMode="Static" CssClass="form-control" AutoComplete="off"></asp:TextBox>
                    <%--<asp:CalendarExtender ID="TxtStartDate_CalendarExtender" runat="server" TargetControlID="TxtStartDate" PopupPosition="TopLeft" Format="dd MMMM yyyy"></asp:CalendarExtender>--%>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-1">To</div>
                <div class="col-md-3">
                    <asp:TextBox ID="TxtEndDate" runat="server" ClientIDMode="Static" CssClass="form-control" AutoComplete="off"></asp:TextBox>
                    <%--<asp:CalendarExtender ID="TxtEndDate_CalendarExtender" runat="server" TargetControlID="TxtEndDate" PopupPosition="TopLeft" Format="dd MMMM yyyy"></asp:CalendarExtender>--%>
                </div>
            </div>
            <asp:Button ID="picker" runat="server" Text="Get Selected Date" CssClass="btn btn-primary" OnClick="picker_Click" />
        </div>
    </div>

        <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('TxtStartDate'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000, 2020],
                numberOfMonths: 1,
            });
    </script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('TxtEndDate'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000, 2020],
                numberOfMonths: 1,
            });
    </script>

    <br />
    <br />

    <div class="row">
        <div class="col-md-8">
            <label class="col-sm-2 control-label">Department List</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="cbo_deptlist" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
        </div>
     </div>
        <br /><br />

    <div class="row">
        <div class="col-md-5">
            <asp:Label ID="Label2" runat="server" Text="Case Summary" Font-Bold="False" Font-Size="X-Large"></asp:Label><br /><br />
            <asp:GridView ID="gvw_allcase" runat="server" AutoGenerateColumns="False" DataKeyNames="CaseStatusName" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" Font-Size="Medium" GridLines="Horizontal" Height="400px" Width="300px">
                <Columns>
                    <asp:BoundField DataField="CaseStatusName" HeaderText="Case Status Name" ReadOnly="True"
                        SortExpression="CaseStatusName" />
                    <asp:BoundField DataField="CSID" HeaderText="Qty"
                        SortExpression="CSID" />
                </Columns>
                 <EmptyDataTemplate>
                    <asp:Label ID="Label3" runat="server" Text="NO DATA" Font-Bold="true" Font-Size="XX-Large"></asp:Label>
                </EmptyDataTemplate>
                <FooterStyle BackColor="White" ForeColor="#333333" />
                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Center" VerticalAlign="Middle" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#487575" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#275353" />
            </asp:GridView>
            <br /><br />
          <%--  <h5>All Case Detail (Manual)</h5>
            <div class="row">
                <div class="col-md-9">
                    <asp:DropDownList ID="cbo_status" runat="server" CssClass="form-control"></asp:DropDownList><br />
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <asp:Button ID="Button1" runat="server" Text="View Detail" OnClick="Button1_Click" CssClass="btn btn-info" />
                </div>  
                <div class="col-md-4">
                    <asp:Button target="_blank" ID="Button2" runat="server" Text="View in New Tab" OnClick="Button2_Click" CssClass="btn btn-info" Visible="false" />
                </div>
            </div>--%>
            <h5>*Fill CREATED DATE FROM and TO First for ON CLICK Chart!</h5>
        </div>

        <div class="col-md-6">
            <blockquote>
                <h2>All Case</h2>
            </blockquote>
            <canvas id="AllCaseChart" width="400" height="400"></canvas>
        </div>
    </div>
    <br />
    <br />

    <div class="row">
        <div class="col-md-4">
            <blockquote>
                <h2>Due Case</h2>
            </blockquote>
            <canvas id="DueCaseChart" width="425" height="425"></canvas>
        </div>
        <div class="col-md-4">
            <blockquote>
                <h2>Overdue Case</h2>
            </blockquote>
            <canvas id="OverDueCaseChart" width="400" height="400"></canvas>
        </div>
        <div class="col-md-4">
            <blockquote>
                <h2>Complete Case</h2>
            </blockquote>
            <canvas id="CompleteCaseChart" width="400" height="400"></canvas>
        </div>
    </div>
    <br /><br />

    <asp:Label ID="chart1data" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart1label" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart2data" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart2label" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart3data" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart3label" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart4data" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart4label" runat="server" Visible="false"></asp:Label>
    
        <% 
        string EmptyData1;
        if (chart1data.Text.ToString() == "")
        {
            EmptyData1 = "[0.000001]";
        } else {
            EmptyData1 = "["+chart1data.Text+"]";
        } %>
    <%
        string EmptyLabel1;
        if (chart1label.Text.ToString() == "")
        {
            EmptyLabel1 = "['NO DATA']";
        }
        else {
            EmptyLabel1 = "[" + chart1label.Text+ "]";
        }
         %>

    <% 
        string EmptyData2;
        if (chart2data.Text.ToString() == "")
        {
            EmptyData2 = "[0.000001]";
        } else {
            EmptyData2 = "[" + chart2data.Text + "]";
        } %>
        <%
        string EmptyLabel2;
        if (chart2label.Text.ToString() == "")
        {
            EmptyLabel2 = "['NO DATA']";
        }
        else {
            EmptyLabel2 = "[" + chart2label.Text+ "]";
        }
         %>

    <%
        string EmptyData3;
        if (chart3data.Text.ToString() == "")
        {
            EmptyData3 = "[0.000001]";
        } else {
            EmptyData3 = "[" + chart3data.Text + "]";
        } %>
            <%
        string EmptyLabel3;
        if (chart3label.Text.ToString() == "")
        {
            EmptyLabel3 = "['NO DATA']";
        }
        else {
            EmptyLabel3 = "[" + chart3label.Text+ "]";
        }
         %>

            <% 
        string EmptyData4;
        if (chart4data.Text.ToString() == "")
        {
            EmptyData4 = "[0.000001]";
        } else {
            EmptyData4 = "[" + chart4data.Text + "]";
        } %>
                <%
        string EmptyLabel4;
        if (chart4label.Text.ToString() == "")
        {
            EmptyLabel4 = "['NO DATA']";
        }
        else {
            EmptyLabel4 = "[" + chart4label.Text+ "]";
        }
         %>

    <script>
        function ForChartParameterDefault(option) {
            return {
                type: 'doughnut',
                data: {
                    labels: option.labels,
                    datasets: [{
                        label: '# of Votes',
                        data: option.data, 
                        backgroundColor: [
                            'rgba(255,0,255, 0.3)', //fuchsia
                            'rgba(0,128,128, 0.3)', //teal
                            'rgba(0,255,0, 0.3)',//lime
                            'rgba(255,0,0, 0.3)',//red
                            'rgba(0,0,255, 0.3)',//lue
                            'rgba(0,128,0, 0.3)',//green
                            'rgba(0,255,255, 0.3)', //aqua
                            'rgba(255,255,0, 0.3)', //yellow
                            'rgba(128,0,128, 0.3)', //purple
                            'rgba(128,0,0, 0.3)',//maroon
                            'rgba(128,128,0, 0.3)', //olive
                            'rgba(128,128,128, 0.3)', //gray
                            'rgba(0,0,0, 0.3)',  //black
                            'rgba(0,0,128, 0.3)',//navy
                            'rgba(192,192,192, 0.3)', //silver
                            'rgba(255,255,255, 0.3)' //white
                        ],
                        borderColor: [
                            'rgba(255,0,255, 1)', //fuchsia
                            'rgba(0,128,128, 1)', //teal
                            'rgba(0,255,0, 1)',//lime
                            'rgba(255,0,0, 1)',//red
                            'rgba(0,0,255, 1)',//lue
                            'rgba(0,128,0, 1)',//green
                            'rgba(0,255,255, 1)', //aqua
                            'rgba(255,255,0, 1)', //yellow
                            'rgba(128,0,128, 1)', //purple
                            'rgba(128,0,0, 1)',//maroon
                            'rgba(128,128,0, 1)', //olive
                            'rgba(128,128,128, 1)', //gray
                            'rgba(0,0,0, 1)',  //black
                            'rgba(0,0,128, 1)',//navy
                            'rgba(192,192,192, 1)', //silver
                            'rgba(255,255,255, 1)' //white
                        ],
                        borderWidth: 1,
                        cursor: 'pointer',
                    }]
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }

        function ChartOnClickFunction (chart) {
            return function(e){
                var activePoint = chart.getElementsAtEvent(e)[0];

                if (activePoint) {
                    var chartData = activePoint['_chart'].config.data;
                    var index = activePoint['_index'];

                    console.log(chartData);
                    console.log(index);

                    var label = chartData.labels[index];
                    var value = chartData.datasets[0].data[index];
                    var stdate = document.getElementById('<%=TxtStartDate.ClientID%>').value;
                    var eddate = document.getElementById('<%=TxtEndDate.ClientID%>').value;
                    var dept = document.getElementById('<%=cbo_deptlist.ClientID%>').value;

                    var url = "ChartDetail.aspx?StartDate=" + stdate + "&EndDate=" + eddate + "&DeptName=" + dept + "&CSID=" + label;
                    window.open(url, '_blank');
                    //alert("You Selected ==> " + " [ "+ label +" ] "+" : " + value);
                }
            } 
        }
        //initCharts
        function SelectedChart() {
            var chartDeclarations = [
                    { id: 'AllCaseChart',       data: <%=EmptyData1%>, labels: <%=EmptyLabel1%> }//,
                    <%--{ id: 'DueCaseChart',       data: <%=EmptyData2%>, labels: <%=EmptyLabel2%> },
                    { id: 'OverDueCaseChart',   data: <%=EmptyData3%>, labels: <%=EmptyLabel3%> },
                    { id: 'CompleteCaseChart',  data: <%=EmptyData4%>, labels: <%=EmptyLabel4%> }--%>
                ];

                for(var i in chartDeclarations){
                    var chartDeclaration = chartDeclarations[i];
                    var chartElem = document.getElementById(chartDeclaration.id);
                    var chartParameters = ForChartParameterDefault(chartDeclaration);
                    var chart = new Chart(chartElem, chartParameters);
                    chartElem.onclick = ChartOnClickFunction(chart);
                }
            }
            SelectedChart();
    </script>

    <script>
        function ForChartParameterDefault2(option) {
            return {
                type: 'doughnut',
                data: {
                    labels: option.labels,
                    datasets: [{
                        label: '# of Votes',
                        data: option.data, 
                        backgroundColor: [
                            'rgba(255,0,255, 0.3)', //fuchsia
                            'rgba(0,128,128, 0.3)', //teal
                            'rgba(0,255,0, 0.3)',//lime
                            'rgba(255,0,0, 0.3)',//red
                            'rgba(0,0,255, 0.3)',//lue
                            'rgba(0,128,0, 0.3)',//green
                            'rgba(0,255,255, 0.3)', //aqua
                            'rgba(255,255,0, 0.3)', //yellow
                            'rgba(128,0,128, 0.3)', //purple
                            'rgba(128,0,0, 0.3)',//maroon
                            'rgba(128,128,0, 0.3)', //olive
                            'rgba(128,128,128, 0.3)', //gray
                            'rgba(0,0,0, 0.3)',  //black
                            'rgba(0,0,128, 0.3)',//navy
                            'rgba(192,192,192, 0.3)', //silver
                            'rgba(255,255,255, 0.3)' //white
                        ],
                        borderColor: [
                            'rgba(255,0,255, 1)', //fuchsia
                            'rgba(0,128,128, 1)', //teal
                            'rgba(0,255,0, 1)',//lime
                            'rgba(255,0,0, 1)',//red
                            'rgba(0,0,255, 1)',//lue
                            'rgba(0,128,0, 1)',//green
                            'rgba(0,255,255, 1)', //aqua
                            'rgba(255,255,0, 1)', //yellow
                            'rgba(128,0,128, 1)', //purple
                            'rgba(128,0,0, 1)',//maroon
                            'rgba(128,128,0, 1)', //olive
                            'rgba(128,128,128, 1)', //gray
                            'rgba(0,0,0, 1)',  //black
                            'rgba(0,0,128, 1)',//navy
                            'rgba(192,192,192, 1)', //silver
                            'rgba(255,255,255, 1)' //white
                        ],
                        borderWidth: 1,
                        cursor: 'pointer',
                    }]
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }

        function ChartOnClickFunction2 (chart) {
            return function(e){
                var activePoint = chart.getElementsAtEvent(e)[0];

                if (activePoint) {
                    var chartData = activePoint['_chart'].config.data;
                    var index = activePoint['_index'];

                    console.log(chartData);
                    console.log(index);

                    var label = chartData.labels[index];
                    var value = chartData.datasets[0].data[index];
                    var stdate = document.getElementById('<%=TxtStartDate.ClientID%>').value;
                    var eddate = document.getElementById('<%=TxtEndDate.ClientID%>').value;
                    var dept = document.getElementById('<%=cbo_deptlist.ClientID%>').value;

                    var url = "CDDueCase.aspx?StartDate=" + stdate + "&EndDate=" + eddate + "&DeptName=" + dept + "&CSID=" + label;
                    window.open(url, '_blank');
                    //alert("You Selected ==> " + " [ "+ label +" ] "+" : " + value);
                }
            } 
        }
        //initCharts
        function SelectedChart() {
            var chartDeclarations = [
                    { id: 'DueCaseChart',       data: <%=EmptyData2%>, labels: <%=EmptyLabel2%> },
                ];

                for(var i in chartDeclarations){
                    var chartDeclaration = chartDeclarations[i];
                    var chartElem = document.getElementById(chartDeclaration.id);
                    var chartParameters = ForChartParameterDefault2(chartDeclaration);
                    var chart = new Chart(chartElem, chartParameters);
                    chartElem.onclick = ChartOnClickFunction2(chart);
                }
            }
            SelectedChart();
    </script>

    <script>
        function ForChartParameterDefault3(option) {
            return {
                type: 'doughnut',
                data: {
                    labels: option.labels,
                    datasets: [{
                        label: '# of Votes',
                        data: option.data, 
                        backgroundColor: [
                            'rgba(255,0,255, 0.3)', //fuchsia
                            'rgba(0,128,128, 0.3)', //teal
                            'rgba(0,255,0, 0.3)',//lime
                            'rgba(255,0,0, 0.3)',//red
                            'rgba(0,0,255, 0.3)',//lue
                            'rgba(0,128,0, 0.3)',//green
                            'rgba(0,255,255, 0.3)', //aqua
                            'rgba(255,255,0, 0.3)', //yellow
                            'rgba(128,0,128, 0.3)', //purple
                            'rgba(128,0,0, 0.3)',//maroon
                            'rgba(128,128,0, 0.3)', //olive
                            'rgba(128,128,128, 0.3)', //gray
                            'rgba(0,0,0, 0.3)',  //black
                            'rgba(0,0,128, 0.3)',//navy
                            'rgba(192,192,192, 0.3)', //silver
                            'rgba(255,255,255, 0.3)' //white
                        ],
                        borderColor: [
                            'rgba(255,0,255, 1)', //fuchsia
                            'rgba(0,128,128, 1)', //teal
                            'rgba(0,255,0, 1)',//lime
                            'rgba(255,0,0, 1)',//red
                            'rgba(0,0,255, 1)',//lue
                            'rgba(0,128,0, 1)',//green
                            'rgba(0,255,255, 1)', //aqua
                            'rgba(255,255,0, 1)', //yellow
                            'rgba(128,0,128, 1)', //purple
                            'rgba(128,0,0, 1)',//maroon
                            'rgba(128,128,0, 1)', //olive
                            'rgba(128,128,128, 1)', //gray
                            'rgba(0,0,0, 1)',  //black
                            'rgba(0,0,128, 1)',//navy
                            'rgba(192,192,192, 1)', //silver
                            'rgba(255,255,255, 1)' //white
                        ],
                        borderWidth: 1,
                        cursor: 'pointer',
                    }]
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }

        function ChartOnClickFunction3 (chart) {
            return function(e){
                var activePoint = chart.getElementsAtEvent(e)[0];

                if (activePoint) {
                    var chartData = activePoint['_chart'].config.data;
                    var index = activePoint['_index'];

                    console.log(chartData);
                    console.log(index);

                    var label = chartData.labels[index];
                    var value = chartData.datasets[0].data[index];
                    var stdate = document.getElementById('<%=TxtStartDate.ClientID%>').value;
                    var eddate = document.getElementById('<%=TxtEndDate.ClientID%>').value;
                    var dept = document.getElementById('<%=cbo_deptlist.ClientID%>').value;

                    var url = "CDOverDueCase.aspx?StartDate=" + stdate + "&EndDate=" + eddate + "&DeptName=" + dept + "&CSID=" + label;
                    window.open(url, '_blank');
                    //alert("You Selected ==> " + " [ "+ label +" ] "+" : " + value);
                }
            } 
        }
        //initCharts
        function SelectedChart() {
            var chartDeclarations = [
                    { id: 'OverDueCaseChart',   data: <%=EmptyData3%>, labels: <%=EmptyLabel3%> }
                ];

                for(var i in chartDeclarations){
                    var chartDeclaration = chartDeclarations[i];
                    var chartElem = document.getElementById(chartDeclaration.id);
                    var chartParameters = ForChartParameterDefault3(chartDeclaration);
                    var chart = new Chart(chartElem, chartParameters);
                    chartElem.onclick = ChartOnClickFunction3(chart);
                }
            }
            SelectedChart();
    </script>

    <script>
        function ForChartParameterDefault1(option) {
            return {
                type: 'doughnut',
                data: {
                    labels: option.labels,
                    datasets: [{
                        label: '# of Votes',
                        data: option.data, 
                        backgroundColor: [
                            'rgba(255,0,255, 0.3)', //fuchsia
                            'rgba(0,128,128, 0.3)', //teal
                            'rgba(0,255,0, 0.3)',//lime
                            'rgba(255,0,0, 0.3)',//red
                            'rgba(0,0,255, 0.3)',//lue
                            'rgba(0,128,0, 0.3)',//green
                            'rgba(0,255,255, 0.3)', //aqua
                            'rgba(255,255,0, 0.3)', //yellow
                            'rgba(128,0,128, 0.3)', //purple
                            'rgba(128,0,0, 0.3)',//maroon
                            'rgba(128,128,0, 0.3)', //olive
                            'rgba(128,128,128, 0.3)', //gray
                            'rgba(0,0,0, 0.3)',  //black
                            'rgba(0,0,128, 0.3)',//navy
                            'rgba(192,192,192, 0.3)', //silver
                            'rgba(255,255,255, 0.3)' //white
                        ],
                        borderColor: [
                            'rgba(255,0,255, 1)', //fuchsia
                            'rgba(0,128,128, 1)', //teal
                            'rgba(0,255,0, 1)',//lime
                            'rgba(255,0,0, 1)',//red
                            'rgba(0,0,255, 1)',//lue
                            'rgba(0,128,0, 1)',//green
                            'rgba(0,255,255, 1)', //aqua
                            'rgba(255,255,0, 1)', //yellow
                            'rgba(128,0,128, 1)', //purple
                            'rgba(128,0,0, 1)',//maroon
                            'rgba(128,128,0, 1)', //olive
                            'rgba(128,128,128, 1)', //gray
                            'rgba(0,0,0, 1)',  //black
                            'rgba(0,0,128, 1)',//navy
                            'rgba(192,192,192, 1)', //silver
                            'rgba(255,255,255, 1)' //white
                        ],
                        borderWidth: 1,
                        cursor: 'pointer',
                    }]
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        }

        function ChartOnClickFunction1 (chart) {
            return function(e){
                var activePoint = chart.getElementsAtEvent(e)[0];

                if (activePoint) {
                    var chartData = activePoint['_chart'].config.data;
                    var index = activePoint['_index'];

                    console.log(chartData);
                    console.log(index);

                    var label = chartData.labels[index];
                    var value = chartData.datasets[0].data[index];
                    var stdate = document.getElementById('<%=TxtStartDate.ClientID%>').value;
                    var eddate = document.getElementById('<%=TxtEndDate.ClientID%>').value;

                    var url = "CDCompleteCase.aspx?StartDate=" + stdate + "&EndDate=" + eddate + "&DeptCode=" + label;
                    window.open(url, '_blank');
                    //alert("You Selected ==> " + " [ "+ label +" ] "+" : " + value);
                }
            } 
        }
        //initCharts
        function SelectedChart() {
            var chartDeclarations = [
                    { id: 'CompleteCaseChart',  data: <%=EmptyData4%>, labels: <%=EmptyLabel4%> }
                ];

                for(var i in chartDeclarations){
                    var chartDeclaration = chartDeclarations[i];
                    var chartElem = document.getElementById(chartDeclaration.id);
                    var chartParameters = ForChartParameterDefault1(chartDeclaration);
                    var chart = new Chart(chartElem, chartParameters);
                    chartElem.onclick = ChartOnClickFunction1(chart);
                }
            }
            SelectedChart();
    </script>

    <%-- <div class="row">
          <div class="col-md-12">
            <asp:Label ID="Label4" runat="server" Text="Chart Detail" Font-Bold="False" Font-Size="X-Large"></asp:Label><br /><br />
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover" AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound" 
                ShowHeaderWhenEmpty="True"
                DataKeyNames="CaseNumber"
                OnPageIndexChanging="gvw_data_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="UNIT CODE" DataField="Unit" SortExpression="Unit" />
                    <asp:BoundField HeaderText="CATEGORY" DataField="HelpCategory"></asp:BoundField>
                    <asp:BoundField HeaderText="DEPT NAME" DataField="DeptName" ></asp:BoundField>
                    <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="CREATED" DataField="CreatedDate" DataFormatString="{0:d MMMM yyyy}" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="SLA(DAYS)" DataField="SLA" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="OVERDUE" DataField="OverdueDate" DataFormatString="{0:d MMMM yyyy}" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="DAYS" DataField="Days" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="CREATED BY" DataField="CreatedBy" />
                    <%--<asp:BoundField HeaderText="STATUS" DataField="CaseStatusID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden"/>--%>
                <%--</Columns>
                <EmptyDataTemplate>
                    No Data
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
     </div>--%>

</asp:Content>
