﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true"
    CodeBehind="listdepartment.aspx.cs" Inherits="residential.web.forms.listdepartment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="buttoncontainer">
        <asp:Button CssClass="button" ID="btn_add" runat="server" Text="ADD" Width="100px"
            OnClick="btn_add_Click" Visible="false" />
        <asp:Button CssClass="button" ID="btn_delete" runat="server" Text="DELETE" Width="100px"
            OnClientClick="return confirmDelete();" OnClick="btn_delete_Click" Visible="false" />
    </div>
    <div class="alert alert-success" id="alert_success" runat="server" visible="false">
        <asp:Label runat="server" ID="alert_success_text"></asp:Label></div>
    <div class="alert alert-danger" id="alert_danger" runat="server" visible="false">
        <asp:Label runat="server" ID="alert_danger_text"></asp:Label></div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                AllowPaging="True" PageSize="20" BorderWidth="0px" Width="100%" OnRowCommand="gvw_data_RowCommand"
                ShowHeaderWhenEmpty="True" OnRowDataBound="gvw_data_RowDataBound" DataKeyNames="departmentid">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                                AutoPostBack="true" />
                        </HeaderTemplate>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chk_select" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="DEPARTMENT CODE">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnk_departmentcode" runat="server" CommandName="cmd_edit" CssClass="gridview_link">department code</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="DEPARTMENT NAME" DataField="departmentname">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="SITE NAME" DataField="SiteName">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
                <PagerStyle BorderColor="Red" BorderStyle="Solid" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
