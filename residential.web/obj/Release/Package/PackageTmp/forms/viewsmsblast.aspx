﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewsmsblast.aspx.cs"
    Inherits="residential.web.forms.viewsmsblast" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>SMS Blast <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase1" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
    </div>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <div class="panel panel-default">
        <div class="panel-heading">SMS Blast Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Service Request</label>
                    <div class="col-sm-10">
                        <asp:GridView ID="gvw_recipients" runat="server" CssClass="table table-hover table-bordered"
                            AutoGenerateColumns="false" PagerStyle-CssClass="pagination-ys" AllowPaging="true">
                            <Columns>
                                <asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="SIM Number" DataField="SMSNumber"></asp:BoundField>
                                <asp:BoundField HeaderText="Name" DataField="SMSName"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Sender</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_sender" runat="server"></asp:Label>
                        </p>
                    </div>
                    <label class="col-sm-2 control-label">Schedule</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_schedule" runat="server" CssClass="labelfield"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Message Content</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_smscontent" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">SMS Status</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_status" runat="server" CssClass="labelfield"></asp:Label>
                        </p>
                    </div>
                    <div class="col-sm-6">
                        <asp:Button ID="btn_send" runat="server" Text="SEND!" OnClick="btn_send_Click" CssClass="btn btn-default" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Qty x Price</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_quantityprice" runat="server" CssClass="labelfield"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Sub Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Adjustment</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_total" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
