﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entrydepartment.aspx.cs"
    Inherits="residential.web.forms.entrydepartment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>:: entry department :: </title>
    <script type="text/javascript" src="../global.js">
    </script>
    <link href="~/maincss.css" rel="stylesheet" type="text/css" />
</head>
<body class="bodypopup">
    <form id="form1" runat="server">
    <div class="popupcontainer">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="buttoncontainer">
                    <asp:Button ID="btn_save" runat="server" Text="SAVE" CssClass="button" Width="100px"
                        OnClick="btn_save_Click" />
                    <asp:Button ID="btn_saveclose" runat="server" Text="SAVE CLOSE" CssClass="button"
                        Width="100px" OnClick="btn_saveclose_Click" />
                    <asp:Button ID="btn_savenew" runat="server" Text="SAVE NEW" CssClass="button" Width="100px"
                        OnClick="btn_savenew_Click" />
                </div>
                <div class="alert alert-success" id="alert_success" runat="server" visible="false">
                    <asp:Label runat="server" ID="alert_success_text"></asp:Label></div>
                <div class="alert alert-danger" id="alert_danger" runat="server" visible="false">
                    <asp:Label runat="server" ID="alert_danger_text"></asp:Label></div>
                <div class="formentrycontent">
                    <div class="formentrycell2">
                        <asp:Label ID="Label1" runat="server" Text="Department Code :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txt_departmentcode" runat="server" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
                    </div>
                    <div class="formentrycell2">
                        <asp:Label ID="Label7" runat="server" Text="Site :" CssClass="labelfield"></asp:Label>
                        <asp:DropDownList ID="cbo_sites" runat="server" CssClass="ComboBoxWindowsStyle" Width="167px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell1">
                        <asp:Label ID="Label4" runat="server" Text="Department Name :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txt_departmentname" CssClass="textbox_entry_left textboxcell1" runat="server"></asp:TextBox>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
