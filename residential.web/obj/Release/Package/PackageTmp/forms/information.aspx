﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="information.aspx.cs" Inherits="residential.web.forms.information" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/bootstrap-notify.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //show_success_alert("123");
        })

        function show_success_case_created_alert() {
            $.notify({
                message: "The Case number has been successfully created"
            }, {
                type: 'success'
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>Information <asp:Label runat="server" ID="lblTitle" /></h1>
            </div>
            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-primary" NavigateUrl="~/forms/entryinformation.aspx"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add</asp:HyperLink>
            <hr />
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-2 control-label">Case Status</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="cbo_status" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" placeholder="search by case number, unit" OnTextChanged="txt_search_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" OnClick="btn_search_Click"  />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" 
                CssClass="table table-bordered table-hover" AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound"
                ShowHeaderWhenEmpty="True"
                DataKeyNames="CaseNumber" AllowSorting="True" OnSorting="gvw_data_Sorting"
                OnPageIndexChanging="gvw_data_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="UNIT CODE" DataField="Unit" SortExpression="Unit" />
                    <asp:BoundField HeaderText="CATEGORY" DataField="HelpCategoryName" SortExpression="HelpCategoryName"></asp:BoundField>
                    <asp:BoundField HeaderText="HELP NAME" DataField="HelpName" SortExpression="HelpName"></asp:BoundField>
                    <asp:BoundField HeaderText="CREATED" DataField="RequestDate" SortExpression="RequestDate" DataFormatString="{0:d MMMM yyyy}" />
                    <asp:BoundField HeaderText="OVERDUE" DataField="OverdueDate" SortExpression="OverdueDate" DataFormatString="{0:d MMMM yyyy}" />
                    <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" SortExpression="CaseStatusName" />
                    <asp:BoundField HeaderText="STATUS" DataField="CaseStatusID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden"/>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
