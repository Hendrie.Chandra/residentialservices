﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="listResidentData.aspx.cs" MasterPageFile="~/main.master" Inherits="residential.web.forms.listResidentData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <link href="../content/bootstrap-dialog.min.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        })
        function ShowAlertSuccess(message) {
            $.notify({
                icon: 'glyphicon glyphicon-ok',
                message: message
            });
        }
        function ShowEmpty() {
            $('#modalAssign').modal('show');

            $("[id$='hfResidentHeaderID']").val('');
            $("[id$='txtUnitCode']").val('');
            $("[id$='txtUnitNo']").val('');
            $("[id$='txtUnitOwner']").val('');
            $("[id$='hdfPsCodeUnitOwner']").val('');
            $("[id$='txtUnitTenant']").val('');
            $("[id$='hdfPsCodeUnitTenant']").val('');

            return true;
        }

        function EnableSaveButton() {
            $("#ContentPlaceHolder1_btn_cek").removeAttr("disabled");
        }
        function DisableSaveButton() {
            $("#ContentPlaceHolder1_btn_cek").attr("disabled", "false");
        }
        function Failed(text) {
            alert(text);
            $('#modalAssign').modal('show');
        }
        function showLoader() {
            if (form1.checkValidity()) {
                $('#<%= UpdateProgress1.ClientID %>').css('display', '');
                return true;
            } else {
                return false;
            }
        }
        function show_dialog(message) {
            BootstrapDialog.confirm({
                title: 'Warning',
                message: message,
                type: BootstrapDialog.TYPE_DANGER,
                callback: function (result) {
                    if (result) {
                        //$('#modalAssign').modal('show');
                        $("#ContentPlaceHolder1_btn_save").click();
                        showLoader();
                    } else {
                        return;
                    }
                }
            });
        }

        function show_message_dialog(issuccess, message) {
            if (issuccess == "0") {
                //failled
                BootstrapDialog.alert({
                    title: 'Warning !',
                    message: message,
                    type: BootstrapDialog.TYPE_DANGER,
                    callback: function (result) {
                        if (result)
                            window.location.href = window.location.href
                        else
                            window.location.href = window.location.href
                    }
                });
            } else if (issuccess == "1") {
                //success
                BootstrapDialog.alert({
                    title: 'Message',
                    message: message,
                    type: BootstrapDialog.TYPE_PRIMARY,
                    callback: function (result) {
                        if (result)
                            window.location.href = window.location.href
                        else
                            window.location.href = window.location.href

                    }
                });
            }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>Resident Data</h1>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Cluster / Tower</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddlCluster" Style="cursor: pointer" runat="server"
                                CssClass="form-control" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlCluster_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" OnTextChanged="txt_search_TextChanged" AutoPostBack="true" placeholder="Search by UnitNo"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Unit Code</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddlUnitCode" Style="cursor: pointer" runat="server"
                                CssClass="form-control" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlUnitCode_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <div class="col-sm-6">
                           <%-- <button type="button" class="btn btn-primary" onclick="ShowEmpty();" data-toggle="tooltip" data-placement="top" title="New Resident Card">New</button>--%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover" AllowPaging="True"
                ShowHeaderWhenEmpty="True" OnRowDataBound="gvw_data_RowDataBound"
                OnPageIndexChanging="gvw_data_PageIndexChanging"
                DataKeyNames="Unit"
                HeaderStyle-HorizontalAlign="Center">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="ResidentHeaderID" DataField="ResidentHeaderID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="UNIT CODE" DataField="Unit" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="OWNER" DataField="OwnerName" SortExpression="OwnerName" />
                    <asp:BoundField HeaderText="TENANT" DataField="TenantName" SortExpression="TenantName" />
                    <asp:BoundField HeaderText="CREATED" DataField="CreatedDate" SortExpression="CreatedDate" DataFormatString="{0:d MMMM yyyy}" />
                    <asp:BoundField HeaderText="RESIDENT" DataField="TotalResident" SortExpression="TotalResident" ItemStyle-HorizontalAlign="Right" />
                    <asp:BoundField HeaderText="CARD" DataField="TotalCard" SortExpression="TotalCard" ItemStyle-HorizontalAlign="Right" />
                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle CssClass="gridview-center" />
                        <ItemTemplate>                            
                            <asp:HyperLink runat="server" ID="lnk_addMember" CssClass="btn btn-info" data-toggle="tooltip" data-placement="top" title="Add Resident Member"><span class="glyphicon glyphicon-user"></span></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="modalAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Resident Data</h4>
                            <asp:HiddenField ID="hfResidentHeaderID" runat="server" />
                        </div>
                        <div class="modal-body">
                            <div class="alert alert-danger alert-dismissible" id="alert_danger" runat="server" visible="false">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <asp:Label ID="alert_danger_text" runat="server" Text="message text"></asp:Label>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><span style="color: Red">*</span> Unit Code</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox runat="server" ID="txtUnitCode" MaxLength="300" OnTextChanged="txtUnitCode_TextChanged" AutoPostBack="true" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><span style="color: Red">*</span> Unit No</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox runat="server" ID="txtUnitNo" MaxLength="300" OnTextChanged="txtUnitNo_TextChanged" AutoPostBack="true" CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Owner Name</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox runat="server" ID="txtUnitOwner" MaxLength="300" CssClass="form-control" ReadOnly="true" />
                                    </div>
                                    <asp:HiddenField ID="hdfPsCodeUnitOwner" runat="server" />
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Tenant Name</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox runat="server" ID="txtUnitTenant" MaxLength="300" CssClass="form-control" ReadOnly="true" />
                                    </div>
                                    <asp:HiddenField ID="hdfPsCodeUnitTenant" runat="server" />
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="modal-footer">
                    <asp:Button ID="btn_save" runat="server" Text="SUBMIT" Style="display: none"
                        CssClass="btn btn-primary "
                        OnClick="btn_save_Click" />
                    <asp:Button ID="btn_cek" runat="server" CssClass="btn btn-primary" Text="SUBMIT" OnClick="btn_cek_Click" Enabled="false" />
                    <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="btn btn-default" data-dismiss="modal" />
                </div>
            </div>
        </div>
    </div>
    <script src="../scripts/bootstrap-dialog.min.js"></script>
    <script src="../scripts/bootstrap-notify.min.js"></script>
</asp:Content>
