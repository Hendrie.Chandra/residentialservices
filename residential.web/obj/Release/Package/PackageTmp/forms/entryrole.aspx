﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryrole.aspx.cs" Inherits="residential.web.forms.entryrole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>Form Role</h1>
            </div>
            <asp:Button ID="btn_saveclose" runat="server" Text="Save & Close" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" />
            <hr />
            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Role Name</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txt_rolename" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txt_roledescription" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                    </div>                    
                </div>
            </div>            
            <div class="formentrycontent">
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role1" runat="server" Text="System Administrator" />
                </div>
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role3" runat="server" Text="Banner" />
                </div>
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role9" runat="server" Text="Reports" />
                </div>
            </div>
            <div class="formentrycontent">
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role2" runat="server" Text="Data Master" />
                </div>
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role4" runat="server" Text="Peminjaman Kendaraan" />
                </div>
                <div class="formentrycell3">
                </div>
            </div>
            <div class="formentrycontent">
                <div class="formentrycell3">
                </div>
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role5" runat="server" Text="Pembuatan KTP-KK" />
                </div>
                <div class="formentrycell3">
                </div>
            </div>
            <div class="formentrycontent">
                <div class="formentrycell3">
                </div>
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role6" runat="server" Text="Penyambungan Meter Air" />
                </div>
                <div class="formentrycell3">
                </div>
            </div>
            <div class="formentrycontent">
                <div class="formentrycell3">
                </div>
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role7" runat="server" Text="SMS Blasting" />
                </div>
                <div class="formentrycell3">
                </div>
            </div>
            <div class="formentrycontent">
                <div class="formentrycell3">
                </div>
                <div class="formentrycell3">
                    <asp:CheckBox ID="chk_role8" runat="server" Text="Other Income" />
                </div>
                <div class="formentrycell3">
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
