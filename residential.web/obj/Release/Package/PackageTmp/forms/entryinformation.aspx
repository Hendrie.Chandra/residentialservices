﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryinformation.aspx.cs" Inherits="residential.web.forms.entryinformation" %>

<%@ Register Src="userHeaderExScheme.ascx" TagName="entryheader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/custom/jquery-customselect-1.9.1.min.js"></script>
    <link href="../scripts/custom/jquery-customselect-1.9.1.css" rel="stylesheet" />

    <script src="../scripts/bootstrap-notify.min.js"></script>
    <link href="../content/bootstrap-dialog.min.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            //show_success_alert("123");
            $("#ContentPlaceHolder1_ddl_helpname").customselect();
        })

        function show_searchable_dropdown() {
            $("#ContentPlaceHolder1_ddl_helpname").customselect();
        }

        function showLoader() {
            if (form1.checkValidity()) {
                $('#<%= UpdateProgress1.ClientID %>').css('display', '');
                return true;
            } else {
                return false;
            }
        }
        function show_message_dialog(issuccess, message) {
            if (issuccess == "0") {
                //failled
                BootstrapDialog.alert({
                    title: 'Warning !',
                    message: message,
                    type: BootstrapDialog.TYPE_DANGER,
                    callback: function (result) {
                        if (result)
                            window.location.href = "entryinformation.aspx"
                        else
                            window.location.href = "entryinformation.aspx"
                    }
                });
            } else if (issuccess == "1") {
                //success
                BootstrapDialog.alert({
                    title: 'Message',
                    message: message,
                    type: BootstrapDialog.TYPE_PRIMARY,
                    callback: function (result) {
                        if (result)
                            window.location.href = "entryinformation.aspx"
                        else
                            window.location.href = "entryinformation.aspx"

                    }
                });
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form Information
            <asp:Label runat="server" ID="lblTitle" /></h1>
    </div>
    <hr />
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Information Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Help Name</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddl_helpname" runat="server" CssClass="form-control custom-select">
                                </asp:DropDownList>
                                <asp:HiddenField ID="HFHelpID" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </ContentTemplate>
    </asp:UpdatePanel>
    <hr />
    <div class="form-group">
        <div class="col-sm-8"></div>
        <div class="col-sm-4 text-right">
            <asp:Button ID="btnSaveClose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSaveClose_Click" OnClientClick="showLoader()" />
            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-default" NavigateUrl="~/forms/information.aspx">Back</asp:HyperLink>
        </div>
    </div>
    <script src="../scripts/bootstrap-dialog.min.js"></script>
</asp:Content>
