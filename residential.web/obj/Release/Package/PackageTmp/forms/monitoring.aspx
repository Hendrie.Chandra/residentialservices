﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="monitoring.aspx.cs" Inherits="residential.web.forms.monitoring" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/Chart.min.js"></script>
    <script src="../scripts/dashboard/pikaday.js"></script>
    <link href="../scripts/dashboard/pikaday.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
                <h1>SLA Monitoring<asp:Label runat="server" ID="lblTitle" /></h1>
            </div>
    <div class="container">        
        <div class="row">
        <div class="col-md-2">
            <asp:Label ID="Label3" runat="server" Text="Closed Date" Font-Bold="True" Font-Size="Medium"></asp:Label>
        </div>
    </div><br />

        <div class="row form-inline">
            <div class="form-group">
                <div class="col-md-2">From</div>
                <div class="col-md-3">
                    <asp:TextBox ID="TxtStartDate" runat="server" ClientIDMode="Static" CssClass="form-control" AutoComplete="off"></asp:TextBox>
                    <%--<asp:CalendarExtender ID="TxtStartDate_CalendarExtender" runat="server" TargetControlID="TxtStartDate" PopupPosition="TopLeft" Format="dd MMMM yyyy"></asp:CalendarExtender>--%>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-1">To</div>
                <div class="col-md-3">
                    <asp:TextBox ID="TxtEndDate" runat="server" ClientIDMode="Static" CssClass="form-control" AutoComplete="off"></asp:TextBox>
                    <%--<asp:CalendarExtender ID="TxtEndDate_CalendarExtender" runat="server" TargetControlID="TxtEndDate" PopupPosition="TopLeft" Format="dd MMMM yyyy"></asp:CalendarExtender>--%>
                </div>
            </div>
            <asp:Button ID="picker" runat="server" Text="Get Selected Date" CssClass="btn btn-primary" OnClick="picker_Click" />
        </div>
    </div>

        <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('TxtStartDate'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000, 2020],
                numberOfMonths: 1,

            });
    </script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('TxtEndDate'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000, 2020],
                numberOfMonths: 1,

            });
    </script>

    <br />
    <br />

    <div class="row">
        <div class="col-md-8">
            <label class="col-sm-2 control-label">Department List</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="cbo_deptlist" runat="server" CssClass="form-control"></asp:DropDownList>
        </div>
    </div>
        </div>
        <br />
    <br />
        <div class="row">
        <div class="col-md-8">
            <blockquote>
                <canvas id="myChart1" width="700" height="600"></canvas>
            </blockquote>
        </div>
            <div class="col-md-4">
                 <asp:Label ID="Label2" runat="server" Text="SLA Summary" Font-Bold="False" Font-Size="X-Large"></asp:Label><br />
            
           <asp:GridView ID="gvw_sla" runat="server" AutoGenerateColumns="False" DataKeyNames="CloseDate" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" Font-Size="Medium" GridLines="Horizontal" Height="500" Width="400">
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CloseDate" HeaderText="Close Date" ReadOnly="True" DataFormatString="{0:d MMMM yyyy}"
                        SortExpression="CloseDate" />
                    <asp:BoundField DataField="MeetSLA" HeaderText="Meet SLA"
                        SortExpression="MeetSLA" />
                    <asp:BoundField DataField="OverSLA" HeaderText="Over SLA"
                        SortExpression="OverSLA" />
                </Columns>
                <EmptyDataTemplate>
                    <asp:Label ID="Label3" runat="server" Text="NO DATA" Font-Bold="true" Font-Size="XX-Large"></asp:Label>
                </EmptyDataTemplate>
                <FooterStyle BackColor="White" ForeColor="#333333" />
                <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="White" ForeColor="#333333" HorizontalAlign="Center" VerticalAlign="Middle" />
                <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F7F7F7" />
                <SortedAscendingHeaderStyle BackColor="#487575" />
                <SortedDescendingCellStyle BackColor="#E5E5E5" />
                <SortedDescendingHeaderStyle BackColor="#275353" />
            </asp:GridView>
            <%--<blockquote>
                <canvas id="myChart2" width="600" height="400"></canvas>
            </blockquote>--%>
        </div>
    </div>
    <br />

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-7">
           
        </div>
        <div class="col-md-2"></div>
        </div>

    <br />

    <asp:Label ID="chart1data" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart1label" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart2data" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="chart2label" runat="server" Visible="false"></asp:Label>

    <% 
        string EmptyData1;
        if (chart1data.Text.ToString() == "")
        {
            EmptyData1 = "[0, 0]";
        } else {
            EmptyData1 = "["+chart1data.Text+"]";
        } %>
        <%
            string EmptyLabel1;
            if (chart2data.Text.ToString() == "")
            {
                EmptyLabel1 = "['NO DATA', 'NO DATA']";
            }
            else {
                EmptyLabel1 = "[" + chart1label.Text + "]";
            }
             %>

    <% 
        string EmptyData2;
        if (chart2data.Text.ToString() == "")
        {
            EmptyData2 = "[0, 0]";
        } else {
            EmptyData2 = "["+chart2data.Text+"]";
        } %>
        <%
            string EmptyLabel2;
            if (chart2label.Text.ToString() == "")
            {
                EmptyLabel2 = "['NO DATA','NO DATA']";
            }
            else {
                EmptyLabel2 = "[" + chart2label.Text + "]";
            }
             %>

    <script>
       var ctx = document.getElementById("myChart1");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                //labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
               labels:<%=EmptyLabel1%>,
                //labels: [<%=chart1label.Text%>],
                datasets: [{
                    label: 'Meet SLA',
                    data: <%=EmptyData1%>,
                   //data: [<%=chart1data.Text%>],

                    backgroundColor: [
                        'rgba(54, 162, 235, 0.2)',
                        
                    ],
                    borderColor: 'blue',
                    borderWidth: 1
                },
                {
                    label: 'Over SLA',
                    data: <%=EmptyData2%>,
                    //data: [<%=chart2data.Text%>],

                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                    ],
                    borderColor: 'red',
                    borderWidth: 1
                }
                ]
            },

           
            options: {
                //elements: {
                //    line: {
                //        skipNull: true,
                //        drawNull: false,
                //    }
                //},
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });

<%--         var ctx = document.getElementById("myChart2");
        var myChart = new Chart(ctx, {
            type: 'line',
           data: { 
                //labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
               labels:<%=EmptyLabel2%>,
               //labels: [<%=chart2label.Text%>],
                datasets: [{
                    label: 'Over SLA',
                    data: <%=EmptyData2%>,
                    //data: [<%=chart2data.Text%>],

                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                    ],
                    borderColor: 'red',
                    borderWidth: 1
                }]
            },

           
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });--%>

        </script>

</asp:Content>
