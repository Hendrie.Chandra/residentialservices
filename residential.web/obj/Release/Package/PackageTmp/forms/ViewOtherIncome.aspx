﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/main.master" CodeBehind="ViewOtherIncome.aspx.cs" Inherits="residential.web.forms.ViewOtherIncome" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/emailviewer.ascx" TagName="EmailViewer" TagPrefix="aspuc" %>  

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">    
    <div class="page-header">
        <h1>Other Income <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_email">Email Viewer</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase" runat="server" />
            <asp:Button ID="btnApproval" Text="Approve" runat="server" 
                                    CssClass="btn btn-warning" 
                onclick="btnApproval_Click"/>
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
        <div id="tab_email" class="tab-pane fade">
            <aspuc:EmailViewer ID="emailviewer" runat="server" />
        </div>
    </div>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <div class="panel panel-default">
        <div class="panel-heading">Request Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Service Request</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_servicerequest" runat="server" ></asp:Label>
                        </p>
                    </div>
                    <label class="col-sm-2 control-label">Payment Scheme</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            <asp:Label ID="lblPaymentScheme" runat="server" ></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                            CssClass="table table-bordered table-hover"
                            ShowHeaderWhenEmpty="True" PageSize="15"
                            HeaderStyle-HorizontalAlign="Center">
                            <PagerStyle CssClass="pagination-ys" />
                            <Columns>
                                <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Item Name" DataField="HelpItemName" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                <asp:BoundField HeaderText="Quantity" DataField="Quantity" SortExpression="Quantity" ItemStyle-HorizontalAlign="Center" />
                                <asp:BoundField HeaderText="Price" DataField="Amount" SortExpression="Price" DataFormatString="{0:n0}" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField HeaderText="TotalPrice" DataField="TotalAmount" SortExpression="TotalPrice" DataFormatString="{0:n0}" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                            <EmptyDataTemplate>
                                No data
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
                <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Sub Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_subtotal" runat="server" Font-Bold="true"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Adjustment</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_adjustment" runat="server" Font-Bold="true"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_total" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                </p>
            </div>
        </div>
            </div>                              
        </div>
    </div>
    <div class="form-horizontal">
        
    </div>
</asp:Content>