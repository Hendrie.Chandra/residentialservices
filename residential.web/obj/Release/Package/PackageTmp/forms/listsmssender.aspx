﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listsmssender.aspx.cs" Inherits="residential.web.forms.listsmssender" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>List SMS Sender</h1>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-primary" NavigateUrl="~/forms/entrysmssender.aspx">Add</asp:HyperLink>
    <asp:Button ID="btn_delete" runat="server" CssClass="btn btn-default" Text="Delete" OnClick="btn_delete_Click" OnClientClick="return confirmDelete();" />
    <hr />
    <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
        CssClass="table table-hover table-bordered" AllowPaging="True"
        OnRowDataBound="gvw_data_RowDataBound"
        OnPageIndexChanging="gvw_data_PageIndexChanging"
        ShowHeaderWhenEmpty="True"
        DataKeyNames="SMSSenderID">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:CheckBox ID="chk_selectall" runat="server"
                        OnCheckedChanged="chk_selectall_CheckedChanged" AutoPostBack="true" />
                </HeaderTemplate>
                <HeaderStyle CssClass="gridview_header" Width="30px" />
                <ItemStyle CssClass="gridview_item_center" />
                <ItemTemplate>
                    <asp:CheckBox ID="chk_select" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="NO">
                <HeaderStyle CssClass="gridview_header" Width="30px" />
                <ItemStyle CssClass="gridview_item_center" />
                <ItemTemplate>
                    <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="SMS SENDER" DataField="SMSSender">
                <HeaderStyle CssClass="gridview_header" />
                <ItemStyle CssClass="gridview_item_left" />
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            No data
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
