﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="smartcardreport.aspx.cs" Inherits="residential.web.forms.smartcardreport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        function showLoader() {
            if (form1.checkValidity()) {
                $('#<%= UpdateProgress1.ClientID %>').css('display', '');
                    return true;
                } else {
                    return false;
                }
            }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="container" style="margin-top: 0px;">
        <div class="page-header">
            <h1>Smartcard Report</h1>
        </div>

        <div id="errID" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
        <asp:Label ID="lbl_error" runat="server" Visible="false"></asp:Label>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning!</strong> <%=lbl_error.Text%>
        </div>

        <div class="panel panel-default">
            <div class="panel-body" style="background-color: #f2f2f2">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6" style="padding-left:100px; padding-right:100px">
                            <asp:Button ID="btn_export" runat="server" Text="Export to Excel" style="padding-left:100px; padding-right:100px" CssClass="btn btn-default" OnClick="btn_export_Click"/>
                        </div>
                        <div class="col-md-6" style="padding-left:100px; padding-right:100px">
                            <asp:Button ID="btn_run_report" runat="server" Text="Run Report" style="padding-left:100px; padding-right:100px" CssClass="btn btn-default" OnClick="btn_run_report_Click" OnClientClick="showLoader()" />
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <br />
        <br />
        <hr />

        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover"
                    AllowPaging="True"
                    ShowHeaderWhenEmpty="True" DataKeyNames="CardNumber" EmptyDataText="No Data"
                    AllowSorting="true" OnSorting="gvw_data_Sorting" OnPageIndexChanging="gvw_data_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="CARD NUMBER" DataField="CardNumber" SortExpression="CardNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="CHIP NUMBER" DataField="ChipNumber" SortExpression="ChipNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="PSCODE" DataField="PSCode" SortExpression="PSCode"></asp:BoundField>
                        <asp:BoundField HeaderText="HOLDER" DataField="HolderName" SortExpression="HolderName"></asp:BoundField>
                        <asp:BoundField HeaderText="BOOMGATE NAME" DataField="BoomGateName" SortExpression="BoomGateName"></asp:BoundField>
                        <asp:BoundField HeaderText="UNIT CODE" DataField="UnitCode" SortExpression="UnitCode"></asp:BoundField>
                        <asp:BoundField HeaderText="UNIT NO" DataField="UnitNo" SortExpression="UnitNo"></asp:BoundField>
<%--                        <asp:TemplateField HeaderText="STATUS" ItemStyle-HorizontalAlign="Center" SortExpression="Status">
                            <ItemTemplate>
                                <asp:Label ID="Status" runat="server" Text='<%# (int)Eval("Status")==1 ? "Active" : "InActive" %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="REQUESTOR" DataField="NamaPelapor" SortExpression="Requestor"></asp:BoundField>--%>
                    </Columns>
                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>--%>

