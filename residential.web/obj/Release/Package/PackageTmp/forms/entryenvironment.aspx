﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryenvironment.aspx.cs" Inherits="residential.web.forms.entryenvironment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register src="entryheader.ascx" tagname="entryheader" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="buttoncontainer">
            <asp:Button ID="btn_save" runat="server" Text="SAVE" CssClass="button" 
                Width="100px" onclick="btn_save_Click" Visible="false" />
            <asp:Button ID="btn_saveclose" runat="server" Text="SAVE CLOSE" OnClientClick="buttonSaving(this)" UseSubmitBehavior="false"
                    CssClass="button" Width="100px" onclick="btn_saveclose_Click" />
            <asp:Button ID="btn_savenew" runat="server" Text="SAVE NEW" CssClass="button" 
                Width="100px" onclick="btn_savenew_Click" Visible="false" />
            <asp:Button ID="btn_back" runat="server" Text="BACK" CssClass="button" 
                Width="100px" onclick="btn_back_Click" />
        </div>                    
                    <div class="alert alert-success" id="alert_success" runat="server" visible="false"><asp:Label runat="server" ID="alert_success_text"></asp:Label></div>
                    <div class="alert alert-danger" id="alert_danger" runat="server" visible="false"><asp:Label runat="server" ID="alert_danger_text"></asp:Label></div>
                    <h3>
                        Environment Department
                    </h3>
                    <uc1:entryheader ID="entryheader" runat="server" />
                    <div class="formentrycontent">
                        <div class="formentrycell1">
                            <asp:Label ID="Label3" runat="server" Text="Service Request :" CssClass="labelfield"></asp:Label>
                            <asp:DropDownList ID="cbo_servicerequests" runat="server" CssClass="ComboBoxWindowsStyle" 
                                Width="502px" 
                                onselectedindexchanged="cbo_servicerequests_SelectedIndexChanged" 
                                AutoPostBack="True"></asp:DropDownList>
                        </div>
                    </div>                    
                    <%--<div class="formentrycontent" id="startandenddate" runat="server">
                        <div class="formentrycell2">
                            <asp:Label ID="Label7" runat="server" Text="Start Date :" CssClass="labelfield"></asp:Label>
                            <asp:TextBox ID="txt_startdate" runat="server" 
                                CssClass="textbox_entry_left textboxcell2" 
                                ontextchanged="txt_startdate_TextChanged" AutoPostBack="True"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_startdate" Format="dd MMMM yyyy"></asp:CalendarExtender>
                        </div>
                        <div class="formentrycell2">
                            <asp:Label ID="Label8" runat="server" Text="End Date :" CssClass="labelfield"></asp:Label>
                            <asp:TextBox ID="txt_enddate" runat="server" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txt_enddate" Format="dd MMMM yyyy"></asp:CalendarExtender>
                        </div>                    
                    </div>--%>
                    <div class="formentrycontent">
                        <div class="formentrycell2">                            
                            <asp:Label ID="Label5" runat="server" Text="Price :" CssClass="labelfield"></asp:Label>                            
                            <asp:Label ID="lbl_price" runat="server"></asp:Label>
                        </div>
                        <div class="formentrycell2">
                            <asp:Label ID="Label1" runat="server" CssClass="labelfield" Text="Adjustment :"></asp:Label>                            
                            <asp:TextBox ID="txt_discount" Style="text-align:right" 
                                CssClass="textboxcell2 textbox_entry_left" runat="server" AutoPostBack="True" 
                                ontextchanged="txt_discount_TextChanged"></asp:TextBox>
                            <asp:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txt_discount" FilterType="Numbers" />
                        </div>
                    </div>                   
                    <div style="border:1px solid red;">
                    <div class="formentrycontent">
                        <div class="formentrycell2" style="text-align:right">
                            <asp:Label ID="Label4" runat="server" Text="Sub Total" CssClass="style1" ></asp:Label> 
                        </div>
                        <div class="formentrycell2" style="text-align:right">                                                                          
                            <asp:Label ID="lbl_subtotal" runat="server" CssClass="style1" ></asp:Label>                            
                        </div>
                    </div>
                    <div class="formentrycontent">
                        <div class="formentrycell2" style="text-align:right">
                            <asp:Label ID="Label2" runat="server" Text="Adjustment" CssClass="style1" ></asp:Label> 
                        </div>
                        <div class="formentrycell2" style="text-align:right">                                                                          
                            <asp:Label ID="lbl_adjustment" runat="server" CssClass="style1" ></asp:Label>
                        </div>
                    </div>
                    <div class="formentrycontent">
                        <div class="formentrycell2" style="text-align:right">
                            <asp:Label ID="Label6" runat="server" Text="Total" CssClass="style1" ></asp:Label> 
                        </div>
                        <div class="formentrycell2" style="text-align:right">                                                                          
                            <asp:Label ID="lbl_total" runat="server" CssClass="style1"></asp:Label>
                        </div>
                    </div> 
                    </div>                   
                </ContentTemplate>
            </asp:UpdatePanel>
     </asp:Content>