﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrysmsblasting.aspx.cs"
    Inherits="residential.web.forms.entrysmsblasting" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form SMS Blast</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <%--<div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>--%>
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">SMS Blast Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sender ID</label>
                            <div class="col-sm-4">
                                <asp:DropDownList CssClass="form-control" ID="cbo_sender" runat="server">
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Subscription Type</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cbo_subscriptionType" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Recipient List</label>
                            <div class="col-sm-10">
                                <asp:GridView ID="gv_simnumber" runat="server" CssClass="table table-hover table-bordered" ShowHeaderWhenEmpty="True"
                                    AutoGenerateColumns="False" DataKeyNames="SMSNumber" AllowPaging="true"
                                    OnRowCommand="gv_simnumber_RowCommand" OnPageIndexChanging="gv_simnumber_PageIndexChanging">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="NO">
                                            <HeaderStyle CssClass="gridview_header" Width="30px" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_no" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="SIM NO" DataField="SMSNumber">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="NAME" DataField="SMSName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <HeaderStyle CssClass="gridview_header" Width="30px" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk_delete" runat="server" CommandArgument="<%# Container.DataItemIndex %>">
                                                    <asp:Image ID="img_delete" runat="server" ImageAlign="Middle" ImageUrl="~/images/icon-delete.png" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <asp:Button CssClass="btn btn-danger" ID="btn_clearAll" runat="server" Text="Clear All"
                                    OnClick="btn_clearAll_Click" UseSubmitBehavior="false" />
                                <asp:Button CssClass="btn btn-warning" ID="btn_removeDuplicateNumber" runat="server"
                                    Text="Remove Duplicate Number" OnClick="btn_removeDuplicateNumber_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Add Recipient(s)</label>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Unit Group</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <asp:TextBox ID="txt_unitgroup" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                    <span class="input-group-addon">
                                        <button type="button" data-toggle="modal" data-target="#modalUnitGroup">...</button></span>
                                </div>
                                <%--<asp:ModalPopupExtender ID="ModalPopupExtender1" runat="server" TargetControlID="btn_unitgroup" BackgroundCssClass="modalBackground" PopupControlID="Panel1"
                                    CancelControlID="btn_cancel">
                                </asp:ModalPopupExtender>--%>
                            </div>
                            <div class="col-sm-2">
                                <asp:Button ID="btn_addGroup" runat="server" Text="Add" CssClass="btn btn-primary" OnClick="btn_addGroup_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Mobile Number</label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txt_SIMNo" runat="server" MaxLength="15" CssClass="form-control" Type="Number" placeholder="mobile number"></asp:TextBox>
                            </div>
                            <label class="col-sm-2 control-label">Name</label>
                            <div class="col-sm-3">
                                <asp:TextBox ID="txt_SMSName" runat="server" CssClass="form-control" placeholder="name"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                <asp:Button ID="btn_addSIMNo" runat="server" Text="Add" CssClass="btn btn-primary" OnClick="btn_addSIMNo_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Import Data</label>
                            <div class="col-sm-8">
                                <asp:FileUpload ID="fu_SIMNo" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-sm-2">
                                <asp:Button ID="btn_UploadSMSGroup" runat="server" Text="Upload" CssClass="btn btn-primary"
                                    OnClick="btn_UploadSMSGroup_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Message Content</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txt_messageContent" runat="server" CssClass="form-control" Rows="3"
                                    MaxLength="160" TextMode="MultiLine" OnTextChanged="txt_messageContent_TextChanged"
                                    AutoPostBack="True"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 text-right">
                                <asp:Label ID="lbl_charLength" runat="server" Text="160"></asp:Label>&nbsp;char(s) left
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <asp:Button CssClass="btn btn-danger" ID="btn_clearMessage" runat="server" Text="Clear Message"
                                    OnClick="btn_clearMessage_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Miscellaneous</label>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-2">
                                <div class="checkbox">
                                    <asp:CheckBox ID="chk_schedule" runat="server" Text="Schedule" OnCheckedChanged="chk_schedule_CheckedChanged"
                                        AutoPostBack="true" />
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_schedule" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                <asp:CalendarExtender runat="server" ID="calExtender1" TargetControlID="txt_schedule"
                                    Format="dd MMMM yyyy" />
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox runat="server" ID="txt_time" Enabled="false" Type="Time" CssClass="form-control"></asp:TextBox>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10">
                                <div class="checkbox">
                                    <asp:CheckBox ID="chk_internalgroup" runat="server" Text="From Internal Group" AutoPostBack="true"
                                        OnCheckedChanged="chk_internalgroup_CheckedChanged" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Quantity x Price</label>
                            <div class="col-sm-4">
                                <p class="form-control-static">
                                    <asp:Label ID="lbl_quantity" runat="server"></asp:Label>
                                    &nbsp;x&nbsp;
                                    <asp:Label ID="lbl_price" runat="server"></asp:Label>
                                </p>                                
                            </div>
                            <label class="col-sm-2 control-label">Adjustment</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_discount" CssClass="form-control" AutoPostBack="true" Type="Number"
                                runat="server" Text="0" OnTextChanged="txt_discount_TextChanged"></asp:TextBox>
                            </div>                          
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
            <%--<asp:Panel ID="Panel1" runat="server" CssClass="modalPopup">
                <asp:TreeView ID="tv_unitgroup" runat="server"
                    OnSelectedNodeChanged="tv_unitgroup_SelectedNodeChanged">
                </asp:TreeView>
                <p>Click to select unit group</p>
                <div style="text-align: center;">
                    <asp:Button ID="btn_cancel" runat="server" Text="Cancel" />
                </div>
            </asp:Panel>--%>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btn_UploadSMSGroup" />
        </Triggers>
    </asp:UpdatePanel>
    <div class="modal fade" id="modalUnitGroup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Recipents from Unit Group</h4>
                </div>
                <div class="modal-body">
                    <asp:TreeView ID="tv_unitgroup" runat="server"
                        OnSelectedNodeChanged="tv_unitgroup_SelectedNodeChanged">
                    </asp:TreeView>
                    <p>* click to select unit group</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>
