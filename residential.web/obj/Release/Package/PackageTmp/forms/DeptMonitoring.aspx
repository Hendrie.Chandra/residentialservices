﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="DeptMonitoring.aspx.cs" Inherits="residential.web.forms.DeptMonitoring" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/dashboard/pikaday.js"></script>
    <link href="../scripts/dashboard/pikaday.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
                <h1>Case Monitoring (Department)<asp:Label runat="server" ID="lblTitle" /></h1>
            </div>
    <div class="container">
        <div class="row">
        <div class="col-md-2">
            <asp:Label ID="Label2" runat="server" Text="Created Date" Font-Bold="True" Font-Size="Medium"></asp:Label>
        </div>
    </div><br />

        <div class="row form-inline">
            <div class="form-group">
                <div class="col-md-2">From</div>
                <div class="col-md-3">
                    <asp:TextBox ID="TxtStartDate" runat="server" ClientIDMode="Static" CssClass="form-control" AutoComplete="off"></asp:TextBox>
                    <%--<asp:CalendarExtender ID="TxtStartDate_CalendarExtender" runat="server" TargetControlID="TxtStartDate" PopupPosition="TopLeft" Format="dd MMMM yyyy"></asp:CalendarExtender>--%>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-1">To</div>
                <div class="col-md-3">
                    <asp:TextBox ID="TxtEndDate" runat="server" ClientIDMode="Static" CssClass="form-control" AutoComplete="off"></asp:TextBox>
                    <%--<asp:CalendarExtender ID="TxtEndDate_CalendarExtender" runat="server" TargetControlID="TxtEndDate" PopupPosition="TopLeft" Format="dd MMMM yyyy"></asp:CalendarExtender>--%>
                </div>
            </div>
            <asp:Button ID="picker" runat="server" Text="Get Selected Date" CssClass="btn btn-primary" OnClick="picker_Click" />
        </div>
    </div>
    <br />

        <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('TxtStartDate'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000, 2020],
                numberOfMonths: 1,

            });
    </script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('TxtEndDate'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000, 2020],
                numberOfMonths: 1,

            });
    </script>

             <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover" OnRowDataBound="gvw_data_RowDataBound" 
                ShowHeaderWhenEmpty="True"
                DataKeyNames="DeptName" AllowSorting="True" OnSorting="gvw_data_Sorting"
                OnPageIndexChanging="gvw_data_PageIndexChanging" HeaderStyle-HorizontalAlign="Center" HeaderStyle-VerticalAlign="Middle" ShowFooter="true">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:BoundField HeaderText="DEPARTMENT NAME" DataField="DeptName" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="PENDING" DataField="Pending" SortExpression="Pending" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="ASSIGN TO PIC" DataField="AssignToPIC" SortExpression="AssignToPIC" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                    <asp:BoundField HeaderText="CANCELLED" DataField="Cancelled" SortExpression="Cancelled" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                    <asp:BoundField HeaderText="IN PROGRESS" DataField="InProgress" SortExpression="InProgress" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                    <asp:BoundField HeaderText="ON HOLD" DataField="OnHold" SortExpression="OnHold" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="PROGRESS COMPLETE" DataField="ProgressComplete" SortExpression="ProgressComplete" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="RESOLVED" DataField="Resolved" SortExpression="Resolved" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="NOT SATISFED" DataField="NotSatisfied" SortExpression="NotSatisfied" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="CASE CLOSED" DataField="CaseClosed" SortExpression="CaseClosed" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="REJECTED" DataField="Rejected" SortExpression="Rejected" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="WAITING FOR VALIDATION" DataField="WaitingForValidation" SortExpression="Pending" ItemStyle-HorizontalAlign="Center" />
                </Columns>
                        <FooterStyle BackColor="#d9ecff" ForeColor="#333333" Font-Bold="True" HorizontalAlign="Center" VerticalAlign="Middle" />
                <EmptyDataTemplate>
                    No Data
                </EmptyDataTemplate>
            </asp:GridView>
</asp:Content>
