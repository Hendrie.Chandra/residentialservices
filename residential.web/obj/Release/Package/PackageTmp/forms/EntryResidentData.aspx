﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/main.master" CodeBehind="EntryResidentData.aspx.cs" Inherits="residential.web.forms.EntryResidentData" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <link href="../content/bootstrap-dialog.min.css" rel="stylesheet" />
    <script>
        function pageLoad(sender, args) {
            var date = new Date();
            var currentMonth = 12;
            var currentDate = 31;
            var currentYear = date.getFullYear();
            $("[id$='txtBirthDate']").datepicker({
                format: 'dd MM yyyy',
                changeMonth: true,
                changeYear: true,
                maxDate: new Date(currentYear, currentMonth, currentDate),
                yearRange: "1920:" + currentYear + ""
            });
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        }
        function ShowEmpty() {
            $('#modalAssign').modal('show');

            var Active = document.getElementById("<%=ddlActive.ClientID %>");
            Active.selectedIndex = 0;

            var Gender = document.getElementById("<%=ddlGender.ClientID %>");
            Gender.selectedIndex = 0;

            var ResidentStatus = document.getElementById("<%=ddlResidentStatus.ClientID %>");
            ResidentStatus.selectedIndex = 0;

            var Nationality = document.getElementById("<%=ddlNationality.ClientID %>");
            Nationality.selectedIndex = 0;

            $("[id$='hfResidentDetailID']").val('');
            $("[id$='txtName']").val('');
            $("[id$='txtBirthDate']").val('');

            return true;
        }
        function Confirm(ResidentDetailID, Name, BirthDate, Gender, ResidentStatus, Nationality, isActive) {
            $('#modalAssign').modal('show');

            $("[id$='hfResidentDetailID']").val(ResidentDetailID);
            $("[id$='txtName']").val(Name);
            $("[id$='ddlActive']").val(isActive);
            $("[id$='txtBirthDate']").val(BirthDate);
            $("[id$='ddlGender']").val(Gender);
            $("[id$='ddlNationality']").val(Nationality);
            $("[id$='ddlResidentStatus']").val(ResidentStatus);

            return true;
        }
        function Failed(text) {
            alert(text);
            $('#modalAssign').modal('show');
        }

        function show_dialog() {
            BootstrapDialog.confirm({
                title: 'Warning',
                message: 'Are you sure want to save this data?',
                type: BootstrapDialog.TYPE_DANGER,
                callback: function (result) {
                    if (result) {

                    } else {
                        return;
                    }
                }
            });
        }
        function show_message_dialog(issuccess, message) {
            if (issuccess == "0") {
                //failled
                BootstrapDialog.alert({
                    title: 'Message',
                    message: message,
                    type: BootstrapDialog.TYPE_DANGER,
                    callback: function (result) {
                        if (result)
                            window.location.href = window.location.href
                        else
                            window.location.href = window.location.href
                    }
                });
            } else if (issuccess == "1") {
                //success
                BootstrapDialog.alert({
                    title: 'Warning',
                    message: message,
                    type: BootstrapDialog.TYPE_PRIMARY,
                    callback: function (result) {
                        if (result)
                            window.location.href = window.location.href
                        else
                            window.location.href = window.location.href

                    }
                });
            }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h3>Form Resident Data Unit : <b><asp:Label runat="server" ID="lblTitle" /></b></h3>
                <asp:HiddenField ID="hfRHI" runat="server" />
            </div>

            <div id="errID" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
                <asp:Label ID="lbl_error" runat="server" Visible="false"></asp:Label>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> <%=lbl_error.Text%>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Owner PsCode / Name</label>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtOwnerPsCode"  CssClass="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Tenant PsCode / Name</label>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtTenantPsCode"  CssClass="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Is Active</label>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtIsActive"  CssClass="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-primary" onclick="ShowEmpty();" data-toggle="tooltip" data-placement="top" title="New Resident Data">New</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover" AllowPaging="True"
                ShowHeaderWhenEmpty="True" OnRowDataBound="gvw_data_RowDataBound"
                OnPageIndexChanging="gvw_data_PageIndexChanging" 
                DataKeyNames="ResidentDetailID"
                 HeaderStyle-HorizontalAlign="Center">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="ResidentDetailID" DataField="ResidentDetailID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="NAME" DataField="Name" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="GENDER" DataField="Gender" SortExpression="Gender"  ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="GENDER" DataField="GenderName" SortExpression="GenderName"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField HeaderText="BIRTH DATE" DataField="BirthDate" SortExpression="BirthDate" DataFormatString="{0:d MMMM yyyy}"  ItemStyle-HorizontalAlign="Right"/>
                    <asp:BoundField HeaderText="NATIONALITY" DataField="nationality" SortExpression="nationality"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField HeaderText="NationalityID" DataField="NationalityID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="RESIDENT STATUS" DataField="ResidentStatusDesc" SortExpression="ResidentStatusDesc"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField HeaderText="ResidentStatusID" DataField="ResidentStatusID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="ACTIVE" DataField="isActive" SortExpression="isActive"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField HeaderText="LAST MODIFY DATE" DataField="ModifiedDate" SortExpression="ModifiedDate" DataFormatString="{0:d MMMM yyyy}"  ItemStyle-HorizontalAlign="Right"/>
                    <asp:BoundField HeaderText="LAST MODIFY" DataField="ModifiedBy" SortExpression="ModifiedBy"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle CssClass="gridview-center" />
                        <ItemTemplate>                            
                            <asp:HyperLink runat="server" ID="lnk_edit" CssClass="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit Resident Member"><span class="glyphicon glyphicon-pencil"></span></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="modalAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Resident Data</h4>
                    <asp:HiddenField ID="hfResidentDetailID" runat="server" />
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Name</label>
                            <div class="col-sm-9">
                                <asp:TextBox runat="server" ID="txtName" MaxLength="300" CssClass="form-control"  />
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Birth Date</label>
                            <div class="col-sm-9">
                                <asp:TextBox runat="server" style="cursor:pointer" ID="txtBirthDate"  data-toggle="tooltip" data-placement="top" ToolTip="Format Day/Month/Year" CssClass="selectpicker form-control"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Gender</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlGender" runat="server" style="cursor:pointer" CssClass="form-control">
                                    <asp:ListItem Text="Male (Pria)" Value="L" />
                                    <asp:ListItem Text="Female (Wanita)" Value="P" /> 
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                     <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Resident Status</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlResidentStatus" runat="server" style="cursor:pointer" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                     <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Nationality</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlNationality" runat="server" style="cursor:pointer" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Active</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlActive" runat="server" style="cursor:pointer" CssClass="form-control">
                                    <asp:ListItem Text="True" Value="1" />
                                    <asp:ListItem Text="False" Value="0" /> 
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                </ContentTemplate></asp:UpdatePanel>
                <div class="modal-footer">
                    <asp:Button ID="btn_save" runat="server" Text="SUBMIT" 
                        CssClass="btn btn-primary" UseSubmitBehavior="false" data-dismiss="modal" 
                        onclick="btn_save_Click"/>
                    <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="btn btn-default" data-dismiss="modal" />
                </div>
            </div>
        </div>  
    </div>
    <script src="../scripts/bootstrap-dialog.min.js"></script>
</asp:Content>