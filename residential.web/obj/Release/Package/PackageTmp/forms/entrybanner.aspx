﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrybanner.aspx.cs" Inherits="residential.web.forms.entrybanner" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%--<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>--%>
<%@ Register Src="~/forms/emailviewer.ascx" TagName="EmailViewer" TagPrefix="aspuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/accounting.js"></script>
    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
                startDate: '<%# DateTime.Today.Date.AddDays(5).ToString("yyyy-mm-dd") %>'
            });



            //$('#ContentPlaceHolder1_txt_adjustment').on('keyup', function () {
            //    var adj = $('#ContentPlaceHolder1_txt_adjustment').val();
            //    var rep = adj.replace(/,/g, "");
            //    var adjint = parseInt(rep);
            //    var adjval = accounting.formatMoney(adjint, "", 0);
            //    alert(adjval);
            //    $('#ContentPlaceHolder1_txt_adjustment').val(adjval);
            //});
        });

        function inputAdjusment() {
            $(<%=txt_adjustment.ClientID%>).on('keyup', function () {

                var adj = $('#ContentPlaceHolder1_txt_adjustment').val();
                var rep = adj.replace(/\,/g, "");                               //format currency production mengunakan (,)
                var adjint = parseInt(rep);
                var adjval = accounting.formatMoney(adjint, "", 0, ",");        //format currency production mengunakan (,)

                $(<%=txt_adjustment.ClientID%>).val(adjval);
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form Pemasangan Banner</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <div id="updaterequest" runat="server">
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
<%--        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>--%>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_email">Email Viewer</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade in active">
            <aspuc:UpdateCase ID="updatecase" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
<%--        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>--%>
        <div id="tab_email" class="tab-pane fade">
            <aspuc:EmailViewer ID="emailviewer" runat="server" />
        </div>
    </div>
    </div>
    <hr />
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <%--<div class="container navbar-fixed-top alert-custom" role="alert" runat="server" id="alert_danger" visible="false">
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
                </div>
            </div>--%>
            <div class="panel panel-default">
                <div class="panel-heading">Banner Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Type</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboType" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="cboType_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Location</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboLocation" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="cboLocation_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Point</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboPoint" runat="server" CssClass="form-control" OnSelectedIndexChanged="cboPoint_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control" placeholder="start date" AutoPostBack="true" OnTextChanged="txtFromDate_TextChanged"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" placeholder="end date"></asp:TextBox>
                            </div>
                            <asp:CalendarExtender ID="txt_date_CalendarExtender" runat="server"
                                TargetControlID="txtFromDate" Format="dd MMMM yyyy">
                            </asp:CalendarExtender>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                TargetControlID="txtToDate" Format="dd MMMM yyyy">
                            </asp:CalendarExtender>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-5">
                                <asp:Button ID="btn_viewBookedTable" runat="server" Text="View Booked Table" CssClass="btn btn-primary" data-toggle="modal" data-target="#myModal" UseSubmitBehavior="false" OnClick="btn_viewBookedTable_Click" Enabled="false" />
                            </div>
                            <div class="col-sm-5 text-right">
                                <asp:Button ID="btn_add_detail" runat="server" Text="Add Detail" CssClass="btn btn-primary" OnClick="btn_add_detail_Click" UseSubmitBehavior="false" />
                                <asp:Button ID="btnDelete" runat="server" Text="Delete Detail" CssClass="btn btn-default" OnClick="btnDelete_Click" OnClientClick="return confirmDelete();" />
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Detail List</label>
                            <div class="col-sm-10">
                                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                    AllowPaging="True" PageSize="10" ShowHeaderWhenEmpty="true" DataKeyNames="BannerDetailID" OnPageIndexChanging="gvw_data_PageIndexChanging">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                                                    AutoPostBack="true" />
                                            </HeaderTemplate>
                                            <HeaderStyle CssClass="gridview_header" Width="30px" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk_select" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Banner Type" DataField="BannerTypeName" SortExpression="BannerTypeName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Location" DataField="LocationName" SortExpression="LocationName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Point" DataField="PointName" SortExpression="PointName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Start Date" DataField="StartDate" SortExpression="StartDate"
                                            DataFormatString="{0:d MMMM yyyy}">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="End Date" DataField="ToDate" SortExpression="ToDate" DataFormatString="{0:d MMMM yyyy}">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Price" DataField="Price" SortExpression="Price">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total Price</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_sewatempat" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Installation Fee</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_biayapasang" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">PPN 10%</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_ppn" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:HiddenField ID="hdf_adjustment" runat="server" />
                            <%--<asp:Label ID="lbl_adjustment" runat="server"></asp:Label>--%> <%--original code--%>
                            <asp:TextBox ID="txt_adjustment" runat="server" CssClass="form-control rightAlign" OnTextChanged="txt_adjustment_TextChanged" AutoPostBack="true"></asp:TextBox>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Booked Table</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Banner Location</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">
                                            <asp:Label ID="lbl_bannerLocation" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Start Date</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_startDateRange" runat="server" CssClass="form-control"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                            TargetControlID="txt_startDateRange" Format="yyyy-MM-dd">
                                        </asp:CalendarExtender>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:Button ID="btn_boundTable" runat="server" Text="Show" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_boundTable_Click" />
                                    </div>
                                </div>
                            </div>
                            <asp:Table ID="tbl_bookedBanner" runat="server" CssClass="table table-bordered">
                            </asp:Table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

