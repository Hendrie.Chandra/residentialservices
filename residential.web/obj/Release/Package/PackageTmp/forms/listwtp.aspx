﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listwtp.aspx.cs" Inherits="residential.web.forms.wtpmenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="innerlinkcontent">
        <div class="innerlinkcell">
            <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icon-meterair.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/forms/watertreatmentplant.aspx" 
                CssClass="contentlink">Pemasangan Meter Air</asp:HyperLink>
            <br />
            Permohonan pemasangan meter air
        </div>
        <div class="innerlinkcell">
            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icon-sambungair.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/forms/rewatertreatmentplant.aspx" 
                CssClass="contentlink">Penyambungan Meter Air</asp:HyperLink>
            <br />
            Permohonan penyambungan meter air
        </div>
    </div>
</asp:Content>
