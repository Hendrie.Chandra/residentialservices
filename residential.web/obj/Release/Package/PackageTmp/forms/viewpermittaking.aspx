﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewpermittaking.aspx.cs"
    Inherits="residential.web.forms.viewpermittaking" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>Pengambilan Izin BCD <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <asp:Button ID="btn_update" runat="server" Text="Update" OnClick="btn_update_Click" />
    <asp:Button ID="btn_cancel" runat="server" Text="Back" OnClick="btn_cancel_Click" />
    <hr />
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Case Detail</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>        
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:ViewHeader ID="viewheader" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>       
    </div>
    <hr />    
    <div class="panel panel-default">
        <div class="panel-heading">BCD Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Service Request</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_servicerequest" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Duration</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_duration" runat="server"></asp:Label>&nbsp;Days
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Start Date</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txt_startdate" runat="server" CssClass="form-control"
                            OnTextChanged="txt_startdate_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_startdate"
                            Format="dd MMMM yyyy">
                        </asp:CalendarExtender>
                    </div>
                    <label class="col-sm-2 control-label">End Date</label>
                    <div class="col-sm-4">
                        <asp:Label ID="lbl_enddate" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Payment Status</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_paymentstatus" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Sub Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Adjustment</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_total" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
