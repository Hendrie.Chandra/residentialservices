﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewcaselibrary.ascx.cs" Inherits="residential.web.forms.viewcaselibrary" %>

<%--<asp:GridView ID="gvw_attachment" runat="server" AutoGenerateColumns="False"
    CssClass="table table-hover table-bordered" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField HeaderText="NO">
            <HeaderStyle Width="30px" />
            <ItemStyle HorizontalAlign="Center" />
            <ItemTemplate>
                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="FILES">
            <ItemTemplate>
                <asp:HyperLink ID="lnk_files" runat="server" Target="_blank" NavigateUrl='<%# Eval("SharepointLink") %>'><%# Eval("FileName") %></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="FileCreated" HeaderText="MODIFIED" />
        <asp:BoundField DataField="ID" HeaderText="ID" Visible="False" />
        <asp:BoundField DataField="SharePointLink" HeaderText="SharePointLink" Visible="False" />
    </Columns>   
</asp:GridView>--%>
<iframe id="AttachmentDownloadContainer" style="display:none"></iframe>

<script>
    function DeleteNote(_casenumber, _attachmentid) {
        var isdelete = confirm("Are you sure that you want to delete this file ?");
        if (isdelete) {
            var url = window.location.href + "&prosesadt=deletenote&attachmentid=" + _attachmentid;
            $.ajax({
                cache: false,
                type: 'POST',
                url: url,
                success: function (response) {
                    console.log(response);
                    if (response == "Sukses") {
                        location.reload();
                    }
                }
            });
        }
    }

    function DownloadNote(_casenumber, _attachmentid) {
        var url = window.location.href + "&prosesadt=downloadnote&attachmentid=" + _attachmentid;
        $("#AttachmentDownloadContainer").attr('src', url);

 
    }
</script>
<asp:ScriptManager ID="ScriptManagerzzz" runat="server"></asp:ScriptManager>
<asp:UpdatePanel ID="UpdatePanelzzz" runat="server">
    <ContentTemplate>
        <asp:GridView ID="gvw_attachmentfromcrm" OnRowCommand="gvw_attachmentfromcrm_RowCommand" OnRowDataBound="gvw_attachmentfromcrm_RowDataBound"
            runat="server" AutoGenerateColumns="False"
            CssClass="table table-hover table-bordered" ShowHeaderWhenEmpty="true" DataKeyNames="Id">
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="ID" Visible="false" />
                <asp:BoundField DataField="FileName" HeaderText="File Name" />
                <%--<asp:BoundField DataField="CaseStatusID" HeaderText="CaseStatusID" />--%>
                <asp:BoundField DataField="MimeType" HeaderText="Type" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="DownloadButton" runat="server"
                            CommandName="DownloadFile">Download</asp:LinkButton> | 
                        <asp:LinkButton ID="DeleteButton" runat="server"
                            CommandName="DeleteFile">Delete</asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel> 
