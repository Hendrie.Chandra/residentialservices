﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/main.master" CodeBehind="BannerReport.aspx.cs" Inherits="residential.web.forms.BannerReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">  
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <script>
    </script>
    <div class="page-header">
        <h1>View Banner Slot</h1>
    </div>
    <div class="panel panel-default">
        <div class="input-group" style="width:30%">
            <asp:DropDownList runat="server" ID="ddlType" CssClass="form-control" />
            <asp:DropDownList runat="server" Id="ddlMonth" CssClass="form-control" />
            <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="Load" CssClass="btn btn-default" />
        </div>
    </div>
    <br />
    <div class="panel">
        <div id="divTable" runat="server">
        </div>
    </div>
</asp:Content>
