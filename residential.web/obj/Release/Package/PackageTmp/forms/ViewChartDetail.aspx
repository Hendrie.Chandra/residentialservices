﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="ViewChartDetail.aspx.cs" Inherits="residential.web.forms.ViewChartDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Chart Detail</h1>
    <div style="width: 320.750px; height: 156px">
    <canvas id="myChart"></canvas>
  </div>

    <script>
    var data = {
      datasets: [{
        data: [300, 50, 100, 200],
        backgroundColor: [
          "#a02020",
          "#ed1c24",
          "#ff9900",
          "#109618",
        ]
      }],
      labels: [
        "Critical",
        "High",
        "Medium",
        "Low"
      ]
    };

    $(document).ready(
      function() {
        var canvas = document.getElementById("myChart");
        var ctx = canvas.getContext("2d");
        var myNewChart = new Chart(ctx, {
          type: 'pie',
          data: [300, 50, 100, 200],
        });

        canvas.onclick = function(evt) {
          var activePoints = myNewChart.getElementsAtEvent(evt);
          var chartData = activePoints[0]['_chart'].config.data;
          var idx = activePoints[0]['_index'];

          var label = chartData.labels[idx];
          var value = chartData.datasets[0].data[idx];

          var url = "http://example.com/?label=" + label + "&value=" + value;
          console.log(url);
          alert(url);
        };
      }
    );
  </script>
</asp:Content>
