﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true"
    CodeBehind="listbannertype.aspx.cs" Inherits="residential.web.forms.listbanner" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>List Banner Type</h1>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-primary" NavigateUrl="~/forms/entrybannertype.aspx">Add</asp:HyperLink>
    <asp:Button ID="Button1" runat="server" CssClass="btn btn-default" Text="Delete" OnClick="btn_delete_Click" OnClientClick="return confirmDelete();"  />
    <hr />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>                
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound"
                OnPageIndexChanging="gvw_data_PageIndexChanging" ShowHeaderWhenEmpty="True"
                DataKeyNames="BannerTypeID">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                                AutoPostBack="true" />
                        </HeaderTemplate>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chk_select" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Banner Type Name">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                        <ItemTemplate>                            
                            <asp:HyperLink ID="lnk_bannertype" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
