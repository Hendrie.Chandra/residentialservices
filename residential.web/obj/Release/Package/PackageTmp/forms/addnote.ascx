﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="addnote.ascx.cs" Inherits="residential.web.forms.addnote" %>
<%@ Register Src="viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>

<script>
    function showLoader() {
        if (form1.checkValidity()) {
            $('#overlay').css('display', '');
            return true;
        } else {
            return false;
        }
    }
    function FormatCurrency(ctrl) {
        //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
        if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
            return;
        }
        var val = ctrl.value;
        val = val.replace(/,/g, "")
        ctrl.value = "";
        val += '';
        x = val.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;

        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        ctrl.value = x1 + x2;
    }
    function CheckNumeric() {
        return event.keyCode >= 48 && event.keyCode <= 57;
    }
</script>
<div id="overlay" style="display:none">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
</div>
<div class="container navbar-fixed-top alert-custom" role="alert" runat="server" id="alert_danger" visible="false">
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>
</div>
<div id="assignCaseForm" runat="server" visible="false">
    <div class="form-horizontal">        
        <div class="form-group">
            <label class="col-sm-2 control-label">Assign Case To :</label>
            <div class="col-sm-4">
                <asp:DropDownList ID="cbo_assign" runat="server" CssClass="form-control" required="required"></asp:DropDownList>
            </div>            
            <asp:Button ID="btn_assign" runat="server" Text="Assign" CssClass="btn btn-primary" OnClick="btn_assign_Click" OnClientClick="showLoader()" />            
        </div>
    </div>
</div>
<hr />
<div id="updateCaseForm" runat="server" visible="false">
    <div class="form-horizontal">        
        <div class="form-group">
            <%--<label class="col-sm-2 control-label">Case Status</label>--%>
            <div class="col-sm-4">
                <asp:DropDownList ID="cbo_casestatus" runat="server" CssClass="form-control" Visible="false"></asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Remarks</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txt_remarks" runat="server" Rows="3" TextMode="MultiLine" CssClass="form-control" placeholder="remarks"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Files Upload</label>
            <div class="col-sm-10">
                <asp:FileUpload ID="filesUpload" runat="server" CssClass="form-control" />
            </div>
        </div>
        <div class="form-group" runat="server" id="Adjustment" visible="false">
            <label class="col-sm-2 control-label">Adjustment</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txt_discount" runat="server" CssClass="form-control" Type="Number"
                                     Text="0" onkeypress="return CheckNumeric();"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12 text-right">
                <asp:Button ID="btn_update" runat="server" Text="Submit" CssClass="btn btn-primary" OnClientClick="showLoader()" OnClick="btn_update_Click" />
            </div>
        </div>
    </div>
</div>

<div id="divHiddenFields">
    <asp:HiddenField ID="hfDepartmentID" runat="server" />
    <asp:HiddenField ID="hfIsFinalInspect" runat="server" />
    <asp:HiddenField ID="hfIsPaskem" runat="server" />
</div>
