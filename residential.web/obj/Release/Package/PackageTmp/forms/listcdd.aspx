﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listcdd.aspx.cs" Inherits="residential.web.forms.cddmenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="innerlinkcontent">
        <div class="innerlinkcell">
            <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icon-banner.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/forms/banner.aspx" 
                CssClass="contentlink">Pemasangan Banner</asp:HyperLink>
            <br />
            Pemasangan banner baru
        </div>
        <div class="innerlinkcell">
            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icon-changebanner.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/forms/rebanner.aspx" 
                CssClass="contentlink">Penggantian Banner</asp:HyperLink>
            <br />
            Permohonan penggantian banner
        </div>
    </div>
    <div class="innerlinkcontent">
        <div class="innerlinkcell">
            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icon-magazine.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/forms/magazine.aspx" 
                CssClass="contentlink">Majalah</asp:HyperLink>
            <br />
            Pemasangan iklan di majalah
        </div>
        <div class="innerlinkcell">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon-sms.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/forms/listsmsblasting.aspx"
                CssClass="contentlink">SMS Blast</asp:HyperLink>
            <br />
            Permohonan SMS Blasting
        </div>
    </div>
    <div class="innerlinkcontent">
        <div class="innerlinkcell">
            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icon-flyer.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/forms/flyer.aspx" 
                CssClass="contentlink">Flyer</asp:HyperLink>
            <br />
            Permohonan penyebaran flyer
        </div>        
    </div>    
</asp:Content>
