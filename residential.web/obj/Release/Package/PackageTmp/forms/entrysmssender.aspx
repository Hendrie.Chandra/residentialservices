﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrysmssender.aspx.cs" Inherits="residential.web.forms.entrysmssender" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="buttoncontainer">
        <asp:button id="btn_saveclose" runat="server" text="SAVE CLOSE" cssclass="button"
            width="100px" onclick="btn_saveclose_Click" onclientclick="buttonSaving(this)"
            usesubmitbehavior="false" />
        <asp:button id="btn_back" runat="server" text="BACK" cssclass="button" width="100px"
            onclick="btn_back_Click" />
    </div>
    <div class="alert alert-success" id="alert_success" runat="server" visible="false">
        <asp:label runat="server" id="alert_success_text"></asp:label>
    </div>
    <div class="alert alert-danger" id="alert_danger" runat="server" visible="false">
        <asp:label runat="server" id="alert_danger_text"></asp:label>
    </div>    
    <div class="formentrycontent">
        <div class="formentrycell1">
            <asp:label id="Label1" runat="server" text="SMS Sender :" cssclass="labelfield"></asp:label>
            <asp:textbox id="txt_smssender" runat="server" cssclass="textbox_entry_left textboxcell1"></asp:textbox>
        </div>        
    </div>
</asp:Content>
