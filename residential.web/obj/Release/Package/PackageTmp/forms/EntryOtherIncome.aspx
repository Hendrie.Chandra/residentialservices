﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/main.master" CodeBehind="EntryOtherIncome.aspx.cs" Inherits="residential.web.forms.EntryOtherIncome" %>

<%@ Register Src="userHeaderExScheme.ascx" TagName="entryheader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script>
        function FormatCurrency(ctrl) {
            //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
            if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
                return;
            }
            var val = ctrl.value;
            val = val.replace(/,/g, "")
            ctrl.value = "";
            val += '';
            x = val.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;

            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            ctrl.value = x1 + x2;
        }
        function CheckNumeric() {
            return event.keyCode >= 48 && event.keyCode <= 57;
        }
    </script>
    <div class="page-header">
        <h1>Form Other Income <asp:Label runat="server" ID="lblTitle" /></h1>
    </div>
    <hr />
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Others Income Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Service Request</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cbo_servicerequests" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="cbo_servicerequests_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="HFHelpID" runat="server" />
                            </div>
                            <label class="col-sm-2 control-label">Payment Scheme</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlPaymentScheme" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Item</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlItem" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="ddlItem_SelectedIndexChanged" AutoPostBack="True">
                                </asp:DropDownList>
                                <asp:HiddenField ID="HiddenField1" runat="server" />
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Quantity</label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control" Type="Number"
                                    Text="1" onkeypress="return CheckNumeric();"></asp:TextBox>
                            </div>
                            <label class="col-sm-1 control-label">Price</label>
                            <div class="col-sm-2">
                                <p class="form-control-static">
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="lbl_price" runat="server" Font-Bold="true" ForeColor="Red" Text="0"></asp:Label>
                                </p>
                            </div>
                            <label class="col-sm-1 control-label"></label>
                            <div class="col-sm-4">
                                <asp:Button ID="btnAdd" Text="Add Item" runat="server" 
                                    CssClass="btn btn-primary" onclick="btnAdd_Click" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:UpdatePanel runat="server" ID="upTable" UpdateMode="Conditional"><ContentTemplate>
                                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered table-hover"
                                    ShowHeaderWhenEmpty="True"  OnRowDeleting="gvw_data_RowDeleting"
                                    DataKeyNames="HelpItemID" PageSize="15"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField HeaderText=""  HeaderStyle-CssClass="gridview_header" ItemStyle-HorizontalAlign="Center" >
                                            <ItemStyle CssClass="gridview_item_center" />
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgDelete" runat="server" ImageUrl="~/Images/icon-delete.png" OnClientClick="return confirm('Are you sure you want to delete this item?');" CommandName="Delete"/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Item Name" DataField="HelpItemName" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField HeaderText="ItemID" DataField="HelpItemID" SortExpression="ItemID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden"/>
                                        <asp:BoundField HeaderText="Quantity" DataField="Quantity" SortExpression="Quantity" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="Price" DataField="Price" SortExpression="Price" DataFormatString="{0:n0}" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField HeaderText="TotalPrice" DataField="TotalPrice" SortExpression="TotalPrice" DataFormatString="{0:n0}" ItemStyle-HorizontalAlign="Right" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </ContentTemplate></asp:UpdatePanel>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-4">
                            </div>
                            <label class="col-sm-2 control-label">Adjustment</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_discount" runat="server" CssClass="form-control" Type="Number"
                                    AutoPostBack="True" OnTextChanged="txt_discount_TextChanged" Text="0" onkeypress="return CheckNumeric();"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" Font-Bold="true" runat="server"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_adjustment" Font-Bold="true" runat="server"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" Font-Bold="true" ForeColor="Red" runat="server"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </p>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <hr />
    <div class="form-group">
        <div class="col-sm-8"></div>
        <div class="col-sm-4 text-right">
            <asp:Button ID="Button1" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click"/>
            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-default" NavigateUrl="~/forms/OtherIncome.aspx">Back</asp:HyperLink>
        </div>
    </div>
</asp:Content>