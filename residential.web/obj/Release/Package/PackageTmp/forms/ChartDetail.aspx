﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="ChartDetail.aspx.cs" Inherits="residential.web.forms.ChartDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/Chart.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
                <h1>Chart Detail : [All Case]<asp:Label runat="server" ID="lblTitle" /></h1>
            </div>
    <div class="row">
              <div class="row">
                  <div class="col-md-4">
                      <asp:Label ID="SD" runat="server" Text="Label" Font-Bold="true" Font-Size="Medium"></asp:Label>
                  </div>
                  <div class="col-md-3">
                      <asp:Label ID="DN" runat="server" Text="Label" Font-Bold="true" Font-Size="Medium"></asp:Label>
                  </div>
                  <div class="col-md-3">
                      <asp:Label ID="ID" runat="server" Text="Label" Font-Bold="true" Font-Size="Medium"></asp:Label><br />
                  </div>
              </div><br /><br ? />
            
              <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover" AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound" 
                ShowHeaderWhenEmpty="True"
                DataKeyNames="CaseNumber"
                OnPageIndexChanging="gvw_data_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="UNIT CODE" DataField="Unit" SortExpression="Unit" />
                    <asp:BoundField HeaderText="CATEGORY" DataField="HelpCategory"></asp:BoundField>
                    <asp:BoundField HeaderText="DEPT NAME" DataField="DeptName" ></asp:BoundField>
                    <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="CREATED" DataField="CreatedDate" DataFormatString="{0:d MMMM yyyy}" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="SLA(DAYS)" DataField="SLA" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="OVERDUE" DataField="OverdueDate" DataFormatString="{0:d MMMM yyyy}" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="DAYS" DataField="Days" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="CREATED BY" DataField="CreatedBy" />
                    <%--<asp:BoundField HeaderText="STATUS" DataField="CaseStatusID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden"/>--%>
                </Columns>
                <EmptyDataTemplate>
                    No Data
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
     </div>
       
</asp:Content>
