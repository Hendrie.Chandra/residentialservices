﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewpeminjamankendaraan.aspx.cs"
    Inherits="residential.web.forms.viewpeminjamankendaraan" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>Peminjaman Kendaraan <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase1" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
    </div>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <div class="panel panel-default">
        <div class="panel-heading">Car Rent Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Car License Plate</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblCarLicensePlate" runat="server"></asp:Label>
                        </p>
                    </div>
                    <label class="col-sm-2 control-label">Package</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblPackage" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Date</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblDate" runat="server"></asp:Label>
                        </p>
                    </div>
                    <label class="col-sm-2 control-label">Time</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblPickupTime" runat="server"></asp:Label>&nbsp;-&nbsp;<asp:Label ID="lblUntil" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Pickup Location</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblPickupLocation" runat="server"></asp:Label>
                        </p>
                    </div>
                    <label class="col-sm-2 control-label">Destination</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblDestination" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Tariff</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblTariff" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6"></div>
        <label class="col-sm-2 control-label">Tariff</label>
        <div class="col-sm-4 text-right">
            <p class="form-control-static">
                <asp:Label ID="lbl_tariff" runat="server"></asp:Label>
            </p>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-6"></div>
        <label class="col-sm-2 control-label">Package</label>
        <div class="col-sm-4 text-right">
            <p class="form-control-static">
                <asp:Label ID="lbl_package" runat="server"></asp:Label>
            </p>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Sub Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Adjustment</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_total" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
