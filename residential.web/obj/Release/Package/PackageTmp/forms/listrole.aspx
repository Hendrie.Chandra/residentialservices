﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true"
    CodeBehind="listrole.aspx.cs" Inherits="residential.web.forms.listrole" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>List Role</h1>
            </div>
            <asp:Button CssClass="btn btn-primary" ID="btn_add" runat="server" Text="Add" OnClick="btn_add_Click" />
            <asp:Button CssClass="btn btn-default" ID="btn_delete" runat="server" Text="Delete"
                OnClientClick="return confirmDelete();" OnClick="btn_delete_Click" />
            <hr />
            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
            </div>
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover"
                AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound"
                ShowHeaderWhenEmpty="true" OnRowCommand="gvw_data_RowCommand" DataKeyNames="roleid">
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="30px">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                                AutoPostBack="true" />
                        </HeaderTemplate>                        
                        <ItemTemplate>
                            <asp:CheckBox ID="chk_select" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">                        
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ROLE NAME" ItemStyle-Font-Bold="true">                        
                        <ItemTemplate>
                            <asp:HyperLink ID="lnk_role" runat="server">role name</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="DESCRIPTION" DataField="roledescription" />
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
