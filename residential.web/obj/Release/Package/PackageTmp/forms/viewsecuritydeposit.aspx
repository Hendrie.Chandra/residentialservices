﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewsecuritydeposit.aspx.cs" Inherits="residential.web.forms.viewsecuritydeposit" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/emailviewer.ascx" TagName="EmailViewer" TagPrefix="aspuc" %>  
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function fillAndCloseRelatedCase(caseNumber)
        {
            $('#<%= txt_relatedCaseNumber.ClientID %>').val(caseNumber);
            $('#modalFindRelatedCase').modal('hide');
        }

        function fillAndRelatedCaseDesc(relatedcasedesc) {
            $('#<%= lbl_relatedcasedescription.ClientID %>').text(relatedcasedesc);
             }
    </script>
    <div class="page-header">
        <h1>Pengambilan Security Deposit <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_email">Email Viewer</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
        <div id="tab_email" class="tab-pane fade">
            <aspuc:EmailViewer ID="emailviewer" runat="server" />
        </div>
    </div>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <div class="panel panel-default">
        <div class="panel-heading">Security Deposit Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-sm-2">Related Case</label>
                    <div class="col-sm-8">
                        <asp:TextBox ID="txt_relatedCaseNumber" runat="server" CssClass="form-control" placeholder="related case number"></asp:TextBox>
                    </div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalFindRelatedCase">Find Related Case</button>
                </div>
               <%-- add related description--%>
            <div class="form-group">
                <label class="col-sm-2 control-label">Related Case Description</label>
                <div class="col-sm-10">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_relatedcasedescription" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
               <%-- add related description--%>
            </div>
            <div class="modal fade" id="modalFindRelatedCase" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Find Related Case</h4>
                        </div>
                        <div class="modal-body">
                           <%-- <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">                               
                                <ContentTemplate>
                                    <asp:GridView ID="gvw_data" runat="server" CssClass="table table-bordered table-hover" AutoGenerateColumns="False" AllowPaging="true" OnPageIndexChanging="gvw_data_PageIndexChanging"
                                        AllowSorting="true" OnSorting="gvw_data_Sorting" DataKeyNames="CaseNumber" OnRowCommand="gvw_data_RowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderText="No" HeaderStyle-Width="30px">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField HeaderText="Case Number" DataField="CaseNumber" SortExpression="CaseNumber"></asp:BoundField>
                                            <asp:BoundField HeaderText="Help Name" DataField="HelpName" SortExpression="HelpName"></asp:BoundField>
                                            <asp:BoundField HeaderText="Created Date" DataField="CreatedDate" SortExpression="CreatedDate" DataFormatString="{0:dd MMMM yyyy}"></asp:BoundField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>                               
                                                    <asp:Button ID="btn_selectRelated" runat="server" Text="Select" CssClass="btn btn-primary btn-sm" CommandArgument='<%# Eval("CaseNumber") %>' CommandName="SelectRelatedCase" UseSubmitBehavior="false" />         
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>                            
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
