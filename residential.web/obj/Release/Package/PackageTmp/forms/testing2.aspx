﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="testing2.aspx.cs" Inherits="residential.web.forms.testing2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/Chart.min.js"></script>
    <script src="../scripts/dashboard/pikaday.js"></script>
    <link href="../scripts/dashboard/pikaday.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

    <script>
        $(function () {

            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Residential Services'
                },
                credits: {
                    enabled: false
                },
                xAxis: {
                    categories: ["Mampus", "Koma", "Cacat", "Luka Bakar"],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'JML Korban'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                legend: {
                    labelFormatter: function () {
                        var total = 0;
                        for (var i = this.yData.length; i--;) {
                            total += this.yData[i];
                        }
                        ;
                        return 'Total: ' + total;
                    },
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 60,
                    floating: true
                },
                plotOptions: { column: { pointPadding: 0.2, borderWidth: 0, dataLabels: { enabled: true } }, },
                series: [{
                    name: 'Data Korban',
                    data: [{ "y": 111, "url": "http://192.168.21.28/residentialdev/Forms/dasboard" }, { "y": 44, "url": "http://192.168.21.28/residentialdev/Forms/Report" }, { "y": 22, "url": "http://192.168.21.28/residentialdev/Forms/monitoring" }, { "y": 10, "url": "http://192.168.21.28/residentialdev" }],
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                window.open(this.options.url, '_blank');
                            }
                        }
                    }
                }]
            });

        });
    </script>

</asp:Content>
