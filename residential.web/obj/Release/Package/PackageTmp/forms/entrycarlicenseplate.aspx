﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrycarlicenseplate.aspx.cs"
    Inherits="residential.web.forms.entrycarlicenseplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="buttoncontainer">
        <asp:Button ID="btn_save" runat="server" Text="SAVE" CssClass="button" Width="100px"
            OnClick="btn_save_Click" Visible="false" />
        <asp:Button ID="btn_saveclose" runat="server" Text="SAVE CLOSE" CssClass="button"
            Width="100px" OnClick="btn_saveclose_Click" />
        <asp:Button ID="btn_savenew" runat="server" Text="SAVE NEW" CssClass="button" Width="100px"
            OnClick="btn_savenew_Click" Visible="false" />
    </div>
    <div class="alert alert-success" id="alert_success" runat="server" visible="false">
        <asp:Label runat="server" ID="alert_success_text"></asp:Label>
    </div>
    <div class="alert alert-danger" id="alert_danger" runat="server" visible="false">
        <asp:Label runat="server" ID="alert_danger_text"></asp:Label>
    </div>
    <div class="formentrycontent">
        <div class="formentrycell1">
            <asp:Label ID="Lbl_CarLicensePlateID" runat="server" Visible="false"></asp:Label>
        </div>
    </div>
    <div class="formentrycontent">
        <div class="formentrycell1">
            <asp:Label ID="Label4" runat="server" Text="Car License Plate :" CssClass="labelfield"></asp:Label>
            <asp:TextBox ID="txt_CarLicensePlateName" runat="server" CssClass="textbox_entry_left textboxcell1"></asp:TextBox>
        </div>
    </div>
</asp:Content>
