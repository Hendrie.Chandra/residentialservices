﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrysmartcardregistration.aspx.cs" Inherits="residential.web.forms.entrysmartcardregistration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>
<%--<%@ Register Src="~/forms/addsmartcard.ascx" TagName="addsmartcard" TagPrefix="aspuc" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/jquery.sumoselect.min.js"></script>

    <link href="../content/sumoselect.css" rel="stylesheet" />
    <link href="../content/bootstrap-dialog.min.css" rel="stylesheet" />
    <style type="text/css">
        .errormessage {
            padding-left: 10px;
            color: #B50128;
            font-size: 12px;
            font-family: Verdana, Tahoma, Arial;
            font-weight: bold;
        }
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }
        #navwrap {
      
          background: #eeeeee;
          -webkit-box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
          -moz-box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
          box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
        }
    </style>


    <script>
        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $('#back-to-top').tooltip('show');

            $(<%=LB_Cluster.ClientID%>).SumoSelect({ selectAll: true });


            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_cardnumber_member.ClientID%>').on("keypress", function () {

                    $('#<%=txt_cardnumber_member.ClientID%>').keypress(function (event) {
                        var keycode = (event.keyCode ? event.keyCode : event.which);
                        if (keycode == '13') {
                            event.preventDefault();
                            return false;
                        }
                    });
                });
            });

            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_effectivedate_member.ClientID%>').on("change", function () {
                    //$('.nav-tabs a:[href="#cardinformation"]').tab('show');

                });
            });

            $("body").find("#modalFormDetailIDResident").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDResident").find(".modal-body").find('#<%=txt_vehiclenumber_resident.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
            $("body").find("#modalFormDetailIDResident").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDResident").find(".modal-body").find('#<%=txt_vehiclebrand_resident.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });

            //validate vehicle number
            $("body").find("#modalFormDetailIDResident").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDResident").find(".modal-body").find('#<%=txt_vehiclenumber_resident.ClientID%>').off('keyup').on('keyup', function (e) {
                    var val = $(this).val();
                    if (val.match(/[^A-Za-z0-9]/g)) {
                        $(this).val(val.replace(/[^A-Za-z0-9]/g, ''));
                    }
                });
            });
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_vehiclenumber_member.ClientID%>').off('keyup').on('keyup', function (e) {
                    var val = $(this).val();
                    if (val.match(/[^A-Za-z0-9]/g)) {
                        $(this).val(val.replace(/[^A-Za-z0-9]/g, ''));
                    }
                });
            });

            //member
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_name_member.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_idcardnumber_member.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_officeaddress_member.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_mobilephone_member.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_vehiclenumber_member.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_vehiclebrand_member.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_cardnumber.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
            $("body").find("#modalFormDetailIDMember").find(".modal-body").on("click", function () {
                $("body").find("#modalFormDetailIDMember").find(".modal-body").find('#<%=txt_chipnumber_member.ClientID%>').on("keypress", function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        return false;
                    }

                });
            });
        });

        //function UserSaveConfirmation() {

        //    if (confirm("Are you sure you want to save?") == true) {
        //        //press ok
        //        return true;
        //    } else {
        //        //press cencel
        //        return false;
        //    }
        //}

        function selecttab(event) {
            $('.nav-tabs a[href="#cardinformation"]').tab('show');
            event.preventDefault();
            //$("#adt_cardinformation").trigger("click");
            //alert(obj.id +"ss");
            return false;
        }


        function select_nexttab() {
            $('.nav-tabs a[href="#cardinformation"]').tab('show');
            return false;

        }


        function domultyselect() {
            $(<%=LB_Cluster.ClientID%>).SumoSelect({
                placeholder: 'Select Boom Gate Cluster',
                csvDispCount: 10,
                search: true,
                searchText: 'search..',
                noMatch: 'No matches for "{0}"'
            });
        }

        function closeDetailForm() {
            //$('#modalFormDetailID').modal('hide');
            var formID = $('#<%=txt_formid.ClientID%>').val();
            if (formID == 1) {
                $('#modalFormDetailIDResident').modal('hide');
            } else {
                $('#modalFormDetailIDMember').modal('hide');
            }


        }
        //debugger;
        function showModalFormDetailID(personalDetailID) {
            var formID = $('#<%=txt_formid.ClientID%>').val();
            if (formID == 1) {
                $('#<%# txt_smartcard_identifier_detail_ID.ClientID %>').val(personalDetailID);
                $('#<%# btn_showDetailPersonalID.ClientID %> ').click();
                $('#modalFormDetailIDResident').modal('show');
            } else {
                $('#<%# txt_smartcard_identifier_detail_ID.ClientID %>').val(personalDetailID);
                $('#<%# btn_showDetailPersonalID.ClientID %> ').click();
                $('#modalFormDetailIDMember').modal('show');
            }

        }

        function showModalFormByCardType() {
            var formID = $('#<%=txt_formid.ClientID%>').val();
            console.log(formID);
            if (formID == 1) {
                //1: resident
                $('#modalFormDetailIDResident').modal('show');
            }
            else {
                $('#modalFormDetailIDMember').modal('show');
            }
        }

        function showLoader() {
            if (form1.checkValidity()) {
                $('#<%= UpdateProgress1.ClientID %>').css('display', '');
                return true;
            } else {
                return false;
            }
        }

        function show_dialog_onsave() {
            BootstrapDialog.confirm({
                title: 'Warning',
                message: 'Are you sure want to save this data?',
                type: BootstrapDialog.TYPE_DANGER,
                callback: function (result) {
                    if (result) {
                        var cluster = $('#ContentPlaceHolder1_LB_Cluster').val();
                        $('#<%= btn_save_smartcard.ClientID%>').click();
                        if (cluster == null)
                            return;
                        showLoader();
                    } else {
                        return;
                    }
                }
            });
        }

        function get_rent_periode_date() {
            <%--var rent_periode_date = $('#<%=txt_rent_periode_date.ClientID%>').val();
            $('#<%=txt_expireddate_member.ClientID%>').val(rent_periode_date);--%>
            
        }

        function save_client_click() {
            $('#<%=btn_save_smartcard.ClientID%>').click();
        }

        function back_client_click() {
            $('#<%=btn_back.ClientID%>').click();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <%--    </asp:ScriptManager>--%>
<%--    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    
    <div class="page-header">
        <h1>Smart Card Request <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
        <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" CausesValidation="false" />
        <%--<asp:Button ID="btn_save_smartcard" CssClass="btn btn-info" runat="server" Text="Save" OnClick="btn_save_smartcard_Click" OnClientClick="showLoader()" />--%>
        <%--<asp:Button ID="btn_save_smartcard" CssClass="btn btn-info" runat="server" Text="Save" OnClientClick="if(confirm('Are you sure?')) showLoader(); else return false;" OnClick="btn_save_smartcard_Click"  meta:resourcekey="BtnUserSaveResource1" />--%>
        <asp:Button ID="btn_save_smartcard_client_click" CssClass="btn btn-primary" runat="server" Text="Save" OnClientClick="show_dialog_onsave(); return false" />
        <asp:Button ID="btn_save_smartcard" CssClass="btn btn-primary" runat="server" Text="Save" style="display:none" OnClick="btn_save_smartcard_Click" />
    </div>
    <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Add Smart Card</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <%-- <aspuc:UpdateCase ID="updatecase" runat="server" />--%>
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
    </div>
    
    <div id="errID" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
        <asp:Label ID="lbl_error" runat="server" Visible="false"></asp:Label>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning!</strong> <%=lbl_error.Text%>
    </div>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
                <asp:TextBox ID="txt_TRSmartcardID" CssClass="form-control" runat="server" Visible="false"></asp:TextBox>
            </div>
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
                <%--<asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
                <asp:Button ID="btn_save_smartcard" CssClass="btn btn-info" runat="server" Text="Save" OnClick="btn_save_smartcard_Click" />--%>
            </div>
        </div>
    </div>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label">Card Type</label>
            <div class="col-sm-4">
                <asp:DropDownList ID="ddl_cardtype" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
            </div>
        </div>
    </div>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label">Qty</label>
            <div class="col-sm-4">
                <asp:TextBox ID="txt_quantity" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
            <label class="col-sm-2 control-label">Price</label>
            <div class="col-sm-4">
                <asp:TextBox ID="txt_proce" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label">Resident Status</label>
            <div class="col-sm-4">
                <asp:DropDownList ID="ddl_redident_status" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <label class="col-sm-2 control-label">Total</label>
            <div class="col-sm-4">
                <asp:TextBox ID="txt_total" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <label id="lbl_rent_periode_date" class="col-sm-2 control-label" runat="server" visible="false">Rent Periode Date</label>
            <div class="col-sm-4">
                <asp:TextBox ID="txt_rent_periode_date" CssClass="form-control" runat="server" Visible="false"></asp:TextBox>
            </div>
        </div>
    </div>

    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-4">
            </div>

        </div>
    </div>
    <hr />
    
    <%--    <div id="errID" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
        <asp:Label ID="lbl_error" runat="server" Visible="false"></asp:Label>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning!</strong> <%=lbl_error.Text%>
    </div>--%>
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hiddenID" />
            <div class="panel panel-default">
                <div class="panel-heading">Smart Card Detail <span class="badge"><%=txt_quantity.Text %></span></div>
                <div class="panel-body">
                    <label runat="server" class="control-label" style="color: gray; font-style: oblique">Please Select Cluster</label>
                    <asp:ListBox ID="LB_Cluster" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator_lbbox" runat="server" ControlToValidate="LB_Cluster" ErrorMessage="Cluster is required" Display="Dynamic" CssClass="errormessage" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <hr />

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Smart Card Detail</label>
                        <div class="col-sm-10">
                            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                OnRowDataBound="gvw_data_RowDataBound" AllowPaging="True" OnPageIndexChanging="gvw_data_PageIndexChanging"
                                ShowHeaderWhenEmpty="True" DataKeyNames="SmartcardIdentificationDetailID" PageSize="5">
                                <PagerStyle CssClass="pagination-ys" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chk_selectall" runat="server"
                                                OnCheckedChanged="chk_selectall_CheckedChanged" AutoPostBack="True" />
                                        </HeaderTemplate>
                                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                                        <ItemStyle CssClass="gridview_item_center" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_select" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Card Number" DataField="CardNumber" SortExpression="CardNumber" />
                                    <asp:BoundField HeaderText="Chip Number" DataField="ChipNumber" SortExpression="ChipNumber" />
                                    <asp:BoundField HeaderText="Name" DataField="HolderName" SortExpression="HolderName" />
                                    <%-- <asp:BoundField HeaderText="Card Holdelder" DataField="CardHolderName" SortExpression="CardNumber" />--%>
                                    <%--                                        <asp:BoundField HeaderText="Gender" DataField="Gender" SortExpression="Gender"></asp:BoundField>
                                        <asp:BoundField HeaderText="Birth Place" DataField="BirthPlace" SortExpression="BirthPlace"></asp:BoundField>
                                        <asp:BoundField HeaderText="Birth Date" DataField="Birthdate" SortExpression="Birthdate"
                                            DataFormatString="{0:d MMMM yyyy}"></asp:BoundField>--%>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnk_edit" CssClass="btn btn-default btn-sm" Text="Edit"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No data
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <asp:TextBox ID="txt_formid" runat="server" Style="display: none;"></asp:TextBox>
                            <%--                            <asp:Button ID="btn_add_detail" runat="server" Text="Add Detail" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_add_detail_Click"
                                data-toggle="modal" data-target="#modalFormDetailID" />--%>
<%--                            <asp:Button ID="btn_add_detail" runat="server" Text="Add Detail" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_add_detail_Click"
                                data-toggle="modal" OnClientClick="showModalFormByCardType()" />--%>
                            <asp:HiddenField ID="hdf_cardremaining" runat="server" />
                            <%--<asp:LinkButton  ID="lnkbtn_add_detail" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClientClick="showModalFormByCardType()" OnClick="lnkbtn_add_detail_Click">Add Detail <span class="badge"><%=hdf_cardremaining.Value %></span></asp:LinkButton>--%>
                            <asp:LinkButton  ID="lnkbtn_add_detail" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="lnkbtn_add_detail_Click">Add Detail <span class="badge"><%=hdf_cardremaining.Value %></span></asp:LinkButton>

                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-default"
                                OnClick="btnDelete_Click" UseSubmitBehavior="false" />
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-info" Style="display: none;"
                                OnClick="btnRefresh_Click" UseSubmitBehavior="false" />
                        </div>
                    </div>

                </div>
            </div>
 
            <%--<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>--%>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="modal fade" id="modalFormDetailID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Smart Card ID Detail</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btn_savedetail" runat="server" Text="Save" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_savedetail_Click" />
                            <button type="button" class="btn btn-default" data-dismiss="modal" runat="server" id="btn_close_modal">Close</button>
                            <asp:Button ID="btn_showDetailPersonalID" runat="server" UseSubmitBehavior="false" Style="display: none" OnClick="btn_showDetailPersonalID_Click" />
                            <hr />
                            <%--                            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
                            </div>--%>
                            <asp:HiddenField ID="txt_smartcard_identifier_detail_ID" runat="server" />
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Card Number</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txt_cardnumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Chip Number</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txt_chipnumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Effective Date</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txt_effectivedate" runat="server" CssClass="form-control">dd/MM/yyyy</asp:TextBox>
                                        <asp:CalendarExtender ID="txt_date_CalendarExtender" runat="server" TargetControlID="txt_effectivedate" PopupPosition="TopLeft"
                                            Format="dd MMMM yyyy">
                                        </asp:CalendarExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Expired date</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txt_expireddate" runat="server" CssClass="form-control">dd/MM/yyyy</asp:TextBox>
                                        <asp:CalendarExtender ID="txt_date_CalendarExtender2" runat="server" TargetControlID="txt_expireddate"
                                            Format="dd MMMM yyyy">
                                        </asp:CalendarExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Card Holder Name</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txt_cardholder_name" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cluster Gate</label>
                                    <div class="col-sm-10">
                                        <asp:ListBox ID="list_box_boomgate" CssClass="form-control" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <%-- start form id member --%>
    <div class="modal fade" id="modalFormDetailIDMember" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelMember">Smart Card ID Detail Additional Card</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="Button1" runat="server" Text="Save" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_savedetail_Click" />
                            <button type="button" class="btn btn-default" data-dismiss="modal" runat="server" id="Button2">Close</button>
                            <asp:Button ID="btn_member_cek_card" runat="server" UseSubmitBehavior="false" Style="display: none" OnClick="btn_cek_card_member_Click" OnClientClick="showLoader()" />
                            <hr />
                            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="Div1" visible="false">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <asp:Label ID="lbl_div1" runat="server"></asp:Label>
                            </div>
                            <asp:HiddenField ID="hdf_rent_periode_date_member" runat="server" />
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#memberinformation" aria-controls="memberinformation" role="tab" data-toggle="tab">Additional Card Information</a></li>
                                <li role="presentation"><a id="adt_cardinformation" href="#cardinformation" aria-controls="cardinformation" role="tab" data-toggle="tab">Card Information</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="memberinformation">
                                    <br />
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Name</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_name_member" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">ID Card Number</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_idcardnumber_member" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Office Address</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_officeaddress_member" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Mobile Phone</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_mobilephone_member" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-8">
                                                <input type="button" id="Button3" class="btn btn-default pull-right" onclick="selecttab(this)" value="next" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- access card tab --%>
                                <div role="tabpanel" class="tab-pane" id="cardinformation">
                                    <br />
                                    <asp:Label ID="lbl_error_message_member" runat="server" Text="" Visible="false"></asp:Label>
                                    <div id="error_meesage_member" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
                                        <asp:Label ID="Label3" runat="server" Visible="false"></asp:Label>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <strong>Warning!</strong><%=lbl_error_message_member.Text %>
                                    </div>
                                    <div class="form-horizontal" id="search_form_member" runat="server">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Search <span class="glyphicon glyphicon-search" aria-hidden="true"></span></label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_search_member" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txt_search_member_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">CardNumber</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_cardnumber_member" runat="server" CssClass="form-control"></asp:TextBox>
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator_cardnumber_member" runat="server" ControlToValidate="txt_cardnumber_member" ErrorMessage="Card number is required" Display="Dynamic" CssClass="errormessage" SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Chip Number</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_chipnumber_member" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal" id="divcardstatusmember" style="display: none" runat="server">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Card Status</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_cardstatus_member" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Vehicle Number</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_vehiclenumber_member" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txt_vehiclenumber_member_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Vehicle Type</label>
                                            <div class="col-sm-8">
                                                <asp:DropDownList ID="ddl_vehicletype" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Vehicle Brand</label>
                                            <div class="col-sm-8">
                                                <%--<asp:DropDownList ID="ddl_vehiclebrand" runat="server" CssClass="form-control"></asp:DropDownList>--%>
                                                <asp:TextBox ID="txt_vehiclebrand_member" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Effective Date</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_effectivedate_member" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txt_effectivedate_member_TextChanged"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender_effectivedate_member" runat="server" TargetControlID="txt_effectivedate_member" PopupPosition="TopLeft"
                                                    Format="dd MMMM yyyy">
                                                </asp:CalendarExtender>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Expired Date</label>
                                            <div class="col-sm-8">
                                                <asp:TextBox ID="txt_expireddate_member" runat="server" ReadOnly="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <%-- end form id member --%>

    <%-- start form id resident --%>
    <div class="modal fade" id="modalFormDetailIDResident" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <%--       <a class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>--%>
                    <h4 class="modal-title" id="myModalLabelResident">Smart Card ID Detail Resident</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="Button4" runat="server" Text="Save" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_savedetail_Click" />
                            <button type="button" class="btn btn-default" data-dismiss="modal" runat="server" id="Button5">Close</button>
                            <asp:Button ID="Button6" runat="server" UseSubmitBehavior="false" Style="display: none" OnClick="btn_showDetailPersonalID_Click" />

                            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="Div2" visible="false">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                            </div>
                            <asp:HiddenField ID="HiddenField2" runat="server" />
                            <hr />
                            <asp:Label ID="lbl_error_message_resident" runat="server" Text="" Visible="false"></asp:Label>
                            <div id="error_meesage_resident" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
                                <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Warning! </strong><%=lbl_error_message_resident.Text %>
                            </div>
                            <div class="form-horizontal" id="search_form_resident" runat="server">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Search <span class="glyphicon glyphicon-search" aria-hidden="true"></span></label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_search_resident" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txt_search_resident_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Card Number</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_cardnumber_resident" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txt_cardnumber_resident_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal" id="divcardstatusresident" style="display: none" runat="server">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Card Status</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_cardstatus_resident" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Chip Number</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_chipnumber_resident" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Vehicle Number</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_vehiclenumber_resident" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txt_vehiclenumber_resident_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Vehicle Type</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddl_vehicletype_resident" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Vehicle Brand</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_vehiclebrand_resident" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Effective Date</label>
                                    <div class="col-sm-8">
                                        <%--<asp:TextBox ID="txt_effectivedate_resident" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txt_effectivedate_resident_TextChanged"></asp:TextBox>--%>
                                        <asp:TextBox ID="txt_effectivedate_resident" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txt_effectivedate_resident_TextChanged"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender_effectivedate_resident" runat="server" TargetControlID="txt_effectivedate_resident" PopupPosition="TopLeft"
                                            Format="dd MMMM yyyy">
                                        </asp:CalendarExtender>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <asp:Label ID="lbl_expireddate_resident" runat="server" CssClass="col-sm-4 control-label" Text="Expired Date" Visible="false"></asp:Label>
                                    <%--<label id="lbl_expireddate_resident" class="col-sm-4 control-label">Expired Date</label>--%>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_expireddate_resident" runat="server" Enabled="false" CssClass="form-control" Visible="false"></asp:TextBox>
                                    </div>
                                </div>
                            </div>


                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <%-- end form id resident --%>
    <div id="navwrap">
        <nav class="navbar navbar-default navbar-fixed-buttom">
                <ul class="nav navbar-nav pull-right">
                    <li>
                        <p class="navbar-btn" style="padding-right:12px">
                            <asp:LinkButton ID="lnkbtn_back" runat="server" OnClientClick="back_client_click();" UseSubmitBehavior="false"  CssClass="btn btn-default"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> &nbsp; Back</asp:LinkButton>
                        </p>
                    </li>
                    <li>
                        <p class="navbar-btn" style="padding-right:12px">
                            <asp:LinkButton ID="lnkbtn_save" runat="server" OnClientClick="show_dialog_onsave(); return false" UseSubmitBehavior="false" CssClass="btn btn-primary">Save &nbsp;<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></asp:LinkButton>
                        </p>
                    </li>
                </ul>
        </nav>
    </div>




    <script src="../scripts/bootstrap-dialog.min.js"></script>
</asp:Content>


