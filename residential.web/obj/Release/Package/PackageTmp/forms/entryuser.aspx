﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entryuser.aspx.cs" Inherits="residential.web.forms.entryuser" MasterPageFile="~/main.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
        <%--<Scripts>
            <asp:ScriptReference Path="~/scripts/bootstrap-multiselect.css" />
            <asp:ScriptReference Path="~/scripts/bootstrap-multiselect.js" />
            <asp:ScriptReference Path="~/scripts/bootstrap.min.css" />
            <asp:ScriptReference Path="~/scripts/bootstrap.min.js" />
        </Scripts>--%>
    </asp:ScriptManager>

    <script type="text/javascript">
        $(function () {
            $('[id=lstSubLocation]').multiselect({
                includeSelectAllOption: true
            });
        });
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>Form User</h1>
            </div>
            <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" />
            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-default" NavigateUrl="~/forms/listuser.aspx">Back</asp:HyperLink>
            <hr />
            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txt_username" runat="server" CssClass="form-control"
                        AutoPostBack="True" OnTextChanged="txt_username_TextChanged" placeholder="domain\username"></asp:TextBox>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Full Name</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txt_fullname" runat="server" Enabled="False" CssClass="form-control" placeholder="full name"></asp:TextBox>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Mobile Number</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txt_mobilenumber" runat="server" CssClass="form-control" MaxLength="15" placeholder="mobile number"></asp:TextBox>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email Address</label>
                    <div class="col-sm-10">
                        <asp:TextBox ID="txt_emailaddress" runat="server" CssClass="form-control" placeholder="email address" Type="email"></asp:TextBox>
                    </div>                    
                </div>
                <hr />
                <div class="form-group">
                    <label class="col-sm-2 control-label">Role</label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="cbo_roles" runat="server" CssClass="form-control" AutoPostBack="true"
                        OnSelectedIndexChanged="cbo_roles_SelectedIndexChanged">
                    </asp:DropDownList>
                    </div>  
                    <label class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="cbo_departments" runat="server"
                        CssClass="form-control" Enabled="false">
                    </asp:DropDownList>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Site</label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="cbo_site" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="cbo_site_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <label class="col-sm-2 control-label">PIC Tower</label>
                    <div class="col-sm-4">
                        <asp:DropDownCheckBoxes ID="ddlSubLocation" runat="server" OnSelectedIndexChanged="ddlSubLocation_SelectedIndexChanged"
                            AddJQueryReference="True" UseSelectAllNode="True" AutoPostBack="true">
                            <style DropDownBoxCssClass="form-control" SelectBoxCssClass="form-control"/>
                            <Texts SelectBoxCaption="Select Cluster/Tower"/>
                        </asp:DropDownCheckBoxes>
                        <%--<asp:ListBox ID="lstSubLocation" runat="server" SelectionMode="Multiple"></asp:ListBox>--%>
                        <asp:HiddenField ID="hfSubLocation" runat="server" />
                        <asp:HiddenField ID="hfSubLocationAll" runat="server" />
                    </div>
                    <%--<label class="col-sm-2 control-label">Default Site</label>
                    <div class="col-sm-4">
                        <asp:DropDownList ID="cbo_defaultsite" runat="server" CssClass="form-control" Enabled="false">
                    </asp:DropDownList>
                    </div>          --%>           
                </div>
            </div>                                   
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
