﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="printresidentcard.aspx.cs" Inherits="residential.web.forms.printresidentcard" %>

<html>
<head>
<title>Print Card</title>
    <style>
                @page 
        {
            size: auto;   /* auto is the current printer page size */
            margin: 0mm;  /* this affects the margin in the printer settings */
        }
 body {
    margin: 0;
}
        .container {
	        /*float:left;*/
	        /*border : 1px solid black;*/
	        margin:0px 0px 0px 0px;
	        /*max-width:270px;*/
             max-width:30%;
	        padding:0px;

        }
        .img{
	        border : 1px solid black;
	        height:50px;
	        width:50px;
	        float:right;
	        margin:5px;
        }

    @media print{
         .container {
	        /*float:left;*/
	        /*border : 1px solid black;*/
	        margin:0px 0px 0px 0px;
	        /*max-width:270px;*/
            max-width:30%;
	        padding:0px;

        }
		.front-card{
            width:470px; height:255px; overflow:hidden; /*background:url(card.jpg) no-repeat center center; background-size:100% 100%; position:relative; -moz-border-radius:20px; -webkit-border-radius:20px; border-radius:20px;*/
			-webkit-print-color-adjust: exact !important;  
			color-adjust: exact !important;                
        }
		.back-card{
            width:431px; height:255px; overflow:hidden; /*background:url(card.jpg) no-repeat center center; background-size:100% 100%; position:relative; -moz-border-radius:20px; -webkit-border-radius:20px; border-radius:20px;*/
			-webkit-print-color-adjust: exact !important;  
			color-adjust: exact !important;                
        }
        .text-white{
            font-family:Arial, Helvetica, sans-serif; color:#ffffff !important;
            -webkit-print-color-adjust: exact !important;  
			color-adjust: exact !important; 
        }
        .text-black{
            font-family:Arial, Helvetica, sans-serif; color:#424243 !important;
            -webkit-print-color-adjust: exact !important;  
			color-adjust: exact !important; 
        }
		/*#print-area{
			width:100%;margin:auto;padding-top:60px;
		}
        .card-container{
            width:30%;
        }*/
	}
    @media print and (color) {
	   * {
		  -webkit-print-color-adjust: exact;
		  print-color-adjust: exact;
	   }
	}
    </style>

<%--    <style>
        @page {
    /* dimensions for the whole page */
    size: A5;
    
    margin: 0;
}

html {
    /* off-white, so body edge is visible in browser */
    background: #eee;
}

body {
    /* A5 dimensions */
    height: 210mm;
    width: 148.5mm;
	
    /* A6 dimensions */
    /*height: 148mm;
    width: 105mm;*/	

    margin: 0;
}

/* fill half the height with each face */
.face {
    height: 50%;
    width: 100%;
    position: relative;
}

/* the back face */
.face-back {
    background: #f6f6f6;
}

/* the front face */
.face-front {
    background: #fff;
}

/* an image that fills the whole of the front face */
.face-front img {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    width: 100%;
    height: 100%;
}
.face-back img {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    width: 100%;
    height: 100%;
}

    </style>--%>
<%--	 <script type="text/javascript">
	     function printcard() {
	         var fronturl = document.getElementById("lblFrontCard").innerHTML;
	         var backurl = document.getElementById("lblFrontCard").innerHTML;

	         document.getElementById("btn-print").style.display = 'none';
	         //setFrontCard(fronturl);
	         //setBackCard(backurl);
	         this.window.print();
	     }


	</script>--%>
</head>
<body>
        <div id="print-area" class="container">
			<div ID="frontCard" class="front-card" runat="server" style="width:470px; height:290px; overflow:hidden; background-size:100% 100%; position:relative; -moz-border-radius:20px; -webkit-border-radius:20px; border-radius:20px;">
                	<div style="position:absolute; left:30px; top:90px;">
						<div style="width:120px; height:150px">                            
                            <asp:Image ID="photo" runat="server" style="width:100px; height:130px" src="" />
                        </div>
					</div>		
					
					<div style="position:absolute; left:180px; top:105px;">
						<h2 class="text-white" style="font-size:18px; margin-bottom:-10; line-height:25px; font-family:Arial, Helvetica, sans-serif; color:#ffffff;">&nbsp;<strong><asp:Label ID="lblCardNo" runat="server" Text="Label"></asp:Label></strong>
						<h2 class="text-black" style="font-size:18px; margin-bottom:-10; line-height:25px; font-family:Arial, Helvetica, sans-serif; color:#424243;">&nbsp;<strong><asp:Label ID="lblCardName" runat="server" Text="Label"></asp:Label></strong>
						<h2 class="text-black" style="font-size:18px; margin-bottom:-20; line-height:25px; font-family:Arial, Helvetica, sans-serif; color:#424243;">&nbsp;<strong><asp:Label ID="lblCluster" runat="server" Text="Label"></asp:Label></strong>
						<h3 class="text-white" style="font-size:18px; margin-bottom:0; line-height:25px; font-family:Arial, Helvetica, sans-serif; color:#ffffff;">&nbsp;<strong><asp:Label ID="lblExpired" runat="server" Text="Label"></asp:Label></strong>
					</div>					
						
			</div>
        </div>
        <div class="container">
            <div ID="backCard" runat="server" class="back-card" style="width:470px; height:290px; overflow:hidden; background-size:100% 100%; position:relative; -moz-border-radius:20px; -webkit-border-radius:20px; border-radius:20px;"></div>
        </div>
        <div style="clear:left">
            <asp:Label ID="lblFrontCard" runat="server" Text=""></asp:Label>
            <br />
            <asp:Label ID="lblBackCard" runat="server" Text=""></asp:Label>
        </div>
<%--    	<div style="clear:left; padding-top:20px">
            <input type="button" id="btn-print" style="border:none; width:100px; height:50px; margin-left:20px;" value="Print" onclick="printcard();" />
        </div>--%>


<%--    <div class="face face-front">
        <asp:Image runat="server" ID="frontCard" />
                	<div style="position:absolute; left:30px; top:200px;">
						<div style="width:120px; height:200px">                            
                            <asp:Image ID="photo" runat="server" style="width:120px; height:150px" src="" />
                        </div>
					</div>		
					
					<div style="position:absolute; left:180px; top:200px;">
						<h2 class="text-white" style="font-size:20px; margin-bottom:-10; line-height:25px; font-family:Arial, Helvetica, sans-serif; color:#ffffff;">&nbsp;<strong><asp:Label ID="lblCardNo" runat="server" Text="Label"></asp:Label></strong>
						<h2 class="text-black" style="font-size:20px; margin-bottom:-10; line-height:25px; font-family:Arial, Helvetica, sans-serif; color:#424243;">&nbsp;<strong><asp:Label ID="lblCardName" runat="server" Text="Label"></asp:Label></strong>
						<h2 class="text-black" style="font-size:20px; margin-bottom:-20; line-height:25px; font-family:Arial, Helvetica, sans-serif; color:#424243;">&nbsp;<strong><asp:Label ID="lblCluster" runat="server" Text="Label"></asp:Label></strong>
						<h3 class="text-white" style="font-size:20px; margin-bottom:0; line-height:25px; font-family:Arial, Helvetica, sans-serif; color:#ffffff;">&nbsp;<strong><asp:Label ID="lblExpired" runat="server" Text="Label"></asp:Label></strong>
					</div>
    </div>
    <div class="face face-back">
        <asp:Image runat="server" ID="backCard"/>
    </div>--%>
</body>
</html>
