﻿$(function () {

});


function load_upload_file_js() {
    $('#ContentPlaceHolder1_CheckBox_identitasdiri').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_identitasdiri').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_identitasdiri').attr("disabled", "disabled")
    });

    $('#ContentPlaceHolder1_CheckBox_kk').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_kk').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_kk').attr('disabled', 'disabled')
    });

    $('#ContentPlaceHolder1_CheckBox_stnk').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_stnk').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_stnk').attr('disabled', 'disabled')
    });

    $('#ContentPlaceHolder1_CheckBox_kepemilikanunit').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_kepemilikanunit').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_kepemilikanunit').attr('disabled', 'disabled')
    });

    $('#ContentPlaceHolder1_CheckBox_perjanjiansewamenyewaunit').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_perjanjiansewamenyewaunit').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_perjanjiansewamenyewaunit').attr('disabled', 'disabled')
    });

    $('#ContentPlaceHolder1_CheckBox_suratketerangankaryawan').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_suratketerangankaryawan').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_suratketerangankaryawan').attr('disabled', 'disabled')
    });

    $('#ContentPlaceHolder1_CheckBox_buktibayarutilitas').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_buktibayarutilitas').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_buktibayarutilitas').attr('disabled', 'disabled')
    });

    $('#ContentPlaceHolder1_CheckBox_suratkuasa').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_suratkuasa').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_suratkuasa').attr('disabled', 'disabled')
    });

    $('#ContentPlaceHolder1_CheckBox_ktppemberipenerimakuasa').change(function () {
        if (this.checked)
            $('#ContentPlaceHolder1_FileUpload_ktppemberipenerimakuasa').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_ktppemberipenerimakuasa').attr('disabled', 'disabled')
    });

    $('#ContentPlaceHolder1_txt_lainlain').change(function () {
        var stringlength = $('#ContentPlaceHolder1_txt_lainlain').val();
        if (stringlength.length > 0)
            $('#ContentPlaceHolder1_FileUpload_lainlain').removeAttr('disabled')
        else
            $('#ContentPlaceHolder1_FileUpload_lainlain').attr('disabled', 'disabled')
    });

}

//start view smartcard
function load_download_file_js() {
    $('#ContentPlaceHolder1_hpl_identitasdiri').click(function () {

        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_identitasdiri_type').val();
            var fileType = getExtentionFile(contentType);
            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_identitasdiri_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_identitasdiri').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_identitasdiri_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_identitasdiri').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    $('#ContentPlaceHolder1_hpl_kk').click(function () {

        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_kk_type').val();
            var fileType = getExtentionFile(contentType);
            var allowedFileType = getAllowedImageExtention(fileType)
            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_kk_base64').val();
                img = document.getElementById('myImg').src = "data:image/jpg;base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_kk').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                var b = $('#hdf_db_field').val();
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_kk_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var base64string = $('#ContentPlaceHolder1_hdf_kk_base64').val();
                var captionvalue = $('#ContentPlaceHolder1_hpl_kk').text();
                var modalFile = document.getElementById("file01");
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                var b = $('#hdf_db_field').val();
                $('#myModalFile').css('display', 'block');
                modalFile.src = imageFile;
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    //$('#ContentPlaceHolder1_hpl_kk').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    //$('#ContentPlaceHolder1_hpl_stnk').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");

    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    $('#ContentPlaceHolder1_hpl_stnk').click(function () {
        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_stnk_type').val();
            var fileType = getExtentionFile(contentType);

            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_stnk_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_stnk').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_stnk_base64_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_stnk').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    //$('#ContentPlaceHolder1_hpl_kepemilikanunit').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");

    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    $('#ContentPlaceHolder1_hpl_kepemilikanunit').click(function () {
        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_kepemilikanunit_type').val();
            var fileType = getExtentionFile(contentType);

            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_kepemilikanunit_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_kepemilikanunit').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_kepemilikanunit_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_kepemilikanunit').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    //$('#ContentPlaceHolder1_hpl_perjanjiansewamenyewaunit').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");

    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    $('#ContentPlaceHolder1_hpl_perjanjiansewamenyewaunit').click(function () {
        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_perjanjiansewamenyewaunit_type').val();
            var fileType = getExtentionFile(contentType);

            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_perjanjiansewamenyewaunit_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_perjanjiansewamenyewaunit').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_perjanjiansewamenyewaunit_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_perjanjiansewamenyewaunit').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    //$('#ContentPlaceHolder1_hpl_suratketerangankaryawan').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");

    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    $('#ContentPlaceHolder1_hpl_suratketerangankaryawan').click(function () {
        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_suratketerangankaryawan_type').val();
            var fileType = getExtentionFile(contentType);

            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_suratketerangankaryawan_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_suratketerangankaryawan').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_suratketerangankaryawan_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_suratketerangankaryawan').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    //$('#ContentPlaceHolder1_hpl_buktibayarutilitas').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");

    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    $('#ContentPlaceHolder1_hpl_buktibayarutilitas').click(function () {
        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_buktibayarutilitas_type').val();
            var fileType = getExtentionFile(contentType);

            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_buktibayarutilitas_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_buktibayarutilitas').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_buktibayarutilitas_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_buktibayarutilitas').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    //$('#ContentPlaceHolder1_hpl_suratkuasa').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");

    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    $('#ContentPlaceHolder1_hpl_suratkuasa').click(function () {
        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_suratkuasa_type').val();
            var fileType = getExtentionFile(contentType);

            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_suratkuasa_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_suratkuasa').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_suratkuasa_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_suratkuasa').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    //$('#ContentPlaceHolder1_hpl_ktppemberipenerimakuasa').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");

    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    $('#ContentPlaceHolder1_hpl_ktppemberipenerimakuasa').click(function () {
        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_ktppemberipenerimakuasa_type').val();
            var fileType = getExtentionFile(contentType);

            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_ktppemberipenerimakuasa_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_ktppemberipenerimakuasa').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_ktppemberipenerimakuasa_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_ktppemberipenerimakuasa').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

    //$('#ContentPlaceHolder1_hpl_other').click(function () {
    //    var hpl_value = $(this).text();
    //    var hpl_ID = $(this).attr("id");
    //    var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");

    //    $('#hdf_db_field').val("");

    //    if (hpl_value.toLowerCase() != 'none') {
    //        $('#hdf_db_field').val(dbField);
    //        var b = $('#hdf_db_field').val();
    //        $('#ContentPlaceHolder1_btn_download_file').click();
    //    }
    //});

    $('#ContentPlaceHolder1_hpl_other').click(function () {
        var hpl_value = $(this).text();
        var hpl_ID = $(this).attr("id");
        var dbField = hpl_ID.replace("ContentPlaceHolder1_hpl_", "");
        $('#hdf_db_field').val("");

        if (hpl_value.toLowerCase() != 'none') {
            var contentType = $('#ContentPlaceHolder1_hdf_other_type').val();
            var fileType = getExtentionFile(contentType);

            var allowedFileType = getAllowedImageExtention(fileType)

            if (allowedFileType) {
                var base64string = $('#ContentPlaceHolder1_hdf_other_base64').val();
                img = document.getElementById('myImg').src = "data:image/" + fileType + ";base64, " + base64string + "";
                var imageFile = $('#myImg').attr("src");
                var captionvalue = $('#ContentPlaceHolder1_hpl_other').text();

                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("caption");

                $('#hdf_db_field').val(dbField);
                $('#myModalImage').css('display', 'block');
                modalImg.src = imageFile;
                captionText.innerHTML = captionvalue;
            }
            else if (fileType == "pdf") {
                var base64string = $('#ContentPlaceHolder1_hdf_other_base64').val();
                var pdfText = base64string;
                var winlogicalname = "detailPDF";
                var winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,' +
                            'resizable,screenX=50,screenY=50,width=850,height=1050';

                var htmlText = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + pdfText
                                 + '"></embed>';

                // Open PDF in new browser window
                var detailWindow = window.open("", "", winparams);
                detailWindow.document.write(htmlText);
                detailWindow.document.close();
            }
            else {
                var captionvalue = $('#ContentPlaceHolder1_hpl_other').text();
                var captionText = document.getElementById("captionfile");

                $('#hdf_db_field').val(dbField);
                $('#myModalFile').css('display', 'block');
                captionText.innerHTML = captionvalue;

                $('#file01').click(function () {
                    $('#ContentPlaceHolder1_btn_download_file').click();
                });
            }
        }
    });

}

function getExtentionFile(contentType) {
    var value = contentType.split('/');
    return value[1];
}

function getAllowedImageExtention(extentionFileName) {
    var allowedFileExtention = ["jpg", "png", "jpeg"];
    var allowed = false;
    for (i = 0; i < allowedFileExtention.length; i++) {
        if (allowedFileExtention[i] == extentionFileName) {
            allowed = true;
            break;
        }
    }
    return allowed;
}

//end view smartcard