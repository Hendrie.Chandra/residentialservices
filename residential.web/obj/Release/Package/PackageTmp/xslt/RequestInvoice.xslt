﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <h1 style="text-align: center;">
      <strong>Town Management</strong>
    </h1>
    <p style="text-align: center;">
      <strong>Customer Invoice</strong>
    </p>
    <xsl:for-each select="DocumentElement/Flyer">
      <p style="text-align: center;">
        <xsl:value-of select="CaseNumber"/>
      </p>
      <hr />
      <table border="0px" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
          <tr>
            <td>Nama Pelapor</td>
            <td>
              : <xsl:value-of select="NamaPelapor"/>
            </td>
            <td>Unit</td>
            <td>
              : <xsl:value-of select="UnitCode"/> - <xsl:value-of select="UnitNo"/>
            </td>
          </tr>
          <tr>
            <td>Telepon Pelapor</td>
            <td>
              : <xsl:value-of select="TeleponPelapor"/>
            </td>
            <td>Email Pelapor</td>
            <td>
              : <xsl:value-of select="EmailPelapor"/>
            </td>
          </tr>
          <tr>
            <td>Description</td>
            <td colspan="3">
              : <xsl:value-of select="Description"/>
            </td>
          </tr>
        </tbody>
      </table>
      <hr />
      <table border="1px solid black" cellpadding="1" cellspacing="0" style="width: 100%;">
        <tbody>
          <tr style="background-color:#DEDEDE;">
            <td style="text-align: center;">
              <strong>R​equest​</strong>
            </td>
            <td style="text-align: center;">
              <strong>Price</strong>
            </td>
            <td style="text-align: center;">
              <strong>Quantity</strong>
            </td>
            <td style="text-align: center;">
              <strong>Total Price</strong>
            </td>
          </tr>
          <tr>
            <td>
              <xsl:value-of select="HelpName"/>
            </td>
            <td>
              <xsl:value-of select="Price"/>
            </td>
            <td>
              <xsl:value-of select="NoOfFlyer"/>
            </td>
            <td>
              <xsl:value-of select="Amount"/>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border-bottom: 0px solid white;"></td>
            <td style="text-align: center;">
              <strong>Adjustment</strong>
            </td>
            <td>
              <xsl:value-of select="DiscountAmount"/>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border-bottom: 0px solid white;"></td>
            <td style="text-align: center;">
              <strong>PPN</strong>
            </td>
            <td>
              <xsl:value-of select="TaxAmount"/>
            </td>
          </tr>
          <tr>
            <td colspan="2"></td>
            <td style="text-align: center;">
              <strong>Total</strong>
            </td>
            <td>
              <xsl:value-of select="ActualAmount"/>
            </td>
          </tr>
        </tbody>
      </table>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>