﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="PluginErrorLogViewer.aspx.cs" Inherits="residential.web.PluginErrorLogViewer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>Plugin Error Log</h1>
    </div>    
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Button ID="btn_Refresh" runat="server" Text="Refresh" OnClick="btn_Refresh_Click" CssClass="btn btn-primary" />
            <hr />
            <asp:GridView ID="gvw_data" runat="server" CssClass="table table-bordered table-hover" AutoGenerateColumns="false"
                AllowPaging="true" OnPageIndexChanging="gvw_data_PageIndexChanging" PagerStyle-CssClass="pagination-ys">
                <Columns>
                    <asp:TemplateField HeaderText="No" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# (Container.DataItemIndex + 1).ToString() %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="LogTime" HeaderText="Time" SortExpression="LogTime"></asp:BoundField>
                    <asp:BoundField DataField="InitiatingUser" HeaderText="Initiating User" SortExpression="InitiatingUser"></asp:BoundField>
                    <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message"></asp:BoundField>
                    <asp:BoundField DataField="StackTrace" HeaderText="Stack Trace" SortExpression="StackTrace"></asp:BoundField>
                    <asp:BoundField DataField="InnerExceptionMessage" HeaderText="Inner Exception Message" SortExpression="InnerMessage"></asp:BoundField>
                    <asp:BoundField DataField="InnerExceptionStackTrace" HeaderText="Inner Exception Stack Trace" SortExpression="InnerExceptionStackTrace"></asp:BoundField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
