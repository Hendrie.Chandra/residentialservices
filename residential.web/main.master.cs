﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.DirectoryServices.AccountManagement;

using residential.libs;
using System.Security.Principal;

using System.Data;
using System.Web.Security;
using System.Threading;

using residential.web.Classes;

namespace residential.web
{
    public partial class main : System.Web.UI.MasterPage
    {
        #region Commented to optimize code
        /*
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Header.DataBind();
                WindowsPrincipal wp = (WindowsPrincipal)Thread.CurrentPrincipal;                

                if (wp != null)
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);
                    String username = wp.Identity.Name;
                                                           
                    DataTable dt = dbclass.retrieve_user(username);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dru in dt.Rows)
                        {
                            DataTable dts = dbclass.retrieve_site(int.Parse(dru["SiteID"].ToString()));
                            DataRow drs = dts.Rows[0];

                            ddl_site.Items.Add(new ListItem(drs["SiteName"].ToString(), drs["SiteID"].ToString()));
                        }

                        DataRow dr = dt.Select("SiteID = " + dt.Rows[0]["DefaultSiteID"].ToString()).First();

                        //DataTable dtsite = dbclass.retrieve_site((Int32)dr["SiteID"], 0);
                        //if (dtsite.Rows.Count > 0)
                        //{
                        //    foreach (DataRow row in dtsite.Rows)
                        //    {
                        //        ListItem item = new ListItem(row["SiteName"].ToString(), row["SiteID"].ToString());
                        //        ddl_site.Items.Add(item);
                        //    }
                        //}
                        //else
                        //{
                        //    ListItem item = new ListItem(dr["SiteName"].ToString(), dr["SiteID"].ToString());
                        //    ddl_site.Items.Add(item);
                        //}

                        ddl_site.SelectedValue = dr["DefaultSiteID"].ToString();                                               
                        lbl_user.Text = dr["FullName"].ToString();                        

                        //if ((Boolean)dr["MultipleSite"])
                        //    ddl_site.Enabled = true;
                        //else
                        //    ddl_site.Enabled = false;

                        String[] MenuID = { "SystemAdministration", "DataMaster", "Tasklist", "CommunityDevelopment", "BuildingControl", "WaterTreatmentPlant", "TenantRelationship", "Environment" };
                        DataTable dt_role = dbclass.retrieve_roleaccess(Int32.Parse(dr["RoleID"].ToString()));
                        var i = 0;
                        foreach (DataColumn dc in dt_role.Columns)
                        {
                            Boolean access = (Boolean)dt_role.Rows[0][dc];
                            Control menu = FindControl(MenuID[i]);
                            if (!access)
                                menu.Visible = false;
                            i++;
                        }
                    }
                    else
                    {
                        Response.Redirect("~/401_unauthorized.aspx");
                    }
                }
            }
        }
         * */
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Page.Header.DataBind();
                WindowsPrincipal wp = (WindowsPrincipal)Thread.CurrentPrincipal;

                if (wp != null)
                {
                    DataSet data = clsUser.GetUserAccessByUsername(Helper.GetUserName());
                    DataTable dtAccessibleSites = data.Tables[0].DefaultView.ToTable(true,"SiteID", "SiteName", "DefaultSiteID", "FullName"); // Add by Andy to distinct SiteID
                    DataTable dtAccessibleMenus = data.Tables[1];

                    if (dtAccessibleSites.Rows.Count > 0)
                    {
                        ddl_site.DataSource = dtAccessibleSites;
                        ddl_site.DataValueField = "SiteID";
                        ddl_site.DataTextField = "SiteName";
                        ddl_site.DataBind();

                        ddl_site.SelectedValue = dtAccessibleSites.Rows[0]["DefaultSiteID"].ToString();
                        lbl_user.Text = dtAccessibleSites.Rows[0]["FullName"].ToString();

                        foreach (DataRow row in dtAccessibleMenus.Rows)
                        {
                            Control menu = FindControl(row["MenuName"].ToString());
                            if (menu != null)
                            {
                                menu.Visible = true;
                            }
                            //Control menuDivider = FindControl
                        }
                    }
                    else
                    {
                        Response.Redirect("~/401_unauthorized.aspx");
                    }
                }
            }
        }

        protected void ddl_site_SelectedIndexChanged(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = new UserData(HttpContext.Current.User.Identity.Name);            

            Int32 SiteID = Int32.Parse(ddl_site.SelectedValue);
            HttpContext.Current.Session[Constants.SessionSiteID] = ddl_site.SelectedValue;

            dbclass.save_defaultusersite(userData.UserID, SiteID, userData.UserName);
            
            Response.Redirect("~/welcome.aspx");
        }        
    }
}