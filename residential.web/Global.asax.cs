﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Data;

using residential.web.Classes;

namespace residential.web
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {            
            Bundle javaScriptBundle = new ScriptBundle("~/javaScriptBundle").Include(
                "~/scripts/jquery-{version}.js",
                "~/scripts/bootstrap.js",
                "~/scripts/bootstrap-datepicker.js",
                "~/scripts/Site.js");

            Bundle styleSheetBundle = new StyleBundle("~/styleSheetBundle").Include(
                "~/content/bootstrap.css",
                "~/content/bootstrap.min.css",
                "~/content/Site.css",
                "~/content/datepicker.css");

            BundleTable.Bundles.Add(javaScriptBundle);
            BundleTable.Bundles.Add(styleSheetBundle);
            BundleTable.EnableOptimizations = true;

            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Session[Constants.SessionUser] = clsUser.GetUserAccessByUsername(Helper.GetUserName());
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            
            dbclass.Save_ErrorLog(Server.GetLastError(), User.Identity.Name);
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}
