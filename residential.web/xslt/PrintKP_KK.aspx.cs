﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Xsl;
using System.Text;
using System.IO;
using System.Configuration;
using residential.libs;

namespace residential.web.xslt
{
    public partial class PrintKP_KK : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserData userData = new UserData(User.Identity.Name);

                String CaseNumber = Request.QueryString["CaseNumber"].ToString();

                DataTable dt = new DataTable();                

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString());
                SqlCommand cmd = new SqlCommand("SP_Retrieve_Personal_Identification", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);

                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(dt);
                conn.Close();

                DataTable dt2 = new DataTable();
                dt2.TableName = "PersonalID";
                
                SqlCommand cmd2 = new SqlCommand("SP_Retrieve_PersonalIdentificationDetail", conn);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@PersonalIdentificationID", dt.Rows[0]["PersonalIdentificationID"].ToString());

                conn.Open();
                SqlDataAdapter adapter2 = new SqlDataAdapter(cmd2);
                adapter2.Fill(dt2);
                conn.Close();

                String Patriarch = "";
                DataRow[] patriarchs = dt2.Select("RelationID = 1");
                if (patriarchs.Count() > 0)
                {
                    DataTable dt_pat = new DataTable();
                    dt_pat = dt2.Clone();
                    dt_pat.TableName = "Patriarch";
                    dt_pat.Rows.Clear();
                    dt_pat.ImportRow(patriarchs[0]);

                    System.IO.StringWriter writer_pat = new System.IO.StringWriter();
                    dt_pat.WriteXml(writer_pat);
                    Patriarch = writer_pat.ToString().Replace("</DocumentElement>", "").Replace("<DocumentElement>", "");
                }

                String PrintBy = "<PrintBy>" + userData.FullName + "</PrintBy>\r\n  ";
                String PrintDate = "<PrintDate>" + DateTime.Now.Date.ToString("dd MMMM yyyy") + "</PrintDate>\r\n  ";

                System.IO.StringWriter writer = new System.IO.StringWriter();
                dt2.WriteXml(writer);
                string results = writer.ToString();
                results = results.Replace("</DocumentElement>", PrintBy + PrintDate + Patriarch + "</DocumentElement>");

                XmlDocument result = new XmlDocument();
                result.LoadXml(results);

                string strXSLTFile = string.Empty;
                strXSLTFile = Server.MapPath("~/xslt/ktp_kk.xslt");

                XslCompiledTransform objXSLTransform = new XslCompiledTransform();
                objXSLTransform.Load(strXSLTFile);
                StringBuilder htmlOutput = new StringBuilder();
                TextWriter htmlWriter = new StringWriter(htmlOutput);
                objXSLTransform.Transform(result, null, htmlWriter);
                Literal1.Text = htmlOutput.ToString();
            }
        }
    }
}