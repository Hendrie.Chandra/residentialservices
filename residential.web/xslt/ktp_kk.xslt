<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:date="http://exslt.org/dates-and-times">

<xsl:template match="/">
<html>
<head>	
	<title>Print</title>
  <style type="text/css" media="print">
    .page
    {
    -webkit-transform: rotate(-90deg); -moz-transform:rotate(-90deg);
    filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
    }
  </style>
	<style>
    th {
    text-align: center;
    }
    table, tr, th, td {
    border-spacing: 0;
    border-collapse: collapse;
    border: 1px solid #000000;
    }
    .footer {
    position : absolute;
    bottom : 10px;
    margin-top : 40px;
    width:100%;
    text-align: center;
    font-weight: bold;
    }
  </style>
</head>
<body>
  <div style="text-align: right; font-size:x-small;">
    Print Date : <xsl:value-of select="//PrintDate"/><br />
    Print By :<xsl:value-of select="//PrintBy"/>
  </div>
	<div style="text-align: center; font-weight: bold;">
		<h2>PEMERINTAH KABUPATEN TANGERANG<br />KARTU KELUARGA</h2>
  </div>
	<hr />
  <table style="border:0px; width:50%;">
    <tr style="border:0px;">
      <td style="border:0px;">Nama Kepala Keluarga</td>
      <td style="border:0px;">: <xsl:value-of select="//Patriarch/Name"/></td>
    </tr>
    <tr style="border:0px;">
      <td style="border:0px;">Alamat</td>
      <td style="border:0px;">: <xsl:value-of select="//Patriarch/Address"/></td>
    </tr>
    <tr style="border:0px;">
      <td style="border:0px;">Desa / Kelurahan</td>
      <td style="border:0px;">: <xsl:value-of select="//Patriarch/Kelurahan"/></td>
    </tr>
    <tr style="border:0px;">
      <td style="border:0px;">Kecamatan</td>
      <td style="border:0px;">: <xsl:value-of select="//Patriarch/Kecamatan"/></td>
    </tr>
  </table>
  <br />
	<table style="width: 100%">		
		<tr>
			<th>Nama Lengkap</th>
			<th>Jenis Kelamin</th>
			<th>Hub dg KK</th>
			<th>Tempat &amp;Tanggal Lahir</th>
			<th>Propinsi / Negara</th>
			<th>Status Perkawinan</th>
			<th>Agama</th>
			<th>Gol. Darah</th>
		</tr>
		<xsl:for-each select="//PersonalID">
		<tr>
			<td><xsl:value-of select="Name"/></td>
			<td><xsl:value-of select="GenderName"/></td>
			<td><xsl:value-of select="RelationName"/></td>
			<td><xsl:value-of select="BirthPlace"/>, <xsl:value-of select="BirthDateF"/></td>
			<td><xsl:value-of select="StateCountry"/></td>
			<td><xsl:value-of select="MaritalStatusName"/></td>
			<td><xsl:value-of select="ReligionName"/></td>
			<td><xsl:value-of select="BloodTypeName"/></td>
		</tr>
    </xsl:for-each>		
	</table>
  <br />
	<table style="width: 100%">		
		<tr>
			<th>Pendidikan</th>
			<th>Pekerjaan</th>
			<th>WNRI / No. dan Tgl SBKRI</th>
			<th>Nama Bapak / Ibu</th>
			<th>Orang Asing</th>
			<th>Keterangan</th>
		</tr>
    <xsl:for-each select="//PersonalID">
		<tr>
			<td><xsl:value-of select="LastEducationName"/></td>
			<td><xsl:value-of select="OccupationName"/></td>
			<td><xsl:value-of select="SBKRINotes"/></td>
			<td><xsl:value-of select="ParentName"/></td>
			<td></td>
			<td><xsl:value-of select="Notes"/></td>
		</tr>
    </xsl:for-each>
	</table>
  * Data di atas telah diperiksa dan dinyatakan benar oleh pemohon  
  <div style="position: absolute; left: 50%; text-align: center; bottom: 5px; page-break-after:always;">
    <div style="position: relative; left: -50%;">
      Pemohon<br /><br/><br /><br />____________________<br /><xsl:value-of select="//PrintDate"/>
    </div>
  </div>

  <!--
  <div style="text-align: right; font-size: x-large; page-break-before:always;">
    <span style="font-weight: bold;">CUSTOMER COPY</span>
  </div>
  <div style="text-align: right; font-size: large;">
    CASE NUMBER : <xsl:value-of select="//CaseNumber" />
  </div>
  <div style="text-align: right; font-size: x-small;">
    Print Date : <xsl:value-of select="//PrintDate" /><br />
    Print By : <xsl:value-of select="//PrintBy" />      
  </div>
  <div style="text-align: center;">
    <h2>
      <span style="font-weight: bold;">Permohonan Pengurusan</span>
    </h2>
    <h2>
      <span style="font-weight: bold;">Kartu Tanda Penduduk / Kartu Keluarga</span>
    </h2>
  </div>
  <hr />
    <p>Dengan ini diajukan permohonan untuk pengurusan : <xsl:value-of select="//HelpName" /></p>
    <table style="text-align: left; border:0px solid white;">      
        <tr>
          <td>Nama Pemohon</td>
          <td>: </td>
        </tr>
        <tr>
          <td>No. Telepon</td>
          <td>:</td>
        </tr>
        <tr>
          <td>Alamat</td>
          <td>:</td>
        </tr>     
    </table>
    <p style="font-weight: bold;">PERSYARATAN :</p>
    <ul>
      <li>Surat pindah</li>
      <li>Fotokop Akte Kelahiran</li>
      <li>Fotokopi Akte Perkawinan</li>
      <li>Pas Foto KTP (2x3) sebanyak 3 lembar</li>
    </ul>
    <span style="font-weight: bold;">PERINCIAN BIAYA</span>
    <span style="font-weight: bold;">:</span>
    <br />
      <ul>
        <li>Wilayah</li>
        <li>Biaya (per wilayah)</li>
      </ul>
      <span style="font-weight: bold;">PERHITUNGAN BIAYA :</span>
      <br />
        <ul>
          <li>Kartu Tanda penduduk (KTP)</li>
          <li>Kartu Keluarga</li>
        </ul>
  <table style="text-align: left; border:0px solid white;">  
      <tr>
        <td>Transfer pembayaran</td>
        <td>
          : <span style="font-weight: bold;">
            PT. Tata Manidir Daerah Lippo Karawaci<br />      
            No. Acc : 570-30-80769-0<br />      
            Bank Lippo TKR, Menara Asia, Lippo Karawaci
          </span>
        </td>
      </tr>    
  </table>
  <br />
                  KETERANGAN :<br />
                    <ol>
                      <li>
                        Mohon pembayaran dilakukan melalui transfer atau kasir.
                        Pengurusan dapat dilakukan setelah bukt ipembayara kami terima atau di
                        FAX ke nomor 5579-7105
                      </li>
                      <li>Tidak ada transaksi pembayaran tunai selain pada kasir</li>
                      <li>Proses pengurusan selama 2 (dua) bulan</li>
                    </ol>
                    <div
                     style="position: absolute; left: 50%; text-align: center; bottom: 5px; page-break-after: always;">
                      <div style="position: relative; left: -50%;">
                        <p>
                          TOWN MANAGEMENT Lippo Karawaci 2121 Boulevard Gadjah Mada
                          #01-01, Lippo Karawaci 100, Tangerang 15811<br />
                            Telp. (62-21)5579-0190, 5579-0191. Fax. (62-21)5579-7111<br />
Website : http://www.lippokarawaci.co.id Email :
tmd@lippokarawaci.co.id
</p>
                      </div>
                    </div>
-->
                  </body>
</html>
</xsl:template>
</xsl:stylesheet>