﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using residential.libs.Models;
using System.IO;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk;

namespace residential.web.forms
{
    public partial class viewincident : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                show_case(Request.QueryString["CaseNumber"]);
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                if (Request.QueryString["DeptID"]!=null)
                    updatecase.DepartmentID = Request.QueryString["DeptID"].ToString();
                else
                {
                    updatecase.DepartmentID = "";
                    //updatecase.Visible = false;
                }

            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Case(_casenumber);

            UserData userData = (UserData)ViewState["UserData"];

            if (dt.Rows.Count > 0)
            {
                ViewState["CRMCaseID"] = dt.Rows[0]["CRMCaseID"].ToString();
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();
                
                lbl_incident.Text = dt.Rows[0]["HelpGroupAndHelpName"].ToString();
                lbl_location.Text = dt.Rows[0]["LocationName"].ToString();
                lbl_sublocation.Text = dt.Rows[0]["SubLocationName"].ToString();
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }   
        }        
    }
}