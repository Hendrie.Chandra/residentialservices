﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Web;

namespace residential.web.forms
{
    public partial class viewrebanner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                show_case(Request.QueryString["CaseNumber"]);                
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                string deptID = Request.QueryString["DeptID"];

                if (deptID == null)
                {
                    //updatecase1.Visible = false;
                    /*tampilkan view untuk update cencel di cs*/
                    updatecase1.Visible = true;
                    updatecase1.DepartmentID = "";
                }
                else
                {
                    updatecase1.Visible = true;
                    updatecase1.DepartmentID = Request.QueryString["DeptID"].ToString();
                }
            }
        }
        
        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_rebanner_by_casenumber(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                Int32 subtotal = 0;
                Int32 total = 0;
                Int32 discount = 0;
                subtotal = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Amount"].ToString()));
                total = Convert.ToInt32(decimal.Parse(dt.Rows[0]["AmountAfterDiscount"].ToString()));
                discount = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString()));

                lbl_subtotal.Text = subtotal.ToString("N0");
                lbl_adjustment.Text = discount.ToString("N0");
                lbl_total.Text = total.ToString("N0");

                DataTable dt3 = dbclass.retrieve_rebanner_detail(_casenumber);

                List<ReBannerDetail> list = new List<ReBannerDetail>();
                ReBannerDetail bd = new ReBannerDetail();

                int i = 1;
                foreach (DataRow dr in dt3.Rows)
                {
                    bd = new ReBannerDetail();
                    bd.LineID = i;
                    bd.LocationID = Int32.Parse(dr["LocationID"].ToString());
                    bd.LocationName = dr["BannerLocationName"].ToString();
                    bd.PointID = Int32.Parse(dr["PointID"].ToString());
                    bd.PointName = dr["PointName"].ToString();
                    bd.ChangeDate = DateTime.Parse(dr["ChangeDate"].ToString());

                    list.Add(bd);
                    i++;
                }

                gvw_detail.DataSource = list;
                gvw_detail.DataBind();
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }        
    }
}