﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using System.Security.Principal;
using System.Threading;
using System.Net;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class listResidentCard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                UserData userData = (UserData)ViewState["UserData"];

                ddlCluster.DataSource = clsResident.ClusterList(userData.OrgID, userData.SiteID);
                ddlCluster.DataValueField = "UnitClusterCode";
                ddlCluster.DataTextField = "UnitClusterDesc";
                ddlCluster.DataBind();

                ddlCluster_SelectedIndexChanged(null, null);
                ddlUnitCode_SelectedIndexChanged(null, null);
            }
        }

        protected void ddlCluster_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_search.Text = "";
            if (ddlCluster.SelectedIndex > -1)
            {
                UserData userData = (UserData)ViewState["UserData"];
                ddlUnitCode.DataSource = clsResident.UnitCodeList(userData.OrgID, userData.SiteID, ddlCluster.SelectedValue.ToString());
                ddlUnitCode.DataValueField = "UnitCode";
                ddlUnitCode.DataTextField = "UnitDesc";
                ddlUnitCode.DataBind();

                ddlUnitCode_SelectedIndexChanged(null, null);
            }
        }

        protected void ddlUnitCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_search.Text = "";
            if (ddlUnitCode.SelectedIndex > -1)
            {
                DataList();
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String ResidentHeaderID = DataBinder.Eval(e.Row.DataItem, "ResidentHeaderID").ToString();
                string Unit = DataBinder.Eval(e.Row.DataItem, "Unit").ToString();
                String linkToForm = "EntryResidentCard.aspx";
                //e.Row.Attributes["onclick"] = "window.location = '" + linkToForm + "?RHI=" + ResidentHeaderID + "&U=" + Unit + "'";
                HyperLink lnkAddCard = (HyperLink)e.Row.FindControl("lnk_addCard");
                lnkAddCard.Attributes["onclick"] = "window.location = '" + linkToForm + "?RHI=" + ResidentHeaderID + "&U=" + Unit + "'";
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            DataList();
        }

        void DataList()
        {
            UserData userData = (UserData)ViewState["UserData"];
            gvw_data.DataSource = null;
            gvw_data.DataSource = clsResident.ResidentList(userData.OrgID, userData.SiteID,
                ddlCluster.SelectedValue.ToString(), ddlUnitCode.SelectedValue.ToString(), txt_search.Text.ToString());
            gvw_data.DataBind();
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            DataList();
        }



    }
}