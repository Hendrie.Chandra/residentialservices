﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class entrycarlicenseplate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                alert_danger.Visible = false;
                if (Request.QueryString["CarLicensePlateID"] != null)
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    int BannerTypeID = Int32.Parse(Request.QueryString["CarLicensePlateID"].ToString());
                    DataTable dt = dbclass.retrieve_carlicenseplate(BannerTypeID);

                    DataRow datarow = dt.Rows[0];
                    Lbl_CarLicensePlateID.Text = Request.QueryString["CarLicensePlateID"].ToString();
                    txt_CarLicensePlateName.Text = datarow["CarLicensePlateName"].ToString();
                }
            }
        }

        private Int32 save_carlicenseplate()
        {
            UserData userData = new UserData(User.Identity.Name);
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = 0;
            if (!String.IsNullOrEmpty(Lbl_CarLicensePlateID.Text))
            {
                returnvalue = dbclass.save_carlicenseplate(Int32.Parse(Lbl_CarLicensePlateID.Text), txt_CarLicensePlateName.Text, userData.UserName);
            }
            else
            {
                returnvalue = dbclass.save_carlicenseplate(0, txt_CarLicensePlateName.Text, userData.UserName);
            }
            return returnvalue;
        }

        protected Boolean sites_validation()
        {
            if (String.IsNullOrEmpty(txt_CarLicensePlateName.Text))
            {
                alert_danger_text.Text = "Car License Plate Name is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_carlicenseplate();
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_carlicenseplate();
                Response.Redirect("listcarlicenseplate.aspx");
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            }
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_carlicenseplate();
                Lbl_CarLicensePlateID.Text = String.Empty;
                txt_CarLicensePlateName.Text = String.Empty;
            }
        }     
    }
}