﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entrysubscriptiontype.aspx.cs" Inherits="residential.web.forms.entrysubscriptiontype" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>
        :: entry subscription type ::
    </title>    
    <script type="text/javascript" src="../global.js">
    </script>
    <link href="~/maincss.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="popupcontainer">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">                
                <ContentTemplate>
                    <div class="buttoncontainer">
                        <asp:Button ID="btn_save" runat="server" Text="SAVE" CssClass="button" 
                            Width="100px" onclick="btn_save_Click" />
                        <asp:Button ID="btn_saveclose" runat="server" Text="SAVE CLOSE" 
                            CssClass="button" Width="100px" onclick="btn_saveclose_Click" />
                        <asp:Button ID="btn_savenew" runat="server" Text="SAVE NEW" CssClass="button" 
                            Width="100px" onclick="btn_savenew_Click" />                        
                    </div>
                    <div class="alert alert-success" id="alert_success" runat="server"><asp:Label runat="server" ID="alert_success_text"></asp:Label></div>
                    <div class="alert alert-danger" id="alert_danger" runat="server"><asp:Label runat="server" ID="alert_danger_text"></asp:Label></div>
                    <h3>Subscription Type</h3>
                    <div class="formentrycontent">
                        <div class="formentrycell1">
                            <asp:Label ID="Label1" runat="server" Text="Name :" CssClass="labelfield"></asp:Label>
                            <asp:TextBox ID="txt_subscriptiontypename" runat="server" 
                                CssClass="textbox_entry_left textboxcell1"></asp:TextBox>
                        </div>                       
                    </div>                   
                    <asp:CheckBoxList ID="lst_smsgroup" runat="server" RepeatColumns="3" Width="100%">                        
                    </asp:CheckBoxList>                              
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
