﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lookupform.aspx.cs" Inherits="residential.web.forms.lookupform" EnableEventValidation = "false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>:: lookup form ::</title>
    <script type="text/javascript" src="../scripts/jquery-2.1.3.min.js"></script>  
    <script type="text/javascript" src="../global.js">
    </script>    
    <link href="~/maincss.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .show-loading-animation
        {
            visibility:visible;     
            position: fixed;
            top: 50%;
            left: 50%;
            margin-top: -50px;
            margin-left: -100px;                   
        }
        
        .hide-loading-animation
        {
            visibility:hidden;
            position:absolute;
        }             
    </style>
    <script type="text/javascript">
        function showLoading() {
            var GridViewID = document.getElementById("gvw_data");
            GridViewID.className = "";
            GridViewID.className = "hide-loading-animation";

            var ImageID = document.getElementById("LoaderImage");
            ImageID.className = "";
            ImageID.className = "show-loading-animation";
        }
    </script>
</head>
<body class="bodypopup">
    <form id="form1" runat="server">
        <div class="popupcontainer">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Label ID="lbl_title" runat="server" CssClass="labellookuptitle"></asp:Label>
                    <div class="buttoncontainer" style="height:auto">
                        <table border="0" cellpadding = "0" width="100%">
                            <tr style="border-bottom: solid 1px white;">
                                <td align="center">
                                    <font color="white">Customer</font>
                                </td>
                                <td align="center" style="color: White;">
                                    <asp:RadioButtonList ID="rb_customertype" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Owner" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Tenant" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td colspan="2">
                                </td>
                            </tr>                            
                            <tr>
                                <td align="center"><font color="white">Personal Data</font></td> 
                                <td align="center"><font color="white">Unit Code or Cluster</font></td>
                                <td align="center"><font color="white">Unit No</font></td>
                                <td rowspan="2">
                                    <asp:Button ID="btn_search" runat="server" Text="SEARCH" CssClass="button" 
                            Width="100px" Height="100%" onclick="btn_search_Click" OnClientClick="showLoading()" />
                                </td>
                            </tr>
                            <tr style="width:100%">
                                <td align="center" width="33.33%">
                                    <asp:TextBox ID="txt_personal" runat="server" width="95%"></asp:TextBox>
                                    <asp:TextBoxWatermarkExtender ID="TWE1" runat="server" TargetControlID="txt_personal" WatermarkText="PSCode, Name, Address" WatermarkCssClass="textbox-watermark" ></asp:TextBoxWatermarkExtender>
                                </td>
                                <td align="center" width="33.33%">
                                    <asp:TextBox ID="txt_unit" runat="server" width="95%"></asp:TextBox>
                                    <asp:TextBoxWatermarkExtender ID="TWE2" runat="server" TargetControlID="txt_unit" WatermarkText="Unit Code, Unit Cluster" WatermarkCssClass="textbox-watermark" ></asp:TextBoxWatermarkExtender>
                                </td>
                                <td align="center" width="33.33%">
                                    <asp:TextBox ID="txt_unitno" runat="server" width="95%"></asp:TextBox>
                                    <asp:TextBoxWatermarkExtender ID="TWE3" runat="server" TargetControlID="txt_unitno" WatermarkText="Unit No" WatermarkCssClass="textbox-watermark" ></asp:TextBoxWatermarkExtender>
                                </td>
                            </tr>
                        </table>                                                
                    </div>
                    <br />                    
                    <div class="lookupgridviewcontainer" >                        
                        <asp:Image ID="LoaderImage" runat="server" ImageUrl="~/images/loading.GIF" CssClass="hide-loading-animation" ImageAlign="Middle" />                        
                        <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" 
                            CssClass="gridview" AllowPaging="True" BorderWidth="0px" 
                            Width="100%" DataKeyNames="pscode"
                            onpageindexchanging="gvw_data_PageIndexChanging" 
                            onrowcommand="gvw_data_RowCommand" onrowdatabound="gvw_data_RowDataBound" 
                            onselectedindexchanged="gvw_data_SelectedIndexChanged">
                            <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Prev" Position="TopAndBottom" />                            
                            <Columns>
                                <%--<asp:TemplateField HeaderText="PS CODE">
                                    <HeaderStyle CssClass="gridview_header" Width="70px" />
                                    <ItemStyle CssClass="gridview_item_left" Font-Bold="true" />
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnk_pscode" runat="server" CommandName="cmd_select" 
                                            CssClass="gridview_link">pscode</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:BoundField HeaderText="PSCODE" DataField="pscode">
                                    <HeaderStyle CssClass="gridview_header" Width="70px" />
                                    <ItemStyle CssClass="gridview_item_left" Font-Bold="true" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="NAME" DataField="psname">
                                    <HeaderStyle CssClass="gridview_header" Width="150px" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="ADDRESS" DataField="psaddress">
                                    <HeaderStyle CssClass="gridview_header" Width="200px" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="UNIT" DataField="UNIT">
                                    <HeaderStyle CssClass="gridview_header" Width="100px" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="CLUSTER" DataField="UnitClusterDesc">
                                    <HeaderStyle CssClass="gridview_header" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                            </Columns>                            
                            <EmptyDataTemplate>
                                No data
                            </EmptyDataTemplate>
                        </asp:GridView>                        
                    </div>
                    <asp:Label ID="lbl_itemdisplayed" runat="server" ></asp:Label>
                    <hr />
                    <asp:Label ID="Label1" runat="server" Text="PS Code : "></asp:Label>
                    <asp:Label ID="lbl_customertype" runat="server"></asp:Label>
                    <asp:Label ID="lbl_pscode" runat="server" Text=""></asp:Label> -
                    <asp:Label ID="lbl_psname" runat="server" Text=""></asp:Label>
                    <div class="buttoncontainerfooter">
                        <asp:Button ID="btn_select" runat="server" Text="OK" CssClass="button" 
                            Width="100px" onclick="btn_select_Click" />
                        <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="button" 
                            Width="100px" onclick="btn_cancel_Click" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
