﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Web;

namespace residential.web.forms
{
    public partial class readonlyheader : System.Web.UI.UserControl
    {
        public Boolean CustomerType
        {
            set { txt_customertype.Value = (value == false) ? "0" : "1"; }
            get { return (txt_customertype.Value == "0") ? false : true; }
        }

        public String OwnerName
        {
            set { txt_customername.Text = value; }
            get { return txt_customername.Text; }
        }

        public String PSCode
        {
            get { return txt_pscode.Text; }
            set { txt_pscode.Text = value; }
        }

        public String Unit
        {
            get { return cbo_unit.Text; }
            set { cbo_unit.Text = value; }
        }

        public String RequestorName
        {
            get { return txt_requestname.Text; }
            set { txt_requestname.Text = value; }
        }

        public String RequestorPhone
        {
            get { return txt_phone.Text; }
            set { txt_phone.Text = value; }
        }

        public String RequestorEmail
        {
            get { return txt_email.Text; }
            set { txt_email.Text = value; }
        }

        public String RequestDescription
        {
            get { return txt_description.Text; }
            set { txt_description.Text = value; }
        }

        public bool RequestDescription_Enabled
        {
            set { txt_description.Enabled = value; }
        }

        public Int32 CaseOriginID
        {
            get { return Int32.Parse(cbo_caseorigin.SelectedValue); }
        }

        public String CaseOrigin
        {
            set { cbo_caseorigin.SelectedValue = value; }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["CaseNumber"] == null)
                    combobox_caseorigin();
            }
        }

        public void combobox_caseorigin()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_caseorigin();

            cbo_caseorigin.DataSource = dt;
            cbo_caseorigin.DataTextField = "CaseOriginName";
            cbo_caseorigin.DataValueField = "CaseOriginID";
            cbo_caseorigin.DataBind();
            cbo_caseorigin.Items.FindByText("Walk In").Selected = true;
        }

        public void combobox_unit()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = new UserData(HttpContext.Current.User.Identity.Name);

            DataTable dt = dbclass.retrieve_unitownership(userData.OrgID, userData.SiteID, txt_pscode.Text, 1);

            if (dt.Rows.Count > 0)
            {
                cbo_unit.Items.Clear();
                foreach (DataRow row in dt.Rows)
                {                    
                    cbo_unit.Items.Add(new ListItem(row["UnitCode"].ToString().Replace(" ", String.Empty) + "-" + row["UnitNo"].ToString().Replace(" ", String.Empty)));
                }
                if (dt.Rows.Count == 1)
                    cbo_unit.SelectedIndex = 0;
            }
            else
                cbo_unit.Items.Clear();
        }

        public void reset_fields(ControlCollection ctls)
        {
            foreach (Control c in ctls)
            {
                if (c is TextBox)
                {
                    TextBox tt = c as TextBox;
                    tt.Text = null;
                }
                else if (c is AjaxControlToolkit.ComboBox)
                {
                    AjaxControlToolkit.ComboBox dd = c as AjaxControlToolkit.ComboBox;
                    if (dd.Items.Count > 0)
                        dd.SelectedIndex = 0;
                }

                if (c.HasControls())
                {
                    reset_fields(c.Controls);
                }
            }
        }
    }
}