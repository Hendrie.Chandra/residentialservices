﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class emailviewer : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["CaseNumber"] != null)
                {
                    bound_emailviewer();
                }                            
            }
        }

        private void bound_emailviewer()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_email_history(Request.QueryString["CaseNumber"]);

            gvw_emailviewer.DataSource = dt;
            gvw_emailviewer.DataBind();
        }

        protected void gvw_emailviewer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hplEmailViewer = (HyperLink)e.Row.FindControl("hpl_viewemail");
                //e.Row.Attributes["onclick"] = "location.href='viewemail.aspx?emailID=" + DataBinder.Eval(e.Row.DataItem, "emailID")+ "'";
            }
        }  
    }
}