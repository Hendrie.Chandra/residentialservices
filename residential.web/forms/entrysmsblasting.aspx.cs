﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Configuration;
using System.Data;
using System.IO;
using System.Globalization;
using residential.libs.Models;
using System.Data.OleDb;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entrysmsblasting : System.Web.UI.Page
    {
        DataTable dt_SIMNo;
        DataTable dt_Group;
        static String prevPage = "listsmsblasting.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                ViewState["UserData"] = new UserData(User.Identity.Name);

                UserData userData = (UserData)ViewState["UserData"];

                DataTable dtprice = dbclass.retrieve_SMSBlastPrice(userData.OrgID, userData.SiteID);
                
                lbl_price.Text = dtprice.Rows[0]["Price"].ToString();
                txt_messageContent.Attributes.Add("onkeyup", "MessageCharacterLeft()");
                show_cbo_Subscriptiontype();
                show_cbo_SMSSender();                
                dt_SIMNo = new DataTable();
                dt_Group = new DataTable();
                MakeSIMNoDataTable();
                MakeGroupDataTable();
                show_tv_group();                
                BindSIMNo();
                calExtender1.StartDate = DateTime.Now;
                update_total();                                       
                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            else
            {
                dt_SIMNo = (DataTable)ViewState["DataTableSIMNo"];
                dt_Group = (DataTable)ViewState["DataTableGroup"];
            }
            ViewState["DataTableSIMNo"] = dt_SIMNo;
            ViewState["DataTableGroup"] = dt_Group;
            entryheader.ToggleAlert = false;
        }

        private void Show_Case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_SMSBlast(_casenumber);

            entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
            entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
            //check customer type -> 0 = Owner, 1 = Tenant
            entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
            entryheader.combobox_unit();
            entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();
            
            if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

            entryheader.combobox_caseorigin();
            entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

            entryheader.disable_all_controls();

            DateTime Schedule = (DateTime)dt.Rows[0]["BlastSchedule"];
            txt_schedule.Text = Schedule.Date.ToString("dd MMMM yyyy");
            txt_time.Text = Schedule.ToShortTimeString();            
            txt_messageContent.Text = dt.Rows[0]["MessageDetail"].ToString();
            cbo_sender.SelectedValue = dt.Rows[0]["SenderID"].ToString();
            lbl_quantity.Text = dt.Rows[0]["Quantity"].ToString();
            lbl_price.Text = dt.Rows[0]["Price"].ToString();
            txt_discount.Text = dt.Rows[0]["TotalDiscount"].ToString();

            DataTable dtnumberdetail = dbclass.retrieve_SMSBlastNumberDetail(_casenumber);
            dt_SIMNo = dtnumberdetail;
            gv_simnumber.DataSource = dtnumberdetail;
            gv_simnumber.DataBind();

            update_total();
        }

        private void show_cbo_Subscriptiontype()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_SubscriptionType();
            cbo_subscriptionType.DataSource = dt;
            cbo_subscriptionType.DataTextField = "SubscriptionTypeName";
            cbo_subscriptionType.DataValueField = "SubscriptionTypeID";
            cbo_subscriptionType.DataBind();
            cbo_subscriptionType.Items.Insert(0, new ListItem("[ Without Subscription ]", "0"));
            cbo_subscriptionType.SelectedIndex = 0;            
        }       
        
        private void show_cbo_SMSSender()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_SMSSender(userData.OrgID, userData.SiteID);
            cbo_sender.DataSource = dt;
            cbo_sender.DataTextField = "SMSSender";
            cbo_sender.DataValueField = "SMSSenderID";
            cbo_sender.DataBind();
            cbo_sender.SelectedIndex = 0;
        }               

        protected void btn_addGroup_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txt_unitgroup.Text))
            {
                entryheader.ShowAlert = "Please select unit group";       
            }
            else
            {
                String[] UnitGroup = txt_unitgroup.Text.Split('-');
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DataTable dt = dbclass.retrieve_SMSPhoneNumber(userData.OrgID, userData.SiteID, Int32.Parse(UnitGroup[0]), UnitGroup[1].Trim());                

                if (dt.Rows.Count == 0)
                {
                    entryheader.ShowAlert = "No Phone Number Data";                    
                }
                else
                    dt_SIMNo.Merge(dt);
                btn_removeDuplicateNumber_Click(this, EventArgs.Empty);
                BindSIMNo();
                update_total();
                txt_unitgroup.Text = String.Empty;
            }                      
        }

        private void MakeGroupDataTable()
        {
            dt_Group.Columns.Add("SMSGroupID");
            dt_Group.Columns.Add("GroupName");
        }

        private void MakeSIMNoDataTable()
        {
            dt_SIMNo.Columns.Add("SMSNumber");
            dt_SIMNo.Columns.Add("SMSName");
        }        

        private void AddToSIMNo()
        {
            DataRow dr = dt_SIMNo.NewRow();
            dr["SMSNumber"] = txt_SIMNo.Text;
            dr["SMSName"] = txt_SMSName.Text;
            dt_SIMNo.Rows.Add(dr);
        }

        private void AddToSIMNo(String[] _contactNumber)
        {
            DataRow dr = dt_SIMNo.NewRow();
            dr["SMSNumber"] = _contactNumber[0];
            dr["SMSName"] = _contactNumber[1];
            dt_SIMNo.Rows.Add(dr);
        }

        //private void BindGroup()
        //{
        //    gv_Group.DataSource = dt_Group;
        //    gv_Group.DataBind();
        //}

        private void BindSIMNo()
        {
            gv_simnumber.DataSource = dt_SIMNo;
            gv_simnumber.DataBind(); 
        }

        protected void btn_addSIMNo_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txt_SIMNo.Text) && !String.IsNullOrEmpty(txt_SMSName.Text))
            {
                string find = txt_SIMNo.Text;
                DataRow[] foundRows = dt_SIMNo.Select("SMSNumber = '" + find + "'");
                if (foundRows.Count() > 0)
                {
                    entryheader.ShowAlert = "Duplicate Number!";                    
                }
                else
                {
                    AddToSIMNo();
                    BindSIMNo();                    
                    update_total();
                }
                txt_SIMNo.Text = null;
                txt_SMSName.Text = null;
            }
        }
        
        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        private String Save_SMSBlasting()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            //General
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
            String SavedBy = userData.UserName;
            String FullName = userData.FullName;

            //TRCase
            Int32 CaseOriginID = entryheader.CaseOriginID;
            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["SMSBlastID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString().Replace(" ", String.Empty);
                UnitNo = unit[1].ToString().Replace(" ", String.Empty);
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            //TRSMSBlast            
            String MessagesDetail = txt_messageContent.Text;            

            DateTime BlastSchedule = DateTime.Now.Date;
            if (chk_schedule.Checked)
            {
                String time = "00:00";                
                if (!String.IsNullOrWhiteSpace(txt_time.Text))
                    time = txt_time.Text;
                String Schedule = txt_schedule.Text + " " + time;
                BlastSchedule = DateTime.Parse(Schedule);
            }            
            Int32 SenderID = Int32.Parse(cbo_sender.SelectedValue);
            Int32 Price = Int32.Parse(lbl_price.Text);
            Int32 Quantity = dt_SIMNo.Rows.Count;
            Int32 TotalAmount = Quantity * Price;
            Int32 TotalDiscount = 0;
            if (!String.IsNullOrEmpty(txt_discount.Text))
                TotalDiscount = Int32.Parse(txt_discount.Text);
            Int32 TotalAmountAfterDiscount = TotalAmount - TotalDiscount;            

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_SMSBlast(Request.QueryString["CaseNumber"].ToString());
                //request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
                dbclass.delete_SMSBlastNumberDetail(Int32.Parse(CaseDT.Rows[0]["SMSBlastID"].ToString()));
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_smsblast(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID, CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode,
                    UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now, RequestDescription, MessagesDetail, 1,
                    BlastSchedule, SenderID, Price, Quantity, TotalAmount, TotalDiscount, TotalAmountAfterDiscount, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    DataTable DT_SMS = dbclass.retrieve_SMSBlast(CaseNumber);
                    Int32 SMSBlastID = Int32.Parse(DT_SMS.Rows[0]["SMSBlastID"].ToString());
                    foreach (DataRow dr in dt_SIMNo.Rows)
                    {
                        String SMSNumber = dr["SMSNumber"].ToString();
                        String SMSName = dr["SMSName"].ToString();
                        dbclass.save_SMSBlastNumberDetail(OrgID, SiteID, SMSBlastID, SMSNumber, SMSName, SavedBy);
                    }

                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }            
            else
                return dbcase.ExceptionMessage;

            return null;            
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {                  
            if (sms_validation())
            {
                String res = Save_SMSBlasting();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        protected void btn_clearMessage_Click(object sender, EventArgs e)
        {            
            txt_messageContent.Text = null;
        }

        protected void btn_clearAll_Click(object sender, EventArgs e)
        {
            dt_SIMNo.Clear();
            dt_Group.Clear();
            BindSIMNo();
            update_total();            
        }
        
        protected void btn_UploadSMSGroup_Click(object sender, EventArgs e)
        {
            if (fu_SIMNo.HasFile)
            {
                String[] filesAccepted = { ".xls", ".xlsx" };
                if (filesAccepted.Contains(Path.GetExtension(fu_SIMNo.PostedFile.FileName)))
                {
                    try
                    {
                        String FileExtension = Path.GetExtension(fu_SIMNo.FileName);
                        String FileLocation = Server.MapPath("~/uploadedcsv/") + DateTime.Now.ToString("ddMMyyyyhhmmff") + FileExtension;
                        fu_SIMNo.SaveAs(FileLocation);

                        OleDbConnection cnn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileLocation + "; Extended Properties=Excel 12.0;");
                        OleDbCommand oconn = new OleDbCommand("select * from [Sheet1$]", cnn);
                        cnn.Open();
                        OleDbDataAdapter adp = new OleDbDataAdapter(oconn);
                        DataTable dt_number = new DataTable();
                        adp.Fill(dt_number);
                        cnn.Close();
                        String[] ContactNumber = new String[2];
                        foreach (DataRow dr in dt_number.Rows)
                        {
                            var i = 0;
                            foreach (var item in dr.ItemArray)
                            {
                                ContactNumber[i] = item.ToString();
                                i++;     
                            }
                            AddToSIMNo(ContactNumber);
                        }                        
                        System.IO.File.Delete(FileLocation);
                        BindSIMNo();
                        update_total();
                    }
                    catch (Exception ex)
                    {
                        entryheader.ShowAlert = "The following errors have occurred: " + ex.ToString();                        
                    }
                }
                else
                {
                    entryheader.ShowAlert = "Only Ms Excel (.xls or .xlsx) files are accepted";                    
                }
            }
        }      

        protected void gv_simnumber_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int index = Convert.ToInt32(e.CommandArgument);
            ((DataTable)ViewState["DataTableSIMNo"]).Rows[index].Delete();
            ((DataTable)ViewState["DataTableSIMNo"]).AcceptChanges();
            gv_simnumber.DataSource = (DataTable)ViewState["DataTableSIMNo"];
            gv_simnumber.DataBind();
            update_total();
        }     

        protected void btn_removeDuplicateNumber_Click(object sender, EventArgs e)
        {
            if (dt_SIMNo.Rows.Count > 0)
            {
                var UniqueRows = dt_SIMNo.AsEnumerable().Distinct(DataRowComparer.Default);
                DataTable dt2 = UniqueRows.CopyToDataTable();
                dt_SIMNo.Clear();
                dt_SIMNo = dt2;
                ViewState["DataTableSIMNo"] = dt_SIMNo;
                BindSIMNo();                
            }
            update_total();
        }

        protected void chk_schedule_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_schedule.Checked)
            {
                txt_schedule.Enabled = true;
                txt_time.Enabled = true;                
            }
            else
            {
                txt_schedule.Enabled = false;
                txt_time.Enabled = false;                
            }
        }

        protected void update_total()
        {
            int aa = txt_messageContent.Text.Length;                        
            Int32 qty = dt_SIMNo.Rows.Count;
            Int32 price = Int32.Parse(lbl_price.Text);
            
            Int32 subtotal = qty * price;
            Int32 discount = 0;            
            if (!String.IsNullOrWhiteSpace(txt_discount.Text))
                discount = Int32.Parse(txt_discount.Text);
            Int32 total = qty * price - discount;

            if (!String.IsNullOrEmpty(txt_discount.Text))
                discount = Int32.Parse(txt_discount.Text);

            lbl_adjustment.Text = discount.ToString("N0");
            lbl_quantity.Text = qty.ToString();
            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_total.Text = total.ToString("N0");
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            Int32 discount = 0;
            if (!String.IsNullOrEmpty(txt_discount.Text))
                discount = Int32.Parse(txt_discount.Text, NumberStyles.Currency);
            lbl_adjustment.Text = discount.ToString("N0");

            update_total();
        }

        protected void DeleteSMSGroup(object sender, EventArgs e)
        {
            Int32 SMSGroupID = Int32.Parse((sender as LinkButton).CommandArgument);
            DataRow[] rows;
            rows = dt_Group.Select("SMSGroupID = " + SMSGroupID);            
            rows[0].Delete();
            //BindGroup();

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_SMSGroupNumberDetail(SMSGroupID);
            dt.Columns.Remove("SMSGroupNumberDetailID");
            dt.Columns.Remove("SMSGroupID");
            var dtexclude = dt_SIMNo.AsEnumerable().Except(dt.AsEnumerable(), DataRowComparer.Default);
            if (dtexclude.Count() == 0)
                dt_SIMNo.Clear();
            else
            {
                dt_SIMNo = dtexclude.CopyToDataTable<DataRow>();
                ViewState["DataTableSIMNo"] = dt_SIMNo;
            }   
            BindSIMNo();
            update_total();
        }

        protected bool sms_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (dt_SIMNo.Rows.Count == 0)
            {
                entryheader.ShowAlert = "Recipient number is required";
                return false;
            }
            if (String.IsNullOrWhiteSpace(txt_messageContent.Text))
            {
                entryheader.ShowAlert = "Message Content is required";
                return false;
            }
            if (txt_messageContent.Text.Count() > 160)
            {
                entryheader.ShowAlert = "Maximum 160 characters";
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }
            return true;
        }
        
        protected void show_tv_group()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt_area, dt_cluster, dt_unitcode = new DataTable();
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
            dt_area = dbclass.retrieve_UnitArea(OrgID, SiteID);
            dt_cluster = dbclass.retrieve_UnitCluster(OrgID, SiteID);
            dt_unitcode = dbclass.retrieve_UnitCode(OrgID, SiteID);

            tv_unitgroup.Nodes.Clear();

            foreach (DataRow dr_area in dt_area.Rows)
            {
                String UnitArea_val = dr_area["UnitAreaCode"].ToString();
                String UnitArea_text = UnitArea_val + " - " + dr_area["UnitAreaDesc"].ToString();
                TreeNode tn_area = new TreeNode(UnitArea_text, UnitArea_val);
                tv_unitgroup.Nodes.Add(tn_area);

                DataRow[] dra_cluster = dt_cluster.Select("[UnitAreaCode] = '" + UnitArea_val + "'");
                foreach (var cluster in dra_cluster)
                {
                    String Cluster_val = cluster["UnitClusterCode"].ToString();
                    String Cluster_text = Cluster_val + " - " + cluster["UnitClusterDesc"].ToString();
                    TreeNode tn_cluster = new TreeNode(Cluster_text, Cluster_val);
                    tn_area.ChildNodes.Add(tn_cluster);

                    DataRow[] dra_unitcode = dt_unitcode.Select("[UnitClusterCode] = '" + Cluster_val + "'");
                    foreach (var unitcode in dra_unitcode)
                    {
                        String UnitCode_val = unitcode["UnitCode"].ToString();
                        String UnitCode_text = UnitCode_val + " - " + unitcode["UnitDesc"].ToString();
                        tn_cluster.ChildNodes.Add(new TreeNode(UnitCode_text, UnitCode_val));
                    }
                }
            }
            tv_unitgroup.CollapseAll();
        }

        protected void txt_messageContent_TextChanged(object sender, EventArgs e)
        {
            lbl_charLength.Text = (160 - txt_messageContent.Text.Count()).ToString();
        }

        protected void chk_internalgroup_CheckedChanged(object sender, EventArgs e)
        {

            if (chk_internalgroup.Checked)
            {
                txt_discount.Text = Int32.Parse(lbl_subtotal.Text, NumberStyles.Currency).ToString();
            }
            else
                txt_discount.Text = "0";
            update_total();
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void btn_selectgroup_Click(object sender, EventArgs e)
        {            
        }

        protected void tv_unitgroup_SelectedNodeChanged(object sender, EventArgs e)
        {
            txt_unitgroup.Text = tv_unitgroup.SelectedNode.Depth.ToString() + "-" + tv_unitgroup.SelectedNode.Text;
        }

        protected void gv_simnumber_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gv_simnumber.PageIndex = e.NewPageIndex;
            BindSIMNo();
        }        
    }
}