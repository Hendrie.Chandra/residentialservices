﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class printresidentcard : System.Web.UI.Page
    {
        private String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
        int orgID = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ResidentCardID"] != null)
                {
                    int residentCardID = int.Parse(Request.QueryString["ResidentCardID"]);
                    DBclass dbclass = new DBclass(constring);
                    DataTable dt = dbclass.retrieve_resident_card(residentCardID);

                    if (dt.Rows.Count > 0)
                    {
                        string photoBase64string = dt.Rows[0]["PhotoID"].ToString();
                        string designTypeCode = dt.Rows[0]["DesignTypeCode"].ToString();
                        string name = dt.Rows[0]["Name"].ToString();
                        string cardNo = dt.Rows[0]["CardNo"].ToString();
                        string snCard = dt.Rows[0]["SNCard"].ToString();
                        string unitClusterDesc = dt.Rows[0]["UnitClusterDesc"].ToString();

                        string expiredDate = "";
                        if (dt.Rows[0]["ExpiredDate"]!=System.DBNull.Value)
                            expiredDate =((DateTime)dt.Rows[0]["ExpiredDate"]).ToString("dd/MM/yyyy");

                        string frontDesignCardUrl = dt.Rows[0]["FrontImageUrl"].ToString();
                        string backDesignCardUrl = dt.Rows[0]["BackImageUrl"].ToString();

                        frontCard.Style["background-image"] = "../" + frontDesignCardUrl;
                        backCard.Style["background-image"] = "../" + backDesignCardUrl;
                        //frontCard.ImageUrl = "../" + frontDesignCardUrl; ;
                        //backCard.ImageUrl = "../" + backDesignCardUrl;

                        if (!string.IsNullOrEmpty(photoBase64string))
                            photo.Attributes["src"] = photoBase64string;
                        else
                            photo.Attributes.Add("display", "none");        //photo not available

                        //DataTable dtd = dbclass.retrieve_resident_design_card(designTypeCode);
                        //if (dtd.Rows.Count > 0)
                        //{
                        //    string frontDesignCardUrl = dtd.Rows[0]["FrontImageUrl"].ToString();                            
                        //    string backDesignCardUrl = dtd.Rows[0]["BackImageUrl"].ToString();

                        //    frontCard.Style["background-image"] = "../" + frontDesignCardUrl;
                        //    backCard.Style["background-image"] = "../" + backDesignCardUrl;

                        //}
                        //lblFrontCard.Text = frontDesignCardUrl;
                        //lblBackCard.Text = backDesignCardUrl;

                        
                        lblCardNo.Text = cardNo;
                        lblCardName.Text = name;
                        lblCluster.Text = unitClusterDesc;
                        lblExpired.Text = expiredDate;
                        
                    }
                }
                else
                {
                    Response.Redirect("~/404_pagenotfound.aspx");
                }
            } 

        }
    }
}