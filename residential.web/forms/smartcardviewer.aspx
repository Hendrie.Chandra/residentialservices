﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="smartcardviewer.aspx.cs" Inherits="residential.web.forms.smartcardviewer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="container" style="margin-top: 0px;">
        <div class="page-header">
            <h1>Smartcard View</h1>
        </div>

        <div class="panel panel-default">
            <div class="panel-body" style="background-color: #f2f2f2">

<%--                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="padding-top: 6px">Searh By</div>
                        <div class="col-md-4">
                            <asp:DropDownList ID="dll_search" runat="server" CssClass="form-control" OnTextChanged="dll_search_TextChanged"></asp:DropDownList>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-5">
                        </div>
                    </div>
                </div>--%>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-2" style="padding-top: 6px; text-align:right">Search by</div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txt_carddetail" runat="server" CssClass="form-control" placeholder="card number / holde rname"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txt_unitCode" runat="server" CssClass="form-control" placeholder="unit code"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:TextBox ID="txt_unitNo" runat="server" CssClass="form-control" placeholder="unit no"></asp:TextBox>
                        </div>
                        <div class="col-md-2">
                            <asp:Button ID="btn_serach_by_cardnumber" runat="server" Text="Search" CssClass="btn btn-default" OnClick="btn_serach_by_cardnumber_Click" />
                        </div>
                        <%--<div class="col-md-6">
                            <asp:Button ID="btn_serach_by_cardnumber" runat="server" Text="Search" CssClass="btn btn-default" OnClick="btn_serach_by_cardnumber_Click" />
                        </div>--%>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <%--<asp:Button ID="btn_serach_by_cardnumber" runat="server" Text="Search" CssClass="btn btn-default" OnClick="btn_serach_by_cardnumber_Click" />--%>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <br />
        <br />
        <hr />

        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover"
                    AllowPaging="True"
                    ShowHeaderWhenEmpty="True" DataKeyNames="CardNumber" EmptyDataText="No Data"
                    AllowSorting="true" OnSorting="gvw_data_Sorting" OnPageIndexChanging="gvw_data_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="CARD NUMBER" DataField="CardNumber" SortExpression="CardNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="BOOM GATE" DataField="BoomGateCode" SortExpression="BoomGateCode"></asp:BoundField>
                        <asp:BoundField HeaderText="HOLDER" DataField="HolderName" SortExpression="HolderName"></asp:BoundField>
                        <asp:TemplateField HeaderText="STATUS" ItemStyle-HorizontalAlign="Center" SortExpression="Status">
                            <ItemTemplate>
                                <%--<asp:Label ID="lbl_isvisitor" runat="server" Text="<%# Eval("IsVisitor").ToString() %>"></asp:Label>--%>
                                <asp:Label ID="Status" runat="server" Text='<%# (int)Eval("Status")==1 ? "Active" : "InActive" %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="REQUESTOR" DataField="NamaPelapor" SortExpression="Requestor"></asp:BoundField>
                    </Columns>
                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>--%>
