﻿using residential.libs;
using residential.libs.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

using residential.web.Classes;

namespace residential.web.forms
{
    public partial class EntryOtherIncome : System.Web.UI.Page
    {
        List<Items> ListItems = new List<Items>();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Dept"] != null)
                {
                    HyperLink2.NavigateUrl = HyperLink2.NavigateUrl + "?Dept=" + Request.QueryString["Dept"].ToString();

                    ViewState["UserData"] = new UserData(User.Identity.Name);

                    UserData userData = (UserData)ViewState["UserData"];

                    DataSet ds = residential.web.Classes.clsOtherIncome.HelpNameList(userData.OrgID, userData.SiteID, Request.QueryString["Dept"].ToString());

                    lblTitle.Text = ds.Tables[0].Rows[0]["Dept"].ToString();

                    cbo_servicerequests.DataSource = ds.Tables[1];
                    cbo_servicerequests.DataTextField = "Helpname";
                    cbo_servicerequests.DataValueField = "HelpID";
                    cbo_servicerequests.DataBind();

                    cbo_servicerequests_SelectedIndexChanged(null, null);

                    lbl_adjustment.Text = "0";
                }
                else
                {
                    Response.Redirect("~/404_pagenotfound.aspx");
                }
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (lbl_total.Text.ToString() != "0")
            {
                UserData userData = (UserData)ViewState["UserData"];

                String SavedBy = userData.UserName;
                Int32 OrgID = userData.OrgID;
                Int32 SiteID = userData.SiteID;

                Int32 HelpID = Int32.Parse(HFHelpID.Value.ToString());

                DataTable dt = residential.web.Classes.clsOtherIncome.CheckTransactionID(HelpID, OrgID, SiteID);

                if (dt.Rows.Count == 0)
                {
                    entryheader.ShowAlert = "Transaction ID dit not exist, please contact IT";
                    return;
                }
            }
            if (entryheader.Validate != "VALID" || cbo_servicerequests.SelectedIndex == -1)
            {
                entryheader.ShowAlert = "Invalid Data";
                return;
            }
            else if (entryheader.Validate == "VALID" || cbo_servicerequests.SelectedIndex > -1)
            {
                if (decimal.Parse(lbl_total.Text) > -1 && gvw_data.Rows.Count > 0)
                {
                    String res = save_OtherIncome();
                    if (!string.IsNullOrEmpty(res))
                    {
                        entryheader.ShowAlert = res;
                    }
                    else
                    {
                        Response.Redirect("OtherIncome.aspx?Dept=" + Request.QueryString["Dept"].ToString());
                    }
                }
                else
                {
                    entryheader.ShowAlert = "Total Amount below 0 or No Item Listed";
                    return;
                }
            }
        }

        private string save_OtherIncome()
        {
            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            Int32 HelpID = Int32.Parse(HFHelpID.Value.ToString());

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString(); 
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;
            DateTime RequestDate = DateTime.Now;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            /*add by bili januari 2018*/
            request.OrgID = userData.OrgID;
            request.SiteID = userData.SiteID;
            /*add by bili januari 2018*/
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);              /*return di ganti crmhelpid mendaji helpid*/
            request.HelpID = HelpID;
            request.FullName = FullName;
            request.UserName = SavedBy;

            string defaultPICHead = string.Empty;
            DataTable dtPIC = clsUser.GetDefaultPICHead(userData.OrgID, userData.SiteID, HelpID);

            request.WorkedBy = dtPIC.Rows[0]["UserName"].ToString();

            //if (Request.QueryString["CaseNumber"] != null)
            //{
            //    DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
            //    request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //}

            /*InsertCaseResponse crmcase = crmclass.InsertCase(request);*/
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                DateTime? OverdueDate = dbcase.OverdueDate;

                int CaseStatusID = 2;
                if (ddlPaymentScheme.SelectedValue.ToString() == "2")
                {
                    if (lbl_total.Text.ToString() != "0")
                    {
                        CaseStatusID = 1;
                    }
                }


                DataTable container = new DataTable();
                DataRow dr;
                container.Columns.Add(new System.Data.DataColumn("HelpItemID", typeof(int)));
                container.Columns.Add(new System.Data.DataColumn("Quantity", typeof(int)));
                container.Columns.Add(new System.Data.DataColumn("Amount", typeof(decimal)));
                container.Columns.Add(new System.Data.DataColumn("TotalAmount", typeof(decimal)));

                foreach (GridViewRow row in gvw_data.Rows)
                {
                    dr = container.NewRow();
                    dr[0] = int.Parse(row.Cells[3].Text.ToString());
                    dr[1] = int.Parse(row.Cells[4].Text.ToString());
                    dr[2] = decimal.Parse(row.Cells[5].Text.ToString().Replace(",", ""));
                    dr[3] = decimal.Parse(row.Cells[6].Text.ToString().Replace(",", ""));
                    container.Rows.Add(dr);
                }

                /*SaveOtherIncome sudah ada save ticket history*/
                String returnvalue = residential.web.Classes.clsOtherIncome.SaveOtherIncome(OrgID, SiteID, CRMCaseID, CaseNumber, CaseStatusID, HelpID,
                    CaseOriginID, entryheader.CustomerType, ddlPaymentScheme.SelectedValue.ToString(), PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, SavedBy, FullName, decimal.Parse(lbl_subtotal.Text.ToString()), decimal.Parse(lbl_total.Text.ToString()), container);

                if (returnvalue != "0")
                {
                    //error save
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    //////kirim email disini                    
                    try
                    {
                        DataTable dt = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                        } 
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    }
                }

                /*try
                {
                    UpdateCaseHelpDeskStatusRequest upd = new UpdateCaseHelpDeskStatusRequest();
                    upd.CRMCaseNumber = CaseNumber;
                    upd.CRMCaseStatusID = CaseStatusID;
                    upd.Remarks = "";
                    upd.UpdateBy = userData.FullName;
                    upd.UpdateDate = DateTime.Now;
                    upd.WorkedBy = string.Empty;
                    upd.SatisfactionID = 1;
                    UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(upd);

                    //dbclass.save_tickethistory(CaseNumber, 2, FullName, null, SavedBy, null);
                }
                catch (Exception e)
                {
                    entryheader.ShowAlert = e.Message;
                }*/
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            //return dt.Rows[0]["CRMHelpID"].ToString();
            return dt.Rows[0]["HelpID"].ToString();
        }

        protected void cbo_servicerequests_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_servicerequests.SelectedIndex > -1)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                UserData userData = (UserData)ViewState["UserData"];

                DataSet ds = residential.web.Classes.clsOtherIncome.HelpNameItemList(userData.OrgID, userData.SiteID, int.Parse(cbo_servicerequests.SelectedValue.ToString()));

                ddlItem.DataSource = ds.Tables[1];
                ddlItem.DataValueField = "HelpItemID";
                ddlItem.DataTextField = "HelpItemName";
                ddlItem.DataBind();

                ddlPaymentScheme.DataSource = ds.Tables[0];
                ddlPaymentScheme.DataTextField = "PaymentSchemeDesc";
                ddlPaymentScheme.DataValueField = "PaymentSchemeID";
                ddlPaymentScheme.DataBind();

                gvw_data.DataSource = null;
                gvw_data.DataBind();
                upTable.Update();

                lbl_subtotal.Text = "0";

                hitung_total();

                ddlItem_SelectedIndexChanged(null, null);
            }
        }

        protected void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            show_requestprice();
        }

        private void show_requestprice()
        {
            if (cbo_servicerequests.SelectedIndex > -1)
            {
                if (ddlItem.SelectedIndex > -1)
                {
                    UserData userData = (UserData)ViewState["UserData"];

                    DataTable ds = residential.web.Classes.clsOtherIncome.PaymentSchemeByHelpPrice(userData.OrgID, userData.SiteID, int.Parse(ddlItem.SelectedValue.ToString()));

                    lbl_price.Text = string.Format("{0:n2}", ds.Rows[0]["Price"]);

                    HFHelpID.Value = ds.Rows[0]["HelpID"].ToString();
                }
                else
                {
                    lbl_price.Text = "0";
                    HFHelpID.Value = "";
                }
            }
        }

        private void hitung_total()
        {
            if (String.IsNullOrWhiteSpace(txt_discount.Text))
                txt_discount.Text = "0";

            Decimal disc = Decimal.Parse(txt_discount.Text);
            lbl_adjustment.Text = disc.ToString("N0");
            lbl_total.Text = (decimal.Parse(lbl_subtotal.Text) - disc).ToString("N0");
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string a = ddlItem.SelectedValue.ToString();

            if (a.Length > 0 && txtQuantity.Text.Length > 0 && txtQuantity.Text != "0")
            {
                ReadExistingItem();

                Items Items = new Items();

                Items.HelpItemID = int.Parse(ddlItem.SelectedValue.ToString());
                Items.HelpItemName = ddlItem.SelectedItem.Text.Substring(0, ddlItem.SelectedItem.Text.IndexOf(" @"));
                Items.Quantity = int.Parse(txtQuantity.Text.ToString());
                Items.Price = decimal.Parse(lbl_price.Text.ToString());
                Items.TotalPrice = int.Parse(txtQuantity.Text.ToString()) * decimal.Parse(lbl_price.Text.ToString());

                ListItems.Add(new Items(Items));

                gvw_data.DataSource = ListItems;
                gvw_data.DataBind();
                upTable.Update();

                decimal total = ListItems.Sum(item => item.TotalPrice);

                lbl_subtotal.Text = (total - decimal.Parse(lbl_adjustment.Text)).ToString("N0");

                hitung_total();
            }
            else
            {

            }
        }

        void ReadExistingItem()
        {
            foreach (GridViewRow dr in gvw_data.Rows)
            {
                Items Items = new Items();

                Items.HelpItemID = int.Parse(dr.Cells[3].Text.ToString());
                Items.HelpItemName = dr.Cells[2].Text.ToString();
                Items.Quantity = int.Parse(dr.Cells[4].Text.ToString());
                Items.Price = decimal.Parse(dr.Cells[5].Text.ToString().Replace(",", ""));
                Items.TotalPrice = decimal.Parse(dr.Cells[6].Text.ToString().Replace(",", ""));

                ListItems.Add(new Items(Items));
            }
        }

        protected void gvw_data_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ReadExistingItem();

            int index = Convert.ToInt32(e.RowIndex);

            if (ListItems.ElementAtOrDefault(index) != null)
            {
                ListItems.RemoveAt(index);
            }

            gvw_data.DataSource = ListItems;

            gvw_data.DataBind();

            decimal total = ListItems.Sum(item => item.TotalPrice);

            lbl_subtotal.Text = (total - decimal.Parse(lbl_adjustment.Text)).ToString("N0");

            hitung_total();
        }

        public class Items
        {
            public Items() { }
            public Items(Items Itm)
            {
                HelpItemID = Itm.HelpItemID;
                HelpItemName = Itm.HelpItemName;
                Quantity = Itm.Quantity;
                Price = Itm.Price;
                TotalPrice = Itm.TotalPrice;
            }
            public int HelpItemID { get; set; }
            public string HelpItemName { get; set; }
            public int Quantity { get; set; }
            public decimal Price { get; set; }
            public decimal TotalPrice { get; set; }
        }
    }
}