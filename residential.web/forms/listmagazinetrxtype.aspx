﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true"
    CodeBehind="listmagazinetrxtype.aspx.cs" Inherits="residential.web.forms.listmagazinetrxtype" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>List Magazine Type</h1>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-primary" NavigateUrl="~/forms/entrymagazinetrxtype.aspx">Add</asp:HyperLink>
    <asp:Button ID="btn_delete" runat="server" CssClass="btn btn-default" Text="Delete" OnClick="btn_delete_Click" OnClientClick="return confirmDelete();" />
    <hr />
    <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
        AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound"
        ShowHeaderWhenEmpty="True" OnPageIndexChanging="gvw_data_PageIndexChanging"
        DataKeyNames="MSMagazineTrxTypeID">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                        AutoPostBack="true" />
                </HeaderTemplate>
                <HeaderStyle CssClass="gridview_header" Width="30px" />
                <ItemStyle CssClass="gridview_item_center" />
                <ItemTemplate>
                    <asp:CheckBox ID="chk_select" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle CssClass="gridview_header" Width="30px" />
                <ItemStyle CssClass="gridview_item_center" />
                <ItemTemplate>
                    <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Magazine Trx Type Name">
                <HeaderStyle CssClass="gridview_header" />
                <ItemStyle CssClass="gridview_item_left" />
                <ItemTemplate>
                    <asp:HyperLink ID="lnk_magazinetrxtype" runat="server"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Price" DataField="Price" DataFormatString="{0:N0}">
                <HeaderStyle CssClass="gridview_header" />
                <ItemStyle CssClass="gridview_item_left" />
            </asp:BoundField>
        </Columns>
        <EmptyDataTemplate>
            No data
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
