﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryflyer.aspx.cs"
    Inherits="residential.web.forms.entryflyer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form Penyebaran Flyer</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <%--<div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>--%>
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Flyer Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Quantity x Price</label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txt_jumlah" runat="server" CssClass="form-control"
                                    OnTextChanged="txt_jumlah_TextChanged" Type="Number" MaxLength="7" AutoPostBack="True">1</asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                <p class="form-control-static">
                                    X&nbsp; 
                            <asp:Label ID="lbl_price" runat="server"></asp:Label>
                                </p>
                            </div>
                            <label class="col-sm-2 control-label">Adjustment</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_adjustment" CssClass="form-control" runat="server" AutoPostBack="True"
                                    OnTextChanged="txt_adjustment_TextChanged" Type="Number">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total Price</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_totalharga" runat="server"></asp:Label></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">PPN 10%</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_ppn" runat="server"></asp:Label></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" runat="server"></asp:Label></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_adjustment" runat="server"></asp:Label></p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" runat="server"></asp:Label></p>
                    </div>
                </div>
            </div>
            <%--<div style="border: 1px solid red;">
                <div class="formentrycontent">
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="Label4" runat="server" Text="Total Price" CssClass="style1"></asp:Label>
                    </div>
                    <div class="formentrycell2" style="text-align: right">
                         CssClass="style1"></asp:Label>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="Label2" runat="server" Text="PPN 10%" CssClass="style1"></asp:Label>
                    </div>
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="lbl_ppn" runat="server" CssClass="style1"></asp:Label>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="Label3" runat="server" Text="Sub Total" CssClass="style1"></asp:Label>
                    </div>
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="lbl_subtotal" runat="server" CssClass="style1"></asp:Label>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="Label7" runat="server" Text="Adjustment" CssClass="style1"></asp:Label>
                    </div>
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="lbl_adjustment" runat="server" CssClass="style1"></asp:Label>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="Label6" runat="server" Text="Total" CssClass="style1"></asp:Label>
                    </div>
                    <div class="formentrycell2" style="text-align: right">
                        <asp:Label ID="lbl_total" runat="server" CssClass="style1"></asp:Label>
                    </div>
                </div>
            </div>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
