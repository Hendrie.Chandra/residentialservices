﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using residential.web.model;
using System.Configuration;
using residential.libs;
using System.Data;
using FirebirdSql.Data.FirebirdClient;
using residential.web.Classes;
using System.Data.SqlClient;

namespace residential.web.forms
{


    public partial class entryuplsmartcard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                load_cardtype();
                cbo_cardtype.Items.Insert(0, new ListItem("--select card type--", "0"));
                load_card_status();

                ViewState["sortOrder"] = "";
                bounddatagrid("","");
            }
            errID.Visible = false;
            succID.Visible = false;
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            string Search = null;
            if (!String.IsNullOrEmpty(txt_search.Text))
            Search = txt_search.Text;

            //DataTable dt = dbclass.retrieve_uplsmartcard(Search);
            string cardstatus = "";
            if (cbo_status.SelectedItem.Value == "0")
                cardstatus = "All";
            else if (cbo_status.SelectedItem.Value == "1")
                cardstatus = "Blank";
            else if (cbo_status.SelectedItem.Value == "2")
                cardstatus = "Card Issued To Member";

            DataTable dt = dbclass.retrieve_uplsmartcard(OrgID, SiteID, Search, cardstatus);


            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("","");
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        //protected void txt_search_TextChanged(object sender, EventArgs e)
        //{
        //    bounddatagrid("", "");
        //}
        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void btn_saveDocument(object sender, EventArgs e)
        {
            string CardNumber, ChipNumber, SaveLocation,fn;
            string BoomGateCode = "";
            string UploadType = "";

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            if ((txt_fileUpload.PostedFile != null) && (txt_fileUpload.PostedFile.ContentLength > 0))
            {
                string _cardType = cbo_cardtype.SelectedItem.Value;
                string _cardTypeDesc = cbo_cardtype.SelectedItem.Text;
                string _uploadSource = "Upload " + _cardTypeDesc;
                fn = System.IO.Path.GetFileName(txt_fileUpload.PostedFile.FileName);
                SaveLocation = Server.MapPath("document") + "\\" + DateTime.Now.ToString("ddmmyyyy_HHmmss") + " " + fn;

                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                int counter = 0;

                if (_cardType == "0")
                {
                    LabelDanger.Text = "Please Select Card Type";
                    errID.Visible = true;
                    return;
                }
                else if (_cardType == "1")//{residen/member upload}
                {
                    try
                    {
                        txt_fileUpload.PostedFile.SaveAs(SaveLocation);
                        string[] lines = System.IO.File.ReadAllLines(SaveLocation);
                        List<UploadMSSmartCard> gcList = new List<UploadMSSmartCard>();

                        for (int i = 1; i < lines.Count(); i++)
                        {                            
                            string[] array = lines[i].Split(',');
                            if (array.Length == 3 || array.Length==2)//new format upload to boomgate
                            {
                                if (array.Length == 3)
                                {
                                    CardNumber = array[0];
                                    ChipNumber = array[1];
                                    BoomGateCode = array[2];
                                }
                                else
                                {
                                    //upload tidak ada boomgate
                                    CardNumber = array[0];
                                    ChipNumber = array[1];
                                    BoomGateCode = "0";
                                }
                            }
                            else
                            {
                                LabelDanger.Text = "Invalid upload format file";
                                errID.Visible = true;
                                return;
                            }

                            UploadMSSmartCard gc = new UploadMSSmartCard()
                            {
                                CardNumber = CardNumber,
                                ChipNumber = ChipNumber,
                                CardStatus = "Blank",
                                BoomGateCode = BoomGateCode,
                                IsActive = true
                            };

                            gcList.Add(gc);
                        }
                        foreach (var data in gcList.ToList())
                        {
                            string defaultCustomername = "";
                            string defaultAlamat = "";

                            int returnvalidate = dbclass.validate_upload_cardnumber(data.CardNumber);
                            if (returnvalidate <= 0)
                            {
                                try
                                {
                                    if (data.BoomGateCode != "0")
                                    {
                                        DataTable dt = dbclass.retrieve_boomgateID(OrgID, SiteID, data.BoomGateCode);
                                        if (dt.Rows.Count > 0)
                                        {
                                            //insert to boomgate 
                                            string boomgateCardType = "R"; //resident maupun additional card
                                            PushDataBoomGate.PushData(data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", boomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                            Int32 returnvalue = dbclass.save_uplmssmartcard(OrgID, SiteID, data.CardNumber, data.ChipNumber, data.CardStatus, data.IsActive, SavedBy);
                                            //pushdata log
                                            int pushdatalog = insert_pushdata_log("Upload Resident", data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "R", data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukkses

                                            //start Penambahan Marge BoomGate 6 sep 2017
                                            DataTable dt_margeBoomGate = clsSmartCard.RetrieveDestinationMargeBoomGate(data.BoomGateCode);
                                            if (dt_margeBoomGate.Rows.Count > 0)
                                            {
                                                string margeBoomGateCode = "";
                                                try
                                                {
                                                    margeBoomGateCode = dt_margeBoomGate.Rows[0]["DestinationBoomGateCode"].ToString();
                                                    PushDataBoomGate.PushData(margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", boomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                                    int margepushdatalog = insert_pushdata_log("Upload Resident", margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukses
                                                }
                                                catch (Exception exMarge)
                                                {
                                                    int margepushdatalog = insert_pushdata_log("Upload Resident", margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + exMarge.Message.ToString(), 0);//log gagal
                                                }
                                            }
                                            //end Penambahan Marge BoomGate

                                            counter++;
                                        }
                                    }
                                    else
                                    {
                                        //tidak ada boomgate, langsung insert db, upload tanpa boomgate
                                        Int32 returnvalue = dbclass.save_uplmssmartcard(OrgID, SiteID, data.CardNumber, data.ChipNumber, data.CardStatus, data.IsActive, SavedBy);

                                        //pushdata log
                                        int pushdatalog = insert_pushdata_log("Upload Resident", data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "R", data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukkses

                                        counter++;
                                    }
                                }
                                catch (Exception fdbm)
                                {
                                    //int retunpushdata = dbclass.save_pushdata_error(data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", fdbm.Message.ToString());
                                    Int32 returnvalue = dbclass.save_uplmssmartcard(OrgID, SiteID, data.CardNumber, data.ChipNumber, data.CardStatus, data.IsActive, SavedBy);
                                    //pushdata log
                                    int pushdatalog = insert_pushdata_log("Upload Resident", data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "R", data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + fdbm.Message.ToString(), 0);//log error
                                    //start Penambahan Marge BoomGate 6 sep 2017
                                    DataTable dt_margeBoomGate = clsSmartCard.RetrieveDestinationMargeBoomGate(data.BoomGateCode);
                                    if (dt_margeBoomGate.Rows.Count > 0)
                                    {
                                        string margeBoomGateCode = "";
                                        try
                                        {
                                            margeBoomGateCode = dt_margeBoomGate.Rows[0]["DestinationBoomGateCode"].ToString();
                                            PushDataBoomGate.PushData(margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", "R", data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                            int margepushdatalog = insert_pushdata_log("Upload Resident", margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "R", data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukses
                                        }
                                        catch (Exception exMarge)
                                        {
                                            int margepushdatalog = insert_pushdata_log("Upload Resident", margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "R", data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + exMarge.Message.ToString(), 0);//log gagal
                                        }
                                    }
                                    //end Penambahan Marge BoomGate
                                    counter++;
                                }
                            }

                            //add 6 sept 2017
                            #region 6 sept 2017
                            //kartu sudah ada di tmdb, push ke fire bird langsung
                            if (returnvalidate > 0)
                            {
                                //jika kartu sudah ada push ke gate saja
                                try
                                {
                                    if (data.BoomGateCode != "0")
                                    {
                                        DataTable dt = dbclass.retrieve_boomgateID(OrgID, SiteID, data.BoomGateCode);
                                        if (dt.Rows.Count > 0)
                                        {
                                            //insert to boomgate
                                            string boomgateCardType = "R"; //resident maupun additional card
                                            PushDataBoomGate.PushData(data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", boomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());

                                            //pushdata log
                                            int pushdatalog = insert_pushdata_log("Upload Resident", data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "R", data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukkses

                                            counter++;
                                        }
                                    }
                                    else
                                    {
                                        //tidak ada boomgate, langsung insert db, upload tanpa boomgate
                                        //pushdata log
                                        int pushdatalog = insert_pushdata_log("Upload Resident", data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "R", data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukkses

                                        counter++;
                                    }
                                }
                                catch (Exception fdbm)
                                {
                                    //int retunpushdata = dbclass.save_pushdata_error(data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", fdbm.Message.ToString());
                                    Int32 returnvalue = dbclass.save_uplmssmartcard(OrgID, SiteID, data.CardNumber, data.ChipNumber, data.CardStatus, data.IsActive, SavedBy);
                                    //pushdata log
                                    int pushdatalog = insert_pushdata_log("Upload Resident", data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "R", data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + fdbm.Message.ToString(), 0);//log error
                                    counter++;
                                }
                            }
                            #endregion
                            //end add 6 sep 2017
                        }
                        //LabelSuccess.Text = "Upload Success!";
                        LabelSuccess.Text = counter + " record data uploded!";
                        succID.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        //Response.Write("Error: " + ex.Message);
                        LabelDanger.Text = "Upload Error!" + ex.Message;
                        errID.Visible = true;
                    }
                }
                else if (_cardType == "2" || _cardType=="4" || _cardType=="5")
                {
                    //visitor
                    try
                    {
                        txt_fileUpload.PostedFile.SaveAs(SaveLocation);
                        string[] lines = System.IO.File.ReadAllLines(SaveLocation);
                        List<UploadMSSmartCardVisitor> gcListVisitor = new List<UploadMSSmartCardVisitor>();

                        for (int i = 1; i < lines.Count(); i++)
                        {
                            string[] array = lines[i].Split(',');
                            
                            if (array.Length != 4)//add for visitor field in template upload
                            {
                                LabelDanger.Text = "Invalid upload format file";
                                errID.Visible = true;
                                return;
                            }

                            CardNumber = array[0];
                            ChipNumber = array[1];
                            BoomGateCode = array[2];
                            UploadType = array[3].ToUpper();  //new request, Kartu Master [X], Member[M]

                            //Validasi file yang di upload dengan pilihan kartu yang di upload di dropdown list
                            if (_cardType == "2" && UploadType != "V")//visitor
                            {
                                LabelDanger.Text = "Invalid upload format file Type";
                                errID.Visible = true;
                                return;
                            }
                            else if (_cardType == "4" && UploadType != "M")//member
                            {
                                LabelDanger.Text = "Invalid upload format file Type";
                                errID.Visible = true;
                                return;
                            }
                            else if (_cardType == "5" && UploadType != "X")//master
                            {
                                LabelDanger.Text = "Invalid upload format file Type";
                                errID.Visible = true;
                                return;
                            }                                

                            UploadMSSmartCardVisitor gc = new UploadMSSmartCardVisitor()
                            {
                                CardNumber = CardNumber,
                                ChipNumber = ChipNumber,
                                CardStatus = "Card Issued To Member",
                                BoomGateCode = BoomGateCode,
                                IsActive = true,
                                UploadType = UploadType
                            };

                            gcListVisitor.Add(gc);
                        }
                        foreach (var data in gcListVisitor.ToList())
                        {
                            string defaultCustomername = "";
                            string defaultAlamat = "";
                            string boomgateCardType = data.UploadType;

                            int returnvalidate = dbclass.validate_upload_cardnumber(data.CardNumber);
                            if (returnvalidate <= 0)
                            {                                
                                try
                                {
                                    DataTable dt = dbclass.retrieve_boomgateID(OrgID, SiteID, data.BoomGateCode);
                                    if (dt.Rows.Count > 0)
                                    {                                        

                                        PushDataBoomGate.PushData(data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", boomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                        //pushdata log
                                        int pushdatalog = insert_pushdata_log(_uploadSource, data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukkses                                      
                                        Int32 membershipID = clsSmartCard.save_uplmssmartcard_nonresident(OrgID, SiteID, data.CardNumber, data.ChipNumber, null, data.CardStatus, true, SavedBy,int.Parse(_cardType));
                                        if (membershipID != null)
                                        {
                                            int boomGateID = (int)dt.Rows[0]["BoomGateID"];
                                            string returnval_memberhip = dbclass.insert_membership_transaction(membershipID, boomGateID, 1, 1); //sync success

                                            counter++;
                                        }

                                        //start Penambahan Marge BoomGate 6 sep 2017
                                        DataTable dt_margeBoomGate = clsSmartCard.RetrieveDestinationMargeBoomGate(data.BoomGateCode);
                                        if (dt_margeBoomGate.Rows.Count > 0)
                                        {
                                            string margeBoomGateCode = "";
                                            try
                                            {
                                                margeBoomGateCode = dt_margeBoomGate.Rows[0]["DestinationBoomGateCode"].ToString();
                                                PushDataBoomGate.PushData(margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", boomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                                int margepushdatalog = insert_pushdata_log(_uploadSource, margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukses
                                            }
                                            catch (Exception exMarge)
                                            {
                                                int margepushdatalog = insert_pushdata_log(_uploadSource, margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + exMarge.Message.ToString(), 0);//log gagal
                                            }
                                        }
                                        //end Penambahan Marge BoomGate
                                    }                                  
                                }
                                catch (Exception fdb)
                                {
                                    //pushdata log
                                    int pushdatalog = insert_pushdata_log(_uploadSource, data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + fdb.Message.ToString(), 0);//log error

                                    DataTable dt = dbclass.retrieve_boomgateID(OrgID, SiteID, data.BoomGateCode);
                                    if (dt.Rows.Count > 0)
                                    {
                                        //Int32 membershipID = dbclass.save_uplmssmartcard_visitor(OrgID, SiteID, data.CardNumber, data.ChipNumber, null, data.CardStatus, true, true, SavedBy);
                                        Int32 membershipID = clsSmartCard.save_uplmssmartcard_nonresident(OrgID, SiteID, data.CardNumber, data.ChipNumber, null, data.CardStatus, true, SavedBy, int.Parse(_cardType));
                                        if (membershipID != null)
                                        {
                                            int boomGateID = (int)dt.Rows[0]["BoomGateID"];
                                            string returnval_memberhip = dbclass.insert_membership_transaction(membershipID, boomGateID, 1, 0); //sync failed
                                            counter++;
                                        }
                                    }

                                    //start Penambahan Marge BoomGate 6 sep 2017
                                    DataTable dt_margeBoomGate = clsSmartCard.RetrieveDestinationMargeBoomGate(data.BoomGateCode);
                                    if (dt_margeBoomGate.Rows.Count > 0)
                                    {
                                        string margeBoomGateCode = "";
                                        try
                                        {
                                            margeBoomGateCode = dt_margeBoomGate.Rows[0]["DestinationBoomGateCode"].ToString();
                                            PushDataBoomGate.PushData(margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", boomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                            int margepushdatalog = insert_pushdata_log(_uploadSource, margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukses
                                        }
                                        catch (Exception exMarge)
                                        {
                                            int margepushdatalog = insert_pushdata_log(_uploadSource, margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + exMarge.Message.ToString(), 0);//log gagal
                                        }
                                    }
                                    //end Penambahan Marge BoomGate
                                }
                            }

                            #region 6 sept 2017
                            if (returnvalidate > 0)
                            {
                                //push ke boomgate sajah
                                try
                                {
                                    DataTable dt = dbclass.retrieve_boomgateID(OrgID, SiteID, data.BoomGateCode);
                                    if (dt.Rows.Count > 0)
                                    {
                                        PushDataBoomGate.PushData(data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", boomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                        //pushdata log
                                        int pushdatalog = insert_pushdata_log(_uploadSource, data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukkses                                      
                                        counter++;
                                    }
                                }
                                catch (Exception fdb)
                                {
                                    //pushdata log
                                    int pushdatalog = insert_pushdata_log(_uploadSource, data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, boomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + fdb.Message.ToString(), 0);//log error
                                    counter++;
                                }
                            }
                            #endregion 6 sept 2017
                        }
                        LabelSuccess.Text = counter + " record data uploded!";
                        succID.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        LabelDanger.Text = "Upload Error!" + ex.Message;
                        errID.Visible = true;
                    }
                }
                else if (_cardType == "3")//vvip
                {
                    try
                    {
                        txt_fileUpload.PostedFile.SaveAs(SaveLocation);
                        string[] lines = System.IO.File.ReadAllLines(SaveLocation);
                        List<UploadMSSmartCard> gcList = new List<UploadMSSmartCard>();

                        for (int i = 1; i < lines.Count(); i++)
                        {
                            string[] array = lines[i].Split(',');

                            if (array.Length != 3)//new format upload to boomgate
                            {
                                LabelDanger.Text = "Invalid upload format file";
                                errID.Visible = true;
                                return;
                            }

                            CardNumber = array[0];
                            ChipNumber = array[1];
                            BoomGateCode = array[2];

                            UploadMSSmartCard gc = new UploadMSSmartCard()
                            {
                                CardNumber = CardNumber,
                                ChipNumber = ChipNumber,
                                CardStatus = "Card Issued To Member",
                                BoomGateCode = BoomGateCode,
                                IsActive = true
                            };

                            gcList.Add(gc);
                        }
                        foreach (var data in gcList.ToList())
                        {
                            string defaultCustomername = "";
                            string defaultAlamat = "";
                            string vvipboomgateCardType = "R"; //resident maupun additional card
                            //int returnvalidate = dbclass.validate_upload_cardnumber(data.CardNumber);
                            //if (returnvalidate <= 0)
                            //{
                                try
                                {
                                    DataTable dt = dbclass.retrieve_boomgateID(OrgID, SiteID, data.BoomGateCode);

                                    if (dt.Rows.Count > 0)
                                    {
                                        //insert to boomgate
                                        PushDataBoomGate.PushData(data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", vvipboomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                        //pushdata log
                                        int pushdatalog = insert_pushdata_log("Upload VVIP", data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, vvipboomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukkses
                                        DataTable dt_vvip = clsSmartCard.save_uplmssmartcard_vvip(OrgID, SiteID,data.CardNumber,data.ChipNumber,null,data.CardStatus,data.IsActive,SavedBy,int.Parse(_cardType));
                                        if (dt_vvip.Rows.Count > 0)
                                        {
                                            int boomgateIDVvip = (int)dt.Rows[0]["BoomGateID"];
                                            int membershipIDVvip = (int)dt_vvip.Rows[0]["MembershipID"];
                                            string returnval_memberhip = dbclass.insert_membership_transaction(membershipIDVvip, boomgateIDVvip, 1, 1); //sync success                                                                                        
                                            counter++;
                                        }

                                        //start Penambahan Marge BoomGate 6 sep 2017
                                        DataTable dt_margeBoomGate = clsSmartCard.RetrieveDestinationMargeBoomGate(data.BoomGateCode);
                                        if (dt_margeBoomGate.Rows.Count > 0)
                                        {
                                            string margeBoomGateCode = "";
                                            try
                                            {
                                                margeBoomGateCode = dt_margeBoomGate.Rows[0]["DestinationBoomGateCode"].ToString();
                                                PushDataBoomGate.PushData(margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, "UPDATE", vvipboomgateCardType, data.CardNumber, null, "", DateTime.Now.ToLocalTime());
                                                int margepushdatalog = insert_pushdata_log("Upload VVIP", margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, vvipboomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "Success", 1);//log sukses
                                            }
                                            catch (Exception exMarge)
                                            {
                                                int margepushdatalog = insert_pushdata_log("Upload VVIP", margeBoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, vvipboomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + exMarge.Message.ToString(), 0);//log gagal
                                            }
                                        }
                                        //end Penambahan Marge BoomGate
                                    }
                                }
                                catch (Exception fdbv)
                                {                                   
                                    //pushdata log
                                    int pushdatalog = insert_pushdata_log("Upload VVIP", data.BoomGateCode, data.ChipNumber, defaultCustomername, defaultAlamat, vvipboomgateCardType, data.CardNumber, "UPDATE", null, null, DateTime.Now.ToLocalTime(), "failed with message : " + fdbv.Message.ToString(), 0);//log error                                    
                                    DataTable dt = dbclass.retrieve_boomgateID(OrgID, SiteID, data.BoomGateCode);

                                    if (dt.Rows.Count > 0)
                                    {
                                        DataTable dt_vvip = clsSmartCard.save_uplmssmartcard_vvip(OrgID, SiteID, data.CardNumber, data.ChipNumber, null, data.CardStatus, data.IsActive, SavedBy, int.Parse(_cardType));
                                        if (dt_vvip.Rows.Count > 0)
                                        {
                                            int boomgateIDVvip = (int)dt.Rows[0]["BoomGateID"];
                                            int membershipIDVvip = (int)dt_vvip.Rows[0]["MembershipID"];
                                            string returnval_memberhip = dbclass.insert_membership_transaction(membershipIDVvip, boomgateIDVvip, 1, 0); //sync failled
                                            counter++;
                                        }
                                    }                                                                     
                                }
                            //}
                        }
                        //LabelSuccess.Text = "Upload Success!";
                        LabelSuccess.Text = counter + " record data uploded!";
                        succID.Visible = true;
                    }
                    catch (Exception ex)
                    {
                        //Response.Write("Error: " + ex.Message);
                        LabelDanger.Text = "Upload Error!" + ex.Message;
                        errID.Visible = true;
                    }
                }
            }
            else
            {
                //Response.Write("Please select a file to upload.");
                LabelDanger.Text = "Please select a file to upload.";
                errID.Visible = true;
            }
            bounddatagrid("", "");
        }

        protected void btn_search_Click1(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void cbo_status_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void load_card_status()
        {
            Dictionary<string, string> cardstatus = new Dictionary<string, string>();
            cardstatus.Add("0", "All");
            cardstatus.Add("1", "Blank");
            cardstatus.Add("2", "Card Issued To Member");

            cbo_status.DataSource = cardstatus;
            cbo_status.DataTextField = "Value";
            cbo_status.DataValueField = "Key";
            cbo_status.DataBind();
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        //protected void load_cardtype() 
        //{
        //    Dictionary<string, string> cardType = new Dictionary<string, string>();
        //    cardType.Add("0", "--select card type--");
        //    cardType.Add("1", "Resident / Additional Card");
        //    cardType.Add("2", "Visitor");
        //    cardType.Add("3", "VVIP");

        //    cbo_cardtype.DataSource = cardType;
        //    cbo_cardtype.DataValueField = "Key";
        //    cbo_cardtype.DataTextField = "Value";
        //    cbo_cardtype.DataBind();
        //}

        protected void load_cardtype()
        {
            DataTable dt = clsSmartCard.RetrieveUploadType();

            cbo_cardtype.DataSource = dt;
            cbo_cardtype.DataValueField = "UploadTypeID";
            cbo_cardtype.DataTextField = "UploadTypeDesc";
            cbo_cardtype.DataBind();
        }

        //log boomgate pushdata
        public int insert_pushdata_log(string _sourcePush, string _boom, string _mifare, string _nama, string _alamat, string _jenis, string _cardno, string _status, DateTime? _kadaluarsa, string _psCode, DateTime _issued, string _msgPush, int _flag)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();

            SqlConnection conn = new SqlConnection(constring);
            SqlCommand cmd = new SqlCommand("SP_Save_PushDataLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SourcePush", _sourcePush);
            cmd.Parameters.AddWithValue("@Boom", _boom);
            cmd.Parameters.AddWithValue("@Mifare", _mifare);
            cmd.Parameters.AddWithValue("@Nama", _nama);
            cmd.Parameters.AddWithValue("@Alamat", _alamat);
            cmd.Parameters.AddWithValue("@Jenis", _jenis);
            cmd.Parameters.AddWithValue("@CardNo", _cardno);
            cmd.Parameters.AddWithValue("@Status", _status);
            cmd.Parameters.AddWithValue("@Kadaluarsa", _kadaluarsa);
            cmd.Parameters.AddWithValue("@PsCode", _psCode);
            cmd.Parameters.AddWithValue("@Issued", _issued);
            cmd.Parameters.AddWithValue("@MessagePush", _msgPush);
            cmd.Parameters.AddWithValue("@Flag", _flag);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue; 
        }
    }
}