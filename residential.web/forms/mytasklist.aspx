﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true"
    CodeBehind="mytasklist.aspx.cs" Inherits="residential.web.forms.mytasklist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>Tasklist</h1>
            </div>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalAssign">Assign</button>
            <hr />
            <div class="alert alert-success alert-dismissible" id="alert_success" runat="server" visible="false">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <asp:Label runat="server" ID="alert_success_text"></asp:Label>
            </div>
            <div class="alert alert-danger alert-dismissible" id="alert_danger" runat="server" visible="false">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <asp:Label runat="server" ID="alert_danger_text"></asp:Label>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8 col-sm-8">
                        <label class="col-sm-2 control-label">Case Status</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="cbo_status" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="cbo_status_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" CssClass="form-control" runat="server"
                                OnTextChanged="txt_search_TextChanged" AutoPostBack="true" placeholder="case number, unit"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="..." CssClass="btn btn-default" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table table-responsive">
                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover"
                    AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound"
                    ShowHeaderWhenEmpty="True" DataKeyNames="CaseNumber" EmptyDataText="No Data"
                    AllowSorting="True" OnSorting="gvw_data_Sorting" OnPageIndexChanging="gvw_data_PageIndexChanging"
                    OnRowCommand="gvw_data_RowCommand" HeaderStyle-HorizontalAlign="Center">
                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                                    AutoPostBack="true" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk_select" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true"></asp:BoundField>
                        <asp:BoundField HeaderText="UNIT" DataField="Unit" SortExpression="UnitCode"></asp:BoundField>
                        <asp:BoundField HeaderText="CATEGORY" DataField="HelpCategoryName" SortExpression="HelpCategoryName"></asp:BoundField>
                        <asp:BoundField HeaderText="HELP NAME" DataField="HelpName" SortExpression="HelpName"></asp:BoundField>
                        <asp:BoundField HeaderText="CREATED" DataField="RequestDate" SortExpression="RequestDate" DataFormatString="{0:d MMMM yyyy}"></asp:BoundField>
                        <asp:BoundField HeaderText="OVERDUE" DataField="OverdueDate" SortExpression="OverdueDate" DataFormatString="{0:d MMMM yyyy}"></asp:BoundField>
                        <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" SortExpression="CaseStatusName"></asp:BoundField>
                        <asp:BoundField DataField="AssignTo" HeaderText="ASSIGN TO"></asp:BoundField>
                        <asp:BoundField DataField="DepartmentID" HeaderText="DepartmentID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </div>
            <div class="modal fade" id="modalAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Assign Case</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Assign To</label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList ID="cbo_assign" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="btn btn-default" data-dismiss="modal" />
                            <asp:Button ID="btn_assigning" runat="server" Text="ASSIGN" CssClass="btn btn-primary" OnClick="btn_assigning_Click" UseSubmitBehavior="false" data-dismiss="modal" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
