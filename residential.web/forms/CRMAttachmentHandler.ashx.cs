﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Xrm.Sdk;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk.Client;
using System.Configuration;
using Microsoft.Xrm.Sdk.Query;
using System.IO;

namespace residential.web.forms
{
    /// <summary>
    /// Summary description for CRMAttachmentHandler
    /// </summary>
    public class CRMAttachmentHandler : IHttpHandler
    {
        private IOrganizationService crmservice()
        {
            Uri uriorg = new Uri(ConfigurationManager.AppSettings["CRMUrlOrg"].ToString());
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential.Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            credentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            credentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            OrganizationServiceProxy serviceproxy = new OrganizationServiceProxy(uriorg, null, credentials, null);
            IOrganizationService service;
            service = (IOrganizationService)serviceproxy;

            return service;
        }

        public void ProcessRequest(HttpContext context)
        {
            Guid AnnotationID = Guid.Parse(context.Request.QueryString["AnnotationID"].ToString());
            Entity annotation = crmservice().Retrieve("annotation", AnnotationID, new ColumnSet(new String[] { "mimetype", "filename", "documentbody" }));
            byte[] fileContent = Convert.FromBase64String(annotation.Attributes["documentbody"].ToString());

            context.Response.Clear();
            context.Response.ContentType = annotation.Attributes["mimetype"].ToString();
            context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + annotation.Attributes["filename"].ToString());
            context.Response.Buffer = true;
            MemoryStream ms = new MemoryStream(fileContent);
            ms.WriteTo(context.Response.OutputStream);
            context.Response.Flush();
            context.Response.End();
        }     

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}