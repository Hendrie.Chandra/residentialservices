﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk.Query;
using residential.libs.Models;
using System.IO;

namespace residential.web.forms
{
    public partial class viewbcd : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                startandenddate.Visible = false;
                show_case(Request.QueryString["CaseNumber"]);                             
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_BCD(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                lbl_servicerequest.Text = dt.Rows[0]["HelpName"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["StartDate"].ToString()) && !String.IsNullOrEmpty(dt.Rows[0]["EndDate"].ToString()))
                {
                    DateTime StartDate = (DateTime)dt.Rows[0]["StartDate"];
                    DateTime EndDate = (DateTime)dt.Rows[0]["EndDate"];
                    lbl_startdate.Text = StartDate.ToString("dd MMMM yyyy");
                    lbl_enddate.Text = EndDate.ToString("dd MMMM yyyy");
                    startandenddate.Visible = true;
                }

                Decimal price = Decimal.Parse(dt.Rows[0]["Price"].ToString());
                lbl_quantityprice.Text = price.ToString("N0");

                Decimal discount = Decimal.Parse(dt.Rows[0]["TotalDiscount"].ToString());
                lbl_disc.Text = discount.ToString("N0");

                Decimal subtotal = Decimal.Parse(dt.Rows[0]["TotalAmount"].ToString());
                lbl_subtotal.Text = subtotal.ToString("N0");

                lbl_adjustment.Text = discount.ToString("N0");

                Decimal total = Decimal.Parse(dt.Rows[0]["TotalAmountAfterDiscount"].ToString());
                lbl_total.Text = total.ToString("N0");
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }               
    }
}