﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listbcdprice.aspx.cs" Inherits="residential.web.forms.listbcdprice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>List BCD Price</h1>
    </div>        
    <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
        CssClass="table table-hover table-bordered" AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound"
        ShowHeaderWhenEmpty="True"
        DataKeyNames="HelpID" AllowSorting="True" OnSorting="gvw_data_Sorting"
        OnPageIndexChanging="gvw_data_PageIndexChanging" PagerStyle-CssClass="pagination-ys">
        <Columns>
            <asp:TemplateField HeaderText="NO">
                <HeaderStyle CssClass="gridview_header" Width="30px" />
                <ItemStyle CssClass="gridview_item_center" />
                <ItemTemplate>
                    <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:BoundField HeaderText="HELP NAME" DataField="HelpName" SortExpression="HelpName">
                <HeaderStyle CssClass="gridview_header" />
                <ItemStyle CssClass="gridview_item_left"/>
            </asp:BoundField>
            <asp:BoundField HeaderText="PRICE" DataField="Price" DataFormatString="{0:N0}">
                <HeaderStyle CssClass="gridview_header" />
                <ItemStyle CssClass="gridview_item_left" />
            </asp:BoundField>            
        </Columns>
        <EmptyDataTemplate>
            No data
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
