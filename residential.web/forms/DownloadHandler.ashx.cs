﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace residential.web.forms
{
    /// <summary>
    /// Summary description for DownloadHandler
    /// </summary>
    public class DownloadHandler : IHttpHandler
    {            
        public void ProcessRequest(HttpContext context)
        {            
            String FileName = context.Request.QueryString["FileName"].ToString();
            String CaseNumber = context.Request.QueryString["CaseNumber"].ToString();
            String FileExtension = Path.GetExtension(FileName);            
            String FileLocation = context.Server.MapPath("~/caseattachment/" + CaseNumber + "/" + FileName);

            String MIMEType = "";
            switch (FileExtension)
            {
                case ".xls": MIMEType = "application/vnd.ms-excel"; break;
                case ".xlsx": MIMEType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; break;
                case ".doc": MIMEType = "application/msword"; break;
                case ".docx": MIMEType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; break;
                case ".pdf": MIMEType = "application/pdf"; break;
                case ".jpg":
                case "jpeg": MIMEType = "image/jpeg"; break;
                case ".bmp": MIMEType = "image/bmp"; break;
                case ".png": MIMEType = "image/png"; break;
                case ".gif": MIMEType = "image/gif"; break;
            }

            context.Response.Clear();
            context.Response.ContentType = MIMEType;
            context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FileLocation));
            context.Response.WriteFile(FileLocation);
            context.Response.Flush();
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}