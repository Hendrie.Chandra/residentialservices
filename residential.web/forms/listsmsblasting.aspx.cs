﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.IO;

namespace residential.web.forms
{
    public partial class smsblasting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";
                show_cbo_casestatus();
                bounddatagrid("", "");            
            }
        }        

        private void show_cbo_casestatus()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_casestatus();
            cbo_status.DataSource = dt;
            cbo_status.DataTextField = "CaseStatusName";
            cbo_status.DataValueField = "CaseStatusID";
            cbo_status.DataBind();
            cbo_status.Items.Insert(0, new ListItem("[ All Status ]", "0"));
            cbo_status.SelectedIndex = 0;
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {                
                String CaseNumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();
                String CaseStatus = DataBinder.Eval(e.Row.DataItem, "CaseStatusName").ToString();
                Int32 CaseStatusID = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "CaseStatusID").ToString());
                String linkToForm = "viewsmsblast.aspx";
                if (CaseStatus.ToUpper() == "PENDING")
                    linkToForm = "entrysmsblasting.aspx";

                e.Row.Attributes["onclick"] = "window.location = '" + linkToForm + "?CaseNumber=" + CaseNumber + "'";

                if (!String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString()) && (CaseStatusID == 2 || CaseStatusID == 4))
                {
                    DateTime OverdueDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString());
                    if (DateTime.Compare(DateTime.Now, OverdueDate) >= 0)
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                }

                LinkButton lnk_detail = (LinkButton)e.Row.FindControl("lnk_detail");
                lnk_detail.Text = "Recipient";
                lnk_detail.CommandArgument = CaseNumber;
                ScriptManager.GetCurrent(this).RegisterPostBackControl(lnk_detail);
            }
        }
        
        protected void cbo_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            Int32 CaseStatusID = Int32.Parse(cbo_status.SelectedValue);
            String Search = null;
            if (!String.IsNullOrEmpty(txt_search.Text))
                Search = txt_search.Text;

            DataTable dt = dbclass.retrieve_SMSBlast(userData.OrgID, userData.SiteID, CaseStatusID, Search);
            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        public void ExportToExcel(DataTable dt, String _caseNumber)
        {
            if (dt.Rows.Count > 0)
            {
                String FileName = "SMS Blast - " + _caseNumber + ".xls";
                String FilePath = Server.MapPath("~/outputxls/") + FileName;
                StringWriter stringWriter = new StringWriter();
                HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWriter);
                DataGrid DataGrd = new DataGrid();
                DataGrd.DataSource = dt;
                DataGrd.DataBind();

                foreach (DataGridItem item in DataGrd.Items)
                {
                    for (int i = 0; i < item.Cells.Count; i++) // loop through all the cells in grid
                        item.Cells[i].Attributes.Add("style", "mso-number-format:\\@");
                }

                DataGrd.RenderControl(htmlWrite);
                string directory = FilePath.Substring(0, FilePath.LastIndexOf("\\"));// GetDirectory(Path);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                System.IO.StreamWriter vw = new System.IO.StreamWriter(FilePath, true);
                stringWriter.ToString().Normalize();
                vw.Write(stringWriter.ToString());
                vw.Flush();
                vw.Close();
            }
        }

        protected void DownloadFile(object sender, EventArgs e)
        {            
            String CaseNumber = (sender as LinkButton).CommandArgument;
            String FilePath = Server.MapPath("~/outputxls/SMS Blast - " + CaseNumber + ".xls");

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_SMSBlastNumberDetail(CaseNumber);

            ExportToExcel(dt, CaseNumber);
            
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.Flush();
            System.IO.File.Delete(FilePath);
            Response.End();            
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }        
    }
}