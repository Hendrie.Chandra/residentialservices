﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using residential.web.Classes;

namespace residential.web.forms
{
    public class ReBannerDetail
    {
        public Int32 LineID { get; set; }
        public Int32 LocationID { get; set; }
        public string LocationName { get; set; }
        public Int32 PointID { get; set; }
        public string PointName { get; set; }
        public DateTime ChangeDate { get; set; }

        public ReBannerDetail() { }

        public ReBannerDetail(Int32 lineID, Int32 locationID, string locationName, Int32 pointID, string pointName, DateTime changeDate) 
        {
            this.LineID = lineID;
            this.LocationID = locationID;
            this.LocationName = locationName;
            this.PointID = pointID;
            this.PointName = pointName;
            this.ChangeDate = changeDate;
        }
    }

    public partial class entryrebanner : System.Web.UI.Page
    {
        static String prevPage = "rebanner.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                Session["ListReBannerDetail"] = null;
                Session["StartDate"] = null;
                Session["EndDate"] = null;

                show_type();                
                hitung_total();

                gvw_data.DataBind();
                gvw_detail.DataBind();                
                
                entryheader.disable_all_controls();
                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            entryheader.ToggleAlert = false;

            if (Request.QueryString["LineID"] != null)
            {
                show_location();
                cboLocation.SelectedValue = Request.QueryString["LocationID"].ToString();                
                show_point();
                cboPoint.SelectedValue = Request.QueryString["PointID"].ToString();
                DateTime StartDate = DateTime.Parse(Request.QueryString["StartDate"].ToString());
                DateTime EndDate = DateTime.Parse(Request.QueryString["EndDate"].ToString());
                Session["StartDate"] = StartDate;
                Session["EndDate"] = EndDate;
                txt_date_CalendarExtender.StartDate = StartDate;
                txt_date_CalendarExtender.EndDate = EndDate;                
            }
        }

        private void Show_Case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            try
            {                
                DataTable dt = dbclass.retrieve_rebanner_by_casenumber(_casenumber);

                entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();                
                entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheader.combobox_unit();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

                entryheader.disable_all_controls();

                Int32 discount = 0;
                discount = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString()));
                txt_adjustment.Text = discount.ToString();

                txtCaseNumber.Text = dt.Rows[0]["CaseReference"].ToString();
                btnLoad_Click(this, EventArgs.Empty);

                DataTable dt3 = dbclass.retrieve_rebanner_detail(_casenumber);

                List<ReBannerDetail> list = new List<ReBannerDetail>();
                ReBannerDetail bd = new ReBannerDetail();

                int i = 1;
                foreach (DataRow dr in dt3.Rows)
                {
                    bd = new ReBannerDetail();
                    bd.LineID = i;
                    bd.LocationID = Int32.Parse(dr["LocationID"].ToString());
                    bd.LocationName = dr["BannerLocationName"].ToString();
                    bd.PointID = Int32.Parse(dr["PointID"].ToString());
                    bd.PointName = dr["PointName"].ToString();
                    bd.ChangeDate = DateTime.Parse(dr["ChangeDate"].ToString());

                    list.Add(bd);
                    i++;
                }
                Session["ListReBannerDetail"] = list;

                gvw_detail.DataSource = list;
                gvw_detail.DataBind();

                hitung_total();
            }
            catch (Exception)
            {
                throw new HttpException(404, "Page not found");

            }
        }

        private void show_type()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_banner_type();
            cboType.DataSource = dt;
            cboType.DataTextField = "BannerTypeName";
            cboType.DataValueField = "BannerTypeID";
            cboType.DataBind();
            cboType.Items.Insert(0, new ListItem("None", "0"));
            cboType.SelectedIndex = 0;
        }

        private void show_location()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_location(userData.OrgID, userData.SiteID, Int32.Parse(cboType.SelectedValue));
            cboLocation.DataSource = dt;
            cboLocation.DataTextField = "BannerLocationName";
            cboLocation.DataValueField = "BannerLocationID";
            cboLocation.DataBind();
            cboLocation.Items.Insert(0, new ListItem("None", "0"));
            cboLocation.SelectedIndex = 0;
        }
        private void show_point()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_point();
            cboPoint.DataSource = dt;
            cboPoint.DataTextField = "PointName";
            cboPoint.DataValueField = "PointID";
            cboPoint.DataBind();
            cboPoint.Items.Insert(0, new ListItem("None", "0"));
            cboPoint.SelectedIndex = 0;
        }

        protected void btnLoad_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_banner_by_casenumber(txtCaseNumber.Text);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dtHelp = dbclass.retrieve_help((Int32)dt.Rows[0]["HelpID"]);
                String BannerType = dtHelp.Rows[0]["HelpName"].ToString();

                if (BannerType == ConfigurationManager.AppSettings["VerticalNewBannerHelpID"].ToString())
                    cboType.SelectedValue = "1";
                else if (BannerType == ConfigurationManager.AppSettings["WhatOnNewBannerHelpID"].ToString())
                    cboType.SelectedValue = "2";
            }

            dt = dbclass.retrieve_banner_detail(txtCaseNumber.Text);

            List<BannerDetail> list = new List<BannerDetail>();
            BannerDetail bd = new BannerDetail();

            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {
                bd = new BannerDetail();
                bd.BannerDetailID = i;
                bd.LocationID = Int32.Parse(dr["LocationID"].ToString());
                bd.LocationName = dr["BannerLocationName"].ToString();
                bd.PointID = Int32.Parse(dr["PointID"].ToString());
                bd.PointName = dr["PointName"].ToString();
                bd.BannerTypeID = Int32.Parse(dr["BannerTypeID"].ToString());
                bd.BannerTypeName = dr["BannerTypeName"].ToString();
                bd.StartDate = DateTime.Parse(dr["StartDate"].ToString());
                bd.ToDate = DateTime.Parse(dr["EndDate"].ToString());
                bd.Price = Convert.ToInt32(decimal.Parse(dr["Price"].ToString()));

                list.Add(bd);
                i++;
            }            

            gvw_data.DataSource = list;
            gvw_data.DataBind();

            dt = dbclass.retrieve_banner_by_casenumber(txtCaseNumber.Text);

            if (dt.Rows.Count > 0)
            {
                entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                entryheader.combobox_unit();

                String aa = dt.Rows[0]["UnitCode"].ToString();
                String bb = dt.Rows[0]["UnitNo"].ToString();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() +"-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String LineID = DataBinder.Eval(e.Row.DataItem, "LineID").ToString();
                String LocationID = DataBinder.Eval(e.Row.DataItem, "LocationID").ToString();
                String PointID = DataBinder.Eval(e.Row.DataItem, "PointID").ToString();
                String StartDate = DataBinder.Eval(e.Row.DataItem, "StartDate").ToString();
                String EndDate = DataBinder.Eval(e.Row.DataItem, "ToDate").ToString();
                
                LinkButton lnk_case = (LinkButton)e.Row.FindControl("lnk_case");
                lnk_case.CommandArgument = LineID;
                lnk_case.Text = LineID;

                if (Request.QueryString["CaseNumber"] != null)
                {
                    string CaseNumber = Request.QueryString["CaseNumber"].ToString();
                    lnk_case.PostBackUrl = string.Format("entryrebanner.aspx?flag=1&CaseNumber={0}&LineID={1}&PointID={2}&LocationID={3}&StartDate={4}&EndDate={5}", CaseNumber, LineID, PointID, LocationID, StartDate, EndDate);
                }
                else
                    lnk_case.PostBackUrl = string.Format("entryrebanner.aspx?flag=1&LineID={0}&PointID={1}&LocationID={2}&StartDate={3}&EndDate={4}", LineID, PointID, LocationID, StartDate, EndDate);
            }

        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_detail.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_detail.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_detail.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        private void hitung_total()
        {
            UserData userData = (UserData)ViewState["UserData"];

            if (String.IsNullOrWhiteSpace(txt_adjustment.Text))
                txt_adjustment.Text = "0";

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_generalprice("Biaya Pasang Banner", userData.SiteID);
            Int32 price = Convert.ToInt32(dt.Rows[0]["Price"]);

            List<ReBannerDetail> list = new List<ReBannerDetail>();
            Int32 countRow = 0;

            if (Session["ListReBannerDetail"] != null)
            {
                list = (List<ReBannerDetail>)Session["ListReBannerDetail"];
                countRow = list.Count;
            }

            Int32 biayapasang = countRow * price;

            Int32 subtotal = biayapasang;
            Int32 total = subtotal - Int32.Parse(txt_adjustment.Text, NumberStyles.Currency);

            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_adjustment.Text = txt_adjustment.Text;
            lbl_total.Text = total.ToString("N0");
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;         
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
           
            String ParameterHelpID = "";
            if (cboType.SelectedIndex == 1)
                ParameterHelpID = ConfigurationManager.AppSettings["VerticalReBannerHelpID"].ToString();
            else if (cboType.SelectedIndex == 2)
                ParameterHelpID = ConfigurationManager.AppSettings["WhatOnReBannerHelpID"].ToString();

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ParameterHelpID);
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);

            Int32 DiscountTypeID = 0;

            Int32 TotalAmount = Int32.Parse(lbl_subtotal.Text, NumberStyles.Currency);
            Int32 TotalDiscount = Int32.Parse(txt_adjustment.Text, NumberStyles.Currency);
            Int32 TotalAmountAfterDiscount = TotalAmount - TotalDiscount;            

            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;            

            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                /*request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();*/
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_rebanner_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, txtCaseNumber.Text, TotalAmount, DiscountTypeID, TotalDiscount, TotalAmountAfterDiscount,
                    0, 0, TotalAmount, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    dt = dbclass.retrieve_rebanner_by_casenumber(CaseNumber);
                    Int32 ReBannerID = Int32.Parse(dt.Rows[0]["ReBannerID"].ToString());

                    if (Request.QueryString["CaseNumber"] != null)
                        dbclass.delete_rebanner_detail_transaction(ReBannerID);

                    foreach (ReBannerDetail bd in (List<ReBannerDetail>)Session["ListReBannerDetail"])
                        dbclass.save_rebanner_detail_transaction(OrgID, SiteID, ReBannerID, bd.LocationID, bd.PointID, bd.ChangeDate, SavedBy);

                    Session["ListReBannerDetail"] = null;

                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected bool transaction_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }
            return true;
        }

        protected void btn_add_Click(object sender, EventArgs e)
        {
            DateTime changeDate;

            if (cboLocation.SelectedIndex <= 0)
            {
                entryheader.ShowAlert = "Location is Required";                
                return;
            }
            if (cboPoint.SelectedIndex <= 0)
            {
                entryheader.ShowAlert = "Point is Required";                
                return;
            }
            if (txtChangeDate.Text == "")
            {
                entryheader.ShowAlert = "Change Date is Required";                
                return;
            }
            else
            {
                try
                {
                    changeDate = DateTime.Parse(txtChangeDate.Text.ToString());
                }
                catch (Exception)
                {
                    entryheader.ShowAlert = "Invalid Date Time Format";                    
                    return;
                }
            }

            DateTime StartDate = DateTime.Parse(Session["StartDate"].ToString());
            DateTime EndDate = DateTime.Parse(Session["EndDate"].ToString());

            if (changeDate < StartDate || changeDate > EndDate)
            {
                entryheader.ShowAlert = string.Format("'Change Date' Should Between {0} And {1}.", StartDate, EndDate);                
                return;
            }

            List<ReBannerDetail> list = null;
            Int32 counter = 0;

            if (Session["ListReBannerDetail"] == null)
            {
                list = new List<ReBannerDetail>();
                counter = 1;
            }
            else
            {
                list = (List<ReBannerDetail>)Session["ListReBannerDetail"];
                if (list.Count > 0)
                {
                    Int32 maxObj = list.Max(r => r.LineID);
                    counter = maxObj + 1;
                }
                else counter = 1;
            }

            Int32 lineId = counter;

            int found = -1;
            foreach (ReBannerDetail obj in list)
            {
                if (obj.PointID == Int32.Parse(cboPoint.SelectedValue) && obj.ChangeDate == DateTime.Parse(txtChangeDate.Text.ToString()))
                    found++;

                if (found >= 0)
                    break;
            }

            if (found >= 0)
            {
                entryheader.ShowAlert = "Can not insert record, point has been added.";                
                return;
            }

            ReBannerDetail md = new ReBannerDetail(
                lineId,
                Int32.Parse(cboLocation.SelectedValue.ToString()),
                cboLocation.SelectedItem.Text.ToString(),
                Int32.Parse(cboPoint.SelectedValue.ToString()),
                cboPoint.SelectedItem.Text.ToString(),
                changeDate);

            list.Add(md);

            Session["ListReBannerDetail"] = list;

            gvw_detail.DataSource = list;
            gvw_detail.DataBind();

            hitung_total();

            cboLocation.SelectedIndex = 0;
            cboPoint.SelectedIndex = 0;
            txtChangeDate.Text = "";            

            txt_referencenumber.Text = null;
            txt_name.Text = null;
            txt_date.Text = null;
            gvw_lookup.DataSource = null;
            gvw_lookup.DataBind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var list = (List<ReBannerDetail>)Session["ListReBannerDetail"];

            foreach (GridViewRow gvr in gvw_detail.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 LineID = Int32.Parse(gvw_detail.DataKeys[gvr.RowIndex].Value.ToString());

                    var itemToRemove = list.SingleOrDefault(r => r.LineID == LineID);
                    if (itemToRemove != null)
                        list.Remove(itemToRemove);
                }
            }

            Session["ListReBannerDetail"] = list;
            gvw_detail.DataSource = list;
            gvw_detail.DataBind();
        }
        
        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void btn_lookupReference_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_banner_by_casenumber(txtCaseNumber.Text);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataTable dtHelp = dbclass.retrieve_help((Int32)dt.Rows[0]["HelpID"]);
                String BannerType = dtHelp.Rows[0]["HelpName"].ToString();

                if (BannerType == ConfigurationManager.AppSettings["VerticalNewBannerHelpID"].ToString())
                    cboType.SelectedValue = "1";
                else if (BannerType == ConfigurationManager.AppSettings["WhatOnNewBannerHelpID"].ToString())
                    cboType.SelectedValue = "2";

                entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheader.combobox_unit();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();
            }

            dt = dbclass.retrieve_banner_detail(txtCaseNumber.Text);

            List<BannerDetail> list = new List<BannerDetail>();
            BannerDetail bd = new BannerDetail();

            int i = 1;
            foreach (DataRow dr in dt.Rows)
            {
                bd = new BannerDetail();
                bd.BannerDetailID = Int32.Parse(dr["BannerDetailID"].ToString());
                bd.LocationID = Int32.Parse(dr["LocationID"].ToString());
                bd.LocationName = dr["BannerLocationName"].ToString();
                bd.PointID = Int32.Parse(dr["PointID"].ToString());
                bd.PointName = dr["PointName"].ToString();
                bd.BannerTypeID = Int32.Parse(dr["BannerTypeID"].ToString());
                bd.BannerTypeName = dr["BannerTypeName"].ToString();
                bd.StartDate = DateTime.Parse(dr["StartDate"].ToString());
                bd.ToDate = DateTime.Parse(dr["EndDate"].ToString());
                bd.Price = Convert.ToInt32(decimal.Parse(dr["Price"].ToString()));

                list.Add(bd);
                i++;
            }

            gvw_data.DataSource = list;
            gvw_data.DataBind();                        
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid_gvw_lookup();
        }

        protected void bounddatagrid_gvw_lookup()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.lookup_referencenumber(txt_referencenumber.Text, txt_name.Text, txt_date.Text, userData.SiteID, userData.OrgID);

            gvw_lookup.DataSource = dt;
            gvw_lookup.DataBind();
        }

        protected void gvw_lookup_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String casenumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();
                e.Row.Attributes["onclick"] = "LookupReference('" + casenumber + "')";
            }
        }

        protected void gvw_lookup_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_lookup.PageIndex = e.NewPageIndex;
            bounddatagrid_gvw_lookup();
        }
    }       
}