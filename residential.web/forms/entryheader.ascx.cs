﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using residential.libs;

namespace residential.web.forms
{
    public partial class entryheader : System.Web.UI.UserControl
    {
        public Boolean CustomerType
        {
            set { txt_customertype.Value = (value == false) ? "0" : "1"; }
            get { return (txt_customertype.Value == "0") ? false : true; }
        }

        public String OwnerName
        {
            set { txt_customername.Text = value; }
            get { return txt_customername.Text; }
        }

        public String PSCode
        {
            get { return txt_pscode.Text; }
            set { txt_pscode.Text = value; }
        }

        public String Unit
        {
            get { return cbo_unit.Text; }
            set { cbo_unit.Text = value; }
        }

        public String RequestorName
        {
            get { return txt_requestname.Text; }
            set { txt_requestname.Text = value; }
        }

        public String RequestorPhone
        {
            get { return txt_phone.Text; }
            set { txt_phone.Text = value; }
        }

        public String RequestorEmail
        {
            get { return txt_email.Text; }
            set { txt_email.Text = value; }
        }

        public String RequestDescription
        {
            get { return txt_description.Text; }
            set { txt_description.Text = value; }
        }

        public Int32 CaseOriginID
        {
            get { return Int32.Parse(cbo_caseorigin.SelectedValue); }
        }

        public String CaseOrigin
        {
            set { cbo_caseorigin.SelectedValue = value; }
        }

        public String PaymentScheme
        {
            get { return cbo_paymentscheme.SelectedValue; }
            set { cbo_paymentscheme.SelectedValue = value; }
        }

        public String ShowAlert
        {
            set
            {
                alert_danger_text.Text = value;
                alert_danger.Visible = true;
            }
        }

        public Boolean ToggleAlert
        {
            set { alert_danger.Visible = value; }
        }

        public String Validate
        {
            get
            {
                if (String.IsNullOrWhiteSpace(this.PSCode))
                {                    
                    return "Owner and PSCode are Required";
                }
                if (String.IsNullOrWhiteSpace(this.Unit))
                {                                  
                    return "Unit is Required";
                }
                if (String.IsNullOrWhiteSpace(this.RequestorPhone))
                {                    
                    return "Phone is Required";
                }
                return "VALID";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(HttpContext.Current.User.Identity.Name);
                combobox_paymentScheme();
                combobox_caseorigin();                                    
            }
        }

        public void combobox_caseorigin()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_caseorigin();

            cbo_caseorigin.DataSource = dt;
            cbo_caseorigin.DataTextField = "CaseOriginName";
            cbo_caseorigin.DataValueField = "CaseOriginID";
            cbo_caseorigin.DataBind();
            if (Request.QueryString["CaseNumber"] == null)
                cbo_caseorigin.Items.FindByText("Walk In").Selected = true;
        }

        public void combobox_paymentScheme()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_paymentSchemeNonIPKL();

            cbo_paymentscheme.DataSource = dt;
            cbo_paymentscheme.DataTextField = "PaymentSchemeDesc";
            cbo_paymentscheme.DataValueField = "PaymentSchemeID";
            cbo_paymentscheme.DataBind();
            cbo_paymentscheme.SelectedIndex = 0;
        }

        public void combobox_unit()
        {
            if (!String.IsNullOrWhiteSpace(txt_pscode.Text))
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = new UserData(HttpContext.Current.User.Identity.Name);

                DataTable dt = dbclass.retrieve_unitownership(userData.OrgID, userData.SiteID, txt_pscode.Text, int.Parse(txt_customertype.Value));

                if (dt.Rows.Count > 0)
                {
                    cbo_unit.Items.Clear();
                    foreach (DataRow row in dt.Rows)
                    {
                        cbo_unit.Items.Add(new ListItem(row["UnitCode"].ToString().Replace(" ", String.Empty) + "-" + row["UnitNo"].ToString().Replace(" ", String.Empty)));
                    }
                    if (dt.Rows.Count == 1)
                        cbo_unit.SelectedIndex = 0;
                }
                else
                    cbo_unit.Items.Clear();
            }
        }

        protected void btn_lookuppersonal_Click(object sender, EventArgs e)
        {
            //String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            //DBclass dbclass = new DBclass(constring);

            //DataTable dt = dbclass.retrieve_personal(txt_pscode.Text);
            //if (dt.Rows.Count > 0)
            //{
            //    txt_requestname.Text = txt_customername.Text;
            //    if (dt.Rows[0]["HPNo"] != null)
            //        txt_phone.Text = dt.Rows[0]["HPNo"].ToString();
            //    if (dt.Rows[0]["Email"] != null)
            //        txt_email.Text = dt.Rows[0]["Email"].ToString();
            //}
            BindCustomerInfo();
            combobox_unit();

            //clear search history
            gvw_data.DataSource = null;
            gvw_data.DataBind();
            rdb_customerTypeLookup.SelectedIndex = 0;
            txt_personal.Text = null;
            txt_unit.Text = null;
            txt_unitno.Text = null;
            lbl_itemdisplayed.Text = null;
        }

        public void reset_fields(ControlCollection ctls)
        {
            foreach (Control c in ctls)
            {
                if (c is TextBox)
                {
                    TextBox tt = c as TextBox;
                    tt.Text = null;
                }
                else if (c is AjaxControlToolkit.ComboBox)
                {
                    AjaxControlToolkit.ComboBox dd = c as AjaxControlToolkit.ComboBox;
                    if (dd.Items.Count > 0)
                        dd.SelectedIndex = 0;
                }

                if (c.HasControls())
                {
                    reset_fields(c.Controls);
                }
            }
        }

        protected void bounddatagrid()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            Int32 customertype = Int32.Parse(rdb_customerTypeLookup.SelectedValue);

            DataTable dt = dbclass.lookup_personal(customertype, userData.OrgID, userData.SiteID, txt_personal.Text, txt_unit.Text, txt_unitno.Text);

            gvw_data.DataSource = dt;
            gvw_data.DataBind();

            ViewState["totalRows"] = dt.Rows.Count;
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid();
            gvw_data.PageIndex = 0;
            gvw_data_PageIndexChanging(this, new GridViewPageEventArgs(0));
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String pscode = DataBinder.Eval(e.Row.DataItem, "pscode").ToString();
                String psname = DataBinder.Eval(e.Row.DataItem, "psname").ToString();

                e.Row.Attributes["onclick"] = "returnVal('" + psname + "', '" + pscode + "', '" + rdb_customerTypeLookup.SelectedValue + "')";
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid();

            int entireRows = gvw_data.Rows.Count;
            if (entireRows > 0)
            {
                int pageSize = gvw_data.PageSize;
                int pageIndex = e.NewPageIndex;
                int startData = pageIndex * pageSize + 1;
                int endData = startData + entireRows - 1;
                lbl_itemdisplayed.Text = "Displaying " + startData + "-" + endData + " of about " + ViewState["totalRows"].ToString() + " records";
            }
            else
                lbl_itemdisplayed.Text = "No Records!";
        }

        public void disable_all_controls()
        {
            btn_OpenLookupWindow.Enabled = false;
            cbo_unit.Enabled = false;
            cbo_paymentscheme.Enabled = false;
            txt_requestname.Enabled = false;
            cbo_caseorigin.Enabled = false;
            txt_phone.Enabled = false;
            txt_email.Enabled = false;
            txt_description.Enabled = false;
        }

        public void BindCustomerInfo()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_personal(txt_pscode.Text);
            if (dt.Rows.Count > 0)
            {
                txt_requestname.Text = txt_customername.Text;
                if (dt.Rows[0]["HPNo"] != null)
                    txt_phone.Text = dt.Rows[0]["HPNo"].ToString();
                if (dt.Rows[0]["Email"] != null)
                    txt_email.Text = dt.Rows[0]["Email"].ToString();
            }
        }
    }
}