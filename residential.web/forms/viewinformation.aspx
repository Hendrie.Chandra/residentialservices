﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewinformation.aspx.cs" Inherits="residential.web.forms.viewinformation" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/emailviewer.ascx" TagName="EmailViewer" TagPrefix="aspuc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>Information Detail <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_email">Email Viewer</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
        <div id="tab_email" class="tab-pane fade">
            <aspuc:EmailViewer ID="emailviewer" runat="server" />
        </div>
    </div>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <div class="panel panel-default">
        <div class="panel-heading">Information Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Information</label>
                    <div class="col-sm-10">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_information" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

