﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listaccessgateregistration.aspx.cs" Inherits="residential.web.forms.listaccessgateregistration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>List Access Gate Registration</h1>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                <div class="form-group">
                    <div class="col-md-8">
                        <div class="col-md-4">
                            <div class="input-group">
                                <asp:DropDownList ID="ddl_case_status" CssClass="form-control" runat="server" AutoPostBack="true" OnTextChanged="ddl_case_status_TextChanged"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" OnTextChanged="txt_search_TextChanged" AutoPostBack="true" placeholder="search by case number, unit"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" OnClick="btn_search_Click" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover" AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound"
                ShowHeaderWhenEmpty="True"
                DataKeyNames="CaseNumber" AllowSorting="True" OnSorting="gvw_data_Sorting"
                OnPageIndexChanging="gvw_data_PageIndexChanging">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="UNIT CODE" DataField="Unit" SortExpression="Unit" />
                    <asp:BoundField HeaderText="NAME" DataField="NamaPelapor" SortExpression="NamaPelapor" />
                    <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" SortExpression="CaseStatusName" />
                    <asp:BoundField HeaderText="CREATED DATE" DataField="CreatedDate" SortExpression="CreatedDate" />
                    <%--<asp:BoundField HeaderText="CREATED" DataField="RequestDate" SortExpression="RequestDate" DataFormatString="{0:d MMMM yyyy}" />--%>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
