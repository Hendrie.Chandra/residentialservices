﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listsubscriptiontype.aspx.cs" Inherits="residential.web.forms.listsubscriptiontype" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>            
            <div class="buttoncontainer">
                <asp:Button CssClass="button" ID="btn_add" runat="server" Text="ADD" 
                    Width="100px" onclick="btn_add_Click" />                                    
            </div>
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" 
                CssClass="gridview" AllowPaging="True" BorderWidth="0px" 
                Width="100%" onrowdatabound="gvw_data_RowDataBound" 
                ShowHeaderWhenEmpty="True" 
                DataKeyNames="SubscriptionTypeID" AllowSorting="True" onsorting="gvw_data_Sorting" 
                onpageindexchanging="gvw_data_PageIndexChanging" 
                onrowcommand="gvw_data_RowCommand">
                <Columns>
                    <asp:TemplateField>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SUBSCRIPTION TYPE">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnk_subscriptiontype" runat="server" CommandName="cmd_edit" 
                                CssClass="gridview_link">subscription type</asp:LinkButton>
                        </ItemTemplate>                        
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="NUMBER OF GROUPS" SortExpression="UnitCode">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>                                        
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>        
    </asp:UpdatePanel>
</asp:Content>
