﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using residential.libs;
using residential.libs.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using LKCommon;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class updatecase : System.Web.UI.UserControl
    {
        static ConnectionClass connClass = new ConnectionClass();

        public string DepartmentID
        {
            set { hfDepartmentID.Value = value; }
        }

        public string IsFinalInspect
        {
            set { hfIsFinalInspect.Value = value; }
        }

        public string IsPaskem
        {
            set { hfIsPaskem.Value = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(HttpContext.Current.User.Identity.Name);

                //loadUpdateAssignSection();
                //loadAssignee();
                if (Request.QueryString["CaseNumber"] != null)
                {
                    loadUpdateAssignSection();
                    loadAssignee();
                }

                if (hfIsFinalInspect.Value.ToString() == "1")
                {
                    Adjustment.Visible = true;
                }
            }
            alert_danger.Visible = false;
        }

        protected void loadUpdateAssignSection()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            String CaseNumber = Request.QueryString["CaseNumber"].ToString();
            DataTable dt_case = dbclass.retrieve_Case(CaseNumber);
            int caseStatus = int.Parse(dt_case.Rows[0]["CaseStatusID"].ToString());
            
            if (hfDepartmentID.Value != ""){
                loadCaseStatus(caseStatus.ToString());
                if (new[] { 1, 2 }.Contains(userData.RoleID))
                {
                    if (caseStatus == 2)
                        assignCaseForm.Visible = true;
                    else if (caseStatus == 4 || caseStatus == 5 || caseStatus == 8) //Added by Andy For On Hold and Not Statisfied can Update
                    {
                        updateCaseForm.Visible = true;
                        assignCaseForm.Visible = true;
                    }
                }
                else if (loadCaseStatus(caseStatus.ToString()) == 1)
                    updateCaseForm.Visible = true;
            }
            else
            {
                //view untuk cs
                updateCaseForm.Visible = true;
                loadCaseStatus(1, caseStatus.ToString()); //ambil semua case status kemudian filter

            }
        }

        //start add by bili
        protected int loadCaseStatus(int _RoleID, string _selected)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt_casestatus = dbclass.retrieve_casestatus(_RoleID, int.Parse(_selected));

            var filterCaseStatus = from DataRow filteredrow in dt_casestatus.Rows
                                   where (int)filteredrow["AvailableCaseStatusID"] == 3 || (int)filteredrow["AvailableCaseStatusID"] == int.Parse(_selected)
                                   select new { CaseStatusName = filteredrow.Field<string>("CaseStatusName"), AvailableCaseStatusID = filteredrow.Field<int>("AvailableCaseStatusID") };

            if (filterCaseStatus.Count() > 0)
            {
                cbo_casestatus.DataSource = filterCaseStatus;
                cbo_casestatus.DataTextField = "CaseStatusName";
                cbo_casestatus.DataValueField = "AvailableCaseStatusID";
                cbo_casestatus.DataBind();
                cbo_casestatus.SelectedValue = _selected;

                if (_selected == "3")
                    btn_update.Enabled = false;
            }
            else
                return 0;

            return 1;

            //availablecencelstatus acs = new availablecencelstatus();
            //acs.availableCaseStatusID = 3;
            //acs.CaseStatusName = "Cencel";
            //cbo_casestatus.DataBind();

            //List<availablecencelstatus> mylist = new List<availablecencelstatus>();
            //mylist.Add(acs);

            //cbo_casestatus.DataSource = mylist;
            //cbo_casestatus.DataTextField = "CaseStatusName";
            //cbo_casestatus.DataValueField = "AvailableCaseStatusID";
            //cbo_casestatus.DataBind();
            //return 1;
        }
        //end add by bili

        protected int loadCaseStatus(string _selected)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt_casestatus = dbclass.retrieve_casestatus(userData.RoleID, int.Parse(_selected));

            if (dt_casestatus.Rows.Count > 0)
            {
                cbo_casestatus.DataSource = dt_casestatus;
                cbo_casestatus.DataTextField = "CaseStatusName";
                cbo_casestatus.DataValueField = "AvailableCaseStatusID";
                cbo_casestatus.DataBind();
                cbo_casestatus.SelectedValue = _selected;
            }
            else
                return 0;

            return 1;
        }

        protected void loadAssignee()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            //DataTable dt = dbclass.retrieve_pic(userData.OrgID, userData.SiteID, userData.DepartmentID);
            DataTable dt = dbclass.retrieve_pic(userData.OrgID, userData.SiteID, CommonFunctions.StringToInt(hfDepartmentID.Value));
            cbo_assign.DataSource = dt;
            cbo_assign.DataTextField = "FullName";
            cbo_assign.DataValueField = "UserID";
            cbo_assign.DataBind();
        }

        protected void btn_update_Click(object sender, EventArgs e)
        {
            if (txt_remarks.Text.Length > 10)
            {
                try
                {
                    UserData userData = (UserData)ViewState["UserData"];

                    String CaseNumber = Request.QueryString["CaseNumber"];
                    Int32 CaseStatusID = Int32.Parse(cbo_casestatus.SelectedValue);
                    String FullName = userData.FullName;
                    String Remarks = txt_remarks.Text;
                    if (cbo_casestatus.SelectedValue.ToString() == "6" && hfIsFinalInspect.Value.ToString() == "1")
                    {
                        Remarks = Remarks + " dan Security Deposit dipotong sebesar Rp. " + txt_discount.Text.ToString();
                    }
                    String SavedBy = userData.UserName;

                    DBclass dbclass = new DBclass(connClass.DBConnString());

                    String SavedFileName = null;
                    if (filesUpload.HasFile)
                    {
                        // Start-- Adhitya Prabowo Update Fixing 20161215
                        Int64 MaxSize = 20971520;

                        if (filesUpload.FileContent.Length > MaxSize)
                        {
                            throw new Exception("File should not be more than 20 MB !!");
                        }
               
                        //simpan file upload ke db
                        string _fileName = null;
                        string _fileType = null;
                        byte[] _fileData = null;

                        _fileName = Path.GetFileName(filesUpload.PostedFile.FileName);
                        _fileType = filesUpload.PostedFile.ContentType;
                        string _fileExtention = Path.GetExtension(filesUpload.PostedFile.FileName);

                        Stream fs = filesUpload.PostedFile.InputStream;
                        _fileData = upload_file(_fileName, _fileType, fs);

                        dbclass.save_ticket_attachment(CaseNumber, CaseStatusID, _fileName, _fileType, _fileData, SavedBy);
                    }

                    //function ini sekaligus update di tr_case juga
                    Int32 returnvalue = dbclass.save_tickethistory(CaseNumber, CaseStatusID, FullName, Remarks, SavedBy, SavedFileName); /*sekaligus update trcase*/

                    /*update_to_crm(CaseNumber, CaseStatusID, Remarks, FullName, null, 0);*/

                    ////kirim email disini
                    try
                    {
                        DataTable dt = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt.Rows[0]["CaseStatusID"];

                            int helpdeskstatus = CaseStatusID;
                            switch (helpdeskstatus)
                            {
                                //case 1: //Pending
                                //    sendEmailToCustomer(adminservice, postincident, preincident, helpdeskstatus);
                                //    break;
                                case 2: //Assigned To PIC - No Pending
                                    clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                                    clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                                    break;
                                case 3: //Cancelled
                                    clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                                    clsEmail.sendEmailToPIC(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                                    break;
                                case 5: //On Hold
                                case 6: //Progress Complete
                                case 9: //Case Closed
                                case 10: //Rejected
                                    clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                                    break;
                                case 8: //Not Satisfied
                                    clsEmail.sendEmailToPIC(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                                    break;
                            }
                        } 
                    }
                    catch (Exception exc)
                    {
                        dbclass.Save_ErrorLog(exc, userData.UserName);
                    }
                  



                    if (cbo_casestatus.SelectedValue.ToString() == "6" && hfIsFinalInspect.Value.ToString() == "1")
                    {
                        //string reletedCaseDescription = "";
                        //if (!String.IsNullOrEmpty(Session["reletedCaseDescription"].ToString()))
                        //    reletedCaseDescription = "|| Related Case Desciption : " + Session["reletedCaseDescription"].ToString();

                        BCDFlow(CaseNumber);
                    }
                    //else if (cbo_casestatus.SelectedValue.ToString() == "6" && hfIsPaskem.Value == "1")
                    //{
                    //    string unitCode = Case.Rows[0]["UnitCode"].ToString();
                    //    string unitNo = Case.Rows[0]["UnitNo"].ToString();
                    //    string psCode = Case.Rows[0]["PSCode"].ToString();
                    //    UpdateBillingComponentStatus(userData.OrgID, userData.SiteID, unitCode, unitNo, psCode);
                    //}
                    if (hfDepartmentID.Value != "")
                    {
                        Response.Redirect("mytasklist.aspx");
                    }
                    else if (Request.QueryString["helpcategory"]!=null)
                    {
                        String helpCategory = Request.QueryString["helpcategory"].ToString();
                        switch (helpCategory)
                        {
                            case "1" :
                                Response.Redirect("complaint.aspx");
                                break;
                            case "2":
                                Response.Redirect("information.aspx");
                                break;
                            case "4":
                                Response.Redirect("incident.aspx");
                                break;
                        }
                    }
                    else if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("viewbanner") || HttpContext.Current.Request.Url.ToString().ToLower().Contains("entrybanner"))
                    {
                        Response.Redirect("banner.aspx");
                    }
                    else if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("viewmagazine") || HttpContext.Current.Request.Url.ToString().ToLower().Contains("entrymagazine"))
                    {                        
                        Response.Redirect("magazine.aspx");
                    }
                }
                catch (Exception ex)
                {
                    alert_danger_text.Text = ex.Message;
                    alert_danger.Visible = true;
                }
            }
            else
            {
                alert_danger_text.Text = "Please Fill Remarks";
                alert_danger.Visible = true;
            }
        }

        protected void btn_assign_Click(object sender, EventArgs e)
        {
            if (cbo_assign.SelectedItem == null)
            {
                alert_danger_text.Text = "Please select person te be assigned";
                alert_danger.Visible = true;
            }
            else
            {
                try
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    UserData userData = (UserData)ViewState["UserData"];

                    Int32 UserID = Int32.Parse(cbo_assign.SelectedValue);
                    String SavedBy = userData.UserName;
                    String FullName = userData.FullName;
                    String CaseNumber = Request.QueryString["CaseNumber"];
                    String Remarks = "Assign to " + cbo_assign.SelectedItem.Text;
                    dbclass.assign_case(UserID, CaseNumber, SavedBy);
                    /*update_to_crm(CaseNumber, 4, Remarks, FullName, cbo_assign.SelectedItem.Text, 3);*/
                    /*dbclass.save_tickethistory(CaseNumber, 4, FullName, Remarks, SavedBy, null);*/

                    Response.Redirect("mytasklist.aspx");
                }
                catch (Exception ex)
                {
                    alert_danger_text.Text = ex.Message;
                    alert_danger.Visible = false;
                }
            }
        }

        private void BCDFlow(string caseNumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring); 

            //CRMclass crmclass = new CRMclass(CRMConn);
            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dt = residential.web.Classes.clsSecurityDeposit.RetreiveData(caseNumber, OrgID, SiteID);

            Int32 HelpID = int.Parse(dt.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = int.Parse(dt.Rows[0]["CaseOriginID"].ToString());
            String PSName = dt.Rows[0]["PSName"].ToString();
            String PSCode = dt.Rows[0]["PSCode"].ToString();
            String UnitCode = dt.Rows[0]["UnitCode"].ToString();
            String UnitNo = dt.Rows[0]["UnitNo"].ToString();

            String NamaPelapor = dt.Rows[0]["NamaPelapor"].ToString();
            String TeleponPelapor = dt.Rows[0]["TeleponPelapor"].ToString();
            String EmailPelapor = dt.Rows[0]["EmailPelapor"].ToString();
            String RequestDescription = dt.Rows[0]["Description"].ToString() + " dan Security Deposit dipotong sebesar Rp. " + decimal.Parse(txt_discount.Text.ToString()).ToString("N0");
            DateTime RequestDate = DateTime.Now;
            String Creator = dt.Rows[0]["CreatedBy"].ToString();

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = userData.OrgID;
            request.SiteID = userData.SiteID;
            request.CustomerType = false;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = dt.Rows[0]["CRMHelpID"].ToString();
            request.HelpID = HelpID;
            request.FullName = Creator;
            request.UserName = Creator;

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;
                DateTime? OverdueDate = dbcase.OverdueDate;

                String returnvalueCreate = residential.web.Classes.clsSecurityDeposit.SaveSecurityDeposit(OrgID, SiteID,
                    CRMCaseID, CaseNumber, 2, HelpID, CaseOriginID, false, PSName, PSCode, UnitCode, UnitNo,
                    NamaPelapor, TeleponPelapor, EmailPelapor, RequestDate, RequestDescription, OverdueDate, Creator, Creator, caseNumber, txt_discount.Text.ToString());

                if (returnvalueCreate != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];
                                                        
                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                            clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    }
                }

                /*try
                {
                    UpdateCaseHelpDeskStatusRequest upd = new UpdateCaseHelpDeskStatusRequest();
                    upd.CRMCaseNumber = NewCaseNumber;
                    upd.CRMCaseStatusID = 2;
                    upd.Remarks = "";
                    upd.UpdateBy = userData.FullName;
                    upd.UpdateDate = DateTime.Now;
                    upd.WorkedBy = string.Empty;
                    upd.SatisfactionID = 1;
                    UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(upd);
                }
                catch (Exception)
                {

                }*/
            }
        }

        private void UpdateBillingComponentStatus(int orgID, int siteID, string unitCode, string unitNo, string psCode)
        {
            DataTable dtSettings = Helper.GetSettings(orgID, siteID, "TRANSACTION_ID", "POTABLE_WATER", string.Empty);
            if (dtSettings.Rows.Count > 0)
            {
                int waterTransactionID = CommonFunctions.StringToInt(dtSettings.Rows[0]["SettingValue"].ToString());
                string param = "<root>";
                param += "<row UnitCode=\"" + unitCode + "\" UnitNo=\"" + unitNo + "\" Remarks=\"Reactivate water from CRM\"" + " Status=\"1\" /></root>";

                clsWTP.UploadBillingComponentStatus(orgID, siteID, waterTransactionID, param);
            }
            else
            {
                throw new Exception("Water transaction has not been setup, please contact IT.");
            }
        }

        //add by bili januari 2018
        private byte[] upload_file(string _fileName, string _fileType, Stream _fs)
        {
            BinaryReader br = new BinaryReader(_fs);
            byte[] bytes = br.ReadBytes((Int32)_fs.Length);

            br.Close();
            _fs.Close();

            return bytes;
        }

        private bool validate_extention_file(string extentionFile)
        {
            string[] allowedExtentianFile = { ".jpg", "jpeg", ".png", ".pdf" };

            if (allowedExtentianFile.Contains(extentionFile.ToLower()))
                return true;
            else
                return false;
        }
    }

    public class availablecencelstatus
    {
        public int availableCaseStatusID { get; set; }
        public string CaseStatusName { get; set; }
    }

    public class helpdeskcencel
    {
        public List<availablecencelstatus> listAvailableCaseStatusForCencel { get; set; }
    }
}