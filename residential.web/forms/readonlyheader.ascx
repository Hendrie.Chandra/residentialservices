﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="readonlyheader.ascx.cs" Inherits="residential.web.forms.readonlyheader" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<link href="../maincss.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../global.js">
</script>
<div class="formentrycontent">
    <div class="formentrycell2">
        <asp:Label ID="Label5" runat="server" Text="Owner :" CssClass="labelfield"></asp:Label>
        <asp:TextBox ID="txt_customername" runat="server" 
            CssClass="textbox_entry_left textboxcelllookup" Enabled="false"></asp:TextBox>             
    </div>
    <div class="formentrycell2">
        <asp:Label ID="Label1" runat="server" Text="PS Code :" CssClass="labelfield"></asp:Label>
        <asp:TextBox ID="txt_pscode" runat="server" 
            CssClass="textbox_entry_left textboxcell2" Enabled="false"></asp:TextBox>
    </div>    
</div>
<div class="formentrycontent">
    <div class="formentrycell2">
        <asp:Label ID="Label2" runat="server" Text="Unit :" CssClass="labelfield"></asp:Label>
        <asp:DropDownList ID="cbo_unit" runat="server" CssClass="ComboBoxWindowsStyle" 
            Width="167px" AutoPostBack="True" Enabled="false"></asp:DropDownList>
    </div>
    <div class="formentrycell2">
        <asp:HiddenField ID="txt_customertype" runat="server" />
    </div>
</div>
<div class="formentrycontent">
    <div class="formentrycell2">
        <asp:Label ID="Label3" runat="server" Text="Customer :" CssClass="labelfield"></asp:Label>
        <asp:TextBox ID="txt_requestname" runat="server" 
            CssClass="textbox_entry_left textboxcell2" Enabled="false"></asp:TextBox>
    </div>
    <div class="formentrycell2">
        <asp:Label ID="Label4" runat="server" Text="Case Origin :" 
            CssClass="labelfield"></asp:Label>
        <asp:DropDownList ID="cbo_caseorigin" runat="server" CssClass="ComboBoxWindowsStyle" 
            Width="167px" Enabled="false"></asp:DropDownList>
    </div>
</div>
<div class="formentrycontent">
    <div class="formentrycell2">
        <asp:Label ID="Label6" runat="server" Text="Phone :" CssClass="labelfield"></asp:Label>
        <asp:TextBox ID="txt_phone" runat="server" Enabled="false"
            CssClass="textbox_entry_left textboxcell2" MaxLength="15"></asp:TextBox>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxPhone" runat="server" TargetControlID="txt_phone" FilterType="Numbers, Custom" ValidChars="+" />
    </div>
    <div class="formentrycell2">
        <asp:Label ID="Label7" runat="server" Text="Email :" CssClass="labelfield"></asp:Label>
        <asp:TextBox ID="txt_email" runat="server" Enabled="false" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
        <asp:FilteredTextBoxExtender ID="FilteredTextBoxEmail" runat="server" TargetControlID="txt_email"
        FilterType="Numbers, LowercaseLetters, UppercaseLetters, Custom" ValidChars=".@_" />
    </div>
</div>
<div class="formentrycontent">
    <div class="formentrycellmultiline">
        <asp:Label ID="Label9" runat="server" Text="Description :" CssClass="labelfield"></asp:Label>
        <asp:TextBox ID="txt_description" runat="server" 
            CssClass="textbox_entry_left textboxcell1multiline" Enabled="false" TextMode="MultiLine"></asp:TextBox>
    </div>
</div>