﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Configuration;
using System.Data;
using System.IO;
using System.Drawing;

namespace residential.web.forms
{
    public partial class ToBeValidate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";

                //show_cbo_casestatus();
                bounddatagrid("", "");
            }
            Response.AppendHeader("refresh", "60");
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            Int32 CaseStatusID=1;
            String Search = "";
            if (!String.IsNullOrEmpty(txt_search.Text))
                Search = txt_search.Text;

            DateTime? StartDate = null;
            DateTime? EndDate = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
                //CaseStatusID = Int32.Parse(cbo_status.SelectedValue);
            }

            DataSet dt = dbclass.case_tobe_validate(userData.OrgID, userData.SiteID, CaseStatusID, Search, StartDate, EndDate);

            DataView dv = new DataView(dt.Tables[0]);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            gvw_data.DataSource = null;
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        protected void picker_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime? StartDate = null;
                DateTime? EndDate = null;
                //Int32 CaseStatusID = Int32.Parse(cbo_status.SelectedValue);

                if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
                {
                    StartDate = Convert.ToDateTime(TxtStartDate.Text);
                    EndDate = Convert.ToDateTime(TxtEndDate.Text);
                    //CaseStatusID = Int32.Parse(cbo_status.SelectedValue);
                }

                DateTime SD = Convert.ToDateTime(TxtStartDate.Text);
                if (SD > DateTime.Now)
                {
                    Response.Write("<script>alert('Created Date FROM cannot  be greater than CURRENT date')</script>");
                }

                DateTime ED = Convert.ToDateTime(TxtEndDate.Text);
                if (ED > DateTime.Now)
                {
                    Response.Write("<script>alert('Created Date TO cannot  be greater than CURRENT date')</script>");
                }

                if (ED < SD)
                {
                    Response.Write("<script>alert('Created Date TO cannot  be less than Created Date FROM')</script>");
                }

                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";

                //show_cbo_casestatus();
                bounddatagrid("", "");
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        //private void show_cbo_casestatus()
        //{
        //    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
        //    DBclass dbclass = new DBclass(constring);

        //    DataTable dt = dbclass.retrieve_casestatus();
        //    cbo_status.DataSource = dt;
        //    cbo_status.DataTextField = "CaseStatusName";
        //    cbo_status.DataValueField = "CaseStatusID";
        //    cbo_status.DataBind();
        //    cbo_status.Items.Insert(0, new ListItem("[ All Status ]", "0"));
        //    cbo_status.SelectedIndex = 0;
        //}

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                e.Row.Attributes["onclick"] = "location.href='ViewAndValidate.aspx?casenumber=" + DataBinder.Eval(e.Row.DataItem, "CaseNumber") + "&helpcategory=1'";

                int CaseStatusID = int.Parse(DataBinder.Eval(e.Row.DataItem, "CaseStatusID").ToString());
                if (!String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString()) && (CaseStatusID == 2 || CaseStatusID == 4))
                {
                    DateTime OverdueDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString());
                    if (DateTime.Compare(DateTime.Now, OverdueDate) >= 0)
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                }
            }
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void cbo_status_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }


        //protected void ExportToExcel(object sender, EventArgs e)
        //{
        //    Response.Clear();
        //    Response.Buffer = true;
        //    Response.AddHeader("content-disposition", "attachment;filename=OC-CaseReport.xls");
        //    Response.Charset = "";
        //    Response.ContentType = "application/vnd.ms-excel";
        //    using (StringWriter sw = new StringWriter())
        //    {
        //        HtmlTextWriter hw = new HtmlTextWriter(sw);

        //        //To Export all pages
        //        gvw_data.AllowPaging = false;
        //        this.bounddatagrid("", "");

        //        gvw_data.HeaderRow.BackColor = Color.White;
        //        foreach (TableCell cell in gvw_data.HeaderRow.Cells)
        //        {
        //            cell.BackColor = gvw_data.HeaderStyle.BackColor;
        //        }
        //        foreach (GridViewRow row in gvw_data.Rows)
        //        {
        //            row.BackColor = Color.White;
        //            foreach (TableCell cell in row.Cells)
        //            {
        //                if (row.RowIndex % 2 == 0)
        //                {
        //                    cell.BackColor = gvw_data.AlternatingRowStyle.BackColor;
        //                }
        //                else
        //                {
        //                    cell.BackColor = gvw_data.RowStyle.BackColor;
        //                }
        //                cell.CssClass = "textmode";
        //            }
        //        }

        //        gvw_data.RenderControl(hw);

        //        //style to format numbers to string
        //        string style = @"<style> .textmode { } </style>";
        //        Response.Write(style);
        //        Response.Output.Write(sw.ToString());
        //        Response.Flush();
        //        Response.End();
        //    }
        //}
        //public override void VerifyRenderingInServerForm(Control control)
        //{
        //    /* Verifies that the control is rendered */
        //}
    }
}