﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.forms
{
    public partial class viewktp_kk : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                Session["ListDetailKTP_KK"] = null;
                show_case(Request.QueryString["CaseNumber"]);
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();
            }

            if (Session["ListDetailKTP_KK"] != null)
            {
                List<DetailKTP_KK> list = (List<DetailKTP_KK>)Session["ListDetailKTP_KK"];
                gvw_detail.DataSource = list;
                gvw_detail.DataBind();
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_Case(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                DataTable dt2 = dbclass.retrieve_personal_identification_by_casenumber(_casenumber);
                lblZone.Text = dt2.Rows[0]["ZoneName"].ToString();
                lblRequest.Text = dt2.Rows[0]["HelpName"].ToString();

                if (dt2.Rows[0]["HelpName"].ToString() == ConfigurationManager.AppSettings["NewPersonalIdentificationHelpID_KTP"])
                    lblJumlahKTP.Visible = true;              
                else if (dt2.Rows[0]["HelpName"].ToString() == ConfigurationManager.AppSettings["NewPersonalIdentificationHelpID_KK"])                
                    lblJumlahKK.Visible = true;                

                Decimal subtotal = Decimal.Parse(dt2.Rows[0]["Amount"].ToString());
                lbl_subtotal.Text = subtotal.ToString("N0");

                Decimal discount = Decimal.Parse(dt2.Rows[0]["DiscountAmount"].ToString());
                lbl_adjustment.Text = discount.ToString("N0");

                Decimal total = Decimal.Parse(dt2.Rows[0]["AmountAfterDiscount"].ToString());
                lbl_total.Text = total.ToString("N0");

                DataTable dt3 = dbclass.retrieve_generalprice("KTP Baru", userData.SiteID, Int32.Parse(dt2.Rows[0]["ZoneID"].ToString()));
                Int32 price1 = Convert.ToInt32(dt3.Rows[0]["Price"]);

                dt3 = dbclass.retrieve_generalprice("KK Baru", userData.SiteID, Int32.Parse(dt2.Rows[0]["ZoneID"].ToString()));
                Int32 price2 = Convert.ToInt32(dt3.Rows[0]["Price"]);

                lblJumlahKTP.Text = dt2.Rows[0]["Qty_KTP"].ToString() + " x " + price1.ToString("N0");
                lblJumlahKK.Text = dt2.Rows[0]["Qty_KK"].ToString() + " x " + price2.ToString("N0");

                DataTable dt_detail = dbclass.retrieve_personal_identification_detail(_casenumber);
                List<DetailKTP_KK> list = StoredToList(dt_detail);
                Session["ListDetailKTP_KK"] = list;
                gvw_detail.DataSource = list;
                gvw_detail.DataBind();
                //}
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }

        private List<DetailKTP_KK> StoredToList(DataTable _dt)
        {
            var convertedList = (from rw in _dt.AsEnumerable()
                                 select new DetailKTP_KK()
                                 {
                                     OrgID = Int32.Parse(rw["OrgID"].ToString()),
                                     SiteID = Int32.Parse(rw["SiteID"].ToString()),
                                     PersonalIdentificationID = Int32.Parse(rw["PersonalIdentificationID"].ToString()),
                                     PersonalIdentificationDetailID = Int32.Parse(rw["PersonalIdentificationDetailID"].ToString()),
                                     Address = rw["Address"].ToString(),
                                     Kelurahan = rw["Kelurahan"].ToString(),
                                     Kecamatan = rw["Kecamatan"].ToString(),
                                     Name = rw["Name"].ToString(),
                                     GenderID = Int32.Parse(rw["GenderID"].ToString()),
                                     Gender = rw["GenderName"].ToString(),
                                     RelationID = Int32.Parse(rw["RelationID"].ToString()),
                                     Relation = rw["RelationName"].ToString(),
                                     BirthPlace = rw["BirthPlace"].ToString(),
                                     BirthDate = DateTime.Parse(rw["BirthDate"].ToString()),
                                     StateCountry = rw["StateCountry"].ToString(),
                                     MaritalStatusID = Int32.Parse(rw["MaritalStatusID"].ToString()),
                                     MaritalStatus = rw["MaritalStatusName"].ToString(),
                                     BloodTypeID = Int32.Parse(rw["BloodTypeID"].ToString()),
                                     BloodType = rw["BloodTypeName"].ToString(),
                                     ReligionID = Int32.Parse(rw["ReligionID"].ToString()),
                                     Religion = rw["ReligionName"].ToString(),
                                     LastEducationID = Int32.Parse(rw["LastEducationID"].ToString()),
                                     LastEducation = rw["LastEducationName"].ToString(),
                                     OccupationID = Int32.Parse(rw["OccupationID"].ToString()),
                                     Occupation = rw["OccupationName"].ToString(),
                                     ParentName = rw["ParentName"].ToString(),
                                     SBKRINotes = rw["SBKRINotes"].ToString(),
                                     Notes = rw["Notes"].ToString(),
                                 }).ToList();

            return convertedList;
        }

        protected void btn_showDetailPersonalID_Click(object sender, EventArgs e)
        {
            List<DetailKTP_KK> ListPersonalIDDetail = (List<DetailKTP_KK>)Session["ListDetailKTP_KK"];            
            IEnumerable<DetailKTP_KK> PIDDetails = ListPersonalIDDetail.Where(detail => detail.PersonalIdentificationDetailID == int.Parse(txtPersonalIdentificationDetailID.Value));
                        
            foreach (DetailKTP_KK det in PIDDetails)
            {
                txtAlamat.Text = det.Address;
                txtKelurahan.Text = det.Kelurahan;
                txtKecamatan.Text = det.Kecamatan;
                txtNamaLengkap.Text = det.Name;
                cboJenisKelamin.Text = det.Gender;
                cboRelation.Text = det.Relation;
                txtTempatLahir.Text = det.BirthPlace;
                txtTglLahir.Text = det.BirthDate.ToShortDateString();
                txtPropinsiNegara.Text = det.StateCountry;
                cboStatusPernikahan.Text = det.MaritalStatus;
                cboGolDarah.Text = det.BloodType;
                cboAgama.Text = det.Religion;
                cboPendidikan.Text = det.LastEducation;
                cboPekerjaan.Text = det.Occupation;
                txtNamaOrangTua.Text = det.ParentName;
                txtKeterangan.Text = det.Notes;
                txtWNRI.Text = det.SBKRINotes;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideModal", "$('#modalFormDetailID').modal('show');", true);
        }

        protected void gvw_detail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string PIDDetailID = DataBinder.Eval(e.Row.DataItem, "PersonalIdentificationDetailID").ToString();
                e.Row.Attributes.Add("onclick", "showModalFormDetailID(" + PIDDetailID + ")");
            }
        }
    }
}