﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class entrysmssender : System.Web.UI.Page
    {
        static String prevPage = "listsmssender.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);                

                if (Request.QueryString["SMSSenderID"] != null)
                {
                    Show_SMSSender(Int32.Parse(Request.QueryString["SMSSenderID"].ToString()));
                }
            }
        }

        protected void Show_SMSSender(Int32 _SMSSenderID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_SMSSender(_SMSSenderID);

            DataRow datarow = dt.Rows[0];
            txt_smssender.Text = datarow["SMSSender"].ToString();            
        }

        Boolean validation()
        {
            if (String.IsNullOrEmpty(txt_smssender.Text))
            {
                alert_danger_text.Text = "SMS Sender Name is required!";
                alert_danger.Visible = true;
                return false;
            }
            
            return true;
        }

        private void Save_SMSSender()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SMSSender = txt_smssender.Text;
            Int32 SMSSenderID = 0;
            
            if (Request.QueryString["SMSSenderID"] != null)
                SMSSenderID = Int32.Parse(Request.QueryString["SMSSenderID"].ToString());

            dbclass.Save_SMSSender(userData.OrgID, userData.DefaultSiteID, SMSSenderID, SMSSender, userData.UserName);
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (validation())
            {
                Save_SMSSender();
                Response.Redirect(prevPage);
            }
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}