﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

using residential.web.model;
using FirebirdSql.Data.FirebirdClient;
using residential.libs.Models;

namespace residential.web.forms
{
    public partial class viewsmartcardregistration : System.Web.UI.Page
    {
        //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
        //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
        //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
        //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
        //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(HttpContext.Current.User.Identity.Name);
                load_msboomgate();
                load_vehicle_type();
                load_card_type();

                load_resident_status();

                Session["ListSmartcardDetail"] = null;
                Session["SmartcardIdentificationDetailID"] = null;

                ViewState["UserData"] = new UserData(User.Identity.Name);
                //show_case(Request.QueryString["CaseNumber"]);
                //ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                Session["CaseNumber"] = null;
                if (Request.QueryString["CaseNumber"] != null)
                    show_case(Request.QueryString["CaseNumber"].ToString());
            }
            lnkbtn_add_detail.Enabled = true;
            lnkbtn_add_detail.Style.Add("opacity", "1");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "domultyselect", "domultyselect();", true);
        }

        private void show_case(String _casenumber)
        {
            Session["CaseNumber"] = _casenumber;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_Case(_casenumber);
            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                DataTable dt2 = dbclass.retrieve_smartcard(_casenumber);
                txt_TRSmartcardID.Text = dt2.Rows[0]["SmartCardID"].ToString();

                txt_quantity.Text = dt2.Rows[0]["Quantity"].ToString();
                ddl_cardtype.SelectedValue = dt2.Rows[0]["CardTypeID"].ToString();
                ddl_redident_status.SelectedValue = dt2.Rows[0]["ResidentStatus"].ToString();

                if (dt2.Rows[0]["ResidentStatus"].ToString().ToLower() == "rent")
                {
                    lbl_rent_periode_date.Visible = true;
                    txt_rent_periode_date.Visible = true;
                    DateTime rentperiodedate = DateTime.Parse(dt2.Rows[0]["RentPeriodeDate"].ToString());
                    txt_rent_periode_date.Text = rentperiodedate.ToString("dd MMMM yyyy");
                }

                txt_proce.Text = dt2.Rows[0]["Price"].ToString();
                txt_total.Text = dt2.Rows[0]["TotalAmount"].ToString();
                txt_formid.Text = ddl_cardtype.SelectedValue.ToString();

                hiddenID.Value = dt2.Rows[0]["SmartcardID"].ToString();
                DataTable dt3 = dbclass.retrieve_card_detail(int.Parse(hiddenID.Value.ToString()));

                //bool isdatasaved = false;
                List<CardDetail> list = new List<CardDetail>();

                foreach (DataRow dr in dt3.Rows)
                {
                    DateTime? objexpireddate = null;
                    if (dr["ExpiredDate"] != System.DBNull.Value)
                    {
                        objexpireddate = (DateTime)dr["ExpiredDate"];
                    }
                    else
                    {
                        objexpireddate = null;
                    }

                    var obj = new CardDetail(
                        (int)dr["OrgID"],
                        (int)dr["SiteID"],
                        (int)dr["SmartCardDetailID"],
                    (int)dr["SmartCardID"],
                    dr["CardNumber"].ToString(),
                    dr["ChipNumber"].ToString(),

                    dr["HolderName"].ToString(),
                    dr["IDCardNumber"].ToString(),
                    dr["OfficeAddress"].ToString(),
                    dr["MobilePhone"].ToString(),
                    dr["VehicleNumber"].ToString(),
                    (int)dr["VehicleType"],
                    dr["VehicleBrand"].ToString(),
                    (DateTime)dr["EffectiveDate"],
                    objexpireddate
                    );

                    //isdatasaved = ((int)dr["SmartCardDetailID"] > 0 ? true : false);

                    list.Add(obj);
                }
                Session["ListSmartcardDetail"] = list;                               
                load_msboomgate();

                //populate cluster selected
                List<string> selectedcluster = new List<string>();
                foreach (CardDetail cd in list)
                {
                    DataTable dtcluster = dbclass.retrieve_selected_cluster(cd.SmartcardIdentificationDetailID);
                    int i = 0;
                    foreach (DataRow dtc in dtcluster.Rows)
                    {
                        selectedcluster.Add(dtcluster.Rows[i]["BoomGateID"].ToString());
                        i++;
                    }
                }

                ListItem selecteditems = null;
                foreach (string sc in selectedcluster)
                {
                    selecteditems = LB_Cluster.Items.FindByValue(sc);
                    if (selecteditems != null)
                        selecteditems.Selected = true;
                }

                lnkbtn_add_detail.Visible = false;
                btnDelete.Visible = false;
                //btn_save_smartcard.Visible = false;
                search_form_member.Attributes.Add("style", "display:none");
                search_form_resident.Attributes.Add("style", "display:none");
                divcardstatusresident.Attributes.Add("style", "display:none");
                divcardstatusmember.Attributes.Add("style", "display:none");

                show_detail();
                controldisable();
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("listcardregistration.aspx");
        }

        protected void btn_cek_card_member_Click(object sender, EventArgs e)
        {
            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            error_meesage_member.Visible = false;
            string cardnumber = txt_cardnumber_member.Text;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_membership_card(OrgID, SiteID, cardnumber);

            if (dt.Rows.Count > 0)
            {
                txt_chipnumber_member.Text = dt.Rows[0]["ChipNumber"].ToString();
                txt_cardstatus_member.Text = dt.Rows[0]["CardStatus"].ToString();
            }
            else
            {
                lbl_error_message_member.Text = "card number is not available";
                error_meesage_member.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "selecttab", "selecttab();", true);
        }


        protected void btn_showDetailPersonalID_Click(object sender, EventArgs e)
        {
            //bool isdatasaved = false;        //0:save    1:view

            var list = (List<CardDetail>)Session["ListSmartcardDetail"];
            if (list.Count == int.Parse(txt_quantity.Text)) 
            {
                lnkbtn_add_detail.Enabled = false;
                lnkbtn_add_detail.Style.Add("opacity", "0.2");
            }

            int PDID = int.Parse(txt_smartcard_identifier_detail_ID.Value);
            foreach (CardDetail det in list)
            {
                if (det.SmartcardIdentificationDetailID == PDID)
                {
                    string formid = txt_formid.Text;
                    if (formid == "2")
                    {
                        txt_name_member.Text = det.HolderName;
                        txt_idcardnumber_member.Text = det.IDCardNumber;
                        txt_officeaddress_member.Text = det.OfficeAddress;
                        txt_mobilephone_member.Text = det.MobilePhone;
                        txt_cardnumber_member.Text = det.CardNumber;
                        txt_chipnumber_member.Text = det.ChipNumber;
                        txt_vehiclenumber_member.Text = det.VehicleNumber;
                        ddl_vehicletype.SelectedValue = det.VehicleType.ToString();
                        txt_vehiclebrand_member.Text = det.VehicleBrand.ToString();

                        txt_effectivedate_member.Text = det.EffectiveDate.ToString("dd MMMM yyyy");

                        DateTime? expireddatemember = det.ExpiredDate;
                        txt_expireddate_member.Text = (expireddatemember.HasValue == true ? expireddatemember.Value.ToString("dd MMMM yyyy") : "[N/A]");

                        divcardstatusmember.Attributes.Add("style", "display:none");
                        //isdatasaved = ((bool)det.IsSaved == true ? true : false);
                    }
                    else
                    {
                        txt_cardnumber_resident.Text = det.CardNumber;
                        txt_chipnumber_resident.Text = det.ChipNumber;
                        txt_vehiclenumber_resident.Text = det.VehicleNumber;
                        ddl_vehicletype_resident.SelectedValue = det.VehicleType.ToString();
                        txt_vehiclebrand_resident.Text = det.VehicleBrand.ToString();

                        txt_effectivedate_resident.Text = det.EffectiveDate.ToString("dd MMMM yyyy");
                        DateTime? expireddateresident = det.ExpiredDate;
                        //txt_expireddate_member.Text = (expireddateresident.HasValue == true ? expireddateresident.Value.ToString("dd MMMM yyyy") : "[N/A]");

                        string residentStatus = ddl_redident_status.SelectedValue;
                        if (residentStatus.ToLower() == "rent")
                        {
                            lbl_expireddate_resident.Visible = true;
                            txt_expireddate_resident.Visible = true;

                            if (expireddateresident != null)
                            {
                                DateTime rentPeriodeDate = DateTime.Parse(det.ExpiredDate.ToString());
                                txt_expireddate_resident.Text = rentPeriodeDate.ToString("dd MMMM yyyy");
                            }
                        }
 
                    }

                    if (det.SmartcardIdentificationDetailID > 0)
                    {
                        //view
                        lnkbtn_add_detail.Visible = false;
                        btnDelete.Visible = false;
                        //Button1.Attributes.Add("style", "display:none");

                    }                 
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "domultyselect", "domultyselect();", true);
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnk_edit = e.Row.FindControl("lnk_edit") as HyperLink;
                String SmartcardIdentificationDetailID = DataBinder.Eval(e.Row.DataItem, "SmartcardIdentificationDetailID").ToString();
                lnk_edit.Attributes["onclick"] = "showModalFormDetailID(" + SmartcardIdentificationDetailID + ");";
                if (int.Parse(SmartcardIdentificationDetailID) > 0)
                {
                    lnk_edit.Text = "View Detail";
                }
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            show_detail();
        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        //protected void btn_add_detail_Click(object sender, EventArgs e)
        //{
        //    show_detail();
        //    reset_form_member();
        //    reset_form_resident();
        //    //load_boomgate_listbox();

        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "domultyselect", "domultyselect();", true);
        //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "select_nexttab", "select_nexttab();", true);


        //    //cboRequest.Enabled = (gvw_data.Rows.Count > 0) ? false : true;

        //    //btnRefresh_Click(this, EventArgs.Empty);

        //    //reset_detail();
        //}

        protected void lnkbtn_add_detail_Click(object sender, EventArgs e)
        {
            if (lnkbtn_add_detail.Enabled)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showModalFormByCardType", "showModalFormByCardType();", true);

            show_detail();
            reset_form_member();
            reset_form_resident();
            reset_detail();
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "domultyselect", "domultyselect();", true);
        }

        private void reset_detail()
        {
            //alert_danger.Visible = false;
            //txt_cardnumber.Text = null;
            //txt_chipnumber.Text = null;
            //txt_cardholder_name.Text = null;
            //txt_effectivedate.Text = null;
            //txt_expireddate.Text = null;
            ////list_box_boomgate.SelectedIndex = 0;
            txt_smartcard_identifier_detail_ID.Value = null;
        }

        public void controldisable()
        {
            ddl_cardtype.Enabled = false;
            txt_quantity.Enabled = false;
            txt_proce.Enabled = false;
            txt_total.Enabled = false;
            ddl_redident_status.Enabled = false;
            //btn_save_smartcard.Visible = false;
        }

        protected void reset_form_member()
        {
            txt_name_member.Text = "";
            txt_idcardnumber_member.Text = "";
            txt_officeaddress_member.Text = "";
            txt_mobilephone_member.Text = "";

            txt_search_member.Text = "";
            txt_cardnumber_member.Text = "";
            txt_chipnumber_member.Text = "";
            txt_cardstatus_member.Text = "";
            txt_vehiclenumber_member.Text = "";
            txt_vehiclebrand_member.Text = "";
            ddl_vehicletype.SelectedIndex = 0;
            txt_vehiclebrand_member.Text = "";
            txt_effectivedate_member.Text = "";
            txt_expireddate_member.Text = "";
        }

        protected void reset_form_resident()
        {

            txt_search_resident.Text = "";
            txt_cardnumber_resident.Text = "";
            txt_chipnumber_resident.Text = "";
            txt_cardstatus_resident.Text = "";
            txt_vehiclebrand_resident.Text = "";
            txt_vehiclenumber_resident.Text = "";
            ddl_vehicletype.SelectedIndex = 0;
            txt_vehiclebrand_resident.Text = "";
            txt_effectivedate_resident.Text = "";
            txt_expireddate_resident.Text = "";
        }



        private void show_detail()
        {
            List<CardDetail> list = null;
            if (Session["ListSmartcardDetail"] != null)
                list = (List<CardDetail>)Session["ListSmartcardDetail"];

            hdf_cardremaining.Value = list.Count + " of " + txt_quantity.Text;
            gvw_data.DataSource = list;
            gvw_data.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "domultyselect", "domultyselect();", true);

            if (list.Count == int.Parse(txt_quantity.Text))
                lnkbtn_add_detail.Enabled = false;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var list = (List<CardDetail>)Session["ListSmartcardDetail"];

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 SmartcardIdentificationDetailID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());

                    if (ViewState["DeletedItems"] != null)
                    {
                        List<Int32> DeletedItems = (List<Int32>)ViewState["DeletedItems"];
                        DeletedItems.Add(SmartcardIdentificationDetailID);

                        ViewState["DeletedItems"] = DeletedItems;
                    }
                    else
                        ViewState["DeletedItems"] = new List<Int32> { SmartcardIdentificationDetailID };

                    var itemToRemove = list.SingleOrDefault(r => r.SmartcardIdentificationDetailID == SmartcardIdentificationDetailID);
                    if (itemToRemove != null)
                        list.Remove(itemToRemove);
                }
            }

            Session["ListSmartcardDetail"] = list;

            show_detail();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            show_detail();
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            //hitung_total();
        }

        /*
        protected void btn_savedetail_Click(object sender, EventArgs e)
        {
            alert_danger.Visible = false;


            //get data from control
            string formid = txt_formid.Text;

            string caseNumber = Request.QueryString["CaseNumber"].ToString();
            string name = "";
            string idcardnumber = "";
            string office = "";
            string officeaddress = "";
            string mobilephone = "";
            string email = "";
            string cardnumber = "";
            string chipnumber = "";
            string vehiclenumber = "";
            string vehicletype = "";
            string vehiclebrand = "";
            DateTime? effectifedatecard = null;
            DateTime? expireddatecard = null;
            if (formid == "2")
            {
                //member
                name = txt_name_member.Text;
                idcardnumber = txt_idcardnumber_member.Text;
                //office = txt_office_member.Text;
                officeaddress = txt_officeaddress_member.Text;
                mobilephone = txt_mobilephone_member.Text;

                cardnumber = txt_cardnumber_member.Text;
                chipnumber = txt_chipnumber_member.Text;

                vehiclenumber = txt_vehiclenumber_member.Text;
                vehicletype = ddl_vehicletype.SelectedValue;
                vehiclebrand = txt_vehiclebrand_member.Text;

                effectifedatecard = DateTime.Parse(txt_effectivedate_member.Text);
                expireddatecard = DateTime.Parse(txt_expireddate_member.Text);
            }
            else
            {
                //resident
                string _casenumber = Request.QueryString["CaseNumber"].ToString();
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                //UserData userData = (UserData)ViewState["UserData"];

                //DataTable dt = dbclass.retrieve_Case(caseNumber);

                //if (dt.Rows.Count > 0)
                //{
                //    name = dt.Rows[0]["psName"].ToString();
                //    //mobilephone = dt.Rows[0]["NamaPelapor"].ToString();
                //    //email = dt.Rows[0]["EmailPelapor"].ToString();
                //}
                name = viewheader.CustomerName;
                cardnumber = txt_cardnumber_resident.Text;
                chipnumber = txt_chipnumber_resident.Text;

                vehiclenumber = txt_vehiclenumber_resident.Text;
                vehicletype = ddl_vehicletype_resident.SelectedValue;
                vehiclebrand = txt_vehiclebrand_resident.Text;

                effectifedatecard = DateTime.Parse(txt_effectivedate_resident.Text);
                expireddatecard = DateTime.Now;

            }

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            List<CardDetail> list = new List<CardDetail>();
            Int32 counter = 0;

            if (Session["ListSmartcardDetail"] == null)
            {
                list = new List<CardDetail>();
                counter = -1;
            }
            else
            {
                list = (List<CardDetail>)Session["ListSmartcardDetail"];
                if (list.Count > 0)
                {
                    Int32 maxObj = list.Min(r => r.SmartcardIdentificationDetailID);
                    counter = maxObj - 1;
                }
                else counter = -1;
            }

            Int32 SmartCardIdentificationDetailID = 0;
            int TRSmartcardID = int.Parse(txt_TRSmartcardID.Text);
            if (!String.IsNullOrEmpty(txt_smartcard_identifier_detail_ID.Value))
            {
                SmartCardIdentificationDetailID = Int32.Parse(txt_smartcard_identifier_detail_ID.Value);

                foreach (CardDetail det in list)
                {
                    if (SmartCardIdentificationDetailID == det.SmartcardIdentificationDetailID)
                    {
                        //update                        
                        if (formid == "2")
                        {

                            det.HolderName = txt_name_member.Text;
                            det.IDCardNumber = txt_idcardnumber_member.Text;
                            det.OfficeAddress = txt_officeaddress_member.Text;
                            det.MobilePhone = txt_mobilephone_member.Text;
                            det.CardNumber = txt_cardnumber_member.Text;
                            det.ChipNumber = txt_chipnumber_member.Text;
                            det.VehicleNumber = txt_vehiclenumber_member.Text;
                            det.VehicleType = int.Parse(ddl_vehicletype.SelectedValue);
                            det.VehicleBrand = txt_vehiclebrand_member.Text;

                            det.EffectiveDate = DateTime.Parse(txt_effectivedate_member.Text);
                            det.ExpiredDate = DateTime.Parse(txt_expireddate_member.Text);


                        }
                        else
                        {
                            det.CardNumber = txt_cardnumber_resident.Text;
                            det.ChipNumber = txt_chipnumber_resident.Text;
                            det.VehicleNumber = txt_vehiclenumber_resident.Text;
                            det.VehicleType = int.Parse(ddl_vehicletype_resident.SelectedValue);
                            det.VehicleBrand = txt_vehiclebrand_resident.Text;

                            det.EffectiveDate = DateTime.Parse(txt_effectivedate_resident.Text);
                            //det.ExpiredDate = null;
                        }
                    }
                }
            }
            else
            {
                SmartCardIdentificationDetailID = counter;

                if (formid == "2")//member
                {
                    var obj = new CardDetail(
                        OrgID,
                        SiteID,
                        SmartCardIdentificationDetailID,
                    SmartCardIdentificationDetailID,
                    cardnumber,
                    chipnumber,
                    name,
                    idcardnumber,
                    officeaddress,
                    mobilephone,
                    vehiclenumber,
                    int.Parse(vehicletype),
                    vehiclebrand,
                    (DateTime)effectifedatecard,
                    (DateTime)expireddatecard
                    );

                    list.Add(obj);
                }
                else
                {
                    var obj = new CardDetail(
                        OrgID,
                        SiteID,
                        SmartCardIdentificationDetailID,
                    SmartCardIdentificationDetailID,
                    cardnumber,
                    chipnumber,
                    name,
                    idcardnumber,
                    officeaddress,
                    mobilephone,
                    vehiclenumber,
                    int.Parse(vehicletype),
                    vehiclebrand,
                    (DateTime)effectifedatecard,
                    null        //{resident expired date null}
                    );

                    list.Add(obj);
                }
            }


            //CardDetail carddetail = new CardDetail(TRSmartcardID,1,txt_cardnumber.Text, txt_chipnumber.Text);
            //list.Add(carddetail);


            Session["ListSmartcardDetail"] = list;
            gvw_data.DataSource = list;
            gvw_data.DataBind();

            hdf_cardremaining.Value = list.Count + " of " + txt_quantity.Text;

            if (list.Count == int.Parse(txt_quantity.Text))
            {
                lnkbtn_add_detail.Enabled = false;
                //lnkbtn_add_detail.Attributes.Add("OnClientClick ", "alert('asasas');");
                lnkbtn_add_detail.Style.Add("opacity", "0.2");
            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideModal", "closeDetailForm();", true);
        }
        */

        protected void btn_save_smartcard_Click(object sender, EventArgs e)
        {           
            try
            {
                lbl_error.Text = "";
                int cards = ((List<CardDetail>)Session["ListSmartcardDetail"]).Count;
                int qty = int.Parse(txt_quantity.Text);

                if (cards > qty)
                {
                    errID.Visible = true;
                    lbl_error.Text = "the card is over quantity";
                    return;
                }
                if (cards<qty){
                    errID.Visible = true;
                    lbl_error.Text = "Please insert more card";
                    return;
                }


                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];
                String CaseNumber = Request.QueryString["CaseNumber"];

                String SavedBy = userData.UserName;
                Int32 OrgID = userData.OrgID;
                Int32 SiteID = userData.SiteID;

                int TRSmartcardID = int.Parse(txt_TRSmartcardID.Text);

                //Int32 PersonalIdentificationID = Int32.Parse(dt.Rows[0]["PersonalIdentificationID"].ToString());
                //hiddenID.Value = PersonalIdentificationID.ToString();
                //Session["PersonalIdentificationID"] = PersonalIdentificationID.ToString();
                //Session["TRSmartcardID"] = TRSmartcardID.ToString();

                if (ViewState["DeletedItems"] != null)
                {
                    List<Int32> DeletedItems = (List<Int32>)ViewState["DeletedItems"];
                    for (var i = 0; i < DeletedItems.Count; i++)
                    {
                        //dbclass.delete_card_detail(DeletedItems[i]);
                    }
                }

                foreach (CardDetail _carddetail in (List<CardDetail>)Session["ListSmartcardDetail"])
                {
                    if (_carddetail.SmartcardIdentificationDetailID < 0)
                    {

                        String[] returnvalue = dbclass.save_card_detail(OrgID, SiteID, TRSmartcardID, _carddetail.CardNumber, _carddetail.ChipNumber, _carddetail.HolderName, _carddetail.IDCardNumber, _carddetail.OfficeAddress, _carddetail.MobilePhone, _carddetail.VehicleNumber, _carddetail.VehicleType, _carddetail.VehicleBrand, _carddetail.EffectiveDate, _carddetail.ExpiredDate, SavedBy);

                        if (returnvalue[0] == "0")
                        {
                            // update card issued to member
                            string casenumber = Request.QueryString["CaseNumber"];
                            DataTable dt = dbclass.retrieve_pscode(casenumber);
                            string pscode = dt.Rows[0]["PSCode"].ToString();
                            string updatereturn = dbclass.update_issued_to_member(_carddetail.CardNumber, pscode);

                            //update case to progress compplate
                            String FullName = userData.FullName;
                            String Remarks = "";
                            int CaseStatusID = 6;//progress complate

                            Int32 returupdatecase = dbclass.save_tickethistory(CaseNumber, CaseStatusID, FullName, Remarks, SavedBy, null);
                            /*update_to_crm(CaseNumber, CaseStatusID, Remarks, FullName, null, 0);*/


                            // insert to TRBoomgateAccess
                            string selectedvaluecluster = string.Empty;
                            foreach (ListItem listgatecluster in LB_Cluster.Items)
                            {
                                if (listgatecluster.Selected == true)
                                {
                                    int TRCardDetailID = int.Parse(returnvalue[1]);
                                    
                                    String[] alamat = viewheader.Unit.Split('-');
                                    string customername = "";
                                    String cluster = "";
                                    String unitnumber = "";

                                    if (int.Parse(txt_formid.Text) == 2)
                                        customername = _carddetail.HolderName;
                                    else
                                        customername = viewheader.CustomerName;

                                    if (alamat.Length == 2)
                                    {
                                        cluster = alamat[0];
                                        unitnumber = alamat[1];
                                    }

                                    try
                                    {
                                        //save to boomgate server
                                        int _boomGateID=0;
                                        string _boomGateCode = "";

                                        _boomGateID = int.Parse(listgatecluster.Value);
                                        DataTable dt_gatecode = dbclass.retrieve_boomgatecode(_boomGateID);
                                        _boomGateCode = dt_gatecode.Rows[0]["BoomGateCode"].ToString();


                                        String boomgateconstr = ConfigurationManager.ConnectionStrings["boomgateconstr"].ToString();
                                        FbConnection bgconn = new FbConnection(boomgateconstr);
                                        bgconn.Open();

                                        FbTransaction trans = bgconn.BeginTransaction();

                                        String cmdsql = "INSERT INTO PushData (Boom, Mifare, Nama, Alamat, Status) VALUES ('" + _boomGateCode + "', '" + _carddetail.ChipNumber + "', '" + customername + "', 'Cluster: " + cluster + ", Unit No: " + unitnumber + "', 'UPDATE')";
                                        FbCommand cmd = new FbCommand(cmdsql, bgconn, trans);
                                        cmd.ExecuteNonQuery();
                                        trans.Commit();

                                        bgconn.Close();

                                        //sync success
                                        string ret_value = dbclass.insert_transaction_cluster_access(OrgID, SiteID, TRCardDetailID, int.Parse(listgatecluster.Value), 1);
                                    
                                    }
                                    catch (Exception fbde)
                                    {
                                        //sync failed
                                        string ret_value = dbclass.insert_transaction_cluster_access(OrgID, SiteID, TRCardDetailID, int.Parse(listgatecluster.Value), 0);
                                    
                                        int retunpushdata = dbclass.save_pushdata_error(listgatecluster.Value, _carddetail.ChipNumber, customername, "Cluster: " + cluster + ",Unit No : " + unitnumber, "UPDATE", fbde.Message.ToString());
                                        //error on push data to boomgate
                                    }
                                }
                            }

                        }
                        else
                        {
                            lbl_error.Text = returnvalue[0].ToString();
                            errID.Visible = true;
                            return;
                        }
                    }
                }

                //tambahan
                //insert to trmembership transaction
                string _casenumber = Session["CaseNumber"].ToString();
                DataTable dt_trans = dbclass.retrieve_smartcard_transaction_bycase(_casenumber);
                foreach (DataRow dr in dt_trans.Rows)
                {
                    int _membershipID = (int)dr["MembershipID"];
                    int _boomGateID = (int)dr["BoomGateID"];
                    int _status = 1;
                    int _sync = (int)dr["Sync"];
                    string returnval_memberhip = dbclass.insert_membership_transaction(_membershipID, _boomGateID, _status, _sync);
                }

                Session["ListSmartcardDetail"] = null;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("listcardregistration.aspx", false);

            }
            catch (Exception ex)
            {
                lbl_error.Text = ex.Message.ToString();
                errID.Visible = true;
                //alert_danger_text.Text = ex.Message;
                //alert_danger.Visible = true;
            }
        }

        //private void update_to_crm(String _casenumber, Int32 _crmcasestatusid, String _remarks, String _updatedby, String _workedby, Int32 _SatisfactionID)
        //{
        //    try
        //    {
        //        String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
        //        String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
        //        String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
        //        String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
        //        String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();

        //        CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

        //        UpdateCaseHelpDeskStatusRequest request = new UpdateCaseHelpDeskStatusRequest();
        //        request.CRMCaseNumber = _casenumber;
        //        request.CRMCaseStatusID = _crmcasestatusid;
        //        request.Remarks = _remarks;
        //        request.UpdateBy = _updatedby;
        //        request.UpdateDate = DateTime.Now;
        //        request.WorkedBy = _workedby;
        //        request.SatisfactionID = _SatisfactionID;
        //        UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(request);
        //    }
        //    catch (Exception e)
        //    {
        //        alert_danger_text.Text = e.Message;
        //        alert_danger.Visible = false;
        //    }
        //}

        //load master


        protected void load_card_type()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_master_card_type();

            ddl_cardtype.DataSource = dt;
            //ddl_cardtype.DataValueField = "CardTypeID";
            //ddl_cardtype.DataTextField = "CardTypeName";
            ddl_cardtype.DataValueField = "SmartcardTypeID";
            ddl_cardtype.DataTextField = "SmartcardTypeName";
            ddl_cardtype.DataBind();

        }

        protected void load_resident_status()
        {
            List<string> resident_status = new List<string>();
            resident_status.Add("Owner");
            resident_status.Add("Rent");

            ddl_redident_status.DataSource = resident_status;
            ddl_redident_status.DataBind();
        }

        protected void load_msboomgate()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
            DataTable dt = dbclass.retrieve_msboomgate(OrgID,SiteID);

            //LB_Cluster.DataValueField = "BoomGateCode";
            LB_Cluster.DataValueField = "BoomGateID";
            LB_Cluster.DataTextField = "BoomGateName";
            LB_Cluster.DataSource = dt;
            LB_Cluster.DataBind();
        }

        protected void load_vehicle_type()
        {
            VehicleType mobil = new VehicleType();
            mobil.VehicleTypeID = 1;
            mobil.VehicleTypeName = "Mobil";

            VehicleType motor = new VehicleType();
            motor.VehicleTypeID = 2;
            motor.VehicleTypeName = "Motor";

            List<VehicleType> list_vehicle_type = new List<VehicleType>();
            list_vehicle_type.Add(mobil);
            list_vehicle_type.Add(motor);

            ddl_vehicletype.DataValueField = "VehicleTypeID";
            ddl_vehicletype.DataTextField = "VehicleTypeName";
            ddl_vehicletype.DataSource = list_vehicle_type;
            ddl_vehicletype.DataBind();

            ddl_vehicletype_resident.DataValueField = "VehicleTypeID";
            ddl_vehicletype_resident.DataTextField = "VehicleTypeName";
            ddl_vehicletype_resident.DataSource = list_vehicle_type;
            ddl_vehicletype_resident.DataBind();

        }

        protected void txt_search_member_TextChanged(object sender, EventArgs e)
        {
            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
            //Button1.Enabled = true;
            error_meesage_member.Visible = false;
            string cardnumber = txt_search_member.Text;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_membership_card(OrgID, SiteID, cardnumber);

            if (dt.Rows.Count > 0)
            {
                List<CardDetail> listCardDetail = new List<CardDetail>();
                if (Session["ListSmartcardDetail"] != null)
                    listCardDetail = (List<CardDetail>)Session["ListSmartcardDetail"];

                if (listCardDetail.Where(w =>
                    w.CardNumber.ToString().Trim().ToLower() == dt.Rows[0]["CardNumber"].ToString().Trim().ToLower()).ToList().Count() <= 0)
                {
                    txt_cardnumber_member.Text = dt.Rows[0]["CardNumber"].ToString();
                    txt_chipnumber_member.Text = dt.Rows[0]["ChipNumber"].ToString();
                    txt_cardstatus_member.Text = dt.Rows[0]["CardStatus"].ToString();
                }
                else
                {
                    lbl_error_message_member.Text = "card number sudah dipilih";
                    error_meesage_member.Visible = true;
                    //Button1.Enabled = false;

                    txt_cardnumber_member.Text = "";
                    txt_chipnumber_member.Text = "";
                    txt_cardstatus_member.Text = "";
                    txt_cardnumber_member.Text = "";
                }
            }
            else
            {
                lbl_error_message_member.Text = "card number tidak tersedia";
                txt_cardnumber_member.Text = "";
                txt_chipnumber_member.Text = "";
                txt_cardstatus_member.Text = "";
                error_meesage_member.Visible = true;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "selecttab", "selecttab();", true);
        }

        protected void txt_search_resident_TextChanged(object sender, EventArgs e)
        {
            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
            //Button4.Enabled = true;
            error_meesage_resident.Visible = false;
            string cardnumber = txt_search_resident.Text;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_membership_card(OrgID, SiteID, cardnumber);

            //List<SmartCard> list = new List<SmartCard>();

            if (dt.Rows.Count > 0)
            {
                List<CardDetail> listCardDetail = new List<CardDetail>();
                if (Session["ListSmartcardDetail"] != null)
                    listCardDetail = (List<CardDetail>)Session["ListSmartcardDetail"];

                if (listCardDetail.Where(w =>
                    w.CardNumber.ToString().Trim().ToLower() == dt.Rows[0]["CardNumber"].ToString().Trim().ToLower()).ToList().Count() <= 0)
                {
                    txt_cardnumber_resident.Text = dt.Rows[0]["CardNumber"].ToString();
                    txt_chipnumber_resident.Text = dt.Rows[0]["ChipNumber"].ToString();
                    txt_cardstatus_resident.Text = dt.Rows[0]["CardStatus"].ToString();
                }
                else
                {
                    lbl_error_message_resident.Text = "card number sudah dipilih";
                    error_meesage_resident.Visible = true;
                    //Button4.Enabled = false;

                    txt_cardnumber_resident.Text = "";
                    txt_cardstatus_resident.Text = "";
                    txt_chipnumber_resident.Text = "";
                }
            }
            else
            {
                lbl_error_message_resident.Text = "card number tidak tersedia";
                error_meesage_resident.Visible = true;

                txt_cardnumber_resident.Text = "";
                txt_cardstatus_resident.Text = "";
                txt_chipnumber_resident.Text = "";
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "selecttab", "selecttab();", true);
        }

        //protected void txt_effectivedate_resident_TextChanged(object sender, EventArgs e)
        //{
        //    //ScriptManager.RegisterStartupScript(this, this.GetType(), "selecttab", "selecttab();", true);
        //    //txt_expireddate_resident.Enabled = false;
        //}

        protected void txt_cardnumber_resident_TextChanged(object sender, EventArgs e)
        {
            //Button4.Enabled = true;
            error_meesage_resident.Visible = false;
            string cardnumber = txt_cardnumber_resident.Text;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            string returncardnumber = dbclass.validate_card_number(cardnumber);
            if (int.Parse(returncardnumber) > 0)
            {
                lbl_error_message_resident.Text = "Card number already exist";
                error_meesage_resident.Visible = true;
            }
            else
            {
                List<CardDetail> lcd = new List<CardDetail>();
                if (Session["ListSmartcardDetail"] != null)
                    lcd = (List<CardDetail>)Session["ListSmartcardDetail"];

                if (lcd.Where(w => w.CardNumber.Trim().ToLower() == txt_cardnumber_resident.Text.Trim().ToLower()).ToList().Count() > 0)
                {
                    lbl_error_message_resident.Text = "Card number already in use";
                    error_meesage_resident.Visible = true;
                    //Button4.Enabled = false;
                }
            }
        }

        protected void txt_effectivedate_member_TextChanged(object sender, EventArgs e)
        {
            error_meesage_member.Visible = false;

            if (!string.IsNullOrEmpty(txt_effectivedate_member.Text))
            {
                DateTime expireddate = DateTime.Parse(txt_effectivedate_member.Text).AddMonths(12);
                txt_expireddate_member.Text = expireddate.ToString("dd MMMM yyyy");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "selecttab", "selecttab();", true);
            }           
        }

        protected void txt_vehiclenumber_resident_TextChanged(object sender, EventArgs e)
        {
            //cek vehicle number
            //Button4.Enabled = true;
            error_meesage_resident.Visible = false;
            string vehicleNumber = txt_vehiclenumber_resident.Text;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            string returnvehiclenumber = dbclass.validate_vehicle_number(vehicleNumber);
            if (int.Parse(returnvehiclenumber) > 0)
            {
                lbl_error_message_resident.Text = "Vehicle number already exist";
                error_meesage_resident.Visible = true;
                //Button4.Enabled = false;
            }
            else
            {
                List<CardDetail> lcd = new List<CardDetail>();
                if (Session["ListSmartcardDetail"] != null)
                    lcd = (List<CardDetail>)Session["ListSmartcardDetail"];

                if (lcd.Where(w => w.VehicleNumber.Trim().ToLower() == txt_vehiclenumber_resident.Text.Trim().ToLower()).ToList().Count() > 0)
                {
                    lbl_error_message_resident.Text = "Vehicle number already in use";
                    error_meesage_resident.Visible = true;
                    //Button4.Enabled = false;
                }
            }
            txt_vehiclenumber_resident.Focus();            
        }

        protected void txt_vehiclenumber_member_TextChanged(object sender, EventArgs e)
        {
            //Button1.Enabled = true;
            error_meesage_member.Visible = false;
            string vehicleNumber = txt_vehiclenumber_member.Text;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            string returnvehiclenumber = dbclass.validate_vehicle_number(vehicleNumber);

            if (int.Parse(returnvehiclenumber) > 0)
            {
                lbl_error_message_member.Text = "Vehicle number already exist";
                error_meesage_member.Visible = true;
                //Button1.Enabled = false;
            }
            else
            {
                List<CardDetail> lcd = new List<CardDetail>();
                if (Session["ListSmartcardDetail"] != null)
                    lcd = (List<CardDetail>)Session["ListSmartcardDetail"];

                if (lcd.Where(w => w.VehicleNumber.Trim().ToLower() == txt_vehiclenumber_member.Text.Trim().ToLower()).ToList().Count()>0)
                {
                    lbl_error_message_member.Text = "Vehicle number already in use";
                    error_meesage_member.Visible = true;
                    //Button1.Enabled = false;
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "selecttab", "selecttab();", true);
        }
    }
}