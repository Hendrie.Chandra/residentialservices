﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listuser.aspx.cs" Inherits="residential.web.forms.listuser" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>List User</h1>
            </div>
            <asp:Button CssClass="btn btn-primary" ID="btn_add" runat="server" Text="Add"
                OnClick="btn_add_Click" />
            <asp:Button CssClass="btn btn-default" ID="btn_delete" runat="server" Text="Delete"
                OnClientClick="return confirmDelete();" OnClick="btn_delete_Click" />
            <hr />
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-6"></div>                   
                    <label class="col-sm-2 control-label">Search </label>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" OnTextChanged="txt_search_TextChanged" AutoPostBack="true" placeholder="username, fullname"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" OnClick="btn_search_Click" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" 
                CssClass="table table-bordered table-hover" AllowPaging="True" onrowcommand="gvw_data_RowCommand" 
                onrowdatabound="gvw_data_RowDataBound" DataKeyNames="userid" 
                OnPageIndexChanging="gvw_data_PageIndexChanging" ShowHeaderWhenEmpty="True">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Center">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk_selectall" runat="server" oncheckedchanged="chk_selectall_CheckedChanged" AutoPostBack="true" />
                        </HeaderTemplate>                        
                        <ItemTemplate>
                            <asp:CheckBox ID="chk_select" runat="server" />
                        </ItemTemplate>                
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="30px">                        
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="FULL NAME" ItemStyle-Font-Bold="true">                       
                        <ItemTemplate>
                            <asp:HyperLink ID="lnk_user" runat="server" CssClass="gridview_link">full name</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="ROLE" DataField="rolename" />
                    <asp:BoundField HeaderText="DEPARTMENT NAME" DataField="DepartmentName" />
                    <asp:BoundField HeaderText="EMAIL" DataField="emailaddress" />
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>                
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
