﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listbannerlocation.aspx.cs" Inherits="residential.web.forms.listbannerlocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>List Banner Location</h1>
    </div>
    <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-primary" NavigateUrl="~/forms/entrybannerlocation.aspx">Add</asp:HyperLink>
    <asp:Button ID="Button1" runat="server" CssClass="btn btn-default" Text="Delete" OnClick="btn_delete_Click" OnClientClick="return confirmDelete();"  />
    <hr />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Banner Type</label>
                    <div class="col-sm-10">
                        <asp:DropDownList ID="cbo_bannerType" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="cbo_bannerType_SelectedIndexChanged" CssClass="form-control">
                        </asp:DropDownList>
                    </div>                    
                </div>
            </div>
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-hover table-bordered" AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound"
                OnPageIndexChanging="gvw_data_PageIndexChanging" PagerStyle-CssClass="pagination-ys"
                ShowHeaderWhenEmpty="True"
                DataKeyNames="BannerLocationID">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk_selectall" runat="server"
                                OnCheckedChanged="chk_selectall_CheckedChanged" AutoPostBack="true" />
                        </HeaderTemplate>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chk_select" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Banner Location Name">
                        <HeaderStyle CssClass="gridview_header" Width="200px" />
                        <ItemStyle CssClass="gridview_item_left" />
                        <ItemTemplate>
                            <asp:HyperLink ID="lnk_bannerlocation" runat="server"
                                CssClass="gridview_link">Banner location ID</asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Banner Location Code" DataField="BannerLocationCode">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Price" HeaderText="PRICE" DataFormatString="{0:N0}">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Points" HeaderText="POINTS">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
