﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using residential.libs;
using System.Configuration;
using System.Data;

namespace residential.web.forms
{
    public partial class CDCompleteCase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["UserData"] = new UserData(User.Identity.Name);
            bounddatagrid();
            SD.Text = "Period      :   " + Request.QueryString["StartDate"] + " - " + Request.QueryString["EndDate"];
            DN.Text = "Department  :   " + Request.QueryString["DeptCode"];
        }

        private void bounddatagrid()
        {
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DateTime StartDate = Convert.ToDateTime(Request.QueryString["StartDate"]);
                DateTime EndDate = Convert.ToDateTime(Request.QueryString["EndDate"]);
                string DeptCode = Convert.ToString(Request.QueryString["DeptCode"]);

                DataSet dt = dbclass.cdcaseclose(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptCode);

                DataView dv = new DataView(dt.Tables[0]);

                gvw_data.DataSource = null;
                gvw_data.DataSource = dv;
                gvw_data.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid();
        }
    }
}