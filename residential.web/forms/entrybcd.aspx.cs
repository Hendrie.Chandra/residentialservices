﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using residential.libs.Models;
using System.Data;
using System.Globalization;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entrybcd : System.Web.UI.Page
    {
        static String prevPage = "listbcd.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {           
            if (!IsPostBack)
            {                
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_cbo_servicerequest();
                show_requestprice();
                hitung_total();                
               
                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            entryheader.ToggleAlert = false;
        }

        private void Show_Case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            try
            {                
                DataTable dt = dbclass.retrieve_BCD(_casenumber);

                entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                //check customer type -> 0 = Owner, 1 = Tenant
                entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheader.combobox_unit();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();                
                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

                entryheader.disable_all_controls();

                cbo_servicerequests.SelectedValue = dt.Rows[0]["HelpID"].ToString();
                if (dt.Rows[0]["Price"].ToString() != null || dt.Rows[0]["Price"].ToString() != "")
                {
                    lbl_price.Text = dt.Rows[0]["Price"].ToString();
                }
                else
                {
                    lbl_price.Text ="0";
                }
                txt_discount.Text = dt.Rows[0]["TotalDiscount"].ToString();
                 hitung_total();
            }
            catch (Exception)
            {                
                throw new HttpException(404, "Page not found");                
            }
        }

        private void show_cbo_servicerequest()
        {            
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            
            DataTable dt = dbclass.retrieve_BCD_HelpName(userData.OrgID, userData.SiteID);

            cbo_servicerequests.DataSource = dt;
            cbo_servicerequests.DataTextField = "HelpName";
            cbo_servicerequests.DataValueField = "HelpID";
            cbo_servicerequests.DataBind();
            cbo_servicerequests.SelectedIndex = 0;
        }

        private void show_requestprice()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            if (cbo_servicerequests.Items.Count > 0)
            {
                Int32 helpid = Int32.Parse(cbo_servicerequests.SelectedValue);
                DataTable dt = dbclass.retrieve_bcdprice(helpid);
                Int32 price = 0;
                if (dt.Rows.Count > 0)
                {
                    price = Convert.ToInt32(dt.Rows[0]["Price"]);
                }
                
                lbl_price.Text = price.ToString("N0");
                hitung_total();
            }
        }

        protected void cbo_servicerequests_SelectedIndexChanged(object sender, EventArgs e)
        {
            show_requestprice();            
        }

        private void hitung_total()
        {            
            if (String.IsNullOrWhiteSpace(txt_discount.Text))
                txt_discount.Text = "0";            

            Int32 subtotal = Int32.Parse(lbl_price.Text, NumberStyles.Currency);
            Int32 total = subtotal - Int32.Parse(txt_discount.Text);

            lbl_subtotal.Text = subtotal.ToString("N0");
            Decimal disc = Decimal.Parse(txt_discount.Text);
            lbl_adjustment.Text = disc.ToString("N0");
            lbl_total.Text = total.ToString("N0");
        }                

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }        

        private String save_bcd_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();            
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
            Int32 HelpID = int.Parse(cbo_servicerequests.SelectedValue);
            Int32 CaseOriginID = entryheader.CaseOriginID;            
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            Nullable<DateTime> StartDate = null;
            Nullable<DateTime> EndDate = null;
            //if (!String.IsNullOrWhiteSpace(txt_startdate.Text) && !String.IsNullOrWhiteSpace(txt_enddate.Text))
            //{
            //    StartDate = DateTime.Parse(txt_startdate.Text);
            //    EndDate = DateTime.Parse(txt_enddate.Text);
            //}

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);
            Int32 Quantity = 1;
            Int32 Price = Int32.Parse(lbl_price.Text, NumberStyles.Currency);
            Int32 TotalAmount = Quantity * Price;
            Int32 TotalDiscount = Int32.Parse(txt_discount.Text, NumberStyles.Currency);
            Int32 TotalAmountAfterDiscount = TotalAmount - TotalDiscount;
            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;
            
            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(Int32.Parse(cbo_servicerequests.SelectedValue));
            request.HelpID = Int32.Parse(cbo_servicerequests.SelectedValue);
            request.TotalPrice = TotalAmount;
            request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            /*if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            }*/

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_bcd_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, StartDate, EndDate, Quantity, Price, TotalAmount, TotalDiscount, TotalAmountAfterDiscount,
                    PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected bool bcd_transaction_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }
            return true;
        }
        
        protected void btn_saveclose_Click(object sender, EventArgs e)
        {                        
            if (bcd_transaction_validation())
            {
                String res = save_bcd_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }        

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }        
        
        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }        
    }
}
