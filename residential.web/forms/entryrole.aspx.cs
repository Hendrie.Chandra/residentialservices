﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.Data;

using residential.libs;

namespace residential.web.forms
{
    public partial class entryrole : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                UserData userData = (UserData)ViewState["UserData"];

                alert_danger.Visible = false;
                Int32 RoleID = 0;
                if (Request.QueryString["RoleID"] != null)
                    RoleID = Int32.Parse(Request.QueryString["RoleID"].ToString());

                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                DataTable dt = dbclass.retrieve_role(RoleID);
                if (dt.Rows.Count > 0)
                {
                    DataRow row = dt.Rows[0];
                    txt_rolename.Text = row["RoleName"].ToString();
                    txt_roledescription.Text = row["RoleDescription"].ToString();

                    String roleaccess = row["RoleAccess"].ToString();
                    if (roleaccess != "0")
                    {
                        if (roleaccess.Substring(0, 1) == "1")
                            chk_role1.Checked = true;
                        else
                            chk_role1.Checked = false;

                        if (roleaccess.Substring(1, 1) == "1")
                            chk_role2.Checked = true;
                        else
                            chk_role2.Checked = false;

                        if (roleaccess.Substring(2, 1) == "1")
                            chk_role3.Checked = true;
                        else
                            chk_role3.Checked = false;

                        if (roleaccess.Substring(3, 1) == "1")
                            chk_role4.Checked = true;
                        else
                            chk_role4.Checked = false;

                        if (roleaccess.Substring(4, 1) == "1")
                            chk_role5.Checked = true;
                        else
                            chk_role5.Checked = false;

                        if (roleaccess.Substring(5, 1) == "1")
                            chk_role6.Checked = true;
                        else
                            chk_role6.Checked = false;

                        if (roleaccess.Substring(6, 1) == "1")
                            chk_role7.Checked = true;
                        else
                            chk_role7.Checked = false;

                        if (roleaccess.Substring(7, 1) == "1")
                            chk_role8.Checked = true;
                        else
                            chk_role8.Checked = false;

                        if (roleaccess.Substring(8, 1) == "1")
                            chk_role9.Checked = true;
                        else
                            chk_role9.Checked = false;
                    }
                }
            }
        }

        private Int32 save_roles()
        {

            UserData userData = (UserData)ViewState["UserData"];
            String SavedBy = userData.UserName;
            Int32 RoleID = 0;
           // String SavedBy = Session["username"].ToString();

            //String flag = Request.QueryString["flag"].ToString();
            if (Request.QueryString["RoleID"] != null)
                RoleID = Int32.Parse(Request.QueryString["RoleID"].ToString());

            String roleaccess = "";

            if (chk_role1.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            if (chk_role2.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            if (chk_role3.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            if (chk_role4.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            if (chk_role5.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            if (chk_role6.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            if (chk_role7.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            if (chk_role8.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            if (chk_role9.Checked)
                roleaccess = roleaccess + "1";
            else
                roleaccess = roleaccess + "0";

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = dbclass.save_role(RoleID, txt_rolename.Text, roleaccess, txt_roledescription.Text, SavedBy);

            return returnvalue;            
        }

        protected Boolean roles_validation()
        {
            if (String.IsNullOrEmpty(txt_rolename.Text))
            {
                alert_danger_text.Text = "Role name is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (roles_validation())
            {
                Int32 returnvalue = save_roles();
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (roles_validation())
            {
                Int32 returnvalue = save_roles();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            }
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            if (roles_validation())
            {
                Int32 returnvalue = save_roles();
                txt_rolename.Text = String.Empty;
                txt_roledescription.Text = String.Empty;

                //foreach (Control c in this.Controls)
                //{
                //    if (c is CheckBox)
                //    {
                //        CheckBox chk = (CheckBox)c;
                //        chk.Checked = false;
                //    }
                //}
                chk_role1.Checked = false;
                chk_role2.Checked = false;
                chk_role3.Checked = false;
                chk_role4.Checked = false;
                chk_role5.Checked = false;
                chk_role6.Checked = false;
                chk_role7.Checked = false;
                chk_role8.Checked = false;
                chk_role9.Checked = false;
            }
        }       
    }
}