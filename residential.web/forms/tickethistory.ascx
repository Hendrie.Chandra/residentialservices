﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="tickethistory.ascx.cs" Inherits="residential.web.forms.tickethistory" %>

<asp:GridView ID="gvw_tickethistory" runat="server" CssClass="table table-hover table-bordered" AutoGenerateColumns="False">
    <Columns>
        <%--<asp:TemplateField HeaderText="Modified Date">
            <ItemTemplate>
                <%# Convert.ToDateTime(Eval("ModifiedDate")).ToLocalTime().ToString("dd MMMM yyyy HH:mm") %>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" />
        </asp:TemplateField>--%>
        <asp:BoundField DataField="ModifiedDate" HeaderText="Modified Date" DataFormatString="{0:dd/MM/yyyy HH:mm}"/>
        <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
        <asp:BoundField DataField="CaseStatusName" HeaderText="Status" />
        <asp:BoundField DataField="FullName" HeaderText="Modified By" />
    </Columns>
    <EmptyDataTemplate>
        No data
    </EmptyDataTemplate>
</asp:GridView>
