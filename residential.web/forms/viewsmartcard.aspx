﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewsmartcard.aspx.cs" Inherits="residential.web.forms.viewsmartcard" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../content/smartcard.css" rel="stylesheet" />
    <script src="../scripts/smartcard.js"></script>
    <script>     
        function selectnexttab(event) {
            $('.nav-tabs a[href="#kelengkapandokumen"]').tab('show');
            event.preventDefault();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
<%--    <div id="loader">
        <img class="loading-gif" src="../images/loader.GIF" />
    </div>--%>
    <div class="page-header">
        <h1>Smart Card Request <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
    </div>
    <%--<hr />--%>
    <aspuc:ViewHeader ID="viewheader" runat="server" /> 
    
   <%-- dokumen--%>
                <div class="panel panel-default">
                <div class="panel-heading">Smart Card Detail</div>
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#smartcardrequestdetail" aria-controls="home" role="tab" data-toggle="tab">Request Detail</a></li>
                        <li role="presentation"><a href="#kelengkapandokumen" aria-controls="profile" role="tab" data-toggle="tab">Supporting Documents</a></li>
                        <li role="presentation"><a href="#history" aria-controls="profile" role="tab" data-toggle="tab">History</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="smartcardrequestdetail">
                            <br />
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Card Type</label>
                                    <div class="col-sm-4">
                                        <%--<asp:DropDownList ID="ddl_card_type" CssClass="form-control" runat="server" onchange="getpriceonchange();"></asp:DropDownList>--%>
                                        <asp:DropDownList ID="ddl_card_type" CssClass="form-control" Enabled="false" runat="server"></asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Resident Status</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_resident_status" CssClass="form-control" Enabled="false" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Quantity</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_quantity" CssClass="form-control" ReadOnly="true" runat="server" Type="Number" MaxLength="7"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:CheckBox ID="CheckBox_isreplacement" runat="server" CssClass="pull-right" style="padding-top:11px"/>
                                    </div>
                                    <label class="col-sm-4 control-label" style="text-align:left">Is Replacement</label>
                                </div>
                                <%--rent periode--%>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">                                        
                                    </div>                                    
                                    <label id="lbl_rent_periode_date" class="col-sm-2 control-label" runat="server" visible="false">Rent Periode Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_rent_periode_date" CssClass="form-control" runat="server" Visible="false"></asp:TextBox>
                                    </div>
                                </div>
                               <%-- rent periode--%>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <%--<asp:TextBox ID="txt_cost" CssClass="form-control" runat="server" placeholder="cost"></asp:TextBox>--%>
                                    </div>
                                    <label class="col-sm-2 control-label">Price</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_price" CssClass="form-control" runat="server" ReadOnly="true" placeholder="Price"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                    </div>
                                    <label class="col-sm-2 control-label">Total</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_total" CssClass="form-control" runat="server" ReadOnly="true" placeholder="total"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        
                                    </div>
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-2">
                                        <%--<asp:Button ID="btn_calculate" runat="server" Text="Calculate" CssClass="btn btn-default" OnClick="btn_calculate_Click" />--%>
                                        
                                    </div>
                                    <div class="col-sm-2">
                                        <%--<asp:Button ID="btn_reset" runat="server" Text="Reset" CssClass="btn btn-default" OnClick="btn_reset_Click" />--%>
                                    </div>
                                </div>

                               <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                    </div>
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <%--<asp:Button ID="btn_next_todokumen" runat="server" UseSubmitBehavior="false" OnClientClick="selectnexttab();" Text="Next" />--%>
                                        <input type="button" id="btn_nexttab" class="btn btn-default pull-right"  onclick="selectnexttab(this)" value="next" />                             
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="kelengkapandokumen">
                            <br />
                            <asp:Button ID="btn_download_file" runat="server" Text="..." style="display:none" OnClick="btn_download_file_Click" />
                            <asp:HiddenField ID="hdf_case_number" runat="server" />
                            <asp:HiddenField ID="hdf_db_field" runat="server" ClientIDMode="Static" />
                            <%-- check box --%>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_identitasdiri_base64" runat="server" />
                                <asp:HiddenField ID="hdf_identitasdiri_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_identitasdiri" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Identitas Diri Terbaru (KTP/SIM/KITAS)</label>
                                <div class="col-sm-6" style="padding-bottom:20px">                                    
                                    <asp:HyperLink ID="hpl_identitasdiri" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_kk_base64" runat="server" />
                                <asp:HiddenField ID="hdf_kk_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_kk" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Kartu Keluarga</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_kk" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_stnk_base64" runat="server" />
                                <asp:HiddenField ID="hdf_stnk_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_stnk" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy STNK</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_stnk" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_kepemilikanunit_base64" runat="server" />
                                <asp:HiddenField ID="hdf_kepemilikanunit_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_kepemilikanunit" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Bukti Kepemilikan Unit (PPJB/AJB/Sertifikat)</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_kepemilikanunit" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_perjanjiansewamenyewaunit_base64" runat="server" />
                                <asp:HiddenField ID="hdf_perjanjiansewamenyewaunit_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_perjanjiansewamenyewaunit" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Perjanjian Sewa Menyewa Unit</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_perjanjiansewamenyewaunit" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_suratketerangankaryawan_base64" runat="server" />
                                <asp:HiddenField ID="hdf_suratketerangankaryawan_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_suratketerangankaryawan" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Keterangan Karyawan</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_suratketerangankaryawan" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_buktibayarutilitas_base64" runat="server" />
                                <asp:HiddenField ID="hdf_buktibayarutilitas_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_buktibayarutilitas" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Bukti Bayar Utilitas 1 Bln Terakhir</label>
                                 <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_buktibayarutilitas" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_suratkuasa_base64" runat="server" />
                                <asp:HiddenField ID="hdf_suratkuasa_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_suratkuasa" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Surat Kuasa Bermaterai (jika perlu)</label>
                                 <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_suratkuasa" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_ktppemberipenerimakuasa_base64" runat="server" />
                                <asp:HiddenField ID="hdf_ktppemberipenerimakuasa_type" runat="server" />
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_ktppemberipenerimakuasa" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy KTP Pemberi & Penerima Kuasa (jika perlu)</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_ktppemberipenerimakuasa" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>                               
                            </div>
                            <div class="form-group">
                                <asp:HiddenField ID="hdf_other_base64" runat="server" />
                                <asp:HiddenField ID="hdf_other_type" runat="server" />
                                <label class="col-sm-1 control-label" style="padding-top:11px; padding-bottom:20px">Lain-lain</label>
                                <div class="col-sm-5">
                                    <asp:TextBox ID="txt_lainlain" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:HyperLink ID="hpl_other" runat="server" CssClass="form-control" style="border:none; box-shadow:none; color:#6e6ef5"></asp:HyperLink>
                                </div>
                            </div>                           

                            <br />
                            <hr />
                            <%-- check box --%>
                        </div>
                        <%--history--%>
                        <div role="tabpanel" class="tab-pane" id="history">
                            <br />
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvw_smartcardhistory" runat="server" CssClass="table table-hover table-bordered" AutoGenerateColumns="False">
                                        <Columns>
                                            <%-- <asp:BoundField DataField="ModifiedDate" HeaderText="Modified Date" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />--%>
                                            <asp:BoundField DataField="CaseNumber" HeaderText="Case Number" />
                                            <asp:BoundField DataField="TransactionTypeDescription" HeaderText="Transaction Type" />
                                            <asp:BoundField DataField="CardNumber" HeaderText="Card Number" />
                                            <asp:BoundField DataField="ChipNumber" HeaderText="Chip Number" />
                                            <asp:TemplateField HeaderText="Card Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="CardType" runat="server" Text='<%#(Eval("CardTypeID")=="1" ? "Resident" : "Member") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="HolderName" HeaderText="Holder Name" />
                                            <%--<asp:BoundField DataField="BoomGateID" HeaderText="Boomgate ID" />--%>
                                            <asp:BoundField DataField="BoomGateName" HeaderText="Boomgate Name" />                           
                                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" DataFormatString="{0:dd/MM/yyyy HH:mm}" />                                            
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No data
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <br />

                            <br />
                            <hr />
                        </div>
                    </div>



                </div>
            </div>


    <%--preview--%>
     <!-- Trigger the Modal -->
    <img id="myImg" alt="Trolltunga, Norway" style="display:none" width="300" height="200">

        <!-- The Modal -->
        <div id="myModalImage" class="modal">

          <!-- The Close Button -->
          <span class="close" onclick="document.getElementById('myModalImage').style.display='none'" style="font-size:100px">&times;</span>

          <!-- Modal Content (The Image) -->
          <img class="modal-content" id="img01">

          <!-- Modal Caption (Image Text) -->
          <div id="caption"></div>
        </div>

    <!-- The Modal -->
        <div id="myModalFile" class="modal">

          <!-- The Close Button -->
          <span class="close" onclick="document.getElementById('myModalFile').style.display='none'" style="font-size:100px">&times;</span>

          <!-- Modal Content (The Image) -->
          <div class="modal-content" id="file01" style="width:30%">
              <img src="../images/downloadFile.png" />
          </div>

          <!-- Modal Caption (Image Text) -->
          <div id="captionfile" class="caption"></div>
        </div>
    <%--previwe--%>

</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>--%>
