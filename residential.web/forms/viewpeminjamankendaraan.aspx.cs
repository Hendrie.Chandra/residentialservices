﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Globalization;
using System.Web;

namespace residential.web.forms
{
    public partial class viewpeminjamankendaraan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_case(Request.QueryString["CaseNumber"]);
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_Case(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                DataTable dt2 = dbclass.retrieve_CarRent(_casenumber);

                lbl_tariff.Text = Int32.Parse(dt2.Rows[0]["Price"].ToString(), NumberStyles.Currency).ToString("N0");
                lbl_package.Text = Int32.Parse(dt2.Rows[0]["CarRentPackageID"].ToString()).ToString("N0");

                Decimal subtotal = Decimal.Parse(dt2.Rows[0]["Amount"].ToString());
                lbl_subtotal.Text = subtotal.ToString("N0");

                Decimal discount = Decimal.Parse(dt2.Rows[0]["DiscountAmount"].ToString());
                lbl_adjustment.Text = discount.ToString("N0");

                Decimal total = Decimal.Parse(dt2.Rows[0]["AmountAfterDiscount"].ToString());
                lbl_total.Text = total.ToString("N0");

                lblCarLicensePlate.Text = dt2.Rows[0]["CarLicensePlateName"].ToString();
                lblPackage.Text = dt2.Rows[0]["CarRentPackageID"].ToString();
                lblDate.Text = dt2.Rows[0]["Date"].ToString();
                lblPickupTime.Text = dt2.Rows[0]["StartPickupTime"].ToString();
                lblUntil.Text = dt2.Rows[0]["EndPickupTime"].ToString();
                lblPickupLocation.Text = dt2.Rows[0]["PickupLocation"].ToString();
                lblDestination.Text = dt2.Rows[0]["Destination"].ToString();
                lblTariff.Text = dt2.Rows[0]["CarRentTariffName"].ToString();
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }
    }
}