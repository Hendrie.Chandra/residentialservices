﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="viewheader.ascx.cs" Inherits="residential.web.forms.viewheader" %>

<script>
    $(function () {
        $('#viewHeaderTab a:first').tab('show')
    })
</script>
<div class="container navbar-fixed-top alert-custom" role="alert" runat="server" id="alert_danger" visible="false">
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>
</div>
<div class="panel panel-default">
    <div class="panel-heading">Personal Detail</div>
    <div class="panel-body">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-2 control-label">Owner / Tenant</label>
                <div class="col-sm-4">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_customername" runat="server"></asp:Label>
                    </p>
                </div>
                <label class="col-sm-2 control-label">PS Code</label>
                <div class="col-sm-4">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_pscode" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Unit</label>
                <div class="col-sm-4">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_unit" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Requestor Name</label>
                <div class="col-sm-4">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_requestorname" runat="server"></asp:Label>
                    </p>
                </div>
                <label class="col-sm-2 control-label">Case Origin</label>
                <div class="col-sm-4">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_caseorigin" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Requestor Phone</label>
                <div class="col-sm-4">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_requestorphone" runat="server"></asp:Label>
                    </p>
                </div>
                <label class="col-sm-2 control-label">Requestor Email</label>
                <div class="col-sm-4">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_requestoremail" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Description</label>
                <div class="col-sm-10">
                    <p class="form-control-static">
                        <asp:Label ID="lbl_description" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
