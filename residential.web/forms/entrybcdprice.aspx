﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrybcdprice.aspx.cs" Inherits="residential.web.entrybcdprice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container navbar-fixed-top alert-custom" role="alert" runat="server" id="alert_danger" visible="false">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
        </div>
    </div>
    <div class="page-header">
        <h1>Form BCD Price</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="SAVE CLOSE" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" />
    <asp:Button ID="btn_back" runat="server" Text="BACK" CssClass="btn btn-default" OnClick="btn_back_Click" />
    <hr />
    <div class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">Help Name</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txt_helpname" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Price</label>
            <div class="col-sm-10">
                <asp:TextBox ID="txt_price" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>       
</asp:Content>
