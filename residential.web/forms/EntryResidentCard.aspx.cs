﻿using residential.libs;
using residential.libs.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using residential.web.Classes;
using System.IO;


namespace residential.web.forms
{
    public partial class EntryResidentCard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Request.QueryString["RHI"] != null && Request.QueryString["U"] != null)
                {
                    txtOwnerPsCode.Attributes.Add("readonly", "readonly");
                    txtTenantPsCode.Attributes.Add("readonly", "readonly");
                    txtIsActive.Attributes.Add("readonly", "readonly");
                    txtExpiredDate.Attributes.Add("readonly", "readonly");                   

                    lblTitle.Text = Request.QueryString["U"].ToString();
                    hfRHI.Value = Request.QueryString["RHI"].ToString();

                    //show residen data to insert
                    ddl_name.DataSource = clsResident.ResidentData(int.Parse(hfRHI.Value));
                    ddl_name.DataTextField = "Name";
                    ddl_name.DataValueField = "ResidentDetailID";
                    ddl_name.DataBind();

                    ViewState["UserData"] = new UserData(User.Identity.Name);

                    UserData userData = (UserData)ViewState["UserData"];

                    ddlDesignType.DataSource = clsResident.CardType(userData.OrgID, userData.SiteID);
                    ddlDesignType.DataTextField = "DesignTypeDesc";
                    ddlDesignType.DataValueField = "DesignTypeCode";                                        
                    ddlDesignType.DataBind();
                    ddlDesignType.Items.Insert(0, new ListItem("None", "0"));

                    DataSet ds = clsResident.ResidentCard(int.Parse(hfRHI.Value.ToString()));

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtOwnerPsCode.Text = ds.Tables[0].Rows[0]["OwnerName"].ToString();
                        txtTenantPsCode.Text = ds.Tables[0].Rows[0]["TenantName"].ToString();
                        txtIsActive.Text = ds.Tables[0].Rows[0]["isActive"].ToString();
                    }
                    gvw_data.DataSource = null;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        //gvw_data.DataSource = ds.Tables[1];

                        DataTable newdt = new DataTable();

                        newdt.Columns.Add("ResidentCardID");
                        newdt.Columns.Add("ResidentDetailID");
                        newdt.Columns.Add("Name");
                        newdt.Columns.Add("CardNo");
                        newdt.Columns.Add("SNCard");
                        newdt.Columns.Add("Remarks");
                        newdt.Columns.Add("ExpiredDate");
                        newdt.Columns.Add("DesignTypeCode");
                        newdt.Columns.Add("isPrinted");
                        newdt.Columns.Add("DisableExpired");
                        newdt.Columns.Add("isActive");
                        newdt.Columns.Add("PhotoID");
                        newdt.Columns.Add("ModifiedDate");
                        newdt.Columns.Add("ModifiedBy");
                        newdt.Columns.Add("DesignTypeDesc");

                        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                        {
                            //Byte[] img = { 0 };
                            //img = null;
                            //img = ds.Tables[1].Rows[i]["PhotoID"] as Byte[];

                            //string src = "data:image/png;base64," + Convert.ToBase64String(img);
                            string src = ds.Tables[1].Rows[i]["PhotoID"].ToString();

                            DataRow _ravi = newdt.NewRow();
                            _ravi["PhotoID"] = src;
                            _ravi["ResidentCardID"] = ds.Tables[1].Rows[i]["ResidentCardID"];
                            _ravi["ResidentDetailID"] = ds.Tables[1].Rows[i]["ResidentDetailID"];
                            _ravi["Name"] = ds.Tables[1].Rows[i]["Name"];
                            _ravi["CardNo"] = ds.Tables[1].Rows[i]["CardNo"];
                            _ravi["SNCard"] = ds.Tables[1].Rows[i]["SNCard"];
                            _ravi["Remarks"] = ds.Tables[1].Rows[i]["Remarks"];
                            _ravi["ExpiredDate"] = ds.Tables[1].Rows[i]["ExpiredDate"];
                            _ravi["DesignTypeCode"] = ds.Tables[1].Rows[i]["DesignTypeCode"];
                            _ravi["isPrinted"] = ds.Tables[1].Rows[i]["isPrinted"];
                            _ravi["DisableExpired"] = ds.Tables[1].Rows[i]["DisableExpired"];
                            _ravi["isActive"] = ds.Tables[1].Rows[i]["isActive"];
                            _ravi["ModifiedDate"] = ds.Tables[1].Rows[i]["ModifiedDate"];
                            _ravi["ModifiedBy"] = ds.Tables[1].Rows[i]["ModifiedBy"];
                            _ravi["DesignTypeDesc"] = ds.Tables[1].Rows[i]["DesignTypeDesc"];

                            newdt.Rows.Add(_ravi);
                        }

                        gvw_data.DataSource = newdt;
                    }
                    gvw_data.DataBind();

                }
                else
                {
                    Response.Redirect("~/404_pagenotfound.aspx");
                }
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String ResidentCardID = DataBinder.Eval(e.Row.DataItem, "ResidentCardID").ToString();
                string ResidentDetailID = DataBinder.Eval(e.Row.DataItem, "ResidentDetailID").ToString();
                string Name = DataBinder.Eval(e.Row.DataItem, "Name").ToString();
                string CardNo = DataBinder.Eval(e.Row.DataItem, "CardNo").ToString();
                string SNCard = DataBinder.Eval(e.Row.DataItem, "SNCard").ToString();
                string Remarks = DataBinder.Eval(e.Row.DataItem, "Remarks").ToString();
                string ExpiredDate = DataBinder.Eval(e.Row.DataItem, "ExpiredDate").ToString();
                string DesignTypeCode = DataBinder.Eval(e.Row.DataItem, "DesignTypeCode").ToString();
                var PhotoID = DataBinder.Eval(e.Row.DataItem, "PhotoID");
                string isPrinted = DataBinder.Eval(e.Row.DataItem, "isPrinted").ToString();
                string DisableExpired = DataBinder.Eval(e.Row.DataItem, "DisableExpired").ToString();

                //
                HyperLink hplEdit = (HyperLink)e.Row.FindControl("hpl_edit");
                HyperLink print = (HyperLink)e.Row.FindControl("hpl_print");



                if (isPrinted == "True")
                {
                    isPrinted = "1";
                }
                else
                {
                    isPrinted = "0";
                }
                string isActive = DataBinder.Eval(e.Row.DataItem, "isActive").ToString();
                if (isActive == "True")
                {
                    isActive = "1";
                }
                else
                {
                    isActive = "0";
                }
                if (DisableExpired == "0")
                {
                    //e.Row.Attributes.Add("onclick", "Confirm('" + ResidentCardID + "', '" + Name + "', '" + DateTime.Parse(ExpiredDate) + "', '" + CardNo + "', '" + SNCard + "', '" + DesignTypeCode + "', '" + PhotoID + "', '" + isPrinted + "', '" + Remarks + "', '" + isActive + "', '" + DisableExpired + "');");
                    hplEdit.Attributes.Add("onclick", "Confirm('" + ResidentCardID + "', '" + ResidentDetailID + "', '" + Name + "', '" + DateTime.Parse(ExpiredDate).ToString("dd MMMM yyyy") + "', '" + CardNo + "', '" + SNCard + "', '" + DesignTypeCode + "', '" + PhotoID + "', '" + isPrinted + "', '" + Remarks + "', '" + isActive + "', '" + DisableExpired + "');");
                  
                }
                else
                {
                    //e.Row.Attributes.Add("onclick", "Confirm('" + ResidentCardID + "', '" + Name + "', '" + "" + "', '" + CardNo + "', '" + SNCard + "', '" + DesignTypeCode + "', '" + PhotoID + "', '" + isPrinted + "', '" + Remarks + "', '" + isActive + "', '" + DisableExpired + "');");
                    hplEdit.Attributes.Add("onclick", "Confirm('" + ResidentCardID + "', '" + ResidentDetailID + "', '" + Name + "', '" + "" + "', '" + CardNo + "', '" + SNCard + "', '" + DesignTypeCode + "', '" + PhotoID + "', '" + isPrinted + "', '" + Remarks + "', '" + isActive + "', '" + DisableExpired + "');");                
                }
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                string txtName = ddl_name.SelectedItem.ToString();
                int residentDetailID = int.Parse(ddl_name.SelectedItem.Value);

                if (txtName.Length > 0 && txtCardNo.Text.Length > 0 && txtSNcard.Text.Length > 0)
                {
                    //byte[] ImageContent = null;
                    string ImageContent = "";
                    if (fu1.HasFile)
                    {
                        if (fu1.PostedFile.ContentType == "image/jpeg" || fu1.PostedFile.ContentType == "image/png")
                        {
                            if (fu1.PostedFile.ContentLength <= 512000)
                            {
                                ImageContent = hdfBase64Photo.Value;
                                //int intImageSize;
                                //string strImageType = null;
                                //Stream ImageStream = default(Stream);

                                //// Gets the Size of the Image
                                //intImageSize = fu1.PostedFile.ContentLength;

                                //// Gets the Image Type
                                //strImageType = fu1.PostedFile.ContentType;

                                //// Reads the Image
                                //ImageStream = fu1.PostedFile.InputStream;

                                //ImageContent = new byte[intImageSize + 1];
                                //int intStatus = 0;

                                //intStatus = ImageStream.Read(ImageContent, 0, intImageSize);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('Your image size must be less than 500Kb');", true);
                                return;
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('Only JPEG or PNG files are accepted!');", true);
                            return;
                        }
                    }
                    else
                    {
                        ImageContent = "";
                    }

                    string Result = "";
                    int NoExpired = 0;

                    if (ckDisable.Checked == true)
                    {
                        NoExpired = 1;
                        txtExpiredDate.Text = "01/01/1900";//"1900-01-01";
                    }

                    UserData userData = (UserData)ViewState["UserData"];

                    if (hfResidentCardID.Value.ToString() == "")
                    {
                        Result = clsResident.InsertResidentCard(int.Parse(hfRHI.Value.ToString()), residentDetailID, txtName, txtRemarks.Text.ToString(),
                            txtCardNo.Text.ToString(), txtSNcard.Text.ToString(), DateTime.Parse(txtExpiredDate.Text), int.Parse(ddlActive.SelectedValue.ToString()),
                            userData.UserName, NoExpired, ddlDesignType.SelectedValue.ToString(), ImageContent, int.Parse(ddlisPrinted.SelectedValue.ToString()));
                    }
                    else
                    {
                        ImageContent = hdfBase64Photo.Value;

                        Result = clsResident.UpdateResidentCard(int.Parse(hfRHI.Value.ToString()), txtName, txtRemarks.Text.ToString(),
                           txtCardNo.Text.ToString(), txtSNcard.Text.ToString(), DateTime.Parse(txtExpiredDate.Text), int.Parse(ddlActive.SelectedValue.ToString()),
                           userData.UserName, NoExpired, ddlDesignType.SelectedValue.ToString(), ImageContent, int.Parse(ddlisPrinted.SelectedValue.ToString()), int.Parse(hfResidentCardID.Value.ToString()));
                    }

                    if (Result == "1")
                    {
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "page redirect", "alert('Resident Card Saved'); window.location='" + Request.Url.AbsoluteUri + "';", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(1,'Resident Card Saved !');", true);
                    }
                    else
                    {
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('" + Result.ToString() + "');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(0,'" + Result.ToString() + "');", true);
                    }
                }
                else
                {
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('Please Fill All Required Field *');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(0,'Please Fill All Required Field *');", true);
                }
            }
            catch (Exception ex)
            {
                errID.Visible = true;
                lbl_error.Text = ex.Message.ToString();
            }
        }
    }
}