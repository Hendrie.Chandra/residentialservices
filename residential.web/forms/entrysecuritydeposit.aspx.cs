﻿using residential.libs;
using residential.libs.Models;
using residential.web.Classes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web
{
    public partial class entrysecuritydeposit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return;
            }

            String res = save_securityDeposit();
            if (!string.IsNullOrEmpty(res))
            {
                entryheader.ShowAlert = res;
            }
            else
                Response.Redirect("listsecuritydeposit.aspx");
        }

        private string save_securityDeposit()
        {           
            UserData userData = (UserData)ViewState["UserData"];

            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            try
            {
            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["SecurityDepositHelpID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;
            DateTime RequestDate = DateTime.Now;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.FullName = FullName;
            request.UserName = SavedBy;

            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                /*request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();*/
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;
                DateTime? OverdueDate = dbcase.OverdueDate;

                String returnvalue = dbclass.save_SecurityDeposit_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 2, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }


                    /*UpdateCaseHelpDeskStatusRequest upd = new UpdateCaseHelpDeskStatusRequest();
                    upd.CRMCaseNumber = CaseNumber;
                    upd.CRMCaseStatusID = 2;
                    upd.Remarks = "";
                    upd.UpdateBy = userData.FullName;
                    upd.UpdateDate = DateTime.Now;
                    upd.WorkedBy = string.Empty;
                    upd.SatisfactionID = 1;
                    UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(upd);*/

                    //dbclass.save_tickethistory(CaseNumber, 2, FullName, null, SavedBy, null);

                }
                else
                    return dbcase.ExceptionMessage;
                }
                catch (Exception e)
                {
                    entryheader.ShowAlert = e.Message;                    
                dbclass.Save_ErrorLog(e, userData.UserName);
                }

            return null;
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }
    }
}
