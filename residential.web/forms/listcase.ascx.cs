﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Configuration;
using System.Data;

namespace residential.web.forms
{
    public partial class listcase : System.Web.UI.UserControl
    {
        public DataTable datacontent;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["sortOrder"] = "";
                //show_cbo_casestatus();
                //bounddatagrid("", "");                
            }
            else
            {
                datacontent = (DataTable)ViewState["GridData"];                
            }
            ViewState["GridData"] = datacontent;            
        }
        
        public void bounddatagrid(String sortExp, String sortDir, DataTable dt)
        {            
            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }
       
        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {
                    Label lbl_no = (Label)e.Row.FindControl("lbl_no");
                    Int32 no_urut = e.Row.RowIndex + 1;
                    no_urut = gvw_data.PageIndex * gvw_data.PageSize + no_urut;
                    lbl_no.Text = no_urut.ToString();
                }

                String CaseNumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();
                String CaseStatus = DataBinder.Eval(e.Row.DataItem, "CaseStatusName").ToString();
                Int32 CaseStatusID = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "CaseStatusID").ToString());
                String linkToForm = "viewbcd.aspx";
                if (CaseStatus.ToUpper() == "PENDING")
                    linkToForm = "entrybcd.aspx";

                LinkButton lnk_case = (LinkButton)e.Row.FindControl("lnk_case");
                lnk_case.CommandArgument = CaseNumber;
                lnk_case.Text = CaseNumber;
                String popup_url = "openWindowModal('" + linkToForm + "?flag=1&CaseNumber=" + CaseNumber + "', 700, 700)";
                lnk_case.Attributes.Add("onclick", popup_url);
                
                if (!String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString()) && (CaseStatusID == 2 || CaseStatusID ==4))
                {
                    DateTime OverdueDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString());
                    if (DateTime.Compare(DateTime.Now, OverdueDate) >= 0)
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                }
            }
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_edit")
            {
                bounddatagrid("", "", datacontent);
            }
        }

        protected void btn_add_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "", datacontent);
        }
        
        protected void cbo_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "", datacontent);
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder, datacontent);
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "", datacontent);
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "", datacontent);
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "", datacontent);
        }
    }
}