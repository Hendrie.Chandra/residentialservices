﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using residential.libs;
using System.Configuration;
using System.Data;

namespace residential.web.forms
{
    public partial class dasboard : System.Web.UI.Page
    {
        String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_cbo_deptlist();
                BindTasklist();
                loadchart1();
                loadchart2();
                loadchart3();
                loadchart4();
            }
            //HtmlMeta refresh = new HtmlMeta();
            //refresh.HttpEquiv = 'refresh';
            //refresh.Content = Convert.ToString(Session.Timeout) + "url=Welcome.aspx";
            //this.Page.Header.Controls.Add(refresh);
            Response.AppendHeader("refresh", "60");
        }

        private void BindTasklist()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            string DeptName = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            if (cbo_deptlist.SelectedItem.Text != "[ All Dept ]")
            {
                DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
            }

            UserData userData = (UserData)ViewState["UserData"];
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.allcase(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName);
            //string DeptName = string.Format(cbo_deptlist.SelectedValue);

            this.gvw_allcase.DataSource = dt;
            this.gvw_allcase.DataBind();
        }

        protected void picker_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text) && !string.IsNullOrEmpty(cbo_deptlist.Text))
                {
                    ViewState["UserData"] = new UserData(User.Identity.Name);
                    DateTime StartDate = Convert.ToDateTime(TxtStartDate.Text);
                    DateTime EndDate = Convert.ToDateTime(TxtEndDate.Text);
                    string DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
                }

                DateTime SD = Convert.ToDateTime(TxtStartDate.Text);
                if (SD > DateTime.Now)
                {
                    Response.Write("<script>alert('Created Date FROM cannot  be greater than CURRENT date')</script>");
                }

                DateTime ED = Convert.ToDateTime(TxtEndDate.Text);
                if (ED > DateTime.Now)
                {
                    Response.Write("<script>alert('Created Date TO cannot  be greater than CURRENT date')</script>");
                }

                if (ED < SD)
                {
                    Response.Write("<script>alert('Created Date TO cannot  be less than Created Date FROM')</script>");
                }

                UserData userData = (UserData)ViewState["UserData"];

                //show_cbo_deptlist(); //disabled on change
                BindTasklist();
                loadchart1();
                loadchart2();
                loadchart3();
                loadchart4();
                //show_cbo_casestatus();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }

        }

        private void show_cbo_deptlist()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_deptlist();
            cbo_deptlist.DataSource = dt;
            cbo_deptlist.DataTextField = "DepartmentName";
            cbo_deptlist.DataValueField = "DepartmentName";
            cbo_deptlist.DataBind();
            cbo_deptlist.Items.Insert(0, new ListItem("[ All Dept ]", "[ All Dept ]"));
            cbo_deptlist.SelectedIndex = 0;
        }

        //GridView
        protected void gvw_allcase_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String CaseStatusName = DataBinder.Eval(e.Row.DataItem, "CaseStatusName").ToString();
                Int32 CSID = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "CSID").ToString());
            }
        }

        //protected void cbo_deptlist_TextChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text) && !string.IsNullOrEmpty(cbo_deptlist.Text))
        //    {
        //        DateTime StartDate = Convert.ToDateTime(TxtStartDate.Text);
        //        DateTime EndDate = Convert.ToDateTime(TxtEndDate.Text);
        //        string DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);

        //        BindTasklist();
        //        loadchart1();
        //        loadchart2();
        //        loadchart3();
        //    }
        //}

        //Chart1
        protected void loadchart1()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            string DeptName = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            if (cbo_deptlist.SelectedItem.Text != "[ All Dept ]")
            {
                DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
            }
            UserData userData = (UserData)ViewState["UserData"];

            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.allcase(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName);

            string[] chartdata = new string[2];
            string tempchartdata = "";
            string tempchartlabel = "";
            if (dt.Rows.Count > 0)
            {
                int chartdatalength = dt.Rows.Count;
                int i = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (i <= (chartdatalength - 1))
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CaseStatusName"].ToString() + '"' + ",";
                        tempchartdata = tempchartdata + dr["CSID"].ToString() + ",";
                    }
                    else
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CaseStatusName"].ToString() + '"';
                        tempchartdata = tempchartdata + dr["CSID"].ToString();
                    }
                    i++;
                }
            }
            chartdata[0] = "[" + tempchartdata + "]";
            chartdata[1] = "[" + tempchartlabel + "]";

            chart1data.Text = tempchartdata;
            chart1label.Text = tempchartlabel;
        }


        //Chart2
        protected void loadchart2()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            string DeptName = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            if (cbo_deptlist.SelectedItem.Text != "[ All Dept ]")
            {
                DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
            }
            UserData userData = (UserData)ViewState["UserData"];

            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.duecase(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName);

            string[] chartdata = new string[2];
            string tempchartdata = "";
            string tempchartlabel = "";
            if (dt.Rows.Count > 0)
            {
                int chartdatalength = dt.Rows.Count;
                int i = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (i <= (chartdatalength - 1))
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CaseStatusName"].ToString() + '"' + ",";
                        tempchartdata = tempchartdata + dr["Due_Qty"].ToString() + ",";
                    }
                    else
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CaseStatusName"].ToString() + '"';
                        tempchartdata = tempchartdata + dr["Due_Qty"].ToString();
                    }
                    i++;
                }
            }
            chartdata[0] = "[" + tempchartdata + "]";
            chartdata[1] = "[" + tempchartlabel + "]";

            chart2data.Text = tempchartdata;
            chart2label.Text = tempchartlabel;
        }

        //Chart3
        protected void loadchart3()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            string DeptName = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            if (cbo_deptlist.SelectedItem.Text != "[ All Dept ]")
            {
                DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
            }
            UserData userData = (UserData)ViewState["UserData"];

            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.overduecase(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName);

            string[] chartdata = new string[2];
            string tempchartdata = "";
            string tempchartlabel = "";
            if (dt.Rows.Count > 0)
            {
                int chartdatalength = dt.Rows.Count;
                int i = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (i <= (chartdatalength - 1))
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CaseStatusName"].ToString() + '"' + ",";
                        tempchartdata = tempchartdata + dr["OVD_Qty"].ToString() + ",";
                    }
                    else
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CaseStatusName"].ToString() + '"';
                        tempchartdata = tempchartdata + dr["OVD_Qty"].ToString();
                    }
                    i++;
                }
            }
            chartdata[0] = "[" + tempchartdata + "]";
            chartdata[1] = "[" + tempchartlabel + "]";

            chart3data.Text = tempchartdata;
            chart3label.Text = tempchartlabel;
        }

        //Chart4
        protected void loadchart4()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            UserData userData = (UserData)ViewState["UserData"];


            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.closecase(userData.OrgID, userData.SiteID, StartDate, EndDate);

            string[] chartdata = new string[2];
            string tempchartdata = "";
            string tempchartlabel = "";
            if (dt.Rows.Count > 0)
            {
                int chartdatalength = dt.Rows.Count;
                int i = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (i <= (chartdatalength - 1))
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["DeptCode"].ToString() + '"' + ",";
                        tempchartdata = tempchartdata + dr["Qty"].ToString() + ",";
                    }
                    else
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["DeptCode"].ToString() + '"';
                        tempchartdata = tempchartdata + dr["Qty"].ToString();
                    }
                    i++;
                }
            }
            chartdata[0] = "[" + tempchartdata + "]";
            chartdata[1] = "[" + tempchartlabel + "]";

            chart4data.Text = tempchartdata;
            chart4label.Text = tempchartlabel;
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text) && !string.IsNullOrEmpty(cbo_deptlist.Text) && !string.IsNullOrEmpty(cbo_status.Text))
        //    {
        //        ViewState["UserData"] = new UserData(User.Identity.Name);
        //        DateTime StartDate = Convert.ToDateTime(TxtStartDate.Text);
        //        DateTime EndDate = Convert.ToDateTime(TxtEndDate.Text);
        //        string DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
        //        Int32 CSID = Int32.Parse(cbo_status.SelectedValue);
        //    }
        //    bounddatagrid();
        //}

        //protected void Button2_Click(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text) && !string.IsNullOrEmpty(cbo_deptlist.Text) && !string.IsNullOrEmpty(cbo_status.Text))
        //    {
        //        ViewState["UserData"] = new UserData(User.Identity.Name);
        //    }
        //    //New Tab
        //    string strJavaScript = "<script>window.open('ChartDetail.aspx?StartDate=" + Server.UrlEncode(TxtStartDate.Text) + "&EndDate=" + Server.UrlEncode(TxtEndDate.Text)
        //        + "&DeptName=" + Server.UrlEncode(cbo_deptlist.SelectedItem.Text) + "&CSID=" + Server.UrlEncode(cbo_status.SelectedValue) + "');</script>";
        //    Response.Write(strJavaScript);
        //    //Current Tab
        //    //Response.Redirect("ChartDetail.aspx?StartDate=" + Server.UrlEncode (TxtStartDate.Text) + "&EndDate=" + Server.UrlEncode (TxtEndDate.Text) 
        //    //    + "&DeptName=" + Server.UrlEncode (cbo_deptlist.SelectedItem.Text) + "&CSID=" + Server.UrlEncode (cbo_status.SelectedValue));
        //}

        //private void show_cbo_casestatus()
        //{
        //    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
        //    DBclass dbclass = new DBclass(constring);

        //    DataTable dt = dbclass.retrieve_casestatus();
        //    cbo_status.DataSource = dt;
        //    cbo_status.DataTextField = "CaseStatusName";
        //    cbo_status.DataValueField = "CaseStatusID";
        //    cbo_status.DataBind();
        //    cbo_status.Items.Insert(0, new ListItem("[ All Status ]", "0"));
        //    cbo_status.SelectedIndex = 0;
        //}

        //private void bounddatagrid()
        //{
        //    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
        //    DBclass dbclass = new DBclass(constring);

        //    UserData userData = (UserData)ViewState["UserData"];

        //    DateTime? StartDate = null;
        //    DateTime? EndDate = null;
        //    string DeptName = null;
        //    Int32 CSID = Int32.Parse(cbo_status.SelectedValue);

        //    if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
        //    {
        //        StartDate = Convert.ToDateTime(TxtStartDate.Text);
        //        EndDate = Convert.ToDateTime(TxtEndDate.Text);
        //        CSID = Int32.Parse(cbo_status.SelectedValue);
        //    }
        //    if (cbo_deptlist.SelectedItem.Text != "[ All Dept ]")
        //    {
        //        DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
        //    }

        //    DataSet dt = dbclass.chartdetail(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName, CSID);

        //    DataView dv = new DataView(dt.Tables[0]);

        //    gvw_data.DataSource = null;
        //    gvw_data.DataSource = dv;
        //    gvw_data.DataBind();
        //}

        //protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {

        //    }
        //}

        //protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    gvw_data.PageIndex = e.NewPageIndex;
        //    bounddatagrid();
        //}
    }
}



