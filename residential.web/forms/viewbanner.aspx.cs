﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using System.Globalization;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Web;


namespace residential.web.forms
{
    public partial class viewbanner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_case(Request.QueryString["CaseNumber"]);

                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                string deptID = Request.QueryString["DeptID"];

                if (deptID == null)
                {
                    //updatecase1.Visible = false;
                    /*tampilkan view untuk update cencel di cs*/
                    updatecase1.Visible = true;
                    updatecase1.DepartmentID = "";
                }
                else
                {
                    updatecase1.Visible = true;
                    updatecase1.DepartmentID = Request.QueryString["DeptID"].ToString();
                }
            }
        }
        
        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_banner_by_casenumber(_casenumber);

            if (dt.Rows.Count > 0)
            {
                ViewState["CRMCaseID"] = dt.Rows[0]["CRMCaseID"].ToString();
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                if ((userData.RoleID == 2) && Int32.Parse(dt.Rows[0]["CaseStatusID"].ToString()) == 1)
                    adjustmentForm.Visible = true;

                DataTable dt3 = dbclass.retrieve_banner_detail(_casenumber);

                List<BannerDetail> list = new List<BannerDetail>();
                BannerDetail bd = new BannerDetail();

                int i = 1;
                foreach (DataRow dr in dt3.Rows)
                {
                    bd = new BannerDetail();
                    bd.BannerDetailID = Int32.Parse(dr["BannerDetailID"].ToString());
                    bd.LocationID = Int32.Parse(dr["BannerLocationID"].ToString());
                    bd.LocationName = dr["BannerLocationName"].ToString();
                    bd.PointID = Int32.Parse(dr["PointID"].ToString());
                    bd.PointName = dr["PointName"].ToString();
                    bd.BannerTypeID = Int32.Parse(dr["BannerTypeID"].ToString());
                    bd.BannerTypeName = dr["BannerTypeName"].ToString();
                    bd.StartDate = DateTime.Parse(dr["StartDate"].ToString());
                    bd.ToDate = DateTime.Parse(dr["EndDate"].ToString());
                    bd.Price = Convert.ToInt32(decimal.Parse(dr["Price"].ToString()));

                    list.Add(bd);
                    i++;
                }

                lbl_sewatempat.Text = Int32.Parse(dt.Rows[0]["Amount"].ToString(), NumberStyles.Currency).ToString("N0");
                lbl_biayapasang.Text = Int32.Parse(dt.Rows[0]["InstallationFee"].ToString(), NumberStyles.Currency).ToString("N0"); ;
                lbl_ppn.Text = Int32.Parse(dt.Rows[0]["TaxAmount"].ToString(), NumberStyles.Currency).ToString("N0");
                lbl_subtotal.Text = Int32.Parse(dt.Rows[0]["AmountAfterTax"].ToString(), NumberStyles.Currency).ToString("N0");

                String Role = userData.RoleName;
                if ((Role == "PIC Head") && Int32.Parse(dt.Rows[0]["CaseStatusID"].ToString()) == 1)
                {
                    lbl_adjustment.Visible = false;
                    txt_adjustment.Visible = true;
                }
                else
                {
                    txt_adjustment.Visible = false;
                    lbl_adjustment.Visible = true;
                }

                txt_adjustment.Text = Int32.Parse(dt.Rows[0]["DiscountAmount"].ToString(), NumberStyles.Currency).ToString("N0");
                lbl_adjustment.Text = Int32.Parse(dt.Rows[0]["DiscountAmount"].ToString(), NumberStyles.Currency).ToString("N0");
                lbl_total.Text = Int32.Parse(dt.Rows[0]["ActualAmount"].ToString(), NumberStyles.Currency).ToString("N0");

                gvw_data.DataSource = list;
                gvw_data.DataBind();
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }                                
       
        protected void txt_adjustment_TextChanged(object sender, EventArgs e)
        {
            Int32 SubTotal = Int32.Parse(lbl_subtotal.Text, NumberStyles.Currency);

            Int32 Adjustment = 0;
            if (String.IsNullOrWhiteSpace(txt_adjustment.Text))
                Adjustment = 0;
            else
                Adjustment = Int32.Parse(txt_adjustment.Text);

            lbl_adjustment.Text = Adjustment.ToString("N0");
            Int32 Total = SubTotal - Adjustment;
            lbl_total.Text = Total.ToString("N0");
        }

        protected void btn_updateAdjustment_Click(object sender, EventArgs e)
        {            
            if (!String.IsNullOrEmpty(txt_adjustment.Text) && int.Parse(txt_adjustment.Text) > 0)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];
                dbclass.Update_Banner_Adjustment(Request.QueryString["CaseNumber"].ToString(), decimal.Parse(txt_adjustment.Text));
            }            
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            show_case(Request.QueryString["CaseNumber"]);
            //BindTasklist();
        }
    }
}
