﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class viewsecuritydeposit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["CaseNumber"] != null)
                {
                    bind_SecurityDeposit(Request.QueryString["CaseNumber"].ToString());
                }

                string a = Request.QueryString["DeptID"];

                if (a == null)
                {
                    //updatecase.Visible = false;
                    /*tampilkan view untuk update cencel di cs*/
                    updatecase.Visible = true;
                    updatecase.DepartmentID = "";
                }
                else
                {
                    updatecase.Visible = true;
                    updatecase.DepartmentID = Request.QueryString["DeptID"].ToString();
                }
            }
        }

        private void bind_SecurityDeposit(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_SecurityDeposit(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                DataTable dtSC = dbclass.retrieve_SecurityDeposit(_casenumber);
                txt_relatedCaseNumber.Text = dtSC.Rows.Count > 0 ? dtSC.Rows[0]["RelatedCaseNumber"].ToString() : null;
                //add by bili
                lbl_relatedcasedescription.Text = dtSC.Rows.Count > 0 ? dtSC.Rows[0]["RelatedCaseDesc"].ToString() : null;

                bind_data("", "");
            }
            else
            {
                Response.Redirect("~/404_pagenotfound.aspx");
            }
        }

        protected void bind_data(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_CaseByCustomer(Request.QueryString["CaseNumber"].ToString(), viewheader.PSCode,
                viewheader.Unit.Split('-')[0].Replace(" ", string.Empty), viewheader.Unit.Split('-')[1].Replace(" ", string.Empty));

            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bind_data("", "");
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bind_data(e.SortExpression, sortOrder);
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"] == null)
                    ViewState["sortOrder"] = "asc";

                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "SelectRelatedCase")
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                
                ScriptManager.RegisterStartupScript(this, GetType(), "fillAndClose", "fillAndCloseRelatedCase('" + e.CommandArgument.ToString() + "');", true);
                dbclass.update_relatedCaseNumberSecurityDeposit(Request.QueryString["CaseNumber"].ToString(), e.CommandArgument.ToString());

                DataTable dtSC = dbclass.retrieve_SecurityDeposit(Request.QueryString["CaseNumber"].ToString());
                lbl_relatedcasedescription.Text = dtSC.Rows.Count > 0 ? dtSC.Rows[0]["RelatedCaseDesc"].ToString() : null;
                ScriptManager.RegisterStartupScript(this, GetType(), "fillAndRelatedCaseDesc", "fillAndRelatedCaseDesc('" + lbl_relatedcasedescription.Text + "');", true);
            }
        }

        protected void txt_relatedCaseNumber_TextChanged(object sender, EventArgs e)
        {
            //String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            //DBclass dbclass = new DBclass(constring);

            ////add by bili
            //string relatedcasedesc = "";
            //DataTable dtSC = dbclass.retrieve_SecurityDeposit(Request.QueryString["CaseNumber"].ToString());
            //if (dtSC.Rows.Count > 0)
            //{
            //    relatedcasedesc = dtSC.Rows[0]["RelatedCaseDesc"].ToString();
            //}
            
        }
    }
}