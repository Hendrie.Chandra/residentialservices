﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class entryhelp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                combobox_departments();

                if (Request.QueryString["stat"] == "edit")
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    Int32 HelpID = Int32.Parse(Request.QueryString["HelpID"].ToString());
                    DataTable dt = dbclass.retrieve_help(HelpID);

                    DataRow datarow = dt.Rows[0];
                    txt_helpname.Text = datarow["HelpName"].ToString();
                    txt_overdue.Text = datarow["OverdueDays"].ToString();                    
                    cbo_departments.Text = datarow["DepartmentID"].ToString();
                    cbo_team.Text = datarow["TeamID"].ToString();
                }     
            }
        }

        private void combobox_departments()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_department();

            cbo_departments.DataSource = dt;
            cbo_departments.DataTextField = "DepartmentName";
            cbo_departments.DataValueField = "DepartmentID";
            cbo_departments.DataBind();
            cbo_departments.SelectedIndex = 0;
        }

        private Int32 save_help()
        {            
            String SavedBy = Session["username"].ToString();

            //String flag = Request.QueryString["flag"].ToString();
            Int32 HelpID = Int32.Parse(Request.QueryString["HelpID"].ToString());
            String stat = Request.QueryString["stat"].ToString();
            Int32 statID = 0;            

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            if (stat == "edit")
            {
                statID = HelpID;
            }

            Int32 TeamID = 1;//Int32.Parse(cbo_team.SelectedValue);
            Int32 DepartmentID = Int32.Parse(cbo_departments.SelectedValue);
            short TreeLevel = short.Parse(Request.QueryString["NodeDepth"].ToString());
            TreeLevel += 1;
            Int32 ParentHelpID = Int32.Parse(Request.QueryString["HelpID"].ToString());
            
            Int32 returnvalue = dbclass.save_help(statID, txt_helpname.Text, ParentHelpID, DepartmentID, TreeLevel, short.Parse(txt_overdue.Text), TeamID, SavedBy);

            return returnvalue;
        }

        protected Boolean help_validation()
        {
            RegularExpressionValidator1.Validate();
            if (String.IsNullOrEmpty(txt_helpname.Text))
            {
                alert_danger_text.Text = "Help name is Required!";
                alert_danger.Visible = true;
                return false;
            }
            else if (!RegularExpressionValidator1.IsValid)
                return false;
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (help_validation())
            {
                alert_danger_text.Visible = false;
                Int32 returnvalue = save_help();
            }                
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (help_validation())
            {
                alert_danger_text.Visible = false;
                Int32 returnvalue = save_help();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            }      
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            if (help_validation())
            {
                alert_danger_text.Visible = false;
                Int32 returnvalue = save_help();
                txt_helpname.Text = String.Empty;
                txt_overdue.Text = String.Empty;
                cbo_departments.SelectedIndex = 0;
                cbo_team.SelectedIndex = 0;
            }     
        }

        protected void txt_jumlah_TextChanged(object sender, EventArgs e)
        {
            RegularExpressionValidator1.Validate();
        }
    }
}