﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class entrybannerlocation : System.Web.UI.Page
    {
        static String prevPage = "listbannerlocation.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_cbo_bannerType();
                gvw_data.DataBind();
                
                if (Request.QueryString["BannerLocationID"] != null)
                {
                    Int32 BannerLocationID = Int32.Parse(Request.QueryString["BannerLocationID"].ToString());
                    show_data(BannerLocationID);
                    div_points.Visible = true;
                }
            }
            alert_danger.Visible = false;
        }

        protected void show_data(Int32 _BannerLocationID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            
            DataTable dt = dbclass.retrieve_banner_location(_BannerLocationID);

            DataRow datarow = dt.Rows[0];                   
            txt_codebanloc.Text = datarow["BannerLocationCode"].ToString();
            txt_bannerlocactionname.Text = datarow["BannerLocationName"].ToString();
            cbo_bannerType.SelectedValue = datarow["BannerTypeID"].ToString();
            txt_price.Text = datarow["Price"].ToString();

            show_Banner_Points();
        }

        protected void show_Banner_Points()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt_Points = dbclass.retrieve_point(Int32.Parse(Request.QueryString["BannerLocationID"].ToString()));
            gvw_data.DataSource = dt_Points;
            gvw_data.DataBind();
        }

        protected void show_cbo_bannerType()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_banner_type();
            cbo_bannerType.DataSource = dt;
            cbo_bannerType.DataTextField = "BannerTypeName";
            cbo_bannerType.DataValueField = "BannerTypeID";
            cbo_bannerType.DataBind();
            cbo_bannerType.SelectedIndex = 0;
        }

        private Int32 save_banner_location()
        {                                  
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            Int32 BannerTypeID = Int32.Parse(cbo_bannerType.SelectedValue);
            Int32 returnvalue = 0;
            Int32 BannerLocationID = 0;
            if (Request.QueryString["BannerLocationID"] != null)
                BannerLocationID = Int32.Parse(Request.QueryString["BannerLocationID"].ToString());
            Int32 Price = Int32.Parse(txt_price.Text);

            return returnvalue = dbclass.save_banner_location(BannerLocationID, txt_bannerlocactionname.Text, txt_codebanloc.Text, BannerTypeID, Price, userData.UserName, userData.SiteID, userData.OrgID);                        
        }

        protected Boolean validation()
        {
            if (String.IsNullOrEmpty(txt_bannerlocactionname.Text))
            {
                alert_danger_text.Text = "Banner Location Name is required!";
                alert_danger.Visible = true;
                return false;
            }
            if (String.IsNullOrEmpty(txt_codebanloc.Text))
            {
                alert_danger_text.Text = "Banner Location Code is required!";
                alert_danger.Visible = true;
                return false;
            }
            if (String.IsNullOrEmpty(txt_price.Text))
            {
                alert_danger_text.Text = "Price is required!";
                alert_danger.Visible = true;
                return false; 
            }
            return true;
        }        

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (validation())
            {
                Int32 returnvalue = save_banner_location();                
                Response.Redirect(prevPage);
            }
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
     
        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            int count = 0;
            foreach (GridViewRow gvRow in gvw_data.Rows)
            {
                CheckBox chkSel = (CheckBox)gvRow.FindControl("chk_select");
                int index = Convert.ToInt32(gvRow.RowIndex);
                if (chkSel.Checked == true)
                {
                    Int32 PointID = Int32.Parse(gvw_data.DataKeys[index].Value.ToString());
                    dbclass.Delete_BannerPoint(PointID, userData.UserName);
                    count++;
                }
            }

            show_Banner_Points();                               
        }        

        protected void btn_add_Click(object sender, EventArgs e)
        {
            txt_point.Text = String.Empty;
        }
       
        protected void btn_savePoint_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txt_point.Text))
            {
                UserData userData = new UserData(User.Identity.Name);
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                int BannerLocationID = int.Parse(Request.QueryString["BannerLocationID"].ToString());
                Int32 returnvalue = dbclass.save_point(txt_point.Text, BannerLocationID, userData.UserName);


            }            
        }       
    }
}