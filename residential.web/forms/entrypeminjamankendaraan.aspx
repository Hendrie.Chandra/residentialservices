﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrypeminjamankendaraan.aspx.cs" Inherits="residential.web.forms.entrypeminjamankendaraan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form Peminjaman Kendaraan</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <%--<div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>--%>
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Car Rent Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Car License Plate</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboCarLicensePlate" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="cboCarLicensePlate_SelectedIndexChanged"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Date</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:CalendarExtender ID="txt_date_CalendarExtender" runat="server" TargetControlID="txtDate"
                                    Format="dd MMMM yyyy">
                                </asp:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Booked List</label>
                            <div class="col-sm-10">
                                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-hover table-bordered" AllowPaging="True" ShowHeaderWhenEmpty="True">
                                    <Columns>
                                        <asp:BoundField HeaderText="PickupTime" DataField="PickupTime"
                                            SortExpression="StartPickupTime">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Pickup Location" DataField="PickupLocation"
                                            SortExpression="PickupLocation">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Destination" DataField="Destination"
                                            SortExpression="EndPickupTime">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Tariff" DataField="CarRentTariff"
                                            SortExpression="CarRentTariff">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>         
                        <hr />               
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pickup Time</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtPickTime" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:MaskedEditExtender ID="mee1" runat="server" Mask="99:99" MaskType="Time" TargetControlID="txtPickTime"
                                    AcceptAMPM="false" OnInvalidCssClass="MaskedEditError">
                                </asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="mev1" runat="server" ControlExtender="mee1" ControlToValidate="txtPickTime"
                                    IsValidEmpty="true"></asp:MaskedEditValidator>
                            </div>
                            <label class="col-sm-2 control-label">Until</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtUntil" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:MaskedEditExtender ID="mee2" runat="server" Mask="99:99" MaskType="Time" TargetControlID="txtUntil"
                                    AcceptAMPM="false" OnInvalidCssClass="MaskedEditError">
                                </asp:MaskedEditExtender>
                                <asp:MaskedEditValidator ID="mev2" runat="server" ControlExtender="mee2" ControlToValidate="txtUntil"
                                    IsValidEmpty="true"></asp:MaskedEditValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Pickup Location</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtPickupLocation" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <label class="col-sm-2 control-label">Destination</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtDestination" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tariff</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboTariff" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="cboTariff_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Package</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_jumlah" runat="server" CssClass="form-control" Type="Number"
                                    MaxLength="5" AutoPostBack="True" OnTextChanged="txt_jumlah_TextChanged" Text="1"></asp:TextBox>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-6"></div>
                            <label class="col-sm-2 control-label">Adjustment</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_discount" runat="server" CssClass="form-control" Type="Number"
                                    AutoPostBack="True" OnTextChanged="txt_discount_TextChanged">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Tariff</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_tariff" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Package</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_package" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
