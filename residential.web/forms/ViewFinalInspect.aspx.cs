﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk.Query;
using residential.libs.Models;
using System.IO;
namespace residential.web.forms
{
    public partial class ViewFinalInspect : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_case(Request.QueryString["CaseNumber"]);
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                string a = Request.QueryString["DeptID"];

                if (a == null)
                {
                    //updatecase.Visible = false;
                    /*tampilkan view untuk update cencel di cs*/
                    updatecase.Visible = true;
                    updatecase.DepartmentID = "";
                }
                else
                {
                    updatecase.Visible = true;
                    updatecase.DepartmentID = Request.QueryString["DeptID"].ToString();
                    updatecase.IsFinalInspect = "1";
                }
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataSet dt = residential.web.Classes.clsFinalInspect.ViewFinalInspectDetail(userData.OrgID, userData.SiteID, _casenumber);

            if (dt.Tables[0].Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Tables[0].Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Tables[0].Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Tables[0].Rows[0]["UnitCode"].ToString() + " - " + dt.Tables[0].Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Tables[0].Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Tables[0].Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Tables[0].Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Tables[0].Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Tables[0].Rows[0]["Description"].ToString();

                lbl_servicerequest.Text = dt.Tables[0].Rows[0]["HelpName"].ToString();

                /*penambahan security deposit final inspect chandra*/
                //Session["reletedCaseDescription"] = dt.Tables[0].Rows[0]["Description"].ToString();
            }
            else
            {
                Response.Redirect("~/404_pagenotfound.aspx");
            }
        }
    }
}