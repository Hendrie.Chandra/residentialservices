﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web
{
    public partial class entrybcdprice : System.Web.UI.Page
    {
        static String prevPage = "listbcdprice.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                
                if (Request.QueryString["HelpID"] != null)
                    Show_Case(Int32.Parse(Request.QueryString["HelpID"].ToString()));
            }
        }

        private void Show_Case(Int32 _HelpID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_BCD_HelpName(_HelpID);

            txt_helpname.Text = dt.Rows[0]["HelpName"].ToString();           
            txt_price.Text = dt.Rows[0]["Price"].ToString();
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (validation())
            {
                try
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    UserData userData = (UserData)ViewState["UserData"];

                    Int32 HelpID = 0;
                    if (Request.QueryString["HelpID"] != null)
                        HelpID = Int32.Parse(Request.QueryString["HelpID"].ToString());

                    dbclass.Save_BCDPrice(HelpID, Int32.Parse(txt_price.Text), userData.UserName);
                    Response.Redirect(prevPage);
                }
                catch (Exception ex)
                {
                    alert_danger_text.Text = ex.Message;
                    alert_danger.Visible = true;
                }
            }
        }

        private Boolean validation()
        {
            if (String.IsNullOrEmpty(txt_price.Text))
            {
                alert_danger_text.Text = "Price is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}