﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class OtherIncome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Dept"] != null)
                {
                    HyperLink1.NavigateUrl = HyperLink1.NavigateUrl + "?Dept=" + Request.QueryString["Dept"].ToString();

                    ViewState["UserData"] = new UserData(User.Identity.Name);
                    ViewState["sortOrder"] = "";

                    show_cbo_casestatus();
                    bounddatagrid("","");
                }
                else
                {
                    Response.Redirect("~/404_pagenotfound.aspx");
                }
            }
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            UserData userData = (UserData)ViewState["UserData"];

            Int32 CaseStatusID = Int32.Parse(cbo_status.SelectedValue);
            String Search = "";
            if (!String.IsNullOrEmpty(txt_search.Text))
                Search = txt_search.Text;

            DataSet dt = residential.web.Classes.clsOtherIncome.ViewCaseList(Request.QueryString["Dept"].ToString(), userData.OrgID, userData.SiteID, Search, CaseStatusID);

            lblTitle.Text = dt.Tables[0].Rows[0]["DepartmentName"].ToString();

            DataView dv = new DataView(dt.Tables[1]);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            gvw_data.DataSource = null;
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        private void show_cbo_casestatus()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_casestatus();
            cbo_status.DataSource = dt;
            cbo_status.DataTextField = "CaseStatusName";
            cbo_status.DataValueField = "CaseStatusID";
            cbo_status.DataBind();
            cbo_status.Items.Insert(0, new ListItem("[ All Status ]", "0"));
            cbo_status.SelectedIndex = 0;
        }

        protected void cbo_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = "location.href='ViewOtherIncome.aspx?casenumber=" + DataBinder.Eval(e.Row.DataItem, "CaseNumber") + "'";

                int CaseStatusID = int.Parse(DataBinder.Eval(e.Row.DataItem, "CaseStatusID").ToString());
                if (!String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString()) && (CaseStatusID == 2 || CaseStatusID == 4))
                {
                    DateTime OverdueDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString());
                    if (DateTime.Compare(DateTime.Now, OverdueDate) >= 0)
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                }
            }
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }
    }
}



  