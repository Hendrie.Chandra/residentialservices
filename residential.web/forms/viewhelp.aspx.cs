﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class viewhelp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                Load_HelpTree();
                
            }
        }

        public void Load_HelpTree()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_help();

            HelpTree.Nodes.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                if (dr["ParentHelpID"].ToString() == "")
                {
                    TreeNode tnParent = new TreeNode();
                    tnParent.Text = dr["HelpName"].ToString();
                    tnParent.Value = dr["HelpID"].ToString();
                    HelpTree.Nodes.Add(tnParent);
                    FillChild(tnParent, int.Parse(tnParent.Value));
                }
            }
            HelpTree.ExpandAll();
            HelpTree.Nodes[0].Selected = true;
            String popup_url_add = "openWindowModal('entryhelp.aspx?flag=1&stat=new&NodeDepth=" + HelpTree.SelectedNode.Depth.ToString() + "&HelpID=" + HelpTree.SelectedValue + "', 700, 400)";            
            btn_add.Attributes.Add("onclick", popup_url_add);

            String popup_url_edit = "openWindowModal('entryhelp.aspx?flag=1&stat=edit&NodeDepth=" + HelpTree.SelectedNode.Depth.ToString() + "&HelpID=" + HelpTree.SelectedValue + "', 700, 400)";
            btn_edit.Attributes.Add("onclick", popup_url_edit);

        }

        public void FillChild(TreeNode _tnParent, int _ParentHelpID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_help(0, _ParentHelpID);            

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    TreeNode child = new TreeNode();
                    child.Text = dr["HelpName"].ToString().Trim();
                    child.Value = dr["HelpID"].ToString();
                    _tnParent.ChildNodes.Add(child);
                    FillChild(child, int.Parse(child.Value));
                }
            }
        }        

        protected void btn_add_Click(object sender, EventArgs e)
        {
            Load_HelpTree();
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            Delete_Node(HelpTree.SelectedNode);
        }

        protected void HelpTree_SelectedNodeChanged(object sender, EventArgs e)
        {
            String popup_url = "openWindowModal('entryhelp.aspx?flag=1&stat=new&NodeDepth=" + HelpTree.SelectedNode.Depth.ToString() + "&HelpID=" + HelpTree.SelectedValue + "', 700, 400)";            
            btn_add.Attributes.Add("onclick", popup_url);

            String popup_url_edit = "openWindowModal('entryhelp.aspx?flag=1&stat=edit&NodeDepth=" + HelpTree.SelectedNode.Depth.ToString() + "&HelpID=" + HelpTree.SelectedValue + "', 700, 400)";
            btn_edit.Attributes.Add("onclick", popup_url_edit);
        }

        public void Delete_Node(TreeNode _ParentNode)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            if (_ParentNode.ChildNodes.Count > 0)
            {
                for (int i = 0; i < _ParentNode.ChildNodes.Count; i++)
                    Delete_Node(_ParentNode.ChildNodes[i]);
                dbclass.delete_help(Int32.Parse(_ParentNode.Value));                                
            }
            else            
                dbclass.delete_help(Int32.Parse(_ParentNode.Value));                                          
            
            Load_HelpTree();
        }

        protected void btn_edit_Click(object sender, EventArgs e)
        {
            Load_HelpTree();
        }        
    }
}