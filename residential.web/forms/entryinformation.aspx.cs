﻿using residential.libs;
using residential.libs.Models;
using residential.web.Classes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class entryinformation : System.Web.UI.Page
    {
        String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var a = HttpContext.Current.Session.SessionID;

                int helpCategoryID = 2; //Chardcoded for information
                ViewState["UserData"] = new UserData(User.Identity.Name);
                UserData userData = (UserData)ViewState["UserData"];

                loadHelpname(userData.OrgID, userData.SiteID, helpCategoryID);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "show_searchable_dropdown", "show_searchable_dropdown();", true);
        }

        protected void loadHelpname(int orgID, int siteID, int helpCategoryID)
        {
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_helpname(orgID, siteID, helpCategoryID);

            ddl_helpname.DataSource = dt;
            ddl_helpname.DataTextField = "Helpname";
            ddl_helpname.DataValueField = "HelpID";

            ddl_helpname.DataBind();
            ddl_helpname.Items.Insert(0, new ListItem("--select one--", "0"));
        }

        private string saveCase()
        {
            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            Int32 HelpID = Int32.Parse(ddl_helpname.SelectedItem.Value);

            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;
            DateTime RequestDate = DateTime.Now;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = HelpID.ToString();//find_crmhelpid(HelpID);              //return CRMHelpID
            request.HelpID = HelpID;
            request.FullName = FullName;
            request.UserName = SavedBy;


            //string defaultPICHead = string.Empty;
            //DataTable dtPIC = clsUser.GetDefaultPICHead(userData.OrgID, userData.SiteID, HelpID);
            //request.WorkedBy = dtPIC.Rows[0]["UserName"].ToString();

            //if (Request.QueryString["CaseNumber"] != null)
            //{
            //    DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
            //    request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //}

            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;
                DateTime? OverdueDate = dbcase.OverdueDate;

                int CaseStatusID = 4;   //default information : Inprogress

                String returnvalue = dbclass.SaveToTMDDB(OrgID, SiteID, null, CaseNumber, CaseStatusID, HelpID, null, null,
                                    CaseOriginID, entryheader.CustomerType, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, RequestDescription, OverdueDate, SavedBy);


                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        string updateRemarks = "Case Created";
                        dbclass.save_tickethistory(CaseNumber, CaseStatusID, FullName, updateRemarks, SavedBy, null);

                        ////kirim email disini                    
                        DataTable dt = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    }
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected void btnSaveClose_Click(object sender, EventArgs e)
        {
            if (entryheader.Validate != "VALID" || ddl_helpname.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Invalid Data";
                return;
            }
            else if (entryheader.Validate == "VALID" || ddl_helpname.SelectedIndex > 0)
            {
                if (ddl_helpname.SelectedValue != "0")
                {
                    string res = saveCase();
                    if (!string.IsNullOrEmpty(res))
                    {
                        entryheader.ShowAlert = res;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(1,'Case number has been successfully created !');", true);
                        //Response.Redirect("information.aspx");
                    }
                }
                else
                {
                    entryheader.ShowAlert = "Please Fill All Required Field !";
                    return;
                }
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }
    }
}