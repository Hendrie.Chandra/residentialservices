﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk.Query;
using residential.libs.Models;
using System.IO;

namespace residential.web.forms
{
    public partial class ViewOtherIncome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_case(Request.QueryString["CaseNumber"]);
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                string deptID = Request.QueryString["DeptID"];

                if (deptID == null)
                {
                    //updatecase.Visible = false;
                    /*tampilkan view untuk update cencel di cs*/
                    updatecase.Visible = true;
                    updatecase.DepartmentID = "";
                }
                else
                {
                    updatecase.Visible = true;
                    updatecase.DepartmentID = Request.QueryString["DeptID"].ToString();
                }
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataSet dt = residential.web.Classes.clsOtherIncome.OtherIncomeDetail(userData.OrgID, userData.SiteID, _casenumber);

            if (dt.Tables[0].Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Tables[0].Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Tables[0].Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Tables[0].Rows[0]["UnitCode"].ToString() + " - " + dt.Tables[0].Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Tables[0].Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Tables[0].Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Tables[0].Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Tables[0].Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Tables[0].Rows[0]["Description"].ToString();

                lbl_servicerequest.Text = dt.Tables[0].Rows[0]["HelpName"].ToString();

                Decimal discount = Decimal.Parse(dt.Tables[0].Rows[0]["TotalDiscount"].ToString());

                Decimal subtotal = Decimal.Parse(dt.Tables[0].Rows[0]["TotalAmount"].ToString());
                lbl_subtotal.Text = subtotal.ToString("N0");

                lbl_adjustment.Text = discount.ToString("N0");

                lblPaymentScheme.Text = dt.Tables[0].Rows[0]["PaymentSchemeDesc"].ToString();

                Decimal total = Decimal.Parse(dt.Tables[0].Rows[0]["TotalAmountAfterDiscount"].ToString());
                lbl_total.Text = total.ToString("N0");

                gvw_data.DataSource = dt.Tables[1];
                gvw_data.DataBind();

                if (dt.Tables[0].Rows[0]["StatusAlreadyApproved"].ToString() == "0")
                {
                    updatecase.Visible = false;

                    string a = Request.QueryString["DeptID"];

                    if (a == null)
                    {
                        a = "0";
                    }

                    DataTable dz = residential.web.Classes.clsOtherIncome.CheckIsNeedApprovalUser
                             (userData.OrgID, userData.SiteID, userData.UserName, a);
                    if (dz.Rows[0]["IsAvailable"].ToString() != "0")
                    {
                        btnApproval.Visible = true;
                    }
                    else
                    {
                        btnApproval.Visible = false;
                    }
                }
                else
                {
                    updatecase.Visible = true;
                    btnApproval.Visible = false;
                }
            }
            else
            {
                Response.Redirect("~/404_pagenotfound.aspx");
            }
        }

        protected void btnApproval_Click(object sender, EventArgs e)
        {
            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = residential.web.Classes.clsOtherIncome.ApprovalUpdateTRRevenue
                    (userData.OrgID, userData.SiteID, userData.UserName, Request.QueryString["CaseNumber"]);

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Info", "alert('The Case Approved. Waiting for Customer payment..');window.location ='" + Request.RawUrl + "';", true);

            //Response.Redirect(Request.RawUrl);
        }
    }
}