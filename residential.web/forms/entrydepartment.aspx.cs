﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class entrydepartment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                              
                this.combobox_sites();

                if (Request.QueryString["DepartmentID"] != null)
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    int DepartmentID = int.Parse(Request.QueryString["DepartmentID"].ToString());
                    DataTable dt = dbclass.retrieve_department(DepartmentID);

                    DataRow datarow = dt.Rows[0];
                    txt_departmentcode.Text = datarow["DepartmentCode"].ToString();
                    txt_departmentname.Text = datarow["DepartmentName"].ToString();
                    cbo_sites.SelectedValue = datarow["SiteID"].ToString();
                }
            }
        }

        private void combobox_sites()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_site();

            cbo_sites.DataSource = dt;
            cbo_sites.DataTextField = "SiteName";
            cbo_sites.DataValueField = "SiteID";
            cbo_sites.DataBind();
            cbo_sites.SelectedIndex = 0;
        }

        private Int32 save_departments()
        {
            Int32 DepartmentID = 0;
            String SavedID = Session["username"].ToString();

            ////String flag = Request.QueryString["flag"].ToString();
            if (Request.QueryString["DepartmentID"] != null)                
                DepartmentID = Int32.Parse(Request.QueryString["DepartmentID"].ToString());

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = dbclass.save_department(DepartmentID, txt_departmentcode.Text, txt_departmentname.Text, int.Parse(cbo_sites.SelectedValue), SavedID);

            return returnvalue;
        }

        protected Boolean departments_validation()
        {
            if (String.IsNullOrEmpty(txt_departmentcode.Text))
            {
                alert_danger_text.Text = "Department Code is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (departments_validation())
            {
                Int32 returnvalue = save_departments();                
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (departments_validation())
            {
                Int32 returnvalue = save_departments();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            }
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            if (departments_validation())
            {
                Int32 returnvalue = save_departments();
                txt_departmentcode.Text = String.Empty;
                txt_departmentname.Text = String.Empty;
                cbo_sites.SelectedIndex = 0;
            }
        }
    }
}