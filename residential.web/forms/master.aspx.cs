﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class master : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //System.Web.UI.HtmlControls.HtmlGenericControl innerlinkcell = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            //innerlinkcell.Style.Add("class", "innerlinkcell");

            //String inner = "<asp:Image ID='Image6' runat='server' ImageUrl='~/images/icon-setup.png' CssClass='innerlinkicon' /> " +
            //                "<br />" +
            //                "<asp:HyperLink ID='HyperLink5' runat='server' NavigateUrl='~/forms/bannerlocations.aspx' " +
            //                "CssClass='contentlink'>Banner Location</asp:HyperLink> " +
            //                "<br /> " +
            //                "Setup all parameters to use in application";
            //innerlinkcell.InnerHtml = inner;

            //Image image = new Image();
            //image.ImageUrl = "~/images/icon-setup.png";
            //image.CssClass = "innerlinkicon";

            //HyperLink link = new HyperLink();
            //link.NavigateUrl = "~/forms/sites.aspx";
            //link.CssClass = "contentlink";
            //link.Text = "Sites";

            //innerlinkcell.Controls.Add(image);
            //innerlinkcell.Controls.Add(new LiteralControl("<br />"));
            //innerlinkcell.Controls.Add(link);
            //innerlinkcell.Controls.Add(new LiteralControl("Manage Sites"));

            //System.Web.UI.HtmlControls.HtmlGenericControl innerlinkcontent = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
            //innerlinkcontent.Style.Add("class", "innerlinkcontent");

            //innerlinkcontent.Controls.Add(innerlinkcell);
            //datamastercontent.Controls.Add(innerlinkcontent);

            for (int i = 0; i < 2; i++)
            {
                System.Web.UI.HtmlControls.HtmlGenericControl innerlinkcell = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                innerlinkcell.Style.Add("class", "innerlinkcell");

                Image image = new Image();
                image.ImageUrl = "~/images/icon-setup.png";
                image.CssClass = "innerlinkicon";

                HyperLink link = new HyperLink();
                link.NavigateUrl = "~/forms/sites.aspx";
                link.CssClass = "contentlink";
                link.Text = "Sites";


                innerlinkcell.Controls.Add(image);
                //innerlinkcell.Controls.Add(new LiteralControl("<br />"));
                innerlinkcell.Controls.Add(link);
                //innerlinkcell.Controls.Add(new LiteralControl("Manage Sites"));

                System.Web.UI.HtmlControls.HtmlGenericControl innerlinkcontent = new System.Web.UI.HtmlControls.HtmlGenericControl("div");
                innerlinkcontent.Style.Add("class", "innerlinkcontent");

                innerlinkcontent.Controls.Add(innerlinkcell);
                datamastercontent.Controls.Add(innerlinkcontent);
            }

        }
    }
}