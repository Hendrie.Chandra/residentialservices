﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewktp_kk.aspx.cs"
    Inherits="residential.web.forms.viewktp_kk" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function closeDetailForm() {
            $('#modalFormDetailID').modal('hide');
        }

        function showModalFormDetailID(personalDetailID) {
            $('#<%# txtPersonalIdentificationDetailID.ClientID %>').val(personalDetailID);
            $('#<%# btn_showDetailPersonalID.ClientID %> ').click();            
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>Permohonan KTP / KK <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase1" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
    </div>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <div class="panel panel-default">
        <div class="panel-heading">Personal ID Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Request</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblRequest" runat="server"></asp:Label>
                        </p>
                    </div>
                    <label class="col-sm-2 control-label">Zone</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblZone" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Qty x Price</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lblJumlahKTP" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblJumlahKK" runat="server" Visible="false"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Personal ID List</label>
                    <div class="col-sm-10">
                        <asp:GridView ID="gvw_detail" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                            ShowHeaderWhenEmpty="True" OnRowDataBound="gvw_detail_RowDataBound"
                            DataKeyNames="PersonalIdentificationDetailID">
                            <Columns>
                                <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30px">
                                    <ItemTemplate>
                                        <%# (Container.DataItemIndex + 1).ToString() %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name"></asp:BoundField>
                                <asp:BoundField HeaderText="Gender" DataField="Gender" SortExpression="Gender"></asp:BoundField>
                                <asp:BoundField HeaderText="Birth Place" DataField="BirthPlace" SortExpression="BirthPlace"></asp:BoundField>
                                <asp:BoundField HeaderText="Birth date" DataField="Birthdate" SortExpression="Birthdate"
                                    DataFormatString="{0:d MMMM yyyy}"></asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No data
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Sub Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Adjustment</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_total" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
<%--    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="modal fade" id="modalFormDetailID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Personal ID Detail</h4>
                        </div>
                        <div class="modal-body">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <asp:Button ID="btn_showDetailPersonalID" runat="server" UseSubmitBehavior="false" Style="display: none" OnClick="btn_showDetailPersonalID_Click" />
                            <hr />
                            <asp:HiddenField ID="txtPersonalIdentificationDetailID" runat="server" />
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Lengkap</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtNamaLengkap" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="cboJenisKelamin" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <label class="col-sm-2 control-label">Hubungan</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="cboRelation" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tempat Lahir</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtTempatLahir" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <label class="col-sm-2 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtTglLahir" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtAlamat" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Desa / Kelurahan</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtKelurahan" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <label class="col-sm-2 control-label">Kecamatan</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtKecamatan" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Propinsi / Negara</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtPropinsiNegara" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="cboStatusPernikahan" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <label class="col-sm-2 control-label">Golongan Darah</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="cboGolDarah" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Agama</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="cboAgama" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <label class="col-sm-2 control-label">Pendidikan</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="cboPendidikan" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pekerjaan</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="cboPekerjaan" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                    <label class="col-sm-2 control-label">Nama Bapak / Ibu</label>
                                    <div class="col-sm-4">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtNamaOrangTua" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Keterangan</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtKeterangan" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">W.N.R.I</label>
                                    <div class="col-sm-10">
                                        <p class="form-control-static">
                                            <asp:Label ID="txtWNRI" runat="server"></asp:Label>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
