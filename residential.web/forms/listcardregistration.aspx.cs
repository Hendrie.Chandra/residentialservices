﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class listcardregistration : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                load_case_status();

                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";
                bounddatagrid("", "");
            }
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String Search = null;
            if (!String.IsNullOrEmpty(txt_search.Text))
                Search = txt_search.Text;

            //use for filter status
            int? CaseStatusID = null;
            if (int.Parse(ddl_case_status.SelectedItem.Value) == 0)
                CaseStatusID = null;
            else if (int.Parse(ddl_case_status.SelectedItem.Value) == 2 || int.Parse(ddl_case_status.SelectedItem.Value) == 6)
                CaseStatusID = int.Parse(ddl_case_status.SelectedItem.Value); 

            //DataTable dt = dbclass.retrieve_smartcard_registration(userData.OrgID, userData.SiteID, Search);
            DataTable dt = dbclass.retrieve_smartcard_registration(userData.OrgID, userData.SiteID, CaseStatusID, Search);
            
            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {
                    Label lbl_no = (Label)e.Row.FindControl("lbl_no");
                    Int32 no_urut = e.Row.RowIndex + 1;
                    no_urut = gvw_data.PageIndex * gvw_data.PageSize + no_urut;
                    lbl_no.Text = no_urut.ToString();
                }

                String CaseNumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();

                if (e.Row.Cells[4].Text.ToLower()=="progress complete")
                    e.Row.Attributes["onclick"] = "window.location = 'viewsmartcardregistration.aspx?CaseNumber=" + CaseNumber + "'";
                else if (e.Row.Cells[4].Text.ToLower() == "assigned to pic")
                    e.Row.Attributes["onclick"] = "window.location = 'entrysmartcardregistration.aspx?CaseNumber=" + CaseNumber + "'";
                
                
            }
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        protected void load_case_status()
        {
            Dictionary<string, string> listscasetatus = new Dictionary<string, string>();
            listscasetatus.Add("0", "All");
            listscasetatus.Add("2", "Assigned to PIC");
            listscasetatus.Add("6", "Progress Complete");

            ddl_case_status.DataValueField = "key";
            ddl_case_status.DataTextField = "Value";
            ddl_case_status.DataSource = listscasetatus;
            ddl_case_status.DataBind();
        }

        protected void ddl_case_status_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }
    }
}