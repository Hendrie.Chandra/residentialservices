﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryrebanner.aspx.cs"
    Inherits="residential.web.forms.entryrebanner" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function LookupReference(_caseNumber) {
            $('#<%= txtCaseNumber.ClientID %>').val(_caseNumber);

            $('#<%= btn_lookupReference.ClientID %>').click();

            $('#modalReferenceLookup').modal('hide');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form Penggantian Banner</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <%--<div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>--%>
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-2 control-label">Case Reference</label>
            <div class="col-sm-8">
                <asp:TextBox ID="txtCaseNumber" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
            </div>
            <div class="col-sm-2">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalReferenceLookup">Load</button>
                <%--<asp:Button ID="btnLoad" runat="server" Text="Load" OnClick="btnLoad_Click" CssClass="btn btn-primary" UseSubmitBehavior="false" data-toggle="modal" data-target="#modalReferenceLookup" />--%>
            </div>
        </div>
        <hr />
    </div>
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Button ID="btn_lookupReference" runat="server" Text="Button Load Case" OnClick="btn_lookupReference_Click" Style="display: none" UseSubmitBehavior="false" />
            <div class="panel panel-default">
                <div class="panel-heading">Re-Banner Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner List</label>
                            <div class="col-sm-10">
                                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                    AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound" ShowHeaderWhenEmpty="true">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Link ID">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk_case" runat="server" CommandName="cmd_edit" CssClass="gridview_link">Link ID</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="BannerType" DataField="BannerTypeName" SortExpression="BannerTypeName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Location" DataField="LocationName" SortExpression="LocationName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Point" DataField="PointName" SortExpression="PointName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="StartDate" DataField="StartDate" SortExpression="StartDate"
                                            DataFormatString="{0:d MMMM yyyy}">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="EndDate" DataField="ToDate" SortExpression="EndDate"
                                            DataFormatString="{0:d MMMM yyyy}">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Price" DataField="Price" SortExpression="Price">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_right" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Change Banner List</label>
                            <div class="col-sm-10">
                                <asp:GridView ID="gvw_detail" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                    AllowPaging="True" ShowHeaderWhenEmpty="true" DataKeyNames="LineID">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                                                    AutoPostBack="true" />
                                            </HeaderTemplate>
                                            <HeaderStyle CssClass="gridview_header" Width="30px" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk_select" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="LineID" DataField="LineID" SortExpression="LineID">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Location" DataField="LocationName" SortExpression="LocationName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Point" DataField="PointName" SortExpression="PointName">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField HeaderText="Change Date" DataField="ChangeDate" SortExpression="ChangeDate"
                                            DataFormatString="{0:d MMMM yyyy}">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Type</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboType" runat="server" CssClass="form-control" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Location</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="cboLocation" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Point</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboPoint" runat="server" CssClass="form-control" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Change Date</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txtChangeDate" runat="server" CssClass="form-control"></asp:TextBox>
                                <asp:CalendarExtender ID="txt_date_CalendarExtender" runat="server" TargetControlID="txtChangeDate"
                                    Format="dd MMMM yyyy">
                                </asp:CalendarExtender>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-10 text-right">
                                <asp:Button ID="btn_add_detail" runat="server" Text="Add" CssClass="btn btn-primary" OnClick="btn_add_Click" UseSubmitBehavior="false" />
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-default" OnClick="btnDelete_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-6"></div>
                            <label class="col-sm-2 control-label">Adjustment</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_adjustment" runat="server" CssClass="form-control" Type="Number"
                                    AutoPostBack="True" OnTextChanged="txt_discount_TextChanged">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Modal -->
    <div class="modal fade" id="modalReferenceLookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Reference Number Lookup</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Search By</label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txt_referencenumber" runat="server" CssClass="form-control" placeholder="case number"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_name" runat="server" CssClass="form-control" placeholder="requestor name"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txt_date" runat="server" CssClass="form-control" placeholder="date"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txt_date"
                                            Format="dd MMMM yyyy">
                                        </asp:CalendarExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <asp:Button ID="btn_search" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="btn_search_Click" UseSubmitBehavior="false" />
                                    </div>
                                </div>
                                <hr />
                                <div class="form-group">
                                    <asp:GridView ID="gvw_lookup" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                        AllowPaging="True" DataKeyNames="CaseNumber" OnPageIndexChanging="gvw_lookup_PageIndexChanging"
                                        OnRowDataBound="gvw_lookup_RowDataBound">
                                        <PagerStyle CssClass="pagination-ys" />
                                        <Columns>
                                            <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber" />
                                            <asp:BoundField HeaderText="NAME" DataField="NamaPelapor" />
                                            <asp:BoundField HeaderText="REQUEST DATE" DataField="RequestDate" DataFormatString="{0:dd-MMM-yyyy}" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No data
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
