﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrybannerlocation.aspx.cs"
    Inherits="residential.web.forms.entrybannerlocation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container navbar-fixed-top alert-custom" role="alert" runat="server" id="alert_danger" visible="false">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
        </div>
    </div>
    <div class="page-header">
        <h1>Form Banner Location</h1>
    </div>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>        
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>       
            <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" UseSubmitBehavior="false" />
            <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />     
            <hr />
            <div class="panel panel-default">
                <div class="panel-heading">Banner Location Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Location Code</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_codebanloc" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                            <label class="col-sm-2 control-label">Location Name</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_bannerlocactionname" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Type</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cbo_bannerType" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Price</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_price" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default" runat="server" id="div_points" visible="false">
                <div class="panel-heading">Point Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Banner Point List</label>
                            <div class="col-sm-10">
                                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                    ShowHeaderWhenEmpty="True" DataKeyNames="PointID">
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Width="30px">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chk_selectall" runat="server" OnCheckedChanged="chk_selectall_CheckedChanged"
                                                    AutoPostBack="true" />
                                            </HeaderTemplate>                                            
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk_select" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No" HeaderStyle-Width="30px" ItemStyle-HorizontalAlign="Center">                                           
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Point Name" DataField="PointName" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No Data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <asp:Button ID="btn_add" runat="server" Text="Add Point" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_add_Click" data-toggle="modal" data-target="#modalPoint" />
                                <asp:Button ID="btn_delete" runat="server" Text="Delete Point" CssClass="btn btn-default" OnClick="btn_delete_Click"  UseSubmitBehavior="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>        
    </asp:UpdatePanel>
    <div class="modal fade" id="modalPoint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Form Banner Point</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Point Name</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txt_point" runat="server" CssClass="form-control" required="required"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <asp:Button ID="btn_savePoint" runat="server" Text="Save" UseSubmitBehavior="false" CssClass="btn btn-primary" OnClick="btn_savePoint_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
