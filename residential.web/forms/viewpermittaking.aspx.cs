﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class viewpermittaking : System.Web.UI.Page
    {
        //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
        //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
        //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
        //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
        //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                
                show_case(Request.QueryString["CaseNumber"]);
                CalendarExtender1.StartDate = DateTime.Today;
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_BCDPermitTaking(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                lbl_servicerequest.Text = dt.Rows[0]["HelpName"].ToString();
                lbl_duration.Text = dt.Rows[0]["Duration"].ToString();

                String PaymentStatus = dt.Rows[0]["PaymentStatus"].ToString();
                lbl_paymentstatus.Text = PaymentStatus;

                Decimal subtotal = Decimal.Parse(dt.Rows[0]["TotalAmount"].ToString());
                lbl_subtotal.Text = subtotal.ToString("N0");

                Decimal discount = Decimal.Parse(dt.Rows[0]["TotalDiscount"].ToString());
                lbl_adjustment.Text = discount.ToString("N0");

                Decimal total = Decimal.Parse(dt.Rows[0]["TotalAmountAfterDiscount"].ToString());
                lbl_total.Text = total.ToString("N0");
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }    
        }

        protected void txt_startdate_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(txt_startdate.Text))
            {
                DateTime StartDate = DateTime.Parse(txt_startdate.Text);
                DateTime EndDate = StartDate.AddDays(Int32.Parse(lbl_duration.Text));
                lbl_enddate.Text = EndDate.ToString("dd MMMM yyyy");
            }
        }

        private Boolean transaction_validation()
        {
            if (String.IsNullOrWhiteSpace(txt_startdate.Text))
            {
                viewheader.ShowAlert = "Start Date is required!";                
                return false;
            }
            return true;
        }

        private void Create_Ticket_History()
        {
            UserData userData = (UserData)ViewState["UserData"];

            /*CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);
            crmclass.Create_Ticket_History(Request.QueryString["CaseNumber"].ToString(), 9, "Taken by Customer", userData.FullName);*/

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            dbclass.save_tickethistory(Request.QueryString["CaseNumber"].ToString(), 9, userData.FullName, "Taken by Customer", userData.UserName, null);
        }

        private void Save_PermitTaking()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            
            UserData userData = (UserData)ViewState["UserData"];
            
            dbclass.Save_BCDPermitTaking(Request.QueryString["CaseNumber"].ToString(), DateTime.Parse(txt_startdate.Text), DateTime.Parse(lbl_enddate.Text), userData.UserName);

            Create_Ticket_History();
        }

        protected void btn_update_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                Save_PermitTaking();                
                Response.Redirect(ViewState["PreviousPageUrl"].ToString());
            }
        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {            
            Response.Redirect(ViewState["PreviousPageUrl"].ToString());
        }        
    }
}