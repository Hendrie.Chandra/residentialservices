﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using System.Security.Principal;
using System.Threading;
using System.Net;
using residential.web.Classes;


namespace residential.web.forms
{
    public partial class listResidentData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                UserData userData = (UserData)ViewState["UserData"];

                ddlCluster.DataSource = clsResident.ClusterList(userData.OrgID, userData.SiteID);
                ddlCluster.DataValueField = "UnitClusterCode";
                ddlCluster.DataTextField = "UnitClusterDesc";
                ddlCluster.DataBind();

                ddlCluster_SelectedIndexChanged(null, null);
                ddlUnitCode_SelectedIndexChanged(null, null);
            }
        }

        protected void ddlCluster_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_search.Text = "";
            if (ddlCluster.SelectedIndex > -1)
            {
                UserData userData = (UserData)ViewState["UserData"];
                ddlUnitCode.DataSource = clsResident.UnitCodeList(userData.OrgID, userData.SiteID, ddlCluster.SelectedValue.ToString());
                ddlUnitCode.DataValueField = "UnitCode";
                ddlUnitCode.DataTextField = "UnitDesc";
                ddlUnitCode.DataBind();

                ddlUnitCode_SelectedIndexChanged(null, null);
            }
        }

        protected void ddlUnitCode_SelectedIndexChanged(object sender, EventArgs e)
        {
            txt_search.Text = "";
            if (ddlUnitCode.SelectedIndex > -1)
            {
                DataList();
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String ResidentHeaderID = DataBinder.Eval(e.Row.DataItem, "ResidentHeaderID").ToString();
                string Unit = DataBinder.Eval(e.Row.DataItem, "Unit").ToString();
                String linkToForm = "EntryResidentData.aspx";
                //e.Row.Attributes["onclick"] = "window.location = '" + linkToForm + "?RHI=" + ResidentHeaderID + "&U=" + Unit + "'";

                HyperLink addNewResidentMember = (HyperLink)e.Row.FindControl("lnk_addMember");
                addNewResidentMember.Attributes["onclick"] = "window.location = '" + linkToForm + "?RHI=" + ResidentHeaderID + "&U=" + Unit + "'";
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            DataList();
        }

        void DataList()
        {
            UserData userData = (UserData)ViewState["UserData"];
            gvw_data.DataSource = null;
            gvw_data.DataSource = clsResident.ResidentList(userData.OrgID, userData.SiteID,
                ddlCluster.SelectedValue.ToString(), ddlUnitCode.SelectedValue.ToString(), txt_search.Text.ToString());
            gvw_data.DataBind();
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            DataList();
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            UserData userData = (UserData)ViewState["UserData"];
            if (txtUnitCode.Text != "" && txtUnitNo.Text != "")
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                string unitCode = txtUnitCode.Text;
                string unitNo =txtUnitNo.Text;
                string ownerPsCode = hdfPsCodeUnitOwner.Value;
                string tenantPsCode = hdfPsCodeUnitTenant.Value;
                
                string result = dbclass.save_resident_header(userData.OrgID, userData.SiteID, unitCode, unitNo, ownerPsCode, tenantPsCode, userData.UserName);

                //result code   1:success update 0:success insert
                if (result == "0")
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(1,'Resident unit header has been successfully created !');", true);
                else if (result=="1")
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(1,'Resident unit header has been successfully updated !');", true);
                else if (result.Length>1)
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog('0," + result + "');", true);          
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('Please Fill All Required Field *');", true);
            }
        }

        protected void txtUnitCode_TextChanged(object sender, EventArgs e)
        {
            alert_danger.Visible = false;
            alert_danger_text.Text = "";

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);            
            UserData userData = (UserData)ViewState["UserData"];
            
            string unitCode = txtUnitCode.Text;
            string unitNo = txtUnitNo.Text;
            DataTable dt = dbclass.retrieve_unit_owner_byunit(userData.OrgID, userData.SiteID, unitCode, unitNo);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string unitStatus = dr["UnitStatus"].ToString();
                    if (unitStatus.ToUpper() == "OWNER")
                        txtUnitOwner.Text = dr["name"].ToString();
                    if (unitStatus.ToUpper() == "TENANT")
                        txtUnitTenant.Text = dr["name"].ToString();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "EnableSaveButton", "EnableSaveButton();",true);

            }
            else
            {
                if (txtUnitCode.Text != "" && txtUnitNo.Text != "")
                {
                    alert_danger.Visible = true;
                    alert_danger_text.Text = "Unit not found !";
                }
                txtUnitOwner.Text = "";
                txtUnitTenant.Text = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DisableSaveButton", "DisableSaveButton();", true);
            }
            txtUnitNo.Focus();

        }

        protected void txtUnitNo_TextChanged(object sender, EventArgs e)
        {
            alert_danger.Visible = false;
            alert_danger_text.Text = "";

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            UserData userData = (UserData)ViewState["UserData"];

            string unitCode = txtUnitCode.Text;
            string unitNo = txtUnitNo.Text;
            DataTable dt = dbclass.retrieve_unit_owner_byunit(userData.OrgID, userData.SiteID, unitCode, unitNo);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    string unitStatus = dr["UnitStatus"].ToString();
                    if (unitStatus.ToUpper() == "OWNER")
                    {
                        txtUnitOwner.Text = dr["name"].ToString();
                        hdfPsCodeUnitOwner.Value = dr["PsCode"].ToString();
                    }
                    if (unitStatus.ToUpper() == "TENANT")
                    {
                        txtUnitTenant.Text = dr["name"].ToString();
                        hdfPsCodeUnitTenant.Value = dr["PsCode"].ToString();
                    }                        
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "EnableSaveButton", "EnableSaveButton();", true);
            }
            else
            {
                if (txtUnitCode.Text != "" && txtUnitNo.Text != "")
                {
                    alert_danger.Visible = true;
                    alert_danger_text.Text = "Unit not found !";
                }
                txtUnitOwner.Text = "";
                txtUnitTenant.Text = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "DisableSaveButton", "DisableSaveButton();", true);
            }
            txtUnitCode.Focus();
        }

        protected void btn_cek_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.cek_unit_exist(txtUnitCode.Text, txtUnitNo.Text);
            if (dt.Rows.Count > 0)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "show_dialog", "show_dialog('Unit already exist, klik ok to update with new owner or new tenant');", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "show_dialog", "show_dialog('Are you sure want to save this data?');", true);
        }



    }
}