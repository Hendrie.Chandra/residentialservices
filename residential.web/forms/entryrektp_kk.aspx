﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryrektp_kk.aspx.cs" Inherits="residential.web.forms.entryrektp_kk" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form perpanjangan KTP</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <%--<div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>--%>
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">RePersonal ID Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Request</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboRequest" runat="server"
                                    CssClass="form-control" AutoPostBack="true"
                                    OnSelectedIndexChanged="cboRequest_SelectedIndexChanged">
                                    <asp:ListItem Selected="True">KTP</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Zone</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboZone" runat="server"
                                    CssClass="form-control"
                                    OnSelectedIndexChanged="cboZone_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-2"></div>
                            <label class="col-sm-4 control-label">Kartu Tanda Penduduk (KTP)</label>
                            <label class="col-sm-2 control-label">Quantity x Price</label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtJumlahKTP" runat="server" Text="1" CssClass="form-control"
                                    OnTextChanged="txtJumlahKTP_TextChanged" AutoPostBack="true" Type="Number"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                <p class="form-control-static">
                                    &nbsp;x&nbsp;<asp:Label ID="lblPriceKTP" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-6"></div>
                            <label class="col-sm-2 control-label">Adjustment</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_discount" runat="server" CssClass="form-control" Type="Number"
                                    AutoPostBack="True" OnTextChanged="txt_discount_TextChanged">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
