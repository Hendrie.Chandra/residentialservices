﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class smartcardviewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";

                //load_serach_category();
            }
        }

        protected void btn_serach_by_cardnumber_Click(object sender, EventArgs e)
        {
            bounddatagrid("","");
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            string cardDetailData = txt_carddetail.Text;
            string unitCode = txt_unitCode.Text;
            string unitNo = txt_unitNo.Text;


            DataTable dt = dbclass.retrieve_smartcard_viewer(OrgID, SiteID, cardDetailData, unitCode, unitNo);


            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void dll_search_TextChanged(object sender, EventArgs e)
        {

        }

        //protected void load_serach_category()
        //{
        //    Dictionary<string, string> serachby = new Dictionary<string, string>();
        //    serachby.Add("0", "--select one--");
        //    serachby.Add("1", "Card Number");
        //    serachby.Add("2", "Unit");

        //    dll_search.DataSource = serachby;
        //    dll_search.DataValueField = "Key";
        //    dll_search.DataTextField = "Value";
        //    dll_search.DataBind();
        //}


     
    }
}