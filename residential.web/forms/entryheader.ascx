﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="entryheader.ascx.cs" Inherits="residential.web.forms.entryheader" %>

<script>
    function returnVal(_psname, _pscode, _customerType) {
        $('#<%= txt_customername.ClientID %>').val(_psname);
        $('#<%= txt_pscode.ClientID %>').val(_pscode);
        $('#<%= txt_customertype.ClientID %>').val(_customerType);

        $('#<%= btn_lookuppersonal.ClientID %>').click();

        $('#ModalLookup').modal('hide');

        if ($('#ContentPlaceHolder1_btn_cekPemutusan').length > 0)
            $('#ContentPlaceHolder1_btn_cekPemutusan').click();
    }

    function showLoader() {

        if (form1.checkValidity()) {
            $('#<%= UpdateProgress1.ClientID %>').css('display', '');
            return true;
        } else {            
            return false;
        }
    }
</script>
<asp:UpdateProgress ID="UpdateProgress1" runat="server">
    <ProgressTemplate>
        <div id="overlay">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
        </div>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:UpdatePanel ID="up_entryHeader" runat="server">
    <ContentTemplate>
        <div class="container navbar-fixed-top alert-custom" role="alert" runat="server" id="alert_danger" visible="false">
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">Personal Detail</div>
            <div class="panel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Owner / Tenant</label>
                        <div class="col-sm-4">
                            <div class="input-group">
                                <asp:TextBox ID="txt_customername" runat="server" CssClass="form-control" required="required" placeholder="owner / tenant" Enabled="false"></asp:TextBox>
                                <span class="input-group-addon">
                                    <%--<button runat="server" id="btn_OpenLookupWindow" type="button" data-toggle="modal" data-target="#ModalLookup">...</button>--%>
                                    <asp:Button ID="btn_OpenLookupWindow" runat="server" Text="..." data-toggle="modal" data-target="#ModalLookup" UseSubmitBehavior="false" />
                                </span>
                            </div>
                            <asp:Button ID="btn_lookuppersonal" runat="server" Text="..." OnClick="btn_lookuppersonal_Click" Style="display: none" UseSubmitBehavior="false" />
                        </div>
                        <label class="col-sm-2 control-label">PS Code</label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txt_pscode" CssClass="form-control" runat="server" required="required" placeholder="ps code" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Unit</label>
                        <div class="col-sm-4">
                            <asp:DropDownList ID="cbo_unit" runat="server" CssClass="form-control" required="required" />
                        </div>
                        <label class="col-sm-2 control-label">Payment Scheme</label>
                        <asp:HiddenField ID="txt_customertype" runat="server" />
                        <div class="col-sm-4">
                            <asp:DropDownList ID="cbo_paymentscheme" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Requestor Name</label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txt_requestname" CssClass="form-control" runat="server" required="required" placeholder="name"></asp:TextBox>
                        </div>
                        <label class="col-sm-2 control-label">Case Origin</label>
                        <div class="col-sm-4">
                            <asp:DropDownList ID="cbo_caseorigin" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Requestor Phone</label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txt_phone" CssClass="form-control" runat="server" required="required" placeholder="phone"></asp:TextBox>
                        </div>
                        <label class="col-sm-2 control-label">Requestor Email</label>
                        <div class="col-sm-4">
                            <asp:TextBox ID="txt_email" CssClass="form-control" runat="server" Type="Email" placeholder="email"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txt_description" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" placeholder="description"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="modal fade" id="ModalLookup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Personal Find</h4>
            </div>
            <div class="modal-body">
                <asp:UpdatePanel ID="up_customerLookup" runat="server">
                    <ContentTemplate>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Customer Type</label>
                                <div class="col-sm-10">
                                    <asp:RadioButtonList ID="rdb_customerTypeLookup" runat="server" CssClass="radio" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Owner" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Tenant" Value="1" Selected="True"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Search By</label>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="txt_personal" runat="server" CssClass="form-control" placeholder="ps code, name, address"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txt_unit" runat="server" CssClass="form-control" placeholder="unit code, unit cluster"></asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txt_unitno" runat="server" CssClass="form-control" placeholder="unit number" Type="number"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <asp:Button ID="btn_search" runat="server" Text="SEARCH" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_search_Click" />
                                </div>
                            </div>
                        </div>
                        <hr />
                        <%--<asp:UpdateProgress ID="updProgressLookup" runat="server" AssociatedUpdatePanelID="up_customerLookup">
                            <ProgressTemplate>
                                <div class="text-center">
                                    <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/loader.GIF" Width="50" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>--%>
                        <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                            CssClass="table table-hover table-bordered" AllowPaging="True"
                            DataKeyNames="pscode" OnPageIndexChanging="gvw_data_PageIndexChanging"
                            OnRowDataBound="gvw_data_RowDataBound">
                            <PagerSettings FirstPageText="First" LastPageText="Last"
                                NextPageText="Next" PreviousPageText="Prev" Position="Bottom" PageButtonCount="5" />
                            <PagerStyle CssClass="pagination-ys" />
                            <Columns>
                                <asp:BoundField HeaderText="PSCODE" DataField="pscode" ItemStyle-Font-Bold="true" />
                                <asp:BoundField HeaderText="NAME" DataField="psname" />
                                <asp:BoundField HeaderText="UNIT" DataField="UNIT" />
                                <asp:BoundField HeaderText="CLUSTER" DataField="UnitClusterDesc" />
                            </Columns>
                            <EmptyDataTemplate>
                                No data
                            </EmptyDataTemplate>
                        </asp:GridView>
                        <asp:Label ID="lbl_itemdisplayed" runat="server"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
