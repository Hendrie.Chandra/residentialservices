﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using residential.libs;

namespace residential.web.forms
{
    public partial class listmagazinetrxtype : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {               
                bounddatagrid();
            }
        }

        private void bounddatagrid()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_magazinetrxtype();

            gvw_data.DataSource = dt;
            gvw_data.DataBind();
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {                
                String MagazineTrxTypeID = DataBinder.Eval(e.Row.DataItem, "MSMagazineTrxTypeID").ToString();
                String MagazineTrxTypeName = DataBinder.Eval(e.Row.DataItem, "MSMagazineTrxTypeName").ToString();
                
                HyperLink lnk_magazinetrxtype = (HyperLink)e.Row.FindControl("lnk_magazinetrxtype");
                lnk_magazinetrxtype.NavigateUrl = "entrymagazinetrxtype.aspx?MSMagazineTrxTypeID=" + MagazineTrxTypeID;
                lnk_magazinetrxtype.Text = MagazineTrxTypeName;
            }
        }
 
        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 ID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());
                    Int32 returnvalue = delete_magazinetrxtype(ID);
                }
            }
            bounddatagrid();
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid();
        }

        private Int32 delete_magazinetrxtype(int dataToDelete)
        {
            String SavedID = Session["username"].ToString();

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = 0;

            returnvalue = dbclass.delete_magazinetrxtype(dataToDelete, SavedID);

            return returnvalue;
        }
    }
}