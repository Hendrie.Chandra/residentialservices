﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI;
using residential.libs;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class entryktp_kk_detail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_cbo_Gender();
                show_cbo_BloodType();
                show_cbo_LastEducation();
                show_cbo_MaritalStatus();
                show_cbo_Occupation();
                show_cbo_Relation();
                show_cbo_Religion();

                if (Request.QueryString["PersonalIdentificationDetailID"] != null)
                    GetDataFromSessionList(Int32.Parse(Request.QueryString["PersonalIdentificationDetailID"].ToString()));
            }
        }

        protected void GetDataFromSessionList(Int32 PersonalIdentificationDetailID)
        {
            var list = (List<DetailKTP_KK>)Session["ListDetailKTP_KK"];

            foreach (DetailKTP_KK det in list)
            {
                if (det.PersonalIdentificationDetailID == PersonalIdentificationDetailID)
                {
                    txtAlamat.Text = det.Address;
                    txtKelurahan.Text = det.Kelurahan;
                    txtKecamatan.Text = det.Kecamatan;
                    txtNamaLengkap.Text = det.Name;                                      
                    cboJenisKelamin.SelectedValue = det.GenderID.ToString();
                    cboRelation.SelectedValue = det.RelationID.ToString();
                    txtTempatLahir.Text = det.BirthPlace;
                    txtTglLahir.Text = det.BirthDate.ToShortDateString();
                    txtPropinsiNegara.Text = det.StateCountry;
                    cboStatusPernikahan.SelectedValue = det.MaritalStatusID.ToString();
                    cboGolDarah.SelectedValue = det.BloodTypeID.ToString();
                    cboAgama.SelectedValue = det.ReligionID.ToString();
                    cboPendidikan.SelectedValue = det.LastEducationID.ToString();
                    cboPekerjaan.SelectedValue = det.OccupationID.ToString();
                    txtNamaOrangTua.Text = det.ParentName;                                                         
                    txtKeterangan.Text = det.Notes;
                    txtWNRI.Text = det.SBKRINotes;
                }
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            DateTime TglLahir;

            if (txtAlamat.Text == "")
            {
                alert_danger_text.Text = "Alamat harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtKelurahan.Text == "")
            {
                alert_danger_text.Text = "Kelurahan harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtKecamatan.Text == "")
            {
                alert_danger_text.Text = "Kecamatan harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (String.IsNullOrWhiteSpace(txtNamaLengkap.Text))
            {
                alert_danger_text.Text = "Nama lengkap harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (cboJenisKelamin.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Jenis kelamin harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (cboRelation.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Hubungan (KK) harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtTempatLahir.Text == "")
            {
                alert_danger_text.Text = "Tempat Lahir harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtTglLahir.Text == "")
            {
                alert_danger_text.Text = "Tanggal lahir harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            else
            {
                try
                {
                    TglLahir = DateTime.Parse(txtTglLahir.Text.ToString());
                }
                catch (Exception)
                {
                    alert_danger_text.Text = "Kesalahan format Tanggal lahir.";
                    alert_danger.Visible = true;
                    return;
                }
            }
            if (cboStatusPernikahan.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Status pernikahan harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (cboAgama.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Agama harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtNamaOrangTua.Text == "")
            {
                alert_danger_text.Text = "Nama orang tua harus diisi.";
                alert_danger.Visible = true;
                return;
            }

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;            
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            List<DetailKTP_KK> list = null;
            Int32 counter = 0;

            if (Session["ListDetailKTP_KK"] == null)
            {
                list = new List<DetailKTP_KK>();
                counter = -1;
            }
            else
            {
                list = (List<DetailKTP_KK>)Session["ListDetailKTP_KK"];
                if (list.Count > 0)
                {
                    Int32 maxObj = list.Min(r => r.PersonalIdentificationDetailID);
                    counter = maxObj - 1;
                }
                else counter = -1;
            }

            Int32 PersonalIdentificationDetailID = 0;
            if (Request.QueryString["PersonalIdentificationDetailID"] != null)
            {
                PersonalIdentificationDetailID = Int32.Parse(Request.QueryString["PersonalIdentificationDetailID"].ToString());

                foreach (DetailKTP_KK det in list)
                {
                    if (PersonalIdentificationDetailID == det.PersonalIdentificationDetailID)
                    {
                        det.Address = txtAlamat.Text;
                        det.Kelurahan = txtKelurahan.Text;
                        det.Kecamatan = txtKecamatan.Text;
                        det.Name = txtNamaLengkap.Text;
                        det.GenderID = Int32.Parse(cboJenisKelamin.SelectedValue.ToString());
                        det.Gender = cboJenisKelamin.SelectedItem.Text;
                        det.RelationID = Int32.Parse(cboRelation.SelectedValue);
                        det.BirthPlace = txtTempatLahir.Text;
                        det.BirthDate = TglLahir;
                        det.StateCountry = txtPropinsiNegara.Text;
                        det.MaritalStatusID = Int32.Parse(cboStatusPernikahan.SelectedValue);
                        det.BloodType = cboGolDarah.SelectedValue;
                        det.BloodTypeID = Int32.Parse(cboGolDarah.SelectedValue);
                        det.ReligionID = Int32.Parse(cboAgama.SelectedValue);
                        det.LastEducationID = Int32.Parse(cboPendidikan.SelectedValue);
                        det.OccupationID = Int32.Parse(cboPekerjaan.SelectedValue);
                        det.ParentName = txtNamaOrangTua.Text;
                        det.SBKRINotes = txtWNRI.Text;
                        det.Notes = txtKeterangan.Text;
                    }
                }
            }
            else
            {
                PersonalIdentificationDetailID = counter;

                var obj = new DetailKTP_KK(
                    OrgID,
                    SiteID,
                    0, // personal identification id is not generated yet
                    PersonalIdentificationDetailID,
                    txtAlamat.Text,
                    txtKelurahan.Text,
                    txtKecamatan.Text,
                    txtNamaLengkap.Text,
                    Int32.Parse(cboJenisKelamin.SelectedValue.ToString()),
                    cboJenisKelamin.SelectedItem.Text,
                    Int32.Parse(cboRelation.SelectedValue),
                    txtTempatLahir.Text,
                    TglLahir,
                    txtPropinsiNegara.Text,
                    Int32.Parse(cboStatusPernikahan.SelectedValue),
                    Int32.Parse(cboGolDarah.SelectedValue),
                    Int32.Parse(cboAgama.SelectedValue),
                    Int32.Parse(cboPendidikan.SelectedValue),
                    Int32.Parse(cboPekerjaan.SelectedValue),
                    txtNamaOrangTua.Text,
                    txtWNRI.Text,
                    txtKeterangan.Text
                    );

                list.Add(obj);
            }

            Session["ListDetailKTP_KK"] = list;

            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
        }

        private void show_cbo_Gender()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Gender();

            cboJenisKelamin.DataSource = dt;
            cboJenisKelamin.DataTextField = "GenderName";
            cboJenisKelamin.DataValueField = "GenderID";
            cboJenisKelamin.DataBind();
            cboJenisKelamin.Items.Insert(0, new ListItem("None", "0"));
            cboJenisKelamin.SelectedIndex = 0;
        }

        private void show_cbo_BloodType()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_BloodType();

            cboGolDarah.DataSource = dt;
            cboGolDarah.DataTextField = "BloodTypeName";
            cboGolDarah.DataValueField = "BloodTypeID";
            cboGolDarah.DataBind();
            cboGolDarah.Items.Insert(0, new ListItem("None", "0"));
            cboGolDarah.SelectedIndex = 0;
        }

        private void show_cbo_Relation()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Relation();

            cboRelation.DataSource = dt;
            cboRelation.DataTextField = "RelationName";
            cboRelation.DataValueField = "RelationID";
            cboRelation.DataBind();
            cboRelation.Items.Insert(0, new ListItem("None", "0"));
            cboRelation.SelectedIndex = 0;
        }

        private void show_cbo_MaritalStatus()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_MaritalStatus();

            cboStatusPernikahan.DataSource = dt;
            cboStatusPernikahan.DataTextField = "MaritalStatusName";
            cboStatusPernikahan.DataValueField = "MaritalStatusID";
            cboStatusPernikahan.DataBind();
            cboStatusPernikahan.Items.Insert(0, new ListItem("None", "0"));
            cboStatusPernikahan.SelectedIndex = 0;
        }

        private void show_cbo_Religion()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Religion();

            cboAgama.DataSource = dt;
            cboAgama.DataTextField = "ReligionName";
            cboAgama.DataValueField = "ReligionID";
            cboAgama.DataBind();
            cboAgama.Items.Insert(0, new ListItem("None", "0"));
            cboAgama.SelectedIndex = 0;
        }

        private void show_cbo_LastEducation()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_LastEducation();

            cboPendidikan.DataSource = dt;
            cboPendidikan.DataTextField = "LastEducationName";
            cboPendidikan.DataValueField = "LastEducationID";
            cboPendidikan.DataBind();
            cboPendidikan.Items.Insert(0, new ListItem("None", "0"));
            cboPendidikan.SelectedIndex = 0;
        }

        private void show_cbo_Occupation()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Occupation();

            cboPekerjaan.DataSource = dt;
            cboPekerjaan.DataTextField = "OccupationName";
            cboPekerjaan.DataValueField = "OccupationID";
            cboPekerjaan.DataBind();
            cboPekerjaan.Items.Insert(0, new ListItem("None", "0"));
            cboPekerjaan.SelectedIndex = 0;
        }
    }
}