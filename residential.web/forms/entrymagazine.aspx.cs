﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entrymagazine : System.Web.UI.Page
    {
        static String prevPage = "magazine.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["ListMagazineDetail"] = null;

                //show_discounttype();
                show_magazine_edition();
                show_magazine_type();                
                gvw_data.DataBind();
                hitung_total();

                //alert_success.Visible = false;
                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
                else
                    updaterequest.Visible = false;
            }
            entryheader.ToggleAlert = false;
        }

        private void Show_Case(String _casenumber)
        {
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DataTable dt = dbclass.retrieve_magazine_by_casenumber(_casenumber);

                entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                //check customer type -> 0 = Owner, 1 = Tenant
                entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheader.combobox_unit();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

                entryheader.disable_all_controls();

                Int32 discount = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString()));
                Int32 subtotal = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Amount"].ToString()));
                Int32 total = Convert.ToInt32(decimal.Parse(dt.Rows[0]["ActualAmount"].ToString()));

                txt_discount.Text = discount.ToString();
                lbl_subtotal.Text = subtotal.ToString("N0");
                lbl_total.Text = total.ToString("N0");


                DataTable dt3 = dbclass.retrieve_magazine_detail(_casenumber);

                List<MagazineDetail> list = new List<MagazineDetail>();
                MagazineDetail md = new MagazineDetail();

                foreach (DataRow dr in dt3.Rows)
                {
                    md = new MagazineDetail();
                    md.MagazineDetailID = Int32.Parse(dr["MagazineDetailID"].ToString());
                    md.MagazineTypeID = Int32.Parse(dr["MagazineTrxTypeID"].ToString());
                    md.MagazineType = dr["MSMagazineTrxTypeName"].ToString();
                    md.MagazineEdition = dr["MagazineEdition"].ToString();
                    md.Qty = Convert.ToInt32(decimal.Parse(dr["Qty"].ToString()));
                    md.Price = Convert.ToInt32(decimal.Parse(dr["Price"].ToString()));
                    md.Amount = Convert.ToInt32(decimal.Parse(dr["Amount"].ToString()));

                    list.Add(md);
                }

                ViewState["ListMagazineDetail"] = list;

                gvw_data.DataSource = list;
                gvw_data.DataBind();
            }
            catch(Exception)
            {
                throw new HttpException(404, "Page not found");
            }
        }
        private void show_magazine_edition()
        {            
            cboEdition.Items.Insert(0, new ListItem("None", "0"));
            for (var i = 1; i <= 12; i++)
                cboEdition.Items.Add(new ListItem(DateTime.Now.AddMonths(i).ToString("MMMM") + " " + DateTime.Now.AddMonths(i).Year, DateTime.Now.AddMonths(i).Month + "-" + DateTime.Now.AddMonths(i).Year));

            cboEdition.SelectedIndex = 0;
        }

        private void show_magazine_type()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_magazine_trx_type();
            cboType.DataSource = dt;
            cboType.DataTextField = "MSMagazineTrxTypeName";
            cboType.DataValueField = "MSMagazineTrxTypeID";
            cboType.DataBind();
            cboType.Items.Insert(0, new ListItem("None", "0"));
            cboType.SelectedIndex = 0;
        }
        
        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }


        protected void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboType.SelectedIndex > 0)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                DataTable dt = dbclass.retrieve_magazineprice(Int32.Parse(cboType.SelectedValue));
                Int32 price = Convert.ToInt32(dt.Rows[0]["Price"]);

                lbPriceDetail.Text = price.ToString("N0");

                if (cboType.SelectedValue == "1" || cboType.SelectedValue == "2" || cboType.SelectedValue == "3")
                    txt_jumlah.Enabled = false;
                else
                    txt_jumlah.Enabled = true;
            }
            else
            {
                lbPriceDetail.Text = "0";
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["MagazineHelpID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);

            Int32 DiscountTypeID = 0;

            Int32 TotalAmount = Int32.Parse(lbl_subtotal.Text, NumberStyles.Currency);
            Int32 TotalDiscount = Int32.Parse(txt_discount.Text, NumberStyles.Currency);
            Int32 TotalAmountAfterDiscount = TotalAmount - TotalDiscount;
            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;
           
            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                /*request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();*/
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_magazine_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, TotalAmount, DiscountTypeID, TotalDiscount, TotalAmountAfterDiscount,
                    0, 0, TotalAmountAfterDiscount, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    dt = dbclass.retrieve_magazine_by_casenumber(CaseNumber);
                    Int32 MagazineID = Int32.Parse(dt.Rows[0]["MagazineID"].ToString());

                    if (Request.QueryString["CaseNumber"] != null)
                        dbclass.delete_magazine_detail_transaction(MagazineID);

                    foreach (MagazineDetail md in (List<MagazineDetail>)ViewState["ListMagazineDetail"])
                        dbclass.save_magazine_detail_transaction(md.MagazineDetailID, MagazineID, md.MagazineTypeID, md.MagazineEdition, md.Qty, md.Price, md.Amount, SavedBy);

                    ViewState["ListMagazineDetail"] = null;

                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected bool transaction_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }
            return true;
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txt_discount.Text))
            {
                txt_discount.Text = "0";
            }

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_magazineprice(Int32.Parse(cboType.SelectedValue));
            Int32 sub_total = Int32.Parse(lbl_subtotal.Text, NumberStyles.Currency);
            Int32 discount = Int32.Parse(txt_discount.Text, NumberStyles.Currency);
            Int32 total = sub_total - discount;
            lbl_adjustment.Text = discount.ToString("N0");
            lbl_total.Text = total.ToString("N0");
        }

        protected void btn_add_detail_Click(object sender, EventArgs e)
        {
            if (cboType.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Please select Magazine Type!";                
                return;
            }
            if (cboEdition.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Please select Magazine Edition!";                
                return;
            }

            if (Int32.Parse(cboType.SelectedValue) == 1 ||
                Int32.Parse(cboType.SelectedValue) == 2 ||
                Int32.Parse(cboType.SelectedValue) == 3)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DataTable dt = dbclass.retrieve_magazine_detail(userData.SiteID, Int32.Parse(cboType.SelectedValue), cboEdition.SelectedValue);

                if (dt.Rows.Count > 0)
                {
                    entryheader.ShowAlert = "Magazine was already booked.";                    
                    return;
                }
            }

            List<MagazineDetail> list = (List<MagazineDetail>)ViewState["ListMagazineDetail"];
            if (list == null) list = new List<MagazineDetail>();

            Int32 MagazineDetailID = -1;



            if (list.Count > 0)
                MagazineDetailID = list.Min(r => r.MagazineDetailID) - 1;

            int found = -1;
            foreach (MagazineDetail obj in list)
            {
                if (obj.MagazineTypeID == Int32.Parse(cboType.SelectedValue))
                    found++;

                if (found >= 0)
                    break;
            }

            if (found >= 0)
            {
                entryheader.ShowAlert = "Magazine type has been added.";                
                return;
            }

            Int32 qty = Int32.Parse(txt_jumlah.Text);
            Int32 price = Int32.Parse(lbPriceDetail.Text, NumberStyles.Currency);
            Int32 amount = qty * price;

            MagazineDetail md = new MagazineDetail(
                MagazineDetailID,
                Int32.Parse(cboType.SelectedValue.ToString()),
                cboType.SelectedItem.Text.ToString(),
                cboEdition.SelectedValue,
                qty,
                price,
                amount);

            list.Add(md);

            ViewState["ListMagazineDetail"] = list;

            gvw_data.DataSource = list;
            gvw_data.DataBind();

            hitung_total();

            cboType.SelectedIndex = 0;
            cboEdition.SelectedIndex = 0;
            lbPriceDetail.Text = "0";            
            txt_jumlah.Enabled = true;
            txt_jumlah.Text = "1";
        }

        private void hitung_total()
        {
            if (txt_discount.Text == string.Empty)
                txt_discount.Text = "0";

            Int32 discount = Convert.ToInt32(txt_discount.Text);
            Int32 total = 0;
            Int32 subtotal = 0;

            if (ViewState["ListMagazineDetail"] == null)
                total = 0;
            else
            {
                var list = (List<MagazineDetail>)ViewState["ListMagazineDetail"];

                if (list.Count == 0)
                    total = 0;
                else
                {
                    foreach (MagazineDetail md in list)
                        total += md.Amount;
                }
            }
            subtotal = total;
            total = subtotal - discount;            
            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_adjustment.Text = discount.ToString("N0");
            lbl_total.Text = total.ToString("N0");
        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var list = (List<MagazineDetail>)ViewState["ListMagazineDetail"];

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 MagazineDetailID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());

                    var itemToRemove = list.SingleOrDefault(r => r.MagazineDetailID == MagazineDetailID);
                    if (itemToRemove != null)
                        list.Remove(itemToRemove);
                }
            }

            ViewState["ListMagazineDetail"] = list;
            gvw_data.DataSource = list;
            gvw_data.DataBind();

            hitung_total();
        }        

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String[] MagzEdition = DataBinder.Eval(e.Row.DataItem, "MagazineEdition").ToString().Split('-');
                e.Row.Cells[3].Text = new DateTime(int.Parse(MagzEdition[1]), int.Parse(MagzEdition[0]), 1).ToString("MMMM") + " " + MagzEdition[1];
            }

            if (e.Row.RowIndex >= 0)
            {
                Label lbl_no = (Label)e.Row.FindControl("lbl_no");
                Int32 no_urut = e.Row.RowIndex + 1;
                no_urut = gvw_data.PageIndex * gvw_data.PageSize + no_urut;
                lbl_no.Text = no_urut.ToString();
            }

        }
    }

    [Serializable]
    public class MagazineDetail
    {
        public Int32 MagazineDetailID { get; set; }
        public Int32 MagazineTypeID { get; set; }
        public string MagazineType { get; set; }
        public string MagazineEdition { get; set; }
        public Int32 Qty { get; set; }
        public Int32 Price { get; set; }
        public Int32 Amount { get; set; }

        public MagazineDetail() { }

        public MagazineDetail(Int32 magazineDetailID, Int32 magazineTypeID, string magazineType, string magazineEdition, Int32 qty, Int32 price, Int32 amount)
        {
            this.MagazineDetailID = magazineDetailID;
            this.MagazineTypeID = magazineTypeID;
            this.MagazineType = magazineType;
            this.MagazineEdition = magazineEdition;
            this.Qty = qty;
            this.Price = price;
            this.Amount = amount;
        }
    }
}