﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class listdepartment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                bounddatagrid();
                alert_danger.Visible = false;
                String popup_url = "openWindowModal('entrydepartment.aspx?flag=1', 700, 400)";
                btn_add.Attributes.Add("onclick", popup_url);
            }
        }

        private void bounddatagrid()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_department(userData.OrgID, userData.SiteID);

            gvw_data.DataSource = dt;
            gvw_data.DataBind();
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String DepartmentCode = DataBinder.Eval(e.Row.DataItem, "DepartmentCode").ToString();
                String DepartmentID = DataBinder.Eval(e.Row.DataItem, "DepartmentID").ToString();
                LinkButton lnk_departmentcode = (LinkButton)e.Row.FindControl("lnk_departmentcode");
                lnk_departmentcode.CommandArgument = DepartmentID;
                lnk_departmentcode.Text = DepartmentCode;
                String popup_url = "openWindowModal('entrydepartment.aspx?flag=1&DepartmentID=" + DepartmentID + "', 700, 400)";
                lnk_departmentcode.Attributes.Add("onclick", popup_url);

                Label lbl_no = (Label)e.Row.FindControl("lbl_no");
                Int32 no_urut = e.Row.RowIndex + 1;
                no_urut = gvw_data.PageIndex * gvw_data.PageSize + no_urut;
                lbl_no.Text = no_urut.ToString();
            }
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_edit")
            {
                bounddatagrid();
            }
        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }   

        protected void btn_add_Click(object sender, EventArgs e)
        {
            bounddatagrid();
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 DepartmentID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());                    
                    if (dbclass.delete_department(DepartmentID) != 0)
                    {
                        alert_danger_text.Text = "Cannot Delete Department!";
                        alert_danger.Visible = true;
                    }
                    else
                        alert_danger.Visible = false;
                    
                }
            }
            bounddatagrid();
        }     

        protected void btn_closeerror_Click(object sender, EventArgs e)
        {
            alert_danger.Visible = false;
        }
    }
}