﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

using residential.web.model;


namespace residential.web.forms
{
    public partial class viewunregistersmartcard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                load_vehicle_type();

                ViewState["UserData"] = new UserData(User.Identity.Name);
                //show_case(Request.QueryString["CaseNumber"]);
                //ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                if (Request.QueryString["CaseNumber"] != null)
                    show_case(Request.QueryString["CaseNumber"].ToString());

                //bound_smartcardhistory();
                bound_smartcardtransaction();
            }
            

        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_Case(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                //retrieve_smartcard_history(dt.Rows[0]["PSCode"].ToString());
                retrieve_smartcard_transaction(dt.Rows[0]["CaseNumber"].ToString());

            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnk_detail = e.Row.FindControl("lnk_detail") as HyperLink;
                String cardnumber = DataBinder.Eval(e.Row.DataItem, "CardNumber").ToString();
                lnk_detail.Attributes["onclick"] = "showModalFormDetailID(" + cardnumber + ");";

                //Label IsActive = e.Row.FindControl("IsActive") as Label;
                //string lbl_IsActive = "";
                //lbl_IsActive = IsActive.Text.ToLower();

                //if (lbl_IsActive != "active")
                //{

                //    e.Row.BackColor = System.Drawing.Color.Tomato;

                //    CheckBox chkbox = e.Row.FindControl("chk_select") as CheckBox;
                //    chkbox.Enabled = false;
                //}                
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            //bound_smartcardhistory();
            bound_smartcardtransaction();
        }

        private void bound_smartcardhistory()
        {
            gvw_data.DataSource = Session["SmartCardHistory"];
            gvw_data.DataBind();

            if (Session["SmartCardHistory"] != null)
            {
                //lnkbtn_unregister.Enabled = true;
                //lnkbtn_unregister.Style.Add("opacity", "1");
            }

        }

        private void bound_smartcardtransaction()
        {
            gvw_data.DataSource = Session["SmartCardTransaction"];
            gvw_data.DataBind();

            if (Session["SmartCardTransaction"] != null)
            {
                //lnkbtn_unregister.Enabled = true;
                //lnkbtn_unregister.Style.Add("opacity", "1");
            }

        }

        public void retrieve_smartcard_history(string pscode)
        {
            Session["SmartCardHistory"] = null;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_smartcard_history(pscode);

            List<SmartCardHistory> list = new List<SmartCardHistory>();

            foreach (DataRow dr in dt.Rows)
            {
                SmartCardHistory obj = new SmartCardHistory();

                obj.OrgID = (int)dr["OrgID"];
                obj.SiteID = (int)dr["SiteID"];
                obj.SmartCardDetailID = (int)dr["SmartCardDetailID"];
                obj.CaseNumber = dr["CaseNumber"].ToString();
                obj.CardTypeID = (dr["CardTypeID"].ToString()=="" ? (int?)null : (int)dr["CardTypeID"]);
                obj.CardNumber = dr["CardNumber"].ToString();
                obj.ChipNumber = dr["ChipNumber"].ToString();
                obj.HolderName = dr["HolderName"].ToString();
                obj.VehicleNumber = dr["VehicleNumber"].ToString();
                obj.BoomGateID = (int)dr["BoomGateid"];
                obj.BoomGateName = dr["BoomGateName"].ToString();
                obj.CreatedDate = (DateTime)dr["CreatedDate"];
                obj.CreatedBy = dr["CreatedBy"].ToString();

                list.Add(obj);
            }

            Session["SmartCardHistory"] = list;
        }

        public void retrieve_smartcard_transaction(string casenumber)
        {
            Session["SmartCardTransaction"] = null;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_smartcard_transaction_bycase(casenumber);

            List<SmartcardTransaction> list = new List<SmartcardTransaction>();

            foreach (DataRow dr in dt.Rows)
            {
                SmartcardTransaction obj = new SmartcardTransaction();
                obj.SmartCardID = (int)dr["SmartCardID"];
                obj.CardNumber = dr["CardNumber"].ToString();
                obj.ChipNumber = dr["ChipNumber"].ToString();
                obj.BoomGateName = dr["BoomGateName"].ToString();
                obj.BoomGateID = (int)dr["BoomGateid"];

                list.Add(obj);
            }

            Session["SmartCardTransaction"] = list;
        }

        protected void btn_showdetail_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            string cardnumber = hdf_cardnumber.Value;

            DataTable dt = dbclass.retrieve_membershipcard_detail(cardnumber);

            if (dt.Rows[0] != null)
            {
                txt_cardnumber.Text = dt.Rows[0]["CardNumber"].ToString();
                txt_holdername.Text = dt.Rows[0]["HolderName"].ToString();
                txt_chipnumber.Text = dt.Rows[0]["ChipNumber"].ToString();
                txt_vehiclenumber.Text = dt.Rows[0]["VehicleNumber"].ToString();
                ddl_vehicletype.SelectedValue = dt.Rows[0]["VehicleType"].ToString();
                txt_vehiclebrand.Text = dt.Rows[0]["VehicleBrand"].ToString();
            }
            
        }

        protected void load_vehicle_type()
        {
            VehicleType mobil = new VehicleType();
            mobil.VehicleTypeID = 1;
            mobil.VehicleTypeName = "Mobil";

            VehicleType motor = new VehicleType();
            motor.VehicleTypeID = 2;
            motor.VehicleTypeName = "Motor";

            List<VehicleType> list_vehicle_type = new List<VehicleType>();
            list_vehicle_type.Add(mobil);
            list_vehicle_type.Add(motor);

            ddl_vehicletype.DataValueField = "VehicleTypeID";
            ddl_vehicletype.DataTextField = "VehicleTypeName";
            ddl_vehicletype.DataSource = list_vehicle_type;
            ddl_vehicletype.DataBind();

        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("unregistersmartcard.aspx");
        }

        
    }
}