﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using System.Globalization;

namespace residential.web.forms
{
    public partial class BannerReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                ShowType();
                BindSelection();
                boundBookedTable();
            }
        }

        public void BindSelection()
        {
            int Year = DateTime.Now.Year;

            DataTable dt = new DataTable();

            dt.Columns.Add("Text");
            dt.Columns.Add("Value");

            int row = 0;

            for (int i = Year - 1; i <= Year + 2; i++)
            {
                for (int a = 1; a <= 12; a++)
                {
                    string Month = ("0" + a.ToString()).Substring(("0" + a.ToString()).Length - 2, 2);

                    dt.Rows.Add(row);
                    dt.Rows[row]["Text"] = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(a) + " " + i.ToString();
                    dt.Rows[row]["Value"] = i.ToString() + "-" + Month + "-01";
                    row = row + 1;
                }
            }

            ddlMonth.DataSource = dt;
            ddlMonth.DataTextField = "Text";
            ddlMonth.DataValueField = "Value";
            ddlMonth.DataBind();
        }

        protected void boundBookedTable()
        {
            if (ddlType.SelectedIndex > -1)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DateTime startRange = DateTime.Parse(ddlMonth.SelectedValue.ToString());

                DataTable dt = dbclass.retrieve_location(userData.OrgID, userData.SiteID, Int32.Parse(ddlType.SelectedValue));

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Label text = new Label();
                    text.CssClass = "Form-Control";
                    text.Text = dt.Rows[i]["BannerLocationName"].ToString().ToUpper() + " - Rp." + decimal.Parse(dt.Rows[i]["Price"].ToString()).ToString("N0");
                    text.Font.Bold = true;
                    text.Font.Size = 14;
                    text.Font.Underline = true;

                    divTable.Controls.Add(text);

                    Table tbl_bookedBanner = new Table();
                    tbl_bookedBanner.CssClass = "table table-bordered";

                    DataTable dt_point = dbclass.retrieve_point(int.Parse(dt.Rows[i]["BannerLocationID"].ToString()));

                    //Clear recent table
                    tbl_bookedBanner.Rows.Clear();

                    //table header -> date
                    TableHeaderRow headerDate = new TableHeaderRow();

                    TableHeaderCell th_title = new TableHeaderCell();
                    th_title.Text = "Point / Date";
                    headerDate.Cells.Add(th_title);

                    int days = DateTime.DaysInMonth(startRange.Year, startRange.Month);

                    for (int x = 0; x < days; x++)
                    {
                        TableHeaderCell thc = new TableHeaderCell();
                        thc.Text = startRange.AddDays(x).ToString("dd");
                        headerDate.Cells.Add(thc);
                    }

                    tbl_bookedBanner.Rows.Add(headerDate);

                    DataTable dt_bannerDetail = dbclass.retrieve_booked_banner(dt.Rows[i]["BannerLocationID"].ToString(), startRange.ToString());

                    //Row -> Point
                    foreach (DataRow row in dt_point.Rows)
                    {
                        TableRow tr = new TableRow();
                        TableCell tp = new TableCell();
                        tp.Text = row["PointName"].ToString();
                        tr.Cells.Add(tp);
                        String PointID = row["PointID"].ToString();

                        DataRow[] nPoint = dt_bannerDetail.Select("PointID = " + PointID);

                        string Remarks = string.Empty;

                        for (int j = 0; j < days; j++)
                        {
                            bool booked = false;
                            foreach (DataRow rowPoint in nPoint)
                            {
                                DateTime startDate = DateTime.Parse(rowPoint["StartDate"].ToString());
                                DateTime endDate = DateTime.Parse(rowPoint["EndDate"].ToString());
                                if (startRange.AddDays(j).Date >= startDate && startRange.AddDays(j).Date <= endDate)
                                {
                                    booked = true;
                                    Remarks = rowPoint["Remarks"].ToString();
                                }
                            }
                            TableCell tp2 = new TableCell();
                            tp2.BackColor = (booked) ? System.Drawing.Color.Red : System.Drawing.Color.Green;
                            tp2.ToolTip = (booked) ? Remarks : "";
                            Remarks = "";
                            tr.Cells.Add(tp2);
                        }
                        tbl_bookedBanner.Rows.Add(tr);
                    }
                    divTable.Controls.Add(tbl_bookedBanner);
                }
            }
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            boundBookedTable();
        }

        public void ShowType()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_banner_type();
            ddlType.DataSource = dt;
            ddlType.DataTextField = "BannerTypeName";
            ddlType.DataValueField = "BannerTypeID";
            ddlType.DataBind();
        }

    }
}