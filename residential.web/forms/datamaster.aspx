﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="datamaster.aspx.cs" Inherits="residential.web.forms.datamaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:TabContainer runat="server" ID="Tabs" Height="100%" ActiveTabIndex="0" Width="100%">
                <asp:TabPanel runat="server" ID="Panel1" HeaderText="Banner">
                    <ContentTemplate>
                        <div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/forms/listbannertype.aspx"
                                    CssClass="contentlink">Banner Type</asp:HyperLink>
                                <br />
                                Setup all parameters to use in application
                            </div>
                            <div class="innerlinkcell">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/forms/listbannerlocation.aspx"
                                    CssClass="contentlink">Banner Location</asp:HyperLink>
                                <br />
                                Setup all parameters to use in application
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Magazine">
                    <ContentTemplate>
                        <div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/forms/listmagazinetrxtype.aspx"
                                    CssClass="contentlink">Magazine Type</asp:HyperLink>
                                <br />
                                Setup all parameters to use in application
                            </div>                            
                        </div>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="SMS Blast">
                    <ContentTemplate>
                        <div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/forms/listsmssender.aspx"
                                    CssClass="contentlink">SMS Sender</asp:HyperLink>
                                <br />
                                Setup all parameters to use in application
                            </div>                            
                        </div>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel4" HeaderText="BCD">
                    <ContentTemplate>
                        <div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/forms/listbcdprice.aspx"
                                    CssClass="contentlink">BCD Price</asp:HyperLink>
                                <br />
                                Setup all parameters to use in application
                            </div>                            
                        </div>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel5" HeaderText="WTP">
                    <ContentTemplate>
                        <div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image10" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/forms/listsite.aspx"
                                    CssClass="contentlink">WTP Categories</asp:HyperLink>
                                <br />
                                Manage roles for application
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel6" HeaderText="KTP KK">
                    <ContentTemplate>
                        <div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image11" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/forms/listsite.aspx"
                                    CssClass="contentlink">Zone</asp:HyperLink>
                                <br />
                                Manage roles for application
                            </div>                            
                        </div>       
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Car Rent">
                    <ContentTemplate>
                        <%--<div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/forms/listsite.aspx"
                                    CssClass="contentlink">Sites</asp:HyperLink>
                                <br />
                                Manage roles for application
                            </div>
                            <div class="innerlinkcell">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/forms/listdepartment.aspx"
                                    CssClass="contentlink">Department</asp:HyperLink>
                                <br />
                                Manage users for application
                            </div>
                        </div>  --%>                      
                        <div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="~/forms/listcarlicenseplate.aspx"
                                    CssClass="contentlink">Car License Plate</asp:HyperLink>
                                <br />
                                Setup all parameters to use in application
                            </div>
                            <div class="innerlinkcell">
                                <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="~/forms/listcarrenttariff.aspx"
                                    CssClass="contentlink">Car Rent Tariff </asp:HyperLink>
                                <br />
                                Setup all parameters to use in application
                            </div>
                        </div>                        
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel7" HeaderText="Others">
                    <ContentTemplate>
                        <%--<div class="innerlinkcontent">
                            <div class="innerlinkcell">
                                <asp:Image ID="Image12" runat="server" ImageUrl="~/images/icon-setup.png" CssClass="innerlinkicon" />
                                <br />
                                <asp:HyperLink ID="HyperLink12" runat="server" NavigateUrl="~/forms/listsite.aspx"
                                    CssClass="contentlink">Zone</asp:HyperLink>
                                <br />
                                Manage roles for application
                            </div>                            
                        </div>--%>       
                    </ContentTemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
