﻿using residential.libs;
using residential.libs.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class EntryResidentData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["RHI"] != null && Request.QueryString["U"] != null)
                {
                    txtOwnerPsCode.Attributes.Add("readonly", "readonly");
                    txtTenantPsCode.Attributes.Add("readonly", "readonly");
                    txtIsActive.Attributes.Add("readonly", "readonly");
                    txtBirthDate.Attributes.Add("readonly", "readonly");

                    ddlResidentStatus.DataSource = clsResident.ResidentStatus();
                    ddlResidentStatus.DataTextField = "ResidentStatusDesc";
                    ddlResidentStatus.DataValueField = "ResidentStatusID";
                    ddlResidentStatus.DataBind();

                    ddlNationality.DataSource = clsResident.Nationality();
                    ddlNationality.DataTextField = "nationality";
                    ddlNationality.DataValueField = "nationID";
                    ddlNationality.DataBind();

                    lblTitle.Text = Request.QueryString["U"].ToString();
                    hfRHI.Value = Request.QueryString["RHI"].ToString();

                    ViewState["UserData"] = new UserData(User.Identity.Name);

                    UserData userData = (UserData)ViewState["UserData"];

                    DataSet ds = clsResident.ResidentDetail(int.Parse(hfRHI.Value.ToString()));

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtOwnerPsCode.Text = ds.Tables[0].Rows[0]["OwnerName"].ToString();
                        txtTenantPsCode.Text = ds.Tables[0].Rows[0]["TenantName"].ToString();
                        txtIsActive.Text = ds.Tables[0].Rows[0]["isActive"].ToString();
                    }
                    gvw_data.DataSource = null;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        gvw_data.DataSource = ds.Tables[1];
                    }
                    gvw_data.DataBind();

                }
                else
                {
                    Response.Redirect("~/404_pagenotfound.aspx");
                }
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String ResidentDetailID = DataBinder.Eval(e.Row.DataItem, "ResidentDetailID").ToString();
                string Name = DataBinder.Eval(e.Row.DataItem, "Name").ToString();
                string BirthDate = DataBinder.Eval(e.Row.DataItem, "BirthDate").ToString();                
                string NationalityID = DataBinder.Eval(e.Row.DataItem, "NationalityID").ToString();
                string ResidentStatusID = DataBinder.Eval(e.Row.DataItem, "ResidentStatusID").ToString();
                string isActive = DataBinder.Eval(e.Row.DataItem, "isActive").ToString();

                if (isActive == "True")
                {
                    isActive = "1";
                }
                else
                {
                    isActive = "0";
                }

                string Gender = DataBinder.Eval(e.Row.DataItem, "Gender").ToString();
                //e.Row.Attributes.Add("onclick", "Confirm('" + ResidentDetailID + "', '" + Name + "', '" + DateTime.Parse(BirthDate).ToString("dd/MM/yyyy") + "', '"
                //    + Gender + "', '" + ResidentStatusID + "', '" + NationalityID + "', '" + isActive + "');");

                HyperLink hpl_editResidentMember = (HyperLink)e.Row.FindControl("lnk_edit");
                hpl_editResidentMember.Attributes.Add("onclick", "Confirm('" + ResidentDetailID + "', '" + Name + "', '" + DateTime.Parse(BirthDate).ToString("dd MMMM yyyy") + "', '" + Gender + "', '" + ResidentStatusID + "', '" + NationalityID + "', '" + isActive + "');");
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (txtName.Text.Length > 2 && txtBirthDate.Text.Length > 3)
            {
                try
                {
                    string Result = "";

                    UserData userData = (UserData)ViewState["UserData"];

                    if (hfResidentDetailID.Value.ToString() == "")
                    {
                        Result = clsResident.InsertResidentDetail(int.Parse(hfRHI.Value.ToString()), txtName.Text.ToString(),
                            DateTime.Parse(txtBirthDate.Text), ddlGender.SelectedValue.ToString(),
                            int.Parse(ddlResidentStatus.SelectedValue.ToString()), ddlNationality.SelectedValue.ToString(),
                            int.Parse(ddlActive.SelectedValue.ToString()), userData.UserName);
                    }
                    else
                    {
                        Result = clsResident.UpdateResidentDetail(int.Parse(hfRHI.Value.ToString()), txtName.Text.ToString(),
                            DateTime.Parse(txtBirthDate.Text), ddlGender.SelectedValue.ToString(),
                            int.Parse(ddlResidentStatus.SelectedValue.ToString()), ddlNationality.SelectedValue.ToString(),
                            int.Parse(ddlActive.SelectedValue.ToString()), userData.UserName, int.Parse(hfResidentDetailID.Value.ToString()));
                    }

                    if (Result == "1")
                    {
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "page redirect", "alert('Resident Data Saved'); window.location='" + Request.Url.AbsoluteUri + "';", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(1,'Resident Data Saved');", true);
                    }
                    else
                    {
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('" + Result.ToString() + "');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(0,'" + Result.ToString() + "');", true);
                    }
                }
                catch (Exception ex)
                {
                    errID.Visible = true;
                    lbl_error.Text = ex.Message.ToString();
                }

            }
            else
            {
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('Please Fill All Required Field *');", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "show_message_dialog", "show_message_dialog(0,'Please Fill All Required Field *');", true);
            }

        }
    }
}