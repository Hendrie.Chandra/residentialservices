﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;


namespace residential.web.forms
{
    public partial class entrybannertype : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                alert_danger.Visible = false;
                if (Request.QueryString["BannerTypeID"] != null)
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    int BannerTypeID = Int32.Parse(Request.QueryString["BannerTypeID"].ToString());
                    DataTable dt = dbclass.retrieve_banner_type(BannerTypeID);

                    DataRow datarow = dt.Rows[0];
                    Lbl_bannerid.Text = Request.QueryString["BannerTypeID"].ToString();
                    txt_bannertypename.Text = datarow["BannerTypeName"].ToString();
                }
            }
            alert_danger.Visible = false;
        }

        private Int32 save_banner_type()
        {            
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = new UserData(User.Identity.Name);

            Int32 returnvalue = 0;
            if (!String.IsNullOrEmpty(Lbl_bannerid.Text))
            {
                returnvalue = dbclass.save_banner_type(Int32.Parse(Lbl_bannerid.Text), txt_bannertypename.Text, userData.UserName);
            }
            else
            {
                returnvalue = dbclass.save_banner_type(0, txt_bannertypename.Text, userData.UserName);
            }
            return returnvalue;
        }

        protected Boolean sites_validation()
        {
            if (String.IsNullOrEmpty(txt_bannertypename.Text))
            {
                alert_danger_text.Text = "banner type code is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_banner_type();
                Response.Redirect("listbannertype.aspx");                
            }
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("listbannertype.aspx");
        }     
    }
}