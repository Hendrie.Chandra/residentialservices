﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using System.Drawing;
using residential.web.Classes;

namespace residential.web.forms
{
    public class DetailKTP_KK
    {
        public Int32 OrgID { get; set; }
        public Int32 SiteID { get; set; }
        public Int32 PersonalIdentificationID { get; set; }
        public Int32 PersonalIdentificationDetailID { get; set; }
        public string Address { get; set; }
        public string Kelurahan { get; set; }
        public string Kecamatan { get; set; }
        public string Name { get; set; }
        public Int32 GenderID { get; set; }
        public string Gender { get; set; }
        public Int32 RelationID { get; set; }
        public string Relation { get; set; }
        public string BirthPlace { get; set; }
        public DateTime BirthDate { get; set; }
        public string StateCountry { get; set; }
        public Int32 MaritalStatusID { get; set; }
        public string MaritalStatus { get; set; }
        public string BloodType { get; set; }
        public Int32 BloodTypeID { get; set; }
        public Int32 ReligionID { get; set; }
        public string Religion { get; set; }
        public Int32 LastEducationID { get; set; }
        public string LastEducation { get; set; }
        public Int32 OccupationID { get; set; }
        public string Occupation { get; set; }
        public string ParentName { get; set; }
        public string SBKRINotes { get; set; }
        public string Notes { get; set; }

        public DetailKTP_KK() { }

        public DetailKTP_KK(Int32 OrgID, Int32 SiteID, Int32 PersonalIdentificationID, Int32 PersonalIdentificationDetailID, string Address, string Kelurahan, string Kecamatan, string Name,
            Int32 GenderID, string Gender, Int32 RelationID, string BirthPlace, DateTime BirthDate, string StateCountry,
            Int32 MaritalStatusID, Int32 BloodTypeID, Int32 ReligionID, Int32 LastEducation, Int32 OccupationID,
            string ParentName, string SBKRINotes, string Notes)
        {
            this.OrgID = OrgID;
            this.SiteID = SiteID;
            this.PersonalIdentificationDetailID = PersonalIdentificationDetailID;
            this.PersonalIdentificationID = PersonalIdentificationID;
            this.Address = Address;
            this.Kelurahan = Kelurahan;
            this.Kecamatan = Kecamatan;
            this.Name = Name;
            this.GenderID = GenderID;
            this.Gender = Gender;
            this.RelationID = RelationID;
            this.BirthPlace = BirthPlace;
            this.BirthDate = BirthDate;
            this.StateCountry = StateCountry;
            this.MaritalStatusID = MaritalStatusID;
            this.BloodTypeID = BloodTypeID;
            this.ReligionID = ReligionID;
            this.LastEducationID = LastEducation;
            this.OccupationID = OccupationID;
            this.ParentName = ParentName;
            this.SBKRINotes = SBKRINotes;
            this.Notes = Notes;
        }
    }

    public partial class entryktp_kk : System.Web.UI.Page
    {
        static String prevPage = "ktp_kk.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                Session["ListDetailKTP_KK"] = null;
                Session["PersonalIdentificationID"] = null;

                hitung_total();
                show_zone();
                Int32 discount = 0;
                txt_discount.Text = discount.ToString();

                cboRequest_SelectedIndexChanged(this, EventArgs.Empty);                               

                gvw_data.DataBind();

                hiddenID.Value = "-1";

                show_cbo_Gender();
                show_cbo_BloodType();
                show_cbo_LastEducation();
                show_cbo_MaritalStatus();
                show_cbo_Occupation();
                show_cbo_Relation();
                show_cbo_Religion();

                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            entryheader.ToggleAlert = false;
        }

        private void Show_Case(String _casenumber)
        {
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                DataTable dt = dbclass.retrieve_personal_identification_by_casenumber(_casenumber);

                Int32 caseStatusId = Int32.Parse(dt.Rows[0]["CaseStatusID"].ToString());
                if (caseStatusId != 1)                
                {
                    entryheader.disable_all_controls();

                    cboZone.Enabled = false;
                    txtJumlahKTP.Enabled = false;
                    txtJumlahKK.Enabled = false;                    
                }

                entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                //check customer type -> 0 = Owner, 1 = Tenant
                entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheader.combobox_unit();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

                entryheader.disable_all_controls();

                DataTable dtHelp = dbclass.retrieve_help((Int32)dt.Rows[0]["HelpID"]);
                String BannerType = dtHelp.Rows[0]["HelpName"].ToString();

                if (BannerType == ConfigurationManager.AppSettings["NewPersonalIdentificationHelpID_KTP"])
                    cboRequest.SelectedValue = "KTP";
                else if (BannerType == ConfigurationManager.AppSettings["NewPersonalIdentificationHelpID_KK"])
                    cboRequest.SelectedValue = "KK";

                cboZone.SelectedIndex = Int32.Parse(dt.Rows[0]["ZoneID"].ToString());
                cboZone_SelectedIndexChanged(this, EventArgs.Empty);

                txtJumlahKTP.Text = dt.Rows[0]["Qty_KTP"].ToString();
                txtJumlahKK.Text = dt.Rows[0]["Qty_KK"].ToString();

                Int32 discount = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString()));
                Int32 subtotal = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Amount"].ToString()));
                Int32 total = Convert.ToInt32(decimal.Parse(dt.Rows[0]["AmountAfterDiscount"].ToString()));

                txt_discount.Text = discount.ToString();
                lbl_adjustment.Text = discount.ToString();
                lbl_subtotal.Text = subtotal.ToString("N0");
                lbl_total.Text = total.ToString("N0");

                Session["PersonalIdentificationID"] = dt.Rows[0]["PersonalIdentificationID"].ToString();
                hiddenID.Value = dt.Rows[0]["PersonalIdentificationID"].ToString();

                dt = dbclass.retrieve_personal_identification_detail(Int32.Parse(hiddenID.Value.ToString()));

                List<DetailKTP_KK> list = new List<DetailKTP_KK>();
                DetailKTP_KK det = new DetailKTP_KK();

                foreach (DataRow dr in dt.Rows)
                {
                    det = new DetailKTP_KK();
                    det.PersonalIdentificationDetailID = Int32.Parse(dr["PersonalIdentificationDetailID"].ToString());
                    det.Address = dr["Address"].ToString();
                    det.Kelurahan = dr["Kelurahan"].ToString();
                    det.Kecamatan = dr["Kecamatan"].ToString();
                    det.Name = dr["Name"].ToString();
                    det.GenderID = Int32.Parse(dr["GenderID"].ToString());
                    det.Gender = dr["GenderName"].ToString();
                    det.RelationID = Int32.Parse(dr["RelationID"].ToString());
                    det.BirthPlace = dr["BirthPlace"].ToString();
                    det.BirthDate = DateTime.Parse(dr["BirthDate"].ToString());
                    det.StateCountry = dr["StateCountry"].ToString();
                    det.MaritalStatusID = Int32.Parse(dr["MaritalStatusID"].ToString());
                    det.BloodTypeID = Int32.Parse(dr["BloodTypeID"].ToString());
                    det.ReligionID = Int32.Parse(dr["ReligionID"].ToString());
                    det.LastEducationID = Int32.Parse(dr["LastEducationID"].ToString());
                    det.OccupationID = Int32.Parse(dr["OccupationID"].ToString());
                    det.ParentName = dr["ParentName"].ToString();
                    det.SBKRINotes = dr["SBKRINotes"].ToString();
                    det.Notes = dr["Notes"].ToString();

                    list.Add(det);
                }
                Session["ListDetailKTP_KK"] = list;

                show_detail();

                cboRequest.Enabled = false;
            }
            catch (Exception)
            {
                throw new HttpException(404, "Page not found");
            }
        }

        private void show_zone()
        {
            UserData userData = (UserData)ViewState["UserData"];

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_zone(userData.OrgID, userData.SiteID);
            cboZone.DataSource = dt;
            cboZone.DataTextField = "ZoneName";
            cboZone.DataValueField = "ZoneID";
            cboZone.DataBind();
            cboZone.Items.Insert(0, new ListItem("None", "0"));
            cboZone.SelectedIndex = 0;
        }

        private void show_detail()
        {
            List<DetailKTP_KK> list = null;
            if (Session["ListDetailKTP_KK"] != null)
                list = (List<DetailKTP_KK>)Session["ListDetailKTP_KK"];

            gvw_data.DataSource = list;
            gvw_data.DataBind();
        }

        protected void btn_add_detail_Click(object sender, EventArgs e)
        {
            show_detail();
            cboRequest.Enabled = (gvw_data.Rows.Count > 0) ? false : true;

            btnRefresh_Click(this, EventArgs.Empty);

            reset_detail();
        }

        private void reset_detail()
        {
            alert_danger.Visible = false;
            txtPersonalIdentificationDetailID.Value = null;
            txtNamaLengkap.Text = null;
            cboJenisKelamin.SelectedIndex = 0;
            cboRelation.SelectedIndex = 0;
            txtTempatLahir.Text = null;
            txtTglLahir.Text = null;
            txtAlamat.Text = null;
            txtKelurahan.Text = null;
            txtKecamatan.Text = null;
            txtPropinsiNegara.Text = null;
            cboStatusPernikahan.SelectedIndex = 0;
            cboGolDarah.SelectedIndex = 0;
            cboAgama.SelectedIndex = 0;
            cboPendidikan.SelectedIndex = 0;
            cboPekerjaan.SelectedIndex = 0;
            txtNamaOrangTua.Text = null;
            txtKeterangan.Text = null;
            txtWNRI.Text = null;
        }

        private void hitung_total()
        {
            if (String.IsNullOrWhiteSpace(txtJumlahKTP.Text) || String.IsNullOrWhiteSpace(txtJumlahKK.Text) || String.IsNullOrWhiteSpace(txt_discount.Text))
            {
                txtJumlahKTP.Text = "1";
                txtJumlahKK.Text = "0";
                txt_discount.Text = "0";
                lbl_subtotal.Text = "0";
                lbl_total.Text = "";
            }

            Int32 subtotal = (Int32.Parse(txtJumlahKTP.Text) * (Int32.Parse(lblPriceKTP.Text, NumberStyles.Currency)) + (Int32.Parse(txtJumlahKK.Text) * (Int32.Parse(lblPriceKK.Text, NumberStyles.Currency))));
            Int32 total = subtotal - Int32.Parse(txt_discount.Text);            

            lbl_adjustment.Text = txt_discount.Text;
            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_total.Text = total.ToString("N0");
        }

        protected void txtJumlahKK_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void txtJumlahKTP_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected bool transaction_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }            
            if (cboZone.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Zone is required";                
                return false;
            }
            if (gvw_data.Rows.Count == 0)
            {
                entryheader.ShowAlert = "No data added";                
                return false;
            }            
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String CaseNumber = save_transaction();

                if (Request.QueryString["CaseNumber"] == null)
                {
                    string url = HttpContext.Current.Request.Url.AbsoluteUri;
                    Response.Redirect(url + (url.IndexOf("?") == -1 ? "?" : "&") + "CaseNumber=" + CaseNumber);
                }
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            String ParameterHelpID = "";
            if (cboRequest.SelectedValue == "KTP")
                ParameterHelpID = ConfigurationManager.AppSettings["NewPersonalIdentificationHelpID_KTP"].ToString();
            else
                ParameterHelpID = ConfigurationManager.AppSettings["NewPersonalIdentificationHelpID_KK"].ToString();

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ParameterHelpID);
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = entryheader.CaseOriginID;            
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);
            Int32 QuantityKTP = Int32.Parse(txtJumlahKTP.Text);
            Int32 QuantityKK = Int32.Parse(txtJumlahKK.Text);
            Int32 PriceKTP = Int32.Parse(lblPriceKTP.Text, NumberStyles.Currency);
            Int32 PriceKK = Int32.Parse(lblPriceKK.Text, NumberStyles.Currency);
            Int32 TotalAmount = (QuantityKTP * PriceKTP) + (QuantityKK * PriceKK);

            Int32 DiscountTypeID = 0;
            Int32 TotalDiscount = Int32.Parse(txt_discount.Text, NumberStyles.Currency);
            Int32 TotalAmountAfterDiscount = TotalAmount - TotalDiscount;
            Double TaxPercentage = 0.1;
            Int32 TaxAmount = 0;
            Int32 TotalAmountAfterTax = TotalAmountAfterDiscount - TaxAmount;
            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                //request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_personal_identification_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, Int32.Parse(cboZone.SelectedValue), QuantityKTP, QuantityKK, TotalAmount, DiscountTypeID, TotalDiscount, TotalAmountAfterDiscount,
                    TaxPercentage, TaxAmount, TotalAmountAfterTax, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    dt = dbclass.retrieve_personal_identification_by_casenumber(CaseNumber);

                    Int32 PersonalIdentificationID = Int32.Parse(dt.Rows[0]["PersonalIdentificationID"].ToString());
                    hiddenID.Value = PersonalIdentificationID.ToString();
                    Session["PersonalIdentificationID"] = PersonalIdentificationID.ToString();

                    if (ViewState["DeletedItems"] != null)
                    {
                        List<Int32> DeletedItems = (List<Int32>)ViewState["DeletedItems"];
                        for (var i = 0; i < DeletedItems.Count; i++)
                            dbclass.delete_personal_identification_detail(DeletedItems[i]);
                    }

                    foreach (DetailKTP_KK det in (List<DetailKTP_KK>)Session["ListDetailKTP_KK"])
                        dbclass.save_personal_identification_detail_transaction(
                            OrgID, SiteID, PersonalIdentificationID, det.PersonalIdentificationDetailID,
                            det.Address, det.Kelurahan, det.Kecamatan, det.Name, det.GenderID, det.RelationID,
                            det.BirthPlace, det.BirthDate, det.StateCountry, det.MaritalStatusID, det.BloodTypeID,
                            det.ReligionID, det.LastEducationID, det.OccupationID, det.ParentName, det.SBKRINotes,
                            det.Notes);

                    Session["ListDetailKTP_KK"] = null;

                    cboRequest.Enabled = false;

                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnk_edit = e.Row.FindControl("lnk_edit") as HyperLink;
                String PersonalIdentificationDetailID = DataBinder.Eval(e.Row.DataItem, "PersonalIdentificationDetailID").ToString();
                lnk_edit.Attributes["onclick"] = "showModalFormDetailID("+ PersonalIdentificationDetailID +");";
            }
        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var list = (List<DetailKTP_KK>)Session["ListDetailKTP_KK"];

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 PersonalIdentificationDetailID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());

                    if (ViewState["DeletedItems"] != null)
                    {
                        List<Int32> DeletedItems = (List<Int32>)ViewState["DeletedItems"];
                        DeletedItems.Add(PersonalIdentificationDetailID);

                        ViewState["DeletedItems"] = DeletedItems;
                    }
                    else
                        ViewState["DeletedItems"] = new List<Int32> { PersonalIdentificationDetailID };

                    var itemToRemove = list.SingleOrDefault(r => r.PersonalIdentificationDetailID == PersonalIdentificationDetailID);
                    if (itemToRemove != null)
                        list.Remove(itemToRemove);
                }
            }

            Session["ListDetailKTP_KK"] = list;

            show_detail();
        }

        protected void cboZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboZone.SelectedIndex != 0)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DataTable dt = dbclass.retrieve_generalprice("KTP Baru", userData.SiteID, Int32.Parse(cboZone.SelectedValue.ToString()));
                Int32 price = Convert.ToInt32(dt.Rows[0]["Price"]);
                lblPriceKTP.Text = price.ToString("N0");

                dt = dbclass.retrieve_generalprice("KK Baru", userData.SiteID, Int32.Parse(cboZone.SelectedValue.ToString()));
                price = Convert.ToInt32(dt.Rows[0]["Price"]);
                lblPriceKK.Text = price.ToString("N0");

                hitung_total();
            }
            else
            {
                lblPriceKTP.Text = "0";
                lblPriceKK.Text = "0";
            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            show_detail();
        }

        public void reset_fields(ControlCollection ctls)
        {
            foreach (Control c in ctls)
            {
                if (c is TextBox)
                {
                    TextBox tt = c as TextBox;
                    tt.Text = null;
                }
                else if (c is AjaxControlToolkit.ComboBox)
                {
                    AjaxControlToolkit.ComboBox dd = c as AjaxControlToolkit.ComboBox;
                    if (dd.Items.Count > 0)
                        dd.SelectedIndex = 0;
                }

                if (c.HasControls())
                {
                    reset_fields(c.Controls);
                }
            }
        }

        protected void cboRequest_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRequest.SelectedValue == "KTP")
            {
                //txtJumlahKK.Enabled = false;
                //txtJumlahKTP.Enabled = true;
                ktpform.Visible = true;
                kkform.Visible = false;
                txtJumlahKTP.Text = "1";
                txtJumlahKK.Text = "0";
            }
            else
            {
                //txtJumlahKK.Enabled = true;
                //txtJumlahKTP.Enabled = false;
                ktpform.Visible = false;
                kkform.Visible = true;
                txtJumlahKTP.Text = "0";
                txtJumlahKK.Text = "1";
            }

            hitung_total();
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        //------- KTP KK Detail ------- //

        private void show_cbo_Gender()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Gender();

            cboJenisKelamin.DataSource = dt;
            cboJenisKelamin.DataTextField = "GenderName";
            cboJenisKelamin.DataValueField = "GenderID";
            cboJenisKelamin.DataBind();
            cboJenisKelamin.Items.Insert(0, new ListItem("None", "0"));
            cboJenisKelamin.SelectedIndex = 0;
        }

        private void show_cbo_BloodType()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_BloodType();

            cboGolDarah.DataSource = dt;
            cboGolDarah.DataTextField = "BloodTypeName";
            cboGolDarah.DataValueField = "BloodTypeID";
            cboGolDarah.DataBind();
            cboGolDarah.Items.Insert(0, new ListItem("None", "0"));
            cboGolDarah.SelectedIndex = 0;
        }

        private void show_cbo_Relation()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Relation();

            cboRelation.DataSource = dt;
            cboRelation.DataTextField = "RelationName";
            cboRelation.DataValueField = "RelationID";
            cboRelation.DataBind();
            cboRelation.Items.Insert(0, new ListItem("None", "0"));
            cboRelation.SelectedIndex = 0;
        }

        private void show_cbo_MaritalStatus()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_MaritalStatus();

            cboStatusPernikahan.DataSource = dt;
            cboStatusPernikahan.DataTextField = "MaritalStatusName";
            cboStatusPernikahan.DataValueField = "MaritalStatusID";
            cboStatusPernikahan.DataBind();
            cboStatusPernikahan.Items.Insert(0, new ListItem("None", "0"));
            cboStatusPernikahan.SelectedIndex = 0;
        }

        private void show_cbo_Religion()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Religion();

            cboAgama.DataSource = dt;
            cboAgama.DataTextField = "ReligionName";
            cboAgama.DataValueField = "ReligionID";
            cboAgama.DataBind();
            cboAgama.Items.Insert(0, new ListItem("None", "0"));
            cboAgama.SelectedIndex = 0;
        }

        private void show_cbo_LastEducation()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_LastEducation();

            cboPendidikan.DataSource = dt;
            cboPendidikan.DataTextField = "LastEducationName";
            cboPendidikan.DataValueField = "LastEducationID";
            cboPendidikan.DataBind();
            cboPendidikan.Items.Insert(0, new ListItem("None", "0"));
            cboPendidikan.SelectedIndex = 0;
        }

        private void show_cbo_Occupation()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_Occupation();

            cboPekerjaan.DataSource = dt;
            cboPekerjaan.DataTextField = "OccupationName";
            cboPekerjaan.DataValueField = "OccupationID";
            cboPekerjaan.DataBind();
            cboPekerjaan.Items.Insert(0, new ListItem("None", "0"));
            cboPekerjaan.SelectedIndex = 0;
        }

        protected void btn_savedetail_Click(object sender, EventArgs e)
        {
            DateTime TglLahir;
            if (String.IsNullOrWhiteSpace(txtNamaLengkap.Text))
            {
                alert_danger_text.Text = "Nama lengkap harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (cboJenisKelamin.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Jenis kelamin harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (cboRelation.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Hubungan (KK) harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtTempatLahir.Text == "")
            {
                alert_danger_text.Text = "Tempat Lahir harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtTglLahir.Text == "")
            {
                alert_danger_text.Text = "Tanggal lahir harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            else
            {
                try
                {
                    TglLahir = DateTime.Parse(txtTglLahir.Text.ToString());
                }
                catch (Exception)
                {
                    alert_danger_text.Text = "Kesalahan format Tanggal lahir.";
                    alert_danger.Visible = true;
                    return;
                }
            }
            if (txtAlamat.Text == "")
            {
                alert_danger_text.Text = "Alamat harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtKelurahan.Text == "")
            {
                alert_danger_text.Text = "Kelurahan harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtKecamatan.Text == "")
            {
                alert_danger_text.Text = "Kecamatan harus diisi.";
                alert_danger.Visible = true;
                return;
            }                        
            if (cboStatusPernikahan.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Status pernikahan harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (cboAgama.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Agama harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (txtNamaOrangTua.Text == "")
            {
                alert_danger_text.Text = "Nama orang tua harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            if (cboPekerjaan.SelectedIndex == 0)
            {
                alert_danger_text.Text = "Pekerjaan harus diisi.";
                alert_danger.Visible = true;
                return;
            }
            alert_danger.Visible = false;

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            List<DetailKTP_KK> list = null;
            Int32 counter = 0;

            if (Session["ListDetailKTP_KK"] == null)
            {
                list = new List<DetailKTP_KK>();
                counter = -1;
            }
            else
            {
                list = (List<DetailKTP_KK>)Session["ListDetailKTP_KK"];
                if (list.Count > 0)
                {
                    Int32 maxObj = list.Min(r => r.PersonalIdentificationDetailID);
                    counter = maxObj - 1;
                }
                else counter = -1;
            }

            Int32 PersonalIdentificationDetailID = 0;
            if (!String.IsNullOrEmpty(txtPersonalIdentificationDetailID.Value))
            {
                PersonalIdentificationDetailID = Int32.Parse(txtPersonalIdentificationDetailID.Value);

                foreach (DetailKTP_KK det in list)
                {
                    if (PersonalIdentificationDetailID == det.PersonalIdentificationDetailID)
                    {
                        det.Address = txtAlamat.Text;
                        det.Kelurahan = txtKelurahan.Text;
                        det.Kecamatan = txtKecamatan.Text;
                        det.Name = txtNamaLengkap.Text;
                        det.GenderID = Int32.Parse(cboJenisKelamin.SelectedValue.ToString());
                        det.Gender = cboJenisKelamin.SelectedItem.Text;
                        det.RelationID = Int32.Parse(cboRelation.SelectedValue);
                        det.BirthPlace = txtTempatLahir.Text;
                        det.BirthDate = TglLahir;
                        det.StateCountry = txtPropinsiNegara.Text;
                        det.MaritalStatusID = Int32.Parse(cboStatusPernikahan.SelectedValue);
                        det.BloodType = cboGolDarah.SelectedValue;
                        det.BloodTypeID = Int32.Parse(cboGolDarah.SelectedValue);
                        det.ReligionID = Int32.Parse(cboAgama.SelectedValue);
                        det.LastEducationID = Int32.Parse(cboPendidikan.SelectedValue);
                        det.OccupationID = Int32.Parse(cboPekerjaan.SelectedValue);
                        det.ParentName = txtNamaOrangTua.Text;
                        det.SBKRINotes = txtWNRI.Text;
                        det.Notes = txtKeterangan.Text;
                    }
                }
            }
            else
            {
                PersonalIdentificationDetailID = counter;

                var obj = new DetailKTP_KK(
                    OrgID,
                    SiteID,
                    0, // personal identification id is not generated yet
                    PersonalIdentificationDetailID,
                    txtAlamat.Text,
                    txtKelurahan.Text,
                    txtKecamatan.Text,
                    txtNamaLengkap.Text,
                    Int32.Parse(cboJenisKelamin.SelectedValue.ToString()),
                    cboJenisKelamin.SelectedItem.Text,
                    Int32.Parse(cboRelation.SelectedValue),
                    txtTempatLahir.Text,
                    TglLahir,
                    txtPropinsiNegara.Text,
                    Int32.Parse(cboStatusPernikahan.SelectedValue),
                    Int32.Parse(cboGolDarah.SelectedValue),
                    Int32.Parse(cboAgama.SelectedValue),
                    Int32.Parse(cboPendidikan.SelectedValue),
                    Int32.Parse(cboPekerjaan.SelectedValue),
                    txtNamaOrangTua.Text,
                    txtWNRI.Text,
                    txtKeterangan.Text
                    );

                list.Add(obj);
            }

            Session["ListDetailKTP_KK"] = list;
            gvw_data.DataSource = list;
            gvw_data.DataBind();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "hideModal", "closeDetailForm();", true);            
        }

        protected void btn_showDetailPersonalID_Click(object sender, EventArgs e)
        {
            var list = (List<DetailKTP_KK>)Session["ListDetailKTP_KK"];
            int PDID = int.Parse(txtPersonalIdentificationDetailID.Value);
            foreach (DetailKTP_KK det in list)
            {
                if (det.PersonalIdentificationDetailID == PDID)
                {
                    txtAlamat.Text = det.Address;
                    txtKelurahan.Text = det.Kelurahan;
                    txtKecamatan.Text = det.Kecamatan;
                    txtNamaLengkap.Text = det.Name;
                    cboJenisKelamin.SelectedValue = det.GenderID.ToString();
                    cboRelation.SelectedValue = det.RelationID.ToString();
                    txtTempatLahir.Text = det.BirthPlace;
                    txtTglLahir.Text = det.BirthDate.ToShortDateString();
                    txtPropinsiNegara.Text = det.StateCountry;
                    cboStatusPernikahan.SelectedValue = det.MaritalStatusID.ToString();
                    cboGolDarah.SelectedValue = det.BloodTypeID.ToString();
                    cboAgama.SelectedValue = det.ReligionID.ToString();
                    cboPendidikan.SelectedValue = det.LastEducationID.ToString();
                    cboPekerjaan.SelectedValue = det.OccupationID.ToString();
                    txtNamaOrangTua.Text = det.ParentName;
                    txtKeterangan.Text = det.Notes;
                    txtWNRI.Text = det.SBKRINotes;
                }
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            show_detail();
        }
    }
}