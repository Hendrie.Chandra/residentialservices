﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="lookupreferencenumber.aspx.cs"
    Inherits="residential.web.lookupreferencenumber" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>:: lookup form ::</title>
    <script type="text/javascript" src="../scripts/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="../global.js">
    </script>
    <link href="~/maincss.css" rel="stylesheet" type="text/css" />
</head>
<body class="bodypopup">
    <form id="form1" runat="server">
    <div class="popupcontainer">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Label ID="lbl_title" runat="server" CssClass="labellookuptitle"></asp:Label>
                <div class="buttoncontainer" style="height: auto">
                    <table border="0" cellpadding="0" width="100%">
                        <tr>
                            <td align="center">
                                <font color="white">Case Number</font>
                            </td>
                            <td align="center">
                                <font color="white">Requestor Name</font>
                            </td>
                            <td align="center">
                                <font color="white">Date</font>
                            </td>
                            <td rowspan="2">
                                <asp:Button ID="btn_search" runat="server" Text="SEARCH" CssClass="button" Width="100px"
                                    Height="100%" OnClick="btn_search_Click" />
                            </td>
                        </tr>
                        <tr style="width: 100%">
                            <td align="center" width="33.33%">
                                <asp:TextBox ID="txt_referencenumber" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td align="center" width="33.33%">
                                <asp:TextBox ID="txt_name" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td align="center" width="33.33%">
                                <asp:TextBox ID="txt_date" runat="server" Width="100%"></asp:TextBox>
                                <asp:CalendarExtender ID="txt_date_CalendarExtender" runat="server" TargetControlID="txt_date"
                                    Format="dd MMMM yyyy">
                                </asp:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="lookupgridviewcontainer">
                    <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="gridview"
                        AllowPaging="True" BorderWidth="0px" Width="100%" DataKeyNames="CaseNumber" OnPageIndexChanging="gvw_data_PageIndexChanging"
                        OnRowCommand="gvw_data_RowCommand" OnRowDataBound="gvw_data_RowDataBound">
                        <PagerSettings Mode="NumericFirstLast" FirstPageText="First" LastPageText="Last"
                            NextPageText="Next" PreviousPageText="Prev" Position="TopAndBottom" />
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <HeaderStyle CssClass="gridview_header" Width="40px" />
                                <ItemStyle CssClass="gridview_item_left" Font-Bold="true" />
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnk_select" runat="server" CommandName="cmd_select" CssClass="gridview_link">SELECT</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CASE NUMBER">
                                <HeaderStyle CssClass="gridview_header" Width="250px" />
                                <ItemStyle CssClass="gridview_item_left" />
                                <ItemTemplate>
                                    <asp:HyperLink ID="lnk_casenumber" runat="server" Target="_blank" CssClass="gridview_link">casenumber</asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="NAME" DataField="NamaPelapor">
                                <HeaderStyle CssClass="gridview_header" Width="250px" />
                                <ItemStyle CssClass="gridview_item_left" />
                            </asp:BoundField>
                            <asp:BoundField HeaderText="REQUEST DATE" DataField="RequestDate" DataFormatString="{0:dd-MMM-yyyy}">
                                <HeaderStyle CssClass="gridview_header" />
                                <ItemStyle CssClass="gridview_item_left" />
                            </asp:BoundField>
                        </Columns>
                        <EmptyDataTemplate>
                            No data
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <asp:Label ID="Label1" runat="server" Text="Reference Number"></asp:Label>
                <asp:Label ID="lbl_refnumber" runat="server" Text=""></asp:Label>
                <div class="buttoncontainerfooter">
                    <asp:Button ID="btn_select" runat="server" Text="OK" CssClass="button" Width="100px"
                        OnClick="btn_select_Click" />
                    <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="button" Width="100px"
                        OnClick="btn_cancel_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
