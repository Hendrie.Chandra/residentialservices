﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class listbannerlocation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_cbo_bannerType();
                bounddatagrid();
            }
        }

        private void bounddatagrid()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
           
            Int32 BannerType = Int32.Parse(cbo_bannerType.SelectedValue);
            DataTable dt = dbclass.retrieve_banner_location(BannerType, userData.SiteID, userData.OrgID);
            gvw_data.DataSource = dt;
            gvw_data.DataBind();
        }

        protected void show_cbo_bannerType()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_banner_type();
            cbo_bannerType.DataSource = dt;
            cbo_bannerType.DataTextField = "BannerTypeName";
            cbo_bannerType.DataValueField = "BannerTypeID";
            cbo_bannerType.DataBind();
            cbo_bannerType.SelectedIndex = 0;
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {                
                String BannerLocationID = DataBinder.Eval(e.Row.DataItem, "BannerLocationID").ToString();
                String BannerLocationName = DataBinder.Eval(e.Row.DataItem, "BannerLocationName").ToString();
                HyperLink lnk_bannerlocation = (HyperLink)e.Row.FindControl("lnk_bannerlocation");
                lnk_bannerlocation.Text = BannerLocationName;
                lnk_bannerlocation.NavigateUrl = "entrybannerlocation.aspx?BannerLocationID=" + BannerLocationID;                
            }
        }
        
        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                //int index = Convert.ToInt32(gvr.RowIndex);
                if (chkselect.Checked == true)
                {
                    Int32 ID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());
                    Int32 returnvalue = delete_bannerloc(ID);

                    DataTable dt_points = dbclass.retrieve_point(ID);
                    foreach (DataRow dr in dt_points.Rows)
                    {
                        dbclass.Delete_BannerPoint((Int32)dr["PointID"], userData.UserName);
                    }
                }
            }            
            bounddatagrid();
        }        

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid();
        }

        private Int32 delete_bannerloc(int dataToDelete)
        {
            UserData userData = (UserData)ViewState["UserData"];

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = 0;

            returnvalue = dbclass.delete_banner_location(dataToDelete, userData.UserName);
           
            return returnvalue;
        }

        protected void cbo_bannerType_SelectedIndexChanged(object sender, EventArgs e)
        {            
            bounddatagrid();
        }
    }
}