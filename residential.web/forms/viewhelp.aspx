﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewhelp.aspx.cs" Inherits="residential.web.forms.viewhelp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="buttoncontainer">
                <asp:Button CssClass="button" ID="btn_add" runat="server" Text="ADD" 
                    Width="100px" onclick="btn_add_Click" />
                <asp:Button CssClass="button" ID="btn_edit" runat="server" Text="EDIT" 
                    Width="100px" onclick="btn_edit_Click" />
                <asp:Button CssClass="button" ID="btn_delete" runat="server" Text="DELETE" 
                    Width="100px" OnClientClick="return treeConfirmDelete();" onclick="btn_delete_Click" />
            </div>
            <asp:TreeView ID="HelpTree" runat="server" ImageSet="Faq" Width="100%" 
                onselectednodechanged="HelpTree_SelectedNodeChanged">
                <HoverNodeStyle Font-Underline="True" ForeColor="Purple" />
                <NodeStyle Font-Names="Tahoma" Font-Size="8pt" ForeColor="DarkBlue" 
                    HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                <ParentNodeStyle Font-Bold="False" />
                <SelectedNodeStyle Font-Underline="True" HorizontalPadding="0px" 
                    VerticalPadding="0px" />
            </asp:TreeView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
