﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.OleDb;
using System.Net;

using System.IO;
using System.Data.SqlClient;

namespace residential.web.forms
{
    public partial class smartcardreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["UserData"] = new UserData(User.Identity.Name);
            ViewState["sortOrder"] = "";
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            //string cardDetailData = txt_carddetail.Text;
            //string unitCode = txt_unitCode.Text;
            //string unitNo = txt_unitNo.Text;


            //DataTable dt = dbclass.retrieve_smartcard_report();
            DataTable dt = retrieve_smartcard_report();


            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void btn_run_report_Click(object sender, EventArgs e)
        {
            errID.Visible = false;
            bounddatagrid("", "");
            //export_toexcel();
        }

        protected void export_toexcel()
        {
            System.Data.OleDb.OleDbConnection conn = new System.Data.OleDb.OleDbConnection();
            try
            {                
                string ExcelFileName = "smartcard_report";
                //string pathOfFileToCreate = @"D:\report\" + ExcelFileName + ".xlsx";
                string pathOfFileToCreate = Server.MapPath(ExcelFileName + ".xlsx");

                //delete file
                if (File.Exists(pathOfFileToCreate))
                    File.Delete(pathOfFileToCreate);

                conn.ConnectionString = String.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES"";", pathOfFileToCreate);

                string tableName = "smartcard";//DateTime.Now.ToString("ddMMyyyyhms");


                conn.Open();
                var cmd = conn.CreateCommand();
                //string sqlCreateTable = "CREATE TABLE Sheet1(CardNumber varchar NULL, ChipNumber varchar NULL, PSCode varchar NULL, HolderName varchar NULL, BoomGateName varchar NULL, UnitCode varchar NULL, UnitNo varchar NULL)";
                string sqlCreateTable = "CREATE TABLE " + tableName + "(CardNumber varchar NULL, ChipNumber varchar NULL, PSCode varchar NULL, HolderName varchar NULL, BoomGateName varchar NULL, UnitCode varchar NULL, UnitNo varchar NULL)";

                cmd.CommandText = sqlCreateTable;
                cmd.ExecuteNonQuery();

                DataTable source = get_data_excel_datasource();

                if (source.Rows.Count > 0)
                {
                    string _cardnumber = "";
                    string _chipnumber = "";
                    string _pscode = "";
                    string _holdername = "";
                    string _boomgatename = "";
                    string _unitcode = "";
                    string _unitno = "";

                    foreach (DataRow dr in source.Rows)
                    {
                        _cardnumber = dr["CardNumber"].ToString();
                        _chipnumber = dr["ChipNumber"].ToString();
                        _pscode = dr["PSCode"].ToString();
                        _holdername = dr["HolderName"].ToString();
                        _boomgatename = dr["BoomGateName"].ToString();
                        _unitcode = dr["UnitCode"].ToString();
                        _unitno = dr["UnitNo"].ToString();

                        //string sqll = "Insert into Sheet1(CardNumber, ChipNumber, PSCode, HolderName, BoomGateName, UnitCode, UnitNo) values('" + _cardnumber + "','" + _chipnumber + "','" + _pscode + "','" + _holdername + "','" + _boomgatename + "','" + _unitcode + "','" + _unitno + "')";
                        string sqll = "Insert into " + tableName + "(CardNumber, ChipNumber, PSCode, HolderName, BoomGateName, UnitCode, UnitNo) values('" + _cardnumber + "','" + _chipnumber + "','" + _pscode + "','" + _holdername + "','" + _boomgatename + "','" + _unitcode + "','" + _unitno + "')";

                        cmd.CommandText = sqll;
                        cmd.ExecuteNonQuery();
                    }
                }
                if (conn.State == ConnectionState.Open) { conn.Close(); }
                /*start of download file excel*/
                WebClient req = new WebClient();
                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                response.ClearContent();
                response.ClearHeaders();
                response.Buffer = true;
                response.AddHeader("Content-Disposition", "attachment;filename=" + ExcelFileName + ".xlsx");
                byte[] data = req.DownloadData(pathOfFileToCreate);
                response.BinaryWrite(data);
                response.End();
                /*end of download file excel*/
            }
            catch (Exception ex)
            {

                errID.Visible = true;
                lbl_error.Text = ex.Message.ToString();
                conn.Close();
            }
        }

        protected DataTable get_data_excel_datasource()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            //DataTable dt = dbclass.retrieve_smartcard_report();
            DataTable dt = retrieve_smartcard_report();

            return dt;
        }

        protected void btn_export_Click(object sender, EventArgs e)
        {
            export_toexcel();
        }

        public DataTable retrieve_smartcard_report()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(constring);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SmartCard_Report", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
    }
}