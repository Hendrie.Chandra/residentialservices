﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using residential.libs;

namespace residential.web.forms
{
    public partial class listpoint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                alert_danger.Visible = false;
                bounddatagrid();

                String popup_url = "openWindowModal('entrypoint.aspx?flag=1', 700, 400)";
                btn_add.Attributes.Add("onclick", popup_url);
            }
        }

        private void bounddatagrid()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_pointmst();

            gvw_data.DataSource = dt;
            gvw_data.DataBind();
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String PointID = DataBinder.Eval(e.Row.DataItem, "PointID").ToString();
                //String BannerLocationID = DataBinder.Eval(e.Row.DataItem, "BannerLocationID").ToString();
                LinkButton lnk_site = (LinkButton)e.Row.FindControl("lnk_site");
                lnk_site.CommandArgument = PointID;
                lnk_site.Text = PointID;
                String popup_url = "openWindowModal('entrypoint.aspx?flag=1&PointID=" + PointID + "', 700, 400)";
                lnk_site.Attributes.Add("onclick", popup_url);

                Label lbl_no = (Label)e.Row.FindControl("lbl_no");
                Int32 no_urut = e.Row.RowIndex + 1;
                no_urut = gvw_data.PageIndex * gvw_data.PageSize + no_urut;
                lbl_no.Text = no_urut.ToString();
            }
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_edit")
            {
                bounddatagrid();
            }

        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btn_add_Click(object sender, EventArgs e)
        {
            bounddatagrid();
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 ID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());
                    Int32 returnvalue = delete_point(ID);
                }
            }
            bounddatagrid();
        }

        protected void btn_closeerror_Click(object sender, EventArgs e)
        {
            alert_danger.Visible = false;
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid();
        }

        private Int32 delete_point(int dataToDelete)
        {
            String SavedID = Session["username"].ToString();

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = 0;

            returnvalue = dbclass.Delete_BannerPoint(dataToDelete, SavedID);

            return returnvalue;
        }
    }
}