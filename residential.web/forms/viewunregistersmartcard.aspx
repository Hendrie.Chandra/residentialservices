﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewunregistersmartcard.aspx.cs" Inherits="residential.web.forms.viewunregistersmartcard" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <style type="text/css">
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $('#back-to-top').tooltip('show');
        });

        function showModalFormDetailID(cardnumber) {
            $('#modalFormDetailID').modal('show');
            
            $('#<%=hdf_cardnumber.ClientID%>').val(cardnumber);
            $('#<%=btn_showdetail.ClientID%>').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="page-header">
        <h1>Smart Card Request <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" UseSubmitBehavior="false" OnClick="btn_back_Click"/>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Unregister Smart Card Detail</div>
                <div class="panel-body">
                    <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                        OnRowDataBound="gvw_data_RowDataBound" AllowPaging="True" OnPageIndexChanging="gvw_data_PageIndexChanging"
                        ShowHeaderWhenEmpty="True" DataKeyNames="CardNumber" PageSize="5">
                        <PagerStyle CssClass="pagination-ys" />
                        <Columns>
<%--                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                    <asp:CheckBox ID="chk_selectall" runat="server"
                                        OnCheckedChanged="chk_selectall_CheckedChanged" AutoPostBack="True" />
                                </HeaderTemplate>
                                <HeaderStyle CssClass="gridview_header" Width="30px" />
                                <ItemStyle CssClass="gridview_item_center" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="chk_select" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <%--<asp:BoundField DataField="CaseNumber" HeaderText="Case Number" />--%>
                            <asp:BoundField DataField="CardNumber" HeaderText="Card Number" />
                            <asp:BoundField DataField="ChipNumber" HeaderText="Chip Number" />
                            <%--<asp:BoundField DataField="CardTypeID" HeaderText="Card Type" />--%>
<%--                            <asp:TemplateField HeaderText="Card Type">
                                <ItemTemplate>
                                    <asp:Label ID="CardTypeID" runat="server" Text='<%# int.Parse(Eval("CardTypeID").ToString())==1 ? "Resident" : "Member" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                           <%-- <asp:BoundField DataField="HolderName" HeaderText="Holder Name" ControlStyle-BorderWidth="20px" />--%>
<%--                            <asp:BoundField DataField="BoomGateCode" HeaderText="Boomgate Code" />--%>
                            <asp:BoundField DataField="BoomGateName" HeaderText="Boomgate Name" />
                            <%--<asp:BoundField DataField="IsActive" HeaderText="Status" />--%>
<%--                            <asp:TemplateField HeaderText="Card Type">
                                <ItemTemplate>
                                    <asp:Label ID="IsActive" runat="server" Text='<%# (bool)Eval("IsActive")==true ? "Active" : "InActive" %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <%--<asp:BoundField DataField="CreatedDate" HeaderText="Created Date" DataFormatString="{0:dd/MM/yyyy HH:mm}" />--%>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnk_detail" CssClass="btn btn-default btn-sm" Text="View Detail"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            No data
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <br />
                    
                </div>
            </div>
            <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
        </ContentTemplate>
    </asp:UpdatePanel>

        <%-- start form detail modal --%>
    <div class="modal fade" id="modalFormDetailID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabelResident">Smart Card Detail</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <%--<asp:Button ID="Button4" runat="server" Text="Save" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_savedetail_Click" />--%>
                            <button type="button" class="btn btn-default" data-dismiss="modal" runat="server" id="Button5">Close</button>
                            <asp:Button ID="btn_showdetail" runat="server" UseSubmitBehavior="false" Style="display: none" OnClick="btn_showdetail_Click" />

                            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="Div2" visible="false">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                            </div>
                            <asp:HiddenField ID="hdf_cardnumber" runat="server" />
                            <hr />
                            <asp:Label ID="lbl_error_message_resident" runat="server" Text="" Visible="false"></asp:Label>
                            <div id="error_meesage_resident" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
                                <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Warning!</strong><%=lbl_error_message_resident.Text %>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Card Number</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_cardnumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Holder Name</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_holdername" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Chip Number</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_chipnumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Vehicle Number</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_vehiclenumber" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Vehicle Type</label>
                                    <div class="col-sm-8">
                                        <asp:DropDownList ID="ddl_vehicletype" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Vehicle Brand</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txt_vehiclebrand" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <%-- end form detail modal --%>

</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>--%>
