﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace residential.web.forms
{
    public partial class DeptMonitoring : System.Web.UI.Page
    {
        String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";
                bounddatagrid("", "");
            }
            Response.AppendHeader("refresh", "60");
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            UserData userData = (UserData)ViewState["UserData"];
            DateTime? StartDate = null;
            DateTime? EndDate = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            DataTable dt = dbclass.deptmonitoring(userData.OrgID, userData.SiteID, StartDate, EndDate);
            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            gvw_data.DataSource = null;
            gvw_data.DataSource = dv;
            gvw_data.DataBind();

            Int32 total1 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("Pending") ?? 0);
            if (total1 != 0)
            {
                gvw_data.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[1].Text = total1.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }
            
            Int32 total2 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("AssignToPIC") ?? 0);
            if (total2 != 0)
            {
                gvw_data.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[2].Text = total2.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total3 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("Cancelled") ?? 0);
            if (total3 != 0)
            {
                gvw_data.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[3].Text = total3.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total4 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("InProgress") ?? 0);
            if (total4 != 0)
            {
                gvw_data.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[4].Text = total4.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total5 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("OnHold") ?? 0);
            if (total5 != 0)
            {
                gvw_data.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[5].Text = total5.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total6 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("ProgressComplete") ?? 0);
            if (total6 != 0)
            {
                gvw_data.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[6].Text = total6.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total7 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("Resolved") ?? 0);
            if (total7 != 0)
            {
                gvw_data.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[7].Text = total7.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total8 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("NotSatisfied") ?? 0);
            if (total8 != 0)
            {
                gvw_data.FooterRow.Cells[8].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[8].Text = total8.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total9 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("CaseClosed") ?? 0);
            if (total9 != 0)
            {
                gvw_data.FooterRow.Cells[9].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[9].Text = total9.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total10 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("Rejected") ?? 0);
            if (total10 != 0)
            {
                gvw_data.FooterRow.Cells[10].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[10].Text = total10.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }

            Int32 total11 = dt.AsEnumerable().Sum(row => row.Field<Int32?>("WaitingForValidation") ?? 0);
            if (total11 != 0)
            {
                gvw_data.FooterRow.Cells[11].HorizontalAlign = HorizontalAlign.Center;
                gvw_data.FooterRow.Cells[11].Text = total11.ToString();
                gvw_data.FooterRow.Cells[0].Text = "TOTAL CASE";
            }
            
        }

        protected void picker_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime? StartDate = null;
                DateTime? EndDate = null;

                if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
                {
                    StartDate = Convert.ToDateTime(TxtStartDate.Text);
                    EndDate = Convert.ToDateTime(TxtEndDate.Text);
                }

                DateTime SD = Convert.ToDateTime(TxtStartDate.Text);
                if (SD > DateTime.Now)
                {
                    Response.Write("<script>alert('Created Date FROM cannot  be greater than CURRENT date')</script>");
                }

                DateTime ED = Convert.ToDateTime(TxtEndDate.Text);
                if (ED > DateTime.Now)
                {
                    Response.Write("<script>alert('Created Date TO cannot  be greater than CURRENT date')</script>");
                }

                if (ED < SD)
                {
                    Response.Write("<script>alert('Created Date TO cannot  be less than Created Date FROM')</script>");
                }

                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";
                bounddatagrid("", "");
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String CaseStatusName = DataBinder.Eval(e.Row.DataItem, "DeptName").ToString();
                Int32 Pending = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "Pending").ToString());
                Int32 AssignToPIC = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "AssignToPIC").ToString());
                Int32 Cancelled = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "Cancelled").ToString());
                Int32 InProgress = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "InProgress").ToString());
                Int32 OnHold = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "OnHold").ToString());
                Int32 ProgressComplete = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "ProgressComplete").ToString());
                Int32 Resolved = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "Resolved").ToString());
                Int32 NotSatisfied = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "NotSatisfied").ToString());
                Int32 CaseClosed = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "CaseClosed").ToString());
                Int32 Rejected = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "Rejected").ToString());
                //Int32 CustomerCannotbeReached = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "CustomerCannotbeReached").ToString());
                Int32 WaitingForValidation = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "WaitingForValidation").ToString());
            }
        }


        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }
    }
}
