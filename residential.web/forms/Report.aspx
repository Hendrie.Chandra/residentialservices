﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="residential.web.forms.Report" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../scripts/dashboard/pikaday.js"></script>
    <link href="../scripts/dashboard/pikaday.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div class="page-header">
                <h1>Report<asp:Label runat="server" ID="lblTitle" /></h1>
            </div>
    <div class="row">
        <div class="col-md-2">
            <asp:Label ID="Label2" runat="server" Text="Created Date" Font-Bold="True" Font-Size="Medium"></asp:Label>
        </div>
    </div><br />
        <div class="row form-inline">
            <div class="form-group">
                <div class="col-md-2">From</div>
                <div class="col-md-3">
                    <asp:TextBox ID="TxtStartDate" runat="server" ClientIDMode="Static" CssClass="form-control" AutoComplete="off"></asp:TextBox>
                    <%--<asp:CalendarExtender ID="TxtStartDate_CalendarExtender" runat="server" TargetControlID="TxtStartDate" PopupPosition="TopLeft" Format="dd MMMM yyyy"></asp:CalendarExtender>--%>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-1">To</div>
                <div class="col-md-3">
                    <asp:TextBox ID="TxtEndDate" runat="server" ClientIDMode="Static" CssClass="form-control" AutoComplete="off"></asp:TextBox>
                    <%--<asp:CalendarExtender ID="TxtEndDate_CalendarExtender" runat="server" TargetControlID="TxtEndDate" PopupPosition="TopLeft" Format="dd MMMM yyyy"></asp:CalendarExtender>--%>
                </div>
            </div>
            <asp:Button ID="picker" runat="server" Text="View" CssClass="btn btn-info" OnClick="picker_Click" />
            <asp:Button ID="download" runat="server" Text="Download" CssClass="btn btn-primary" OnClick = "ExportToExcel"/>
        </div>
    <br />

            <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('TxtStartDate'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000, 2020],
                numberOfMonths: 1,

            });
    </script>

    <script type="text/javascript">
        var picker = new Pikaday(
            {
                field: document.getElementById('TxtEndDate'),
                firstDay: 1,
                minDate: new Date('2000-01-01'),
                maxDate: new Date('2020-12-31'),
                yearRange: [2000, 2020],
                numberOfMonths: 1,

            });
    </script>
    
   <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-2 control-label">Case Status</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="cbo_status" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" placeholder="search by case number, unit" OnTextChanged="txt_search_TextChanged" AutoPostBack="true"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" OnClick="btn_search_Click"  />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
    <br />

                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover" AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound" 
                ShowHeaderWhenEmpty="True"
                DataKeyNames="CaseNumber" AllowSorting="True" OnSorting="gvw_data_Sorting"
                OnPageIndexChanging="gvw_data_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="UNIT CODE" DataField="Unit" SortExpression="Unit" />
                    <asp:BoundField HeaderText="CATEGORY" DataField="HelpCategoryName" SortExpression="HelpCategoryName"></asp:BoundField>
                    <asp:BoundField HeaderText="DEPT NAME" DataField="DeptName" SortExpression="DeptName"></asp:BoundField>
                    <asp:BoundField HeaderText="HELP NAME" DataField="HelpName" SortExpression="HelpName"></asp:BoundField>                    
                    <asp:BoundField HeaderText="CREATED" DataField="RequestDate" SortExpression="RequestDate" DataFormatString="{0:d MMMM yyyy}" />
                    <asp:BoundField HeaderText="OVERDUE" DataField="OverdueDate" SortExpression="OverdueDate" DataFormatString="{0:d MMMM yyyy}" />
                    <asp:BoundField HeaderText="SLA(DAYS)" DataField="SLA" SortExpression="SLA" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="DAYS" DataField="Days" SortExpression="Days" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" SortExpression="CaseStatusName" />
                    <asp:BoundField HeaderText="CREATED BY" DataField="CreatedBy" SortExpression="CreatedBy" />
                    <%--<asp:BoundField HeaderText="STATUS" DataField="CaseStatusID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden"/>--%>
                </Columns>
                <EmptyDataTemplate>
                    No Data
                </EmptyDataTemplate>
            </asp:GridView>
        </asp:Content>

