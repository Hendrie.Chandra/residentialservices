﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listwtpcategories.aspx.cs" Inherits="residential.web.forms.listwtpcategories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h4 style="text-align: center;">Water Plant Categories</h4>
    <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
        CssClass="gridview" AllowPaging="True" BorderWidth="0px"
        Width="100%" OnRowDataBound="gvw_data_RowDataBound"
        ShowHeaderWhenEmpty="True"
        DataKeyNames="WaterPlantCategoryID" AllowSorting="True" OnSorting="gvw_data_Sorting"
        OnPageIndexChanging="gvw_data_PageIndexChanging">
        <Columns>
            <asp:TemplateField HeaderText="NO">
                <HeaderStyle CssClass="gridview_header" Width="30px" />
                <ItemStyle CssClass="gridview_item_center" />
                <ItemTemplate>
                    <asp:Label ID="lbl_no" runat="server" Text="Label"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>            
            <asp:BoundField HeaderText="CATEGORY" DataField="WaterPlantCategoryName" SortExpression="WaterPlantCategoryName">
                <HeaderStyle CssClass="gridview_header" />
                <ItemStyle CssClass="gridview_item_left"/>
            </asp:BoundField>
            <asp:BoundField HeaderText="PRICE" DataField="Price" DataFormatString="{0:N0}">
                <HeaderStyle CssClass="gridview_header" />
                <ItemStyle CssClass="gridview_item_left" />
            </asp:BoundField>            
        </Columns>
        <EmptyDataTemplate>
            No data
        </EmptyDataTemplate>
    </asp:GridView>
</asp:Content>
