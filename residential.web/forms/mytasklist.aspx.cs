﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using residential.libs.Models;

using LKC = LKCommon;
using residential.web.Classes;
using System.Globalization;

namespace residential.web.forms
{
    public partial class mytasklist : System.Web.UI.Page
    {
        private string SortExpression
        {
            get
            {
                object o = ViewState["SortExpression"];
                if (o == null)
                {
                    return string.Empty;
                }
                else
                {
                    return o.ToString();
                }
            }
            set { ViewState["SortExpression"] = value; }
        }

        private bool SortAscending
        {
            get
            {
                object o = ViewState["SortAscending"];
                if (o == null)
                {
                    return true;
                }
                else
                {
                    return Convert.ToBoolean(o, CultureInfo.InvariantCulture);
                }
            }
            set { ViewState["SortAscending"] = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                ViewState["sortOrder"] = "";
                show_cbo_casestatus();
                //BindTasklist("", "");
                BindTasklist();
                show_pic();                
            }
        }

        private void BindTasklist()
        {
            try
            {
                int caseStatusID = LKC.CommonFunctions.StringToInt(cbo_status.SelectedValue);
                string search = txt_search.Text.ToString();

                DataTable dtTasklist = Helper.GetTasklist(Helper.OrgID, Helper.SiteID, caseStatusID, search);
                if (dtTasklist.Rows.Count > 0)
                {
                    if (SortExpression.Length > 0)
                    {
                        if (SortAscending)
                        {
                            dtTasklist.DefaultView.Sort = SortExpression + " ASC";
                        }
                        else
                        {
                            dtTasklist.DefaultView.Sort = SortExpression + " DESC";
                        }
                    }
                    this.gvw_data.DataSource = dtTasklist;
                    this.gvw_data.DataBind();
                }
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        private void show_cbo_casestatus()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_casestatus();
            cbo_status.DataSource = dt;
            cbo_status.DataTextField = "CaseStatusName";
            cbo_status.DataValueField = "CaseStatusID";
            cbo_status.DataBind();
            cbo_status.Items.Insert(0, new ListItem("[ All Status ]", "0"));
            cbo_status.SelectedIndex = 0;
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String CaseNumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();
                Int32 CaseStatusID = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "CaseStatusID").ToString());
                String ViewFormName = DataBinder.Eval(e.Row.DataItem, "ViewFormName").ToString();

                String CaseStatus = DataBinder.Eval(e.Row.DataItem, "CaseStatusName").ToString();
                string departmentID = DataBinder.Eval(e.Row.DataItem, "DepartmentID").ToString();
                if (CaseStatus.ToUpper() != "ASSIGNED TO PIC")
                    ((CheckBox)e.Row.FindControl("chk_select")).Visible = false;
                
                for (var i = 1; i < e.Row.Cells.Count; i++)
                    e.Row.Cells[i].Attributes["onclick"] = "window.location = '" + ViewFormName + "?CaseNumber=" + CaseNumber + "&DeptID=" + departmentID + "'";

                if (!String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString()) && (CaseStatusID == 2 || CaseStatusID == 4))
                {
                    DateTime OverdueDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString());
                    if (DateTime.Compare(DateTime.Now, OverdueDate) >= 0)
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                }
            }
        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            BindTasklist();
        }

        protected void cbo_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindTasklist();
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (e.SortExpression == this.SortExpression)
            {
                SortAscending = !SortAscending;
            }
            else
            {
                SortAscending = true;
            }
            this.SortExpression = e.SortExpression; 
            BindTasklist();
        }        

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            BindTasklist();
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            BindTasklist();
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_edit")
            {
                BindTasklist();
            }
        }

        private void show_pic()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_pic(userData.OrgID, userData.SiteID, userData.DepartmentID);
            if (dt.Rows.Count > 0)
            {
                cbo_assign.DataSource = dt;
                cbo_assign.DataTextField = "FullName";
                cbo_assign.DataValueField = "UserID";
            }
            else
                cbo_assign.Items.Add(new ListItem("No data", "0"));

            cbo_assign.DataBind();
            cbo_assign.SelectedIndex = 0;
        }

        protected void btn_assigning_Click(object sender, EventArgs e)
        {
            if (cbo_assign.SelectedValue == "0")
            {
                alert_danger_text.Text = "Please select person te be assigned";
                alert_danger.Visible = true;
            }
            else
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                Int32 UserID = Int32.Parse(cbo_assign.SelectedValue);
                String Remarks = "Assign to " + cbo_assign.SelectedItem.Text;

                foreach (GridViewRow gvRow in gvw_data.Rows)
                {
                    CheckBox chkSel = (CheckBox)gvRow.FindControl("chk_select");
                    int index = Convert.ToInt32(gvRow.RowIndex);
                    if (chkSel.Checked == true)
                    {
                        String CaseNumber = gvw_data.DataKeys[index].Value.ToString();
                        dbclass.assign_case(UserID, CaseNumber, userData.UserName);
                        /*update_to_crm(CaseNumber, 4, Remarks, userData.FullName, cbo_assign.SelectedItem.Text, 3);*/
                    }
                }
                BindTasklist();
            }
        }

        //private void update_to_crm(String _casenumber, Int32 _crmcasestatusid, String _remarks, String _updatedby, String _workedby, Int32 _SatisfactionID)
        //{
        //    String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
        //    String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
        //    String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
        //    String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
        //    String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
        //    CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

        //    UpdateCaseHelpDeskStatusRequest request = new UpdateCaseHelpDeskStatusRequest();
        //    request.CRMCaseNumber = _casenumber;
        //    request.CRMCaseStatusID = _crmcasestatusid;
        //    request.Remarks = _remarks;
        //    request.UpdateBy = _updatedby;
        //    request.UpdateDate = DateTime.Now;
        //    request.WorkedBy = _workedby;
        //    request.SatisfactionID = _SatisfactionID;
        //    UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(request);
        //}
    }
}