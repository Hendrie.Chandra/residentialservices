﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using residential.libs;
using residential.libs.Models;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.IO;
using System.Linq;

using residential.web.model;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entrysmartcard : System.Web.UI.Page
    {
        static String prevPage = "smartcard.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(HttpContext.Current.User.Identity.Name);

                load_card_type();
                //ddl_card_type.Items.Insert(0,new ListItem("--Please Select One--","0"));
                load_resident_status();
                calculateprice();

                Session["SmartCardHistory"] = null;

                ViewState["UserData"] = new UserData(User.Identity.Name);
                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }


            entryheadersmartcard.ToggleAlert = false;
            if (!string.IsNullOrEmpty(entryheadersmartcard.UnitCluster))
            {
                //entryheadersmartcard.retrieve_smartcard_history(entryheadersmartcard.PSCode);
                string[] unit_cluster = entryheadersmartcard.UnitCluster.Split('-');
                string UnitCode = "";
                string UnitNo = "";
                if (unit_cluster.Length == 2)
                {
                    UnitCode = unit_cluster[0];
                    UnitNo = unit_cluster[1];
                }

                UserData userData = (UserData)ViewState["UserData"];
                Int32 OrgID = userData.OrgID;
                Int32 SiteID = userData.SiteID;
                entryheadersmartcard.retrieve_smartcard_history_byunit(OrgID, SiteID, UnitCode, UnitNo);
            }            

            if (Session["SmartCardHistory"] != null)
            {
                bound_smartcardhistory();
                //entryheadersmartcard.retrieve_smartcard_history();
                //gvw_smartcardhistory.DataSource = Session["SmartCardHistory"];
                //gvw_smartcardhistory.DataBind();
            }          

            ScriptManager.RegisterStartupScript(this, this.GetType(), "load_upload_file_js", "load_upload_file_js();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "load_unit_history", "load_unit_history();", true);
        }


        private void Show_Case(String _casenumber)
        {
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                DataTable dt = dbclass.retrieve_smartcard(_casenumber);

                entryheadersmartcard.OwnerName = dt.Rows[0]["psName"].ToString();
                entryheadersmartcard.PSCode = dt.Rows[0]["PSCode"].ToString();
                //check customer type -> 0 = Owner, 1 = Tenant
                entryheadersmartcard.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheadersmartcard.combobox_unit();
                entryheadersmartcard.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheadersmartcard.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheadersmartcard.combobox_caseorigin();
                entryheadersmartcard.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheadersmartcard.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheadersmartcard.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheadersmartcard.RequestDescription = dt.Rows[0]["Description"].ToString();

                entryheadersmartcard.disable_all_controls();

                //txt_total.Text = decimal.Parse(dt.Rows[0]["TotalAmount"].ToString()).ToString("F2");
                txt_total.Text = dt.Rows[0]["TotalAmount"].ToString();
                ddl_resident_status.SelectedValue = dt.Rows[0]["ResidentStatus"].ToString();

                //if (dt.Rows[0]["ResidentStatus"].ToString().ToLower() == "rent")
                //{                    
                //    lbl_rent_periode_date.Visible = true;
                //    txt_rent_periode_date.Visible = true;
                //    txt_rent_periode_date.Enabled = false;
                    
                //    DateTime rentperiodedate = DateTime.Parse(dt.Rows[0]["RentPeriodeDate"].ToString());
                //    txt_rent_periode_date.Text = rentperiodedate.ToString("dd MMMM yyyy");
                //}              

                ddl_card_type.SelectedValue = dt.Rows[0]["CardTypeID"].ToString();
                txt_quantity.Text = dt.Rows[0]["Quantity"].ToString();


                bool IsIdentitasDiri = ((bool)dt.Rows[0]["IsIdentitasDiri"] == true ? true : false);
                bool IsKK = ((bool)dt.Rows[0]["IsKK"] == true ? true : false);
                bool IsSTNK = ((bool)dt.Rows[0]["IsSTNK"] == true ? true : false);
                bool IsKepemilikanUnit = ((bool)dt.Rows[0]["IsKepemilikanUnit"] == true ? true : false);
                bool IsPerjanjianSewaMenyewaUnit = ((bool)dt.Rows[0]["IsPerjanjianSewaMenyewaUnit"] == true ? true : false);
                bool IsSuratKeteranganKaryawan = ((bool)dt.Rows[0]["IsSuratKeteranganKaryawan"] == true ? true : false);
                bool IsBuktiBayarUtilitas = ((bool)dt.Rows[0]["IsBuktiBayarUtilitas"] == true ? true : false);
                bool IsSuratKuasa = ((bool)dt.Rows[0]["IsSuratKuasa"] == true ? true : false);
                bool IsKTPPemberiPenerimaKuasa = ((bool)dt.Rows[0]["IsKTPPemberiPenerimaKuasa"] == true ? true : false);


                CheckBox_identitasdiri.Checked = IsIdentitasDiri;
                CheckBox_kk.Checked = IsKK;
                CheckBox_stnk.Checked = IsSTNK;
                CheckBox_kepemilikanunit.Checked = IsKepemilikanUnit;
                CheckBox_perjanjiansewamenyewaunit.Checked = IsPerjanjianSewaMenyewaUnit;
                CheckBox_suratketerangankaryawan.Checked = IsSuratKeteranganKaryawan;
                CheckBox_buktibayarutilitas.Checked = IsBuktiBayarUtilitas;
                CheckBox_suratkuasa.Checked = IsSuratKuasa;
                CheckBox_ktppemberipenerimakuasa.Checked = IsKTPPemberiPenerimaKuasa;
                txt_lainlain.Text = dt.Rows[0]["Other"].ToString();

                disable_control_smartcard_detail();
                disablecontrol();
            }
            catch (Exception)
            {
                throw new HttpException(404, "Page not found");
            }
        }

        protected void disable_control_smartcard_detail()
        {
            CheckBox_identitasdiri.Enabled = false;
            CheckBox_kk.Enabled = false;
            CheckBox_stnk.Enabled = false;
            CheckBox_perjanjiansewamenyewaunit.Enabled = false;
            CheckBox_buktibayarutilitas.Enabled = false;
            CheckBox_ktppemberipenerimakuasa.Enabled = false;
            CheckBox_kepemilikanunit.Enabled = false;
            CheckBox_suratketerangankaryawan.Enabled = false;
            CheckBox_suratkuasa.Enabled = false;
            txt_lainlain.Enabled = false;
        }



        protected bool transaction_validation()
        {
            if (entryheadersmartcard.Validate != "VALID")
            {
                entryheadersmartcard.ShowAlert = entryheadersmartcard.Validate;
                return false;
            }
            if (ddl_resident_status.SelectedValue.ToLower() == "rent" && ddl_card_type.SelectedValue=="1")
            {
                if (string.IsNullOrEmpty(txt_rent_periode_date.Text))
                {
                    entryheadersmartcard.ShowAlert = "Rent periode date is required";
                    return false;
                }
            }
            //if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            //{
            //    entryheader.ShowAlert = "Invalid total value";
            //    return false;
            //}
            return true;
        }
        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheadersmartcard.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }
        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }
        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["SmartCardHelpID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());
            //Int32 HelpID = find_help("Flyer");

            Int32 CaseOriginID = entryheadersmartcard.CaseOriginID;
            String PSName = entryheadersmartcard.OwnerName;
            String PSCode = entryheadersmartcard.PSCode;
            String Unit = entryheadersmartcard.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheadersmartcard.RequestorName;
            String TeleponPelapor = entryheadersmartcard.RequestorPhone;
            String EmailPelapor = entryheadersmartcard.RequestorEmail;
            String RequestDescription = entryheadersmartcard.RequestDescription;

            //start cek apakah unit ini di jual?
            if (UnitCode != "" && UnitNo != "")
            {
                DataTable dt_unit = dbclass.retrieve_smartcard_unit_incase(OrgID, SiteID, UnitCode, UnitNo);

                string ucase = "";
                string upscode = "";
                List<MembershipOwnership> autoUnregisterMembership = new List<MembershipOwnership>();
                if (dt_unit.Rows.Count > 0)
                {
                    foreach (DataRow udr in dt_unit.Rows)
                    {
                        ucase = udr["CaseNumber"].ToString();
                        upscode = udr["PSCode"].ToString();

                        if (upscode != PSCode)
                        {
                            //unregister by system
                            DataTable dt_listUnreg = dbclass.retrieve_smartcard_auto_unregister(ucase);                            

                            if (dt_listUnreg.Rows.Count > 0)
                            {                                                            
                                foreach (DataRow dt_unreg in dt_listUnreg.Rows)
                                {
                                    MembershipOwnership obj = new MembershipOwnership();
                                    obj.MembershipID = (int)dt_unreg["MembershipID"];
                                    obj.CardNumber = dt_unreg["CardNumber"].ToString(); 
                                    obj.ChipNumber = dt_unreg["ChipNumber"].ToString();
                                    obj.BoomGateID = (int)dt_unreg["BoomGateID"];
                                    obj.BoomGateCode = dt_unreg["BoomGateCode"].ToString();

                                    autoUnregisterMembership.Add(obj);
                                }//end foreach  
                                
                                //unregister kartu lain berdasarkan unit yang dijual disini

                         


                            }//end if
                        }
                    }
                }

                //return "keluar";
                //add bili
                if (autoUnregisterMembership.Count > 0)
                {
                    List<TempCardNumber> temp = new List<TempCardNumber>();
                    foreach (MembershipOwnership unreg in autoUnregisterMembership.OrderBy(c => c.MembershipID))
                    {
                        int intemp = (from d in temp
                                      where d.CardNumber == unreg.CardNumber
                                      select d).ToList().Count();

                        if (intemp <= 0)
                        {
                            var autoUnregCardList = from card in autoUnregisterMembership
                                                    where card.MembershipID == unreg.MembershipID
                                                    select card;

                            String[] returnvalue = dbclass.save_card_detail_unregister(OrgID, SiteID, 0, unreg.CardNumber, unreg.ChipNumber, "System");
                            int TRCardDetailID = int.Parse(returnvalue[1]);

                            
                            foreach (var c in autoUnregCardList)
                            {
                                //new 5 januari
                                string _custName = "";
                                string _unitCode = "";
                                string _unitNo="";
                                int? _cardTypeId = null;
                                string boomgateCardType = "";
                                DataTable dtcd = dbclass.retrieve_membershipcard_detail(c.CardNumber);

                                if (dtcd.Rows.Count > 0)
                                {
                                    _custName = dtcd.Rows[0]["HolderName"].ToString();
                                    _unitCode = dtcd.Rows[0]["UnitCode"].ToString();
                                    _unitNo = dtcd.Rows[0]["UnitNo"].ToString();
                                    _cardTypeId = (int)dtcd.Rows[0]["CardTypeID"];

                                    if (_cardTypeId == 1)//resident
                                        boomgateCardType = "R";
                                    else if (_cardTypeId == 2)
                                        boomgateCardType = "M";                                    
                                }

                                try
                                {
                                    //push data boom gate
                                    //insert cluster access
                                    PushDataBoomGate.PushData(c.BoomGateCode, c.ChipNumber, _custName, "Cluster: " + _unitCode + ",Unit No : " + _unitNo, "DELETE", boomgateCardType, c.CardNumber, null, PSCode, DateTime.Now.ToLocalTime());
                                    string ret_value = dbclass.insert_transaction_cluster_access(OrgID, SiteID, TRCardDetailID, c.BoomGateID, 2);
                                    string returnval_unreg = dbclass.update_unregister_card(c.MembershipID, c.BoomGateID, 2);
                                }
                                catch (Exception e)
                                {
                                    string ret_value = dbclass.insert_transaction_cluster_access(OrgID, SiteID, TRCardDetailID, c.BoomGateID, 1);
                                    string returnval_unreg = dbclass.update_unregister_card(c.MembershipID, c.BoomGateID, 1);
                                    int retunpushdata = dbclass.save_pushdata_error(c.BoomGateCode, c.ChipNumber, "cusname", "Cluster: " + _unitCode + ",Unit No : " + _unitNo, "DELETE", e.Message.ToString());
                                }
                            }
                        }
                        TempCardNumber tempcardnumber = new TempCardNumber();
                        tempcardnumber.CardNumber = unreg.CardNumber;
                        temp.Add(tempcardnumber);
                    }
                }
                //end bili
            }

            //return "keluar";
            //end

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);

            Int16 Duration = 0;
            if (dt != null && dt.Rows.Count != 0)
                Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());

            DateTime OverdueDate = RequestDate.AddDays(Duration);

            //cek box
            bool IsIdentitasDiri = false;
            bool IsKK = false;
            bool IsSTNK = false;
            bool IsKepemilikanUnit = false;
            bool IsPerjanjianSewaMenyewaUnit = false;
            bool IsSuratKeteranganKaryawan = false;
            bool IsBuktiBayarUtilitas = false;
            bool IsSuratKuasa = false;
            bool IsKTPPemberiPenerimaKuasa = false;
            string lainlain = "";



            IsIdentitasDiri = (CheckBox_identitasdiri.Checked ? true : false);
            string IsIdentitasDiri_fileName = null;
            string IsIdentitasDiri_fileType = null;
            byte[] IsIdentitasDiri_fileData = null;
            if (IsIdentitasDiri & FileUpload_identitasdiri.HasFile)
            {
                IsIdentitasDiri_fileName = Path.GetFileName(FileUpload_identitasdiri.PostedFile.FileName);                
                IsIdentitasDiri_fileType = FileUpload_identitasdiri.PostedFile.ContentType;
                int fileSize = FileUpload_identitasdiri.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_identitasdiri.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize/1024) <= (3*1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_identitasdiri.PostedFile.InputStream;
                    IsIdentitasDiri_fileData = upload_file(IsIdentitasDiri_fileName, IsIdentitasDiri_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsIdentitasDiri_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            IsKK = (CheckBox_kk.Checked ? true : false);
            string IsKK_fileName = null;
            string IsKK_fileType = null;
            byte[] IsKK_fileData = null;
            if (IsKK & FileUpload_kk.HasFile)
            {
                IsKK_fileName = Path.GetFileName(FileUpload_kk.PostedFile.FileName);
                IsKK_fileType = FileUpload_kk.PostedFile.ContentType;
                int fileSize = FileUpload_kk.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_kk.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_kk.PostedFile.InputStream;
                    IsKK_fileData = upload_file(IsKK_fileName, IsKK_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsKK_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            IsSTNK = (CheckBox_stnk.Checked ? true : false);
            string IsSTNK_fileName = null;
            string IsSTNK_fileType = null;
            byte[] IsSTNK_fileData = null;
            if (IsSTNK & FileUpload_stnk.HasFile)
            {
                IsSTNK_fileName = Path.GetFileName(FileUpload_stnk.PostedFile.FileName);
                IsSTNK_fileType = FileUpload_stnk.PostedFile.ContentType;
                int fileSize = FileUpload_stnk.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_stnk.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_stnk.PostedFile.InputStream;
                    IsSTNK_fileData = upload_file(IsSTNK_fileName, IsSTNK_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsSTNK_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            IsKepemilikanUnit = (CheckBox_kepemilikanunit.Checked ? true : false);
            string IsKepemilikanUnit_fileName = null;
            string IsKepemilikanUnit_fileType = null;
            byte[] IsKepemilikanUnit_fileData = null;
            if (IsKepemilikanUnit & FileUpload_kepemilikanunit.HasFile)
            {
                IsKepemilikanUnit_fileName = Path.GetFileName(FileUpload_kepemilikanunit.PostedFile.FileName);
                IsKepemilikanUnit_fileType = FileUpload_kepemilikanunit.PostedFile.ContentType;
                int fileSize = FileUpload_kepemilikanunit.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_kepemilikanunit.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_kepemilikanunit.PostedFile.InputStream;
                    IsKepemilikanUnit_fileData = upload_file(IsKepemilikanUnit_fileName, IsKepemilikanUnit_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsKepemilikanUnit_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            IsPerjanjianSewaMenyewaUnit = (CheckBox_perjanjiansewamenyewaunit.Checked ? true : false);
            string IsPerjanjianSewaMenyewaUnit_fileName = null;
            string IsPerjanjianSewaMenyewaUnit_fileType = null;
            byte[] IsPerjanjianSewaMenyewaUnit_fileData = null;
            if (IsPerjanjianSewaMenyewaUnit & FileUpload_perjanjiansewamenyewaunit.HasFile)
            {
                IsPerjanjianSewaMenyewaUnit_fileName = Path.GetFileName(FileUpload_perjanjiansewamenyewaunit.PostedFile.FileName);
                IsPerjanjianSewaMenyewaUnit_fileType = FileUpload_perjanjiansewamenyewaunit.PostedFile.ContentType;
                int fileSize = FileUpload_perjanjiansewamenyewaunit.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_perjanjiansewamenyewaunit.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_perjanjiansewamenyewaunit.PostedFile.InputStream;
                    IsPerjanjianSewaMenyewaUnit_fileData = upload_file(IsPerjanjianSewaMenyewaUnit_fileName, IsPerjanjianSewaMenyewaUnit_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsPerjanjianSewaMenyewaUnit_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            IsSuratKeteranganKaryawan = (CheckBox_suratketerangankaryawan.Checked ? true : false);
            string IsSuratKeteranganKaryawan_fileName = null;
            string IsSuratKeteranganKaryawan_fileType = null;
            byte[] IsSuratKeteranganKaryawan_fileData = null;
            if (IsSuratKeteranganKaryawan & FileUpload_suratketerangankaryawan.HasFile)
            {
                IsSuratKeteranganKaryawan_fileName = Path.GetFileName(FileUpload_suratketerangankaryawan.PostedFile.FileName);
                IsSuratKeteranganKaryawan_fileType = FileUpload_suratketerangankaryawan.PostedFile.ContentType;
                int fileSize = FileUpload_suratketerangankaryawan.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_suratketerangankaryawan.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_suratketerangankaryawan.PostedFile.InputStream;
                    IsSuratKeteranganKaryawan_fileData = upload_file(IsSuratKeteranganKaryawan_fileName, IsSuratKeteranganKaryawan_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsSuratKeteranganKaryawan_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            IsBuktiBayarUtilitas = (CheckBox_buktibayarutilitas.Checked ? true : false);
            string IsBuktiBayarUtilitas_fileName = null;
            string IsBuktiBayarUtilitas_fileType = null;
            byte[] IsBuktiBayarUtilitas_fileData = null;
            if (IsBuktiBayarUtilitas & FileUpload_buktibayarutilitas.HasFile)
            {
                IsBuktiBayarUtilitas_fileName = Path.GetFileName(FileUpload_buktibayarutilitas.PostedFile.FileName);
                IsBuktiBayarUtilitas_fileType = FileUpload_buktibayarutilitas.PostedFile.ContentType;
                int fileSize = FileUpload_buktibayarutilitas.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_buktibayarutilitas.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_buktibayarutilitas.PostedFile.InputStream;
                    IsBuktiBayarUtilitas_fileData = upload_file(IsBuktiBayarUtilitas_fileName, IsBuktiBayarUtilitas_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsBuktiBayarUtilitas_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            IsSuratKuasa = (CheckBox_suratkuasa.Checked ? true : false);
            string IsSuratKuasa_fileName = null;
            string IsSuratKuasa_fileType = null;
            byte[] IsSuratKuasa_fileData = null;
            if (IsSuratKuasa & FileUpload_suratkuasa.HasFile)
            {
                IsSuratKuasa_fileName = Path.GetFileName(FileUpload_suratkuasa.PostedFile.FileName);
                IsSuratKuasa_fileType = FileUpload_suratkuasa.PostedFile.ContentType;
                int fileSize = FileUpload_suratkuasa.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_suratkuasa.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_suratkuasa.PostedFile.InputStream;
                    IsSuratKuasa_fileData = upload_file(IsSuratKuasa_fileName, IsSuratKuasa_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsSuratKuasa_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            IsKTPPemberiPenerimaKuasa = (CheckBox_ktppemberipenerimakuasa.Checked ? true : false);
            string IsKTPPemberiPenerimaKuasa_fileName = null;
            string IsKTPPemberiPenerimaKuasa_fileType = null;
            byte[] IsKTPPemberiPenerimaKuasa_fileData = null;
            if (IsKTPPemberiPenerimaKuasa & FileUpload_ktppemberipenerimakuasa.HasFile)
            {
                IsKTPPemberiPenerimaKuasa_fileName = Path.GetFileName(FileUpload_ktppemberipenerimakuasa.PostedFile.FileName);
                IsKTPPemberiPenerimaKuasa_fileType = FileUpload_ktppemberipenerimakuasa.PostedFile.ContentType;
                int fileSize = FileUpload_ktppemberipenerimakuasa.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_ktppemberipenerimakuasa.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_ktppemberipenerimakuasa.PostedFile.InputStream;
                    IsKTPPemberiPenerimaKuasa_fileData = upload_file(IsKTPPemberiPenerimaKuasa_fileName, IsKTPPemberiPenerimaKuasa_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = IsKTPPemberiPenerimaKuasa_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }
            lainlain = txt_lainlain.Text;
            string lainlain_fileName = null;
            string lainlain_fileType = null;
            byte[] lainlain_fileData = null;
            if (lainlain.Length>0 & FileUpload_lainlain.HasFile)
            {
                lainlain_fileName = Path.GetFileName(FileUpload_lainlain.PostedFile.FileName);
                lainlain_fileType = FileUpload_lainlain.PostedFile.ContentType;
                int fileSize = FileUpload_lainlain.PostedFile.ContentLength;
                string fileExtention = Path.GetExtension(FileUpload_lainlain.PostedFile.FileName);
                bool allowedExtentionFile = validate_extention_file(fileExtention);
                if ((fileSize / 1024) <= (3 * 1024) && allowedExtentionFile)
                {
                    Stream fs = FileUpload_lainlain.PostedFile.InputStream;
                    lainlain_fileData = upload_file(lainlain_fileName, lainlain_fileType, fs);
                }
                else
                {
                    reset_document_checkbox();
                    string uploadMessage = lainlain_fileName + " file size is too large or the extension file not support";
                    return uploadMessage;
                }
            }


            int TotalAmount = int.Parse(txt_total.Text);
            //string ResidentStatus = ddl_resident_status.SelectedValue;
            int CardTypeID = int.Parse(ddl_card_type.SelectedValue);

            int quantity = int.Parse(txt_quantity.Text);
            string residentstatus = ddl_resident_status.SelectedValue;
            DateTime? rentperiodedate = null;
            if (residentstatus.ToLower() == "rent" && ddl_card_type.SelectedValue=="1")
            {
                rentperiodedate = DateTime.Parse(txt_rent_periode_date.Text);
            }
            int price = int.Parse(txt_price.Text);

            Int32 PeriodNo = 1; //dummy
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheadersmartcard.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            //request.ActualPrice = ActualAmount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            /*if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            }*/

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                //cek apakah replacement
                int transactionType = 0; // 0 pembuatan baru 1:replacement
                transactionType = (CheckBox_isreplacement.Checked ? 1 : 0);

                if (transactionType == 1)
                {
                    price = 0;
                    TotalAmount = 0;
                }

                string returnvalue = "";
                returnvalue = dbclass.save_smartcard_transaction(OrgID, SiteID, transactionType, CRMCaseID, CaseNumber, 1, HelpID,
                            CaseOriginID, entryheadersmartcard.CustomerType, entryheadersmartcard.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                            RequestDescription, OverdueDate, quantity, residentstatus, rentperiodedate, CardTypeID, entryheadersmartcard.RequestorEmail, entryheadersmartcard.RequestorPhone, entryheadersmartcard.RequestorPhone, null, price, TotalAmount,
                            IsIdentitasDiri, IsIdentitasDiri_fileName, IsIdentitasDiri_fileType, IsIdentitasDiri_fileData,
                            IsKK, IsKK_fileName, IsKK_fileType, IsKK_fileData,
                            IsSTNK, IsSTNK_fileName, IsSTNK_fileType, IsSTNK_fileData,
                            IsKepemilikanUnit, IsKepemilikanUnit_fileName, IsKepemilikanUnit_fileType, IsKepemilikanUnit_fileData,
                            IsPerjanjianSewaMenyewaUnit, IsPerjanjianSewaMenyewaUnit_fileName, IsPerjanjianSewaMenyewaUnit_fileType, IsPerjanjianSewaMenyewaUnit_fileData,
                            IsSuratKeteranganKaryawan, IsSuratKeteranganKaryawan_fileName, IsSuratKeteranganKaryawan_fileType, IsSuratKeteranganKaryawan_fileData,
                            IsBuktiBayarUtilitas, IsBuktiBayarUtilitas_fileName, IsBuktiBayarUtilitas_fileType, IsBuktiBayarUtilitas_fileData,
                            IsSuratKuasa, IsSuratKuasa_fileName, IsSuratKuasa_fileType, IsSuratKuasa_fileData,
                            IsKTPPemberiPenerimaKuasa, IsKTPPemberiPenerimaKuasa_fileName, IsKTPPemberiPenerimaKuasa_fileType, IsKTPPemberiPenerimaKuasa_fileData,
                            lainlain, lainlain_fileName, lainlain_fileType, lainlain_fileData,
                            PeriodNo, IsSetted, SavedBy, FullName);

                if (transactionType == 1)
                {
                    //update case to assign to pic
                    string Remarks = "Request replacement smartcard";
                    Int32 returnvalutickethistory = dbclass.save_tickethistory(CaseNumber, 2, FullName, Remarks, SavedBy, null);
                    /*update_to_crm(CaseNumber, 2, Remarks, FullName, null, 0);*/
                }

                //if (!isReplacement)
                //{
                //    //pembuatan baru : transaction type : 0 hardcoded
                //    returnvalue = dbclass.save_smartcard_transaction(OrgID, SiteID, 0, CRMCaseID, CaseNumber, 1, HelpID,
                //            CaseOriginID, entryheadersmartcard.CustomerType, entryheadersmartcard.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                //            RequestDescription, OverdueDate, quantity, residentstatus, rentperiodedate, CardTypeID, entryheadersmartcard.RequestorEmail, entryheadersmartcard.RequestorPhone, entryheadersmartcard.RequestorPhone, null, price, TotalAmount,
                //            IsIdentitasDiri, IsIdentitasDiri_fileName, IsIdentitasDiri_fileType, IsIdentitasDiri_fileData,
                //            IsKK, IsKK_fileName, IsKK_fileType, IsKK_fileData, 
                //            IsSTNK, IsSTNK_fileName, IsSTNK_fileType, IsSTNK_fileData, 
                //            IsKepemilikanUnit, IsKepemilikanUnit_fileName, IsKepemilikanUnit_fileType, IsKepemilikanUnit_fileData, 
                //            IsPerjanjianSewaMenyewaUnit, IsPerjanjianSewaMenyewaUnit_fileName, IsPerjanjianSewaMenyewaUnit_fileType, IsPerjanjianSewaMenyewaUnit_fileData,
                //            IsSuratKeteranganKaryawan, IsSuratKeteranganKaryawan_fileName, IsSuratKeteranganKaryawan_fileType, IsSuratKeteranganKaryawan_fileData, 
                //            IsBuktiBayarUtilitas, IsBuktiBayarUtilitas_fileName, IsBuktiBayarUtilitas_fileType, IsBuktiBayarUtilitas_fileData, 
                //            IsSuratKuasa, IsSuratKuasa_fileName, IsSuratKuasa_fileType, IsSuratKuasa_fileData, 
                //            IsKTPPemberiPenerimaKuasa, IsKTPPemberiPenerimaKuasa_fileName, IsKTPPemberiPenerimaKuasa_fileType, IsKTPPemberiPenerimaKuasa_fileData, 
                //            lainlain, lainlain_fileName, lainlain_fileType, lainlain_fileData,
                //            PeriodNo, IsSetted, SavedBy, FullName);
                //}
                //else
                //{
                //    //replacement : transactionType : 1 hardcoded
                //    returnvalue = dbclass.save_smartcard_replacement_transaction(OrgID, SiteID, 1, CRMCaseID, CaseNumber, 1, HelpID,
                //            CaseOriginID, entryheadersmartcard.CustomerType, entryheadersmartcard.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                //            RequestDescription, OverdueDate, quantity, residentstatus, rentperiodedate, CardTypeID, entryheadersmartcard.RequestorEmail, entryheadersmartcard.RequestorPhone, entryheadersmartcard.RequestorPhone, null, price, TotalAmount,
                //            IsIdentitasDiri, IsKK, IsSTNK, IsKepemilikanUnit, IsPerjanjianSewaMenyewaUnit, IsSuratKeteranganKaryawan, IsBuktiBayarUtilitas, IsSuratKuasa, IsKTPPemberiPenerimaKuasa, lainlain,
                //            PeriodNo, IsSetted, SavedBy, FullName);

                //    //update case to assign to pic
                //    string Remarks = "Request replacement smartcard";
                //    Int32 returnvalutickethistory = dbclass.save_tickethistory(CaseNumber, 2, FullName, Remarks, SavedBy, null);
                //    update_to_crm(CaseNumber, 2, Remarks, FullName, null, 0);
                //}

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
     
        protected void load_card_type()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_master_card_type();

            ddl_card_type.DataSource = dt;
            ddl_card_type.DataValueField = "SmartCardTypeID";
            ddl_card_type.DataTextField = "SmartCardTypeName";
            ddl_card_type.DataBind();

        }

        protected void load_resident_status()
        {
            List<string> resident_status = new List<string>();
            resident_status.Add("Owner");
            resident_status.Add("Rent");

            ddl_resident_status.DataSource = resident_status;
            ddl_resident_status.DataBind();
        }

        protected void btn_calculate_Click(object sender, EventArgs e)
        {
            int cardtypeid = int.Parse(ddl_card_type.SelectedValue);
            int quantity = int.Parse(txt_quantity.Text);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_smartcard_price(cardtypeid);

            if (dt.Rows.Count > 0)
            {
                int price = (int)dt.Rows[0]["Price"];
                int total = quantity * price;
                txt_price.Text = dt.Rows[0]["Price"].ToString();
                txt_total.Text = total.ToString();
            }
        }

        protected void btn_reset_Click(object sender, EventArgs e)
        {
            txt_iscalculate.Text = "false";
            txt_total.Text = "";
            btn_calculate.Enabled = true;
        }

        protected void disablecontrol()
        {
            ddl_card_type.Enabled = false;
            ddl_resident_status.Enabled = false;
            txt_total.ReadOnly = true;
            txt_quantity.ReadOnly = true;
        }

        protected void calculateprice()
        {
            int cardtypeid = int.Parse(ddl_card_type.SelectedValue);
            int quantity = int.Parse(txt_quantity.Text);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_smartcard_price(cardtypeid);

            if (dt.Rows.Count > 0)
            {
                int price = (int)dt.Rows[0]["Price"];
                int total = quantity * price;
                txt_price.Text = dt.Rows[0]["Price"].ToString();
                txt_total.Text = total.ToString();
            }
        }

        protected void txt_quantity_TextChanged(object sender, EventArgs e)
        {
            calculateprice();
        }

        protected void ddl_card_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            calculateprice();

            string resident_status = "";
            string card_type = "";
            txt_rent_periode_date.Text = "";
            txt_rent_periode_date.Visible = false;
            lbl_rent_periode_date.Visible = false;
            resident_status = ddl_resident_status.SelectedValue;
            card_type = ddl_card_type.SelectedValue;

            if (resident_status.ToLower() == "rent" && card_type == "1")
            {
                txt_rent_periode_date.Visible = true;
                lbl_rent_periode_date.Visible = true;
            }
        }

        //private void update_to_crm(String _casenumber, Int32 _crmcasestatusid, String _remarks, String _updatedby, String _workedby, Int32 _SatisfactionID)
        //{
        //    String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
        //    String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
        //    String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
        //    String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
        //    String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();

        //    CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

        //    UpdateCaseHelpDeskStatusRequest request = new UpdateCaseHelpDeskStatusRequest();
        //    request.CRMCaseNumber = _casenumber;
        //    request.CRMCaseStatusID = _crmcasestatusid;
        //    request.Remarks = _remarks;
        //    request.UpdateBy = _updatedby;
        //    request.UpdateDate = DateTime.Now;
        //    request.WorkedBy = _workedby;
        //    request.SatisfactionID = _SatisfactionID;
        //    UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(request);
        //}

        private void bound_smartcardhistory()
        {
            gvw_smartcardhistory.DataSource = Session["SmartCardHistory"];
            gvw_smartcardhistory.DataBind();
        }

        protected void CheckBox_isreplacement_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_isreplacement.Checked)
            {
                txt_price.Visible = false;
                txt_total.Visible = false;
                lbl_price.Visible = false;
                lbl_total.Visible = false;
            }
            else
            {
                txt_price.Visible = true;
                txt_total.Visible = true;
                lbl_price.Visible = true;
                lbl_total.Visible = true;
            }
            
        }

        protected void ddl_resident_status_TextChanged(object sender, EventArgs e)
        {
            string resident_status = "";
            string card_type="";
            txt_rent_periode_date.Text = "";
            txt_rent_periode_date.Visible = false;
            lbl_rent_periode_date.Visible = false;
            resident_status = ddl_resident_status.SelectedValue;
            card_type = ddl_card_type.SelectedValue;

            if (resident_status.ToLower() == "rent" && card_type=="1")
            {
                txt_rent_periode_date.Visible = true;
                lbl_rent_periode_date.Visible = true;
            }
        }

        private byte[] upload_file(string _fileName, string _fileType, Stream _fs)
        {
            BinaryReader br = new BinaryReader(_fs);
            byte[] bytes = br.ReadBytes((Int32)_fs.Length);

            br.Close();
            _fs.Close();

            return bytes;            
        }

        //private Byte[] get_file_content(Stream _stream)
        //{
        //    Stream fs = _stream;
        //    BinaryReader br = new BinaryReader(fs);
        //    Int32 Int = Convert.ToInt32(fs.Length);
        //    byte[] bytes = br.ReadBytes(Int);
        //    return bytes;
        //}

        private bool validate_extention_file(string extentionFile) 
        {
            string[] allowedExtentianFile = { ".jpg", "jpeg", ".png",".pdf" };

            if (allowedExtentianFile.Contains(extentionFile.ToLower()))
                return true;
            else
                return false;                  
        }

        private void reset_document_checkbox()
        {
            CheckBox_identitasdiri.Checked = false;
            CheckBox_kk.Checked = false;
            CheckBox_stnk.Checked = false;
            CheckBox_kepemilikanunit.Checked = false;
            CheckBox_perjanjiansewamenyewaunit.Checked = false;
            CheckBox_suratketerangankaryawan.Checked = false;
            CheckBox_buktibayarutilitas.Checked = false;
            CheckBox_suratkuasa.Checked = false;
            CheckBox_ktppemberipenerimakuasa.Checked = false;
            txt_lainlain.Text = "";
        }

        //protected void lnkbtn_save_Click(object sender, EventArgs e)
        //{
        //    if (transaction_validation())
        //    {
        //        String res = save_transaction();
        //        if (!String.IsNullOrEmpty(res))
        //        {
        //            entryheadersmartcard.ShowAlert = res;
        //        }
        //        else
        //            Response.Redirect(prevPage);
        //    }
        //}

        protected void buttom_save_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheadersmartcard.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        protected void btn_load_history_byunit_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            string[] unit = entryheadersmartcard.Unit.Split('-');
            string UnitCode="";
            string UnitNo="";
            if (unit.Length == 2)
            {
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }

            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
            entryheadersmartcard.retrieve_smartcard_history_byunit(OrgID, SiteID, UnitCode, UnitNo);

            bound_smartcardhistory();
        }


    }
}