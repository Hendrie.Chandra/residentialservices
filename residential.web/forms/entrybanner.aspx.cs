﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using System.Text.RegularExpressions;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entrybanner : System.Web.UI.Page
    {
        static String prevPage = "banner.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                Session["ListBannerDetail"] = null;
                Session["dsBookedDate"] = null;
                txt_date_CalendarExtender.StartDate = DateTime.Now.AddDays(1);
                show_type();                
                hitung_total();

                gvw_data.DataSource = null;
                gvw_data.DataBind();

                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
                else
                    updaterequest.Visible = false;
            }            
            entryheader.ToggleAlert = false;

            //add by bili 6 juni 2017
            ScriptManager.RegisterStartupScript(this, this.GetType(), "inputAdjusment", "inputAdjusment();", true);
            txt_adjustment.Style.Add("text-align", "right");
        }

        private void Show_Case(String _casenumber)
        {
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                DataTable dt = dbclass.retrieve_banner_by_casenumber(_casenumber);

                entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                //check customer type -> 0 = Owner, 1 = Tenant
                entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheader.combobox_unit();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

                entryheader.disable_all_controls();

                DataTable dtHelp = dbclass.retrieve_help((Int32)dt.Rows[0]["HelpID"]);
                String BannerType = dtHelp.Rows[0]["HelpName"].ToString();

                if (BannerType == ConfigurationManager.AppSettings["VerticalNewBannerHelpID"])
                    cboType.SelectedIndex = 1;
                else if (BannerType == ConfigurationManager.AppSettings["WhatOnNewBannerHelpID"])
                    cboType.SelectedIndex = 2;

                cboType_SelectedIndexChanged(this, EventArgs.Empty);

                //lbl_adjustment.Text = Int32.Parse(dt.Rows[0]["DiscountAmount"].ToString(), NumberStyles.Currency).ToString();
                txt_adjustment.Text = Int32.Parse(dt.Rows[0]["DiscountAmount"].ToString(), NumberStyles.Currency).ToString();
                hdf_adjustment.Value = Int32.Parse(dt.Rows[0]["DiscountAmount"].ToString(), NumberStyles.Currency).ToString();

                List<BannerDetail> list = new List<BannerDetail>();
                BannerDetail bd = new BannerDetail();

                if (Session["ListBannerDetail"] == null)
                {
                    DataTable dt3 = dbclass.retrieve_banner_detail(_casenumber);

                    int i = 1;
                    foreach (DataRow dr in dt3.Rows)
                    {
                        bd = new BannerDetail();
                        bd.BannerDetailID = i;
                        bd.LocationID = Int32.Parse(dr["BannerLocationID"].ToString());
                        bd.LocationName = dr["BannerLocationName"].ToString();
                        bd.PointID = Int32.Parse(dr["PointID"].ToString());
                        bd.PointName = dr["PointName"].ToString();
                        bd.BannerTypeID = Int32.Parse(dr["BannerTypeID"].ToString());
                        bd.BannerTypeName = dr["BannerTypeName"].ToString();
                        bd.StartDate = DateTime.Parse(dr["StartDate"].ToString());
                        bd.ToDate = DateTime.Parse(dr["EndDate"].ToString());
                        bd.Price = Convert.ToInt32(decimal.Parse(dr["Price"].ToString()));

                        list.Add(bd);
                        i++;
                    }
                    Session["ListBannerDetail"] = list;
                }

                gvw_data.DataSource = list;
                gvw_data.DataBind();

                if (list.Count > 0)
                    cboType.Enabled = false;
                else cboType.Enabled = true;

                hitung_total();
            }
            catch (Exception)
            {
                throw new HttpException(404, "Page not found");
            }
        }

        private void show_type()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_banner_type();
            cboType.DataSource = dt;
            cboType.DataTextField = "BannerTypeName";
            cboType.DataValueField = "BannerTypeID";
            cboType.DataBind();
            cboType.Items.Insert(0, new ListItem("None", "0"));
            cboType.SelectedIndex = 0;
        }

        private void show_location()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_location(userData.OrgID, userData.SiteID, Int32.Parse(cboType.SelectedValue));
            cboLocation.DataSource = dt;
            cboLocation.DataTextField = "BannerLocationName";
            cboLocation.DataValueField = "BannerLocationID";
            cboLocation.DataBind();
            cboLocation.Items.Insert(0, new ListItem("None", "0"));
            cboLocation.SelectedIndex = 0;
        }

        private void show_point(Int32 LocationID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_point(LocationID);
            cboPoint.DataSource = dt;
            cboPoint.DataTextField = "PointName";
            cboPoint.DataValueField = "PointID";
            cboPoint.DataBind();
            cboPoint.Items.Insert(0, new ListItem("None", "0"));
            cboPoint.SelectedIndex = 0;
        }        

        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        protected void cboLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboLocation.SelectedIndex == 0)
            {
                btn_viewBookedTable.Enabled = false;
                return;
            }

            btn_viewBookedTable.Enabled = true;
            show_point(Int32.Parse(cboLocation.SelectedValue.ToString()));

        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btn_add_detail_Click(object sender, EventArgs e)
        {
            DateTime startDate;
            DateTime toDate;

            if (cboLocation.SelectedIndex <= 0)
            {                
                entryheader.ShowAlert = "Location is Required";
                return;
            }
            if (cboPoint.SelectedIndex <= 0)
            {
                entryheader.ShowAlert = "Point is Required";                
                return;
            }
            if (txtFromDate.Text == "")
            {
                entryheader.ShowAlert = "'From Date' is Required";                
                return;
            }
            else
            {
                try
                {
                    startDate = DateTime.Parse(txtFromDate.Text.ToString());
                }
                catch (Exception)
                {
                    entryheader.ShowAlert = "'From Date' Invalid Date Format";                    
                    return;
                }

                if (startDate < DateTime.Today)
                {
                    entryheader.ShowAlert = "'From Date' is Not Valid";                    
                    return;
                }
            }
            if (txtToDate.Text == "")
            {
                entryheader.ShowAlert = "'To Date' is Required";                
                return;
            }
            else
            {
                try
                {
                    toDate = DateTime.Parse(txtToDate.Text.ToString());
                }
                catch (Exception)
                {
                    entryheader.ShowAlert = "'To Date' Invalid Date Format";                    
                    return;
                }

                if (toDate < DateTime.Today)
                {
                    entryheader.ShowAlert = "'To Date' is Not Valid";                    
                    return;
                }
            }

            if (startDate > toDate)
            {
                entryheader.ShowAlert = "'From Date' Should Not Exceed The 'To Date'";                
                return;
            }

            Int32 error = -1;
            DataTable dsBookedDate = (DataTable)Session["dsBookedDate"];
            foreach (DataRow dr in dsBookedDate.Rows)
            {
                var _startDate = DateTime.Parse(dr["StartDate"].ToString()).Date;
                var _endDate = DateTime.Parse(dr["EndDate"].ToString()).Date;

                foreach (DateTime day in EachDay(_startDate, _endDate))
                {
                    foreach (DateTime dayEntry in EachDay(startDate, toDate))
                    {
                        if (day.Date == dayEntry.Date)
                            error++;

                        if (error >= 0)
                            break;
                    }
                }
            }

            if (error >= 0)
            {
                entryheader.ShowAlert = "Point and Location on That Date Was Already Booked";                
                return;
            }

            List<BannerDetail> list = null;
            Int32 counter = 0;

            if (Session["ListBannerDetail"] == null)
            {
                list = new List<BannerDetail>();
                counter = 1;
            }
            else
            {
                list = (List<BannerDetail>)Session["ListBannerDetail"];
                if (list.Count > 0)
                {
                    Int32 maxObj = list.Max(r => r.BannerDetailID);
                    counter = maxObj + 1;
                }
                else counter = 1;
            }

            Int32 lineId = counter;

            int found = -1;
            foreach (BannerDetail obj in list)
            {
                if (obj.PointID == Int32.Parse(cboPoint.SelectedValue))
                {
                    foreach (DateTime day in EachDay(obj.StartDate, obj.ToDate))
                    {
                        foreach (DateTime dayEntry in EachDay(startDate, toDate))
                        {
                            if (day.Date == dayEntry.Date)
                                found++;

                            if (found >= 0)
                                break;
                        }
                    }
                }                
            }

            if (found >= 0)
            {
                entryheader.ShowAlert = "Data Exist!";                
                return;
            }
            
            Int32 days = 1 + Int32.Parse((toDate - startDate).TotalDays.ToString());

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_banner_location(Int32.Parse(cboLocation.SelectedValue));

            Int32 PriceSewaTempat = 0;
            if (dt != null && dt.Rows.Count > 0)
                PriceSewaTempat = Int32.Parse(dt.Rows[0]["Price"].ToString());

            Int32 sewatempat = days * PriceSewaTempat;

            BannerDetail md = new BannerDetail(
                lineId,
                Int32.Parse(cboLocation.SelectedValue.ToString()),
                cboLocation.SelectedItem.Text.ToString(),
                Int32.Parse(cboPoint.SelectedValue.ToString()),
                cboPoint.SelectedItem.Text.ToString(),
                Int32.Parse(cboType.SelectedValue.ToString()),
                cboType.SelectedItem.Text.ToString(),
                startDate,
                toDate,
                sewatempat);

            list.Add(md);

            Session["ListBannerDetail"] = list;            

            hitung_total();

            //cboLocation.SelectedIndex = 0;
            cboPoint.SelectedIndex = 0;
            txtFromDate.Text = String.Empty;
            txtToDate.Text = String.Empty;            
            PriceSewaTempat = 0;

            Session["dsBookedDate"] = null;

            cboType.Enabled = false;

            gvw_data.DataSource = list;
            gvw_data.DataBind();
        }

        private void hitung_total()
        {
            Int32 discount = 0;            
            Int32 total = 0;
            Int32 biayapasang = 0;
            Int32 sewatempat = 0;
            Int32 subtotal = 0;
            Int32 ppn = 0;

            //add by bili
            if (!string.IsNullOrEmpty(hdf_adjustment.Value))
                discount = Int32.Parse(hdf_adjustment.Value.ToString());

            if (Session["ListBannerDetail"] != null)
            {
                var list = (List<BannerDetail>)Session["ListBannerDetail"];

                if (list.Count >= 1)
                {
                    foreach (BannerDetail md in list)
                        sewatempat += md.Price;
                }

                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DataTable dt = dbclass.retrieve_generalprice("Biaya Pasang Banner", userData.SiteID);

                Int32 price = 0;
                if (dt != null && dt.Rows.Count > 0)
                    price = Convert.ToInt32(dt.Rows[0]["Price"]);

                biayapasang = list.Count * price;
            }            

            ppn = (sewatempat + biayapasang) * 10 / 100;
            subtotal = sewatempat + biayapasang + ppn;     
            total = subtotal - discount;

            lbl_sewatempat.Text = sewatempat.ToString("N0");
            lbl_biayapasang.Text = biayapasang.ToString("N0");
            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_ppn.Text = ppn.ToString("N0");
            //lbl_adjustment.Text = discount.ToString();
            txt_adjustment.Text = discount.ToString();
            lbl_total.Text = total.ToString("N0");
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            var list = (List<BannerDetail>)Session["ListBannerDetail"];

            if (list != null)
            {
                foreach (GridViewRow gvr in gvw_data.Rows)
                {
                    CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                    if (chkselect.Checked == true)
                    {
                        Int32 BannerDetailID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());

                        var itemToRemove = list.SingleOrDefault(r => r.BannerDetailID == BannerDetailID);
                        if (itemToRemove != null)
                            list.Remove(itemToRemove);
                    }
                }

                Session["ListBannerDetail"] = list;
                gvw_data.DataSource = list;
                gvw_data.DataBind();

                hitung_total();

                if (list.Count > 0)
                    cboType.Enabled = false;
                else cboType.Enabled = true;                
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;            
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            String ParameterHelpID = "";
            if (cboType.SelectedIndex == 1)
                ParameterHelpID = ConfigurationManager.AppSettings["VerticalNewBannerHelpID"].ToString();
            else if (cboType.SelectedIndex == 2)
                ParameterHelpID = ConfigurationManager.AppSettings["WhatOnNewBannerHelpID"].ToString();

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ParameterHelpID);
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = entryheader.CaseOriginID;            
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);
           
            Int32 Amount = Int32.Parse(lbl_sewatempat.Text, NumberStyles.Currency);
            Int32 InstallationFee = Int32.Parse(lbl_biayapasang.Text, NumberStyles.Currency);
            Double TaxPercentage = 0.1;
            Int32 TaxAmount = Int32.Parse(lbl_ppn.Text, NumberStyles.Currency);            
            Int32 AmountAfterTax = Int32.Parse(lbl_subtotal.Text, NumberStyles.Currency);
            Int32 DiscountTypeID = 0;
            //Int32 DiscountAmount = Int32.Parse(lbl_adjustment.Text);
            Int32 DiscountAmount = Int32.Parse((txt_adjustment.Text.Replace(".","")).Replace(",",""));

            Int32 ActualAmount = Int32.Parse(lbl_total.Text, NumberStyles.Currency);

            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = Amount;
            request.ActualPrice = ActualAmount;
            request.FullName = FullName;
            request.UserName = SavedBy;
            
            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                //request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;
                
                String returnvalue = dbclass.save_banner_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, Amount, InstallationFee, TaxPercentage, TaxAmount, AmountAfterTax, DiscountTypeID,
                    DiscountAmount, ActualAmount, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    dt = dbclass.retrieve_banner_by_casenumber(CaseNumber);
                    Int32 BannerID = Int32.Parse(dt.Rows[0]["BannerID"].ToString());

                    if (Request.QueryString["CaseNumber"] != null)
                        dbclass.delete_banner_detail_transaction(BannerID);

                    foreach (BannerDetail bd in (List<BannerDetail>)Session["ListBannerDetail"])
                        dbclass.save_banner_detail_transaction(OrgID, SiteID, BannerID, bd.BannerTypeID, bd.LocationID, bd.PointID, bd.StartDate, bd.ToDate, bd.Price, SavedBy);

                    Session["ListBannerDetail"] = null;

                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected bool transaction_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (cboType.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Banner Type is Required";               
                return false;
            }
            if (gvw_data.Rows.Count == 0)
            {
                entryheader.ShowAlert = "No Banners Booked";                
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }
            return true;
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        protected void txtFromDate_TextChanged(object sender, EventArgs e)
        {
            CalendarExtender1.StartDate = DateTime.Parse(txtFromDate.Text).AddDays(1);
            txtToDate.Focus();
        }

        protected void cboType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboPoint.Items.Clear();
            show_location();
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void boundBookedTable()
        {
            lbl_bannerLocation.Text = cboLocation.SelectedItem.Text;
            DateTime startRange = DateTime.Today.Date;
            if (!String.IsNullOrEmpty(txt_startDateRange.Text) && Regex.IsMatch(txt_startDateRange.Text, "^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$"))           
                startRange = DateTime.Parse(txt_startDateRange.Text);

            txt_startDateRange.Text = startRange.ToString("yyyy-MM-dd");
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);            

            DataTable dt_point = dbclass.retrieve_point(int.Parse(cboLocation.SelectedValue));

            //Clear recent table
            tbl_bookedBanner.Rows.Clear();

            //table header -> date
            TableHeaderRow headerDate = new TableHeaderRow();

            TableHeaderCell th_title = new TableHeaderCell();
            th_title.Text = "Point / Date";
            headerDate.Cells.Add(th_title);
            
            for (int x = 0; x <= 14; x++)
            {
                TableHeaderCell thc = new TableHeaderCell();
                thc.Text = startRange.AddDays(x).ToString("dd");
                headerDate.Cells.Add(thc);
            }

            tbl_bookedBanner.Rows.Add(headerDate);

            DataTable dt_bannerDetail = dbclass.retrieve_booked_banner(cboLocation.SelectedValue, txt_startDateRange.Text);

            //Row -> Point
            foreach (DataRow row in dt_point.Rows)
            {
                TableRow tr = new TableRow();
                TableCell tp = new TableCell();
                tp.Text = row["PointName"].ToString();
                tr.Cells.Add(tp);
                String PointID = row["PointID"].ToString();

                DataRow[] nPoint = dt_bannerDetail.Select("PointID = " + PointID);

                for (int j = 1; j <= 15; j++)
                {
                    bool booked = false;
                    foreach (DataRow rowPoint in nPoint)
                    {
                        DateTime startDate = DateTime.Parse(rowPoint["StartDate"].ToString());
                        DateTime endDate = DateTime.Parse(rowPoint["EndDate"].ToString());
                        if (startRange.AddDays(j).Date >= startDate && startRange.AddDays(j).Date <= endDate)
                            booked = true;
                    }
                    TableCell tp2 = new TableCell();
                    tp2.BackColor = (booked) ? System.Drawing.Color.Red : System.Drawing.Color.Green;
                    tr.Cells.Add(tp2);
                }
                tbl_bookedBanner.Rows.Add(tr);
            }           
        }
        
        protected void btn_viewBookedTable_Click(object sender, EventArgs e)
        {            
            boundBookedTable();
        }

        protected void btn_boundTable_Click(object sender, EventArgs e)
        {
            boundBookedTable();
        }

        protected void cboPoint_SelectedIndexChanged(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_booked_banner_point(cboPoint.SelectedValue);
            Session["dsBookedDate"] = dt;
        }

        protected void txt_adjustment_TextChanged(object sender, EventArgs e)
        {
            //total = sub total - adjustment
            Int32 subtotal = 0;
            Int32 adjustment = 0;
            Int32 total = 0;

            subtotal = int.Parse(lbl_subtotal.Text,NumberStyles.Currency);
            adjustment = int.Parse(this.txt_adjustment.Text, NumberStyles.Currency);
            total = subtotal - adjustment;

            lbl_total.Text = total.ToString("N0");


        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            var list = (List<BannerDetail>)Session["ListBannerDetail"];
            Session["ListBannerDetail"] = list;
            gvw_data.PageIndex = e.NewPageIndex;
            gvw_data.DataSource = list;
            gvw_data.DataBind();
        }
    }

    public class BannerDetail
    {
        public Int32 BannerDetailID { get; set; }
        public Int32 LocationID { get; set; }
        public string LocationName { get; set; }
        public Int32 PointID { get; set; }
        public string PointName { get; set; }
        public Int32 BannerTypeID { get; set; }
        public string BannerTypeName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime ToDate { get; set; }
        public Int32 Price { get; set; }

        public BannerDetail() { }

        public BannerDetail(
            Int32 bannerDetailID, Int32 locationID, string locationName, Int32 pointID, string pointName,
            Int32 bannerTypeID, string bannerTypeName, DateTime startDate, DateTime toDate, Int32 price)
        {
            this.BannerDetailID = bannerDetailID;
            this.BannerTypeID = bannerTypeID;
            this.BannerTypeName = bannerTypeName;
            this.LocationID = locationID;
            this.LocationName = locationName;
            this.PointID = pointID;
            this.PointName = pointName;
            this.StartDate = startDate;
            this.ToDate = toDate;
            this.Price = price;
        }
    }
}