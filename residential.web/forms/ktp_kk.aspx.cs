﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;

namespace residential.web.forms
{
    public partial class ktp_kk : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";
                show_cbo_casestatus();
                bounddatagrid("", "");
            }
        }

        private void show_cbo_casestatus()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_casestatus();
            cbo_status.DataSource = dt;
            cbo_status.DataTextField = "CaseStatusName";
            cbo_status.DataValueField = "CaseStatusID";
            cbo_status.DataBind();
            cbo_status.Items.Insert(0, new ListItem("[ Semua ]", "0"));
            cbo_status.SelectedIndex = 0;
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String CaseNumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();
                String CaseStatus = DataBinder.Eval(e.Row.DataItem, "CaseStatusName").ToString();
                Int32 CaseStatusID = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "CaseStatusID").ToString());
                HyperLink lnk_print = (HyperLink)e.Row.FindControl("lnk_print");
                lnk_print.NavigateUrl = "~/xslt/PrintKP_KK.aspx?CaseNumber=" + CaseNumber;
                lnk_print.Target = "_blank";
                 String linkToForm = "viewktp_kk.aspx";
                                
                if (CaseStatus.ToUpper() == "PENDING")
                    linkToForm = "entryktp_kk.aspx";
                
                for (var i = 2; i < e.Row.Cells.Count; i++)
                    e.Row.Cells[i].Attributes["onclick"] = "window.location = '" + linkToForm + "?CaseNumber=" + CaseNumber + "'";

                if (!String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString()) && (CaseStatusID == 2 || CaseStatusID == 4))
                {
                    DateTime OverdueDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString());
                    if (DateTime.Compare(DateTime.Now, OverdueDate) >= 0)
                        e.Row.BackColor = System.Drawing.Color.LightPink;
                }
            }
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            
            Int32 CaseStatusID = Int32.Parse(cbo_status.SelectedValue);            
            String Search = null;
            if (!String.IsNullOrEmpty(txt_search.Text))
                Search = txt_search.Text;

            DataTable dt = dbclass.retrieve_personal_identification(userData.OrgID, userData.SiteID, CaseStatusID, userData.DepartmentID, Search);
            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        protected void cbo_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        } 
    }
}