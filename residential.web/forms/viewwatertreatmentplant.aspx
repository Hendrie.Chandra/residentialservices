﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewwatertreatmentplant.aspx.cs"
    Inherits="residential.web.forms.viewwatertreatmentplant" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/emailviewer.ascx" TagName="EmailViewer" TagPrefix="aspuc" %> 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>Pemasangan Meter Air <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_email">Email Viewer</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase1" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
        <div id="tab_email" class="tab-pane fade">
            <aspuc:EmailViewer ID="emailviewer" runat="server" />
        </div>
    </div>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <div class="panel panel-default">
        <div class="panel-heading">WTP Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Ukuran Meter Air</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_category" runat="server"></asp:Label>
                        </p>
                    </div>                    
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Currency</label>
                    <div class="col-sm-4">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_currency" runat="server"></asp:Label>
                        </p>
                    </div>
                    <div runat="server" id="rate" visible="false">
                        <label class="col-sm-2 control-label">Exchange Rate</label>
                        <div class="col-sm-4">
                            <p class="form-control-static">
                                <asp:Label ID="lbl_rate" runat="server"></asp:Label>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Sub Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Adjustment</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_total" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
