﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listsmspriority.aspx.cs" Inherits="residential.web.forms.listsmspriority" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="buttoncontainer">
                <asp:Button CssClass="button" ID="btn_add" runat="server" Text="ADD" 
                    Width="100px" onclick="btn_add_Click" />
                <asp:Button CssClass="button" ID="btn_delete" runat="server" Text="DELETE" 
                    Width="100px" OnClientClick="return confirmDelete();" onclick="btn_delete_Click" />
            </div>
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" 
                CssClass="gridview" AllowPaging="True" PageSize="20" BorderWidth="0px" 
                Width="100%" onrowdatabound="gvw_data_RowDataBound" 
                ShowHeaderWhenEmpty="True" onrowcommand="gvw_data_RowCommand" 
                DataKeyNames="siteid">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="chk_selectall" runat="server" 
                                oncheckedchanged="chk_selectall_CheckedChanged" AutoPostBack="true" />
                        </HeaderTemplate>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chk_select" runat="server" />
                        </ItemTemplate>                
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="SITE CODE">
                        <HeaderStyle CssClass="gridview_header" Width="200px" />
                        <ItemStyle CssClass="gridview_item_left" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnk_site" runat="server" CommandName="cmd_edit" 
                                CssClass="gridview_link">site code</asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="SITE NAME" DataField="sitename">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
