﻿using residential.libs;
using residential.libs.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class EntryFinalInspect : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["Dept"] != null)
                {
                    HyperLink2.NavigateUrl = HyperLink2.NavigateUrl + "?Dept=" + Request.QueryString["Dept"].ToString();

                    ViewState["UserData"] = new UserData(User.Identity.Name);

                    UserData userData = (UserData)ViewState["UserData"];
                }
                else
                {
                    Response.Redirect("~/404_pagenotfound.aspx");
                }
            }
            if (entryheader.PSCode.Length > 7)
            {
                UserData userData = (UserData)ViewState["UserData"];

                DataSet ds = residential.web.Classes.clsFinalInspect.ViewHistoryList(Request.QueryString["Dept"].ToString(),
                       userData.OrgID, userData.SiteID, entryheader.PSCode);

                gvw_data.DataSource = null;
                gv_detail.DataSource = null;

                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvw_data.DataSource = ds.Tables[0];
                    gv_detail.DataSource = ds.Tables[1];    
                }

                gvw_data.DataBind();
                gv_detail.DataBind();
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = "Invalid Data";
                return;
            }
            else
            {
                String res = save_OtherIncome();
                if (!string.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                {
                    Response.Redirect("FinalInspect.aspx?Dept=" + Request.QueryString["Dept"].ToString());
                }
            }
        }

        private string save_OtherIncome()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dt = residential.web.Classes.clsFinalInspect.GetFinalInspectHelpID(Request.QueryString["Dept"].ToString(),
                       userData.OrgID, userData.SiteID);

            if (dt.Rows.Count < 1)
            {
                return "HelpID Final Inspect was not exist. Please Contact IT Application";
            }

            Int32 HelpID = int.Parse(dt.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;
            DateTime RequestDate = DateTime.Now;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.FullName = FullName;
            request.UserName = SavedBy;

            //if (Request.QueryString["CaseNumber"] != null)
            //{
            //    DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
            //    request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //}

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;
                DateTime? OverdueDate = dbcase.OverdueDate;

                int CaseStatusID = 2;

                String returnvalue = residential.web.Classes.clsFinalInspect.SaveFinalInspect(OrgID, SiteID, CRMCaseID, CaseNumber, CaseStatusID, HelpID,
                    CaseOriginID, entryheader.CustomerType, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }

                /*try
                {
                    UpdateCaseHelpDeskStatusRequest upd = new UpdateCaseHelpDeskStatusRequest();
                    upd.CRMCaseNumber = CaseNumber;
                    upd.CRMCaseStatusID = CaseStatusID;
                    upd.Remarks = "";
                    upd.UpdateBy = userData.FullName;
                    upd.UpdateDate = DateTime.Now;
                    upd.WorkedBy = string.Empty;
                    upd.SatisfactionID = 1;
                    UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(upd);
                }
                catch (Exception e)
                {
                    entryheader.ShowAlert = e.Message;
                }*/
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

    }
}