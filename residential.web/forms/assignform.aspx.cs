﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class assignform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                show_cbo_assign();
            }
        }

        private void show_cbo_assign()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_pic(userData.OrgID, userData.SiteID, userData.DepartmentID);
            cbo_assign.DataSource = dt;
            cbo_assign.DataTextField = "FullName";
            cbo_assign.DataValueField = "UserID";
            cbo_assign.DataBind();
        }    

        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
        }

        protected void btn_assign_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
        }

        protected void cbo_assign_SelectedIndexChanged(object sender, EventArgs e)
        {
            userid.Value = cbo_assign.SelectedValue;
        }
    }
}