﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/main.master" CodeBehind="listResidentCard.aspx.cs" Inherits="residential.web.forms.listResidentCard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        })
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>Resident Card</h1>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Cluster / Tower</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddlCluster" Style="cursor: pointer" runat="server"
                                CssClass="form-control" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlCluster_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" OnTextChanged="txt_search_TextChanged" AutoPostBack="true" placeholder="Search by UnitNo"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Unit Code</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddlUnitCode" Style="cursor: pointer" runat="server"
                                CssClass="form-control" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlUnitCode_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-bordered table-hover" AllowPaging="True"
                    ShowHeaderWhenEmpty="True" OnRowDataBound="gvw_data_RowDataBound"
                    OnPageIndexChanging="gvw_data_PageIndexChanging"
                    DataKeyNames="Unit"
                    HeaderStyle-HorizontalAlign="Center">
                    <PagerStyle CssClass="pagination-ys" />
                    <Columns>
                        <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="ResidentHeaderID" DataField="ResidentHeaderID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                        <asp:BoundField HeaderText="UNIT CODE" DataField="Unit" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                        <asp:BoundField HeaderText="OWNER" DataField="OwnerName" SortExpression="OwnerName" />
                        <asp:BoundField HeaderText="TENANT" DataField="TenantName" SortExpression="TenantName" />
                        <asp:BoundField HeaderText="CREATED" DataField="CreatedDate" SortExpression="CreatedDate" DataFormatString="{0:d MMMM yyyy}" />
                        <asp:BoundField HeaderText="RESIDENT" DataField="TotalResident" SortExpression="TotalResident" ItemStyle-HorizontalAlign="Right" />
                        <asp:BoundField HeaderText="CARD" DataField="TotalCard" SortExpression="TotalCard" ItemStyle-HorizontalAlign="Right" />
                        <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle CssClass="gridview-center" />
                            <ItemTemplate>
                                <asp:HyperLink runat="server" ID="lnk_addCard" CssClass="btn btn-info" data-toggle="tooltip" data-placement="top" title="Add Resident Card"><span class="glyphicon glyphicon-credit-card"></span></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No data
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
