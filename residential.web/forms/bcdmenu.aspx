﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="bcdmenu.aspx.cs" Inherits="residential.web.forms.bcdmenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="innerlinkcontent">
        <div class="innerlinkcell">
            <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icon-request-bcd.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/forms/listbcd.aspx" 
                CssClass="contentlink">Permohonan BCD</asp:HyperLink>
            <br />
            Permohonan izin BCD
        </div>
        <div class="innerlinkcell">
            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icon-permit-taking.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/forms/listpermittaking.aspx" 
                CssClass="contentlink">Pengambilan Izin</asp:HyperLink>
            <br />
            Pengambilan Izin BCD
        </div>
    </div>
</asp:Content>
