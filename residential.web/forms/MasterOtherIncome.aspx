﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/main.master" CodeBehind="MasterOtherIncome.aspx.cs" Inherits="residential.web.forms.MasterOtherIncome" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
     <script>
         function Confirm(HelpItemID, HelpID, HelpItemName, HelpItemPrice, isActive, DepartmentID) {
             $('#modalAssign').modal('show');

             $("[id$='hfHelpItemID']").val(HelpItemID);
             $("[id$='ddlDepartment']").val(DepartmentID);
             $("[id$='ddlHelpname']").val(HelpID);
             $("[id$='txtHelpItemName']").val(HelpItemName);
             $("[id$='txtPrice']").val(HelpItemPrice);
             $("[id$='ddlActive']").val(isActive);

              __doPostBack('<%= ddlDepartment.UniqueID %>', '');

             return true;
         };
         function FormatCurrency(ctrl) {
             //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
             if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
                 return;
             }
             var val = ctrl.value;
             val = val.replace(/,/g, "")
             ctrl.value = "";
             val += '';
             x = val.split('.');
             x1 = x[0];
             x2 = x.length > 1 ? '.' + x[1] : '';
             var rgx = /(\d+)(\d{3})/;

             while (rgx.test(x1)) {
                 x1 = x1.replace(rgx, '$1' + ',' + '$2');
             }
             ctrl.value = x1 + x2;
         }
         function CheckNumeric() {
             return event.keyCode >= 48 && event.keyCode <= 57;
         }
         function ShowEmpty() {
             $("[id$='hfHelpItemID']").val('');
             $("[id$='txtHelpItemName']").val('');
             $("[id$='txtPrice']").val('');

             __doPostBack('<%= ddlDepartment.UniqueID %>', '');

             $('#modalAssign').modal('show');

             return true;
         };
         function Close(text) {
             alert(text);
             $('#modalAssign').modal('hide');
             
         }
         function Failed(text) {
             alert(text);
         }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>Master Others Income</h1>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-2 control-label">Department</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="cbo_status" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="cbo_status_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" CssClass="form-control" runat="server"
                                OnTextChanged="txt_search_TextChanged" AutoPostBack="true" placeholder="Search Helpname"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" ToolTip="Search" Text="..." CssClass="btn btn-default" />
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-2 control-label">HelpName</label>
                        <div class="col-sm-6">
                            <asp:DropDownList ID="ddlHelpNameList" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlHelpNameList_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="alert alert-success alert-dismissible" id="alert_success" runat="server" visible="false">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <asp:Label runat="server" ID="alert_success_text"></asp:Label>
            </div>
            <div class="alert alert-danger alert-dismissible" id="alert_danger" runat="server" visible="false">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <asp:Label runat="server" ID="alert_danger_text"></asp:Label>
            </div>
            <button type="button" class="btn btn-primary" onclick="ShowEmpty();">New</button>
            <hr />
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover"
                AllowPaging="True" OnRowDataBound="gvw_data_RowDataBound" PageSize="15"
                ShowHeaderWhenEmpty="True" DataKeyNames="HelpItemID" EmptyDataText="No Data"
                AllowSorting="True" OnSorting="gvw_data_Sorting" OnPageIndexChanging="gvw_data_PageIndexChanging"
                OnRowCommand="gvw_data_RowCommand" HeaderStyle-HorizontalAlign="Center">
                <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="HelpItemID" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" DataField="HelpItemID" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true"></asp:BoundField>
                    <asp:BoundField HeaderText="HelpID" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" DataField="HelpID" SortExpression="HelpID"></asp:BoundField>
                    <asp:BoundField HeaderText="Help Name" ItemStyle-HorizontalAlign="Center" DataField="HelpName" SortExpression="HelpName"></asp:BoundField>
                    <asp:BoundField HeaderText="Item Name" ItemStyle-HorizontalAlign="Center" DataField="HelpItemName" SortExpression="HelpItemName"></asp:BoundField>
                    <asp:BoundField DataField="HelpItemPrice" ItemStyle-HorizontalAlign="Right" HeaderText="HelpItemPrice" DataFormatString="{0:N2}"></asp:BoundField>
                    <asp:BoundField DataField="ModifiedBy" ItemStyle-HorizontalAlign="Right" HeaderText="Last Modified User"></asp:BoundField>
                    <asp:BoundField DataField="ModifiedDate" ItemStyle-HorizontalAlign="Right" HeaderText="Last Modified Date" DataFormatString="{0:d MMMM yyyy, HH:mm}"></asp:BoundField>
                    <asp:BoundField DataField="isActive" ItemStyle-HorizontalAlign="Center"  HeaderText="isActive" SortExpression="isActive"></asp:BoundField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="modalAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">HelpName</h4>
                    <asp:HiddenField ID="hfHelpItemID" runat="server" />
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Department*</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="ddlDepartment" runat="server" CssClass="form-control" AutoPostBack="true"
                                    onselectedindexchanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">HelpName*</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="ddlHelpname" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Help Item Name*</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtHelpItemName" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Price*</label>
                            <div class="col-sm-10">
                                <asp:TextBox runat="server" ID="txtPrice"  CssClass="form-control" onkeypress="return CheckNumeric();" onkeyup="FormatCurrency(this);"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Active*</label>
                            <div class="col-sm-10">
                                <asp:DropDownList ID="ddlActive" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="True" Value="1" />
                                    <asp:ListItem Text="False" Value="0" /> 
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button ID="btn_assigning" runat="server" Text="SUBMIT" 
                        CssClass="btn btn-primary" UseSubmitBehavior="false" data-dismiss="modal" 
                        onclick="btn_assigning_Click" OnClientClick="this.disabled=true; this.value='Saving...';" />
                    <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="btn btn-default" data-dismiss="modal" />
                </div>
            </div>
        </div>
        </ContentTemplate></asp:UpdatePanel>
    </div>
</asp:Content>
