﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="emailviewer.ascx.cs" Inherits="residential.web.forms.emailviewer" %>

<asp:GridView ID="gvw_emailviewer" runat="server" CssClass="table table-hover table-bordered" AutoGenerateColumns="False"
    OnRowDataBound="gvw_emailviewer_RowDataBound">
    <Columns>
        <asp:BoundField DataField="emailFromDisplayName" HeaderText="From" />
        <asp:BoundField DataField="emailTo" HeaderText="To" />
        <asp:BoundField DataField="emailSubject" HeaderText="Subject" />
        <asp:BoundField DataField="EmailStatus" HeaderText="Status" />
        <asp:TemplateField HeaderText="View Email" ItemStyle-HorizontalAlign="Center">
            <HeaderStyle CssClass="gridview-center" Width="150px" />
            <ItemTemplate>
                <asp:HyperLink runat="server" ID="hpl_viewemail" CssClass="btn btn-info" data-toggle="tooltip" data-placement="top" title="View Email" NavigateUrl='<%# Eval("emailID","~/forms/viewemail.aspx?EmailID={0}") %>' Target="_blank"><span class="glyphicon glyphicon-envelope"></span></asp:HyperLink>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EmptyDataTemplate>
        No data
    </EmptyDataTemplate>
</asp:GridView>

