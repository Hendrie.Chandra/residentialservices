﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Web;

namespace residential.web.forms
{
    public partial class viewwatertreatmentplant : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_case(Request.QueryString["CaseNumber"]);                
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                string deptID = Request.QueryString["DeptID"];

                if (deptID == null)
                {
                    //updatecase1.Visible = false;
                    /*tampilkan view untuk update cencel di cs*/
                    updatecase1.Visible = true;
                    updatecase1.DepartmentID = "";
                }
                else
                {
                    updatecase1.Visible = true;
                    updatecase1.DepartmentID = Request.QueryString["DeptID"].ToString();
                    updatecase1.IsPaskem = "1";
                }
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_Case(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                DataTable dt2 = dbclass.retrieve_wtp(_casenumber);

                lbl_category.Text = dt2.Rows[0]["WaterPlantCategoryName"].ToString();

                lbl_currency.Text = dt2.Rows[0]["CurrencyID"].ToString();
                //lbl_curr1.Text = dt2.Rows[0]["CurrencySymbol"].ToString();
                //lbl_curr2.Text = dt2.Rows[0]["CurrencySymbol"].ToString();
                //lbl_curr3.Text = dt2.Rows[0]["CurrencySymbol"].ToString();

                Decimal subtotal = Decimal.Parse(dt2.Rows[0]["Amount"].ToString());
                Decimal discount = Decimal.Parse(dt2.Rows[0]["DiscountAmount"].ToString());
                Decimal total = Decimal.Parse(dt2.Rows[0]["AmountAfterDiscount"].ToString());

                if (dt2.Rows[0]["CurrencyID"].ToString() != "1")
                {
                    lbl_rate.Text = dt2.Rows[0]["Rate"].ToString();
                    rate.Visible = true;

                    lbl_subtotal.Text = subtotal.ToString("N2");
                    lbl_adjustment.Text = discount.ToString("N2");
                    lbl_total.Text = total.ToString("N2");
                }
                else
                {
                    lbl_subtotal.Text = subtotal.ToString("N0");
                    lbl_adjustment.Text = discount.ToString("N0");
                    lbl_total.Text = total.ToString("N0");
                }
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }
    }
}