﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrysmartcard.aspx.cs" Inherits="residential.web.forms.entrysmartcard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%--<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>--%>
<%@ Register Src="entryheadersmartcard.ascx" TagName="entryheader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <style type="text/css">
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display:none;
        }
        #navwrap {
      
          background: #eeeeee;
          -webkit-box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
          -moz-box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
          box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
        }        
    </style>
    <script src="../scripts/bootstrap-multiselect.js"></script>
    <script src="../scripts/smartcard.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {            

            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $("body").find($('#<%=txt_quantity.ClientID%>')).on("change", function (event) {
            });

            //$(function () {
            //    $('[id*=lstFruits]').multiselect({
            //        includeSelectAllOption: true
            //    });
            //});            
            <%--$('#<%=CheckBox_identitasdiri.ClientID%>').change(function () {
                if (this.checked)
                    $('#<%=FileUpload_identitasdiri.ClientID%>').removeAttr('disabled')
                else
                    $('#<%=FileUpload_identitasdiri.ClientID%>').attr('disabled', 'disabled')
            });--%>



            function showLoader() {
                if (form1.checkValidity()) {
                    $('#<%= UpdateProgress1.ClientID %>').css('display', '');
                    return true;
                } else {
                    return false;
                }
            }

        });

        function selectnexttab(event) {
            $('.nav-tabs a[href="#kelengkapandokumen"]').tab('show');
            event.preventDefault();
            return false;
        }

        function save_client_click() {
            $('#<%=btn_saveclose.ClientID%>').click();
        }

        function back_client_click() {
            $('#<%=btn_back.ClientID%>').click();
        }

        function load_unit_history()
        {
            $('#ContentPlaceHolder1_entryheadersmartcard_cbo_unit').on('change', function () {
                $('#<%=btn_load_history_byunit.ClientID%>').click();
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form Permohonan Smart Card</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <div id="errID" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
        <asp:Label ID="lbl_error" runat="server" Visible="false"></asp:Label>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning!</strong> <%=lbl_error.Text%>
    </div>
    <%--<uc1:entryheader ID="entryheader" runat="server" />--%>
    <uc1:entryheader ID="entryheadersmartcard" runat="server" />
    <asp:Button ID="btn_load_history_byunit" runat="server" Text="..." style="display:none" OnClick="btn_load_history_byunit_Click" UseSubmitBehavior="false"/>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Smart Card Detail</div>
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#smartcardrequestdetail" aria-controls="home" role="tab" data-toggle="tab">Request Detail</a></li>
                        <li role="presentation"><a href="#kelengkapandokumen" aria-controls="profile" role="tab" data-toggle="tab">Supporting Document</a></li>
                        <li role="presentation"><a href="#history" aria-controls="profile" role="tab" data-toggle="tab">History</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="smartcardrequestdetail">
                            <br />
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Card Type</label>
                                    <div class="col-sm-4">
                                        <%--<asp:DropDownList ID="ddl_card_type" CssClass="form-control" runat="server" onchange="getpriceonchange();"></asp:DropDownList>--%>
                                        <asp:DropDownList ID="ddl_card_type" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_card_type_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Resident Status</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_resident_status" CssClass="form-control" runat="server" AutoPostBack="true" OnTextChanged="ddl_resident_status_TextChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Quantity</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_quantity" CssClass="form-control" runat="server" AutoPostBack="true" OnTextChanged="txt_quantity_TextChanged" Type="Number" MaxLength="7">1</asp:TextBox>
                                    </div>  
                                    <label id="lbl_rent_periode_date" class="col-sm-2 control-label" runat="server" visible="false">Rent Periode Date</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_rent_periode_date" CssClass="form-control" runat="server" Visible="false"></asp:TextBox>
                                         <asp:CalendarExtender ID="CalendarExtender_rent_periode_date" runat="server" TargetControlID="txt_rent_periode_date" PopupPosition="TopLeft"
                                            Format="dd MMMM yyyy">
                                         </asp:CalendarExtender>
                                    </div>                                                                      
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 control-label">
                                        <asp:CheckBox ID="CheckBox_isreplacement" runat="server" CssClass="pull-right" style="padding-top:11px" AutoPostBack="true" OnCheckedChanged="CheckBox_isreplacement_CheckedChanged" />
                                    </div>
                                    <label class="col-sm-4 control-label" style="text-align:left">Is Replacement</label>                              
                                </div>
                                <hr />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <%--<asp:TextBox ID="txt_cost" CssClass="form-control" runat="server" placeholder="cost"></asp:TextBox>--%>
                                    </div>
                                    <label id="lbl_price" class="col-sm-2 control-label" runat="server">Price</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_price" CssClass="form-control" runat="server" ReadOnly="true" placeholder="Price"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                    </div>
                                    <label id="lbl_total" class="col-sm-2 control-label" runat="server">Total</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_total" CssClass="form-control" runat="server" ReadOnly="true" placeholder="total"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_iscalculate" CssClass="form-control" runat="server" placeholder="is calculate?" Visible="false"></asp:TextBox>
                                    </div>
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-2">
                                        <%--<asp:Button ID="btn_calculate" runat="server" Text="Calculate" CssClass="btn btn-default" OnClick="btn_calculate_Click" />--%>
                                        <asp:Button ID="btn_calculate" runat="server" Text="Calculate" Style="display: none" UseSubmitBehavior="false" OnClick="btn_calculate_Click" />
                                    </div>
                                    <div class="col-sm-2">
                                        <%--<asp:Button ID="btn_reset" runat="server" Text="Reset" CssClass="btn btn-default" OnClick="btn_reset_Click" />--%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                    </div>
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <%--<asp:Button ID="btn_next_todokumen" runat="server" UseSubmitBehavior="false" OnClientClick="selectnexttab();" Text="Next" />--%>
                                        <input type="button" id="btn_nexttab" class="btn btn-default pull-right" onclick="selectnexttab(this)" value="next" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="kelengkapandokumen">
                            <br />
                            <div class="alert alert-success" role="alert">document must be in .jpg, .jpeg, .png, .pdf format and maximum 3 Mb</div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_identitasdiri" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Identitas Diri Terbaru (KTP/SIM/KITAS)</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_identitasdiri" runat="server" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_kk" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Kartu Keluarga</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_kk" runat="server" CssClass="form-control" Enabled="false" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_stnk" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy STNK</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_stnk" runat="server" CssClass="form-control" Enabled="false"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_kepemilikanunit" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Bukti Kepemilikan Unit (PPJB/AJB/Sertifikat)</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_kepemilikanunit" runat="server" CssClass="form-control" Enabled="false"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_perjanjiansewamenyewaunit" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px"">Copy Perjanjian Sewa Menyewa Unit</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_perjanjiansewamenyewaunit" runat="server" CssClass="form-control" Enabled="false"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_suratketerangankaryawan" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Keterangan Karyawan</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_suratketerangankaryawan" runat="server" CssClass="form-control" Enabled="false"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_buktibayarutilitas" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy Bukti Bayar Utilitas 1 Bln Terakhir</label>
                                 <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_buktibayarutilitas" runat="server" CssClass="form-control" Enabled="false"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_suratkuasa" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Surat Kuasa Bermaterai (jika perlu)</label>
                                 <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_suratkuasa" runat="server" CssClass="form-control" Enabled="false"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <asp:CheckBox ID="CheckBox_ktppemberipenerimakuasa" runat="server" CssClass="pull-right" />
                                </div>
                                <label class="col-sm-5 control-label" style="padding-bottom:20px">Copy KTP Pemberi & Penerima Kuasa (jika perlu)</label>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_ktppemberipenerimakuasa" runat="server" CssClass="form-control" Enabled="false"/>
                                </div>
                                <%--<label class="col-sm-1 control-label" style="padding-top:11px">Lain-lain</label>
                                <div class="col-sm-5">
                                    <asp:TextBox ID="txt_lainlain" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>--%>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-1 control-label" style="padding-top:11px; padding-bottom:20px">Lain-lain</label>
                                <div class="col-sm-5">
                                    <asp:TextBox ID="txt_lainlain" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-sm-6" style="padding-bottom:20px">
                                    <asp:FileUpload ID="FileUpload_lainlain" runat="server" CssClass="form-control" Enabled="false"/>
                                </div>
                            </div>


                            <div>
                                <asp:Image ID="Image2" runat="server" />
                            </div>

                            <br />
                            <hr />
                        </div>
                        <%--history--%>
                        <div role="tabpanel" class="tab-pane" id="history">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvw_smartcardhistory" runat="server" CssClass="table table-hover table-bordered" AutoGenerateColumns="False">
                                        <Columns>
                                            <%-- <asp:BoundField DataField="ModifiedDate" HeaderText="Modified Date" DataFormatString="{0:dd/MM/yyyy HH:mm}" />
                                    <asp:BoundField DataField="Remarks" HeaderText="Remarks" />--%>
<%--                                            <asp:BoundField DataField="TransactionTypeDescription" HeaderText="Transaction Type" />--%>
                                            <asp:BoundField DataField="CardNumber" HeaderText="Card Number" />
                                            <asp:BoundField DataField="PSCode" HeaderText="PSCode" />
<%--                                            <asp:BoundField DataField="ChipNumber" HeaderText="Chip Number" />--%>
                                            <asp:TemplateField HeaderText="Card Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="CardType" runat="server" Text='<%#(Eval("CardTypeID").ToString()=="1" ? "Resident" : "Member") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="HolderName" HeaderText="Holder Name" />
                                            <asp:BoundField DataField="Unit" HeaderText="Unit" />
                                            <asp:BoundField DataField="BoomGateName" HeaderText="Boom Gate" />
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="Status" runat="server" Text='<%#(Eval("Status").ToString()=="1" ? "Active" : "In active") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CreatedBy" HeaderText="Created By" />
                                            <%--<asp:BoundField DataField="BoomGateName" HeaderText="Boomgate Name" />                           
                                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" DataFormatString="{0:dd/MM/yyyy HH:mm}" />   --%>                                         
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No data
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <br />


                            <br />
                            <hr />
                        </div>

                    </div>
                </div>
            </div>
            <%--<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="navwrap">
        <nav class="navbar navbar-default navbar-fixed-buttom">
                <ul class="nav navbar-nav pull-right">
                    <li>
                        <p class="navbar-btn" style="padding-right:12px">
                            <asp:LinkButton ID="lnkbtn_back" runat="server" OnClientClick="back_client_click();" UseSubmitBehavior="false"  CssClass="btn btn-default"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> &nbsp; Back</asp:LinkButton>
                        </p>
                    </li>
                    <li>
                        <p class="navbar-btn" style="padding-right:12px">
                            <%--<asp:LinkButton ID="lnkbtn_save" runat="server" OnClientClick="save_client_click();" UseSubmitBehavior="false" CssClass="btn btn-primary">Save &nbsp;<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></asp:LinkButton>--%>
                            <%--<asp:LinkButton ID="lnkbtn_save" runat="server" OnClick="lnkbtn_save_Click" CssClass="btn btn-primary">Save &nbsp;<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></asp:LinkButton>--%>
                            <asp:Button ID="buttom_save" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="buttom_save_Click" OnClientClick="showLoader()" />
                        </p>
                    </li>
                </ul>
        </nav>
    </div>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>--%>
