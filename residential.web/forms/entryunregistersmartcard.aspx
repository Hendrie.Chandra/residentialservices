﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryunregistersmartcard.aspx.cs" Inherits="residential.web.forms.entryunregistersmartcard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheadersmartcard.ascx" TagName="entryheader" TagPrefix="uc1" %>
<%--<%@ Register Src="~/forms/entryheadersmartcard.ascx" TagPrefix="uc1" TagName="entryheadersmartcard" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .back-to-top {
            cursor: pointer;
            position: fixed;
            bottom: 20px;
            right: 20px;
            display: none;
        }
        #navwrap {      
          background: #eeeeee;
          -webkit-box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
          -moz-box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
          box-shadow: 0 7px 8px rgba(0, 0, 0, 0.12);
        }
    </style>
    <script src="../scripts/bootstrap-multiselect.js"></script>
    <link href="../content/bootstrap-dialog.min.css" rel="stylesheet" />
    <script type="text/javascript">

        $(document).ready(function () {

            $(window).scroll(function () {
                if ($(this).scrollTop() > 50) {
                    $('#back-to-top').fadeIn();
                } else {
                    $('#back-to-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            function showLoader() {
                if (form1.checkValidity()) {
                    $('#<%= UpdateProgress1.ClientID %>').css('display', '');
                    return true;
                } else {
                    return false;
                }
            }
        });

        function selectnexttab(event) {
            $('.nav-tabs a[href="#kelengkapandokumen"]').tab('show');
            event.preventDefault();
            return false;
        }

        function show_dialog_onsave() {
            BootstrapDialog.confirm({
                title: 'Warning',
                message: 'Are you sure want to save this data?',
                type: BootstrapDialog.TYPE_DANGER,
                callback: function (result) {
                    if (result) {
                        //showLoader();
                        //alert(a);
                        eval($("#<%=lnkbtn_unregister.ClientID %>").attr('href'));

                        //showLoader();
                    } else {
                        return;
                    }
                }
            });
        }

        function showModalFormDetailID(cardnumber) {
            $('#<%=hdf_cardnumber.ClientID%>').val(cardnumber);
            $('#<%=btn_showdetail.ClientID%>').click();
        }

        function showModalFormDetail() {
            $('#modalFormDetailID').modal('show');
        }

        function load_unit_history() {
            $('#ContentPlaceHolder1_entryheadersmartcard_cbo_unit').on('change', function () {
                $('#<%=btn_load_history_byunit.ClientID%>').click();
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="page-header">
        <h1>Form Unregister Smart Card</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="show_dialog_onsave(); return false" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" UseSubmitBehavior="false" OnClick="btn_back_Click"/>
    <hr />
<%--    <div id="errID" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
        <asp:Label ID="lbl_error" runat="server" Visible="false"></asp:Label>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Warning! </strong> <%=lbl_error.Text%>
    </div>--%>
    <%--<uc1:entryheader ID="entryheader" runat="server" />--%>
    <uc1:entryheader ID="entryheadersmartcard" runat="server" />
    <asp:Button ID="btn_load_history_byunit" runat="server" Text="..." style="display:none" OnClick="btn_load_history_byunit_Click" UseSubmitBehavior="false" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Unregister Smart Card Detail</div>
                <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#unregistersmartcarddetail" aria-controls="home" role="tab" data-toggle="tab">Unregister Smartcard Detail</a></li>
                        <li role="presentation"><a href="#history" aria-controls="profile" role="tab" data-toggle="tab">History</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="unregistersmartcarddetail">
                            <br />
                            <asp:GridView ID="gvw_data" runat="server" DataKeyNames="MembershipID" OnRowDataBound="gvw_data_RowDataBound" AutoGenerateColumns="False" CssClass="table table-hover table-bordered">
                                <PagerStyle CssClass="pagination-ys" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                                        <ItemStyle CssClass="gridview_item_center" />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk_select" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CardNumber" HeaderText="Card Number" />
                                    <asp:BoundField DataField="ChipNumber" HeaderText="Chip Number" />
<%--                                    <asp:TemplateField HeaderStyle-Width="50px">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="BoomGateID" runat="server" Value='<%# Eval("BoomGateID").ToString() %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                    <asp:BoundField DataField="BoomGateID" HeaderText="BoomGate ID" />
                                    <asp:BoundField DataField="BoomGateCode" HeaderText="BoomGate Code" />
                                    <asp:BoundField DataField="BoomGateName" HeaderText="Boomgate Name" />
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="Status" runat="server" Text='<%# (int)Eval("Status")==1 ? "Active" : "InActive" %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="lnk_detail" CssClass="btn btn-default btn-sm" Text="View Detail"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No data
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <br />
                            <%-- <asp:Button ID="btn_unregister" OnClick="lnkbtn_unregister_Click" Visible="false" runat="server" Text="Button" />--%>
                            <asp:LinkButton ID="lnkbtn_unregister" CssClass="btn btn-default pull-right" runat="server" Style="display: none" OnClientClick="" OnClick="lnkbtn_unregister_Click">Unregister <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
                        </div>
                        <%--history--%>
                        <div role="tabpanel" class="tab-pane" id="history">
                            <br />
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvw_smartcardhistory" runat="server" CssClass="table table-hover table-bordered" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="CardNumber" HeaderText="Card Number" />
                                            <asp:BoundField DataField="PSCode" HeaderText="PSCode" />
                                            <asp:TemplateField HeaderText="Card Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="CardType" runat="server" Text='<%#(Eval("CardTypeID").ToString()=="1" ? "Resident" : "Member") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="HolderName" HeaderText="Holder Name" />
                                            <asp:BoundField DataField="BoomGateName" HeaderText="Boom Gate" />
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="Status" runat="server" Text='<%#(Eval("Status").ToString()=="1" ? "Active" : "In active") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CreatedBy" HeaderText="Created By" />
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No data
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <br />
                        </div>
                    </div>
                </div>
                </div>
                <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>

                <div class="modal fade" id="modalFormDetailID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabelResident">Smart Card Detail</h4>
                            </div>
                            <div class="modal-body">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <button type="button" class="btn btn-default" data-dismiss="modal" runat="server" id="Button5">Close</button>
                                        <asp:Button ID="btn_showdetail" runat="server" UseSubmitBehavior="false" Style="display: none" OnClick="btn_showdetail_Click" />

                                        <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="Div2" visible="false">
                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                                        </div>
                                        <asp:HiddenField ID="hdf_cardnumber" runat="server" />
                                        <hr />
                                        <asp:Label ID="lbl_error_message_resident" runat="server" Text="" Visible="false"></asp:Label>
                                        <div id="error_meesage_resident" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
                                            <asp:Label ID="Label5" runat="server" Visible="false"></asp:Label>
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <strong>Warning!</strong><%=lbl_error_message_resident.Text %>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Card Number</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txt_cardnumber" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Holder Name</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txt_holdername" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Chip Number</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txt_chipnumber" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Vehicle Number</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txt_vehiclenumber" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Vehicle Type</label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddl_vehicletype" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Vehicle Brand</label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txt_vehiclebrand" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="navwrap">
        <nav class="navbar navbar-default navbar-fixed-buttom">
                <ul class="nav navbar-nav pull-right">
                    <li>
                        <p class="navbar-btn" style="padding-right:12px">
                            <asp:Button ID="buttom_back" runat="server" Text="Back" CssClass="btn btn-default" UseSubmitBehavior="false" OnClick="buttom_back_Click"/>
                            <%--<asp:LinkButton ID="lnkbtn_back" runat="server" OnClientClick="back_client_click();" UseSubmitBehavior="false"  CssClass="btn btn-default"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> &nbsp; Back</asp:LinkButton>--%>
                        </p>
                    </li>
                    <li>
                        <p class="navbar-btn" style="padding-right:12px">
                            <%--<asp:LinkButton ID="lnkbtn_save" runat="server" OnClientClick="save_client_click();" UseSubmitBehavior="false" CssClass="btn btn-primary">Save &nbsp;<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></asp:LinkButton>--%>
                            <%--<asp:LinkButton ID="lnkbtn_save" runat="server" OnClick="lnkbtn_save_Click" CssClass="btn btn-primary">Save &nbsp;<span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></asp:LinkButton>--%>
                            <asp:Button ID="buttom_save" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="show_dialog_onsave(); return false" />
                        </p>
                    </li>
                </ul>
        </nav>
    </div>
        
    <script src="../scripts/bootstrap-dialog.min.js"></script>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>--%>
