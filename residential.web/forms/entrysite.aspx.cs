﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class entrysite : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                if (Request.QueryString["SiteID"] != null)
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    Int32 SiteID = Int32.Parse(Request.QueryString["SiteID"].ToString());
                    DataTable dt = dbclass.retrieve_site(SiteID);

                    DataRow datarow = dt.Rows[0];
                    txt_sitecode.Text = datarow["SiteCode"].ToString();
                    txt_sitename.Text = datarow["SiteName"].ToString();
                }
            }
        }

        private Int32 save_sites()
        {
            Int32 SiteID = 0;
            String SavedID = Session["username"].ToString();
            Int32 OrgID = Int32.Parse(Session["OrgID"].ToString());            
            if (Request.QueryString["SiteID"] != null)                            
                SiteID = Int32.Parse(Request.QueryString["SiteID"].ToString());

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = dbclass.save_site(SiteID,txt_sitecode.Text, txt_sitename.Text, OrgID, SavedID);

            return returnvalue;
        }

        protected Boolean sites_validation()
        {
            if (String.IsNullOrEmpty(txt_sitecode.Text))
            {
                alert_danger_text.Text = "Site code is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_sites();
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_sites();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            }
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_sites();
                txt_sitecode.Text = String.Empty;
                txt_sitename.Text = String.Empty;
            }
        }     
    }
}