﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class entrycarrenttariff : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                if (Request.QueryString["CarRentTariffID"] != null)
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    Int32 CarRentTariffID = Int32.Parse(Request.QueryString["CarRentTariffID"].ToString());
                    DataTable dt = dbclass.retrieve_CarRentTariff(CarRentTariffID);

                    DataRow datarow = dt.Rows[0];
                    lbl_CarRentTariffID.Text = datarow["CarRentTariffID"].ToString();
                    txt_CarRentTariffName.Text = datarow["CarRentTariffName"].ToString();
                    txt_price.Text = String.Format("{0:0}", ((decimal)datarow["Price"]));
                }
            }
        }

        private Int32 save_CarRentTariff()
        {
            UserData userData = new UserData(User.Identity.Name);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = 0;
            if (!String.IsNullOrEmpty(lbl_CarRentTariffID.Text))
            {
                returnvalue = dbclass.save_CarRentTariff(Int32.Parse(lbl_CarRentTariffID.Text), txt_CarRentTariffName.Text, Int32.Parse(txt_price.Text), userData.UserName);
            }
            else
            {
                returnvalue = dbclass.save_CarRentTariff(0, txt_CarRentTariffName.Text, Int32.Parse(txt_price.Text), userData.UserName);
            }
            return returnvalue;
        }

        protected Boolean sites_validation()
        {
            if (String.IsNullOrEmpty(txt_CarRentTariffName.Text))
            {
                alert_danger_text.Text = "Car rent name is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_CarRentTariff();
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_CarRentTariff();
                Response.Redirect("listcarrenttariff.aspx");
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            }
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_CarRentTariff();
                lbl_CarRentTariffID.Text = String.Empty;
                txt_CarRentTariffName.Text = String.Empty;
                txt_price.Text = String.Empty;
            }
        }     
    }
}