﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entrywtpcategory.aspx.cs" Inherits="residential.web.forms.entrywtpcategory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="buttoncontainer">        
        <asp:Button ID="btn_saveclose" runat="server" Text="SAVE CLOSE" CssClass="button"
            Width="100px" OnClick="btn_saveclose_Click" />
        <asp:Button ID="btn_back" runat="server" Text="BACK" CssClass="button"
            Width="100px" OnClick="btn_back_Click" />       
    </div>
    <div class="alert alert-success" id="alert_success" runat="server" visible="false">
        <asp:Label runat="server" ID="alert_success_text"></asp:Label>
    </div>
    <div class="alert alert-danger" id="alert_danger" runat="server" visible="false">
        <asp:Label runat="server" ID="alert_danger_text"></asp:Label>
    </div>
    <div class="formentrycontent">
        <div class="formentrycell1">
            <asp:Label ID="Label4" runat="server" Text="WTP Category :" CssClass="labelfield"></asp:Label>
            <asp:TextBox ID="txt_wtpcategory" runat="server" CssClass="textbox_entry_left textboxcell1"></asp:TextBox>            
        </div>
    </div>
    <div class="formentrycontent">
        <div class="formentrycell2">
            <asp:Label ID="Label1" runat="server" Text="Price :" CssClass="labelfield"></asp:Label>
            <asp:TextBox ID="txt_price" runat="server" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
        </div>
    </div>
</asp:Content>
