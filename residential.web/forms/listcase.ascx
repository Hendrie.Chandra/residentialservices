﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="listcase.ascx.cs" Inherits="residential.web.forms.listcase" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<link href="../maincss.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../global.js">
</script>
<%-- 
<div class="formentrycontent">
    <div class="formentrycell2">
        <asp:Label ID="Label1" runat="server" Text="Case Status :"></asp:Label>
        <asp:DropDownList ID="cbo_status" runat="server" CssClass="ComboBoxWindowsStyle" AutoPostBack="True"
            OnSelectedIndexChanged="cbo_status_SelectedIndexChanged" Width="167px">
        </asp:DropDownList>
    </div>
    <div class="formentrycell2" style="text-align:right">
        <asp:TextBox ID="txt_search" CssClass="textbox_entry_left textboxcell2" 
            runat="server" ontextchanged="txt_search_TextChanged" AutoPostBack="true"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="TWE_search" runat="server" TargetControlID="txt_search" WatermarkText="case number, unit" WatermarkCssClass="textbox-watermark" ></asp:TextBoxWatermarkExtender>
        <asp:Button ID="btn_search" runat="server" onclick="btn_search_Click" Text="Search" />
    </div>
</div>
--%>
<asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" 
    CssClass="gridview" AllowPaging="True" BorderWidth="0px" 
    Width="100%" onrowdatabound="gvw_data_RowDataBound" 
    ShowHeaderWhenEmpty="True" onrowcommand="gvw_data_RowCommand" 
    DataKeyNames="CaseNumber" AllowSorting="True" onsorting="gvw_data_Sorting" 
    onpageindexchanging="gvw_data_PageIndexChanging">
    <Columns>
        <asp:TemplateField  HeaderText="NO">
            <HeaderStyle CssClass="gridview_header" Width="30px" />
            <ItemStyle CssClass="gridview_item_center" />
            <ItemTemplate>
                <asp:Label ID="lbl_no" runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>                   
        <asp:TemplateField HeaderText="CASE NUMBER">
            <HeaderStyle CssClass="gridview_header" Width="115px" />
            <ItemStyle CssClass="gridview_item_left" />
            <ItemTemplate>
                <asp:LinkButton ID="lnk_case" runat="server" CommandName="cmd_edit" 
                    CssClass="gridview_link">case id</asp:LinkButton>
            </ItemTemplate>                        
        </asp:TemplateField>
        <asp:BoundField HeaderText="UNIT" DataField="Unit" SortExpression="Unit">
            <HeaderStyle CssClass="gridview_header" />
            <ItemStyle CssClass="gridview_item_left" />
        </asp:BoundField>
        <asp:BoundField HeaderText="CATEGORY" DataField="HelpName" SortExpression="HelpName">
            <HeaderStyle CssClass="gridview_header" />
            <ItemStyle CssClass="gridview_item_left" />
        </asp:BoundField>
        <asp:BoundField HeaderText="CREATED" DataField="RequestDate" SortExpression="RequestDate"
            DataFormatString="{0:d MMM yyyy}">
            <HeaderStyle CssClass="gridview_header" />
            <ItemStyle CssClass="gridview_item_left" />
        </asp:BoundField>
        <asp:BoundField HeaderText="OVERDUE" DataField="OverdueDate" SortExpression="OverdueDate"
            DataFormatString="{0:d MMM yyyy}">
            <HeaderStyle CssClass="gridview_header" />
            <ItemStyle CssClass="gridview_item_left" />
        </asp:BoundField>
        <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" SortExpression="CaseStatusName">
            <HeaderStyle CssClass="gridview_header" />
            <ItemStyle CssClass="gridview_item_left" />
        </asp:BoundField>
    </Columns>
    <EmptyDataTemplate>
        No data
    </EmptyDataTemplate>
</asp:GridView>