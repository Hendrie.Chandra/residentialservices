﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using residential.libs;
using residential.libs.Models;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entryflyer : System.Web.UI.Page
    {
        static String prevPage = "flyer.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                
                show_requestprice();
                hitung_total();
                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            entryheader.ToggleAlert = false;
        }

        private void Show_Case(String _casenumber)
        {
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                DataTable dt = dbclass.retrieve_flyer(_casenumber);

                entryheader.OwnerName = dt.Rows[0]["psName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                //check customer type -> 0 = Owner, 1 = Tenant
                entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheader.combobox_unit();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

                entryheader.disable_all_controls();

                txt_jumlah.Text = dt.Rows[0]["NoOfFlyer"].ToString();
                lbl_price.Text = decimal.Parse(dt.Rows[0]["Price"].ToString()).ToString("F2");
                txt_adjustment.Text = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString())).ToString("N0");
                lbl_totalharga.Text = decimal.Parse(dt.Rows[0]["Amount"].ToString()).ToString("F2");
                lbl_adjustment.Text = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString())).ToString("N0");
                lbl_subtotal.Text = decimal.Parse(dt.Rows[0]["AmountAfterTax"].ToString()).ToString("N0");
                lbl_ppn.Text = decimal.Parse(dt.Rows[0]["TaxAmount"].ToString()).ToString("F2");
                lbl_total.Text = Convert.ToInt32(decimal.Parse(dt.Rows[0]["ActualAmount"].ToString())).ToString("N0");

                // hitung_total();
            }
            catch (Exception)
            {
                throw new HttpException(404, "Page not found");
            }
        }

        private void show_requestprice()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_flyerPrice(userData.OrgID, userData.SiteID);

            Int32 price = 0;
            if (dt != null && dt.Rows.Count > 0)
                price = Convert.ToInt32(dt.Rows[0]["Price"]);
            Double realprice = price / 1.1;
            lbl_price.Text = realprice.ToString("F2");
            hitung_total();
        }

        private void hitung_total()
        {
            if (String.IsNullOrWhiteSpace(txt_jumlah.Text))
                txt_jumlah.Text = "1";

            if (String.IsNullOrWhiteSpace(txt_adjustment.Text))
                txt_adjustment.Text = "0";

            Double TotalPrice = Int32.Parse(txt_jumlah.Text) * Double.Parse(lbl_price.Text, NumberStyles.Currency);
            lbl_totalharga.Text = TotalPrice.ToString("F2");
            Double PPN = TotalPrice * 10 / 100;
            lbl_ppn.Text = PPN.ToString("F");
            Double SubTotal = TotalPrice + PPN;
            lbl_subtotal.Text = SubTotal.ToString("N0");
            Int32 Adjustment = Int32.Parse(txt_adjustment.Text);
            lbl_adjustment.Text = Adjustment.ToString("N0");
            Double Total = SubTotal - Adjustment;
            lbl_total.Text = Total.ToString("N0");
        }

        protected void txt_jumlah_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void txt_adjustment_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected bool transaction_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }
            return true;
        }        

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["FlyerHelpID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());
            //Int32 HelpID = find_help("Flyer");

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);

            Int16 Duration = 0;
            if (dt != null && dt.Rows.Count == 0)
                Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());

            DateTime OverdueDate = RequestDate.AddDays(Duration);

            Int32 Quantity = Int32.Parse(txt_jumlah.Text);
            Double Price = Double.Parse(lbl_price.Text);
            Double TotalAmount = Double.Parse(lbl_totalharga.Text);
            Double TaxPercentage = 0.1;
            Double TaxAmount = Double.Parse(lbl_ppn.Text);
            Double TotalAmountAfterTax = TotalAmount + TaxAmount;
            Int32 DiscountTypeID = 0;
            Int32 TotalDiscount = Int32.Parse(txt_adjustment.Text);
            Int32 ActualAmount = Int32.Parse(lbl_total.Text, NumberStyles.Currency);

            Int32 PeriodNo = 1; //dummy
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            request.ActualPrice = ActualAmount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                //request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;                

                String returnvalue = dbclass.save_flyer_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, Quantity, Price, TotalAmount, TaxPercentage, TaxAmount, TotalAmountAfterTax,
                    DiscountTypeID, TotalDiscount, ActualAmount, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}