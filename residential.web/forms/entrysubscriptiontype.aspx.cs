﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Configuration;
using System.Data;

namespace residential.web.forms
{
    public partial class entrysubscriptiontype : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                alert_danger.Visible = false;
                alert_success.Visible = false;
                show_all_smsgroup();
                if (Request.QueryString["SubscriptionTypeID"] != null)
                    show_subscriptiontype(Int32.Parse(Request.QueryString["SubscriptionTypeID"].ToString()));
            }
        }

        protected void show_all_smsgroup()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_SMSGroup();
            lst_smsgroup.DataSource = dt;
            lst_smsgroup.DataTextField = "SMSGroupName";
            lst_smsgroup.DataValueField = "SMSGroupID";
            lst_smsgroup.DataBind();
        }

        private void save_subscriptiontype()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 SubscriptionTypeID = 0;            
            if (Request.QueryString["SubscriptionTypeID"] != null)           
                SubscriptionTypeID = Int32.Parse(Request.QueryString["SubscriptionTypeID"].ToString());                
            String SubscriptionTypeName = txt_subscriptiontypename.Text;
            String SavedBy = Session["username"].ToString();
            SubscriptionTypeID = dbclass.save_SubscriptionType(SubscriptionTypeID, SubscriptionTypeName, SavedBy);
            
            foreach (ListItem list in lst_smsgroup.Items)
            {
                if (list.Selected)
                    dbclass.save_SubscriptionTypeDetail(SubscriptionTypeID, Int32.Parse(list.Value), SavedBy);
            }                      
        }

        private void show_subscriptiontype(Int32 _SubscriptionTypeID)
        {
            //String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            //DBclass dbclass = new DBclass(constring);

            //DataTable dt = dbclass.retrieve_SubscriptionTypeDetail(_SubscriptionTypeID);

            //txt_subscriptiontypename.Text = dt.Rows[0]["SubscriptionTypeName"].ToString();
            
            //foreach (ListItem list in lst_smsgroup.Items)
            //{
            //    if (dt.Select("SMSGroupID = '" + list.Value + "'").Length > 0)
            //        list.Selected = true;
            //}
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            save_subscriptiontype();
            if (alert_danger.Visible)
                alert_danger.Visible = false;

            alert_success_text.Text = "Data has been saved!";
            alert_success.Visible = true;                
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            save_subscriptiontype();
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            save_subscriptiontype();
            if (alert_danger.Visible)
                alert_danger.Visible = false;

            alert_success_text.Text = "Data has been saved!";
            alert_success.Visible = true;

            txt_subscriptiontypename.Text = null;
            lst_smsgroup.ClearSelection();
        }       
    }
}