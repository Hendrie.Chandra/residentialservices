﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryuplsmartcard.aspx.cs" Inherits="residential.web.forms.entryuplsmartcard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        $(document).ready(function () {
            $("#FeaturedContent_txt_search").keypress(function (event) {
                var keycode = (event.keyCode ? event.keyCode : event.which);
                if (keycode == '13') {
                    event.preventDefault();
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="container" style="margin-top: 80px;">
        <div class="page-header">
            <h1>Upload Smartcard</h1>
        </div>
        <div id="succID" class="alert alert-success alert-dismissible" role="alert" runat="server" visible="false">
            <asp:Label ID="LabelSuccess" runat="server" Visible="false"></asp:Label>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Ok!</strong> <%=LabelSuccess.Text%>
        </div>


        <div id="errID" class="alert alert-danger alert-dismissible" role="alert" runat="server" visible="false">
            <asp:Label ID="LabelDanger" runat="server" Visible="false"></asp:Label>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <strong>Warning!</strong> <%=LabelDanger.Text%>
        </div>

        <div class="panel panel-default">
            <div class="panel-body" style="background-color:#f2f2f2">
                <div class="row">
                    <div class="col-md-2">Card Type</div>
                    <div class="col-md-4">
                        <asp:DropDownList ID="cbo_cardtype" runat="server" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="col-md-4">
                        <input type="file" runat="server" class="form-control" id="txt_fileUpload" />
                    </div>
                    <div class="col-md-2">
                        <button id="btn_upload" class="btn btn-default" runat="server" style="padding-left: 50px; padding-right: 50px" onserverclick="btn_saveDocument">Upload</button>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <hr />
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-2 control-label">Case Status</label>
                        <div class="col-sm-6">
                           <asp:DropDownList ID="cbo_status" runat="server" CssClass="form-control" OnTextChanged="cbo_status_TextChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" placeholder="Card Number,Chip Number and Status.." AutoPostBack="true" OnTextChanged="txt_search_TextChanged"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" OnClick="btn_search_Click1" />
                            </span>
                        </div>
                    </div>
                </div>
            </div> 

<%--        <div class="row">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
                <asp:TextBox ID="txt_search" CssClass="form-control" runat="server"
                    OnTextChanged="txt_search_TextChanged" AutoPostBack="true" placeholder="Card Number,Chip Number and Status.."></asp:TextBox>
            </div>
        </div>--%>
        <br />
        <br />

        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-hover"
                    AllowPaging="True"
                    ShowHeaderWhenEmpty="True" DataKeyNames="CardNumber" EmptyDataText="No Data"
                    AllowSorting="true" OnSorting="gvw_data_Sorting" OnPageIndexChanging="gvw_data_PageIndexChanging" HeaderStyle-HorizontalAlign="Center">
                    <PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
                    <Columns>
                        <%--<asp:TemplateField></asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="CARD NUMBER" DataField="CardNumber" SortExpression="CardNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="CHIP NUMBER" DataField="ChipNumber" SortExpression="ChipNumber"></asp:BoundField>
                        <asp:BoundField HeaderText="CARD STATUS" DataField="CardStatus" SortExpression="CardStatus"></asp:BoundField>
<%--                        <asp:TemplateField HeaderText="CARD TYPE" ItemStyle-HorizontalAlign="Center" SortExpression="IsVisitor">
                            <ItemTemplate>
                                <asp:Label ID="Status" runat="server" Text='<%# (bool)Eval("IsVisitor")==true ? "Visitor" : "Resident/Additional Card" %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:BoundField HeaderText="CARD TYPE" DataField="UploadTypeDesc" SortExpression="UploadTypeDesc"></asp:BoundField>

                    </Columns>
                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>
