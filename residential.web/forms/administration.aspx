﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="administration.aspx.cs" Inherits="residential.web.forms.administration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="innerlinkcontent">
        <div class="innerlinkcell">
            <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icon-roles.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/forms/listrole.aspx" 
                CssClass="contentlink">Roles</asp:HyperLink>
            <br />
            Manage roles for application
        </div>
        <div class="innerlinkcell">
            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icon-users.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/forms/listuser.aspx" 
                CssClass="contentlink">User Management</asp:HyperLink>
            <br />
            Manage users for application
        </div>
    </div>    
</asp:Content>
