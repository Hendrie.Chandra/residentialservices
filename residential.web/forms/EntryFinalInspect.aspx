﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/main.master" CodeBehind="EntryFinalInspect.aspx.cs" Inherits="residential.web.forms.EntryFinalInspect" %>

<%@ Register Src="userHeaderExScheme.ascx" TagName="entryheader" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <script>
        function FormatCurrency(ctrl) {
            //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
            if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
                return;
            }
            var val = ctrl.value;
            val = val.replace(/,/g, "")
            ctrl.value = "";
            val += '';
            x = val.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;

            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            ctrl.value = x1 + x2;
        }
        function CheckNumeric() {
            return event.keyCode >= 48 && event.keyCode <= 57;
        }
    </script>
    <div class="page-header">
        <h1>Form Final Inspect <asp:Label runat="server" ID="lblTitle" /></h1>
    </div>
    <hr />
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">Case List</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Summary Case</label>
                            <div class="col-sm-9">
                                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered table-hover"
                                    ShowHeaderWhenEmpty="True"
                                     PageSize="15"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Unit" DataField="Unit" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField HeaderText="Request with Deposit" DataField="Request" SortExpression="Request" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="Request Final Inspect" DataField="FinalInspect" SortExpression="FinalInspect" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="Request Pengembalian Deposit" DataField="SecDep" SortExpression="SecDep"  ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">History Case</label>
                            <div class="col-sm-9">
                                <asp:GridView ID="gv_detail" runat="server" AutoGenerateColumns="False"
                                    CssClass="table table-bordered table-hover"
                                    ShowHeaderWhenEmpty="True"
                                    DataKeyNames="CaseNumber" PageSize="15"
                                    HeaderStyle-HorizontalAlign="Center">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Request Date" DataField="RequestDate" ItemStyle-HorizontalAlign="Right" ItemStyle-Font-Bold="true"  DataFormatString="{0:d MMMM yyyy}"/>
                                        <asp:BoundField HeaderText="Unit" DataField="Unit" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                                        <asp:BoundField HeaderText="CaseNumber" DataField="CaseNumber" SortExpression="CaseNumber" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="HelpName" DataField="HelpName" SortExpression="HelpName" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField HeaderText="Status" DataField="CaseStatusName" SortExpression="CaseStatusName"  ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <hr />
    <div class="form-group">
        <div class="col-sm-8"></div>
        <div class="col-sm-4 text-right">
            <asp:Button ID="Button1" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click"/>
            <asp:HyperLink ID="HyperLink2" runat="server" CssClass="btn btn-default" NavigateUrl="~/forms/OtherIncome.aspx">Back</asp:HyperLink>
        </div>
    </div>
</asp:Content>