﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    /// <summary>
    /// Summary description for UserSessionHandler
    /// </summary>
    public class UserSessionHandler : IHttpHandler, IReadOnlySessionState, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {            
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            String username = context.Request.QueryString["username"].ToString();
            DataTable dt = dbclass.retrieve_user(username);

            context.Session["UserID"] = dt.Rows[0]["UserID"].ToString();
            context.Session["username"] = username;
            context.Session["OrgID"] = dt.Rows[0]["OrgID"].ToString();
            context.Session["SiteID"] = dt.Rows[0]["SiteID"].ToString();
            context.Session["RoleID"] = dt.Rows[0]["RoleID"].ToString();
            if (!String.IsNullOrEmpty(dt.Rows[0]["DepartmentID"].ToString()))
                context.Session["DepartmentID"] = dt.Rows[0]["DepartmentID"].ToString();
            else
                context.Session["DepartmentID"] = "0";
            context.Session["FullName"] = dt.Rows[0]["FullName"].ToString();
            context.Session["test"] = "123";
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}