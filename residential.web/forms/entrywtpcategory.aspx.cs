﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class entrywtpcategory : System.Web.UI.Page
    {
        static String prevPage = "listwtpcategories.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                if (Request.QueryString["WaterPlantCategoryID"] != null)
                    Show_Case(Int32.Parse(Request.QueryString["WaterPlantCategoryID"].ToString()));
            }
        }

        private void Show_Case(Int32 _WaterPlantCategoryID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_waterplantcategory(_WaterPlantCategoryID);

            txt_wtpcategory.Text = dt.Rows[0]["WaterPlantCategoryName"].ToString();
            txt_price.Text = dt.Rows[0]["Price"].ToString();
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (validation())
            {
                try
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    UserData userData = (UserData)ViewState["UserData"];

                    Int32 WaterPlantCategoryID = 0;
                    if (Request.QueryString["txt_wtpcategory"] != null)
                        WaterPlantCategoryID = Int32.Parse(Request.QueryString["txt_wtpcategory"].ToString());

                    dbclass.save_WTPCategory(userData.OrgID, userData.SiteID, WaterPlantCategoryID, txt_wtpcategory.Text, Int32.Parse(txt_price.Text), userData.UserName);
                    Response.Redirect(prevPage);
                }
                catch (Exception ex)
                {
                    alert_danger_text.Text = ex.Message;
                    alert_danger.Visible = true;
                }
            }
        }

        private Boolean validation()
        {
            if (String.IsNullOrEmpty(txt_wtpcategory.Text))
            {
                alert_danger_text.Text = "WTP Category Name is required!";
                alert_danger.Visible = true;
                return false;
            }
            if (String.IsNullOrEmpty(txt_price.Text))
            {
                alert_danger_text.Text = "Price is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}