﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entryrektp_kk : System.Web.UI.Page
    {
        static String prevPage = "rektp_kk.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                hitung_total();
                show_zone();
                Int32 discount = 0;
                txt_discount.Text = discount.ToString();

                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            entryheader.ToggleAlert = false;
        }

        private void Show_Case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_repersonal_identification_by_casenumber(_casenumber);

            entryheader.OwnerName = dt.Rows[0]["psName"].ToString();
            entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
            //check customer type -> 0 = Owner, 1 = Tenant
            entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
            entryheader.combobox_unit();
            entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

            entryheader.combobox_caseorigin();
            entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

            entryheader.disable_all_controls();

            cboRequest.SelectedValue = "KTP";

            cboZone.SelectedIndex = Int32.Parse(dt.Rows[0]["ZoneID"].ToString());
            cboZone_SelectedIndexChanged(this, EventArgs.Empty);

            txtJumlahKTP.Text = dt.Rows[0]["Qty_KTP"].ToString();
            //txtJumlahKK.Text = dt.Rows[0]["Qty_KK"].ToString();

            Int32 discount = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString()));
            Int32 subtotal = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Amount"].ToString()));
            Int32 total = Convert.ToInt32(decimal.Parse(dt.Rows[0]["AmountAfterDiscount"].ToString()));

            txt_discount.Text = discount.ToString();
            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_total.Text = total.ToString("N0");
        }

        private Int32 find_help(string param)
        {
            Int32 ParameterHelpID = 0;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_parameter_help(param);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                    ParameterHelpID = Int32.Parse(dt.Rows[0]["HelpID"].ToString());
                else
                    ParameterHelpID = 0;
            }
            else
                ParameterHelpID = 0;

            return ParameterHelpID;
        }

        private void show_zone()
        {
            UserData userData = (UserData)ViewState["UserData"];

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_zone(userData.OrgID, userData.SiteID);
            cboZone.DataSource = dt;
            cboZone.DataTextField = "ZoneName";
            cboZone.DataValueField = "ZoneID";
            cboZone.DataBind();
            cboZone.Items.Insert(0, new ListItem("None", "0"));
            cboZone.SelectedIndex = 0;
        }

        private void hitung_total()
        {
            if (String.IsNullOrWhiteSpace(txtJumlahKTP.Text) ||
                String.IsNullOrWhiteSpace(txt_discount.Text))
            {
                txtJumlahKTP.Text = "1";               
                txt_discount.Text = "0";
            }

            Int32 subtotal = (Int32.Parse(txtJumlahKTP.Text) * (Int32.Parse(lblPriceKTP.Text, NumberStyles.Currency)));
            Int32 disc = Int32.Parse(txt_discount.Text);
            Int32 total = subtotal - disc;

            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_adjustment.Text = disc.ToString();
            lbl_total.Text = total.ToString("N0");
        }

        protected bool transaction_validation()
        {
            if (String.IsNullOrWhiteSpace(entryheader.PSCode))
            {
                entryheader.ShowAlert = "Owner and PSCode are Required";
                return false;
            }
            if (String.IsNullOrWhiteSpace(entryheader.Unit))
            {
                entryheader.ShowAlert = "Unit is Required";
                return false;
            }
            if (String.IsNullOrWhiteSpace(entryheader.RequestorPhone))
            {
                entryheader.ShowAlert = "Phone is Required";
                return false;
            }
            if (cboZone.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Zone is required.";                
                return false;
            }            
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String CaseNumber = save_transaction();

                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                Response.Redirect(url + (url.IndexOf("?") == -1 ? "?" : "&") + "CaseNumber=" + CaseNumber);
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["RePersonalIdentificationHelpID_KTP"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);
            Int32 QuantityKTP = Int32.Parse(txtJumlahKTP.Text);
            //Int32 QuantityKK = Int32.Parse(txtJumlahKK.Text);
            Int32 PriceKTP = Int32.Parse(lblPriceKTP.Text, NumberStyles.Currency);
            //Int32 PriceKK = Int32.Parse(lblPriceKK.Text, NumberStyles.Currency);
            Int32 TotalAmount = (QuantityKTP * PriceKTP);

            Int32 DiscountTypeID = 0;
            Int32 TotalDiscount = Int32.Parse(txt_discount.Text);
            Int32 TotalAmountAfterDiscount = TotalAmount - TotalDiscount;
            Double TaxPercentage = 0.1;
            Int32 TaxAmount = 0;
            Int32 TotalAmountAfterTax = TotalAmountAfterDiscount - TaxAmount;
            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            //String CRMCaseID = "";
            //String CaseNumber = "";
            //if (Request.QueryString["CaseNumber"] != null)
            //{
            //    DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
            //    request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //    CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //    CaseNumber = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //}
            //else
            //{
            //    CRMCaseID = Guid.NewGuid().ToString();
            //    CaseNumber = CRMCaseID;
            //}

            /*if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            }*/

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_re_personal_identification_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, Int32.Parse(cboZone.SelectedValue), QuantityKTP, 0, TotalAmount, DiscountTypeID, TotalDiscount, TotalAmountAfterDiscount,
                    TaxPercentage, TaxAmount, TotalAmountAfterTax, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected void txtJumlahKTP_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void cboZone_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboZone.SelectedIndex != 0)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DataTable dt = dbclass.retrieve_generalprice("Perpanjangan KTP", userData.SiteID, Int32.Parse(cboZone.SelectedValue.ToString()));
                Int32 price = Convert.ToInt32(dt.Rows[0]["Price"]);
                lblPriceKTP.Text = price.ToString("N0");

                hitung_total();
            }
            else
            {
                lblPriceKTP.Text = "0";               
            }
        }

        protected void cboRequest_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboRequest.SelectedValue == "KTP")
            {
                //txtJumlahKK.Enabled = false;
                txtJumlahKTP.Enabled = true;
                txtJumlahKTP.Text = "1";
                //txtJumlahKK.Text = "0";
            }
            else
            {
                //txtJumlahKK.Enabled = true;
                txtJumlahKTP.Enabled = false;
                txtJumlahKTP.Text = "0";
                //txtJumlahKK.Text = "1";
            }

            hitung_total();
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}