﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using residential.libs;

namespace residential.web.forms
{
    public partial class viewheader : System.Web.UI.UserControl
    {        
        protected void Page_Load(object sender, EventArgs e)
        {                       
        }        

        public String CustomerName
        {
            set { lbl_customername.Text = value; }
            get { return lbl_customername.Text; }       //add for smartcard
        }

        public String PSCode
        {
            set { lbl_pscode.Text = value; }
            get { return lbl_pscode.Text; }
        }

        public String Unit
        {
            set { lbl_unit.Text = value; }
            get { return lbl_unit.Text; }
        }

        public String RequestorName
        {
            set { lbl_requestorname.Text = value; }
        }

        public String RequestorPhone
        {
            set { lbl_requestorphone.Text = value; }
        }

        public String RequestorEmail
        {
            set { lbl_requestoremail.Text = value; }
        }

        public String CaseOrigin
        {
            set { lbl_caseorigin.Text = value; }
        }

        public String Description
        {
            set { lbl_description.Text = value; }
        }

        public String ShowAlert
        {
            set
            {
                alert_danger_text.Text = value;
                alert_danger.Visible = true;
            }
        }

        public Boolean ToggleAlert
        {
            set { alert_danger.Visible = value; }
        }
    }
}