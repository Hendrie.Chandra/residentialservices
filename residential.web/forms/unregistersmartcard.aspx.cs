﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;

namespace residential.web.forms
{
    public partial class unregistersmartcard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["UserData"] = new UserData(User.Identity.Name);
            ViewState["sortOrder"] = "";
            show_cbo_casestatus();
            bounddatagrid("", "");
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            Int32 CaseStatusID = Int32.Parse(cbo_status.SelectedValue);
            String Search = null;
            if (!String.IsNullOrEmpty(txt_search.Text))
                Search = txt_search.Text;

            DataTable dt = dbclass.retrieve_unregister_smartcard(userData.OrgID, userData.SiteID, CaseStatusID, userData.DepartmentID, Search);
            DataView dv = new DataView();
            dv = dt.DefaultView;
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }

            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String CaseNumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();
                String CaseStatus = DataBinder.Eval(e.Row.DataItem, "CaseStatusName").ToString();
                Int32 CaseStatusID = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "CaseStatusID").ToString());
                String linkToForm = "viewunregistersmartcard.aspx";
                //if (CaseStatus.ToUpper() == "PENDING")
                //    linkToForm = "entrysmartcard.aspx";

                e.Row.Attributes["onclick"] = "window.location = '" + linkToForm + "?CaseNumber=" + CaseNumber + "'";

                //if (!String.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString()) && (CaseStatusID == 2 || CaseStatusID == 4))
                //{
                //    DateTime OverdueDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "OverdueDate").ToString());
                //    if (DateTime.Compare(DateTime.Now, OverdueDate) >= 0)
                //        e.Row.BackColor = System.Drawing.Color.LightPink;
                //}
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        private void show_cbo_casestatus()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_casestatus();
            cbo_status.DataSource = dt;
            cbo_status.DataTextField = "CaseStatusName";
            cbo_status.DataValueField = "CaseStatusID";
            cbo_status.DataBind();
            cbo_status.Items.Insert(0, new ListItem("[ All Status ]", "0"));
            cbo_status.SelectedIndex = 0;
        }
    }
}