﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using residential.libs;
using System.Configuration;
using System.Data;
using System.Drawing;

namespace residential.web
{
    public partial class lookupreferencenumber : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["title"] != null)
                lbl_title.Text = Request.QueryString["title"].ToString();
            txt_referencenumber.Focus();
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                txt_date_CalendarExtender.StartDate = DateTime.Now;
            }
        }

        protected void btn_select_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "returnlookuprefnumber(self)", true);
        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            if (!(txt_referencenumber.Text.Length == 0 && txt_name.Text.Length == 0 && txt_date.Text.Length == 0))
            {
                bounddatagrid();
            }
        }

        protected void bounddatagrid()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.lookup_referencenumber(txt_referencenumber.Text, txt_name.Text, txt_date.Text, userData.SiteID, userData.OrgID);

            gvw_data.DataSource = dt;
            gvw_data.DataBind();       
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String casenumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();
                LinkButton lnk_select = (LinkButton)e.Row.FindControl("lnk_select");
                HyperLink lnk_casenumber = (HyperLink)e.Row.FindControl("lnk_casenumber");

                lnk_select.CommandArgument = casenumber;

                lnk_casenumber.Text = casenumber;
                lnk_casenumber.NavigateUrl = "viewbanner.aspx?CaseNumber=" + casenumber;
            }
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_select")
            {
                lbl_refnumber.Text = e.CommandArgument.ToString();

                GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int RowIndex = gvr.RowIndex;

                foreach (GridViewRow row in gvw_data.Rows)
                {
                    if (row.RowIndex == RowIndex)
                    {
                        row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                        row.ToolTip = string.Empty;
                    }
                    else
                    {
                        row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                        row.ToolTip = "Click to select this row.";
                    }
                }
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid();
        }
    }
}