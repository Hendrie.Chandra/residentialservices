﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class listsubscriptiontype : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["sortOrder"] = "";
                bounddatagrid("", "");

                String popup_url = "openWindowModal('entrysubscriptiontype.aspx?flag=1', 700, 800)";
                btn_add.Attributes.Add("onclick", popup_url);
            }
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_SubscriptionType();
            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex >= 0)
                {
                    Label lbl_no = (Label)e.Row.FindControl("lbl_no");
                    Int32 no_urut = e.Row.RowIndex + 1;
                    no_urut = gvw_data.PageIndex * gvw_data.PageSize + no_urut;
                    lbl_no.Text = no_urut.ToString();
                }

                Int32 SubscriptionTypeID = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "SubscriptionTypeID").ToString());
                String SubscriptionTypeName = DataBinder.Eval(e.Row.DataItem, "SubscriptionTypeName").ToString();                
                LinkButton lnk_subscriptiontype = (LinkButton)e.Row.FindControl("lnk_subscriptiontype");
                lnk_subscriptiontype.CommandArgument = SubscriptionTypeID.ToString();
                lnk_subscriptiontype.Text = SubscriptionTypeName;
                String popup_url = "openWindowModal('entrysubscriptiontype.aspx?flag=1&SubscriptionTypeID=" + SubscriptionTypeID.ToString() + "', 700, 700)";
                lnk_subscriptiontype.Attributes.Add("onclick", popup_url);
            }
        }

        protected void btn_add_Click(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_edit")
            {
                bounddatagrid("", "");
            }
        }
    }
}