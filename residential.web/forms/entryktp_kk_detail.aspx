﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="entryktp_kk_detail.aspx.cs"
    Inherits="residential.web.forms.entryktp_kk_detail" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>:: entry ktp / kk :: </title>
    <script type="text/javascript" src="../scripts/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="../global.js">
    </script>
    <link href="~/maincss.css" rel="stylesheet" type="text/css" />
    <base target="_self" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="popupcontainer">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="buttoncontainer">
                    <asp:Button ID="btn_saveclose" runat="server" Text="SAVE CLOSE" OnClientClick="buttonUpdating2(this)"
                        UseSubmitBehavior="false" CssClass="button" Width="100px" OnClick="btn_saveclose_Click" />
                </div>
                <div class="alert alert-success" id="alert_success" runat="server" visible="false">
                    <asp:Label runat="server" ID="alert_success_text"></asp:Label></div>
                <div class="alert alert-danger" id="alert_danger" runat="server" visible="false">
                    <asp:Label runat="server" ID="alert_danger_text"></asp:Label></div>
                <div class="formentrycontent">
                    <div class="formentrycell1">
                        <asp:Label ID="Label8" runat="server" Text="Alamat :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtAlamat" runat="server" CssClass="textbox_entry_left textboxcell1"></asp:TextBox>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2">
                        <asp:Label ID="Label13" runat="server" Text="Desa / Kelurahan :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtKelurahan" runat="server" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
                    </div>
                    <div class="formentrycell2">
                        <asp:Label ID="Label21" runat="server" Text="Kecamatan :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtKecamatan" runat="server" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
                    </div>
                </div>
                <hr />
                <div class="formentrycontent">
                    <div class="formentrycell1">
                        <asp:Label ID="Label22" runat="server" Text="Name Lengkap :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtNamaLengkap" runat="server" CssClass="textbox_entry_left textboxcell1"></asp:TextBox>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2">
                        <asp:Label ID="Label23" runat="server" Text="Jenis Kelamin:" CssClass="labelfield"></asp:Label>
                        <asp:DropDownList ID="cboJenisKelamin" runat="server" CssClass="ComboBoxWindowsStyle"
                            Width="167px">
                        </asp:DropDownList>
                    </div>
                    <div class="formentrycell2">
                        <asp:Label ID="Label1" runat="server" Text="Hubungan (KK) :" CssClass="labelfield"></asp:Label>
                        <asp:DropDownList ID="cboRelation" runat="server" CssClass="ComboBoxWindowsStyle" Width="167px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2">
                        <asp:Label ID="Label2" runat="server" Text="Tempat Lahir :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtTempatLahir" runat="server" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
                    </div>
                    <div class="formentrycell2">
                        <asp:Label ID="Label3" runat="server" Text="Tgl. Lahir :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtTglLahir" runat="server" CssClass="textbox_entry_left textboxcell2">dd/MM/yyyy</asp:TextBox>
                        <asp:CalendarExtender ID="txt_date_CalendarExtender" runat="server" TargetControlID="txtTglLahir"
                            Format="dd MMMM yyyy">
                        </asp:CalendarExtender>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2">
                        <asp:Label ID="Label4" runat="server" Text="Propinsi / Negara :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtPropinsiNegara" runat="server" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2">
                        <asp:Label ID="Label5" runat="server" Text="Status :" CssClass="labelfield"></asp:Label>
                        <asp:DropDownList ID="cboStatusPernikahan" runat="server" CssClass="ComboBoxWindowsStyle"
                            Width="167px">
                        </asp:DropDownList>
                    </div>
                    <div class="formentrycell2">
                        <asp:Label ID="Label6" runat="server" Text="Golongan Darah :" CssClass="labelfield"></asp:Label>
                        <asp:DropDownList ID="cboGolDarah" runat="server" CssClass="ComboBoxWindowsStyle" Width="167px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2">
                        <asp:Label ID="Label7" runat="server" Text="Agama :" CssClass="labelfield"></asp:Label>
                        <asp:DropDownList ID="cboAgama" runat="server" CssClass="ComboBoxWindowsStyle" Width="167px">
                        </asp:DropDownList>
                    </div>
                    <div class="formentrycell2">
                        <asp:Label ID="Label9" runat="server" Text="Pendidikan :" CssClass="labelfield"></asp:Label>
                        <asp:DropDownList ID="cboPendidikan" runat="server" CssClass="ComboBoxWindowsStyle" Width="167px">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell2">
                        <asp:Label ID="Label10" runat="server" Text="Pekerjaan :" CssClass="labelfield"></asp:Label>
                        <asp:DropDownList ID="cboPekerjaan" runat="server" CssClass="ComboBoxWindowsStyle" Width="167px">
                        </asp:DropDownList>
                    </div>
                    <div class="formentrycell2">
                        <asp:Label ID="Label11" runat="server" Text="Nama Bapak / Ibu :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtNamaOrangTua" runat="server" CssClass="textbox_entry_left textboxcell2"></asp:TextBox>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell1">
                        <asp:Label ID="Label12" runat="server" Text="Keterangan :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtKeterangan" runat="server" CssClass="textbox_entry_left textboxcell1"></asp:TextBox>
                    </div>
                </div>
                <div class="formentrycontent">
                    <div class="formentrycell1">
                        <asp:Label ID="Label14" runat="server" Text="W.N.R.I :" CssClass="labelfield"></asp:Label>
                        <asp:TextBox ID="txtWNRI" runat="server" CssClass="textbox_entry_left textboxcell1"></asp:TextBox>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
