﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.DirectoryServices.AccountManagement;

using System.Data;
using System.Data.SqlClient;

using residential.libs;
using residential.web.Classes;
using System.Security.Principal;
using System.Text.RegularExpressions;

using LKCommon;

namespace residential.web.forms
{
    public partial class entryuser : System.Web.UI.Page
    {
        private const string CheckAll = "1";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                combobox_roles();
                cbo_roles_SelectedIndexChanged(this, EventArgs.Empty);
                combobox_sites();
                cbo_site_SelectedIndexChanged(this, EventArgs.Empty);

                DataTable dtSubLocation = clsLocation.GetLocation(Helper.OrgID, Helper.SiteID, 0, 0);
                ddlSubLocation.DataSource = dtSubLocation;
                ddlSubLocation.DataTextField = "SubLocationName";
                ddlSubLocation.DataValueField = "SubLocationID";
                ddlSubLocation.DataBind();
                                  
                if (Request.QueryString["UserID"] != null)
                    show_userprofile();
            }
            alert_danger.Visible = false;
        }

        private void show_userprofile()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 UserID = Int32.Parse(Request.QueryString["UserID"].ToString());
            //DataTable dt = dbclass.retrieve_user(UserID);
            DataTable dt = clsUser.GetUser(Helper.OrgID, Helper.SiteID, UserID, string.Empty);

            DataRow datarow = dt.Rows[0];
            txt_username.Text = datarow["UserName"].ToString();
            txt_username.Enabled = false;
            txt_fullname.Text = datarow["FullName"].ToString();
            txt_mobilenumber.Text = datarow["MobileNumber"].ToString();
            cbo_roles.SelectedValue = datarow["RoleID"].ToString();
            cbo_roles_SelectedIndexChanged(this, EventArgs.Empty);
            txt_emailaddress.Text = datarow["EmailAddress"].ToString();
            cbo_site.SelectedValue = datarow["SiteID"].ToString();
            cbo_site_SelectedIndexChanged(this, EventArgs.Empty);
            cbo_departments.SelectedValue = datarow["DepartmentID"].ToString();
            //cbo_defaultsite.SelectedValue = datarow["DefaultSiteID"].ToString();

            foreach (DataRow row in dt.Rows)
            {
                foreach (ListItem item in ddlSubLocation.Items)
                {
                    if (item.Value == row["SubLocationID"].ToString())
                    {
                        item.Selected = true;
                        break;
                    }
                    else if (row["SubLocationID"].ToString() == "0")
                    {
                        item.Selected = true;
                    }
                }
            }

            ddlSubLocation_SelectedIndexChanged(null, null);
        }

        private void combobox_roles()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_role();

            cbo_roles.DataSource = dt;
            cbo_roles.DataTextField = "RoleName";
            cbo_roles.DataValueField = "RoleID";
            cbo_roles.DataBind();
            cbo_roles.SelectedIndex = 0;
        }

        private void combobox_departments(Int32 _SiteID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_department(userData.OrgID, _SiteID);

            if (dt.Rows.Count > 0)
            {
                cbo_departments.DataSource = dt;
                cbo_departments.DataTextField = "DepartmentName";
                cbo_departments.DataValueField = "DepartmentID";
                cbo_departments.DataBind();
                cbo_departments.SelectedIndex = 0;
            }
            else
                cbo_departments.Items.Clear();
        }

        //private void combobox_sites()
        //{
        //    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
        //    DBclass dbclass = new DBclass(constring);
        //    DataTable dt = dbclass.retrieve_site();

        //    cbo_site.DataSource = dt;
        //    cbo_site.DataTextField = "SiteName";
        //    cbo_site.DataValueField = "SiteID";
        //    cbo_site.DataBind();
        //    cbo_site.SelectedIndex = 0;
        //}

        private void combobox_sites()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_site(2, 0);

            cbo_site.DataSource = dt;
            cbo_site.DataTextField = "SiteName";
            cbo_site.DataValueField = "SiteID";
            cbo_site.DataBind();
            cbo_site.SelectedIndex = 0;
        }

        //private void combobox_defaultsite()
        //{
        //    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
        //    DBclass dbclass = new DBclass(constring);
        //    DataTable dt = dbclass.retrieve_site(2, 0);

        //    cbo_defaultsite.DataSource = dt;
        //    cbo_defaultsite.DataTextField = "SiteName";
        //    cbo_defaultsite.DataValueField = "SiteID";
        //    cbo_defaultsite.DataBind();
        //    cbo_defaultsite.SelectedIndex = 0;
        //}

        private Int32 save_users()
        {
            Int32 returnvalue = 0;


            UserData userData = (UserData)ViewState["UserData"];

            Int32 UserID = 0;
            String SavedBy = userData.UserName;

            if (Request.QueryString["UserID"] != null)
                UserID = Int32.Parse(Request.QueryString["UserID"].ToString());

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            String UserName = txt_username.Text;
            String FullName = txt_fullname.Text;
            String MobileNUmber = txt_mobilenumber.Text;
            String EmailAddress = txt_emailaddress.Text;

            Int32 RoleID = Int32.Parse(cbo_roles.SelectedItem.Value);
            Int32 DepartmentID = 0;
            if (cbo_departments.Items.Count > 0)
                DepartmentID = Int32.Parse(cbo_departments.SelectedValue);
            Int32 OrgID = 1;
            Int32 SiteID = Int32.Parse(cbo_site.SelectedValue);
            //Int32 DefaultSiteID = Int32.Parse(cbo_defaultsite.SelectedValue);
            //int subLocationID = CommonFunctions.StringToInt(ddlSubLocation.SelectedValue);
            string lstSubLocation = ConstructSubLocationID(); 

            Int32 DefaultSiteID = SiteID;

            DataTable dt = dbclass.retrieve_user(UserName);
            if (Request.QueryString["UserID"] != null)
            {
                DataTable dtuser = dbclass.retrieve_user(int.Parse(Request.QueryString["UserID"].ToString()));
                if (dtuser.Rows[0]["SiteID"].ToString() == dt.Rows[0]["DefaultSiteID"].ToString() && dtuser.Rows[0]["SiteID"].ToString() != cbo_site.SelectedValue)
                {
                    DefaultSiteID = int.Parse(cbo_site.SelectedValue);
                    dbclass.save_defaultusersite(0, int.Parse(cbo_site.SelectedValue), dtuser.Rows[0]["UserName"].ToString());
                }
                else
                {
                    DefaultSiteID = int.Parse(dt.Rows[0]["DefaultSiteID"].ToString());
                }

                //DataTable dtuser = dbclass.retrieve_user(int.Parse(Request.QueryString["UserID"].ToString()));
                //if (dt.Rows.Count == 1)
                //{                    
                //    if (dtuser.Rows[0]["SiteID"].ToString() != cbo_site.SelectedValue)
                //        DefaultSiteID = int.Parse(cbo_site.SelectedValue);
                //}                
                //else
                //{
                //    if (Request.QueryString["UserID"].ToString() == dt.Rows[0]["DefaultSiteID"].ToString())

                //    DefaultSiteID = int.Parse(dt.Rows[0]["DefaultSiteID"].ToString());
                //}
            }
            else
            {                
                if (dt.Rows.Count > 0)
                    DefaultSiteID = int.Parse(dt.Rows[0]["DefaultSiteID"].ToString());
            }

            String domain = ConfigurationManager.AppSettings["domain"].ToString();
            ADclass adclass = new ADclass(domain);

            //String usersid = "";
            //if (!String.IsNullOrEmpty(txt_username.Text))
            //{
            //    UserPrincipal user = adclass.getuserad(txt_username.Text);
            //    if (user != null)
            //    {
            //        usersid = user.Sid.ToString();
            //    }
            //}

            returnvalue = dbclass.save_user(UserID, UserName, FullName, MobileNUmber, EmailAddress, DepartmentID, RoleID, OrgID, SiteID, DefaultSiteID, SavedBy, lstSubLocation);

            hfSubLocation.Value = string.Empty;
            hfSubLocationAll.Value = string.Empty;

            return returnvalue;
        }

        protected Boolean search_username()
        {
            String domain = ConfigurationManager.AppSettings["domain"].ToString();
            ADclass adclass = new ADclass(domain);

            if (!txt_username.Text.Contains("\\"))
                txt_username.Text = domain + "\\" + txt_username.Text;

            if (!String.IsNullOrEmpty(txt_username.Text))
            {
                UserPrincipal user = adclass.getuserad(txt_username.Text);
                if (user != null)
                {
                    return true;
                }
            }
            return false;
        }

        protected Boolean UserValidation()
        {
            try
            {
                string userID = Request.QueryString["UserID"] != null ? Request.QueryString["UserID"].ToString() : string.Empty;

                DataTable dtUser = clsUser.GetUser(Helper.OrgID, Helper.SiteID, 0, txt_username.Text);

                if (String.IsNullOrEmpty(txt_username.Text))
                {
                    alert_danger_text.Text = "Username is Required!";
                    alert_danger.Visible = true;
                    return false;
                }

                if (!Regex.IsMatch(txt_username.Text, @"^[A-Za-z0-9\\\._-]{7,}$"))
                {
                    alert_danger_text.Text = "Username is not valid";
                    alert_danger.Visible = true;
                    return false;
                }

                if (!search_username())
                {
                    alert_danger_text.Text = "Username not found on Active Directory!";
                    alert_danger.Visible = true;
                    return false;
                }

                if (!string.IsNullOrEmpty(cbo_departments.SelectedValue) && string.IsNullOrEmpty(userID))
                {
                    DataRow[] drUser = dtUser.Select("DepartmentID = '" + cbo_departments.SelectedValue + "'");
                    if (drUser.Count() > 0)
                    {
                        alert_danger_text.Text = "User already exist for site: " + cbo_site.SelectedItem.Text + " and department: " + cbo_departments.SelectedItem.Text;
                        alert_danger.Visible = true;
                        return false;
                    }
                }

                if (userID != "")
                {
                    DataTable dt = clsUser.ValidateOldDepartment(int.Parse(userID));
                    int CheckReplacement = int.Parse(dt.Rows[0]["Result"].ToString());

                    if (CheckReplacement == 0)
                    {
                        alert_danger_text.Text = "Error !! The department don't have Replacement for user " + txt_username.Text.ToString();

                        alert_danger.Visible = true;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                alert_danger_text.Text = ex.Message;
                alert_danger.Visible = true;

                return false;
            }

            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (UserValidation())
            {
                alert_danger_text.Visible = false;
                Int32 returnvalue = save_users();
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (UserValidation())
            {
                alert_danger_text.Visible = false;
                Int32 returnvalue = save_users();
                Response.Redirect("listuser.aspx");
            }
        }

        protected void txt_username_TextChanged(object sender, EventArgs e)
        {
            try
            {
                String domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                ADclass adclass = new ADclass(domain);

                if (!txt_username.Text.Contains("\\"))
                    txt_username.Text = domain + "\\" + txt_username.Text;

                if (!String.IsNullOrEmpty(txt_username.Text))
                {
                    UserPrincipal user = adclass.getuserad(txt_username.Text);
                    if (user != null)
                    {
                        //txt_username.Text = user.DistinguishedName.Split(',')[2].Remove(0, 3) + "\\" + user.SamAccountName;
                        txt_fullname.Text = user.DisplayName;
                        txt_emailaddress.Text = user.EmailAddress;
                        txt_mobilenumber.Text = user.VoiceTelephoneNumber;
                    }
                    else
                    {
                        txt_fullname.Text = String.Empty;
                        alert_danger_text.Text = "user not found";
                        alert_danger.Visible = true;
                    }
                }
            }
            catch (Exception exx)
            {
                exx.Message.ToString();
            }
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            if (UserValidation())
            {
                alert_danger.Visible = false;
                Int32 returnvalue = save_users();
                txt_username.Text = String.Empty;
                txt_fullname.Text = String.Empty;
                txt_mobilenumber.Text = String.Empty;
                txt_emailaddress.Text = String.Empty;
                cbo_departments.SelectedIndex = 0;
                cbo_roles.SelectedIndex = 0;
            }
        }

        protected void cbo_site_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((Boolean)ViewState["UseDepartment"])
                combobox_departments(Int32.Parse(cbo_site.SelectedValue));

            //if ((Boolean)ViewState["MultipleSite"])
            //{
            //    combobox_defaultsite();
            //    cbo_defaultsite.Enabled = true;
            //}

            //if (cbo_site.SelectedValue != "2")
            //{
            //    cbo_defaultsite.Enabled = false;
            //    cbo_defaultsite.SelectedValue = cbo_site.SelectedValue;
            //}
        }

        protected void cbo_roles_SelectedIndexChanged(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_role(Int32.Parse(cbo_roles.SelectedValue));

            //if ((Boolean)dt.Rows[0]["MultipleSite"])
            //{
            //    combobox_sites();

            //    combobox_defaultsite();
            //    cbo_defaultsite.Enabled = true;
            //}
            //else
            //{
            //    combobox_sites_UseDepartment();

            //    //cbo_defaultsite.Items.Clear();
            //    cbo_defaultsite.Enabled = false;
            //}

            if ((Boolean)dt.Rows[0]["UseDepartment"])
            {
                combobox_departments(Int32.Parse(cbo_site.SelectedValue));
                cbo_departments.Enabled = true;
            }
            else
            {
                cbo_departments.Items.Clear();
                cbo_departments.Enabled = false;
            }

            ViewState["UseDepartment"] = (Boolean)dt.Rows[0]["UseDepartment"];
            //ViewState["MultipleSite"] = (Boolean)dt.Rows[0]["MultipleSite"];

            //cbo_site_SelectedIndexChanged(this, EventArgs.Empty);
        }

        protected void ddlSubLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<String> ClusterCode = new List<string>();
            int count = 0;

            foreach (System.Web.UI.WebControls.ListItem item in ddlSubLocation.Items)
            {
                if (item.Selected)
                {
                    count += 1;
                    ClusterCode.Add(item.Value);
                }

                hfSubLocation.Value = String.Join(",", ClusterCode.ToArray());
            }

            ddlSubLocation.Texts.SelectBoxCaption = count.ToString() + " Item(s) Selected";

            if (count == ddlSubLocation.Items.Count)
            {
                hfSubLocationAll.Value = CheckAll; //means check all = true;
            }
            else
            {
                hfSubLocationAll.Value = "0";
            }
        }

        private string ConstructSubLocationID()
        {
            string result = string.Empty;

            result += "<root>";

            if (hfSubLocationAll.Value == CheckAll)
            {
                result += "<row SubLocationID=\"0\" />";
            }
            else
            {
                string[] temp = hfSubLocation.Value.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string subLocationID in temp)
                {
                    result += "<row SubLocationID=\"" + subLocationID + "\" />";
                }
            }

            result += "</root>";

            return result;
        }
    }
}
