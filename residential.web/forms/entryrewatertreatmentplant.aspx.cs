﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entryrewatertreatmentplant : System.Web.UI.Page
    {
        static String prevPage = "rewatertreatmentplant.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                                
                txt_discount.Text = "0";
                lbl_subtotal.Text = "0";
                lbl_total.Text = "0";
                hiddenID.Value = "0";

                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            ucEntryHeader.ToggleAlert = false;     
        }

        private void Show_Case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_rewtp(_casenumber);

            ucEntryHeader.OwnerName = dt.Rows[0]["PSName"].ToString();
            ucEntryHeader.PSCode = dt.Rows[0]["PSCode"].ToString();
            //check customer type -> 0 = Owner, 1 = Tenant
            ucEntryHeader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
            ucEntryHeader.combobox_unit();
            ucEntryHeader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();
            
            if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                ucEntryHeader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

            ucEntryHeader.combobox_caseorigin();
            ucEntryHeader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                ucEntryHeader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                ucEntryHeader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                ucEntryHeader.RequestDescription = dt.Rows[0]["Description"].ToString();

            ucEntryHeader.disable_all_controls();

            hiddenID.Value = dt.Rows[0]["WaterPaskemID"].ToString();
            txtUkuranMeterAir.Text = dt.Rows[0]["WaterPaskemName"].ToString();
            txt_tglputus.Text = DateTime.Parse(dt.Rows[0]["CutOffDate"].ToString()).ToString("dd MMMM yyyy");
            txt_lamapemutusan.Text = dt.Rows[0]["CutOffDay"].ToString();            

            lbl_subtotal.Text = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Amount"].ToString())).ToString("N0");
            txt_discount.Text = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString())).ToString();
            lbl_total.Text = Convert.ToInt32(decimal.Parse(dt.Rows[0]["AmountAfterDiscount"].ToString())).ToString("N0");
        }               
                
        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    ucEntryHeader.ShowAlert = res;                    
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["ReWaterTreatmentPlantHelpID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = ucEntryHeader.CaseOriginID;
            String PSName = ucEntryHeader.OwnerName;
            String PSCode = ucEntryHeader.PSCode;
            String Unit = ucEntryHeader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = ucEntryHeader.RequestorName;
            String TeleponPelapor = ucEntryHeader.RequestorPhone;
            String EmailPelapor = ucEntryHeader.RequestorEmail;
            String RequestDescription = ucEntryHeader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);
            Int32 TotalAmount = Int32.Parse(lbl_subtotal.Text, NumberStyles.Currency);
            Int32 TotalDiscount = Int32.Parse(txt_discount.Text, NumberStyles.Currency);
            Int32 TotalAmountAfterDiscount = TotalAmount - TotalDiscount;
            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = ucEntryHeader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            //String CRMCaseID = "";
            //String CaseNumber = "";
            //if (Request.QueryString["CaseNumber"] != null)
            //{
            //    DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
            //    request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //    CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //    CaseNumber = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //}
            //else
            //{
            //    CRMCaseID = Guid.NewGuid().ToString();
            //    CaseNumber = CRMCaseID;
            //}

            /*if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            }*/

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_rewtp_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, ucEntryHeader.CustomerType, ucEntryHeader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, Int32.Parse(hiddenID.Value.ToString()), DateTime.Parse(txt_tglputus.Text.ToString()),
                    Int32.Parse(txt_lamapemutusan.Text.ToString()), TotalAmount, 0, TotalDiscount, TotalAmountAfterDiscount,
                    0, 0, TotalAmount, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    hiddenID.Value = "0";


                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected bool transaction_validation()
        {
            if (ucEntryHeader.Validate != "VALID")
            {
                ucEntryHeader.ShowAlert = ucEntryHeader.Validate;
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                ucEntryHeader.ShowAlert = "Invalid total value";
                return false;
            }
            return true;
        }        

        public void reset_fields(ControlCollection ctls)
        {
            foreach (Control c in ctls)
            {
                if (c is TextBox)
                {
                    TextBox tt = c as TextBox;
                    tt.Text = null;
                }
                else if (c is AjaxControlToolkit.ComboBox)
                {
                    AjaxControlToolkit.ComboBox dd = c as AjaxControlToolkit.ComboBox;
                    if (dd.Items.Count > 0)
                        dd.SelectedIndex = 0;
                }

                if (c.HasControls())
                {
                    reset_fields(c.Controls);
                }
            }
        }
        
        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            Int32 subtotal = 0;
            DataTable dt = dbclass.retrieve_ReWTPPrice(int.Parse(txt_lamapemutusan.Text.ToString()));
            if (dt != null && dt.Rows.Count != 0)
                subtotal = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Price"].ToString()));

            Int32 total = subtotal - int.Parse(txt_discount.Text.ToString());

            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_total.Text = total.ToString("N0");
        }

        protected void btn_cekPemutusan_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            if (string.IsNullOrEmpty(ucEntryHeader.Unit))
            {
                ucEntryHeader.BindCustomerInfo();
                ucEntryHeader.combobox_unit(); 
            }           

            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(ucEntryHeader.Unit))
            {
                String[] unit = ucEntryHeader.Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }

            DataTable dt = dbclass.retrieve_cutoffdateWTP(userData.OrgID, UnitCode, UnitNo, userData.SiteID);
            if (dt == null || dt.Rows.Count == 0)
            {
                ucEntryHeader.ShowAlert = "Tidak ada record pemutusan meter air untuk customer berikut.";                
                return;
            }

            txt_tglputus.Text = DateTime.Parse(dt.Rows[0]["CutOffDate"].ToString()).ToString("dd MMMM yyyy");
            
            Int32 LamaPutus = (DateTime.Today.Date - DateTime.Parse(dt.Rows[0]["CutOffDate"].ToString()).Date).Days;
            txt_lamapemutusan.Text = LamaPutus.ToString();

            dt = dbclass.GetMSWaterPaskem(userData.OrgID, userData.SiteID, UnitCode, UnitNo, LamaPutus);
            if (dt == null || dt.Rows.Count == 0)
            {
                ucEntryHeader.ShowAlert = "Harga paskem untuk unit ini belum di-setup.";                
                return;
            }
            //dt = dbclass.retrieve_pipeSizeWTP(UnitCode, UnitNo, userData.SiteID);
            //if (dt == null || dt.Rows.Count == 0)
            //{
            //    ucEntryHeader.ShowAlert = "Meter air tidak terdaftar.";                
            //    return;
            //}

            hiddenID.Value = dt.Rows[0]["WaterPaskemID"].ToString();
            txtUkuranMeterAir.Text = dt.Rows[0]["WaterPaskemName"].ToString();

            Int32 subtotal = 0;
            subtotal = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Amount"].ToString()));

            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_total.Text = subtotal.ToString("N0");
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }
    }
}
