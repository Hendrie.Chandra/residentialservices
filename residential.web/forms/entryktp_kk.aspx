﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="entryktp_kk.aspx.cs"
    Inherits="residential.web.forms.entryktp_kk" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="entryheader.ascx" TagName="entryheader" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function closeDetailForm()
        {
            $('#modalFormDetailID').modal('hide');
        }

        function showModalFormDetailID(personalDetailID)
        {
            $('#<%# txtPersonalIdentificationDetailID.ClientID %>').val(personalDetailID);
            $('#<%# btn_showDetailPersonalID.ClientID %> ').click();
            $('#modalFormDetailID').modal('show');
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="page-header">
        <h1>Form Permohonan KTP/KK</h1>
    </div>
    <asp:Button ID="btn_saveclose" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btn_saveclose_Click" OnClientClick="showLoader()" />
    <asp:Button ID="btn_back" runat="server" Text="Back" CssClass="btn btn-default" OnClick="btn_back_Click" UseSubmitBehavior="false" />
    <hr />
    <%--<div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
    </div>--%>
    <uc1:entryheader ID="entryheader" runat="server" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:HiddenField runat="server" ID="hiddenID" />
            <div class="panel panel-default">
                <div class="panel-heading">Personal ID Detail</div>
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Request</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboRequest" runat="server" CssClass="form-control"
                                    AutoPostBack="true" OnSelectedIndexChanged="cboRequest_SelectedIndexChanged">
                                    <asp:ListItem Selected="True">KTP</asp:ListItem>
                                    <asp:ListItem>KK</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <label class="col-sm-2 control-label">Zone</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="cboZone" runat="server" CssClass="form-control"
                                    OnSelectedIndexChanged="cboZone_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group" runat="server" id="ktpform">
                            <div class="col-sm-2"></div>
                            <label class="col-sm-4 control-label">Kartu Tanda Penduduk (KTP)</label>
                            <label class="col-sm-2 control-label">Quantity x Price</label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtJumlahKTP" runat="server" Text="1" CssClass="form-control"
                                    OnTextChanged="txtJumlahKTP_TextChanged" AutoPostBack="true" Type="Number"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                <p class="form-control-static">
                                    &nbsp;x&nbsp;<asp:Label ID="lblPriceKTP" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-group" runat="server" id="kkform">
                            <div class="col-sm-2"></div>
                            <label class="col-sm-4 control-label">Kartu Keluarga (KK)</label>
                            <label class="col-sm-2 control-label">Quantity x Price</label>
                            <div class="col-sm-2">
                                <asp:TextBox ID="txtJumlahKK" runat="server" Text="1" CssClass="form-control"
                                    OnTextChanged="txtJumlahKK_TextChanged" AutoPostBack="true"></asp:TextBox>
                            </div>
                            <div class="col-sm-2">
                                <p class="form-control-static">
                                    &nbsp;x&nbsp;<asp:Label ID="lblPriceKK" runat="server" Text="0"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Personal ID Detail</label>
                            <div class="col-sm-10">
                                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                                    OnRowDataBound="gvw_data_RowDataBound" AllowPaging="True" OnPageIndexChanging="gvw_data_PageIndexChanging"
                                    ShowHeaderWhenEmpty="True" DataKeyNames="PersonalIdentificationDetailID" PageSize="5">
                                    <PagerStyle CssClass="pagination-ys" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chk_selectall" runat="server"
                                                    OnCheckedChanged="chk_selectall_CheckedChanged" AutoPostBack="True" />
                                            </HeaderTemplate>
                                            <HeaderStyle CssClass="gridview_header" Width="30px" />
                                            <ItemStyle CssClass="gridview_item_center" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chk_select" runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField HeaderText="Name" DataField="Name" SortExpression="Name" />                                        
                                        <asp:BoundField HeaderText="Gender" DataField="Gender" SortExpression="Gender"></asp:BoundField>
                                        <asp:BoundField HeaderText="Birth Place" DataField="BirthPlace" SortExpression="BirthPlace"></asp:BoundField>
                                        <asp:BoundField HeaderText="Birth Date" DataField="Birthdate" SortExpression="Birthdate"
                                            DataFormatString="{0:d MMMM yyyy}"></asp:BoundField>
                                        <asp:TemplateField HeaderText="Edit" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:HyperLink runat="server" ID="lnk_edit" CssClass="btn btn-default btn-sm" Text="Edit"></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No data
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 text-right">
                                <asp:Button ID="btn_add_detail" runat="server" Text="Add Detail" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_add_detail_Click"
                                    data-toggle="modal" data-target="#modalFormDetailID" />
                                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn btn-default"
                                    OnClick="btnDelete_Click" UseSubmitBehavior="false" />
                                <asp:Button ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-info" style="display:none;"
                                    OnClick="btnRefresh_Click" UseSubmitBehavior="false" />
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-6"></div>
                            <label class="col-sm-2 control-label">Adjustment</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="txt_discount" runat="server" CssClass="form-control" Type="Number"
                                    AutoPostBack="True" OnTextChanged="txt_discount_TextChanged">0</asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Sub Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6"></div>
                    <label class="col-sm-2 control-label">Total</label>
                    <div class="col-sm-4 text-right">
                        <p class="form-control-static">
                            <asp:Label ID="lbl_total" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="modalFormDetailID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Personal ID Detail</h4>
                </div>
                <div class="modal-body">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>                            
                            <asp:Button ID="btn_savedetail" runat="server" Text="Save" CssClass="btn btn-primary" UseSubmitBehavior="false" OnClick="btn_savedetail_Click" />    
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>     
                            <asp:Button ID="btn_showDetailPersonalID" runat="server" UseSubmitBehavior="false" Style="display:none" OnClick="btn_showDetailPersonalID_Click" /> 
                            <hr />
                            <div class="alert alert-danger alert-dismissible" role="alert" runat="server" id="alert_danger" visible="false">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <asp:Label ID="alert_danger_text" runat="server" Text="Label"></asp:Label>
                            </div>
                            <asp:HiddenField ID="txtPersonalIdentificationDetailID" runat="server" />
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Lengkap</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtNamaLengkap" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Kelamin</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="cboJenisKelamin" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Hubungan</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="cboRelation" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tempat Lahir</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtTempatLahir" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="col-sm-2 control-label">Tanggal Lahir</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtTglLahir" runat="server" CssClass="form-control">dd/MM/yyyy</asp:TextBox>
                                    <asp:CalendarExtender ID="txt_date_CalendarExtender" runat="server" TargetControlID="txtTglLahir"
                                        Format="dd MMMM yyyy">
                                    </asp:CalendarExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Alamat</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtAlamat" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Desa / Kelurahan</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtKelurahan" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="col-sm-2 control-label">Kecamatan</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtKecamatan" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Propinsi / Negara</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtPropinsiNegara" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="cboStatusPernikahan" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Golongan Darah</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="cboGolDarah" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Agama</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="cboAgama" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Pendidikan</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="cboPendidikan" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pekerjaan</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="cboPekerjaan" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                    </div>
                                    <label class="col-sm-2 control-label">Nama Bapak / Ibu</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtNamaOrangTua" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Keterangan</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtKeterangan" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">W.N.R.I</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="txtWNRI" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>                                    
                                </div>
                            </div>                            
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
