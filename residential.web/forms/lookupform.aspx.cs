﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using residential.libs;
using System.Configuration;
using System.Data;
using System.Drawing;

namespace residential.web.forms
{
    public partial class lookupform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                if (Request.QueryString["title"] != null)
                    lbl_title.Text = Request.QueryString["title"].ToString();
                gvw_data.DataBind();
            }
        }

        protected void bounddatagrid()
        {            
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            Int32 customertype = Int32.Parse(rb_customertype.SelectedValue);            
            
            DataTable dt = dbclass.lookup_personal(customertype, userData.OrgID, userData.SiteID, txt_personal.Text, txt_unit.Text, txt_unitno.Text);

            gvw_data.DataSource = dt;
            gvw_data.DataBind();
            
            LoaderImage.CssClass = "hide-loading-animation";
            ViewState["totalRows"] = dt.Rows.Count;
        }

        protected void btn_select_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "returnlookup(self)", true);
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {           
            bounddatagrid();
            gvw_data_PageIndexChanging(this, new GridViewPageEventArgs(0));
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String pscode = DataBinder.Eval(e.Row.DataItem, "pscode").ToString();
                String psname = DataBinder.Eval(e.Row.DataItem, "psname").ToString();
                //LinkButton lnk_pscode = (LinkButton)e.Row.FindControl("lnk_pscode");
                //lnk_pscode.Text = pscode;
                //lnk_pscode.CommandArgument = pscode + "|" + psname;

                e.Row.Attributes["onclick"] = Page.ClientScript.GetPostBackClientHyperlink(gvw_data, "Select$" + e.Row.RowIndex);
                
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#FFFFBF'");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;            
            bounddatagrid();

            int entireRows = gvw_data.Rows.Count;
            if (entireRows > 0)
            {
                int pageSize = gvw_data.PageSize;
                int pageIndex = e.NewPageIndex;
                int startData = pageIndex * pageSize + 1;
                int endData = startData + entireRows - 1;
                lbl_itemdisplayed.Text = "Displaying " + startData + "-" + endData + " of about " + ViewState["totalRows"].ToString() + " records";
            }
            else
                lbl_itemdisplayed.Text = "No Records!";
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_select")
            {
                String[] args = e.CommandArgument.ToString().Split('|');
                lbl_pscode.Text = args[0];
                lbl_psname.Text = args[1];

                GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int RowIndex = gvr.RowIndex;

                foreach (GridViewRow row in gvw_data.Rows)
                {
                    if (row.RowIndex == RowIndex)
                    {
                        row.BackColor = ColorTranslator.FromHtml("#A1DCF2");
                        row.ToolTip = string.Empty;
                    }
                    else
                    {
                        row.BackColor = ColorTranslator.FromHtml("#FFFFFF");
                        row.ToolTip = "Click to select this row.";
                    }
                }
            }
        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
        }

        protected void gvw_data_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvw_data.SelectedRow.BackColor = ColorTranslator.FromHtml("#A1DCF2");
            lbl_customertype.Text = rb_customertype.SelectedValue;
            lbl_pscode.Text = gvw_data.SelectedRow.Cells[0].Text;
            lbl_psname.Text = gvw_data.SelectedRow.Cells[1].Text;            

            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "returnlookup(self)", true);
        }      
    }
}