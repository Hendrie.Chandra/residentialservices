﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using residential.libs;
using System.Configuration;
using System.Data;

namespace residential.web.forms
{
    public partial class CDDueCase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["UserData"] = new UserData(User.Identity.Name);
            bounddatagrid();
            SD.Text = "Period      :   " + Request.QueryString["StartDate"] + " - " + Request.QueryString["EndDate"];
            DN.Text = "Department  :   " + Request.QueryString["DeptName"];
            ID.Text = "Case Status :   " + Request.QueryString["CSID"];
        }

        private void bounddatagrid()
        {
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];
                string DeptName = Convert.ToString(Request.QueryString["DeptName"]);

                DateTime StartDate = Convert.ToDateTime(Request.QueryString["StartDate"]);
                DateTime EndDate = Convert.ToDateTime(Request.QueryString["EndDate"]);

                if (DeptName == "[ All Dept ]")
                {
                    DeptName = null;
                }

                //Int32 CSID = Int32.Parse(Request.QueryString["CSID"]);
                string CSID = Convert.ToString(Request.QueryString["CSID"]);

                DataSet dt = dbclass.cdduecase(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName, CSID);

                DataView dv = new DataView(dt.Tables[0]);

                gvw_data.DataSource = null;
                gvw_data.DataSource = dv;
                gvw_data.DataBind();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid();
        }
    }
}