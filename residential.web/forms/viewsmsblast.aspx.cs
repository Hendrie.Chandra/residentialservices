﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Configuration;
using System.Data;
using System.IO;
using residential.libs.Models;
using System.Net;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace residential.web.forms
{
    public partial class viewsmsblast : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                show_case(Request.QueryString["CaseNumber"]);                                
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_SMSBlast(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                //if (!String.IsNullOrEmpty(dt.Rows[0]["SMSPriority"].ToString()))
                //    lbl_priority.Text = dt.Rows[0]["SMSPriority"].ToString();
                if (!String.IsNullOrEmpty(dt.Rows[0]["BlastSchedule"].ToString()))
                    lbl_schedule.Text = ((DateTime)dt.Rows[0]["BlastSchedule"]).ToString("dd-MM-yyyy hh:mm");

                lbl_smscontent.Text = dt.Rows[0]["MessageDetail"].ToString();
                lbl_sender.Text = dt.Rows[0]["SMSSender"].ToString();
                lbl_status.Text = dt.Rows[0]["SMSStatusName"].ToString();

                if (Int32.Parse(dt.Rows[0]["SMSStatusID"].ToString()) != 1)
                    btn_send.Visible = false;

                Decimal price = Decimal.Parse(dt.Rows[0]["Price"].ToString());
                lbl_quantityprice.Text = dt.Rows[0]["Quantity"].ToString() + " X " + price.ToString("N0");

                Decimal discount = Decimal.Parse(dt.Rows[0]["TotalDiscount"].ToString());
                lbl_adjustment.Text = discount.ToString("N0");

                Decimal subtotal = Decimal.Parse(dt.Rows[0]["TotalAmount"].ToString());
                lbl_subtotal.Text = subtotal.ToString("N0");

                Decimal total = Decimal.Parse(dt.Rows[0]["TotalAmountAfterDiscount"].ToString());
                lbl_total.Text = total.ToString("N0");

                DataTable dt_recipients = dbclass.retrieve_SMSBlastNumberDetail(_casenumber);

                gvw_recipients.DataSource = dt_recipients;
                gvw_recipients.DataBind();
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }
        
        protected void btn_send_Click(object sender, EventArgs e)
        {            
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            dbclass.Update_SMSStatus(Request.QueryString["CaseNumber"].ToString(), 2);            
        }
    }
}