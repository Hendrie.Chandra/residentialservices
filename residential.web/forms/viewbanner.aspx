﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewbanner.aspx.cs"
    Inherits="residential.web.forms.viewbanner" %>

<%@ Register Src="~/forms/viewheader.ascx" TagName="ViewHeader" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/updatecase.ascx" TagName="UpdateCase" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/tickethistory.ascx" TagName="TicketHistory" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/viewcaselibrary.ascx" TagName="Attachment" TagPrefix="aspuc" %>
<%@ Register Src="~/forms/emailviewer.ascx" TagName="EmailViewer" TagPrefix="aspuc" %>  
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-header">
        <h1>Pemasangan Banner <small><%: Request.QueryString["CaseNumber"].ToString() %></small></h1>
    </div>
    <ul class="nav nav-tabs" role="tablist" id="viewHeaderTab">
        <li>
            <a data-toggle="tab" role="tab" href="#tab_updatecase">Update Case</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_tickethistory">Ticket History</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_attachment">Attachment</a>
        </li>
        <li>
            <a data-toggle="tab" role="tab" href="#tab_email">Email Viewer</a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="form-group"></div>
        <div id="tab_updatecase" class="tab-pane fade">
            <aspuc:UpdateCase ID="updatecase1" runat="server" />
        </div>
        <div id="tab_tickethistory" class="tab-pane fade">
            <aspuc:TicketHistory ID="tickethistory" runat="server" />
        </div>
        <div id="tab_attachment" class="tab-pane fade">
            <aspuc:Attachment ID="attachment" runat="server" />
        </div>
        <div id="tab_email" class="tab-pane fade">
            <aspuc:EmailViewer ID="emailviewer" runat="server" />
        </div>
    </div>
    <hr />
    <aspuc:ViewHeader ID="viewheader" runat="server" />
    <div class="panel panel-default">
        <div class="panel-heading">Banner Detail</div>
        <div class="panel-body">
            <div class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Banner Detail List</label>
                    <div class="col-sm-10">
                        <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" CssClass="table table-hover table-bordered"
                            AllowPaging="True" ShowHeaderWhenEmpty="true" OnPageIndexChanging="gvw_data_PageIndexChanging"
                            DataKeyNames="BannerDetailID">
                            <Columns>
                                <asp:TemplateField HeaderText="No" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Banner Type" DataField="BannerTypeName" SortExpression="BannerTypeName">
                                    <HeaderStyle CssClass="gridview_header" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Location" DataField="LocationName" SortExpression="LocationName">
                                    <HeaderStyle CssClass="gridview_header" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Point" DataField="PointName" SortExpression="PointName">
                                    <HeaderStyle CssClass="gridview_header" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Start Date" DataField="StartDate" SortExpression="StartDate"
                                    DataFormatString="{0:d MMMM yyyy}">
                                    <HeaderStyle CssClass="gridview_header" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="To Date" DataField="ToDate" SortExpression="ToDate" DataFormatString="{0:d MMMM yyyy}">
                                    <HeaderStyle CssClass="gridview_header" />
                                    <ItemStyle CssClass="gridview_item_left" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="Price" DataField="Price" SortExpression="Price">
                                    <HeaderStyle CssClass="gridview_header" />
                                    <ItemStyle CssClass="gridview_item_right" />
                                </asp:BoundField>
                            </Columns>
                            <EmptyDataTemplate>
                                No data
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
                <div class="form-group" runat="server" id="adjustmentForm" visible="false">
                    <label class="col-sm-2 control-label">Adjustment</label>
                    <div class="col-sm-4">
                        <asp:TextBox ID="txt_adjustment" runat="server" CssClass="form-control" AutoPostBack="True" OnTextChanged="txt_adjustment_TextChanged" Type="Number"></asp:TextBox>
                    </div>
                    <div class="col-sm-4">
                        <asp:Button ID="btn_updateAdjustment" runat="server" Text="Update Adjustment" OnClick="btn_updateAdjustment_Click" CssClass="form-control" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Total Price</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_sewatempat" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Installation Fee</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_biayapasang" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">PPN 10%</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_ppn" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Sub Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_subtotal" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Adjustment</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_adjustment" runat="server"></asp:Label>
                </p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-6"></div>
            <label class="col-sm-2 control-label">Total</label>
            <div class="col-sm-4 text-right">
                <p class="form-control-static">
                    <asp:Label ID="lbl_total" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
