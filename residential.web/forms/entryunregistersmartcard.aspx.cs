﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using residential.libs;
using residential.libs.Models;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using residential.web.model;

using System.Linq;

using FirebirdSql.Data.FirebirdClient;
using residential.web.Classes;
using System.Data.SqlClient;

namespace residential.web.forms
{
    public partial class entryunregistersmartcard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                load_vehicle_type();
                ViewState["UserData"] = new UserData(User.Identity.Name);
            }
            Session["SmartCardHistory"] = null;
            Session["MembershipOwnership"] = null;
            entryheadersmartcard.ToggleAlert = false;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "load_unit_history", "load_unit_history();", true);

            if (!string.IsNullOrEmpty(entryheadersmartcard.UnitCluster))
            {
                string[] unit_cluster = entryheadersmartcard.UnitCluster.Split('-');
                string UnitCode = "";
                string UnitNo = "";
                if (unit_cluster.Length == 2)
                {
                    UnitCode = unit_cluster[0];
                    UnitNo = unit_cluster[1];
                }

                UserData userData = (UserData)ViewState["UserData"];
                Int32 OrgID = userData.OrgID;
                Int32 SiteID = userData.SiteID;
                entryheadersmartcard.retrieve_smartcard_history_byunit(OrgID, SiteID, UnitCode, UnitNo);

                retrieve_unregister_smartcard(OrgID, SiteID, UnitCode, UnitNo);
            }  

            if (Session["MembershipOwnership"] != null)
            {
                int chk_counter = 0;
                foreach (GridViewRow gvrow in gvw_data.Rows)
                {
                    CheckBox chkselect = (CheckBox)gvrow.FindControl("chk_select");

                    if (chkselect.Checked)
                    {
                        chk_counter++;
                    }
                }
                if (chk_counter == 0)
                {
                    bound_smartcardhistory();
                    bound_membership_ownership();
                }                   
            }
        }

        private void bound_smartcardhistory()
        {
            gvw_smartcardhistory.DataSource = Session["SmartCardHistory"];
            gvw_smartcardhistory.DataBind();
        }

        public void bound_membership_ownership() 
        {
            gvw_data.DataSource = Session["MembershipOwnership"];
            gvw_data.DataBind();
        }

        public void retrieve_unregister_smartcard(int _orgID, int _siteID, string _unitCode, string _unitNo)
        {
            Session["MembershipOwnership"] = null;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            //DataTable dt = dbclass.retrieve_membershipcard_ownership(pscode);
            DataTable dt = null;
            if (!string.IsNullOrEmpty(entryheadersmartcard.UnitCluster))
            {
                string[] unit_cluster = entryheadersmartcard.UnitCluster.Split('-');
                string UnitCode = "";
                string UnitNo = "";
                if (unit_cluster.Length == 2)
                {
                    UnitCode = unit_cluster[0];
                    UnitNo = unit_cluster[1];
                }

                dt = dbclass.retrieve_smartcard_history_byunit(_orgID, _siteID, _unitCode, _unitNo);
            }
            List<MembershipOwnership> list = new List<MembershipOwnership>();
            foreach (DataRow dr in dt.Rows)
            {
                MembershipOwnership obj = new MembershipOwnership();
                obj.MembershipID = (int)dr["MembershipID"];
                obj.CardNumber = dr["CardNumber"].ToString();
                obj.ChipNumber = dr["ChipNumber"].ToString();
                obj.PSCode = dr["PSCode"].ToString();
                obj.BoomGateID = (int)dr["BoomGateID"];
                obj.BoomGateName = dr["BoomGateName"].ToString();
                obj.BoomGateCode = dr["BoomGateCode"].ToString();
                obj.Status = (int)dr["Status"];
                obj.Sync = (int)dr["Sync"];

                list.Add(obj);
            }
            Session["MembershipOwnership"] = list;
        }

        public void retrieve_smartcard_history(string pscode)
        {
            Session["SmartCardHistory"] = null;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_smartcard_history(pscode);

            List<SmartCardHistory> list = new List<SmartCardHistory>();

            foreach (DataRow dr in dt.Rows)
            {
                SmartCardHistory obj = new SmartCardHistory();

                obj.OrgID = (int)dr["OrgID"];
                obj.SiteID = (int)dr["SiteID"];
                obj.SmartCardDetailID = (int)dr["SmartCardDetailID"];
                obj.CaseNumber = dr["CaseNumber"].ToString();

                obj.CardNumber = dr["CardNumber"].ToString();
                obj.ChipNumber = dr["ChipNumber"].ToString();
                obj.HolderName = dr["HolderName"].ToString();
                obj.VehicleNumber = dr["VehicleNumber"].ToString();
                obj.BoomGateID = (int)dr["BoomGateID"];
                obj.BoomGateName = dr["BoomGateName"].ToString();
                obj.CreatedDate = (DateTime)dr["CreatedDate"];
                obj.CreatedBy = dr["CreatedBy"].ToString();

                list.Add(obj);
            }

            Session["SmartCardHistory"] = list;
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink lnk_detail = e.Row.FindControl("lnk_detail") as HyperLink;
                String cardnumber = DataBinder.Eval(e.Row.DataItem, "CardNumber").ToString();
                lnk_detail.Attributes["onclick"] = "showModalFormDetailID(" + cardnumber + ");";
                
                Label IsActive = e.Row.FindControl("Status") as Label;
                string lbl_IsActive = "";
                lbl_IsActive = IsActive.Text.ToLower();
                
                if(lbl_IsActive!="active")
                {                   
                    e.Row.BackColor = System.Drawing.Color.Tomato;
                    CheckBox chkbox = e.Row.FindControl("chk_select") as CheckBox;
                    chkbox.Enabled = false;
                }
                
                //e.Row.BackColor = System.Drawing.Color.Red;
                //HyperLink lnk_edit = e.Row.FindControl("lnk_edit") as HyperLink;
                //String SmartcardIdentificationDetailID = DataBinder.Eval(e.Row.DataItem, "SmartcardIdentificationDetailID").ToString();
                //lnk_edit.Attributes["onclick"] = "showModalFormDetailID(" + SmartcardIdentificationDetailID + ");";
            }
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bound_smartcardhistory();
        }

        protected void lnkbtn_unregister_Click(object sender, EventArgs e) 
        {
            do_unregister_smartcrad();
        }

        protected void do_unregister_smartcrad()
        {
            //errID.Visible = true;
            UserData userData = (UserData)ViewState["UserData"];
            List<MembershipOwnership> unregmembership = new List<MembershipOwnership>();

            foreach (GridViewRow gvr in gvw_data.Rows)
            {                
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");

                if (chkselect.Checked)
                {                    
                    Int32 MembershipID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());
                    
                    MembershipOwnership obj = new MembershipOwnership();
                    obj.MembershipID=MembershipID;
                    obj.CardNumber = gvr.Cells[1].Text;
                    obj.ChipNumber = gvr.Cells[2].Text;
                    obj.BoomGateID = int.Parse(gvr.Cells[3].Text);  
                    obj.BoomGateCode = gvr.Cells[4].Text;

                    unregmembership.Add(obj); 
                }
            }

            if (unregmembership.Count > 0)
            {
                /*
                 * insert transaction / case
                 * case direcly updated to progress complated
                 * update selected smartcard to inactive
                 * send selected smartcard to pushData database boomgdate
                */

                String SavedBy = userData.UserName;
                Int32 OrgID = userData.OrgID;
                Int32 SiteID = userData.SiteID;

                if (transaction_validation())
                {
                    String res = save_transaction();
                    
                    if (!String.IsNullOrEmpty(res))
                    {
                        entryheadersmartcard.ShowAlert = res;                        
                    }
                    else
                    {
                        //pushData to boomgate
                        String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                        DBclass dbclass = new DBclass(constring);

                        //add 4 jan 2017
                        string pscode = (entryheadersmartcard.PSCode).Trim();

                        List<TempCardNumber> temp = new List<TempCardNumber>();
                        foreach (MembershipOwnership unreg in unregmembership.OrderBy(c=>c.MembershipID))
                        {
                            //string returnval_unreg = dbclass.update_unregister_card(unreg.SmartCardDetailID, unreg.BoomGateCode);

                            //insert into trclusteraccess
                            int intemp = (from d in temp
                                         where d.CardNumber==unreg.CardNumber
                                         select d).ToList().Count();
                            
                            if (intemp<=0)
                            {
                                var unregcardlist = from card in unregmembership
                                                    where card.MembershipID==unreg.MembershipID
                                                    select card;

                                int TRSmartcardID = int.Parse(Session["UnregisterSmartcardID"].ToString());                                

                                String[] returnvalue = dbclass.save_card_detail_unregister(userData.OrgID,userData.SiteID, TRSmartcardID, unreg.CardNumber, unreg.ChipNumber,userData.UserName);
                                                                
                                int TRCardDetailID = int.Parse(returnvalue[1]);
                                foreach (var c in unregcardlist)
                                {                                                                 
                                    //string[] alamat = entryheadersmartcard.Unit.Split('-');
                                    //string customername = "";
                                    //String cluster = "";
                                    //String unitnumber = "";
                                    //string _alamat = "";

                                    //DataTable dtmembershipdetail = dbclass.retrieve_membershipcard_detail(unreg.CardNumber);
                                    //customername = dtmembershipdetail.Rows[0]["Holdername"].ToString();

                                    //new 5 januari
                                    string _custName = "";
                                    string _unitCode = "";
                                    string _unitNo = "";
                                    int? _cardTypeId = null;
                                    string boomgateCardType = "";
                                    string _alamat = "";
                                    DataTable dtcd = dbclass.retrieve_membershipcard_detail(c.CardNumber);

                                    if (dtcd.Rows.Count > 0)
                                    {
                                        _custName = dtcd.Rows[0]["HolderName"].ToString();
                                        _unitCode = dtcd.Rows[0]["UnitCode"].ToString();
                                        _unitNo = dtcd.Rows[0]["UnitNo"].ToString();
                                        _cardTypeId = (int)dtcd.Rows[0]["CardTypeID"];

                                        if (_cardTypeId == 1)//resident
                                            boomgateCardType = "R";
                                        else if (_cardTypeId == 2)
                                            boomgateCardType = "R";
                                    }

                                    try
                                    {
                                        //if (alamat.Length == 2)
                                        //{
                                        //    cluster = alamat[0];
                                        //    unitnumber = alamat[1];
                                        //    _alamat = "Cluster : " + cluster + " , Unit No : " + unitnumber;
                                        //}

                                        _alamat = "Cluster: " + _unitCode + ",Unit No : " + _unitNo;


                                        //PushDataBoomGate.PushData(c.BoomGateCode.ToString, c.ChipNumber, customername, _alamat, "DELETE", boomgateCardType, _carddetail.CardNumber, boomgateExpiredDate); 
                                        //PushDataBoomGate.PushData(c.BoomGateCode.ToString(), c.ChipNumber, customername, _alamat, "DELETE", null, c.CardNumber, null,"123",null); 
                                        PushDataBoomGate.PushData(c.BoomGateCode.ToString(), c.ChipNumber, _custName, "Cluster: " + _unitCode + ",Unit No : " + _unitNo, "DELETE", boomgateCardType, c.CardNumber, null, pscode, DateTime.Now.ToLocalTime());

                                        
                                        string ret_value = dbclass.insert_transaction_cluster_access(OrgID, SiteID, TRCardDetailID, c.BoomGateID, 2);
                                        string returnval_unreg = dbclass.update_unregister_card(c.MembershipID, c.BoomGateID,2);

                                        //pushdata log
                                        int pushdatalog = insert_pushdata_log("Unregister Card", c.BoomGateCode.ToString(), c.ChipNumber, _custName, _alamat, boomgateCardType, c.CardNumber, "DELETE", null, pscode, DateTime.Now.ToLocalTime(), "Success", 1);//log sukses
                                    }
                                    catch (Exception fbde)
                                    {
                                        
                                        string ret_value = dbclass.insert_transaction_cluster_access(OrgID, SiteID, TRCardDetailID, c.BoomGateID, 0);
                                        string returnval_unreg = dbclass.update_unregister_card(c.MembershipID, c.BoomGateID, 1);
                                        int retunpushdata = dbclass.save_pushdata_error(c.BoomGateID.ToString(), c.ChipNumber, _custName, "Cluster: " + _unitCode + ",Unit No : " + _unitNo, "DELETE", fbde.Message.ToString());
                                        //error on push data to boomgate

                                        //pushdata log
                                        int pushdatalog = insert_pushdata_log("Unregister Card", c.BoomGateCode.ToString(), c.ChipNumber, _custName, _alamat, boomgateCardType, c.CardNumber, "DELETE", null, pscode, DateTime.Now.ToLocalTime(), "Failed with message : " + fbde.Message.ToString(), 0);//log gagal
                                    }
                                    //string returnval_unreg = dbclass.update_unregister_card(c.MembershipID, c.BoomGateID);
                                }
                            }
                            TempCardNumber tmpcardnumber = new TempCardNumber();
                            tmpcardnumber.CardNumber=unreg.CardNumber;
                            temp.Add(tmpcardnumber);                            
                        }
                        Response.Redirect("unregistersmartcard.aspx");
                    }                        
                }
            }
            else
            {
                entryheadersmartcard.ShowAlert = "No data selected";
            }
        }

        protected bool transaction_validation()
        {
            if (entryheadersmartcard.Validate != "VALID")
            {
                entryheadersmartcard.ShowAlert = entryheadersmartcard.Validate;
                return false;
            }
            return true;
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        //private void update_to_crm(String _casenumber, Int32 _crmcasestatusid, String _remarks, String _updatedby, String _workedby, Int32 _SatisfactionID)
        //{
        //    String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
        //    String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
        //    String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
        //    String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
        //    String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();

        //    CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

        //    UpdateCaseHelpDeskStatusRequest request = new UpdateCaseHelpDeskStatusRequest();
        //    request.CRMCaseNumber = _casenumber;
        //    request.CRMCaseStatusID = _crmcasestatusid;
        //    request.Remarks = _remarks;
        //    request.UpdateBy = _updatedby;
        //    request.UpdateDate = DateTime.Now;
        //    request.WorkedBy = _workedby;
        //    request.SatisfactionID = _SatisfactionID;
        //    UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(request);
        //}

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["SmartCardHelpID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());
            //Int32 HelpID = find_help("Flyer");

            Int32 CaseOriginID = entryheadersmartcard.CaseOriginID;
            String PSName = entryheadersmartcard.OwnerName;
            String PSCode = entryheadersmartcard.PSCode;
            String Unit = entryheadersmartcard.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheadersmartcard.RequestorName;
            String TeleponPelapor = entryheadersmartcard.RequestorPhone;
            String EmailPelapor = entryheadersmartcard.RequestorEmail;
            String RequestDescription = entryheadersmartcard.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);

            Int16 Duration = 0;
            if (dt != null && dt.Rows.Count == 0)
                Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());

            DateTime OverdueDate = RequestDate.AddDays(Duration);            

            Int32 PeriodNo = 1; //dummy
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheadersmartcard.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            //request.TotalPrice = TotalAmount;
            //request.ActualPrice = ActualAmount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                /*request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();*/
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                //unregister smartcard : transaction type : 2 hardcoded
                Session["UnregisterSmartcardID"] = null;
                //string[] returnvalue = dbclass.save_smartcard_unregister_transaction(OrgID, SiteID, 2, CRMCaseID, CaseNumber, 1, HelpID,
                //        CaseOriginID, entryheadersmartcard.CustomerType, entryheadersmartcard.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                //        RequestDescription, OverdueDate, null, null, null, entryheadersmartcard.RequestorEmail, entryheadersmartcard.RequestorPhone, entryheadersmartcard.RequestorPhone, null, 0, 0,
                //        false, false, false, false, false, false, false, false, false, null,
                //        PeriodNo, IsSetted, SavedBy, FullName);

                string[] returnvalue = dbclass.save_smartcard_unregister_transaction(OrgID, SiteID, 2, CRMCaseID, CaseNumber, 1, HelpID,
                        CaseOriginID, entryheadersmartcard.CustomerType, entryheadersmartcard.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                        RequestDescription, OverdueDate, null, null, null, entryheadersmartcard.RequestorEmail, entryheadersmartcard.RequestorPhone, entryheadersmartcard.RequestorPhone, null, 0, 0,                   
                        PeriodNo, IsSetted, SavedBy, FullName);

                //update case to progress complate
                string Remarks = "Unregister smartcard";
                Int32 returnvalutickethistory = dbclass.save_tickethistory(CaseNumber, 6, FullName, Remarks, SavedBy, null);
                /*update_to_crm(CaseNumber, 6, Remarks, FullName, null, 0);*/

                if (returnvalue[0] != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue[0].ToString();
                }
                else
                {                    
                    Session["UnregisterSmartcardID"] = int.Parse(returnvalue[1]);                    
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected void btn_showdetail_Click(object sender, EventArgs e)
        {           
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            string cardnumber = hdf_cardnumber.Value;
            DataTable dt = dbclass.retrieve_membershipcard_detail(cardnumber);

            if (dt.Rows[0] != null)
            {
                txt_cardnumber.Text = dt.Rows[0]["CardNumber"].ToString();
                txt_holdername.Text = dt.Rows[0]["HolderName"].ToString();
                txt_chipnumber.Text = dt.Rows[0]["ChipNumber"].ToString();
                txt_vehiclenumber.Text = dt.Rows[0]["VehicleNumber"].ToString();
                ddl_vehicletype.SelectedValue = dt.Rows[0]["VehicleType"].ToString();
                txt_vehiclebrand.Text = dt.Rows[0]["VehicleBrand"].ToString();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "showModalFormDetail", "showModalFormDetail();", true);                      
        }

        protected void load_vehicle_type()
        {
            VehicleType mobil = new VehicleType();
            mobil.VehicleTypeID = 1;
            mobil.VehicleTypeName = "Mobil";

            VehicleType motor = new VehicleType();
            motor.VehicleTypeID = 2;
            motor.VehicleTypeName = "Motor";

            List<VehicleType> list_vehicle_type = new List<VehicleType>();
            list_vehicle_type.Add(mobil);
            list_vehicle_type.Add(motor);

            ddl_vehicletype.DataValueField = "VehicleTypeID";
            ddl_vehicletype.DataTextField = "VehicleTypeName";
            ddl_vehicletype.DataSource = list_vehicle_type;
            ddl_vehicletype.DataBind();
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("unregistersmartcard.aspx");
        }

        protected void buttom_save_Click(object sender, EventArgs e)
        {
            do_unregister_smartcrad();
        }

        protected void buttom_back_Click(object sender, EventArgs e)
        {
            Response.Redirect("unregistersmartcard.aspx");
        }

        protected void btn_load_history_byunit_Click(object sender, EventArgs e)
        {
            
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            string[] unit = entryheadersmartcard.Unit.Split('-');
            string UnitCode = "";
            string UnitNo = "";
            if (unit.Length == 2)
            {
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }

            UserData userData = (UserData)ViewState["UserData"];
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;
            entryheadersmartcard.retrieve_smartcard_history_byunit(OrgID, SiteID, UnitCode, UnitNo);

            bound_smartcardhistory();

            retrieve_unregister_smartcard(OrgID, SiteID, UnitCode, UnitNo);
            bound_membership_ownership();
        }

        //log boomgate pushdata
        public int insert_pushdata_log(string _sourcePush, string _boom, string _mifare, string _nama, string _alamat, string _jenis, string _cardno, string _status, DateTime? _kadaluarsa, string _psCode, DateTime _issued, string _msgPush, int _flag)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();

            SqlConnection conn = new SqlConnection(constring);
            SqlCommand cmd = new SqlCommand("SP_Save_PushDataLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SourcePush", _sourcePush);
            cmd.Parameters.AddWithValue("@Boom", _boom);
            cmd.Parameters.AddWithValue("@Mifare", _mifare);
            cmd.Parameters.AddWithValue("@Nama", _nama);
            cmd.Parameters.AddWithValue("@Alamat", _alamat);
            cmd.Parameters.AddWithValue("@Jenis", _jenis);
            cmd.Parameters.AddWithValue("@CardNo", _cardno);
            cmd.Parameters.AddWithValue("@Status", _status);
            cmd.Parameters.AddWithValue("@Kadaluarsa", _kadaluarsa);
            cmd.Parameters.AddWithValue("@PsCode", _psCode);
            cmd.Parameters.AddWithValue("@Issued", _issued);
            cmd.Parameters.AddWithValue("@MessagePush", _msgPush);
            cmd.Parameters.AddWithValue("@Flag", _flag);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }
    }
}