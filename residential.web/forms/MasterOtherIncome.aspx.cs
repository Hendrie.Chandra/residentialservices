﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using residential.libs.Models;

namespace residential.web.forms
{
    public partial class MasterOtherIncome : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                ViewState["sortOrder"] = "";

                Department();
                bounddatagrid("", "");
            }
            if (hfHelpItemID.Value == "")
            {
                ddlDepartment.Attributes.Remove("readonly");
                ddlHelpname.Attributes.Remove("readonly");
                txtHelpItemName.Attributes.Remove("readonly");

                ddlDepartment.Enabled = true;
                ddlHelpname.Enabled = true;
            }
            else if (hfHelpItemID.Value != "")
            {
                ddlDepartment.Attributes.Add("readonly", "readonly");
                ddlHelpname.Attributes.Add("readonly", "readonly");
                txtHelpItemName.Attributes.Add("readonly", "readonly");

                ddlDepartment.Enabled = false;
                ddlHelpname.Enabled = false;
            }
        }

        private void Department()
        {
            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = residential.web.Classes.clsMasterOtherIncome.GetDepartment(userData.OrgID, userData.SiteID, userData.UserName);
            ddlDepartment.DataSource = dt;
            ddlDepartment.DataTextField = "DepartmentName";
            ddlDepartment.DataValueField = "DepartmentID";
            ddlDepartment.DataBind();
            ddlDepartment.SelectedIndex = 0;

            cbo_status.DataSource = dt;
            cbo_status.DataTextField = "DepartmentName";
            cbo_status.DataValueField = "DepartmentID";
            cbo_status.DataBind();

            ddlDepartment_SelectedIndexChanged(null, null);
            cbo_status_SelectedIndexChanged(null, null);
        }

        private void bounddatagrid(String sortExp, String sortDir)
        {
            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = residential.web.Classes.clsMasterOtherIncome.ViewMasterHelpPriceAll(txt_search.Text.ToString(), int.Parse(ddlHelpNameList.SelectedValue.ToString()), userData.OrgID, userData.SiteID, int.Parse(cbo_status.SelectedValue.ToString()));

            DataView dv = new DataView(dt);
            if (sortExp != string.Empty)
            {
                dv.Sort = string.Format("{0} {1}", sortExp, sortDir);
            }
            
            gvw_data.DataSource = null;
            gvw_data.DataSource = dv;
            gvw_data.DataBind();
        }

        public string sortOrder
        {
            get
            {
                if (ViewState["sortOrder"].ToString() == "desc")
                {
                    ViewState["sortOrder"] = "asc";
                }
                else
                {
                    ViewState["sortOrder"] = "desc";
                }

                return ViewState["sortOrder"].ToString();
            }
            set
            {
                ViewState["sortOrder"] = value;
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRow row = ((DataRowView)e.Row.DataItem).Row;
                string HelpItemID = row[0].ToString();
                string HelpID = row[1].ToString();
                string HelpItemName = row[3].ToString();
                string HelpItemPrice = string.Format("{0:n2}", row[4]);
                string isActive = row[7].ToString();
                string DepartmentID = cbo_status.SelectedValue.ToString();

                UserData userData = (UserData)ViewState["UserData"];

                e.Row.Attributes.Add("onclick", "Confirm('" + HelpItemID + "', '" + HelpID + "', '" + HelpItemName +  "', '" + HelpItemPrice + "', '" + isActive + "', '" + DepartmentID + "');");
            }
        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid("","");
        }

        protected void cbo_status_SelectedIndexChanged(object sender, EventArgs e)
        {
            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = residential.web.Classes.clsMasterOtherIncome.GetHelpName(userData.OrgID, userData.SiteID, int.Parse(cbo_status.SelectedValue.ToString()));

            ddlHelpNameList.DataSource = null;
            ddlHelpNameList.DataSource = dt;
            ddlHelpNameList.DataTextField = "HelpName";
            ddlHelpNameList.DataValueField = "HelpID";
            ddlHelpNameList.DataBind();
            ddlHelpNameList.Items.Insert(0, new ListItem("[ All Helpname ]", "0"));
            ddlHelpNameList.SelectedIndex = 0;

            ddlHelpNameList_SelectedIndexChanged(null, null);
        }

        protected void gvw_data_Sorting(object sender, GridViewSortEventArgs e)
        {
            bounddatagrid(e.SortExpression, sortOrder);
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid("", "");
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid("", "");
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_edit")
            {
                bounddatagrid("", "");
            }
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlDepartment.SelectedIndex > -1)
            {
                UserData userData = (UserData)ViewState["UserData"];

                DataTable dt = residential.web.Classes.clsMasterOtherIncome.GetHelpName(userData.OrgID, userData.SiteID, int.Parse(ddlDepartment.SelectedValue.ToString()));

                ddlHelpname.DataSource = null;
                ddlHelpname.DataSource = dt;
                ddlHelpname.DataTextField = "HelpName";
                ddlHelpname.DataValueField = "HelpID";
                ddlHelpname.DataBind();
            }
        }

        protected void ddlHelpNameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlHelpNameList.SelectedIndex > -1)
            {
                bounddatagrid("", "");
            }
        }

        protected void btn_assigning_Click(object sender, EventArgs e)
        {
            if (txtPrice.Text.Length < 1 )
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('Please Fill Price');", true);
            }
            else if (ddlHelpname.SelectedIndex == -1)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Failed('Please Select Helpname');", true);
            }
            else
            {
                UserData userData = (UserData)ViewState["UserData"];

                string Result = "";

                if (hfHelpItemID.Value == "")
                {
                    Result = residential.web.Classes.clsMasterOtherIncome.InsertData(userData.OrgID, userData.SiteID, 
                        int.Parse(ddlHelpname.SelectedValue.ToString()), txtHelpItemName.Text.ToString(), 
                        decimal.Parse(txtPrice.Text.ToString()), int.Parse(ddlActive.SelectedValue.ToString()), userData.UserName);
                }
                else if (hfHelpItemID.Value != "")
                {
                    Result = residential.web.Classes.clsMasterOtherIncome.UpdateData(userData.OrgID, userData.SiteID,
                        int.Parse(hfHelpItemID.Value.ToString()), decimal.Parse(txtPrice.Text.ToString()),
                        int.Parse(ddlActive.SelectedValue.ToString()), userData.UserName);
                }

                bounddatagrid("", "");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "dialog", "Close('" + Result + "');", true);
            }
        }

    }
}