﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listenvironment.aspx.cs" Inherits="residential.web.forms.listenvironment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="buttoncontainer">
                <asp:Button CssClass="button" ID="btn_add" runat="server" Text="ADD" 
                    Width="100px" onclick="btn_add_Click" />
            </div>            
            <div class="formentrycontent">
                <div class="formentrycell2">
                    <asp:Label ID="Label1" runat="server" Text="Case Status :" CssClass="labelfield"></asp:Label>
                    <asp:DropDownList ID="cbo_status" runat="server" CssClass="ComboBoxWindowsStyle" AutoPostBack="True"
                        OnSelectedIndexChanged="cbo_status_SelectedIndexChanged" Width="167px">
                    </asp:DropDownList>
                </div>
                <div class="formentrycell2" style="text-align:right">
                    <asp:TextBox ID="txt_search" CssClass="textbox_entry_left textboxcell2" 
                        runat="server" ontextchanged="txt_search_TextChanged" AutoPostBack="true"></asp:TextBox>
                    <asp:TextBoxWatermarkExtender ID="TWE_search" runat="server" TargetControlID="txt_search" WatermarkText="case number, unit" WatermarkCssClass="textbox-watermark" ></asp:TextBoxWatermarkExtender>
                    <asp:Button ID="btn_search" runat="server" onclick="btn_search_Click" Text="Search" />
                </div>
            </div>            
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" 
                CssClass="gridview" AllowPaging="True" BorderWidth="0px" 
                Width="100%" onrowdatabound="gvw_data_RowDataBound" 
                ShowHeaderWhenEmpty="True" onrowcommand="gvw_data_RowCommand" 
                DataKeyNames="CaseNumber" AllowSorting="True" onsorting="gvw_data_Sorting" 
                onpageindexchanging="gvw_data_PageIndexChanging">
                <Columns>
                    <asp:TemplateField HeaderText="NO">                        
                        <HeaderStyle CssClass="gridview_header" Width="30px" />
                        <ItemStyle CssClass="gridview_item_center" />
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="Label"></asp:Label>
                        </ItemTemplate>                
                    </asp:TemplateField>                   
                    <%--<asp:TemplateField HeaderText="PRINT">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                        <ItemTemplate>
                            <asp:LinkButton ID="lnk_print" runat="server" CommandName="cmd_edit" 
                                CssClass="gridview_link">print</asp:LinkButton>
                        </ItemTemplate>                        
                    </asp:TemplateField>--%>
                    <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber">
                        <HeaderStyle CssClass="gridview_header"  Width="125px" />
                        <ItemStyle CssClass="gridview_item_left" Font-Bold="true" />                                      
                    </asp:BoundField>
                    <asp:BoundField HeaderText="UNIT" DataField="Unit" SortExpression="Unit">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CATEGORY" DataField="HelpName" SortExpression="HelpName">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="CREATED" DataField="RequestDate" SortExpression="RequestDate"
                        DataFormatString="{0:d MMM yyyy}">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="OVERDUE" DataField="OverdueDate" SortExpression="OverdueDate"
                        DataFormatString="{0:d MMM yyyy}">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" SortExpression="CaseStatusName">
                        <HeaderStyle CssClass="gridview_header" />
                        <ItemStyle CssClass="gridview_item_left" />
                    </asp:BoundField>
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>                        
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
