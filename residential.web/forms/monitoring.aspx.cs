﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using System.Configuration;
using System.Data;


namespace residential.web.forms
{
    public partial class monitoring : System.Web.UI.Page
    {
        String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                show_cbo_deptlist();
                BindTasklist();
                loadchart1();
                loadchart2();
            }
            Response.AppendHeader("refresh", "60");
        }

        private void BindTasklist()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            string DeptName = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            if (cbo_deptlist.SelectedItem.Text != "[ All Dept ]")
            {
                DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
            }
            UserData userData = (UserData)ViewState["UserData"];

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.closeSLA(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName);

            this.gvw_sla.DataSource = dt;
            this.gvw_sla.DataBind();
        }

        protected void picker_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text) && !string.IsNullOrEmpty(cbo_deptlist.Text))
                {
                    ViewState["UserData"] = new UserData(User.Identity.Name);
                    DateTime StartDate = Convert.ToDateTime(TxtStartDate.Text);
                    DateTime EndDate = Convert.ToDateTime(TxtEndDate.Text);
                    string DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
                }

                DateTime SD = Convert.ToDateTime(TxtStartDate.Text);
                if (SD > DateTime.Now)
                {
                    Response.Write("<script>alert('Created Date FROM cannot  be greater than CURRENT date')</script>");
                }

                DateTime ED = Convert.ToDateTime(TxtEndDate.Text);
                if (ED > DateTime.Now)
                {
                    Response.Write("<script>alert('Created Date TO cannot  be greater than CURRENT date')</script>");
                }

                if (ED < SD)
                {
                    Response.Write("<script>alert('Created Date TO cannot  be less than Created Date FROM')</script>");
                }

                UserData userData = (UserData)ViewState["UserData"];
                //show_cbo_deptlist();
                BindTasklist();
                loadchart1();
                loadchart2();
            }
            catch (Exception err)
            {
                Response.Write("<script>alert('" + err.Message + "')</script>");
            }
        }

        private void show_cbo_deptlist()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_deptlist();
            cbo_deptlist.DataSource = dt;
            cbo_deptlist.DataTextField = "DepartmentName";
            cbo_deptlist.DataValueField = "DepartmentName";
            cbo_deptlist.DataBind();
            cbo_deptlist.Items.Insert(0, new ListItem("[ All Dept ]", "0"));
            cbo_deptlist.SelectedIndex = 0;
        }

        protected void gvw_sla_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime CloseDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "CloseDate").ToString());
                Int32 MeetSLA = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "MeetSLA").ToString());
                Int32 OverSLA = Int32.Parse(DataBinder.Eval(e.Row.DataItem, "OverSLA").ToString());
            }
        }
        //DataFormatString="{0:d MMMM yyyy}"

        //onchange
        //protected void cbo_deptlist_TextChanged(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text) && !string.IsNullOrEmpty(cbo_deptlist.Text))
        //    {
        //        DateTime StartDate = Convert.ToDateTime(TxtStartDate.Text);
        //        DateTime EndDate = Convert.ToDateTime(TxtEndDate.Text);
        //        string DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);

        //        BindTasklist();
        //        loadchart1();
        //        loadchart2();
        //    }
        //}

//Chart1
        protected void loadchart1()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            string DeptName = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            if (cbo_deptlist.SelectedItem.Text != "[ All Dept ]")
            {
                DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
            }
            UserData userData = (UserData)ViewState["UserData"];

            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.closeSLA(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName);

            string[] chartdata = new string[2];
            string tempchartdata = "";
            string tempchartlabel = "";

            if (dt.Rows.Count > 0)
            {
                int chartdatalength = dt.Rows.Count;
                int i = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (i <= (chartdatalength - 1))
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CloseDate"].ToString() + '"' + ",";
                        tempchartdata = tempchartdata + dr["MeetSLA"].ToString() + ",";
                    }
                    else
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CloseDate"].ToString() + '"';
                        tempchartdata = tempchartdata + dr["MeetSLA"].ToString();
                    }
                    i++;
                }
            }
            chartdata[0] = "[" + tempchartdata + "]";
            chartdata[1] = "[" + tempchartlabel + "]";

            chart1data.Text = tempchartdata;
            chart1label.Text = tempchartlabel;
        }

//chart2
        protected void loadchart2()
        {
            DateTime? StartDate = null;
            DateTime? EndDate = null;
            string DeptName = null;

            if (!string.IsNullOrEmpty(TxtStartDate.Text) && !string.IsNullOrEmpty(TxtEndDate.Text))
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                StartDate = Convert.ToDateTime(TxtStartDate.Text);
                EndDate = Convert.ToDateTime(TxtEndDate.Text);
            }
            if (cbo_deptlist.SelectedItem.Text != "[ All Dept ]")
            {
                DeptName = Convert.ToString(cbo_deptlist.SelectedItem.Text);
            }
            UserData userData = (UserData)ViewState["UserData"];

            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.closeSLA(userData.OrgID, userData.SiteID, StartDate, EndDate, DeptName);

            string[] chartdata = new string[2];
            string tempchartdata = "";
            string tempchartlabel = "";

            if (dt.Rows.Count > 0)
            {
                int chartdatalength = dt.Rows.Count;
                int i = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (i <= (chartdatalength - 1))
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CloseDate"].ToString() + '"' + ",";
                        tempchartdata = tempchartdata + dr["OverSLA"].ToString() + ",";
                    }
                    else
                    {
                        tempchartlabel = tempchartlabel + '"' + dr["CloseDate"].ToString() + '"';
                        tempchartdata = tempchartdata + dr["OverSLA"].ToString();
                    }
                    i++;
                }
            }
            chartdata[0] = "[" + tempchartdata + "]";
            chartdata[1] = "[" + tempchartlabel + "]";

            chart2data.Text = tempchartdata;
            chart2label.Text = tempchartlabel;
        }
    }
}