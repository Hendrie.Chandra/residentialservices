﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;

using residential.libs;

namespace residential.web.forms
{
    public partial class listrole : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bounddatagrid();                                
            }
        }

        private void bounddatagrid()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_role();

            gvw_data.DataSource = dt;
            gvw_data.DataBind();        
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String RoleName = DataBinder.Eval(e.Row.DataItem, "RoleName").ToString();
                String RoleID = DataBinder.Eval(e.Row.DataItem, "RoleID").ToString();
                HyperLink lnk_role = (HyperLink)e.Row.FindControl("lnk_role");
                lnk_role.NavigateUrl = "~/forms/entryrole?RoleID=" + RoleID;
                lnk_role.Text = RoleName;           
            }
        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }

        protected void btn_add_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/forms/entryrole.aspx");
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_edit")
            {
                bounddatagrid();
            }
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {            
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);              

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 RoleID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());
                    if (dbclass.delete_role(RoleID) != 0)
                    {
                        alert_danger_text.Text = "Cannot Delete Roles!";
                        alert_danger.Visible = true;
                    }
                    else
                        alert_danger.Visible = false;
                }
            }
            bounddatagrid();
        }

        protected void btn_closeerror_Click(object sender, EventArgs e)
        {
            alert_danger.Visible = false;
        }
    }
}