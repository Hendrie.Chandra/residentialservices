﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="listtr.aspx.cs" Inherits="residential.web.forms.trmenu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="innerlinkcontent">
        <div class="innerlinkcell">
            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icon-idbaru.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="~/forms/ktp_kk.aspx" 
                CssClass="contentlink">Permohonan KTP/KK</asp:HyperLink>
            <br />
            Permohonan pembuatan KTP/KK baru
        </div>
        <div class="innerlinkcell">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon-perpanjangid.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/forms/rektp_kk.aspx"
                CssClass="contentlink">Perpanjangan KTP</asp:HyperLink>
            <br />
            Permohonan perpanjangan KTP
        </div>
    </div>
    <div class="innerlinkcontent">
        <div class="innerlinkcell">
            <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icon-rentcar.png" CssClass="innerlinkicon" />
            <br />
            <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/forms/peminjamankendaraan.aspx" 
                CssClass="contentlink">Peminjaman Kendaraan</asp:HyperLink>
            <br />
            Permohonan peminjaman kendaraan
        </div>        
    </div>    
</asp:Content>
