﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/main.master" CodeBehind="EntryResidentCard.aspx.cs" Inherits="residential.web.forms.EntryResidentCard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
        <ProgressTemplate>
            <div id="overlay">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/loader_white.GIF" Width="50" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <link href="../content/bootstrap-dialog.min.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            //$("#ContentPlaceHolder1_ddlActive").change(function () {
            //    //alert("change");
            //    var deactivate = $("#ContentPlaceHolder1_ddlActive").val();
            //    if (deactivate == 0) {
            //        $("#form-reason").css('display', 'block')
            //    } else {
            //        $("#form-reason").css('display', 'none')
            //    }
            //})
        })

        function showLoader() {
            if (form1.checkValidity()) {
                $('#<%= UpdateProgress1.ClientID %>').css('display', '');
                return true;
            } else {
                return false;
            }
        }
        function pageLoad(sender, args) {
            //var date = new Date();
            //var currentMonth = 4;
            //var currentDate = 4;
            //var currentYear = date.getFullYear();
            //$("[id$='txtExpiredDate']").datepicker({
            //    dateFormat: 'dd-M-yy',
            //    changeMonth: true,
            //    changeYear: true
            //});
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
                $("[id$='txtExpiredDate']").datepicker({
                    format: 'dd MM yyyy'
                });

                $("[id$='fu1']").change(function (event) {
                    var uploadFile = $("[id$='fu1']").val();
                    if (uploadFile != null) {
                        uploadImagePrev(event);
                    }                    
                });
            });
        }
   
        var uploadImagePrev = function (event) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function () {
               var dataURL = reader.result;
               var output = document.getElementById([id$='largeImg']);
               output.src = dataURL;
               $("#hdfBase64Photo").val(dataURL);
            };
            reader.readAsDataURL(input.files[0]);
        };
 

        function Confirm(ResidentCardID, ResidentDetailID, Name, ExpiredDate, CardNo, SNCard, DesignTypeCode, img, isPrinted, Remarks, isActive, DisableExpired) {
            $('#modalAssign').modal('show');
            $("[id$='hfResidentCardID']").val(ResidentCardID);
            //$("[id$='txtName']").val(Name);
            $("[id$='ddl_name']").val(ResidentDetailID);
            $("[id$='ddlActive']").val(isActive);
            $("[id$='txtExpiredDate']").val(ExpiredDate);
            $("[id$='txtCardNo']").val(CardNo);
            $("[id$='txtSNcard']").val(SNCard);
            $("[id$='ddlDesignType']").val(DesignTypeCode);
            $("[id$='ddlisPrinted']").val(isPrinted);
            $("[id$='txtRemarks']").val(Remarks);
            $("[id$='fu1']").val("");

            var src = img;
            document.getElementById('largeImg').src = src;

            if (DisableExpired == 0) {
                document.getElementById("<%=ckDisable.ClientID%>").checked = false;
            }
            else {
                document.getElementById("<%=ckDisable.ClientID%>").checked = true;
            }
            return true;
        }
        function Failed(text) {
            alert(text);
            $('#modalAssign').modal('show');
        }
        function ShowEmpty() {
            $('#modalAssign').modal('show');

            var Active = document.getElementById("<%=ddlActive.ClientID %>");
            Active.selectedIndex = 0;

            var Printed = document.getElementById("<%=ddlisPrinted.ClientID %>");
            Printed.selectedIndex = 0;

            var DesignType = document.getElementById("<%=ddlDesignType.ClientID %>");
            DesignType.selectedIndex = 0;

            $("[id$='hfResidentCardID']").val('');
            $("[id$='txtName']").val('');
            $("[id$='txtExpiredDate']").val('');
            $("[id$='txtCardNo']").val('');
            $("[id$='txtSNcard']").val('');
            $("[id$='txtRemarks']").val('');

            document.getElementById("<%=ckDisable.ClientID%>").checked = false;
            document.getElementById('largeImg').src = null;

            return true;
        }
        function show_message_dialog(issuccess, message) {
            if (issuccess == "0") {
                //failled
                BootstrapDialog.alert({
                    title: 'Warning !',
                    message: message,
                    type: BootstrapDialog.TYPE_DANGER,
                    callback: function (result) {
                        if (result)
                            window.location.href = window.location.href
                        else
                            window.location.href = window.location.href
                    }
                });
            } else if (issuccess == "1") {
                //success
                BootstrapDialog.alert({
                    title: 'Message',
                    message: message,
                    type: BootstrapDialog.TYPE_PRIMARY,
                    callback: function (result) {
                        if (result)
                            window.location.href = window.location.href
                        else
                            window.location.href = window.location.href

                    }
                });
            }
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h3>Form Resident Card Unit : <b><asp:Label runat="server" ID="lblTitle" /></b></h3>
                <asp:HiddenField ID="hfRHI" runat="server" />
            </div>

            <div id="errID" class="alert alert-warning alert-dismissible" role="alert" runat="server" visible="false">
                <asp:Label ID="lbl_error" runat="server" Visible="false"></asp:Label>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Warning!</strong> <%=lbl_error.Text%>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Owner PsCode / Name</label>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtOwnerPsCode"  CssClass="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Tenant PsCode / Name</label>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtTenantPsCode"  CssClass="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-3 control-label">Is Active</label>
                        <div class="col-sm-6">
                            <asp:TextBox runat="server" ID="txtIsActive"  CssClass="form-control"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <div class="col-sm-6">
                            <button type="button" class="btn btn-primary" onclick="ShowEmpty();" data-toggle="tooltip" data-placement="top" title="New Resident Card">New</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-horizontal">
                <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False"
                CssClass="table table-bordered table-hover" AllowPaging="True"
                ShowHeaderWhenEmpty="True" OnRowDataBound="gvw_data_RowDataBound"
                OnPageIndexChanging="gvw_data_PageIndexChanging" 
                DataKeyNames="ResidentCardID"
                 HeaderStyle-HorizontalAlign="Center">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="ResidentCardID" DataField="ResidentCardID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="NAME" DataField="Name" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="CARD NO" DataField="CardNo" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="SN CARD" DataField="SNCard" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="REMARKS" DataField="Remarks" SortExpression="Remarks" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="EXPIRED" DataField="ExpiredDate" SortExpression="ExpiredDate" DataFormatString="{0:d MMMM yyyy}"  ItemStyle-HorizontalAlign="Right"/>
                    <asp:BoundField HeaderText="DESIGN TYPE" DataField="DesignTypeDesc" SortExpression="DesignTypeDesc"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField HeaderText="DesignTypeCode" DataField="DesignTypeCode" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="PRINTED" DataField="isPrinted" SortExpression="isPrinted"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField HeaderText="PhotoID" DataField="PhotoID" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="DisableExpired" DataField="DisableExpired" ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" />
                    <asp:BoundField HeaderText="ACTIVE" DataField="isActive" SortExpression="isActive"  ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField HeaderText="LAST MODIFY DATE" DataField="ModifiedDate" SortExpression="ModifiedDate" DataFormatString="{0:d MMMM yyyy}"  ItemStyle-HorizontalAlign="Right"/>
                    <asp:BoundField HeaderText="LAST MODIFY" DataField="ModifiedBy" SortExpression="ModifiedBy"  ItemStyle-HorizontalAlign="Center"/>                    
                    <asp:TemplateField HeaderText="Photo" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle CssClass="gridview-center" />
                        <ItemTemplate>
                            <%--<asp:ImageButton ToolTip="Click to Enlarge" ID="Images" Width="20px" Height="20px" runat="server" ImageUrl='<%# Bind("PhotoID") %>' />--%>
                                <asp:Image ID="Images" Width="30px" Height="35px" runat="server" ImageUrl='<%# Bind("PhotoID") %>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center">
                        <HeaderStyle CssClass="gridview-center" Width="150px" />
                        <ItemTemplate>                            
                            <asp:HyperLink runat="server" ID="hpl_edit" CssClass="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Resident Card"><span class="glyphicon glyphicon-pencil"></span></asp:HyperLink>
                            <asp:HyperLink runat="server" ID="hpl_print" CssClass="btn btn-info" data-toggle="tooltip" data-placement="top" title="Print Resident Card" NavigateUrl='<%# Eval("ResidentCardID","~/forms/printresidentcard.aspx?ResidentCardID={0}") %>' Target="_blank"><span class="glyphicon glyphicon-print"></span></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="modal fade" id="modalAssign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server"><ContentTemplate>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Resident Card</h4>
                    <asp:HiddenField ID="hfResidentCardID" runat="server" />
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
<%--                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Name</label>
                            <div class="col-sm-9">
                                <asp:TextBox runat="server" ID="txtName" MaxLength="300" CssClass="form-control"  />
                            </div>
                        </div>--%>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Name</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddl_name" runat="server" style="cursor:pointer" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Card No</label>
                            <div class="col-sm-9">
                                <asp:TextBox runat="server" ID="txtCardNo" MaxLength="300" CssClass="form-control"  />
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> SN Card</label>
                            <div class="col-sm-9">
                                <asp:TextBox runat="server" ID="txtSNcard" MaxLength="300" CssClass="form-control"  />
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Remarks</label>
                            <div class="col-sm-9">
                                <asp:TextBox runat="server" ID="txtRemarks" MaxLength="300" CssClass="form-control"  />
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Expired Date</label>
                            <div class="col-sm-9">
                                <div>
                                <asp:TextBox runat="server" style="cursor:pointer"  ID="txtExpiredDate"  data-toggle="tooltip" data-placement="top" ToolTip="Format Day/Month/Year" CssClass="selectpicker form-control"/>
                                <asp:CheckBox runat="server" ID="ckDisable" Text=" Disable" style="cursor:pointer"/>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Design Type</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlDesignType" runat="server" style="cursor:pointer" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Printed</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlisPrinted" runat="server" style="cursor:pointer" CssClass="form-control">
                                    <asp:ListItem Text="True" Value="1" />
                                    <asp:ListItem Text="False" Value="0" /> 
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><span style="color:Red">*</span> Active</label>
                            <div class="col-sm-9">
                                <asp:DropDownList ID="ddlActive" runat="server" style="cursor:pointer" CssClass="form-control">
                                    <asp:ListItem Text="True" Value="1" />
                                    <asp:ListItem Text="False" Value="0" /> 
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
<%--                    <div class="form-horizontal" id="form-reason" style="display:none">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Deactivate Reason</label>
                            <div class="col-sm-9">
                                <asp:TextBox runat="server" ID="txtReason" MaxLength="300" CssClass="form-control"  />
                            </div>
                        </div>
                    </div>--%>
                    <div class="form-horizontal">
                         <div class="form-group">
                            <label class="col-sm-3 control-label"></label>
                            <img class="col-sm-9" id="largeImg" style="width:200px;height:200px" src="#" />
                             <asp:HiddenField ID="hdfBase64Photo" runat="server" ClientIDMode="Static" />
                        </div>
                    </div>
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"> Photo ID</label>
                            <div class="col-sm-9" style="vertical-align:middle">
                                <asp:FileUpload ID="fu1" CssClass="form-control selectpicker" style="cursor:pointer" runat="server" />
                            </div>
                        </div>
                    </div>
                </div>
                </ContentTemplate></asp:UpdatePanel>
                <div class="modal-footer">
                    <asp:Button ID="btn_save" runat="server" Text="SUBMIT" 
                        CssClass="btn btn-primary" UseSubmitBehavior="false" data-dismiss="modal" OnClientClick="showLoader();"
                        onclick="btn_save_Click" />
                    <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="btn btn-default" data-dismiss="modal" />
                </div>
            </div>
        </div>  
    </div>
    <script src="../scripts/bootstrap-dialog.min.js"></script>
</asp:Content>
