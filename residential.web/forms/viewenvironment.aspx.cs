﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;
using System.IO;
using residential.libs.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace residential.web.forms
{
    public partial class viewenvironment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                startandenddate.Visible = false;
                show_case(Request.QueryString["CaseNumber"]);
                bounddatagrid_tickethistory();
                show_cbo_assign();
                show_cbo_satisfaction();
                satisfactionblock.Visible = false;
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();
            }
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_Environment(_casenumber);

            ViewState["CRMCaseID"] = dt.Rows[0]["CRMCaseID"].ToString();
            viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
            viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
            viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
            viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
            viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
            viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
            viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
            viewheader.Description = dt.Rows[0]["Description"].ToString();

            lbl_servicerequest.Text = dt.Rows[0]["HelpName"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["StartDate"].ToString()) && !String.IsNullOrEmpty(dt.Rows[0]["EndDate"].ToString()))
            {
                DateTime StartDate = (DateTime)dt.Rows[0]["StartDate"];
                DateTime EndDate = (DateTime)dt.Rows[0]["EndDate"];
                lbl_startdate.Text = StartDate.ToString("dd MMMM yyyy");
                lbl_enddate.Text = EndDate.ToString("dd MMMM yyyy");
                startandenddate.Visible = true;
            }

            Decimal price = Decimal.Parse(dt.Rows[0]["Price"].ToString());
            lbl_quantityprice.Text = price.ToString("N0");

            Decimal discount = Decimal.Parse(dt.Rows[0]["TotalDiscount"].ToString());
            lbl_disc.Text = discount.ToString("N0");

            Decimal subtotal = Decimal.Parse(dt.Rows[0]["TotalAmount"].ToString());
            lbl_subtotal.Text = subtotal.ToString("N0");

            lbl_discount.Text = discount.ToString("N0");

            Decimal total = Decimal.Parse(dt.Rows[0]["TotalAmountAfterDiscount"].ToString());
            lbl_total.Text = total.ToString("N0");

            String CaseStatus = dt.Rows[0]["CaseStatusID"].ToString();
            Int32 RoleID = userData.RoleID;

            show_cbo_casestatus(CaseStatus);
            if (CaseStatus == "2" && RoleID == 2)
            {
                updatecase.Visible = false;
                updatecaseform.Visible = false;
                assigncase.Visible = true;
            }
            else
            {
                //updatecase.Visible = true;
                //updatecaseform.Visible = true;
                assigncase.Visible = false;
            }
        }

        private void show_cbo_casestatus(String _selected)
        {
            UserData userData = (UserData)ViewState["UserData"];

            Int32 RoleID = userData.RoleID;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_casestatus(RoleID, Int32.Parse(_selected));

            if (dt.Rows.Count > 0)
            {
                cbo_casestatus.DataSource = dt;
                cbo_casestatus.DataTextField = "CaseStatusName";
                cbo_casestatus.DataValueField = "AvailableCaseStatusID";
                cbo_casestatus.DataBind();
                cbo_casestatus.SelectedValue = _selected;
            }
            else
            {
                updatecase.Visible = false;
                updatecaseform.Visible = false;
            }
        }

        private void show_cbo_assign()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];            

            DataTable dt = dbclass.retrieve_pic(userData.OrgID, userData.SiteID, userData.DepartmentID);
            cbo_assign.DataSource = dt;
            cbo_assign.DataTextField = "FullName";
            cbo_assign.DataValueField = "UserID";
            cbo_assign.DataBind();
        }

        private void show_cbo_satisfaction()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_satisfaction();
            cbo_satisfaction.DataSource = dt;
            cbo_satisfaction.DataTextField = "SatisfactionName";
            cbo_satisfaction.DataValueField = "SatisfactionID";
            cbo_satisfaction.DataBind();
            cbo_satisfaction.SelectedIndex = 0;
        }

        //private void update_to_crm(String _casenumber, Int32 _crmcasestatusid, String _remarks, String _updatedby, String _workedby, Int32 _SatisfactionID)
        //{
        //    String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
        //    String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
        //    String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
        //    String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
        //    String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
        //    CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

        //    UpdateCaseHelpDeskStatusRequest request = new UpdateCaseHelpDeskStatusRequest();
        //    request.CRMCaseNumber = _casenumber;
        //    request.CRMCaseStatusID = _crmcasestatusid;
        //    request.Remarks = _remarks;
        //    request.UpdateBy = _updatedby;
        //    request.UpdateDate = DateTime.Now;
        //    request.WorkedBy = _workedby;
        //    request.SatisfactionID = _SatisfactionID;
        //    UpdateCaseHelpDeskStatusResponse response = crmclass.UpdateCaseHelpDeskStatus(request);
        //}

        private void bounddatagrid_tickethistory()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_tickethistory(Request.QueryString["CaseNumber"]);

            gvw_data.DataSource = dt;
            gvw_data.DataBind();
        }

        Int32 save_tickethistory()
        {
            UserData userData = (UserData)ViewState["UserData"];

            String CaseNumber = Request.QueryString["CaseNumber"];
            Int32 CaseStatusID = Int32.Parse(cbo_casestatus.SelectedValue);
            String FullName = userData.FullName;
            String Remarks = txt_remarks.Text;
            String SavedBy = userData.UserName;

            String SavedFileName = null;
            if (fu_uploadattachment.HasFile)
            {
                //String FilePath = Server.MapPath("~/caseattachment/" + CaseNumber + "/");
                //if (!Directory.Exists(FilePath))
                //    Directory.CreateDirectory(FilePath);

                //String FullFileName = fu_uploadattachment.FileName;
                //String FileName = Path.GetFileNameWithoutExtension(FullFileName);
                //String FileExtension = Path.GetExtension(FullFileName);
                //int counter = 1;
                //if (File.Exists(FilePath + FullFileName))
                //{
                //    while (File.Exists(FilePath + FileName + counter.ToString() + FileExtension))
                //    {
                //        counter++;
                //    }
                //    SavedFileName = FileName + counter.ToString() + FileExtension;
                //}
                //else
                //    SavedFileName = FullFileName;

                //fu_uploadattachment.SaveAs(FilePath + SavedFileName);

                //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
                //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
                //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
                //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
                //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();

                //String SharePointSite = ConfigurationManager.AppSettings["SharePointSite"].ToString();
                //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);
                //IOrganizationService crmservice = crmclass.crmservice_conn();
                //SPClass spclass = new SPClass(Domain, UserName, Password, SharePointSite, crmservice);

                //Entity mycase = crmservice.Retrieve("incident", new Guid(ViewState["CRMCaseID"].ToString()), new ColumnSet(true));

                //string fileBasePath = fu_uploadattachment.PostedFile.FileName;
                //string fileName = fu_uploadattachment.FileName;
                //string UploadedFile = Server.MapPath("~/TempUploadedFile/" + fileName);
                //fu_uploadattachment.SaveAs(UploadedFile);
                //byte[] byteData = File.ReadAllBytes(UploadedFile);
                //File.Delete(UploadedFile);
                //spclass.sendfiletosharepoint(mycase, "ticketnumber", byteData, fileName);
            }

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = dbclass.save_tickethistory(CaseNumber, CaseStatusID, FullName, Remarks, SavedBy, SavedFileName);
            /*update_to_crm(CaseNumber, CaseStatusID, Remarks, FullName, null, Int32.Parse(cbo_satisfaction.SelectedValue));*/
            if (CaseStatusID == 7) dbclass.save_satisfaction(CaseNumber, Int32.Parse(cbo_satisfaction.SelectedValue));

            return returnvalue;
        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            Response.Redirect(ViewState["PreviousPageUrl"].ToString());
        }

        protected void btn_update_Click(object sender, EventArgs e)
        {            
            save_tickethistory();
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);

            Response.Redirect(ViewState["PreviousPageUrl"].ToString());
        }

        protected void btn_assign_Click(object sender, EventArgs e)
        {
            if (cbo_assign.SelectedItem == null)
            {
                alert_danger_text.Text = "Please select person te be assigned";
                alert_danger.Visible = true;
            }
            else
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                Int32 UserID = Int32.Parse(cbo_assign.SelectedValue);
                String SavedBy = userData.UserName;
                String FullName = userData.FullName;
                String CaseNumber = Request.QueryString["CaseNumber"];
                String Remarks = "Assign to " + cbo_assign.SelectedItem.Text;
                dbclass.assign_case(UserID, CaseNumber, SavedBy);
                /*update_to_crm(CaseNumber, 4, Remarks, FullName, cbo_assign.SelectedItem.Text, Int32.Parse(cbo_satisfaction.SelectedValue));*/

                Response.Redirect(ViewState["PreviousPageUrl"].ToString());
            }
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DateTime ModifiedDate = DateTime.Parse(DataBinder.Eval(e.Row.DataItem, "ModifiedDate").ToString());
                System.Web.UI.WebControls.Label lbldate = (System.Web.UI.WebControls.Label)e.Row.FindControl("lbl_modifieddate");
                lbldate.Text = String.Format("{0:dd MMM yyyy HH:mm}", ModifiedDate.ToLocalTime());
            }
        }

        //protected void cbo_casestatus_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (cbo_casestatus.SelectedValue == "7")
        //        satisfactionblock.Visible = true;
        //    else
        //        satisfactionblock.Visible = false;
        //}
    }
}