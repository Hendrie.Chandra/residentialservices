﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using residential.libs;
using System.Xml.Serialization;
using System.Text;

namespace residential.web.forms
{
    public partial class listuser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);

                bounddatagrid();                
            }
        }

        private void bounddatagrid()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            
            DataTable dt = new DataTable();
            if (String.IsNullOrWhiteSpace(txt_search.Text))
                dt = dbclass.retrieve_user(userData.OrgID, userData.SiteID);
            else
                dt = dbclass.retrieve_user(userData.OrgID, userData.SiteID, txt_search.Text);

            gvw_data.DataSource = dt;
            gvw_data.DataBind();            
        }

        protected void gvw_data_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                String FullName = DataBinder.Eval(e.Row.DataItem, "FullName").ToString();                
                String UserID = DataBinder.Eval(e.Row.DataItem, "UserID").ToString();
                HyperLink lnk_user = (HyperLink)e.Row.FindControl("lnk_user");
                lnk_user.Text = FullName;                
                lnk_user.NavigateUrl = "entryuser.aspx?UserID=" + UserID;
            }
        }

        protected void gvw_data_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "cmd_edit")
            {
                bounddatagrid();
            }
        }

        protected void chk_selectall_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chk_selectall = (CheckBox)gvw_data.HeaderRow.FindControl("chk_selectall");
            if (chk_selectall != null)
            {
                if (chk_selectall.Checked)
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow row in gvw_data.Rows)
                    {
                        CheckBox chkselect = (CheckBox)row.FindControl("chk_select");
                        chkselect.Checked = false;
                    }
                }
            }
        }       

        protected void btn_add_Click(object sender, EventArgs e)
        {
            Response.Redirect("entryuser.aspx");
        }

        protected void btn_delete_Click(object sender, EventArgs e)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);            

            foreach (GridViewRow gvr in gvw_data.Rows)
            {
                CheckBox chkselect = (CheckBox)gvr.FindControl("chk_select");
                if (chkselect.Checked == true)
                {
                    Int32 UserID = Int32.Parse(gvw_data.DataKeys[gvr.RowIndex].Value.ToString());
                    DataTable dtuser = dbclass.retrieve_user(UserID);
                    dbclass.delete_user(UserID);

                    DataTable dtusers = dbclass.retrieve_user(dtuser.Rows[0]["UserName"].ToString());
                    if (dtusers.Rows.Count > 0 && dtuser.Rows[0]["DefaultSiteID"].ToString() == dtuser.Rows[0]["SiteID"].ToString())
                    {
                        dbclass.save_defaultusersite(0, int.Parse(dtusers.Rows[0]["SiteID"].ToString()), dtusers.Rows[0]["UserName"].ToString());
                    }                    
                }
            }
            bounddatagrid();
        }

        protected void gvw_data_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvw_data.PageIndex = e.NewPageIndex;
            bounddatagrid();
        }

        protected void btn_search_Click(object sender, EventArgs e)
        {
            bounddatagrid();
        }

        protected void txt_search_TextChanged(object sender, EventArgs e)
        {
            bounddatagrid();
        }
    }
}