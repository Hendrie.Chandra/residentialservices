﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;

using System.Linq;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entrywatertreatmentplant : System.Web.UI.Page
    {
        static String prevPage = "watertreatmentplant.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                UserData userData = (UserData)ViewState["UserData"];

                show_ukuran_meter_air(userData.SiteID);
                //show_discounttype();
                Int32 discount = 0;
                lbl_subtotal.Text = "0";
                txt_discount.Text = discount.ToString();
                lbl_total.Text = "0";                
                show_currency();
                update_total();
                //alert_success.Visible = false;
                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            entryheader.ToggleAlert = false;
        }

        private void Show_Case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_wtp(_casenumber);

            entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
            entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
            //check customer type -> 0 = Owner, 1 = Tenant
            entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
            entryheader.combobox_unit();
            entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

            entryheader.combobox_caseorigin();
            entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

            if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

            entryheader.disable_all_controls();

            Int32 discount = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString()));
            Int32 subtotal = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Amount"].ToString()));
            Int32 total = Convert.ToInt32(decimal.Parse(dt.Rows[0]["AmountAfterDiscount"].ToString()));

            cboUkuranMeterAir.SelectedValue = dt.Rows[0]["WaterPlantCategoryID"].ToString();
            cboUkuranMeterAir_SelectedIndexChanged(this, EventArgs.Empty);

            cbo_currency.SelectedValue = dt.Rows[0]["CurrencyID"].ToString();
            cbo_currency_SelectedIndexChanged(this, EventArgs.Empty);

            lbl_rate.Text = dt.Rows[0]["Rate"].ToString();
            txt_discount.Text = discount.ToString();
            lbl_subtotal.Text = subtotal.ToString("N0");
            lbl_total.Text = total.ToString("N0");            
        }

        private void show_ukuran_meter_air(Int32 site)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_waterplantcategory(site);
            cboUkuranMeterAir.DataSource = dt;
            cboUkuranMeterAir.DataTextField = "WaterPlantCategoryName";
            cboUkuranMeterAir.DataValueField = "WaterPlantCategoryID";
            cboUkuranMeterAir.DataBind();
            cboUkuranMeterAir.Items.Insert(0, new ListItem("None", "0"));
            cboUkuranMeterAir.SelectedIndex = 0;
        }

        private void show_currency()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];
            DataTable dt = dbclass.retrieve_Currency(userData.OrgID, userData.SiteID, string.Empty);

            String[] CurrencySymbols = dt.AsEnumerable().Select(row => row.Field<String>("CurrencyID")).ToArray();
            ViewState["CurrencyID"] = CurrencySymbols;

            Decimal[] ExchangeRate = dt.AsEnumerable().Select(row => row.Field<Decimal>("ExchangeRate")).ToArray();
            ViewState["ExchangeRate"] = ExchangeRate;

            cbo_currency.DataSource = dt;
            cbo_currency.DataTextField = "CurrencyID";
            cbo_currency.DataValueField = "CurrencyID";
            cbo_currency.DataBind();            
            cbo_currency.SelectedIndex = 0;
        }      
       
        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }
        
        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["WaterTreatmentPlantHelpID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());
                       
            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);

            string CurrencyID = cbo_currency.SelectedValue;
            Nullable<Int32> Rate = null;
            if (cbo_currency.SelectedValue != "IDR")
                Rate = Int32.Parse(lbl_rate.Text, NumberStyles.Currency);

            Double TotalAmount = Double.Parse(lbl_subtotal.Text);
            Int32 TotalDiscount = Int32.Parse(txt_discount.Text, NumberStyles.Currency);
            Double TotalAmountAfterDiscount = TotalAmount - TotalDiscount;
            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            //request.TotalPrice = TotalAmount;
            //request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;
            
            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                /*request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();*/
                request.CaseNumber = CaseDT.Rows[0]["CaseNumber"].ToString();
            }

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_wtp_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, Int32.Parse(cboUkuranMeterAir.SelectedValue), CurrencyID, Rate, TotalAmount, 0, TotalDiscount, TotalAmountAfterDiscount,
                    0, 0, TotalAmountAfterDiscount, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }

        protected bool transaction_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (cboUkuranMeterAir.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Pipe Size is required.";                
                return false;
            }
            if (Decimal.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }      
            return true;
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            save_transaction();
            entryheader.reset_fields(Page.Controls);
            entryheader.combobox_unit();

            lbl_subtotal.Text = "0";
            lbl_total.Text = "0";
            txt_discount.Text = "0";
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(txt_discount.Text))
                txt_discount.Text = "0";
            update_total();
        }

        protected void cboUkuranMeterAir_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboUkuranMeterAir.SelectedIndex != 0)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);

                UserData userData = (UserData)ViewState["UserData"];

                DataTable dt = dbclass.retrieve_waterplantcategory(Int32.Parse(cboUkuranMeterAir.SelectedValue), userData.SiteID);
                Int32 price = Convert.ToInt32(dt.Rows[0]["Price"]);

                lbl_subtotal.Text = price.ToString("N0");
                lbl_total.Text = price.ToString("N0");

                ViewState["Price"] = price;
            }
            else
            {
                lbl_subtotal.Text = "0";
                lbl_total.Text = "0";
                ViewState["Price"] = 0;
            }
            update_total();
        }

        protected void cbo_currency_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbo_currency.SelectedValue == "IDR")
            {
                kurs.Visible = false;
            }
            else
            {                
                kurs.Visible = true;
            }
            //String CurrencySymbol = ((String[])ViewState["CurrencySymbols"])[cbo_currency.SelectedValue];
            //lbl_cursym1.Text = CurrencySymbol;
            //lbl_cursym2.Text = CurrencySymbol;

            //String[] strExchangeRate = (String[])ViewState["CurrencySymbols"];
            //Decimal ExchangeRate = strExchangeRate.Select(x => Decimal.Parse(x)).ToArray()[Int32.Parse(cbo_currency.SelectedValue) - 1];

            //Decimal ExchangeRate = ((Decimal[])ViewState["ExchangeRate"])[Int32.Parse(cbo_currency.SelectedValue) - 1];
            //lbl_rate.Text = ExchangeRate.ToString("N0");
            lbl_rate.Text = "1";
            update_total();
        }        
        private void update_total()
        {            
            Decimal Price = 0;
            if (ViewState["Price"] != null) Price = Decimal.Parse(ViewState["Price"].ToString());
            Decimal SubTotal = 0;
            Decimal Adjustment = 0;
            Decimal Total = 0;

            if (lbl_rate.Text == "")
            {
                lbl_rate.Text = "1";
            }

            if (cbo_currency.SelectedValue != "1")
            {
                SubTotal = Price / Decimal.Parse(lbl_rate.Text);
                Adjustment = Decimal.Parse(txt_discount.Text);
                Total = SubTotal - Adjustment;

                lbl_subtotal.Text = SubTotal.ToString("N2");
                lbl_adjustment.Text = Adjustment.ToString();
                lbl_total.Text = Total.ToString("N2");
            }
            else
            {
                SubTotal = Price;
                Adjustment = Decimal.Parse(txt_discount.Text);
                Total = SubTotal - Adjustment;

                lbl_adjustment.Text = Adjustment.ToString();
                lbl_subtotal.Text = SubTotal.ToString("N0");
                lbl_total.Text = Total.ToString("N0");
            }                        
        }

        protected void txt_rate_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(lbl_rate.Text))
                lbl_rate.Text = "1";
            update_total();
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }        
    }
}