﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="smartcard.aspx.cs" Inherits="residential.web.forms.smartcard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="page-header">
                <h1>List Request Smart Card</h1>
            </div>
            <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-primary" NavigateUrl="~/forms/entrysmartcard">Add</asp:HyperLink>
            <hr />
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-8">
                        <label class="col-sm-2 control-label">Case Status</label>
                        <div class="col-sm-6">
                           <asp:DropDownList ID="cbo_status" runat="server" CssClass="form-control" OnSelectedIndexChanged="cbo_status_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="txt_search" runat="server" CssClass="form-control" OnTextChanged="txt_search_TextChanged" AutoPostBack="true" placeholder="search by case number, unit"></asp:TextBox>
                            <span class="input-group-btn">
                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" OnClick="btn_search_Click" />
<%--                                <asp:Button ID="btn_search" runat="server" Text="..." CssClass="btn btn-default" />--%>
                            </span>
                        </div>
                    </div>
                </div>
            </div> 
            <asp:GridView ID="gvw_data" runat="server" AutoGenerateColumns="False" 
                CssClass="table table-bordered table-hover" AllowPaging="True" 
                onrowdatabound="gvw_data_RowDataBound" ShowHeaderWhenEmpty="true" 
                DataKeyNames="CaseNumber" AllowSorting="true" onsorting="gvw_data_Sorting" 
                onpageindexchanging="gvw_data_PageIndexChanging">
                <PagerStyle CssClass="pagination-ys" />
                <Columns>
                    <asp:TemplateField HeaderText="NO" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <asp:Label ID="lbl_no" runat="server" Text="<%# (Container.DataItemIndex + 1).ToString() %>"></asp:Label>
                        </ItemTemplate>                
                    </asp:TemplateField>                                      
                    <asp:BoundField HeaderText="CASE NUMBER" DataField="CaseNumber" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Bold="true" />
                    <asp:BoundField HeaderText="UNIT CODE" DataField="Unit" SortExpression="Unit" />
                    <asp:BoundField HeaderText="CATEGORY" DataField="HelpName" SortExpression="HelpName" />
                    <asp:TemplateField HeaderText="CARD TYPE" SortExpression="CardTypeID">
                        <ItemTemplate>
                            <asp:Label ID="lbl_cardtype" runat="server" Text='<%#(Eval("CardTypeID").ToString()=="1" ? "Resident" : "Member") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="RESIDENT" DataField="ResidentStatus" SortExpression="ResidentStatus" />
                    <asp:BoundField HeaderText="CREATED" DataField="RequestDate" SortExpression="RequestDate" DataFormatString="{0:d MMMM yyyy}" />                        
                    <asp:BoundField HeaderText="OVERDUE" DataField="OverdueDate" SortExpression="OverdueDate" DataFormatString="{0:d MMMM yyyy}" />                    
                    <asp:BoundField HeaderText="STATUS" DataField="CaseStatusName" SortExpression="CaseStatusName" />                   
                </Columns>
                <EmptyDataTemplate>
                    No data
                </EmptyDataTemplate>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<%--<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
</asp:Content>--%>
