﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Drawing;

using residential.web.model;
using System.Data.SqlClient;
using System.Text;

namespace residential.web.forms
{
    public partial class viewsmartcard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["SmartCardHistory"] = null;
                load_card_type();
                load_resident_status();

                ViewState["UserData"] = new UserData(User.Identity.Name);
                show_case(Request.QueryString["CaseNumber"]);
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();
            }

            if (Session["SmartCardHistory"] != null)
            {
                bound_smartcardhistory();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "load_download_file_js", "load_download_file_js();", true);
        }

        private void show_case(String _casenumber)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_Case(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                DataTable dt2 = dbclass.retrieve_smartcard(_casenumber);
                hdf_case_number.Value = _casenumber;
                retrieve_smartcard_history(viewheader.PSCode);

                //cek box
                bool IsIdentitasDiri = false;
                bool IsKK = false;
                bool IsSTNK = false;
                bool IsKepemilikanUnit = false;
                bool IsPerjanjianSewaMenyewaUnit = false;
                bool IsSuratKeteranganKaryawan = false;
                bool IsBuktiBayarUtilitas = false;
                bool IsSuratKuasa = false;
                bool IsKTPPemberiPenerimaKuasa = false;
                string lainlain = "";

                IsIdentitasDiri = ((bool)dt2.Rows[0]["IsIdentitasDiri"] ? CheckBox_identitasdiri.Checked=true : CheckBox_identitasdiri.Checked=false);
                CheckBox_identitasdiri.Enabled = false;
                if (dt2.Rows[0]["IdentitasDiriFileUpload"].ToString().Length>0)
                {
                    hpl_identitasdiri.Text = dt2.Rows[0]["IdentitasDiriFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["IdentitasDiriFileUpload"];
                    hdf_identitasdiri_base64.Value = convertToBase64String(data);
                    hdf_identitasdiri_type.Value = dt2.Rows[0]["IdentitasDiriFileType"].ToString();
                    hpl_identitasdiri.Enabled = true;
                }
                else
                {
                    hpl_identitasdiri.Text = "None";
                    hpl_identitasdiri.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_identitasdiri.Enabled = false;
                }
                IsKK = ((bool)dt2.Rows[0]["IsKK"] ? CheckBox_kk.Checked = true : CheckBox_kk.Checked = false);                
                CheckBox_kk.Enabled = false;
                if (dt2.Rows[0]["KKFileUpload"].ToString().Length>0)
                {
                    hpl_kk.Text = dt2.Rows[0]["KKFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["KKFileUpload"];
                    hdf_kk_base64.Value = convertToBase64String(data);
                    hdf_kk_type.Value = dt2.Rows[0]["KKFileType"].ToString();
                    hpl_kk.Enabled = true;
                }
                else
                {
                    hpl_kk.Text = "None";
                    hpl_kk.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_kk.Enabled = false;
                }
                IsSTNK = ((bool)dt2.Rows[0]["IsSTNK"] ? CheckBox_stnk.Checked = true : CheckBox_stnk.Checked = false);
                CheckBox_stnk.Enabled = false;
                if (dt2.Rows[0]["STNKFileUpload"].ToString().Length>0)
                {
                    //hpl_stnk.Text = dt2.Rows[0]["STNKFileName"].ToString();
                    //hpl_stnk.Enabled = true;
                    hpl_stnk.Text = dt2.Rows[0]["STNKFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["STNKFileUpload"];
                    hdf_stnk_base64.Value = convertToBase64String(data);
                    hdf_stnk_type.Value = dt2.Rows[0]["STNKFileType"].ToString();
                    hpl_stnk.Enabled = true;
                }
                else
                {
                    hpl_stnk.Text = "None";
                    hpl_stnk.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_stnk.Enabled = false;
                }
                IsKepemilikanUnit = ((bool)dt2.Rows[0]["IsKepemilikanUnit"] ? CheckBox_kepemilikanunit.Checked = true : CheckBox_kepemilikanunit.Checked = false);
                CheckBox_kepemilikanunit.Enabled = false;
                if (dt2.Rows[0]["KepemilikanUnitFileUpload"].ToString().Length>0)
                {
                    //hpl_kepemilikanunit.Text = dt2.Rows[0]["KepemilikanUnitFileName"].ToString();
                    //hpl_kepemilikanunit.Enabled = true;
                    hpl_kepemilikanunit.Text = dt2.Rows[0]["KepemilikanUnitFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["KepemilikanUnitFileUpload"];
                    hdf_kepemilikanunit_base64.Value = convertToBase64String(data);
                    hdf_kepemilikanunit_type.Value = dt2.Rows[0]["KepemilikanUnitFileType"].ToString();
                    hpl_kepemilikanunit.Enabled = true;
                }
                else
                {
                    hpl_kepemilikanunit.Text = "None";
                    hpl_kepemilikanunit.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_kepemilikanunit.Enabled = false;
                }
                IsPerjanjianSewaMenyewaUnit = ((bool)dt2.Rows[0]["IsPerjanjianSewaMenyewaUnit"] ? CheckBox_perjanjiansewamenyewaunit.Checked = true : CheckBox_perjanjiansewamenyewaunit.Checked = false);
                CheckBox_perjanjiansewamenyewaunit.Enabled = false;
                if (dt2.Rows[0]["PerjanjianSewaMenyewaUnitFileUpload"].ToString().Length>0)
                {
                    //hpl_perjanjiansewamenyewaunit.Text = dt2.Rows[0]["PerjanjianSewaMenyewaUnitFileName"].ToString();
                    //hpl_perjanjiansewamenyewaunit.Enabled = true;
                    hpl_perjanjiansewamenyewaunit.Text = dt2.Rows[0]["PerjanjianSewaMenyewaUnitFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["PerjanjianSewaMenyewaUnitFileUpload"];
                    hdf_perjanjiansewamenyewaunit_base64.Value = convertToBase64String(data);
                    hdf_perjanjiansewamenyewaunit_type.Value = dt2.Rows[0]["PerjanjianSewaMenyewaUnitFileType"].ToString();
                    hpl_perjanjiansewamenyewaunit.Enabled = true;
                }
                else
                {
                    hpl_perjanjiansewamenyewaunit.Text = "None";
                    hpl_perjanjiansewamenyewaunit.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_perjanjiansewamenyewaunit.Enabled = false;
                }
                IsSuratKeteranganKaryawan = ((bool)dt2.Rows[0]["IsSuratKeteranganKaryawan"] ? CheckBox_suratketerangankaryawan.Checked = true : CheckBox_suratketerangankaryawan.Checked = false);
                CheckBox_suratketerangankaryawan.Enabled = false;
                if (dt2.Rows[0]["SuratKeteranganKaryawanFileUpload"].ToString().Length>0)
                {
                    //hpl_suratketerangankaryawan.Text = dt2.Rows[0]["SuratKeteranganKaryawanFileName"].ToString();
                    //hpl_suratketerangankaryawan.Enabled = true;
                    hpl_suratketerangankaryawan.Text = dt2.Rows[0]["SuratKeteranganKaryawanFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["SuratKeteranganKaryawanFileUpload"];
                    hdf_suratketerangankaryawan_base64.Value = convertToBase64String(data);
                    hdf_suratketerangankaryawan_type.Value = dt2.Rows[0]["SuratKeteranganKaryawanFileType"].ToString();
                    hpl_suratketerangankaryawan.Enabled = true;
                }
                else
                {
                    hpl_suratketerangankaryawan.Text = "None";
                    hpl_suratketerangankaryawan.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_suratketerangankaryawan.Enabled = false;
                }
                IsBuktiBayarUtilitas = ((bool)dt2.Rows[0]["IsBuktiBayarUtilitas"] ? CheckBox_buktibayarutilitas.Checked = true : CheckBox_buktibayarutilitas.Checked = false);
                CheckBox_buktibayarutilitas.Enabled = false;
                if (dt2.Rows[0]["BuktiBayarUtilitasFileUpload"].ToString().Length>0)
                {
                    //hpl_buktibayarutilitas.Text = dt2.Rows[0]["BuktiBayarUtilitasFileName"].ToString();
                    //hpl_buktibayarutilitas.Enabled = true;
                    hpl_buktibayarutilitas.Text = dt2.Rows[0]["BuktiBayarUtilitasFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["BuktiBayarUtilitasFileUpload"];
                    hdf_buktibayarutilitas_base64.Value = convertToBase64String(data);
                    hdf_buktibayarutilitas_type.Value = dt2.Rows[0]["BuktiBayarUtilitasFileType"].ToString();
                    hpl_buktibayarutilitas.Enabled = true;
                }
                else
                {
                    hpl_buktibayarutilitas.Text = "None";
                    hpl_buktibayarutilitas.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_buktibayarutilitas.Enabled = false;
                }
                IsSuratKuasa = ((bool)dt2.Rows[0]["IsSuratKuasa"] ? CheckBox_suratkuasa.Checked = true : CheckBox_suratkuasa.Checked = false);
                CheckBox_suratkuasa.Enabled = false;
                if (dt2.Rows[0]["SuratKuasaFileUpload"].ToString().Length>0)
                {
                    //hpl_suratkuasa.Text = dt2.Rows[0]["SuratKuasaFileName"].ToString();
                    //hpl_suratkuasa.Enabled = true;
                    hpl_suratkuasa.Text = dt2.Rows[0]["SuratKuasaFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["SuratKuasaFileUpload"];
                    hdf_suratkuasa_base64.Value = convertToBase64String(data);
                    hdf_suratkuasa_type.Value = dt2.Rows[0]["SuratKuasaFileType"].ToString();
                    hpl_suratkuasa.Enabled = true;
                }
                else
                {
                    hpl_suratkuasa.Text = "None";
                    hpl_suratkuasa.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_suratkuasa.Enabled = false;
                }
                IsKTPPemberiPenerimaKuasa = ((bool)dt2.Rows[0]["IsKTPPemberiPenerimaKuasa"] ? CheckBox_ktppemberipenerimakuasa.Checked = true : CheckBox_ktppemberipenerimakuasa.Checked = false);
                CheckBox_ktppemberipenerimakuasa.Enabled = false;
                if (dt2.Rows[0]["KTPPemberiPenerimaKuasaFileUpload"].ToString().Length>0)
                {
                    //hpl_ktppemberipenerimakuasa.Text = dt2.Rows[0]["KTPPemberiPenerimaKuasaFileName"].ToString();
                    //hpl_ktppemberipenerimakuasa.Enabled = true;
                    hpl_ktppemberipenerimakuasa.Text = dt2.Rows[0]["KTPPemberiPenerimaKuasaFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["KTPPemberiPenerimaKuasaFileUpload"];
                    hdf_ktppemberipenerimakuasa_base64.Value = convertToBase64String(data);
                    hdf_ktppemberipenerimakuasa_type.Value = dt2.Rows[0]["KTPPemberiPenerimaKuasaFileType"].ToString();
                    hpl_ktppemberipenerimakuasa.Enabled = true;
                }
                else
                {
                    hpl_ktppemberipenerimakuasa.Text = "None";
                    hpl_ktppemberipenerimakuasa.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_ktppemberipenerimakuasa.Enabled = false;
                }
                txt_lainlain.Text = dt2.Rows[0]["Other"].ToString();
                txt_lainlain.ReadOnly = true;
                if (dt2.Rows[0]["OtherFileUpload"].ToString().Length>0)
                {
                    //hpl_other.Text = dt2.Rows[0]["OtherFileName"].ToString();
                    //hpl_other.Enabled = true;
                    hpl_other.Text = dt2.Rows[0]["OtherFileName"].ToString();
                    byte[] data = (byte[])dt2.Rows[0]["OtherFileUpload"];
                    hdf_other_base64.Value = convertToBase64String(data);
                    hdf_other_type.Value = dt2.Rows[0]["OtherFileType"].ToString();
                    hpl_other.Enabled = true;
                }
                else
                {
                    hpl_other.Text = "None";
                    hpl_other.Attributes.Add("style", "border:none; box-shadow:none; color:#BDBDC3");
                    hpl_other.Enabled = false;
                }

                ddl_card_type.Items.FindByValue(dt2.Rows[0]["CardTypeID"].ToString()).Selected = true;
                ddl_resident_status.Items.FindByValue(dt2.Rows[0]["ResidentStatus"].ToString()).Selected = true;
                if (dt2.Rows[0]["ResidentStatus"].ToString().ToLower() == "rent" && dt2.Rows[0]["CardTypeID"].ToString().ToLower()== "1")
                {
                    lbl_rent_periode_date.Visible = true;
                    txt_rent_periode_date.Visible = true;
                    txt_rent_periode_date.Enabled = false;

                    DateTime rentperiodedate = DateTime.Parse(dt2.Rows[0]["RentPeriodeDate"].ToString());
                    txt_rent_periode_date.Text = rentperiodedate.ToString("dd MMMM yyyy");
                }  

                int transactiontype = (int)dt2.Rows[0]["TransactionType"];
                if (transactiontype == 1)
                {
                    CheckBox_isreplacement.Checked = true;
                }

                txt_quantity.Text = dt2.Rows[0]["Quantity"].ToString();
                txt_price.Text = dt2.Rows[0]["Price"].ToString();
                txt_total.Text = dt2.Rows[0]["TotalAmount"].ToString();              
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }

        protected void load_card_type()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_master_card_type();

            ddl_card_type.DataSource = dt;
            ddl_card_type.DataValueField = "SmartCardTypeID";
            ddl_card_type.DataTextField = "SmartCardTypeName";
            ddl_card_type.DataBind();

        }
        protected void load_resident_status()
        {
            List<string> resident_status = new List<string>();
            resident_status.Add("Owner");
            resident_status.Add("Rent");

            ddl_resident_status.DataSource = resident_status;
            ddl_resident_status.DataBind();
        }

        private void bound_smartcardhistory()
        {
            gvw_smartcardhistory.DataSource = Session["SmartCardHistory"];
            gvw_smartcardhistory.DataBind();
        }

        public void retrieve_smartcard_history(string pscode)
        {
            Session["SmartCardHistory"] = null;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_smartcard_history(pscode);

            List<SmartCardHistory> list = new List<SmartCardHistory>();

            foreach (DataRow dr in dt.Rows)
            {
                SmartCardHistory obj = new SmartCardHistory();

                obj.OrgID = (int)dr["OrgID"];
                obj.SiteID = (int)dr["SiteID"];
                obj.SmartCardDetailID = (int)dr["SmartCardDetailID"];
                obj.CaseNumber = dr["CaseNumber"].ToString();
                obj.TransactionType = (int)dr["TransactionType"];
                obj.TransactionTypeDescription = dr["TransactionTypeDescription"].ToString();
                obj.CardTypeID = (dr["CardTypeID"].ToString() == "" ? (int?)null : (int)dr["CardTypeID"]);
                obj.CardNumber = dr["CardNumber"].ToString();
                obj.ChipNumber = dr["ChipNumber"].ToString();
                obj.HolderName = dr["HolderName"].ToString();
                obj.VehicleNumber = dr["VehicleNumber"].ToString();
                obj.BoomGateID = (int)dr["BoomGateid"];
                obj.BoomGateName = dr["BoomGateName"].ToString();
                obj.CreatedDate = (DateTime)dr["CreatedDate"];
                obj.CreatedBy = dr["CreatedBy"].ToString();
                //obj.IsActive = (bool)dr["IsActive"];

                list.Add(obj);
            }

            Session["SmartCardHistory"] = list;
        }

        protected void download(DataTable dt)
        {
            if (CheckBox_identitasdiri.Checked)
            {
                Byte[] bytes = (Byte[])dt.Rows[0]["IdentitasDiriFileUpload"];
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = dt.Rows[0]["IdentitasDiriFileType"].ToString();
                Response.AddHeader("content-disposition", "attachment;filename=" + dt.Rows[0]["IdentitasDiriFileName"].ToString());
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }

        }

        public void retrieve_file_download(string case_number, string file_name)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_file_upload(case_number);
            download(dt);
        }

        protected void btn_download_file_Click(object sender, EventArgs e)
        {
            string caseNumber = hdf_case_number.Value;
            string key = hdf_db_field.Value;

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_file_upload(caseNumber);

            Byte[] bytes = (Byte[])dt.Rows[0][""+key+"FileUpload"+""];
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = dt.Rows[0]["" + key + "FileType" + ""].ToString();
            Response.AddHeader("content-disposition", "attachment;filename=\"" + dt.Rows[0]["" + key + "FileName" + ""].ToString()+"\"");
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }

        protected string convertToBase64String(byte[] data)
        {
            return Convert.ToBase64String(data);
        }
    }
}