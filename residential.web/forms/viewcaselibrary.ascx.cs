﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using residential.libs.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data;
using System.IO;

namespace residential.web.forms
{
    public partial class viewcaselibrary : System.Web.UI.UserControl
    {
        /*static ConnectionClass connClass = new ConnectionClass();
        static CRMConnectionClass CRMConn = connClass.CRMConnection();
        static SharePointConnectionClass SPConn = connClass.SharePointConnection();*/

        String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["prosesadt"] == null)
                {
                    show_case_library();
                }
                else
                {
                    if (Request.QueryString["prosesadt"].ToString() == "downloadnote")
                    {
                        DownloadNote(Request.QueryString["CaseNumber"].ToString(), int.Parse(Request.QueryString["attachmentid"]));
                    }
                    else
                    {
                        DeleteNote(Request.QueryString["CaseNumber"].ToString(), int.Parse(Request.QueryString["attachmentid"]));
                    }
                }
            }
        }

        protected void show_case_library()
        {
            DBclass dbclass = new DBclass(constring);

            if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable dt = dbclass.retrieve_attachment(Request.QueryString["CaseNumber"].ToString(),null);
                if (dt.Rows.Count > 0)
                {
                    DataTable table = new DataTable();

                    table.Columns.Add("Id", typeof(int));
                    table.Columns.Add("CaseNumber", typeof(string));
                    table.Columns.Add("FileName", typeof(string));
                    table.Columns.Add("CaseStatusID", typeof(int));
                    table.Columns.Add("MimeType", typeof(string));

                    Int32 i = 0;
                    foreach (DataRow dr in dt.Rows)
                    {
                        var row = table.NewRow();
                        row["Id"] = dr["AttachmentID"];           
                        row["CaseNumber"] = dr["CaseNumber"];
                        row["FileName"] = dr["FileUploadName"];     
                        row["CaseStatusID"] = dr["CaseStatusID"];
                        row["MimeType"] = dr["FileUploadType"];

                        table.Rows.Add(row);
                        i++;
                    }
                    gvw_attachmentfromcrm.DataSource = table;
                }else
                    gvw_attachmentfromcrm.EmptyDataText = "No attachment !!";
            }
            else
            {
                gvw_attachmentfromcrm.EmptyDataText = "No attachment !!";
            }
            gvw_attachmentfromcrm.DataBind();          
        }

        protected void DownloadNote(String CaseNumber, int AttachmentID)
        {
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_attachment(CaseNumber, AttachmentID);
            if (dt.Rows.Count > 0)
            {
                string mimetype = dt.Rows[0]["FileUploadType"].ToString();

                byte[] byteData = (Byte[])dt.Rows[0]["Attachment"];

                Response.Clear();
                Response.BinaryWrite(byteData);
                Response.AddHeader("Content-Disposition", "attachment; filename=" + dt.Rows[0]["FileUploadName"].ToString());
                Response.AddHeader("Content-Length", byteData.Length.ToString());
                Response.ContentType = mimetype;

                //Response.Output.Write("data:" + mimetype + ";base64," + Attachment.Attributes["documentbody"].ToString());
                Response.Flush();
                Response.End();
            }
        }

        protected void DeleteNote(String CaseNumber, int AttachmentID)
        {
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_attachment(CaseNumber, AttachmentID);
            if (dt.Rows.Count > 0)
            {
                string returnValue = dbclass.delete_attachment(CaseNumber, AttachmentID);
                Response.Clear();
                Response.Output.Write("Sukses");
                Response.Flush();
                Response.End();
            }
        }

        // Start-- Adhitya Prabowo Update Fixing 20161215
        protected void gvw_attachmentfromcrm_RowCommand(object sender, GridViewCommandEventArgs e)
        {
        }

        protected void gvw_attachmentfromcrm_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int id = int.Parse(DataBinder.Eval(e.Row.DataItem, "Id").ToString());
                string CaseNumber = DataBinder.Eval(e.Row.DataItem, "CaseNumber").ToString();
                LinkButton linkbutton = (LinkButton)e.Row.FindControl("DeleteButton");
                String url = "DeleteNote('" + CaseNumber + "','" + id + "')";
                linkbutton.Attributes.Add("onclick", url);

                linkbutton = (LinkButton)e.Row.FindControl("DownloadButton");
                url = "DownloadNote('" + CaseNumber + "','" + id + "')";
                linkbutton.Attributes.Add("onclick", url);
            }

        }
        // Adhitya Prabowo Update Fixing 20161215 --End


    }
}