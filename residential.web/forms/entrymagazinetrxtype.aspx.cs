﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;

namespace residential.web.forms
{
    public partial class entrymagazinetrxtype : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                if (Request.QueryString["MSMagazineTrxTypeID"] != null)
                {
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);

                    Int32 MSMagazineTrxTypeID = Int32.Parse(Request.QueryString["MSMagazineTrxTypeID"].ToString());
                    DataTable dt = dbclass.retrieve_magazinetrxtype(MSMagazineTrxTypeID);

                    DataRow datarow = dt.Rows[0];
                    lbl_MagazineTrxTypeId.Text = datarow["MSMagazineTrxTypeID"].ToString();
                    txt_bannerMagazineTrxTypeName.Text = datarow["MSMagazineTrxTypeName"].ToString();
                    //txt_price.Text = ((decimal)datarow["Price"]).ToString("N0");
                    txt_price.Text = String.Format("{0:0}", ((decimal)datarow["Price"]));
                }
            }
        }

        private Int32 save_magazinetrxtype()
        {
            UserData userData = new UserData(User.Identity.Name);        

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = 0;
            if (!String.IsNullOrEmpty(lbl_MagazineTrxTypeId.Text))
            {
                returnvalue = dbclass.save_magazinetrxtype(Int32.Parse(lbl_MagazineTrxTypeId.Text), txt_bannerMagazineTrxTypeName.Text, Int32.Parse(txt_price.Text), userData.UserName);
            }
            else
            {
                returnvalue = dbclass.save_magazinetrxtype(0, txt_bannerMagazineTrxTypeName.Text, Int32.Parse(txt_price.Text), userData.UserName);
            }
            return returnvalue;
        }

        protected Boolean sites_validation()
        {
            if (String.IsNullOrEmpty(txt_bannerMagazineTrxTypeName.Text) )
            {
                alert_danger_text.Text = "banner Magazine Trx Type Name is required!";
                alert_danger.Visible = true;
                return false;
            }
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_magazinetrxtype();
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_magazinetrxtype();
                Response.Redirect("listmagazinetrxtype.aspx");
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            }
        }

        protected void btn_savenew_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_magazinetrxtype();
                lbl_MagazineTrxTypeId.Text = String.Empty;
                txt_bannerMagazineTrxTypeName.Text = String.Empty;
                txt_price.Text = String.Empty;
            }
        }     
    }
}