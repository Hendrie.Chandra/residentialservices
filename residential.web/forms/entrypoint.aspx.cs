﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using residential.libs;
using System.Data;


namespace residential.web.forms
{
    public partial class entrypoint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                if (Request.QueryString["PointID"] != null)
                {
                    show_data(Int32.Parse(Request.QueryString["PointID"].ToString()));
                }
            }
        }

        protected void show_data(Int32 _PointID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);            
            DataTable dt = dbclass.retrieve_pointmst(_PointID);

            DataRow datarow = dt.Rows[0];
            txt_pointname.Text = datarow["PointName"].ToString();            
        }

        private Int32 save_pointmst()
        {
            UserData userData = new UserData(User.Identity.Name);
            
            Int32 PointID = (Request.QueryString["PointID"] == null) ? 0 : Int32.Parse(Request.QueryString["PointID"].ToString());
            Int32 BannerLocationID = Int32.Parse(Request.QueryString["BannerLocationID"].ToString());

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();            
            DBclass dbclass = new DBclass(constring);

            Int32 returnvalue = dbclass.save_point(txt_pointname.Text, BannerLocationID, userData.UserName);
            return returnvalue;
        }

        protected Boolean sites_validation()
        {
            if (String.IsNullOrEmpty(txt_pointname.Text))
            {
                alert_danger_text.Text = "Point Name is required!";
                alert_danger.Visible = true;
                return false;
            }            
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_pointmst();
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (sites_validation())
            {
                Int32 returnvalue = save_pointmst();
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "closedetail", "closeWindow(self)", true);
            }
        }        
    }
}