﻿<%@ Page Language="C#" MasterPageFile="~/main.master" AutoEventWireup="true" CodeBehind="viewenvironment.aspx.cs"
    Inherits="residential.web.forms.viewenvironment" %>

<%@ Register Src="viewcaselibrary.ascx" TagName="ViewCaseLibrary" TagPrefix="asp" %>
<%@ Register Src="viewheader.ascx" TagName="viewheader" TagPrefix="viewheader" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:TabContainer runat="server" ID="Tabs" Height="100%" ActiveTabIndex="0" Width="100%">
                <asp:TabPanel runat="server" ID="Panel1" HeaderText="Detail">
                    <ContentTemplate>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <div>
                                    <div class="alert alert-success" id="alert_success" runat="server" visible="false">
                                        <asp:Label runat="server" ID="alert_success_text"></asp:Label></div>
                                    <div class="alert alert-danger" id="alert_danger" runat="server" visible="false">
                                        <asp:Label runat="server" ID="alert_danger_text"></asp:Label></div>
                                    <div class="style2">
                                        <span class="style3"><strong>Environment Department</strong></span></div>
                                    <viewheader:viewheader ID="viewheader" runat="server" />
                                    <hr />
                                    <div class="formentrycontent">
                                        <div class="formentrycell1">
                                            <asp:Label ID="Label18" runat="server" Text="Service Request :" CssClass="labelfield"></asp:Label>
                                            <asp:Label ID="lbl_servicerequest" runat="server" CssClass="labeldesc"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="formentrycontent" runat="server" id="startandenddate">
                                        <div class="formentrycell2">
                                            <asp:Label ID="Label11" runat="server" Text="Start Date :" CssClass="labelfield"></asp:Label>
                                            <asp:Label ID="lbl_startdate" runat="server" CssClass="labelfield"></asp:Label>
                                        </div>
                                        <div class="formentrycell2">
                                            <asp:Label ID="Label13" runat="server" CssClass="labelfield" Text="End Date :"></asp:Label>
                                            <asp:Label ID="lbl_enddate" runat="server" CssClass="labelfield"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="formentrycontent">
                                        <div class="formentrycell2">
                                            <asp:Label ID="Label5" runat="server" Text="Price :" CssClass="labelfield"></asp:Label>
                                            <asp:Label ID="lbl_quantityprice" runat="server" CssClass="labelfield"></asp:Label>
                                        </div>
                                        <div class="formentrycell2">
                                            <asp:Label ID="Label1" runat="server" CssClass="labelfield" Text="Discount :"></asp:Label>
                                            <asp:Label ID="lbl_disc" runat="server" CssClass="labelfield"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="formentrycontent">
                                        <div class="formentrycell2" style="text-align: right">
                                            <asp:Label ID="Label4" runat="server" Text="Sub Total" CssClass="style1"></asp:Label>
                                        </div>
                                        <div class="formentrycell2" style="text-align: right">
                                            <asp:Label ID="lbl_subtotal" runat="server" CssClass="style1"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="formentrycontent">
                                        <div class="formentrycell2" style="text-align: right">
                                            <asp:Label ID="Label2" runat="server" Text="Discount" CssClass="style1"></asp:Label>
                                        </div>
                                        <div class="formentrycell2" style="text-align: right">
                                            <asp:Label ID="lbl_discount" runat="server" CssClass="style1"></asp:Label>
                                        </div>
                                    </div>
                                    <div class="formentrycontent">
                                        <div class="formentrycell2" style="text-align: right">
                                            <asp:Label ID="Label6" runat="server" Text="Total" CssClass="style1"></asp:Label>
                                        </div>
                                        <div class="formentrycell2" style="text-align: right">
                                            <asp:Label ID="lbl_total" runat="server" CssClass="style1"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div runat="server" id="updatecaseform">
                                    <div class="formentrycontent">
                                        <div class="formentrycell2">
                                            <asp:Label ID="Label7" runat="server" CssClass="labelfield" Text="Status :"></asp:Label>
                                            <asp:DropDownList ID="cbo_casestatus" runat="server" AutoPostBack="true" CssClass="ComboBoxWindowsStyle"
                                                Width="167px">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="formentrycell2" runat="server" id="satisfactionblock">
                                            <asp:Label ID="Label3" runat="server" CssClass="labelfield" Text="Satisfaction :"></asp:Label>
                                            <asp:DropDownList ID="cbo_satisfaction" runat="server" CssClass="ComboBoxWindowsStyle"
                                                Width="167px">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="formentrycontent">
                                        <div class="formentrycellmultiline">
                                            <asp:Label ID="Label9" runat="server" Text="Remarks * :" CssClass="labelfield"></asp:Label>
                                            <asp:TextBox ID="txt_remarks" runat="server" CssClass="textbox_entry_left textboxcell1multiline"
                                                TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="formentrycontent">
                                        <div class="formentrycell1">
                                            <asp:Label ID="Label8" runat="server" Text="File :" CssClass="labelfield"></asp:Label>
                                            <asp:FileUpload ID="fu_uploadattachment" runat="server" CssClass="textbox_entry_left" />
                                        </div>
                                    </div>
                                </div>
                                <asp:Label ID="Label10" runat="server" Text="Ticket History :" CssClass="labelfield"></asp:Label>
                                <asp:GridView ID="gvw_data" runat="server" CssClass="gridview" Width="100%" AutoGenerateColumns="False"
                                    OnRowDataBound="gvw_data_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="MODIFIED DATE">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_modifieddate" runat="server">date</asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Remarks" HeaderText="REMARKS">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CaseStatusName" HeaderText="STATUS">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FullName" HeaderText="MODIFIED BY">
                                            <HeaderStyle CssClass="gridview_header" />
                                            <ItemStyle CssClass="gridview_item_left" />
                                        </asp:BoundField>
                                    </Columns>
                                </asp:GridView>
                                <div class="formentrycontent">
                                    <div class="formentrycell2">
                                        <div runat="server" id="assigncase">
                                            <div class="formentrycell3">
                                                <asp:DropDownList ID="cbo_assign" runat="server" CssClass="ComboBoxWindowsStyle" Width="167px">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="formentrycell6">
                                                <asp:Button ID="btn_assign" runat="server" Text="ASSIGN" CssClass="button" OnClick="btn_assign_Click"
                                                    OnClientClick="buttonAssigning(this)" UseSubmitBehavior="false" />
                                            </div>
                                        </div>
                                    </div>
                                    <div style="text-align: right;" class="formentrycell2">
                                        <div class="formentrycell3">
                                            <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="button" Width="100px"
                                                OnClick="btn_cancel_Click" />
                                        </div>
                                        <div class="formentrycell6" runat="server" id="updatecase">
                                            <asp:Button ID="btn_update" runat="server" Text="UPDATE" CssClass="button" Width="100px"
                                                OnClick="btn_update_Click" OnClientClick="buttonUpdating(this)" UseSubmitBehavior="false" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btn_update" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Library">
                    <ContentTemplate>
                        <asp:ViewCaseLibrary runat="server" ID="ViewCaseLibrary1"></asp:ViewCaseLibrary>
                    </ContentTemplate>
                </asp:TabPanel>
            </asp:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
