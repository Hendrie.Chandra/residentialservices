﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Collections.Generic;
using System.Globalization;
using System.Web;


namespace residential.web.forms
{
    public partial class viewflyer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
                show_case(Request.QueryString["CaseNumber"]);                            
                ViewState["PreviousPageUrl"] = (Request.UrlReferrer == null) ? "Mytasklist.aspx" : Request.UrlReferrer.ToString();

                string deptID = Request.QueryString["DeptID"];

                if (deptID == null)
                {
                    updatecase.Visible = false;
                }
                else
                {
                    updatecase.Visible = true;
                    updatecase.DepartmentID = Request.QueryString["DeptID"].ToString();
                }
            }
        }
      
        private void show_case(String _casenumber)
        {           
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            DataTable dt = dbclass.retrieve_Case(_casenumber);

            if (dt.Rows.Count > 0)
            {
                viewheader.CustomerName = dt.Rows[0]["psName"].ToString();
                viewheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                viewheader.Unit = dt.Rows[0]["UnitCode"].ToString() + " - " + dt.Rows[0]["UnitNo"].ToString();
                viewheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();
                viewheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();
                viewheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();
                viewheader.CaseOrigin = dt.Rows[0]["CaseOriginName"].ToString();
                viewheader.Description = dt.Rows[0]["Description"].ToString();

                DataTable dt2 = dbclass.retrieve_flyer(_casenumber);

                Decimal price = Decimal.Parse(dt2.Rows[0]["Price"].ToString());
                lbl_quantityprice.Text = dt2.Rows[0]["NoOfFlyer"].ToString() + " X " + price.ToString("F2");

                lbl_adjustment.Text = Convert.ToInt32(decimal.Parse(dt2.Rows[0]["DiscountAmount"].ToString())).ToString();
                lbl_totalharga.Text = Convert.ToInt32(decimal.Parse(dt2.Rows[0]["Amount"].ToString())).ToString("N0");
                lbl_adjustment.Text = Convert.ToInt32(decimal.Parse(dt2.Rows[0]["DiscountAmount"].ToString())).ToString("N0");
                lbl_subtotal.Text = (Int32.Parse(lbl_totalharga.Text, NumberStyles.Currency) - Int32.Parse(lbl_adjustment.Text, NumberStyles.Currency)).ToString("N0"); //Convert.ToInt32(decimal.Parse(dt2.Rows[0]["AmountAfterDiscount"].ToString())).ToString("N0");
                lbl_ppn.Text = Convert.ToInt32(decimal.Parse(dt2.Rows[0]["TaxAmount"].ToString())).ToString("N0");
                lbl_total.Text = Convert.ToInt32(decimal.Parse(dt2.Rows[0]["ActualAmount"].ToString())).ToString("N0");
            }
            else
            {
                throw new HttpException(404, "Page not found");
            }
        }                      
    }
}