﻿using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using residential.libs;
using residential.libs.Models;
using residential.web.Classes;

namespace residential.web.forms
{
    public partial class entrypeminjamankendaraan : System.Web.UI.Page
    {
        static String prevPage = "peminjamankendaraan.aspx";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["UserData"] = new UserData(User.Identity.Name);
               

                show_car_license_plate();
                show_tarif();
                txtDate.Text = DateTime.Today.Date.ToString("dd MMMM yyyy");                
                hitung_total();
                gvw_data.DataBind();             

                if (Request.QueryString["CaseNumber"] != null)
                    Show_Case(Request.QueryString["CaseNumber"].ToString());
            }
            entryheader.ToggleAlert = false;
        }

        private void Show_Case(String _casenumber)
        {
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                DataTable dt = dbclass.retrieve_CarRent(_casenumber);

                entryheader.OwnerName = dt.Rows[0]["PSName"].ToString();
                entryheader.PSCode = dt.Rows[0]["PSCode"].ToString();
                //check customer type -> 0 = Owner, 1 = Tenant
                entryheader.CustomerType = Boolean.Parse(dt.Rows[0]["CustomerType"].ToString());
                entryheader.combobox_unit();
                entryheader.Unit = dt.Rows[0]["UnitCode"].ToString() + "-" + dt.Rows[0]["UnitNo"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["NamaPelapor"].ToString()))
                    entryheader.RequestorName = dt.Rows[0]["NamaPelapor"].ToString();

                entryheader.combobox_caseorigin();
                entryheader.CaseOrigin = dt.Rows[0]["CaseOriginID"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["TeleponPelapor"].ToString()))
                    entryheader.RequestorPhone = dt.Rows[0]["TeleponPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["EmailPelapor"].ToString()))
                    entryheader.RequestorEmail = dt.Rows[0]["EmailPelapor"].ToString();

                if (!String.IsNullOrEmpty(dt.Rows[0]["Description"].ToString()))
                    entryheader.RequestDescription = dt.Rows[0]["Description"].ToString();

                entryheader.disable_all_controls();

                cboCarLicensePlate.SelectedIndex = Int32.Parse(dt.Rows[0]["CarLicensePlateID"].ToString());
                txt_jumlah.Text = dt.Rows[0]["CarRentPackageID"].ToString();

                txtDate.Text = ((DateTime)dt.Rows[0]["Date"]).ToLocalTime().Date.ToString("dd MMMM yyyy");                

                txtPickTime.Text = dt.Rows[0]["StartPickupTime"].ToString();
                txtUntil.Text = dt.Rows[0]["EndPickupTime"].ToString();
                txtPickupLocation.Text = dt.Rows[0]["PickupLocation"].ToString();
                txtDestination.Text = dt.Rows[0]["Destination"].ToString();
                cboTariff.SelectedIndex = Int32.Parse(dt.Rows[0]["CarRentTariffID"].ToString());

                txt_discount.Text = "0";

                gvw_data.DataSource = FetchCarRent(Int32.Parse(cboCarLicensePlate.SelectedValue.ToString()), DateTime.Parse(txtDate.Text));
                gvw_data.DataBind();

                //hitung_total();

                Int32 discount = Convert.ToInt32(decimal.Parse(dt.Rows[0]["DiscountAmount"].ToString()));
                Int32 subtotal = Convert.ToInt32(decimal.Parse(dt.Rows[0]["Amount"].ToString()));
                Int32 total = Convert.ToInt32(decimal.Parse(dt.Rows[0]["AmountAfterDiscount"].ToString()));

                lbl_package.Text = txt_jumlah.Text;
                lbl_tariff.Text = Int32.Parse(dt.Rows[0]["Price"].ToString(), NumberStyles.Currency).ToString("N0");
                txt_discount.Text = discount.ToString();
                lbl_subtotal.Text = subtotal.ToString("N0");
                lbl_total.Text = total.ToString("N0");
            }
            catch(Exception)
            {
                throw new HttpException(404, "Page not found");
            }
        }

        private void hitung_total()
        {
            if (txt_discount.Text == string.Empty)
                txt_discount.Text = "0";

            if (cboTariff.SelectedIndex > 0)
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                DataTable dt = dbclass.retrieve_carrenttariff(Int32.Parse(cboTariff.SelectedValue));

                Int32 package = 1;
                if (!String.IsNullOrWhiteSpace(txt_jumlah.Text.ToString()))
                    package = Int32.Parse(txt_jumlah.Text.ToString());

                Int32 price = Int32.Parse(dt.Rows[0]["Price"].ToString(), NumberStyles.Currency);
                Int32 subtotal = price * package;
                Int32 discount = int.Parse(txt_discount.Text, NumberStyles.Currency);
                Int32 total = subtotal - discount;

                lbl_tariff.Text = price.ToString("N0");
                lbl_package.Text = package.ToString();
                lbl_subtotal.Text = subtotal.ToString("N0");
                lbl_adjustment.Text = discount.ToString("N0");
                lbl_total.Text = total.ToString("N0");
            }
        }

        private void show_car_license_plate()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_car_license_plate();
            cboCarLicensePlate.DataSource = dt;
            cboCarLicensePlate.DataTextField = "CarLicensePlateName";
            cboCarLicensePlate.DataValueField = "CarLicensePlateID";
            cboCarLicensePlate.DataBind();
            cboCarLicensePlate.Items.Insert(0, new ListItem("None", "0"));
            cboCarLicensePlate.SelectedIndex = 0;
        }

        private void show_tarif()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_car_rent_tariff();
            cboTariff.DataSource = dt;
            cboTariff.DataTextField = "CarRentTariffName";
            cboTariff.DataValueField = "CarRentTariffID";
            cboTariff.DataBind();
            cboTariff.Items.Insert(0, new ListItem("None", "0"));
            cboTariff.SelectedIndex = 0;
        }

        protected void cboTariff_SelectedIndexChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void txt_discount_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        private Int32 find_help(string param)
        {
            Int32 ParameterHelpID = 0;
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_parameter_help(param);
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                    ParameterHelpID = Int32.Parse(dt.Rows[0]["HelpID"].ToString());
                else
                    ParameterHelpID = 0;
            }
            else
                ParameterHelpID = 0;

            return ParameterHelpID;
        }

        private String find_crmhelpid(Int32 _helpid)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_help(_helpid);

            return dt.Rows[0]["CRMHelpID"].ToString();
        }

        protected bool transaction_validation()
        {
            if (entryheader.Validate != "VALID")
            {
                entryheader.ShowAlert = entryheader.Validate;
                return false;
            }
            if (cboCarLicensePlate.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Car license plate is required.";                
                return false;
            }
            if (txt_jumlah.Text == string.Empty)
            {
                entryheader.ShowAlert = "Package is required.";                
                return false;
            }
            if (cboTariff.SelectedIndex == 0)
            {
                entryheader.ShowAlert = "Tariff is required.";                
                return false;
            }
            if (Int32.Parse(lbl_total.Text, NumberStyles.Currency) < 0)
            {
                entryheader.ShowAlert = "Invalid total value";
                return false;
            }
            try
            {
                DateTime dt = DateTime.Parse(txtDate.Text.ToString());
            }
            catch (Exception)
            {
                entryheader.ShowAlert = "Invalid datetime format.";                
                return false;
            }
            Decimal total = Decimal.Parse(lbl_total.Text, NumberStyles.Currency);
            if (total < 0)
            {
                entryheader.ShowAlert = "Total value is invalid";                
                return false;
            }
            return true;
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String CaseNumber = save_transaction();

                string url = HttpContext.Current.Request.Url.AbsoluteUri;
                Response.Redirect(url + (url.IndexOf("?") == -1 ? "?" : "&") + "CaseNumber=" + CaseNumber);
            }
        }

        protected void btn_saveclose_Click(object sender, EventArgs e)
        {
            if (transaction_validation())
            {
                String res = save_transaction();
                if (!String.IsNullOrEmpty(res))
                {
                    entryheader.ShowAlert = res;
                }
                else
                    Response.Redirect(prevPage);
            }
        }

        private String save_transaction()
        {
            //String UrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
            //String Org = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
            //String UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            //String Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            //String Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            //CRMclass crmclass = new CRMclass(UrlOrg, Org, UserName, Password, Domain);

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            UserData userData = (UserData)ViewState["UserData"];

            String SavedBy = userData.UserName;
            Int32 OrgID = userData.OrgID;
            Int32 SiteID = userData.SiteID;

            DataTable dtHelpID = dbclass.retrieve_helpID(OrgID, SiteID, ConfigurationManager.AppSettings["CarRentHelpID"].ToString());
            Int32 HelpID = Int32.Parse(dtHelpID.Rows[0]["HelpID"].ToString());

            Int32 CaseOriginID = entryheader.CaseOriginID;
            String PSName = entryheader.OwnerName;
            String PSCode = entryheader.PSCode;
            String Unit = entryheader.Unit;
            String UnitCode = "";
            String UnitNo = "";
            if (!String.IsNullOrEmpty(Unit))
            {
                String[] unit = Unit.Split('-');
                UnitCode = unit[0].ToString();
                UnitNo = unit[1].ToString();
            }
            String NamaPelapor = entryheader.RequestorName;
            String TeleponPelapor = entryheader.RequestorPhone;
            String EmailPelapor = entryheader.RequestorEmail;
            String RequestDescription = entryheader.RequestDescription;

            DateTime RequestDate = DateTime.Now;
            DataTable dt = dbclass.retrieve_help(HelpID);
            Int16 Duration = Int16.Parse(dt.Rows[0]["Duration"].ToString());
            DateTime OverdueDate = RequestDate.AddDays(Duration);

            Int32 Price = Int32.Parse(lbl_tariff.Text, NumberStyles.Currency);
            Int32 Package = Int32.Parse(txt_jumlah.Text);
            Int32 TotalAmount = Price * Package;

            Int32 DiscountTypeID = 0;
            Int32 TotalDiscount = Int32.Parse(txt_discount.Text, NumberStyles.Currency);
            Int32 TotalAmountAfterDiscount = TotalAmount - TotalDiscount;
            Double TaxPercentage = 0.1;
            Int32 TaxAmount = 0;
            Int32 TotalAmountAfterTax = TotalAmountAfterDiscount - TaxAmount;
            Int32 PeriodNo = 1; //dummy       
            Boolean IsSetted = false;
            String FullName = userData.FullName;

            InsertCaseRequest request = new InsertCaseRequest();
            request.OrgID = OrgID;              /*semua request tambah kan orgid dan siteid*/
            request.SiteID = SiteID;
            request.CustomerType = entryheader.CustomerType;
            request.PSCode = PSCode;
            request.UnitCode = UnitCode;
            request.UnitNumber = UnitNo;
            request.NamaPelapor = NamaPelapor;
            request.TeleponPelapor = TeleponPelapor;
            request.EmailPelapor = EmailPelapor;
            request.MediaSource = CaseOriginID;
            request.Description = RequestDescription;
            //request.HelpName = find_crmhelpid(HelpID);
            request.HelpID = HelpID;
            request.TotalPrice = TotalAmount;
            request.ActualPrice = TotalAmountAfterDiscount;
            request.FullName = FullName;
            request.UserName = SavedBy;

            //String CRMCaseID = "";
            //String CaseNumber = "";
            //if (Request.QueryString["CaseNumber"] != null)
            //{
            //    DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
            //    request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //    CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //    CaseNumber = CaseDT.Rows[0]["CRMCaseID"].ToString();
            //}
            //else
            //{
            //    CRMCaseID = Guid.NewGuid().ToString();
            //    CaseNumber = CRMCaseID;
            //}

            /*if (Request.QueryString["CaseNumber"] != null)
            {
                DataTable CaseDT = dbclass.retrieve_Case(Request.QueryString["CaseNumber"].ToString());
                request.CRMCaseID = CaseDT.Rows[0]["CRMCaseID"].ToString();
            }*/

            //InsertCaseResponse crmcase = crmclass.InsertCase(request);
            //modified by bili jan 2018
            InsertCaseResponse dbcase = dbclass.InsertCase(request);

            if (dbcase.ExceptionMessage == null)
            {
                String CRMCaseID = dbcase.CaseID.ToString();
                String CaseNumber = dbcase.CaseNumber;

                String returnvalue = dbclass.save_car_rent_transaction(OrgID, SiteID, CRMCaseID, CaseNumber, 1, HelpID,
                    CaseOriginID, entryheader.CustomerType, entryheader.PaymentScheme, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DateTime.Now,
                    RequestDescription, OverdueDate, Int32.Parse(cboCarLicensePlate.SelectedValue), Int32.Parse(txt_jumlah.Text.ToString()),
                    DateTime.Parse(txtDate.Text.ToString()), txtPickTime.Text, txtUntil.Text, txtPickupLocation.Text, txtDestination.Text, Int32.Parse(cboTariff.SelectedValue), TotalAmount, DiscountTypeID, TotalDiscount, TotalAmountAfterDiscount,
                    TaxPercentage, TaxAmount, TotalAmountAfterTax, PeriodNo, IsSetted, SavedBy, FullName);

                if (returnvalue != "0")
                {
                    //crmclass.Delete_Case_Record(Guid.Parse(CRMCaseID));
                    dbclass.delete_case_record(CaseNumber);
                    return returnvalue;
                }
                else
                {
                    try
                    {
                        ////kirim email disini                    
                        DataTable dt_case = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt_case.Rows.Count > 0)
                        {
                            /*complaint send email ke customer dan pic*/
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt_case.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt_case.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt_case.Rows[0]["CaseStatusID"];

                            clsEmail.sendEmailToCustomer(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                            //jika case status assign ti pic kirim email ke pic head
                            if (helpdeskStatus == 2)
                                clsEmail.sendEmailToPICHead(mailServiceUrl, mailCategory, dt_case, emailFrom, displayEmailFrom, helpdeskStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        dbclass.Save_ErrorLog(ex, userData.UserName);
                    } 
                }
            }
            else
                return dbcase.ExceptionMessage;

            return null;
        }        

        private DataTable FetchCarRent(Int32 CarLicensePlateID, DateTime date)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_CarRent(CarLicensePlateID, date);

            return dt;
        }

        protected void cboCarLicensePlate_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime day = DateTime.Parse(txtDate.Text);
            txtDate.Text = day.Date.ToShortDateString();

            gvw_data.DataSource = FetchCarRent(Int32.Parse(cboCarLicensePlate.SelectedValue.ToString()), day.Date);
            gvw_data.DataBind();
        }

        protected void txt_jumlah_TextChanged(object sender, EventArgs e)
        {
            hitung_total();
        }

        protected void btn_back_Click(object sender, EventArgs e)
        {
            Response.Redirect(prevPage);
        }

        protected void calBanner_DayRender(object sender, DayRenderEventArgs e)
        {
            if (e.Day.Date < DateTime.Today)
            {
                e.Day.IsSelectable = false;
                e.Cell.Font.Strikeout = true;
            }
        }        
    }
}