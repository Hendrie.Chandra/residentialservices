﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace residential.web.forms
{
    public partial class tickethistory : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bound_tickethistory();
            }
        }

        private void bound_tickethistory()
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt = dbclass.retrieve_tickethistory(Request.QueryString["CaseNumber"]);

            gvw_tickethistory.DataSource = dt;
            gvw_tickethistory.DataBind();
        }       
    }
}