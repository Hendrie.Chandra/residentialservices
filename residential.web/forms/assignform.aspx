﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assignform.aspx.cs" Inherits="residential.web.forms.assignform" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Assign To</title>
    <link href="~/maincss.css" rel="stylesheet" type="text/css" />
</head>
<body class="bodypopup">
    <form id="form1" runat="server">
    <div class="assigncontainer">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div style="padding: 5px; width: 100%; text-align:center;">
                    <asp:Label ID="Label1" runat="server" Text="Assign To" Style="font-size: large; font-weight: 700"></asp:Label>
                    <asp:HiddenField ID="userid" runat="server" />
                </div>
                <div style="padding: 5px; width: 100%; text-align:center;">
                    <asp:DropDownList ID="cbo_assign" runat="server" CssClass="ComboBoxWindowsStyle" 
                        onselectedindexchanged="cbo_assign_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div style="padding: 5px; width: 100%;">
                    <asp:Button ID="btn_cancel" runat="server" Text="CANCEL" CssClass="button" Width="100px"
                        OnClick="btn_cancel_Click" />
                    <asp:Button ID="btn_assign" runat="server" Text="UPDATE" CssClass="button" Width="100px"
                        OnClick="btn_assign_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
