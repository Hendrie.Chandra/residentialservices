﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace residential.web.Classes
{
    public class clsMasterOtherIncome
    {
        public static DataTable ViewMasterHelpPriceAll(string Search, int HelpID, int OrgID, int SiteID, int DepartmentID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetMSHelpPrice";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("Search", Search));
                    cmd.Parameters.Add(new SqlParameter("HelpID", HelpID));
                    cmd.Parameters.Add(new SqlParameter("DepartmentID", DepartmentID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable GetDepartment(int OrgID, int SiteID, string UserName)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetDepartmentList";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("UserName", UserName));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable GetHelpName(int OrgID, int SiteID, int DepartmentID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetHelpNameList";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("DepartmentID", DepartmentID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable ListItemUnit()
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spMSHCUItemUnitGet";
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable ListPaymentMethod()
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSelectPaymentSchemeCommon";
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static string UpdateData(int OrgID, int SiteID, int HelpItemID, decimal Price,  int Active, string User)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spUpdateMSHelpPrice";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("HelpItemID", HelpItemID));
                    cmd.Parameters.Add(new SqlParameter("Price", Price));
                    cmd.Parameters.Add(new SqlParameter("Active", Active));
                    cmd.Parameters.Add(new SqlParameter("User", User));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data.Rows[0]["Result"].ToString();
            }
        }

        public static string InsertData(int OrgID, int SiteID, int HelpID, string HelpItemName, decimal Price, int Active, string User)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spInsertMSHelpPrice";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("HelpID", HelpID));
                    cmd.Parameters.Add(new SqlParameter("HelpItemName", HelpItemName));
                    cmd.Parameters.Add(new SqlParameter("Price", Price));
                    cmd.Parameters.Add(new SqlParameter("Active", Active));
                    cmd.Parameters.Add(new SqlParameter("User", User));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data.Rows[0]["Result"].ToString();
            }
        }

    }
}