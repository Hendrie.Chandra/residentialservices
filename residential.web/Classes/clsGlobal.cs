﻿using residential.libs;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace residential.web.Classes
{
    public class clsGlobal
    {
        public static string GenerateCaseNumber(int siteID)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_numbering(1, siteID);

            int numbering = 0;
            if (dt.Rows.Count > 0)
            {
                numbering = int.Parse(dt.Rows[0]["NewNumber"].ToString());
            }
            
            //CAS-07275-F6Y7X5
            string randomString = RandomString(6);
            return (siteID.ToString("000") +"-"+ numbering.ToString("00000") +"-"+ randomString).ToUpper();

        }

        public static string RandomString(int size)
        {
            string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random rand = new Random();
            char[] chars = new char[size];
            for (int i = 0; i < size; i++)
            {
                chars[i] = Alphabet[rand.Next(Alphabet.Length)];
            }
            return new string(chars);
        }

        private static Boolean IsWeekend(DateTime _date)
        {
            return _date.DayOfWeek == DayOfWeek.Saturday || _date.DayOfWeek == DayOfWeek.Sunday;
        }

        public static DateTime GetOverdueDate(int _Duration)
        {
            DateTime StartDate = DateTime.Now;
            DateTime CurrentDate = StartDate;
            DateTime EndDate = DateTime.Now.AddDays(_Duration);

            for (int i = 1; i <= _Duration; i++)
            {
                if (IsWeekend(CurrentDate))
                {
                    EndDate = EndDate.AddDays(1);
                    _Duration++;
                }
                CurrentDate = CurrentDate.AddDays(1);
            }

            return EndDate;
        }
    }
}