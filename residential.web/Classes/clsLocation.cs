﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace residential.web.Classes
{
    public static class clsLocation
    {
        public static DataTable GetLocation(int orgID, int siteID, int locationID, int subLocationID)
        {
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "Start Method", string.Empty, Helper.GetUserName());
            DataTable data = new DataTable();

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPLocation_Get";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", orgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", siteID));
                    cmd.Parameters.Add(new SqlParameter("LocationID", locationID));
                    cmd.Parameters.Add(new SqlParameter("SubLocationID", subLocationID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    //LKLog.Write(logID, "Helper.cs", "GetSettings", ex.StackTrace, ex.Message, Helper.GetUserName());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "End Method", string.Empty, Helper.GetUserName());
            return data;
        }
    }
}