﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.Classes
{
    public static class Constants
    {
        public const string TMDBConnection = "residentialdev";

        public const string SessionOrgID = "OrgID";
        public const string SessionSiteID = "SiteID";
        public const string SessionUser = "User";

        public const int OrganizationID = 1;
    }
}