﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Linq;

namespace residential.web.Classes
{
    public class clsSecurityDeposit
    {
        public static DataTable RetreiveData(string CaseNumber, int OrgID, int SiteID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetSecurityDepositHelpID";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("CaseNumber", CaseNumber));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static String SaveSecurityDeposit(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description, DateTime? _OverdueDate,
            String _SavedBy, String _FullName, string RelatedCaseNumber, string Amount
            )
        {
            try
            {

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString());
                SqlCommand cmd = new SqlCommand("SPSaveUpdatePengembalianSecDep", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);
                cmd.Parameters.AddWithValue("@RelatedCaseNumber", RelatedCaseNumber);
                cmd.Parameters.AddWithValue("@Amount", Amount);
                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}