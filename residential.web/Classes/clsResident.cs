﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace residential.web.Classes
{
    public class clsResident
    {
        public static DataTable ResidentList(int OrgID, int SiteID, string ClusterCode, string UnitCode, string Search)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetResidentDataList";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("ClusterCode", ClusterCode));
                    cmd.Parameters.Add(new SqlParameter("UnitCode", UnitCode));
                    cmd.Parameters.Add(new SqlParameter("Search", Search));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable ClusterList(int OrgID, int SiteID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetClusterList";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable UnitCodeList(int OrgID, int SiteID, string ClusterCode)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetUnitCodeList";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("ClusterCode", ClusterCode));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable ResidentStatus()
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spRetreiveMSResidentStatus";
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable Nationality()
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spRetreiveNationality";
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataSet ResidentDetail(int ResidentHeaderID)
        {
            DataSet data = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spRetreiveResidentDetail";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("ResidentHeaderID", ResidentHeaderID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataSet ResidentCard(int ResidentHeaderID)
        {
            DataSet data = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spRetreiveResidentCard";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("ResidentHeaderID", ResidentHeaderID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static string InsertResidentDetail(int ResidentHeaderID, string Name, DateTime BirthDate, string Gender, int ResidentStatusID,
            string NationalityID, int isActive, string UserName)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSaveResidentDetail";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("ResidentHeaderID", ResidentHeaderID));
                    cmd.Parameters.Add(new SqlParameter("Name", Name));
                    cmd.Parameters.Add(new SqlParameter("BirthDate", BirthDate));
                    cmd.Parameters.Add(new SqlParameter("Gender", Gender));
                    cmd.Parameters.Add(new SqlParameter("ResidentStatusID", ResidentStatusID));
                    cmd.Parameters.Add(new SqlParameter("NationalityID", NationalityID));
                    cmd.Parameters.Add(new SqlParameter("isActive", isActive));
                    cmd.Parameters.Add(new SqlParameter("UserName", UserName));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data.Rows[0]["Result"].ToString();
            }
        }

        public static string UpdateResidentDetail(int ResidentHeaderID, string Name, DateTime BirthDate, string Gender, int ResidentStatusID,
            string NationalityID, int isActive, string UserName, int ResidentDetailID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spUpdateResidentDetail";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("ResidentHeaderID", ResidentHeaderID));
                    cmd.Parameters.Add(new SqlParameter("Name", Name));
                    cmd.Parameters.Add(new SqlParameter("BirthDate", BirthDate));
                    cmd.Parameters.Add(new SqlParameter("Gender", Gender));
                    cmd.Parameters.Add(new SqlParameter("ResidentStatusID", ResidentStatusID));
                    cmd.Parameters.Add(new SqlParameter("NationalityID", NationalityID));
                    cmd.Parameters.Add(new SqlParameter("isActive", isActive));
                    cmd.Parameters.Add(new SqlParameter("UserName", UserName));
                    cmd.Parameters.Add(new SqlParameter("ResidentDetailID", ResidentDetailID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data.Rows[0]["Result"].ToString();
            }
        }

        public static DataTable CardType(int OrgID, int SiteID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spRetreiveMSCardType";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static string InsertResidentCard(int ResidentHeaderID, int ResidentDetailID, string Name, string Remarks, string CardNo, string SNCard,
            DateTime ExpiredDate, int isActive, string UserName, int EnableExpired, string DesignTypeCode, string PhotoID, int isPrinted)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSaveResidentCard";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("ResidentHeaderID", ResidentHeaderID));
                    cmd.Parameters.Add(new SqlParameter("ResidentDetailID", ResidentDetailID));
                    cmd.Parameters.Add(new SqlParameter("Name", Name));
                    cmd.Parameters.Add(new SqlParameter("Remarks", Remarks));
                    cmd.Parameters.Add(new SqlParameter("CardNo", CardNo));
                    cmd.Parameters.Add(new SqlParameter("SNCard", SNCard));
                    cmd.Parameters.Add(new SqlParameter("ExpiredDate", ExpiredDate));
                    cmd.Parameters.Add(new SqlParameter("EnableExpired", EnableExpired));
                    cmd.Parameters.Add(new SqlParameter("DesignTypeCode", DesignTypeCode));
                    cmd.Parameters.Add(new SqlParameter("PhotoID", PhotoID));
                    cmd.Parameters.Add(new SqlParameter("isPrinted", isPrinted));
                    cmd.Parameters.Add(new SqlParameter("isActive", isActive));
                    cmd.Parameters.Add(new SqlParameter("UserName", UserName));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data.Rows[0]["Result"].ToString();
            }
        }

        public static string UpdateResidentCard(int ResidentHeaderID, string Name, string Remarks, string CardNo, string SNCard,
            DateTime ExpiredDate, int isActive, string UserName, int EnableExpired, string DesignTypeCode, string PhotoID, int isPrinted, int ResidentCardID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spUpdateResidentCard";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("ResidentHeaderID", ResidentHeaderID));
                    cmd.Parameters.Add(new SqlParameter("Name", Name));
                    cmd.Parameters.Add(new SqlParameter("Remarks", Remarks));
                    cmd.Parameters.Add(new SqlParameter("CardNo", CardNo));
                    cmd.Parameters.Add(new SqlParameter("SNCard", SNCard));
                    cmd.Parameters.Add(new SqlParameter("ExpiredDate", ExpiredDate));
                    cmd.Parameters.Add(new SqlParameter("EnableExpired", EnableExpired));
                    cmd.Parameters.Add(new SqlParameter("DesignTypeCode", DesignTypeCode));
                    cmd.Parameters.Add(new SqlParameter("PhotoID", PhotoID));
                    cmd.Parameters.Add(new SqlParameter("isPrinted", isPrinted));
                    cmd.Parameters.Add(new SqlParameter("isActive", isActive));
                    cmd.Parameters.Add(new SqlParameter("UserName", UserName));
                    cmd.Parameters.Add(new SqlParameter("ResidentCardID", ResidentCardID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data.Rows[0]["Result"].ToString();
            }
        }

        //add by bili januari 2018
        public static DataTable ResidentData(int ResidentHeaderID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "splk_retrieve_resident_detail";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("ResidentHeaderID", ResidentHeaderID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }
    }
}