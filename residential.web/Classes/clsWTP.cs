﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace residential.web.Classes
{
    public static class clsWTP
    {
        public static void UploadBillingComponentStatus(int orgID, int siteID, int transactionID, string data)
        {
            //LKLog.Write(logID, "clsBillingComponent.cs", "UploadBillingComponentStatus", "Start Method", string.Empty, Helper.GetUserName());

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPMSBillingComponent_UploadStatus";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", orgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", siteID));
                    cmd.Parameters.Add(new SqlParameter("TransactionID", transactionID));
                    cmd.Parameters.Add(new SqlParameter("Data", data));
                    cmd.Parameters.Add(new SqlParameter("Username", Helper.GetUserName()));
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    //LKLog.Write(logID, "clsBillingComponent.cs", "UploadBillingComponentStatus", ex.StackTrace, ex.Message, Helper.GetUserName());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            //LKLog.Write(logID, "clsBillingComponent.cs", "UploadBillingComponentStatus", "End Method", string.Empty, Helper.GetUserName());
        }
    }
}