﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace residential.web.Classes
{
    public static class clsUser
    {
        public static DataSet GetUserAccessByUsername(string username)
        {
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "Start Method", string.Empty, Helper.GetUserName());
            DataSet data = new DataSet();

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPUserAccess_GetByUsername";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("Username", username));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    //LKLog.Write(logID, "Helper.cs", "GetSettings", ex.StackTrace, ex.Message, Helper.GetUserName());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "End Method", string.Empty, Helper.GetUserName());
            return data;
        }

        public static DataTable GetUser(int orgID, int siteID, int userID, string username)
        {
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "Start Method", string.Empty, Helper.GetUserName());
            DataTable data = new DataTable();

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPMSUser_Get";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", orgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", siteID));
                    cmd.Parameters.Add(new SqlParameter("UserID", userID));
                    cmd.Parameters.Add(new SqlParameter("Username", username));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    //LKLog.Write(logID, "Helper.cs", "GetSettings", ex.StackTrace, ex.Message, Helper.GetUserName());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "End Method", string.Empty, Helper.GetUserName());
            return data;
        }

        public static DataTable ValidateOldDepartment(int userID)
        {
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "Start Method", string.Empty, Helper.GetUserName());
            DataTable data = new DataTable();

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPMSUser_ValidateOldDepartment";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("UserID", userID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    //LKLog.Write(logID, "Helper.cs", "GetSettings", ex.StackTrace, ex.Message, Helper.GetUserName());
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
            }
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "End Method", string.Empty, Helper.GetUserName());
            return data;
        }

        public static DataTable GetDefaultPICHead(int orgID, int siteID, int helpID)
        {
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "Start Method", string.Empty, Helper.GetUserName());
            DataTable data = new DataTable();

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPPICHead_GET";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", orgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", siteID));
                    cmd.Parameters.Add(new SqlParameter("HelpID", helpID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    //LKLog.Write(logID, "Helper.cs", "GetSettings", ex.StackTrace, ex.Message, Helper.GetUserName());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "End Method", string.Empty, Helper.GetUserName());
            return data;
        }

        public static DataTable GetDefaultPICHead(int orgID, int siteID, int helpID, int subLocationID)
        {
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "Start Method", string.Empty, Helper.GetUserName());
            DataTable data = new DataTable();

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPPICHead_GET";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", orgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", siteID));
                    cmd.Parameters.Add(new SqlParameter("HelpID", helpID));
                    cmd.Parameters.Add(new SqlParameter("SubLocationID", subLocationID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    //LKLog.Write(logID, "Helper.cs", "GetSettings", ex.StackTrace, ex.Message, Helper.GetUserName());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "End Method", string.Empty, Helper.GetUserName());
            return data;
        }
    }
}