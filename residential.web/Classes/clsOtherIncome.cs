﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Linq;

namespace residential.web.Classes
{
    public class clsOtherIncome
    {
        public static DataTable ViewMasterHelpPrice(string DeptCode, int OrgID, int SiteID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetMsHelpPriceByDept";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("DeptCode", DeptCode));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable CheckTransactionID(int HelpID, int OrgID, int SiteID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPCheckTransactionID";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("HelpID", HelpID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable ApprovalUpdateTRRevenue(int OrgID, int SiteID, string UserName, string CaseNumber)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPTRRevenueUpdateIsActive";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("UserName", UserName));
                    cmd.Parameters.Add(new SqlParameter("CaseNumber", CaseNumber));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable CheckIsNeedApprovalUser(int OrgID, int SiteID, string UserName, string DeptCode)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPCheckIsNeedApprovalUser";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("UserName", UserName));
                    cmd.Parameters.Add(new SqlParameter("DeptCode", DeptCode));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataSet ViewCaseList(string DeptCode, int OrgID, int SiteID, string Search, int CaseStatusID)
        {
            DataSet data = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spGetOtherIncomeListByDepartment";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("DeptCode", DeptCode));
                    cmd.Parameters.Add(new SqlParameter("Search", Search));
                    cmd.Parameters.Add(new SqlParameter("CaseStatusID", CaseStatusID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable PaymentSchemeByHelpPrice(int OrgID, int SiteID, int HelpItemID)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSelectPaymentSchemeByHelpPriceID";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("HelpItemID", HelpItemID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataSet HelpNameList(int OrgID, int SiteID, string DeptCode)
        {
            DataSet data = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSelectHelpNameOtherIncome";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("DeptCode", DeptCode));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataSet HelpNameItemList(int OrgID, int SiteID, int HelpID)
        {
            DataSet data = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSelectOtherIncomeItem";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("HelpID", HelpID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }
        
        public static DataSet OtherIncomeDetail(int OrgID, int SiteID, string CaseNumber)
        {
            DataSet data = new DataSet();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPGetOtherIncomeDetail";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("CaseNumber", CaseNumber));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static String SaveOtherIncome(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description,DateTime? _OverdueDate,
            String _SavedBy, String _FullName, decimal Amount, decimal ActualAmount, DataTable data
            )
        {
            try
            {
                string DataConvert = ConvertDataTableToXML(data);

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString());
                SqlCommand cmd = new SqlCommand("SPSaveUpdateOtherIncome", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);               
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);              //add by bili januari 2018
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);
                cmd.Parameters.AddWithValue("@Amount", Amount);
                cmd.Parameters.AddWithValue("@ActualAmount", ActualAmount);
                cmd.Parameters.AddWithValue("@DataConvert", DataConvert);
                

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static string ConvertDataTableToXML(DataTable data)
        {
            XDocument doc = new XDocument(new XDeclaration("1.0", "UTF-8", "yes"),
                new XElement("root",
                    from p in data.AsEnumerable()
                    select new XElement("row",
                        new XAttribute("HelpItemID", p["HelpItemID"]),
                        new XAttribute("Quantity", p["Quantity"]),
                        new XAttribute("Amount", p["Amount"]),
                        new XAttribute("TotalAmount", p["TotalAmount"]))));

            return doc.ToString();
        }
    }
}

