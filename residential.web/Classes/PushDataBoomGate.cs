﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace residential.web.Classes
{
    public class PushDataBoomGate
    {
        public static void PushData(string _Boom, string _Mifare, string _Nama, string _Alamat, string _Status)
        {
            String boomgateconstr = ConfigurationManager.ConnectionStrings["boomgateconstr"].ToString();
            FbConnection bgconn = new FbConnection(boomgateconstr);
            bgconn.Open();

            FbTransaction trans = bgconn.BeginTransaction();

            String cmdsql = "INSERT INTO PushData (Boom, Mifare, Nama, Alamat, Status) VALUES ('" + _Boom + "', '" + _Mifare + "', '" + _Nama + "', '" + _Alamat + "', '" + _Status + "')";
            FbCommand cmd = new FbCommand(cmdsql, bgconn, trans);
            cmd.ExecuteNonQuery();
            trans.Commit();

            bgconn.Close();
        }

        public static void PushData(string _Boom, string _Mifare, string _Nama, string _Alamat, string _Status, string _Jenis, string _CardNo, DateTime? _Kadaluarsa, string _PSCode,  DateTime? _Issued)
        {


            String boomgateconstr = ConfigurationManager.ConnectionStrings["boomgateconstr"].ToString();
            FbConnection bgconn = new FbConnection(boomgateconstr);
            bgconn.Open();

            FbTransaction trans = bgconn.BeginTransaction();

            string dt_str = "";
            string cmdsql = "";


            if (_Jenis == "V" || _Jenis == "v")
            {
                //cmdsql = "INSERT INTO PushData (Boom, Mifare, Status, Jenis, CardNo, Issued) VALUES ('" + _Boom + "', '" + _Mifare + "', '" + _Status + "', '" + _Jenis + "', '" + _CardNo + "', '" + _Issued + "')";
                cmdsql = "INSERT INTO PushData (Boom, Mifare, Status, Jenis, CardNo, Issued) VALUES ('" + _Boom + "', '" + _Mifare + "', '" + _Status + "', '" + _Jenis + "', '" + _CardNo + "', current_timestamp)";
            }
            else
            {
                if (_Kadaluarsa == null)
                {
                    //cmdsql = "INSERT INTO PushData (Boom, Mifare, Nama, Alamat, Status, Jenis, CardNo, Kadaluarsa, Pscode, Issued) VALUES ('" + _Boom + "', '" + _Mifare + "', '" + _Nama + "', '" + _Alamat + "', '" + _Status + "', '" + _Jenis + "', '" + _CardNo + "', null, '" + _PSCode + "', '" + _Issued + "')";
                    cmdsql = "INSERT INTO PushData (Boom, Mifare, Nama, Alamat, Status, Jenis, CardNo, Kadaluarsa, Pscode, Issued) VALUES ('" + _Boom + "', '" + _Mifare + "', '" + _Nama + "', '" + _Alamat + "', '" + _Status + "', '" + _Jenis + "', '" + _CardNo + "', null, '" + _PSCode + "', current_timestamp)";
                }
                else
                {
                    dt_str = Convert.ToDateTime(_Kadaluarsa).ToString("yyyy-MM-dd");
                    //cmdsql = "INSERT INTO PushData (Boom, Mifare, Nama, Alamat, Status, Jenis, CardNo, Kadaluarsa, Pscode, Issued) VALUES ('" + _Boom + "', '" + _Mifare + "', '" + _Nama + "', '" + _Alamat + "', '" + _Status + "', '" + _Jenis + "', '" + _CardNo + "', '" + dt_str + "','" + _PSCode + "', '" + _Issued + "')";
                    cmdsql = "INSERT INTO PushData (Boom, Mifare, Nama, Alamat, Status, Jenis, CardNo, Kadaluarsa, Pscode, Issued) VALUES ('" + _Boom + "', '" + _Mifare + "', '" + _Nama + "', '" + _Alamat + "', '" + _Status + "', '" + _Jenis + "', '" + _CardNo + "', '" + dt_str + "','" + _PSCode + "', current_timestamp)";
                }
            }

            
            FbCommand cmd = new FbCommand(cmdsql, bgconn, trans);
            cmd.ExecuteNonQuery();
            trans.Commit();

            bgconn.Close();
        }
    }
}