﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace residential.web.Classes
{
    public class clsSpecialRequest
    {
        public static DataTable SearchCaseByDept(string DepartmentCode, int OrgID, int SiteID, int CaseStatusID, string Search)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SP_GetCaseListByDepartment";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("DepartmentCode", DepartmentCode));
                    cmd.Parameters.Add(new SqlParameter("Search", Search));
                    cmd.Parameters.Add(new SqlParameter("CaseStatusID", CaseStatusID));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable HelpNameListByDepartment(int OrgID, int SiteID, string DeptCode)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSelectHelpNameByDepartment";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("DeptCode", DeptCode));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static String SaveSpecialRequest(
            Int32 _OrgID, Int32 _SiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, Int32 _HelpID,
            Int32 _CaseOriginID, Boolean _CustomerType, String _PaymentSchemeID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo, String _NamaPelapor,
            String _TeleponPelapor, String _EmailPelapor, DateTime _RequestDate, String _Description,
            String _SavedBy, String _FullName, decimal Amount, decimal ActualAmount
            )
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString());
                SqlCommand cmd = new SqlCommand("SPSaveUpdateSpecialRequest", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@HelpID", _HelpID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@CustomerType", _CustomerType);
                cmd.Parameters.AddWithValue("@PaymentSchemeID", _PaymentSchemeID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@RequestDate", _RequestDate);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@FullName", _FullName);
                cmd.Parameters.AddWithValue("@Amount", Amount);
                cmd.Parameters.AddWithValue("@ActualAmount", ActualAmount);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public static DataTable SpecialRequestDetail(int OrgID, int SiteID, string CaseNumber)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPGetSpecialRequestDetail";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", OrgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", SiteID));
                    cmd.Parameters.Add(new SqlParameter("CaseNumber", CaseNumber));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

    }
}