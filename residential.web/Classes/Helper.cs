﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Security.Principal;
using System.Web.Security;
using System.Threading;
using System.Configuration;
using LKC = LKCommon;

namespace residential.web.Classes
{
    public static class Helper
    {
        public static string GetUserName()
        {
            //return HttpContext.Current.User.Identity.Name;
            return "vsndev\\fadel";
        }

        public static string GetTMDBConnection()
        {
            return ConfigurationManager.ConnectionStrings[Constants.TMDBConnection].ToString();
        }

        public static int OrgID
        {
            get
            {   
                if (HttpContext.Current.Session[Constants.SessionOrgID] == null)
                {
                    HttpContext.Current.Session[Constants.SessionOrgID] = Constants.OrganizationID;
                }
                //else
                //{
                //    logID = LKLog.GetNewLogID(Constants.AppID, Helper.GetUserName());
                //}
                return LKC.CommonFunctions.StringToInt(HttpContext.Current.Session[Constants.SessionOrgID].ToString());
            }
        }

        public static int SiteID
        {
            get
            {
                int siteID = 0;

                if (HttpContext.Current.Session[Constants.SessionSiteID] == null)
                {

                    if (HttpContext.Current.Session[Constants.SessionUser] == null)
                    {
                        HttpContext.Current.Session[Constants.SessionUser] = clsUser.GetUserAccessByUsername(Helper.GetUserName());
                    }

                    DataSet dtData = (DataSet)HttpContext.Current.Session[Constants.SessionUser];
                    DataTable dtAccessibleSites = dtData.Tables[0];
                    if (dtAccessibleSites.Rows.Count > 0)
                    {
                        siteID = LKC.CommonFunctions.StringToInt(dtAccessibleSites.Rows[0]["DefaultSiteID"].ToString());
                    }

                    if (siteID == 0)
                    {
                        throw new Exception("User not authorized");
                    }
                }
                else
                {
                    siteID = LKC.CommonFunctions.StringToInt(HttpContext.Current.Session[Constants.SessionSiteID].ToString());
                }

                return siteID;
            }
        }

        public static DataTable GetTasklist(int orgID, int siteID, int caseStatusID, string search)
        {
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "Start Method", string.Empty, Helper.GetUserName());
            DataTable data = new DataTable();

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SPTasklist_Get";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", orgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", siteID));
                    cmd.Parameters.Add(new SqlParameter("CaseStatusID", caseStatusID));
                    cmd.Parameters.Add(new SqlParameter("Search", search));
                    cmd.Parameters.Add(new SqlParameter("Username", Helper.GetUserName()));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    //LKLog.Write(logID, "Helper.cs", "GetSettings", ex.StackTrace, ex.Message, Helper.GetUserName());
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            //LKLog.Write(logID, "Helper.cs", "GetSettings", "End Method", string.Empty, Helper.GetUserName());
            return data;
        }

        public static DataTable GetSettings(int orgID, int siteID, string settingName, string settingCategory, string settingValue)
        {
            DataTable data = new DataTable();

            using (SqlConnection conn = new SqlConnection(Helper.GetTMDBConnection()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spMSSettings_Get";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("OrgID", orgID));
                    cmd.Parameters.Add(new SqlParameter("SiteID", siteID));
                    cmd.Parameters.Add(new SqlParameter("SettingName", settingName));
                    cmd.Parameters.Add(new SqlParameter("SettingCategory", settingCategory));
                    cmd.Parameters.Add(new SqlParameter("SettingValue", settingValue));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }
            }
            return data;
        }
    }
}