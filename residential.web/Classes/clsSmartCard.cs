﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace residential.web.Classes
{
    public class clsSmartCard
    {
        public static DataTable RetrieveDestinationMargeBoomGate(string _sourceMargeBoomGateCode) 
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SP_Retrieve_Marge_BoomGateCode";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("BoomGateCode", _sourceMargeBoomGateCode));
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable RetrieveUploadType()
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SP_Retrieve_Upload_Type";
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static DataTable save_uplmssmartcard_vvip(int _OrgID, int _SiteID, String _CardNumber, String _ChipNumber, string _PSCode, String _CardStatus, bool _IsActive, string _savedBy, int _uploadType)
        {
            DataTable data = new DataTable();
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString()))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SP_Save_UplMSSmartCardVVIP";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                    cmd.Parameters.AddWithValue("@SiteID", _SiteID);
                    cmd.Parameters.AddWithValue("@CardNumber", _CardNumber);
                    cmd.Parameters.AddWithValue("@ChipNumber", _ChipNumber);
                    cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                    cmd.Parameters.AddWithValue("@CardStatus", _CardStatus);
                    cmd.Parameters.AddWithValue("@IsActive", _IsActive);
                    cmd.Parameters.AddWithValue("@CreatedBy", _savedBy);
                    cmd.Parameters.AddWithValue("@UploadType", _uploadType);
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(data);
                    oAdapter.Dispose();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    conn.Close();
                }
                return data;
            }
        }

        public static Int32 save_uplmssmartcard_nonresident(int _OrgID, int _SiteID, String _CardNumber, String _ChipNumber, string _PSCode, String _CardStatus, bool _IsActive, string _savedBy, int _uploadType)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["residentialdev"].ToString());
            SqlCommand cmd = new SqlCommand("SP_Save_UplMSSmartCardNonResident", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CardNumber", _CardNumber);
            cmd.Parameters.AddWithValue("@ChipNumber", _ChipNumber);
            cmd.Parameters.AddWithValue("@PSCode", _PSCode);
            cmd.Parameters.AddWithValue("@CardStatus", _CardStatus);
            cmd.Parameters.AddWithValue("@IsActive", _IsActive);
            cmd.Parameters.AddWithValue("@CreatedBy", _savedBy);
            cmd.Parameters.AddWithValue("@UploadType", _uploadType);

            cmd.Parameters.Add("@NewID", SqlDbType.Int).Direction = ParameterDirection.Output;
            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            Int32 returnvalue = (Int32)cmd.Parameters["@NewID"].Value;

            return returnvalue;
        }
    }
}