﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.model
{
    public class SmartcardTransaction
    {

        public int SmartCardID { get; set; }
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        public int BoomGateID { get; set; }
        public string BoomGateName { get; set; }

        public SmartcardTransaction() { }
        
        public SmartcardTransaction(int SmartCardID, string CardNumber, string ChipNumber, int BoomGateID, string BoomGateName) 
        {
            this.SmartCardID = SmartCardID;
            this.CardNumber = CardNumber;
            this.ChipNumber = ChipNumber;
            this.BoomGateID = BoomGateID;
            this.BoomGateName = BoomGateName;
        }

    }
}