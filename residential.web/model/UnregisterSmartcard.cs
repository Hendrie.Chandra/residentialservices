﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.model
{
    public class UnregisterSmartcard
    {
        public int SmartCardDetailID { get; set; }
        public string ChipNumber { get; set; }
        public string CardNumber { get; set; }
        public string HolderName { get; set; }
        public int BoomGateID { get; set; }
        public bool IsActive { get; set; }
        

        public UnregisterSmartcard() { }

        public UnregisterSmartcard(int SmartCardDetailID, string ChipNumber, string CardNumber, string HolderName, int BoomGateID, bool IsActive)
        {
            this.SmartCardDetailID = SmartCardDetailID;
            this.ChipNumber = ChipNumber;
            this.CardNumber = CardNumber;
            this.HolderName = HolderName;
            this.BoomGateID = BoomGateID;
            this.IsActive = IsActive;
        }
    }

    public class TempCardNumber
    {
        public int MembershipID { get; set; }
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        public TempCardNumber() { }

        public TempCardNumber(int MembershipID)
        {
            this.MembershipID = MembershipID;
        }

        public TempCardNumber(string CardNumber)
        {
            this.CardNumber = CardNumber;
        }

        public TempCardNumber(string CardNumber, string ChipNumber)
        {
            this.CardNumber = CardNumber;
            this.ChipNumber = ChipNumber;
        }

    }

    public class AutoUnregiterSmartcard
    {
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        public int MembershipID { get; set; }
        public int BoomGateID { get; set; }
        public string BoomGateCode { get; set; }
        public string HolderName { get; set; }

        public AutoUnregiterSmartcard() { }

        public AutoUnregiterSmartcard(string CardNumber, string ChipNumber, int MembershipID, int BoomGateID, string BoomGateCode, string HolderName)
        {
            this.CardNumber = CardNumber;
            this.ChipNumber = ChipNumber;
            this.MembershipID = MembershipID;
            this.BoomGateID = BoomGateID;
            this.BoomGateCode = BoomGateCode;
            this.HolderName = HolderName;
        }
    }
}