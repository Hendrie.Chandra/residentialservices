﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.model
{
    public class UploadMSSmartCard
    {
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        public string CardStatus { get; set; }
        public string BoomGateCode { get; set; } // new format, upload to boomgate
        public bool IsActive { get; set; }
    }

    public class UploadMSSmartCardVisitor
    {
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        public string CardStatus { get; set; }
        public string BoomGateCode { get; set; }
        public bool IsActive { get; set; }
        public string UploadType { get; set; } // new request, upload kartu msater & Member, langsung ke boomgate
    }

}