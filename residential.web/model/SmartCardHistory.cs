﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.model
{
    public class SmartCardHistory
    {
        public int OrgID { get; set; }
        public int SiteID { get; set; }
        public int SmartCardID { get; set; }
        public int SmartCardDetailID { get; set; }
        public int? CardTypeID { get; set; }
        public string CaseNumber { get; set; }
        public int TransactionType { get; set; }
        public string TransactionTypeDescription { get; set; }
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        public string HolderName { get; set; }
        public string VehicleNumber { get; set; }
        public int BoomGateID { get; set; }
        public string BoomGateName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        //public bool IsActive { get; set; }

        public SmartCardHistory() { }

        public SmartCardHistory(int OrgID, int SiteID, int SmartCardID, int SmartCardDetailID, int? CardTypeID, string CaseNumber, int TransactionType, string TransactionTypeDescription, string CardNumber, string ChipNumber, string HolderName, string VehicleNumber, int BoomGateID, string BoomGateName, DateTime CreatedDate, string CreatedBy)
        {
            this.OrgID = OrgID;
            this.SiteID = SiteID;
            this.SmartCardID = SmartCardID;
            this.SmartCardDetailID = SmartCardDetailID;
            this.CardTypeID = CardTypeID;
            this.CaseNumber = CaseNumber;
            this.TransactionType = TransactionType;
            this.TransactionTypeDescription = TransactionTypeDescription;
            this.CardNumber = CardNumber;
            this.ChipNumber = ChipNumber;
            this.HolderName = HolderName;
            this.VehicleNumber = VehicleNumber;
            this.BoomGateID = BoomGateID;
            this.BoomGateName = BoomGateName;
            this.CreatedDate = CreatedDate;
            this.CreatedBy = CreatedBy;
            //this.IsActive = IsActive;
        }
    }

    public class SmartCardHistoryByUnit
    {
        public string PSCode { get; set; }
        public string CardNumber { get; set; }
        public int CardTypeID { get; set; }
        public string HolderName { get; set; }
        public string Unit { get; set; }
        public string BoomGateCode { get; set; }
        public string BoomGateName { get; set; }
        public string CreatedBy { get; set; }

        public int Status { get; set; }

        public SmartCardHistoryByUnit() { }

        public SmartCardHistoryByUnit(string PSCode, string CardNumber, int CardTypeID, string HolderName, string Unit, string BoomGateName, string BoomGateCode, int Status, string CreatedBy) 
        {
            this.PSCode = PSCode;
            this.CardNumber = CardNumber;
            this.CardTypeID = CardTypeID;
            this.HolderName = HolderName;
            this.Unit = Unit;
            this.BoomGateName = BoomGateName;
            this.BoomGateCode = BoomGateCode;
            this.Status = Status;
        }
    }
}