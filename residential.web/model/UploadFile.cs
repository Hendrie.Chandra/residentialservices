﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.model
{
    public class UploadFile
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
        public byte[] FileData { get; set; }

        public UploadFile() { }

        public UploadFile(string FileName, string FileType, byte[] FileData)
        {
            this.FileName = FileName;
            this.FileType = FileType;
            this.FileData = FileData;
        }
    }
}