﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.model
{
    public class MasterData
    {

    }

    public class SmartCard
    {
        public string CardNumber { get; set; }

        public SmartCard() { }

        public SmartCard(string CardNumber)
        {
            this.CardNumber = CardNumber;
        }
    }

    public class VehicleType
    {
        public int VehicleTypeID { get; set; }
        public string VehicleTypeName { get; set; }

        public VehicleType() { }

        public VehicleType(int VehicleTypeID, string VehicleTypeName)
        {
            this.VehicleTypeID = VehicleTypeID;
            this.VehicleTypeName = VehicleTypeName;
        }
    }

    #region resident status
    public class ResidentStatus
    {
        public int ResidentStatusID { get; set; }
        public string ResidentStatusName { get; set; }

        public ResidentStatus(int ResidentStatusID, string ResidentStatusName)
        {
            this.ResidentStatusID = ResidentStatusID;
            this.ResidentStatusName = ResidentStatusName;
        }
    }
    #endregion resident status

    #region card type
    public class CardType
    {
        int CardTypeID { get; set; }
        string CardTypeName { get; set; }

        public CardType(int CardTypeID, string CardTypeName)
        {
            this.CardTypeID = CardTypeID;
            this.CardTypeName = CardTypeName;
        }

    }
    #endregion cardtype

    #region validity periode
    public class MasterValidityPeriode
    {
        int ValidityPeriodeId { get; set; }
        int ValidityPeriode { get; set; }
        string ValidityPeriodeDescription { get; set; }

        public MasterValidityPeriode() { }

        public MasterValidityPeriode(int ValidityPeriodeId, int ValidityPeriode, string ValidityPeriodeDescription)
        {
            this.ValidityPeriodeId = ValidityPeriodeId;
            this.ValidityPeriode = ValidityPeriode;
            this.ValidityPeriodeDescription = ValidityPeriodeDescription;
        }

    }
    #endregion validity periode

    #region quantity
    public class MasterQuantity
    {
        int QuantityId { get; set; }
        int Quantity { get; set; }
    }
    #endregion quantity

}