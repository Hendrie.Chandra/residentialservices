﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.model
{
    public class CardDetail
    {
        //public int CardDetailID { get; set; } //SmartcardIdentificationDetailID
        //public int TRSmartcardID { get; set; }
        public int OrgID { get; set; }
        public int SiteID {get;set;}
        public int SmartcardID { get; set; }
        public int SmartcardIdentificationDetailID { get; set; }    //SmartCardDetailID
        //public string CaseNumber { get; set; }
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        public string HolderName { get; set; }
        public string IDCardNumber { get; set; }
        public string OfficeAddress { get; set; }
        public string MobilePhone { get; set; }
        public string VehicleNumber { get; set; }
        public int VehicleType { get; set; }
        public string VehicleBrand { get; set; }
        //public string BoomGateID { get; set; }
        public DateTime EffectiveDate { get; set; }
        //public DateTime ExpiredDate { get; set; }
        public DateTime? ExpiredDate { get; set; }
        //public string BoomGateID { get; set; }

        public CardDetail() { }

        public CardDetail(int OrgID, int SiteID, int SmartcardIdentificationDetailID, int SmartCardID, string CardNumber, string ChipNumber,string HolderName, string IDCardNumber, string OfficeAddress, string MobilePhone, string VehicleNumber, int VehicleType, string VehicleBrand, DateTime EffectiveDate, DateTime? ExpiredDate)
        {
            this.OrgID = OrgID;
            this.SiteID=SiteID;
            this.SmartcardID = SmartCardID;
            this.SmartcardIdentificationDetailID = SmartcardIdentificationDetailID; //SmartCardDetailID
            //this.CaseNumber = CaseNumber;
            this.CardNumber = CardNumber;
            this.ChipNumber = ChipNumber;
            this.HolderName = HolderName;
            this.IDCardNumber=IDCardNumber;
            this.OfficeAddress=OfficeAddress;
            this.MobilePhone=MobilePhone;
            this.VehicleNumber = VehicleNumber;
            this.VehicleType = VehicleType;
            this.VehicleBrand = VehicleBrand;
            //this.BoomGateID = BoomGateID;
            this.EffectiveDate = EffectiveDate;
            this.ExpiredDate = ExpiredDate;
            //this.BoomGateID = BoomGateID;
        }
    }

    public class AdditionalAccessCardDetail
    {
        public int SmartcardIdentificationDetailID { get; set; }
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        public string HolderName { get; set; }
        public int BoomGateID { get; set; }
        public string BoomGateName { get; set; }

        public AdditionalAccessCardDetail(){}
        
        //public AdditionalAccessCardDetail(int SmartcardIdentificationDetailID, string CardNumber, string ChipNumber, string HolderName, int BoomGateID, string BoomGateName)
        public AdditionalAccessCardDetail(int SmartcardIdentificationDetailID, string CardNumber, string ChipNumber, string HolderName)
        {
            this.SmartcardIdentificationDetailID=SmartcardIdentificationDetailID;
            this.CardNumber = CardNumber;
            this.ChipNumber = ChipNumber;
            this.HolderName = HolderName;
            //this.BoomGateID = BoomGateID;
            //this.BoomGateName = BoomGateName;
        }
    }

}