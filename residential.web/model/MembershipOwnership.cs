﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.web.model
{
    public class MembershipOwnership
    {
        public int MembershipID { get; set; }
        public string CardNumber { get; set; }
        public string ChipNumber { get; set; }
        //public string HolderName { get; set; }
        public string PSCode { get; set; }
        public int BoomGateID { get; set; }
        public string BoomGateName { get; set; }
        public string BoomGateCode { get; set; }
        public int Status { get; set; }
        public int Sync { get; set; }

        public MembershipOwnership() { }
        public MembershipOwnership(int MembershipID, string CardNumber, string ChipNumber, string PSCode, int BoomGateID, string BoomGateName, string BoomGateCode, int Status, int Sync) 
        {
            this.MembershipID = MembershipID;
            this.CardNumber = CardNumber;
            this.ChipNumber = ChipNumber;
            //this.HolderName = HolderName;
            this.PSCode = PSCode;
            this.BoomGateID = BoomGateID;
            this.BoomGateName = BoomGateName;
            this.BoomGateCode = BoomGateCode;
            this.Status = Status;
            this.Sync = Sync;
        }
    }
}