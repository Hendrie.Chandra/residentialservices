﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel.Description;
using System.Configuration;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk;
using System.Windows.Forms;
using System.IO;
using Microsoft.Xrm.Sdk.Query;

namespace residential.importHelpName
{
    static class Program
    {
        
        static IOrganizationService crmservice_conn()
        {
            Uri uriorg = new Uri(ConfigurationManager.AppSettings["CRMUrlOrg"].ToString());
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential.Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            credentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            credentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            OrganizationServiceProxy serviceproxy = new OrganizationServiceProxy(uriorg, null, credentials, null);
            IOrganizationService service;
            service = (IOrganizationService)serviceproxy;

            return service;
        }

        static IOrganizationService service;

        static void createrecord(IOrganizationService service, StreamWriter log, List<string> line, int i, string duration, string name, string parent, string helpcategory, string department, int ipklincludable, string rsviewformname, string owner)
        {
            Boolean flag = true;
            String error = "";
            if (duration != "" && name != "" && helpcategory != "" && owner != "")
            {

                //Search existing self record
                ConditionExpression checkself_name = new ConditionExpression();
                checkself_name.AttributeName = "new_name";
                checkself_name.Operator = ConditionOperator.Equal;
                checkself_name.Values.Add(name);

                ConditionExpression checkself_status = new ConditionExpression();
                checkself_status.AttributeName = "statecode";
                checkself_status.Operator = ConditionOperator.Equal;
                checkself_status.Values.Add(0);

                FilterExpression checkself = new FilterExpression();
                checkself.Conditions.Add(checkself_name);
                checkself.Conditions.Add(checkself_status);

                QueryExpression checkselfquery = new QueryExpression("new_helpname");
                checkselfquery.Criteria.AddFilter(checkself);

            
                //helpcategory
                LinkEntity checkself_link_helpcategory = new LinkEntity();
                checkself_link_helpcategory.LinkFromEntityName = "new_helpname";
                checkself_link_helpcategory.LinkFromAttributeName = "new_helpcategory";
                checkself_link_helpcategory.LinkToEntityName = "new_helpcategory";
                checkself_link_helpcategory.LinkToAttributeName = "new_helpcategoryid";

                FilterExpression checkself_link_helpcategory_name = new FilterExpression();
            
                ConditionExpression condition_helpcategory_name = new ConditionExpression();
                condition_helpcategory_name.AttributeName = "new_name";
                condition_helpcategory_name.Operator = ConditionOperator.Equal;
                condition_helpcategory_name.Values.Add(helpcategory);

                checkself_link_helpcategory_name.AddCondition(condition_helpcategory_name);
                checkself_link_helpcategory.LinkCriteria.AddFilter(checkself_link_helpcategory_name);

                checkselfquery.LinkEntities.Add(checkself_link_helpcategory);

                //owner
                LinkEntity checkself_link_owner = new LinkEntity();
                checkself_link_owner.LinkFromEntityName = "new_helpname";
                checkself_link_owner.LinkFromAttributeName = "owningteam";
                checkself_link_owner.LinkToEntityName = "team";
                checkself_link_owner.LinkToAttributeName = "teamid";

                FilterExpression checkself_link_owner_name = new FilterExpression();
            
                ConditionExpression condition_ownerlink_name = new ConditionExpression();
                condition_ownerlink_name.AttributeName = "name";
                condition_ownerlink_name.Operator = ConditionOperator.Equal;
                condition_ownerlink_name.Values.Add(owner);

                checkself_link_owner_name.AddCondition(condition_ownerlink_name);
                checkself_link_owner.LinkCriteria.AddFilter(checkself_link_owner_name);

                checkselfquery.LinkEntities.Add(checkself_link_owner);

                //parent
                if (parent != "")
                {
                    LinkEntity checkself_link_parent = new LinkEntity();
                    checkself_link_parent.LinkFromEntityName = "new_helpname";
                    checkself_link_parent.LinkFromAttributeName = "new_parent";
                    checkself_link_parent.LinkToEntityName = "new_helpname";
                    checkself_link_parent.LinkToAttributeName = "new_helpnameid";

                    FilterExpression checkself_link_parent_name = new FilterExpression();

                    ConditionExpression condition_parentlink_name = new ConditionExpression();
                    condition_parentlink_name.AttributeName = "new_name";
                    condition_parentlink_name.Operator = ConditionOperator.Equal;
                    condition_parentlink_name.Values.Add(parent);

                    checkself_link_parent_name.AddCondition(condition_parentlink_name);
                    checkself_link_parent.LinkCriteria.AddFilter(checkself_link_parent_name);

                    checkselfquery.LinkEntities.Add(checkself_link_parent);
                }

                EntityCollection checkselfresult = service.RetrieveMultiple(checkselfquery);

                //If Exists
                if (checkselfresult != null && checkselfresult.Entities.Count > 0)
                {
                    Console.WriteLine((i + 1).ToString() + " - ERROR: Record yang sama terdeteksi sudah ada untuk line berikut: " + line[i]);
                    log.WriteLine((i + 1).ToString() + " - ERROR: Record yang sama terdeteksi sudah ada untuk line berikut: " + line[i]);
                }
                else
                {

                    Entity newhelpname = new Entity("new_helpname");
                    newhelpname.Attributes["new_duration"] = int.Parse(duration);
                    newhelpname.Attributes["new_name"] = name;
                    newhelpname.Attributes["new_ipklincludable"] = Convert.ToBoolean(ipklincludable);
                    if (rsviewformname != "")
                        newhelpname.Attributes["new_rsviewformname"] = rsviewformname;

                    //insert parent
                    if (parent != "")
                    {
                        //newhelpname.Attributes["new_parent"] = new EntityReference("new_helpname", new Guid(parentid));
                        //retrieve parent
                        QueryExpression retrieveparentquery = new QueryExpression("new_helpname");

                        FilterExpression retrieveparent = new FilterExpression();

                        ConditionExpression retrieveparent_status = new ConditionExpression();
                        retrieveparent_status.AttributeName = "statecode";
                        retrieveparent_status.Operator = ConditionOperator.Equal;
                        retrieveparent_status.Values.Add(0);

                        ConditionExpression retrieveparent_name = new ConditionExpression();
                        retrieveparent_name.AttributeName = "new_name";
                        retrieveparent_name.Operator = ConditionOperator.Equal;
                        retrieveparent_name.Values.Add(parent);

                        retrieveparent.AddCondition(retrieveparent_status);
                        retrieveparent.AddCondition(retrieveparent_name);

                        LinkEntity retrieveparent_link_team = new LinkEntity();
                        retrieveparent_link_team.LinkFromEntityName = "new_helpname";
                        retrieveparent_link_team.LinkFromAttributeName = "owningteam";
                        retrieveparent_link_team.LinkToEntityName = "team";
                        retrieveparent_link_team.LinkToAttributeName = "teamid";

                        FilterExpression retrieveparent_link_team_filter = new FilterExpression();

                        ConditionExpression retrieveparent_link_team_name = new ConditionExpression();
                        retrieveparent_link_team_name.AttributeName = "name";
                        retrieveparent_link_team_name.Operator = ConditionOperator.Equal;
                        retrieveparent_link_team_name.Values.Add(owner);

                        retrieveparent_link_team_filter.AddCondition(retrieveparent_link_team_name);

                        retrieveparent_link_team.LinkCriteria.AddFilter(retrieveparent_link_team_filter);

                        retrieveparentquery.Criteria.AddFilter(retrieveparent);
                        retrieveparentquery.LinkEntities.Add(retrieveparent_link_team);

                        EntityCollection retrieveparentresult = service.RetrieveMultiple(retrieveparentquery);

                        //If Exists
                        if (retrieveparentresult != null && retrieveparentresult.Entities.Count > 0)
                        {
                            newhelpname.Attributes["new_parent"] = new EntityReference("new_helpname", retrieveparentresult[0].Id);
                        }
                        else
                        {
                            flag = false;
                            error += "Parent, ";
                        }
                    }

                    //search and insert helpcategory
                    if (helpcategory != "")
                    {
                        QueryExpression helpcategoryquery = new QueryExpression("new_helpcategory");
                        FilterExpression helpcategoryqueryfilter = new FilterExpression();

                        ConditionExpression condition_status = new ConditionExpression();
                        condition_status.AttributeName = "statecode";
                        condition_status.Operator = ConditionOperator.Equal;
                        condition_status.Values.Add(0);

                        ConditionExpression condition_name = new ConditionExpression();
                        condition_name.AttributeName = "new_name";
                        condition_name.Operator = ConditionOperator.Equal;
                        condition_name.Values.Add(helpcategory);

                        helpcategoryqueryfilter.AddCondition(condition_status);
                        helpcategoryqueryfilter.AddCondition(condition_name);

                        helpcategoryquery.Criteria.AddFilter(helpcategoryqueryfilter);

                        EntityCollection helpcategoryqueryresult = service.RetrieveMultiple(helpcategoryquery);

                        if (helpcategoryqueryresult != null && helpcategoryqueryresult.Entities.Count > 0)
                        {
                            newhelpname.Attributes["new_helpcategory"] = new EntityReference("new_helpcategory", helpcategoryqueryresult[0].Id);
                        }
                        else
                        {
                            flag = false;
                            error += "HelpCategory, ";
                        }
                    }

                    //search and insert department
                    if (department != "")
                    {
                        QueryExpression departmentquery = new QueryExpression("new_department");
                        FilterExpression departmentqueryfilter = new FilterExpression();

                        ConditionExpression condition_status = new ConditionExpression();
                        condition_status.AttributeName = "statecode";
                        condition_status.Operator = ConditionOperator.Equal;
                        condition_status.Values.Add(0);

                        ConditionExpression condition_name = new ConditionExpression();
                        condition_name.AttributeName = "new_name";
                        condition_name.Operator = ConditionOperator.Equal;
                        condition_name.Values.Add(department);

                        LinkEntity departmentquerylink = new LinkEntity()
                        {
                            LinkFromEntityName = "new_department",
                            LinkFromAttributeName = "owningteam",
                            LinkToEntityName = "team",
                            LinkToAttributeName = "teamid"
                        };

                        FilterExpression departmentquerylink_filter = new FilterExpression();

                        ConditionExpression condition_team = new ConditionExpression();
                        condition_team.AttributeName = "name";
                        condition_team.Operator = ConditionOperator.Equal;
                        condition_team.Values.Add(owner);

                        departmentquerylink_filter.AddCondition(condition_team);
                        departmentquerylink.LinkCriteria.AddFilter(departmentquerylink_filter);

                        departmentqueryfilter.AddCondition(condition_status);
                        departmentqueryfilter.AddCondition(condition_name);

                        departmentquery.Criteria.AddFilter(departmentqueryfilter);
                        departmentquery.LinkEntities.Add(departmentquerylink);

                        EntityCollection departmentqueryresult = service.RetrieveMultiple(departmentquery);

                        if (departmentqueryresult != null && departmentqueryresult.Entities.Count > 0)
                        {
                            newhelpname.Attributes["new_department"] = new EntityReference("new_department", departmentqueryresult[0].Id);
                        }
                        else
                        {
                            flag = false;
                            error += "Department, ";
                        }
                    }

                    //search and insert Owner
                    QueryExpression ownerquery = new QueryExpression("team");

                    FilterExpression ownerqueryfilter = new FilterExpression();

                    ConditionExpression condition_owner_name = new ConditionExpression();
                    condition_owner_name.AttributeName = "name";
                    condition_owner_name.Operator = ConditionOperator.Equal;
                    condition_owner_name.Values.Add(owner);

                    ownerqueryfilter.Conditions.Add(condition_owner_name);

                    ownerquery.Criteria.AddFilter(ownerqueryfilter);

                    EntityCollection ownerqueryresult = service.RetrieveMultiple(ownerquery);

                    if (ownerqueryresult != null && ownerqueryresult.Entities.Count > 0)
                    {
                        newhelpname.Attributes["ownerid"] = new EntityReference("team", ownerqueryresult[0].Id);
                    }
                    else
                    {
                        flag = false;
                        error += "Owner, ";
                    }

                    if (flag == true)
                    {
                        Guid _helpname = service.Create(newhelpname);

                        Console.WriteLine((i + 1).ToString() + " - " + _helpname.ToString() + " - Record telah berhasil dibuat untuk line berikut: " + line[i]);
                        log.WriteLine((i + 1).ToString() + " - " + _helpname.ToString() + " - Record telah berhasil dibuat untuk line berikut: " + line[i]);

                    }
                    else
                    {
                        error = error.Remove(error.Length - 2, 2);
                        Console.WriteLine((i + 1).ToString() + " - ERROR: Data lookup dalam field " + error + " tidak ditemukan di system untuk line berikut: " + line[i]);
                        log.WriteLine((i + 1).ToString() + " - ERROR: Data lookup dalam field " + error + " tidak ditemukan di system untuk line berikut: " + line[i]);
                    }

                }
            }
            else
            {
                Console.WriteLine((i + 1).ToString() + " - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Kolom duration, name, helpcategory, dan owner wajib diisi");
                log.WriteLine((i + 1).ToString() + " - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Kolom duration, name, helpcategory, dan owner wajib diisi");
            }
        }

        private static bool findEmpty(string str)
        {
            if (str == "")
            { return true; }
            else
            { return false; }
        }


        public static int[] FindAllIndexOf<T>(this T[] a, Predicate<T> match)
        {
            T[] subArray = Array.FindAll<T>(a, match);
            return (from T item in subArray select Array.IndexOf(a, item)).ToArray();
        }

        [STAThread]
        static void Main(string[] args)
        {
            service = crmservice_conn();

            string filepath;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "CSV Files|*.csv";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                filepath = dialog.FileName.ToString();

                List<string> line = new List<string>();
                List<string> duration = new List<string>();
                List<string> name = new List<string>();
                List<string> parent = new List<string>();
                List<string> helpcategory = new List<string>();
                List<string> department = new List<string>();
                List<string> ipklincludable = new List<string>();
                List<string> rsviewformname = new List<string>();
                List<string> owner = new List<string>();

                String logpath = ConfigurationManager.AppSettings["PathResultLog"].ToString();
                StreamWriter log;

                if (!File.Exists(logpath))
                {
                    log = new StreamWriter(logpath);
                }
                else
                {
                    log = new StreamWriter(logpath, true);
                }

                log.WriteLine("-----------------------" + DateTime.Now + "-----------------------");

                try
                {
                    using (StreamReader readFile = new StreamReader(filepath))
                    {
                        string baris;
                        int count = 0;
                        while ((baris = readFile.ReadLine()) != null)
                        {
                            String[] kolom = baris.Split(';');
                            if (kolom.Length == 8)
                            {
                                line.Add(baris);
                                duration.Add(kolom.ElementAt<string>(0));
                                name.Add(kolom.ElementAt<string>(1));
                                parent.Add(kolom.ElementAt<string>(2));
                                helpcategory.Add(kolom.ElementAt<string>(3));
                                department.Add(kolom.ElementAt<string>(4));
                                ipklincludable.Add(kolom.ElementAt<string>(5));
                                rsviewformname.Add(kolom.ElementAt<string>(6));
                                owner.Add(kolom.ElementAt<string>(7));
                                count++;
                            }
                            else
                            {
                                Console.WriteLine("ERROR: Baris: \"" + baris + "\" memiliki jumlah elemen yang salah");
                                log.WriteLine("ERROR: Baris: \"" + baris + "\" memiliki jumlah elemen yang salah");
                            }
                        }

                        if (count > 0)
                        {
                            line.RemoveAt(0);
                            duration.RemoveAt(0);
                            name.RemoveAt(0);
                            parent.RemoveAt(0);
                            helpcategory.RemoveAt(0);
                            department.RemoveAt(0);
                            ipklincludable.RemoveAt(0);
                            rsviewformname.RemoveAt(0);
                            owner.RemoveAt(0);
                            count--;
                        }

                        for (int i = 0; i < count; i++)
                        {
                            try
                            {
                                int _ipklincludable;
                                if (int.TryParse(ipklincludable[i], out _ipklincludable))
                                {
                                    int _duration;
                                    if (int.TryParse(duration[i], out _duration))
                                    {
                                        if (_ipklincludable == 0 || _ipklincludable == 1)
                                        {
                                            createrecord(service, log, line, i, duration[i], name[i], parent[i], helpcategory[i], department[i], int.Parse(ipklincludable[i]), rsviewformname[i], owner[i]);
                                        }
                                        else
                                        {
                                            Console.WriteLine((i + 1).ToString() + " - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom ipklincludable yang salah");
                                            log.WriteLine((i + 1).ToString() + " - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom ipklincludable yang salah");
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine((i + 1).ToString() + " - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom duration yang salah");
                                        log.WriteLine((i + 1).ToString() + " - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom duration yang salah");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine((i + 1).ToString() + " - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom ipklincludable yang salah");
                                    log.WriteLine((i + 1).ToString() + " - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom ipklincludable yang salah");
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine((i + 1).ToString() + " - ERROR: Record gagal dibuat untuk line berikut: " + line[i] + "\n" + ex.ToString());
                                log.WriteLine((i + 1).ToString() + " - ERROR: Record gagal dibuat untuk line berikut: " + line[i] + "\n" + ex.ToString());
                            }
                        }
                    }
                    log.WriteLine();
                    log.WriteLine();
                    log.Close();
                    Console.Write("\nCOMPLETED...!");
                    Console.ReadLine();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("===ERROR=== " + ex.ToString());
                    log.WriteLine("===ERROR=== " + ex.ToString());
                    log.WriteLine();
                    log.WriteLine();
                    log.Close();
                    Console.ReadLine();
                }
            }
        }
        
    }
}
