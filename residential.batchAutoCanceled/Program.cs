﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel.Description;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace residential.AutoCancelled
{
    class Program
    {
        static IOrganizationService crmservice_conn()
        {
            Uri uriorg = new Uri(ConfigurationManager.AppSettings["CRMUrlOrg"].ToString());
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential.Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            credentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            credentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            OrganizationServiceProxy serviceproxy = new OrganizationServiceProxy(uriorg, null, credentials, null);
            IOrganizationService service;
            service = (IOrganizationService)serviceproxy;

            return service;
        }
        
        static void Main(string[] args)
        {
            String PathResultLog = ConfigurationManager.AppSettings["PathResultLog"].ToString();
            String linesLog = "";

            IOrganizationService crmservice = crmservice_conn();            

            ConditionExpression condition1 = new ConditionExpression();
            condition1.AttributeName = "new_date";
            condition1.Operator = ConditionOperator.GreaterEqual;
            condition1.Values.Add(DateTime.Now.AddYears(-1).ToUniversalTime().Date);

            ConditionExpression condition2 = new ConditionExpression();
            condition2.AttributeName = "new_date";
            condition2.Operator = ConditionOperator.LessEqual;
            condition2.Values.Add(DateTime.Now.ToUniversalTime().Date);

            FilterExpression filter1 = new FilterExpression();
            filter1.Conditions.Add(condition1);
            filter1.Conditions.Add(condition2);

            QueryExpression query = new QueryExpression("new_offdays");
            query.ColumnSet.AddColumns("new_date");
            query.Criteria.AddFilter(filter1);

            EntityCollection allharilibur = crmservice.RetrieveMultiple(query);

            List<DateTime> listharilibur = new List<DateTime>();
            foreach (Entity harilibur in allharilibur.Entities)
            {
                if (harilibur.Attributes.Contains("new_date"))
                    listharilibur.Add(((DateTime)harilibur.Attributes["new_date"]).ToLocalTime());
            }

            if (!listharilibur.Contains(DateTime.Now) && DateTime.Now.DayOfWeek != DayOfWeek.Saturday && DateTime.Now.DayOfWeek != DayOfWeek.Sunday)
            {
                DataTable dthitung_Aging = get_TrxCase_Pending();

                if (dthitung_Aging != null)
                {
                    String Log = "";
                    foreach (DataRow dtRow in dthitung_Aging.Rows)
                    {
                        int aging = 0;
                        DateTime createon = ((DateTime)dtRow["CreatedDate"]).ToLocalTime();
                        TimeSpan difference = DateTime.Now - createon;
                        Int32 days = Convert.ToInt32(difference.TotalDays) - 1;
                        DateTime temp = createon.AddDays(days);

                        
                        String NomorCase = dtRow["CaseNumber"].ToString();
                        String CreatedDate = dtRow["CreatedDate"].ToString();
                        String CRMCaseID = dtRow["CRMCaseID"].ToString();

                        while (createon <= temp)
                        {
                            if (createon.DayOfWeek != DayOfWeek.Saturday && createon.DayOfWeek != DayOfWeek.Sunday && !listharilibur.Contains(createon.Date))
                                aging++;
                            createon = createon.AddDays(1);
                        }

                        if (3 <= aging)
                        {
                            Update_Status_Cancelled(NomorCase, CRMCaseID);
                            Log = "Non Aktifkan  NomorCase = " + NomorCase + " Karena aging Sudah Mencapai  " + aging + " Hari Dari Case Yang Dibuat Tanggal = " + CreatedDate;
                            linesLog += Log + "\r\n";
                            Console.WriteLine(Log);
                        }
                    }                                        
                }
                else
                {
                    linesLog += "There are no running batch AutoCancel now because this is holiday \r\n";
                    Console.WriteLine("There are no running batch AutoCancel now because this is holiday");
                }
                linesLog += "Done running bacth AutoCancel at " + DateTime.Now.ToString("HH:mm:ss tt") + "\r\n";                
                linesLog += cleanFileTemp(PathResultLog);
                writeLog(linesLog, PathResultLog + "logBatchAutoCancelled - " + DateTime.Now.ToString("dd-MM-yyyy") + ".txt");
                
                Console.WriteLine("Done");
                Console.Write("Press any key to exit...");
                Console.ReadKey();
            }
        }

        static private void writeLog(string linesLog, String PathResultLog)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(PathResultLog);
            file.WriteLine(linesLog);
            file.Close();
        }

        static string cleanFileTemp(string path)
        {
            String log = "";
            DateTime minus7 = DateTime.Now.AddDays(-7);
            string file = path + "logBatchAutoCancelled - " + minus7.ToString("dd-MM-yyyy") + ".txt";

            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
                log += file + " will be deleted";
            }
            else
            {
                log += file + " not found";
            }

            return log;
        }

        static DataTable get_TrxCase_Pending()
        {
            string DBConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;

            SqlConnection conn = new SqlConnection(DBConnectionString);
            conn.Open();
            SqlCommand com = new SqlCommand("SP_Get_Case_Pending", conn);
            com.CommandType = CommandType.StoredProcedure;            
            SqlDataAdapter sda = new SqlDataAdapter(com);
            DataTable dta = new DataTable();
            sda.Fill(dta);
            conn.Close();

            return dta;
        }

        static private void Update_Status_Cancelled(String _NomorCase, String _CRMCaseID)
        {
            IOrganizationService crmservice = crmservice_conn();

            //Update to CRM
            Entity updateCase = crmservice.Retrieve("incident", new Guid(_CRMCaseID), new ColumnSet(true));
            updateCase.Attributes["new_helpdeskstatus"] = new OptionSetValue(3);
            updateCase.Attributes["new_remarks"] = "Case Cancelled. No payment within 3 days";
            crmservice.Update(updateCase);
           
            //Update to TMDDB
            string DBConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(DBConnectionString);
            conn.Open();
            SqlCommand com = new SqlCommand("SP_Update_Status_TRCase_Cancelled", conn);
            com.Parameters.AddWithValue("@CaseNumber", _NomorCase);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(com);
            DataTable dta = new DataTable();
            sda.Fill(dta);
            conn.Close();
        }
    }
}
