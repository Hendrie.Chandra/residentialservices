﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace residential.batchEmailReminder
{
    public class DBclass
    {
        #region sendingEmailData
        private String connectionstring;

        public DBclass(String _connectionstring)
        {
            connectionstring = _connectionstring;
        }

        public DataTable retrieve_personal_email(string _pscode)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_personal_email", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@PSCode", _pscode);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public DataTable retrieve_case_detail(string _CaseNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_case_detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public string save_email_request_log(string _CaseNumber, int _CaseStatusID, string _EmailTo, string _EmailBody)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("splk_insert_email_request", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@EmailTo", _EmailTo);
                cmd.Parameters.AddWithValue("@EmailBody", _EmailBody);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        #endregion sendingEmailData
    }

}
