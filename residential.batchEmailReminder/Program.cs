﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel.Description;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Client;
using System.IO;

namespace residential.batchEmailReminder
{
    class caseOverdue{
        public string CaseNumber { get; set; }
        public string CRMCaseID { get; set; }
        public Int32 CaseStatusID { get; set; }
    }
    class Program
    {
        static void Main(string[] args)
        {
            String PathResultLog = ConfigurationManager.AppSettings["PathResultLog"].ToString();
            if (!Directory.Exists(PathResultLog))
                Directory.CreateDirectory(PathResultLog);

            String LogData = "";

            try
            {
                DataTable cases = Retrieve_Case_Overdue();

                if (cases.Rows.Count > 0)
                {
                    //
                    List<caseOverdue> filter = new List<caseOverdue>();

                    String Log = "";
                    foreach (DataRow dr in cases.Rows)
                    {
                        String LogNotification="";
                        String CaseNumber = dr["CaseNumber"].ToString();
                        Int32 CaseStatusID = Int32.Parse(dr["CaseStatusID"].ToString());

                        //kirim email disini
                        //template email >>Case Overdue Notification
                        //Send_Email_PIC_PICHead(CaseNumber, CRMCaseID, CaseStatusID, out LogNotification);
                        ////kirim email disini 
                        String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                        DBclass dbclass = new DBclass(constring);
                        DataTable dt = dbclass.retrieve_case_detail(CaseNumber);
                        if (dt.Rows.Count > 0)
                        {
                            string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                            string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                            string emailFrom = dt.Rows[0]["EmailFrom"].ToString();
                            string displayEmailFrom = dt.Rows[0]["DisplayEmailFrom"].ToString();
                            int helpdeskStatus = (int)dt.Rows[0]["CaseStatusID"];

                            EmailClass.sendEmailToPICHeadOverdue(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus, out LogNotification);
                        }

                        Log = "Case number : " + CaseNumber + " | Details : " + LogNotification + " | Time : " + DateTime.Now.ToString();
                        LogData += Log + "\r\n";
                        Console.WriteLine(Log);
                    }
                }
                else
                {
                    LogData += "No Case Overdue \r\n";
                    Console.WriteLine("No Case Overdue");
                }
                LogData += "Batch Email Reminder has been Completed | " + DateTime.Now.ToString() + "\r\n";
                LogData += CleanFileTemp(PathResultLog);
                WriteLog(LogData, PathResultLog + "logBatchEmailReminder - " + DateTime.Now.ToString("dd-MM-yyyy") + ".txt");

                Console.WriteLine("Done");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                LogData = ex.Message;
                WriteLog(LogData, PathResultLog + "logBatchEmailReminder - " + DateTime.Now.ToString("dd-MM-yyyy") + ".txt");
            }
            //Console.Write("Press any key to exit...");
            //Console.ReadKey();
        }
        
        static private void WriteLog(string linesLog, String PathResultLog)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(PathResultLog);
            file.WriteLine(linesLog);
            file.Close();
        }

        static string CleanFileTemp(string path)
        {
            String log = "";
            DateTime minus7 = DateTime.Now.AddDays(-7);
            string file = path + "logBatchEmailReminder - " + minus7.ToString("dd-MM-yyyy") + ".txt";

            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
                log += file + " will be deleted";
            }
            else
            {
                log += file + " not found";
            }

            return log;
        }

        static DataTable Retrieve_Case_Overdue()
        {
            string DBConnectionString = ConfigurationManager.ConnectionStrings["residentialdev"].ConnectionString;
            SqlConnection conn = new SqlConnection(DBConnectionString);
            SqlCommand cmd = new SqlCommand("SP_Get_Case_Overdue", conn);
            cmd.CommandType = CommandType.StoredProcedure;           
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        static private void Retrieve_PICHead(String _CaseNumber, out String _PICHeadEmailAddress, out String _PICHeadName, SqlConnection _Connection)
        {
            SqlCommand cmd_pichead = new SqlCommand("SP_Retrieve_PICHead", _Connection);
            cmd_pichead.CommandType = CommandType.StoredProcedure;
            cmd_pichead.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            _Connection.Open();
            SqlDataAdapter adp_pichead = new SqlDataAdapter(cmd_pichead);

            DataTable dt_pichead = new DataTable();
            adp_pichead.Fill(dt_pichead);
            _Connection.Close();

            _PICHeadEmailAddress = "";
            _PICHeadName = "";

            if (dt_pichead.Rows.Count > 0)
            {
                _PICHeadEmailAddress = dt_pichead.Rows[0]["EmailAddress"].ToString();
                _PICHeadName = dt_pichead.Rows[0]["FullName"].ToString();
            }
        }

        static private void Retrieve_PIC(String _CaseNumber, out String _PICEmailAddress, out String _PICName, SqlConnection _Connection)
        {
            SqlCommand cmd_case = new SqlCommand("SP_Retrieve_Case", _Connection);
            cmd_case.CommandType = CommandType.StoredProcedure;
            cmd_case.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            _Connection.Open();
            SqlDataAdapter adp_case = new SqlDataAdapter(cmd_case);

            DataTable dt_case = new DataTable();
            adp_case.Fill(dt_case);
            _Connection.Close();

            _PICEmailAddress = "";
            _PICName = "";

            if (dt_case.Rows.Count > 0)
            {
                String PICUserName = dt_case.Rows[0]["WorkedBy"].ToString();

                SqlCommand cmd_pic = new SqlCommand("SP_Retrieve_User", _Connection);
                cmd_pic.CommandType = CommandType.StoredProcedure;
                cmd_pic.Parameters.AddWithValue("@UserName", PICUserName);
                _Connection.Open();
                SqlDataAdapter adp_pic = new SqlDataAdapter(cmd_pic);

                DataTable dt_pic = new DataTable();
                adp_pic.Fill(dt_pic);
                _Connection.Close();
                
                if (dt_pic.Rows.Count > 0)
                {
                    _PICEmailAddress = dt_pic.Rows[0]["EmailAddress"].ToString();
                    _PICName = dt_pic.Rows[0]["FullName"].ToString();
                }
            }
        }

        //static private void Send_Email_PIC_PICHead(String _CaseNumber, String _CRMCaseID, Int32 _CaseStatusID, out String _Log)
        //{
        //    IOrganizationService crmservice = crmservice_conn();

        //    String DBConnectionString = ConfigurationManager.ConnectionStrings["residentialdev"].ConnectionString;
        //    SqlConnection conn = new SqlConnection(DBConnectionString);

        //    Entity mycase = crmservice.Retrieve("incident", new Guid(_CRMCaseID), new ColumnSet(new string[] { "new_unit" }));
        //    Entity unit = crmservice.Retrieve("new_unit", ((EntityReference)mycase.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
        //    Entity businessunit = crmservice.Retrieve("businessunit", ((EntityReference)unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue" }));

        //    QueryByAttribute templatequery = new QueryByAttribute("template");
        //    templatequery.ColumnSet = new ColumnSet(true);
        //    templatequery.Attributes.AddRange("title");
        //    templatequery.Values.AddRange("Case Overdue Notification");
        //    EntityCollection emailtemplate = crmservice.RetrieveMultiple(templatequery);

        //    if (emailtemplate.Entities.Count > 0)
        //    {
        //        EntityCollection From = new EntityCollection();
        //        Entity QueueEmail = new Entity("activityparty");
        //        QueueEmail.Attributes.Add("partyid", new EntityReference("queue", ((EntityReference)businessunit.Attributes["new_emailqueue"]).Id));
        //        From.Entities.Add(QueueEmail);

        //        EntityCollection To = new EntityCollection();

        //        String Representative = "";

        //        //Retrieve PIC Head Email
        //        String PICHeadEmailAddress = "";
        //        String PICHeadName = "";
        //        Retrieve_PICHead(_CaseNumber, out PICHeadEmailAddress, out PICHeadName, conn);
        //        Representative = PICHeadName;

        //        if (!String.IsNullOrWhiteSpace(PICHeadEmailAddress) &&
        //                Regex.IsMatch(PICHeadEmailAddress, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z"))
        //        {
        //            Entity activityPICHead = new Entity("activityparty");
        //            activityPICHead.Attributes.Add("addressused", PICHeadEmailAddress);
        //            To.Entities.Add(activityPICHead);
        //        }

        //        if (_CaseStatusID == 4)
        //        {
        //            //Retrieve PIC Email
        //            String PICEmailAddress = "";
        //            String PICName = "";
        //            Retrieve_PIC(_CaseNumber, out PICEmailAddress, out PICName, conn);
        //            if (!String.IsNullOrWhiteSpace(PICEmailAddress) &&
        //                Regex.IsMatch(PICEmailAddress, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z")
        //                && (PICEmailAddress != PICHeadEmailAddress)) //PIC won't added if case worked by Head
        //            {
        //                Entity activityPIC = new Entity("activityparty");
        //                activityPIC.Attributes.Add("addressused", PICEmailAddress);
        //                To.Entities.Add(activityPIC);
        //                Representative += " and " + PICName;
        //            }
        //        }

        //        if (To.Entities.Count > 0)
        //        {
        //            InstantiateTemplateRequest instTemplateReq = new InstantiateTemplateRequest();
        //            instTemplateReq.TemplateId = emailtemplate.Entities[0].Id;
        //            instTemplateReq.ObjectId = mycase.Id;
        //            instTemplateReq.ObjectType = mycase.LogicalName;

        //            InstantiateTemplateResponse instTemplateResp = (InstantiateTemplateResponse)crmservice.Execute(instTemplateReq);                    

        //            Entity email = new Entity("email");
        //            email = instTemplateResp.EntityCollection[0];

        //            String Subject = email.Attributes["subject"].ToString();
        //            String MessageContent = email.Attributes["description"].ToString();

        //            MessageContent = MessageContent.Replace("{Representative}", Representative);

        //            email.Attributes["regardingobjectid"] = new EntityReference(mycase.LogicalName, mycase.Id);
        //            email.Attributes["from"] = From;
        //            email.Attributes["to"] = To;
        //            email.Attributes["subject"] = Subject;
        //            email.Attributes["description"] = MessageContent;
        //            Guid EmailID = crmservice.Create(email);

        //            SendEmailRequest reqSendEmail = new SendEmailRequest();
        //            reqSendEmail.EmailId = EmailID;
        //            reqSendEmail.TrackingToken = "";
        //            reqSendEmail.IssueSend = true;

        //            SendEmailResponse res = (SendEmailResponse)crmservice.Execute(reqSendEmail);
        //            _Log = "Email sent successfully";
        //        }
        //        else
        //            _Log = "Invalid Email Address";
        //    }
        //    else
        //        _Log = "Email template not found";            
        //}
    }
}
