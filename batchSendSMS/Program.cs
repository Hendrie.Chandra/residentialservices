﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Net;
using System.Windows.Forms;
using System.Collections.Specialized;
//using System.Net.Http.Formatting;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;
using System.Threading;

namespace batchSendSMS
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {            
            while (0 == 0)
            {
                var stopwatch = Stopwatch.StartNew();
                            
                Console.WriteLine("- SMS BLAST BATCH -");

                //Configuration            
                String ProxyAddress = ConfigurationManager.AppSettings["ProxyAddress"].ToString();
                Int32 ProxyPort = Int32.Parse(ConfigurationManager.AppSettings["ProxyPort"].ToString());
                String ProxyUserName = ConfigurationManager.AppSettings["ProxyUserName"].ToString();
                String ProxyPassword = ConfigurationManager.AppSettings["ProxyPassword"].ToString();
                String ProxyDomain = ConfigurationManager.AppSettings["ProxyDomain"].ToString();

                String APIUserName = ConfigurationManager.AppSettings["APIUserName"].ToString();
                String APIPassword = ConfigurationManager.AppSettings["APIPassword"].ToString();

                try
                {
                    //Retrieve all SMSBlast case
                    DataTable dt_SMSBlastCases = RetrieveSMSBlastSending();
                    if (dt_SMSBlastCases.Rows.Count > 0)
                    {
                        Console.WriteLine("Found " + dt_SMSBlastCases.Rows.Count + " case(s)\r\n");
                        foreach (DataRow SMSBlastCases in dt_SMSBlastCases.Rows)
                        {
                            String From = SMSBlastCases["SMSSender"].ToString();
                            String Message = SMSBlastCases["MessageDetail"].ToString();

                            String CaseNumber = SMSBlastCases["CaseNumber"].ToString();
                            //retrieve all numbers in a case
                            DataTable dt_SMSBlastDetails = RetrieveSMSBlastNumberDetail(CaseNumber);

                            //select rows only when flag status = false -> has not been sent
                            DataRow[] SMSBlastDetails = dt_SMSBlastDetails.Select("Status = False");

                            //Log Content, header
                            String LogContent = "Case Number : " + SMSBlastCases["CaseNumber"].ToString() + "\r\n" +
                                "Count : " + SMSBlastDetails.Count() + " numbers\r\n" +
                                "Running time : " + DateTime.Now.ToString("dd MMMM yyyy - HH:mm:ss") + "\r\n\r\n" +
                                "From : " + From + "\r\n" +
                                "Message : " + Message + "\r\n\r\n";
                            Console.Write(LogContent);

                            Int32 MessageSent = 0;
                            Int32 LineNumber = 1;

                            if (SMSBlastDetails.Count() > 0)
                            {
                                foreach (DataRow SMSBlastDetail in SMSBlastDetails)
                                {
                                    String result = "";
                                    String SMSURIQuery = "from=" + From + "&message=" + Message + "&msisdn=" + SMSBlastDetail["SMSNumber"].ToString(); //

                                    try
                                    {
                                        WebRequest req = System.Net.WebRequest.Create("http://sms.linkit360.com:8080/api/single/?" + SMSURIQuery);

                                        //Add proxy
                                        if (!String.IsNullOrWhiteSpace(ProxyAddress))
                                        {
                                            WebProxy proxy = new WebProxy(ProxyAddress, ProxyPort);
                                            proxy.Credentials = new NetworkCredential(ProxyUserName, ProxyPassword, ProxyDomain);
                                            req.Proxy = proxy;
                                        }
                                        req.Credentials = new NetworkCredential(APIUserName, APIPassword);
                                        HttpWebResponse response = (HttpWebResponse)req.GetResponse();

                                        result = new StreamReader(response.GetResponseStream(), ASCIIEncoding.ASCII).ReadToEnd();

                                        if (Int32.Parse(result.Substring(result.IndexOf("Error status: ") + 14, 1)) == 0)
                                        {
                                            UpdateSMSNumberDetailStatus(Int32.Parse(SMSBlastDetail["SMSBlastNumberDetailID"].ToString()), 1);
                                            MessageSent++;
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        result = e.Message;
                                    }
                                    String LogPerLine = LineNumber.ToString() + " | " + DateTime.Now.ToString("dd/MM/yyyy - HH:mm:ss") + " | " + SMSBlastDetail["SMSNumber"].ToString() + " | " + result + "\r\n";
                                    LogContent += LogPerLine;
                                    Console.Write(LogPerLine);
                                    LineNumber++;
                                }
                            }
                            else
                                Console.WriteLine("No number detail data!");

                            //Update status to SENT
                            if (MessageSent == SMSBlastDetails.Count())
                                Update_SMSStatus(CaseNumber, 3);

                            //report log
                            String SendingReport = "\r\nREPORTS : " + MessageSent + " SENT, " + (SMSBlastDetails.Count() - MessageSent).ToString() + " Failed\r\n";
                            LogContent += SendingReport;
                            Console.WriteLine(SendingReport);

                            stopwatch.Stop();
                            TimeSpan ts = stopwatch.Elapsed;
                            String ElapsedTime = "Elapsed Time : " + ts.Hours + "h " + ts.Minutes + "m " + ts.Seconds + "s";
                            LogContent += ElapsedTime;
                            Console.WriteLine(ElapsedTime);                           

                            //write to log file
                            WriteLogFile(LogContent, ConfigurationManager.AppSettings["LogFilePath"].ToString() + CaseNumber + "_" + DateTime.Now.ToString("dd MMMM yyyy_HH-mm") + ".txt");
                            Console.WriteLine("\r\n--------------------------------------\r\n");
                        }
                    }
                    else
                        Console.WriteLine("No Case data!");

                    Console.WriteLine("\r\n\r\nFINISH..!");
                }
                catch (Exception e)
                {
                    WriteLogFile("An error occured : " + e.Message, ConfigurationManager.AppSettings["LogFilePath"].ToString() + "Batch Error Log_" + DateTime.Now.ToString("dd MMMM yyyy_HH-mm") + ".txt");
                }
                int delay = int.Parse(ConfigurationManager.AppSettings["BatchDelay"].ToString()) * 6000; // * 6000 = miliseconds to minute
                Thread.Sleep(delay); // batch will sleep
                Console.Clear();
            }
        }

        static void Update_SMSStatus(String _CaseNumber, Int32 _SMSStatusID)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ToString());
            SqlCommand cmd = new SqlCommand("SP_Update_SMSStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@SMSStatusID", _SMSStatusID);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        static private void UpdateSMSNumberDetailStatus(Int32 _SMSBlastNumberDetailID, int _Status)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ToString());
            SqlCommand cmd = new SqlCommand("SP_Update_SMSNumberDetailStatus", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SMSBlastNumberDetailID", _SMSBlastNumberDetailID);
            cmd.Parameters.AddWithValue("@Status", _Status);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        static private void WriteLogFile(String _LogContent, String _FilePath)
        {
            if (!Directory.Exists(ConfigurationManager.AppSettings["LogFilePath"].ToString()))
                Directory.CreateDirectory(ConfigurationManager.AppSettings["LogFilePath"].ToString());

            System.IO.StreamWriter file = new System.IO.StreamWriter(_FilePath);
            file.WriteLine(_LogContent);
            file.Close();
        }

        static private DataTable RetrieveSMSBlastSending()
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ToString());
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSBlastSending", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        static private DataTable RetrieveSMSBlastNumberDetail(String _caseNumber)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnectionString"].ToString());
            SqlCommand cmd = new SqlCommand("SP_Retrieve_SMSBlastNumberDetail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
    }
}
