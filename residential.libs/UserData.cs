﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Configuration;
using System.Data;

namespace residential.libs
{   
    [Serializable]
    public class UserData
    {
        public UserData(String _UserName)
        {
            //remove this line to production
            _UserName = "vsndev\\fadel";

            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_user(_UserName);
           
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Select("SiteID = " + dt.Rows[0]["DefaultSiteID"].ToString()).First();

                this.UserID = Int32.Parse(dr["UserID"].ToString());
                this.OrgID = Int32.Parse(dr["OrgID"].ToString());
                this.SiteID = Int32.Parse(dr["DefaultSiteID"].ToString());
                this.RoleID = Int32.Parse(dr["RoleID"].ToString());
                this.DepartmentID = String.IsNullOrEmpty(dr["DepartmentID"].ToString()) ? 0 : Int32.Parse(dr["DepartmentID"].ToString());
                this.DefaultSiteID = Int32.Parse(dr["DefaultSiteID"].ToString());
                this.UserName = dr["UserName"].ToString();
                this.FullName = dr["FullName"].ToString();
                this.RoleName = dr["RoleName"].ToString();
                this.UserAuth = new List<UserAuthorization>();

                DataTable dt_userAuth = dbclass.retrieve_userAuthorization(_UserName);
                int i = 0;
                if (dt_userAuth.Rows.Count > 0)
                    foreach (DataRow row in dt_userAuth.Rows)
                    {
                        UserAuthorization ua = new UserAuthorization();
                        ua.UserAuthorizationID = int.Parse(row["UserAuthorizationID"].ToString());
                        ua.SiteID = int.Parse(row["SiteID"].ToString());
                        ua.RoleID = int.Parse(row["RoleID"].ToString());
                        ua.DepartmentID = int.Parse(row["DepartmentID"].ToString());
                        ua.IsDefault = bool.Parse(row["IsDefault"].ToString());
                        this.UserAuth.Add(ua);
                        i++;
                    }                                    
            }
        }

        [DataMember]
        public Int32 UserID { set; get; }

        [DataMember]
        public Int32 OrgID { set; get; }

        [DataMember]
        public Int32 SiteID { set; get; }

        [DataMember]
        public Int32 RoleID { set; get; }

        [DataMember]
        public Int32 DepartmentID { set; get; }

        [DataMember]
        public Int32 DefaultSiteID { set; get; }

        [DataMember]
        public String UserName { set; get; }

        [DataMember]
        public String FullName { set; get; }

        [DataMember]
        public String RoleName { set; get; }

        //[DataMember]
        //public UserAuthorization[] UserAuth { set; get; }

        [DataMember]
        public List<UserAuthorization> UserAuth { set; get; }
    }

    [Serializable]
    public class UserAuthorization
    {
        public UserAuthorization() //int _UserAuthorizationID, int _SiteID, int _RoleID, int _DepartmentID, bool _IsDefault
        {
            //this.UserAuthorizationID = _UserAuthorizationID;
            //this.SiteID = _SiteID;
            //this.RoleID = _SiteID;
            //this.DepartmentID = _DepartmentID;
            //this.IsDefault = _IsDefault;
        }

        [DataMember]
        public Int32 UserAuthorizationID { set; get; }

        [DataMember]
        public Int32 SiteID { set; get; }

        [DataMember]
        public Int32 RoleID { set; get; }

        [DataMember]
        public Int32 DepartmentID { set; get; }

        [DataMember]
        public Boolean IsDefault { set; get; }
    }

}
