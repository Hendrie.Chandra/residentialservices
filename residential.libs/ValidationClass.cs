﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace residential.libs
{
    public class ValidationClass
    {
        public bool Required(string _value)
        {
            return !string.IsNullOrWhiteSpace(_value);
        }

        public bool IsNumber(string _value)
        {
            return Regex.IsMatch(_value, @"^\d$");
        }

        public bool NumberRange(string _value, string _minLength, string _maxLength)
        {
            return Regex.IsMatch(_value, @"^\d{" + _minLength + "," + _maxLength + "}$");
        }
    }
}
