﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
using System.DirectoryServices.AccountManagement;
using System.Configuration;

namespace residential.libs
{
    public class ADclass
    {
        private String domain;

        #region Constructors and Destructors

        public ADclass(String _domain)
        {
            domain = _domain;        
        }

        #endregion

        #region public function

        public DataTable getalladuser()
        {
            DataTable returndt = new DataTable();

            PrincipalContext context = new PrincipalContext(ContextType.Domain, domain);
            GroupPrincipal group = GroupPrincipal.FindByIdentity(context, IdentityType.SamAccountName, "Domain Users");
            if (group != null)
            {
                returndt.Columns.Add("USERNAME");
                returndt.Columns.Add("FULLNAME");

                foreach (Principal principal in group.GetMembers(false))
                {
                    DataRow dr = returndt.NewRow();
                    dr["USERNAME"] = principal.SamAccountName;
                    dr["FULLNAME"] = principal.DisplayName;
                    returndt.Rows.Add(dr);
                }
            }

            return returndt;
        }

        public DataTable searchaduserbyname(String _name)
        {
            DataTable returndt = new DataTable();

            PrincipalContext domainContext = new PrincipalContext(ContextType.Domain, domain);
            UserPrincipal user = new UserPrincipal(domainContext);
            user.Name = _name + "*";
            PrincipalSearcher search = new PrincipalSearcher();
            search.QueryFilter = user;
            PrincipalSearchResult<Principal> results = search.FindAll();
            if (results != null && results.Count() > 0)
            {
                returndt.Columns.Add("USERNAME");
                returndt.Columns.Add("FULLNAME");

                foreach (Principal principal in results)
                {
                    DataRow dr = returndt.NewRow();
                    dr["USERNAME"] = principal.SamAccountName;
                    dr["FULLNAME"] = principal.DisplayName;
                    returndt.Rows.Add(dr);
                }

            }
            return returndt;
        }


        public UserPrincipal getuserad(String _username)
        {
            UserPrincipal returnvalue = null;
            string userAD = ConfigurationManager.AppSettings["CRMADUser"].ToString();
            string userADPass = ConfigurationManager.AppSettings["CRMADPassword"].ToString();

            try
            {
                //PrincipalContext context = new PrincipalContext(ContextType.Domain, _username.Split('\\')[0], "useradmindomain", "paswordadmindomain");
                
                PrincipalContext context = new PrincipalContext(ContextType.Domain, _username.Split('\\')[0], userAD, userADPass);
                UserPrincipal user = UserPrincipal.FindByIdentity(context, _username);
                if (user == null)
                {
                    context = new PrincipalContext(ContextType.Domain, _username.Split('\\')[0]);
                    user = UserPrincipal.FindByIdentity(context, _username);
                }

                returnvalue = user;
            }
            catch (Exception ex)
            {
                if (_username.Split('\\')[0].Trim().ToLower() == "karawacinet")
                {
                    _username = _username.Replace(_username.Split('\\')[0], "lippokarawaci");

                    try
                    {
                        PrincipalContext context = new PrincipalContext(ContextType.Domain, _username.Split('\\')[0], userAD, userADPass);
                        UserPrincipal user = UserPrincipal.FindByIdentity(context, _username);
                        if (user == null)
                        {
                            context = new PrincipalContext(ContextType.Domain, _username.Split('\\')[0]);
                            user = UserPrincipal.FindByIdentity(context, _username);
                        }

                        returnvalue = user;
                    }
                    catch (Exception ext)
                    {
                        returnvalue = null;

                    }
                }
            }

            return returnvalue;
        }

        public Boolean isadlogin(String _username, String _password)
        {
            Boolean returnvalue = false;

            PrincipalContext domainContext = new PrincipalContext(ContextType.Domain, domain);
            Boolean isvalidate = domainContext.ValidateCredentials(_username, _password);
            if (isvalidate)
                returnvalue = true;

            return returnvalue;
        }

        #endregion
    }
}
