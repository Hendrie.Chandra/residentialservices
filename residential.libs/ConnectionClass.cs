﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace residential.libs
{
    public class ConnectionClass
    {
        public String DBConnString()
        {
            return ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
        }        

        //public CRMConnectionClass CRMConnection()
        //{
        //    CRMConnectionClass CrmConn = new CRMConnectionClass();
        //    CrmConn.CRMUrlOrg = ConfigurationManager.AppSettings["CRMUrlOrg"].ToString();
        //    CrmConn.CRMOrganization = ConfigurationManager.AppSettings["CRMOrganization"].ToString();
        //    CrmConn.CRMUserName = ConfigurationManager.AppSettings["CRMUserName"].ToString();
        //    CrmConn.CRMPassword = ConfigurationManager.AppSettings["CRMPassword"].ToString();
        //    CrmConn.CRMDomain = ConfigurationManager.AppSettings["CRMDomain"].ToString();

        //    return CrmConn;
        //}

        //public SharePointConnectionClass SharePointConnection()
        //{
        //    SharePointConnectionClass SPConn = new SharePointConnectionClass();
        //    SPConn.SharePointSite = ConfigurationManager.AppSettings["SharePointSite"].ToString();
        //    SPConn.SharePointLibraryName = ConfigurationManager.AppSettings["SharePointLibraryName"].ToString();
        //    SPConn.SharePointCaseListName = ConfigurationManager.AppSettings["SharePointCaseListName"].ToString();
        //    SPConn.SharePointSubSite = ConfigurationManager.AppSettings["SharePointSubSite"].ToString();

        //    return SPConn;
        //}

        //public String UserDomain
        //{
        //    get { return ConfigurationManager.AppSettings["domain"].ToString(); }
        //}
    }

    //public class CRMConnectionClass
    //{
    //    public String CRMUrlOrg { set; get; }
    //    public String CRMOrganization { set; get; }
    //    public String CRMUserName { set; get; }
    //    public String CRMPassword { set; get; }
    //    public String CRMDomain { set; get; }
    //}

    //public class SharePointConnectionClass
    //{
    //    public String SharePointSite { set; get; }
    //    public String SharePointLibraryName { set; get; }
    //    public String SharePointCaseListName { set; get; }
    //    public String SharePointSubSite { set; get; }
    //}
}
