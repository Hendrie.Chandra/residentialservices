﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using residential.libs.spservice.dws;
using residential.libs.spservice.copy;
using residential.libs.spservice.lists;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Net;

using residential.libs.Models;
using System.Xml;

namespace residential.libs
{
    public class SPClass
    {
        #region Fields
        //

        private String spdomain;
        private String spusername;
        private String sppassword;
        private String sharepointsite;
        private IOrganizationService crmservice;

        #endregion

        #region Constructors and Destructors

        //public SPClass(String _domain, String _username, String _password, String _sharepointsite, IOrganizationService _crmservice)
        //{
        //    //
        //    this.spdomain = _domain;
        //    this.spusername = _username;
        //    this.sppassword = _password;
        //    this.sharepointsite = _sharepointsite;
        //    this.crmservice = _crmservice;
        //}

        //public SPClass(SharePointConnectionClass _spconn, CRMConnectionClass _crmconn)
        //{
        //    //
        //    this.spdomain = _crmconn.CRMDomain;
        //    this.spusername = _crmconn.CRMUserName;
        //    this.sppassword = _crmconn.CRMPassword;
        //    this.sharepointsite = _spconn.SharePointSite;

        //    CRMclass crmclass = new CRMclass(_crmconn.CRMUrlOrg, _crmconn.CRMOrganization, _crmconn.CRMUserName, _crmconn.CRMPassword, _crmconn.CRMDomain);
        //    IOrganizationService crmservice = crmclass.crmservice_conn();
        //    this.crmservice = crmservice;
        //}

        #endregion

        #region Private Methods

        private Entity getrecord_byattribute(IOrganizationService _service, String _entityname, String _searchattr, String _searchvalue, String[] _columnset)
        {
            QueryByAttribute query = new QueryByAttribute(_entityname);
            query.ColumnSet = new ColumnSet(_columnset);
            query.Attributes.AddRange(new String[] { _searchattr });
            query.Values.AddRange(new Object[] { _searchvalue });
            EntityCollection entities = _service.RetrieveMultiple(query);
            Entity entity = null;
            if (entities != null)
                if (entities.Entities.Count > 0)
                    entity = entities.Entities[0];

            return entity;
        }    

        #endregion

        #region Public Methods

        //public void sendfiletosharepoint(Entity _entity, String _entityfoldername, Byte[] _filesource, String _filename)
        //{
        //    Entity parentdocumentlocation = getrecord_byattribute(crmservice, "sharepointdocumentlocation", "relativeurl", _entity.LogicalName, new String[] { "relativeurl" });

        //    Dws dwsservice = new Dws();
        //    dwsservice.Url = sharepointsite + "/_vti_bin/dws.asmx";
        //    //dwsservice.Credentials = new NetworkCredential("vsndev\\crmadmin", "Password1!"); //CredentialCache.DefaultCredentials;
        //    dwsservice.Credentials = new NetworkCredential(this.spusername, this.sppassword, this.spdomain);
        //    dwsservice.PreAuthenticate = true;
        //    dwsservice.UnsafeAuthenticatedConnectionSharing = true;
        //    dwsservice.InitializeLifetimeService();

        //    String foldername = "";
        //    if (_entity.Attributes.Contains(_entityfoldername))
        //        foldername = _entity.Attributes[_entityfoldername].ToString();
        //    else
        //        foldername = _entity.Id.ToString();
        //    String result = dwsservice.CreateFolder(_entity.LogicalName + "/" + foldername);

        //    Copy copyservice = new Copy();
        //    copyservice.Url = sharepointsite + "/_vti_bin/copy.asmx"; ;
        //    //copyservice.Credentials = CredentialCache.DefaultCredentials;
        //    copyservice.Credentials = new NetworkCredential(this.spusername, this.sppassword, this.spdomain);
        //    copyservice.PreAuthenticate = true;
        //    copyservice.UnsafeAuthenticatedConnectionSharing = true;
        //    copyservice.InitializeLifetimeService();

        //    String sourceUrl = _filename;
        //    String[] destinationUrl = { sharepointsite + "/" + _entity.LogicalName + "/" + foldername + "/" + _filename };            

        //    spservice.copy.CopyResult copyresult1 = new spservice.copy.CopyResult();
        //    spservice.copy.CopyResult copyresult2 = new spservice.copy.CopyResult();
        //    spservice.copy.CopyResult[] copyresultarray = { copyresult1, copyresult2 };

        //    spservice.copy.FieldInformation fieldinfo = new spservice.copy.FieldInformation();
        //    //fieldinfo.DisplayName = "DisplayName";
        //    //fieldinfo.Type = spservice.copy.FieldType.Text;
        //    //fieldinfo.Value = "Value";

        //    spservice.copy.FieldInformation[] fieldinfoarray = { fieldinfo };

        //    uint copyresult = copyservice.CopyIntoItems(sourceUrl, destinationUrl, fieldinfoarray, _filesource, out copyresultarray);

        //    if (result == "<Result/>")
        //    {
        //        Entity documentlocation = new Entity("sharepointdocumentlocation");
        //        documentlocation.Attributes["name"] = foldername;
        //        documentlocation.Attributes["parentsiteorlocation"] = new EntityReference("sharepointdocumentlocation", parentdocumentlocation.Id);
        //        documentlocation.Attributes["regardingobjectid"] = new EntityReference(_entity.LogicalName, _entity.Id);
        //        documentlocation.Attributes["relativeurl"] = foldername;

        //        crmservice.Create(documentlocation);
        //    }
        //}

        //public SendFileToSharePointResponse sendfiletosharepoint(SendFileToSharePointRequest request)
        //{
        //    SendFileToSharePointResponse response = new SendFileToSharePointResponse();

        //    Entity parentdocumentlocation = getrecord_byattribute(crmservice, "sharepointdocumentlocation", "relativeurl", request.Entity.LogicalName, new String[] { "relativeurl" });

        //    Dws dwsservice = new Dws();
        //    dwsservice.Url = this.sharepointsite + "/_vti_bin/dws.asmx";
        //    //dwsservice.Credentials = CredentialCache.DefaultCredentials; //new NetworkCredential("vsndev\\crmadmin", "Password1!");
        //    dwsservice.Credentials = new NetworkCredential(this.spusername, this.sppassword, this.spdomain);
        //    dwsservice.PreAuthenticate = true;
        //    dwsservice.UnsafeAuthenticatedConnectionSharing = true;
        //    dwsservice.InitializeLifetimeService();

        //    String foldername = "";
        //    if (request.Entity.Attributes.Contains(request.EntityFolderName))
        //        foldername = request.Entity.Attributes[request.EntityFolderName].ToString();
        //    else
        //        foldername = request.Entity.Id.ToString();
        //    String result = dwsservice.CreateFolder(request.Entity.LogicalName + "/" + foldername);

        //    Copy copyservice = new Copy();
        //    copyservice.Url = this.sharepointsite + "/_vti_bin/copy.asmx"; ;
        //    //copyservice.Credentials = CredentialCache.DefaultCredentials;
        //    copyservice.Credentials = new NetworkCredential(this.spusername, this.sppassword, this.spdomain);
        //    copyservice.PreAuthenticate = true;
        //    copyservice.UnsafeAuthenticatedConnectionSharing = true;
        //    copyservice.InitializeLifetimeService();

        //    String sourceUrl = request.FileName;
        //    String[] destinationUrl = { sharepointsite + "/" + request.Entity.LogicalName + "/" + foldername + "/" + request.FileName };

        //    spservice.copy.CopyResult copyresult1 = new spservice.copy.CopyResult();
        //    spservice.copy.CopyResult copyresult2 = new spservice.copy.CopyResult();
        //    spservice.copy.CopyResult[] copyresultarray = { copyresult1, copyresult2 };

        //    spservice.copy.FieldInformation fieldinfo = new spservice.copy.FieldInformation();
        //    //fieldinfo.DisplayName = "DisplayName";
        //    //fieldinfo.Type = spservice.copy.FieldType.Text;
        //    //fieldinfo.Value = "Value";

        //    spservice.copy.FieldInformation[] fieldinfoarray = { fieldinfo };

        //    uint copyresult = copyservice.CopyIntoItems(sourceUrl, destinationUrl, fieldinfoarray, request.FileSource, out copyresultarray);

        //    if (result == "<Result/>")
        //    {

        //        Entity documentlocation = new Entity("sharepointdocumentlocation");
        //        documentlocation.Attributes["name"] = foldername;
        //        documentlocation.Attributes["parentsiteorlocation"] = new EntityReference("sharepointdocumentlocation", parentdocumentlocation.Id);
        //        documentlocation.Attributes["regardingobjectid"] = new EntityReference(request.Entity.LogicalName, request.Entity.Id);
        //        documentlocation.Attributes["relativeurl"] = foldername;

        //        response.DocumentLocationID = crmservice.Create(documentlocation);
        //    }

        //    return response;
        //}

        //public List<RetrieveLibraryResponse> retrievelibrary(RetrieveLibraryRequest request)
        //{
        //    List<RetrieveLibraryResponse> listresponse = new List<RetrieveLibraryResponse>();

        //    Lists listsservice = new Lists();
        //    listsservice.Url = this.sharepointsite + "/_vti_bin/lists.asmx";
        //    //listsservice.Credentials = new NetworkCredential("karawacinet\\crmadmin", "password2!");
        //    listsservice.Credentials = new NetworkCredential(this.spusername, this.sppassword, this.spdomain);
        //    listsservice.PreAuthenticate = true;
        //    listsservice.UnsafeAuthenticatedConnectionSharing = true;
        //    listsservice.InitializeLifetimeService();

        //    XmlDocument xDoc = new XmlDocument();
        //    XmlNode ndQuery = xDoc.CreateNode(XmlNodeType.Element, "Query", "");
        //    XmlNode ndViewFields = xDoc.CreateNode(XmlNodeType.Element, "ViewFields", "");
        //    XmlNode ndQueryOptions = xDoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");
        //    ndQuery.InnerXml = "<Where><Eq><FieldRef Name='FileDirRef' /><Value Type='Text'>" + request.EntityName + "/" + request.RecordName + "</Value></Eq></Where>";            
        //    ndViewFields.InnerXml = "";
        //    ndQueryOptions.InnerXml = "<IncludeMandatoryColumns>True</IncludeMandatoryColumns><ViewAttributes Scope='RecursiveAll' IncludeRootFolder='True' />";

        //    try
        //    {
        //        XmlNode tess = listsservice.GetListCollection();
        //        XmlNode ndListItems = listsservice.GetListItems(request.ListName, "", ndQuery, ndViewFields, "", ndQueryOptions, "");


        //        foreach (XmlNode node in ndListItems.ChildNodes)
        //        {
        //            if (node.Name == "rs:data")
        //            {
        //                foreach (XmlNode childnode in node.ChildNodes)
        //                {
        //                    if (childnode.Name == "z:row")
        //                    {
        //                        RetrieveLibraryResponse response = new RetrieveLibraryResponse();
        //                        foreach (XmlAttribute attr in childnode.Attributes)
        //                        {
        //                            if (attr.Name == "ows_LinkFilename")
        //                                response.Filename = attr.Value;
        //                            if (attr.Name == "ows_EncodedAbsUrl")
        //                                response.SharepointLink = attr.Value;
        //                            if (attr.Name == "ows_ID")
        //                                response.ID = attr.Value;
        //                            if (attr.Name == "ows_Created")
        //                            {
        //                                Int32 year = Convert.ToInt32(attr.Value.Substring(0, 4));
        //                                Int32 month = Convert.ToInt32(attr.Value.Substring(5, 2));
        //                                Int32 day = Convert.ToInt32(attr.Value.Substring(8, 2));
        //                                Int32 hour = Convert.ToInt32(attr.Value.Substring(11, 2));
        //                                Int32 minute = Convert.ToInt32(attr.Value.Substring(14, 2));
        //                                Int32 second = Convert.ToInt32(attr.Value.Substring(17, 2));

        //                                DateTime datecreated = new DateTime(year, month, day, hour, minute, second);
        //                                response.FileCreated = datecreated;
        //                            }
        //                        }
        //                        listresponse.Add(response);
        //                    }
        //                }

        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        listresponse = null;
        //    }

        //    return listresponse;
        //}

        //public void deletelistitems(List<DeleteFileAttachmentRequest> reqlist)
        //{
        //    Lists listsservice = new Lists();
        //    listsservice.Url = this.sharepointsite + "/_vti_bin/lists.asmx";
        //    //dwsservice.Credentials = CredentialCache.DefaultCredentials; //new NetworkCredential("vsndev\\crmadmin", "Password1!");
        //    listsservice.Credentials = new NetworkCredential(this.spusername, this.sppassword, this.spdomain);
        //    listsservice.PreAuthenticate = true;
        //    listsservice.UnsafeAuthenticatedConnectionSharing = true;
        //    listsservice.InitializeLifetimeService();

        //    XmlDocument xDoc = new XmlDocument();
        //    XmlElement elBatch = xDoc.CreateElement("Batch");
        //    elBatch.SetAttribute("OnError", "Continue");
        //    elBatch.InnerXml = "";
        //    for (int i=0; i < reqlist.Count; i++)
        //    {
        //        elBatch.InnerXml += "<Method ID='" + (i + 1).ToString() +"' Cmd='Delete'><Field Name='ID'>" + reqlist[i].ID + "</Field ><Field Name='FileRef'>" + reqlist[i].SharepointLink + "</Field></Method>";
        //    }
        //    XmlNode ndReturn = listsservice.UpdateListItems(reqlist[0].ListName, elBatch);
        //}

        #endregion
    }
}
