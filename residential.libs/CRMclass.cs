﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.HtmlControls;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using System.ServiceModel.Description;

using residential.libs.Models;
using Microsoft.Crm.Sdk.Messages;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;

namespace residential.libs
{
    public class CRMclass
    {

        #region Fields

        //private String urlorg;
        //private String organization;
        //private String username;
        //private String password;
        //private String domain;

        #endregion

        #region Constructors and Destructors

        //public CRMclass(String _urlorg, String _organization,
        //        String _username, String _password, String _domain)
        //{
        //    this.urlorg = _urlorg;
        //    this.organization = _organization;
        //    this.username = _username;
        //    this.password = _password;
        //    this.domain = _domain;
        //}

        //public CRMclass(CRMConnectionClass _crmConn)
        //{
        //    this.urlorg = _crmConn.CRMUrlOrg;
        //    this.organization = _crmConn.CRMOrganization;
        //    this.username = _crmConn.CRMUserName;
        //    this.password = _crmConn.CRMPassword;
        //    this.domain = _crmConn.CRMDomain;
        //}

        #endregion

        #region Private Methods

        //public IOrganizationService crmservice_conn()
        //{
        //    Uri uriorg = new Uri(this.urlorg);
        //    ClientCredentials credentials = new ClientCredentials();
        //    credentials.Windows.ClientCredential.Domain = this.domain;
        //    credentials.Windows.ClientCredential.UserName = this.username;
        //    credentials.Windows.ClientCredential.Password = this.password;
        //    OrganizationServiceProxy serviceproxy = new OrganizationServiceProxy(uriorg, null, credentials, null);
        //    IOrganizationService service;
        //    service = (IOrganizationService)serviceproxy;

        //    return service;
        //}

        //private Entity getrecord_byattribute(IOrganizationService _service, String _entityname, String _searchattr, String _searchvalue, String[] _columnset)
        //{
        //    QueryByAttribute query = new QueryByAttribute(_entityname);
        //    query.ColumnSet = new ColumnSet(_columnset);
        //    query.Attributes.AddRange(new String[] { _searchattr });
        //    query.Values.AddRange(new Object[] { _searchvalue });
        //    EntityCollection entities = _service.RetrieveMultiple(query);
        //    Entity entity = null;
        //    if (entities != null)
        //        if (entities.Entities.Count > 0)
        //            entity = entities.Entities[0];

        //    return entity;        
        //}    

        #endregion

        #region Public Methods

        //public UpdateCaseHelpDeskStatusResponse UpdateCaseHelpDeskStatus(UpdateCaseHelpDeskStatusRequest _request)
        //{
        //    UpdateCaseHelpDeskStatusResponse response = new UpdateCaseHelpDeskStatusResponse();
        //    IOrganizationService service = crmservice_conn();

        //    Entity updatecase = getrecord_byattribute(service, "incident", "ticketnumber", _request.CRMCaseNumber, new String[] { "new_updatehelpdeskstatus" });
        //    updatecase.Attributes["new_helpdeskstatus"] = new OptionSetValue(_request.CRMCaseStatusID);

        //    if (_request.CRMCaseStatusID == 4 && !String.IsNullOrWhiteSpace(_request.WorkedBy))
        //        updatecase.Attributes["new_workedby"] = _request.WorkedBy;

        //    if (_request.CRMCaseStatusID == 7)
        //        updatecase.Attributes["customersatisfactioncode"] = new OptionSetValue(_request.SatisfactionID);
        //    updatecase.Attributes["new_remarks"] = _request.Remarks;
        //    service.Update(updatecase);
        //    response.Result = 0;

        //    Entity tickethistory = new Entity("new_tickethistory");
        //    tickethistory.Attributes["new_caseid"] = new EntityReference("incident", updatecase.Id);
        //    tickethistory.Attributes["new_remarks"] = _request.Remarks;
        //    tickethistory.Attributes["new_helpdeskstatus"] = new OptionSetValue(_request.CRMCaseStatusID);
        //    tickethistory.Attributes["new_updateddate"] = _request.UpdateDate;
        //    tickethistory.Attributes["new_updatedby"] = _request.UpdateBy;
        //    Guid tickethistoryid = service.Create(tickethistory);

        //    return response;        
        //}

        //public InsertCaseResponse InsertCase(InsertCaseRequest _request)
        //{
        //    InsertCaseResponse response = new InsertCaseResponse();
        //    response.CaseID = Guid.Empty;
        //    Guid CaseID = Guid.Empty;
        //    Guid tickethistoryid = Guid.Empty;

        //    IOrganizationService service = crmservice_conn();
            
        //    try
        //    {                
        //        Entity unit = null;

        //        ConditionExpression cond1 = new ConditionExpression();
        //        cond1.AttributeName = "new_name";
        //        cond1.Operator = ConditionOperator.Equal;
        //        cond1.Values.Add(_request.UnitCode);

        //        ConditionExpression cond2 = new ConditionExpression();
        //        cond2.AttributeName = "new_unitno";
        //        cond2.Operator = ConditionOperator.Equal;
        //        cond2.Values.Add(_request.UnitNumber);

        //        FilterExpression filter = new FilterExpression();
        //        filter.Conditions.Add(cond1);
        //        filter.Conditions.Add(cond2);
        //        filter.FilterOperator = LogicalOperator.And;

        //        QueryExpression q_unit = new QueryExpression("new_unit");
        //        q_unit.ColumnSet = new ColumnSet(new String[] { "new_unitcode" });
        //        q_unit.Criteria = filter;
        //        EntityCollection units = service.RetrieveMultiple(q_unit);

        //        if (units != null)
        //            if (units.Entities.Count > 0)
        //                unit = units.Entities[0];

        //        Entity personal = getrecord_byattribute(service, "contact", "new_pscode", _request.PSCode, new String[] { "fullname" });
        //        Entity helpname = service.Retrieve("new_helpname", new Guid(_request.HelpName), new ColumnSet(new String[] { "new_parent" }));

        //        Guid helpcategoryid = Guid.Empty;
        //        Guid helpgroupid = Guid.Empty;
        //        Guid helpnameid = Guid.Empty;

        //        string helpcategory = "";
        //        if (helpname != null)
        //        {
        //            helpnameid = helpname.Id;

        //            if (helpname.Contains("new_parent"))
        //            {
        //                EntityReference helpgroupref = (EntityReference)helpname.Attributes["new_parent"];
        //                helpgroupid = helpgroupref.Id;
        //            }
        //            Entity helpnamegroup = service.Retrieve("new_helpname", helpnameid, new ColumnSet(new String[] { "new_helpcategory" }));
        //            EntityReference helpcategoryref = (EntityReference)helpnamegroup.Attributes["new_helpcategory"];
        //            helpcategoryid = helpcategoryref.Id;

        //            //add by bili
        //            Entity helpcategoryentity = service.Retrieve(helpcategoryref.LogicalName, helpcategoryid, new ColumnSet(new string[] { "new_name" }));
        //            if (helpcategoryentity.Attributes.Contains("new_name"))
        //                helpcategory = helpcategoryentity.Attributes["new_name"].ToString();
        //        }

        //        Entity newcase = new Entity("incident");
        //        newcase.Attributes["new_customertype"] = _request.CustomerType; // == "0" ? false : true;
        //        newcase.Attributes["customerid"] = new EntityReference("contact", personal.Id);
        //        newcase.Attributes["new_unit"] = new EntityReference("new_unit", unit.Id);
        //        newcase.Attributes["new_unitnumber"] = _request.UnitNumber;
        //        newcase.Attributes["new_name"] = _request.NamaPelapor;
        //        newcase.Attributes["new_phone"] = _request.TeleponPelapor;
        //        newcase.Attributes["new_email"] = _request.EmailPelapor;
        //        newcase.Attributes["new_remarks"] = "Case Created";
        //        newcase.Attributes["caseorigincode"] = new OptionSetValue(_request.MediaSource);
        //        newcase.Attributes["new_workedby"] = _request.WorkedBy;

        //        if (helpnameid != Guid.Empty)
        //            newcase.Attributes["new_helpname"] = new EntityReference("new_helpname", helpnameid);

        //        if (helpgroupid != Guid.Empty)
        //            newcase.Attributes["new_helpgroup"] = new EntityReference("new_helpname", helpgroupid);

        //        if (helpcategoryid != Guid.Empty)
        //            newcase.Attributes["new_helpcategory"] = new EntityReference("new_helpcategory", helpcategoryid);

        //        newcase.Attributes["new_description"] = _request.Description;

        //        //add by bili Nov 2017
        //        //complaint need to insert location and sublocation
        //        if (helpcategory.ToUpper() == "COMPLAINT")
        //        {
        //            newcase.Attributes["new_location"] = new EntityReference("new_location", new Guid(_request.CRMLocationID));
        //            newcase.Attributes["new_sublocation"] = new EntityReference("new_sublocation", new Guid(_request.CRMSublocationID));
        //            newcase.Attributes["new_remarks"] = string.Empty;
        //            //update helpdesk status di set oleh plugin crm
        //            //newcase.Attributes["new_updatehelpdeskstatus"] = new OptionSetValue(2);     //default for complaint Assign to PIC
        //        }

        //        if (String.IsNullOrEmpty(_request.CRMCaseID))
        //        {
        //            CaseID = service.Create(newcase);
        //            response.CaseID = CaseID;

        //            QueryByAttribute query = new QueryByAttribute("systemuser");
        //            query.ColumnSet = new ColumnSet(new String[] { "domainname" });
        //            query.Attributes.AddRange("domainname");
        //            query.Values.AddRange(_request.UserName);

        //            EntityCollection systemusers = service.RetrieveMultiple(query);
        //            Entity systemuser = systemusers.Entities[0];

        //            AssignRequest assignrequest = new AssignRequest();
        //            assignrequest.Assignee = new EntityReference("systemuser", systemuser.Id);
        //            assignrequest.Target = new EntityReference("incident", CaseID);
        //            service.Execute(assignrequest);

        //            newcase = service.Retrieve("incident", CaseID, new ColumnSet(new String[] { "ticketnumber", "new_helpcategory", "followupby" }));
        //            if (newcase != null)
        //            {
        //                if (newcase.Attributes.Contains("ticketnumber"))
        //                    response.CaseNumber = newcase.Attributes["ticketnumber"].ToString();
        //                if (newcase.Attributes.Contains("new_helpcategory"))
        //                    helpcategory = ((EntityReference)newcase.Attributes["new_helpcategory"]).Name;
        //            }
        //            Entity tickethistory = new Entity("new_tickethistory");
        //            tickethistory.Attributes["new_caseid"] = new EntityReference("incident", CaseID);
        //            string Remarks = "Case Created";
        //            tickethistory.Attributes["new_helpdeskstatus"] = new OptionSetValue(1);                   
        //            tickethistory.Attributes["new_remarks"] = Remarks;
        //            tickethistory.Attributes["new_updateddate"] = DateTime.Now;
        //            tickethistory.Attributes["new_updatedby"] = _request.FullName;

        //            //add by bili nov 2017
        //            if (newcase.Attributes.Contains("followupby"))
        //                response.OverdueDate = DateTime.Parse(newcase.Attributes["followupby"].ToString());

        //            if (helpcategory.ToUpper() == "COMPLAINT" || helpcategory.ToUpper() == "INFORMATION")
        //            {
        //               //complaint & information insert ticket history setelah save case ke tmdb
        //            }
        //            else
        //            {
        //                tickethistoryid = service.Create(tickethistory);
        //            }
                    
        //        }
        //        else
        //        {
        //            CaseID = Guid.Parse(_request.CRMCaseID);
        //            newcase.Id = CaseID;
        //            service.Update(newcase);
        //            response.CaseID = CaseID;

        //            newcase = service.Retrieve("incident", CaseID, new ColumnSet(new String[] { "ticketnumber" }));
        //            if (newcase != null)
        //                if (newcase.Attributes.Contains("ticketnumber"))
        //                    response.CaseNumber = newcase.Attributes["ticketnumber"].ToString();                    
        //        }               
        //    }
        //    catch (Exception e)
        //    {
        //        response.ExceptionMessage = e.Message;
        //        if (CaseID != Guid.Empty)
        //            service.Delete("incident", CaseID);

        //        if (tickethistoryid != Guid.Empty)
        //            service.Delete("new_tickethistory", tickethistoryid);
        //    }
        //    return response;        
        //}

        //public void Delete_Case_Record(Guid _CaseID)
        //{
        //    IOrganizationService service = crmservice_conn();
        //    service.Delete("incident", _CaseID);
        //}

        //public void Create_Ticket_History(String _CaseNumber, Int32 _HelpDeskStatus, String _Remarks, String _UpdatedBy)
        //{
        //    IOrganizationService service = crmservice_conn();

        //    QueryByAttribute findcaseid = new QueryByAttribute("incident");
        //    findcaseid.ColumnSet = new ColumnSet(new String[] { "ticketnumber" });
        //    findcaseid.Attributes.AddRange("ticketnumber");
        //    findcaseid.Values.AddRange(_CaseNumber);
        //    EntityCollection caseref = service.RetrieveMultiple(findcaseid);

        //    if (caseref.Entities.Count > 0)
        //    {
        //        Entity tickethistory = new Entity("new_tickethistory");
        //        tickethistory.Attributes["new_caseid"] = new EntityReference("incident", caseref.Entities[0].Id);
        //        tickethistory.Attributes["new_remarks"] = _Remarks;
        //        tickethistory.Attributes["new_helpdeskstatus"] = new OptionSetValue(_HelpDeskStatus);
        //        tickethistory.Attributes["new_updatedby"] = _UpdatedBy;
        //        tickethistory.Attributes["new_updateddate"] = DateTime.Now;
        //        service.Create(tickethistory);
        //    }
        //}

        //public void Show_CRM_Attachment(String _CaseNumber, HtmlGenericControl _Control)
        //{
        //    IOrganizationService service = crmservice_conn();

        //    QueryExpression query = new QueryExpression()
        //    {
        //        Distinct = false,
        //        EntityName = "annotation",
        //        ColumnSet = new ColumnSet(true)
        //    };

        //    query.AddLink("incident", "objectid", "incidentid").
        //    LinkCriteria.AddCondition("ticketnumber", ConditionOperator.Equal, _CaseNumber);
        //    EntityCollection Annos = service.RetrieveMultiple(query);

        //    foreach (Entity ent in Annos.Entities)
        //    {
        //        HyperLink hyperlink = new HyperLink();
        //        hyperlink.Text = ent.Attributes["filename"].ToString();
        //        hyperlink.NavigateUrl = "CRMAttachmentHandler.ashx?AnnotationID=" + ent.Attributes["annotationid"].ToString();
        //        hyperlink.CssClass = "AttachmentLink";
        //        _Control.Controls.Add(hyperlink);
        //    }        
        //}

        //new 2018
        //private string RandomString(int size)
        //{
        //    string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        //    Random rand = new Random();
        //    char[] chars = new char[size];
        //    for (int i = 0; i < size; i++)
        //    {
        //        chars[i] = Alphabet[rand.Next(Alphabet.Length)];
        //    }
        //    return new string(chars);
        //}

        #endregion

    }
}
