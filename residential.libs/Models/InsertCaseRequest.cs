﻿using System;
using System.Runtime.Serialization;

namespace residential.libs.Models
{
    [DataContract]
    public class InsertCaseRequest
    {
        /*add by bili*/
        [DataMember]
        public Int32 OrgID { set; get; }
        [DataMember]
        public Int32 SiteID { set; get; }
        /*add by bili*/

        [DataMember]
        public Boolean CustomerType { set; get; }

        [DataMember]
        public String PSCode { set; get; }

        [DataMember]
        public String UnitCode { set; get; }

        [DataMember]
        public String UnitNumber { set; get; }

        [DataMember]
        public String NamaPelapor { set; get; }

        [DataMember]
        public String TeleponPelapor { set; get; }

        [DataMember]
        public String EmailPelapor { set; get; }

        [DataMember]
        public Int32 MediaSource { set; get; }

        [DataMember]
        public String Description { set; get; }

        //[DataMember]
        //public String HelpName { set; get; }

        [DataMember]
        public Int32 HelpID { set; get; }

        [DataMember]
        public Double TotalPrice { set; get; }

        [DataMember]
        public Double ActualPrice { set; get; }

        [DataMember]
        public String FullName { set; get; }

        [DataMember]
        public String UserName { set; get; }

        //[DataMember]
        //public String CRMCaseID { set; get; }

        [DataMember]
        public String CaseNumber { set; get; }

        [DataMember]
        public byte[] RequestInvoice { set; get; }

        [DataMember]
        public String WorkedBy { set; get; }

        //add by bili 13 Nov 2017 from complaint and information
        [DataMember]
        public String CRMLocationID { set; get; }

        [DataMember]
        public String CRMSublocationID { set; get; }
    }
}
