﻿using System;
using System.Runtime.Serialization;

namespace residential.libs.Models
{
    [DataContract]
    public class RetrieveLibraryRequest
    {
        [DataMember]
        public String EntityName { get; set; }

        [DataMember]
        public String ListName { get; set; }

        [DataMember]
        public String RecordName { get; set; }
    }
}
