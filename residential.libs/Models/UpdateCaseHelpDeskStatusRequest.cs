﻿using System;
using System.Runtime.Serialization;

namespace residential.libs.Models
{
    [DataContract]
    public class UpdateCaseHelpDeskStatusRequest
    {
        [DataMember]
        public String CRMCaseID { set; get; }

        [DataMember]
        public String CRMCaseNumber { set; get; }

        [DataMember]
        public Int32 CRMCaseStatusID { set; get; }

        [DataMember]
        public String Remarks { set; get; }

        [DataMember]
        public DateTime UpdateDate { set; get; }

        [DataMember]
        public String UpdateBy { set; get; }

        [DataMember]
        public String WorkedBy { set; get; }

        [DataMember]
        public Int32 SatisfactionID { set; get; }
    }
}
