﻿using System;
using System.Runtime.Serialization;

namespace residential.libs.Models
{
    [DataContract]
    public class UpdateCaseHelpDeskStatusResponse
    {
        [DataMember]
        public Int32 Result { get; set; }
    }
}
