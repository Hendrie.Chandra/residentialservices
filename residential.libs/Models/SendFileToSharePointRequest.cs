﻿using System;
using System.Runtime.Serialization;
using Microsoft.Xrm.Sdk;

namespace residential.libs.Models
{
    [DataContract]
    public class SendFileToSharePointRequest
    {
        [DataMember]
        public Entity Entity { set; get; }

        [DataMember]
        public String EntityFolderName { set; get; }

        [DataMember]
        public Byte[] FileSource { set; get; }

        [DataMember]
        public String FileName { set; get; }

    }
}
