﻿using System;
using System.Runtime.Serialization;

namespace residential.libs.Models
{
    [DataContract]
    public class DeleteFileAttachmentRequest
    {
        [DataMember]
        public String ListName { get; set; }

        [DataMember]
        public string SharepointLink { get; set; }

        [DataMember]
        public string ID { get; set; }
    }    
}
