﻿using System;
using System.Runtime.Serialization;

namespace residential.libs.Models
{
    [DataContract]
    public class InsertCaseResponse
    {
        [DataMember]
        public Guid CaseID { get; set; }

        [DataMember]
        public String CaseNumber { get; set; }

        [DataMember]
        public String HelpCategory { get; set; }        //add by bili nov 2017

        [DataMember]
        public DateTime? OverdueDate { get; set; }        //add by bili nov 2017

        [DataMember]
        public String ExceptionMessage { get; set; }
    }
}
