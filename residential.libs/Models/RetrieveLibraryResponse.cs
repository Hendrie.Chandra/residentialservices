﻿using System;
using System.Runtime.Serialization;

namespace residential.libs.Models
{
    [DataContract]
    public class RetrieveLibraryResponse
    {
        [DataMember]
        public String Filename { get; set; }

        [DataMember]
        public String SharepointLink { get; set; }

        [DataMember]
        public String ID { get; set; }

        [DataMember]
        public DateTime FileCreated { get; set; }
    }
}
