﻿using System;
using System.Runtime.Serialization;

namespace residential.libs.Models
{
    [DataContract]
    public class SendFileToSharePointResponse
    {
        [DataMember]
        public Guid DocumentLocationID { get; set; }
    }
}
