﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using System.Configuration;
using System.Windows.Forms;
using System.IO;

namespace residential.batchHelpnameAndMapping
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                IOrganizationService service = crmservice_conn();

                string filepath;
                StreamWriter log;
                Guid _helpname;

                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Filter = "CSV Files|*.csv";

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    filepath = dialog.FileName.ToString();

                    List<string> line = new List<string>();
                    List<string> duration = new List<string>();
                    List<string> name = new List<string>();
                    List<string> parent = new List<string>();
                    List<string> helpcategory = new List<string>();
                    List<string> department = new List<string>();
                    List<string> ipklincludable = new List<string>();
                    List<string> rsviewformname = new List<string>();
                    List<string> owner = new List<string>();
                    List<string> sublocation = new List<string>();

                    String logpath = "Helpname_Department_Mapping-" + DateTime.Now.ToString() + ".txt";
                    logpath = logpath.Replace("/", "").Replace(":", "");
                    logpath = ConfigurationManager.AppSettings["Logpath"].ToString() + logpath;

                    if (!File.Exists(logpath))
                    {
                        log = new StreamWriter(logpath);
                    }
                    else
                    {
                        log = new StreamWriter(logpath, true);
                    }

                    log.WriteLine("-----------------------" + DateTime.Now + "-----------------------");
                    try
                    {
                        string baris;
                        int count = 0;
                        int helpnamecreated = 0;
                        int departmentmappingcreated = 0;
                        using (StreamReader readFile = new StreamReader(filepath))
                        {
                            //string baris;
                            //int count = 0;
                            while ((baris = readFile.ReadLine()) != null)
                            {
                                String[] kolom = baris.Split(';');
                                if (kolom.Length == 9)
                                {
                                    line.Add(baris);
                                    duration.Add(kolom.ElementAt<string>(0));
                                    name.Add(kolom.ElementAt<string>(1));
                                    parent.Add(kolom.ElementAt<string>(2));
                                    helpcategory.Add(kolom.ElementAt<string>(3));
                                    department.Add(kolom.ElementAt<string>(4));
                                    ipklincludable.Add(kolom.ElementAt<string>(5));
                                    rsviewformname.Add(kolom.ElementAt<string>(6));
                                    owner.Add(kolom.ElementAt<string>(7));
                                    sublocation.Add(kolom.ElementAt<string>(8));
                                    count++;
                                }
                                else
                                {
                                    Console.WriteLine("ERROR: Baris: \"" + baris + "\" memiliki jumlah elemen yang salah");
                                    log.WriteLine("ERROR: Baris: \"" + baris + "\" memiliki jumlah elemen yang salah");
                                }
                            }

                            if (count > 0)
                            {
                                line.RemoveAt(0);
                                duration.RemoveAt(0);
                                name.RemoveAt(0);
                                parent.RemoveAt(0);
                                helpcategory.RemoveAt(0);
                                department.RemoveAt(0);
                                ipklincludable.RemoveAt(0);
                                rsviewformname.RemoveAt(0);
                                owner.RemoveAt(0);
                                sublocation.RemoveAt(0);
                                count--;
                            }

                            for (int i = 0; i < count; i++)
                            {
                                try
                                {
                                    int _ipklincludable;
                                    if (int.TryParse(ipklincludable[i], out _ipklincludable))
                                    {
                                        int _duration;
                                        if (int.TryParse(duration[i], out _duration))
                                        {
                                            if (_ipklincludable == 0 || _ipklincludable == 1)
                                            {
                                                #region Main Operation
                                                if (!String.IsNullOrEmpty(duration[i]) && !String.IsNullOrEmpty(name[i]) && !String.IsNullOrEmpty(helpcategory[i]) && !String.IsNullOrEmpty(owner[i]))
                                                {
                                                    #region Check & Retrieve Helpname

                                                    crmretrieve cr_helpname = new crmretrieve(service, "new_helpname");

                                                    //Name
                                                    cr_helpname.addCondition("new_name", ConditionOperator.Equal, name[i]);
                                                    //State Active
                                                    cr_helpname.addCondition("statecode", ConditionOperator.Equal, 0);
                                                    //Help Category
                                                    cr_helpname.addLinkCondition("new_helpcategory", "new_helpcategory", "new_helpcategoryid", "new_name", ConditionOperator.Equal, helpcategory[i]);
                                                    //Owner
                                                    cr_helpname.addLinkCondition("owningteam", "team", "teamid", "name", ConditionOperator.Equal, owner[i]);

                                                    if (!String.IsNullOrEmpty(parent[i]))
                                                    {
                                                        //Parent
                                                        cr_helpname.addLinkCondition("new_parent", "new_helpname", "new_helpnameid", "new_name", ConditionOperator.Equal, parent[i]);
                                                        //Help Category of Parent
                                                        cr_helpname.addLink2Condition("new_parent", "new_helpname", "new_helpnameid", "new_helpcategory", "new_helpcategory", "new_helpcategoryid", "new_name", ConditionOperator.Equal, helpcategory[i]);
                                                        //Owner of Parent
                                                        cr_helpname.addLink2Condition("new_parent", "new_helpname", "new_helpnameid", "owningteam", "team", "teamid", "name", ConditionOperator.Equal, owner[i]);
                                                    }

                                                    EntityCollection checkselfresult = cr_helpname.retrieveQuery();

                                                    #endregion

                                                    #region Create New Helpname if it doesn't already exist
                                                    if (checkselfresult == null || checkselfresult.Entities.Count == 0)
                                                    {
                                                        Entity newhelpname = new Entity("new_helpname");
                                                        //duration
                                                        newhelpname.Attributes["new_duration"] = _duration;
                                                        //name
                                                        newhelpname.Attributes["new_name"] = name[i];
                                                        //Ipkl Includable
                                                        newhelpname.Attributes["new_ipklincludable"] = Convert.ToBoolean(_ipklincludable);
                                                        //RS View Form Name
                                                        if (!String.IsNullOrEmpty(rsviewformname[i]))
                                                            newhelpname.Attributes["new_rsviewformname"] = rsviewformname[i];

                                                        //Check & Retrieve Parent
                                                        if (!String.IsNullOrEmpty(parent[i]))
                                                        {
                                                            crmretrieve cr_parent = new crmretrieve(service, "new_helpname");
                                                            //Parent Name
                                                            cr_parent.addCondition("new_name", ConditionOperator.Equal, parent[i]);
                                                            //Parent State Active
                                                            cr_parent.addCondition("statecode", ConditionOperator.Equal, 0);
                                                            //Parent with no Parent
                                                            cr_parent.addCondition("new_parent", ConditionOperator.Null, 0);
                                                            //Parent Team
                                                            cr_parent.addLinkCondition("owningteam", "team", "teamid", "name", ConditionOperator.Equal, owner[i]);
                                                            //Parent Help Category
                                                            cr_parent.addLinkCondition("new_helpcategory", "new_helpcategory", "new_helpcategoryid", "new_name", ConditionOperator.Equal, helpcategory[i]);

                                                            EntityCollection retrieveparentresult = cr_parent.retrieveQuery();

                                                            if (retrieveparentresult != null && retrieveparentresult.Entities.Count > 0)
                                                            {
                                                                //If Parent Exists
                                                                newhelpname.Attributes["new_parent"] = new EntityReference("new_helpname", retrieveparentresult[0].Id);
                                                            }
                                                            else
                                                            {
                                                                //If Parent Doesn't Exist, create Parent

                                                                //Retrieve Entity Team
                                                                crmretrieve cr_team = new crmretrieve(service, "team");
                                                                Entity team = cr_team.getrecord_byattribute("name", owner[i], new ColumnSet(false));

                                                                //Retrieve Entity Team
                                                                crmretrieve cr_category = new crmretrieve(service, "new_helpcategory");
                                                                Entity category = cr_category.getrecord_byattribute("new_name", helpcategory[i], new ColumnSet(false));

                                                                //Create New Parent
                                                                Entity new_parent = new Entity("new_helpname");
                                                                new_parent.Attributes["new_name"] = parent[i];
                                                                new_parent.Attributes["ownerid"] = new EntityReference("team", team.Id);
                                                                new_parent.Attributes["new_helpcategory"] = new EntityReference("new_helpcategory", category.Id);
                                                                Guid new_parentid = service.Create(new_parent);
                                                                newhelpname.Attributes["new_parent"] = new EntityReference("new_helpname", new_parentid);
                                                            }
                                                        }

                                                        //Check & Retrieve HelpCategory
                                                        crmretrieve cr_helpcategory = new crmretrieve(service, "new_helpcategory");
                                                        cr_helpcategory.addCondition("new_name", ConditionOperator.Equal, helpcategory[i]);
                                                        cr_helpcategory.addCondition("statecode", ConditionOperator.Equal, 0);
                                                        EntityCollection helpcategoryqueryresult = cr_helpcategory.retrieveQuery();

                                                        if (helpcategoryqueryresult != null && helpcategoryqueryresult.Entities.Count > 0)
                                                        {
                                                            newhelpname.Attributes["new_helpcategory"] = new EntityReference("new_helpcategory", helpcategoryqueryresult[0].Id);
                                                        }

                                                        //Check & Insert Owner
                                                        crmretrieve cr_owner = new crmretrieve(service, "team");
                                                        Entity retrievedowner = cr_owner.getrecord_byattribute("name", owner[i], new ColumnSet(false));
                                                        if (retrievedowner != null)
                                                        {
                                                            newhelpname.Attributes["ownerid"] = new EntityReference("team", retrievedowner.Id);
                                                        }

                                                        _helpname = service.Create(newhelpname);
                                                        helpnamecreated++;

                                                    }
                                                    //If helpname already exist
                                                    else
                                                    {
                                                        _helpname = checkselfresult[0].Id;

                                                        //update duration
                                                        Entity updateHelpname = service.Retrieve("new_helpname", _helpname, new ColumnSet(true));
                                                        if (updateHelpname.Contains("new_duration"))
                                                            updateHelpname.Attributes["new_duration"] = _duration;
                                                        else
                                                            updateHelpname.Attributes.Add("new_duration", _duration);                                                        
                                                        service.Update(updateHelpname);
                                                    }
                                                    #endregion

                                                    #region Department Mapping Table Operation
                                                    if (helpcategory[i].ToUpper() == "COMPLAINT")
                                                    {
                                                        //Retrieve Sublocation
                                                        if (!String.IsNullOrEmpty(department[i]) && !String.IsNullOrEmpty(sublocation[i]))
                                                        {
                                                            crmretrieve cr_subloc = new crmretrieve(service, "new_sublocation");
                                                            cr_subloc.addCondition("new_name", ConditionOperator.Equal, sublocation[i]);
                                                            cr_subloc.addLinkCondition("owningteam", "team", "teamid", "name", ConditionOperator.Equal, owner[i]);
                                                            EntityCollection sublocs = cr_subloc.retrieveQuery();
                                                            if (sublocs != null && sublocs.Entities.Count > 0)
                                                            {
                                                                //get guid of department
                                                                crmretrieve cr_dept = new crmretrieve(service, "new_department");
                                                                cr_dept.addCondition("new_name", ConditionOperator.Equal, department[i]);
                                                                cr_dept.addLinkCondition("owningteam", "team", "teamid", "name", ConditionOperator.Equal, owner[i]);
                                                                EntityCollection dept = cr_dept.retrieveQuery();
                                                                if (dept != null && dept.Entities.Count == 1)
                                                                {
                                                                    foreach (Entity ent in sublocs.Entities)
                                                                    {
                                                                        //Check if already exist in department mapping
                                                                        crmretrieve cr_map = new crmretrieve(service, "new_departmentmapping");
                                                                        cr_map.addCondition("new_helpname", ConditionOperator.Equal, _helpname.ToString());
                                                                        cr_map.addCondition("new_sublocation", ConditionOperator.Equal, ent.Id.ToString());
                                                                        cr_map.addLinkCondition("new_department", "new_department", "new_departmentid", "new_name", ConditionOperator.Equal, department[i]);
                                                                        EntityCollection depmap = cr_map.retrieveQuery();

                                                                        //if doesn't exist, create it
                                                                        if (depmap == null || depmap.Entities.Count == 0)
                                                                        {
                                                                            Entity newdepmap = new Entity("new_departmentmapping");
                                                                            newdepmap.Attributes["new_helpname"] = new EntityReference("new_helpname", _helpname);
                                                                            newdepmap.Attributes["new_department"] = new EntityReference("new_department", dept[0].Id);
                                                                            newdepmap.Attributes["new_sublocation"] = new EntityReference("new_department", ent.Id);
                                                                            service.Create(newdepmap);
                                                                            departmentmappingcreated++;
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Console.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Department tidak dikenali");
                                                                    log.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Department tidak dikenali");
                                                                }
                                                            }
                                                            //else
                                                            //{
                                                            //    Console.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Sublocation pada site tersebut tidak dikenali");
                                                            //    log.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Sublocation pada site tersebut tidak dikenali");
                                                            //}
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" Departemen dan SubLocation wajib diisi");
                                                            log.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" Departemen dan SubLocation wajib diisi");
                                                        }
                                                    }
                                                    else if (helpcategory[i].ToUpper() == "REQUEST" || helpcategory[i].ToUpper() == "INFORMATION")
                                                    {
                                                        //Retrieve Sublocation
                                                        if (!String.IsNullOrEmpty(department[i]))
                                                        {
                                                            //get guid of department
                                                            crmretrieve cr_dept = new crmretrieve(service, "new_department");
                                                            cr_dept.addCondition("new_name", ConditionOperator.Equal, department[i]);
                                                            cr_dept.addLinkCondition("owningteam", "team", "teamid", "name", ConditionOperator.Equal, owner[i]);
                                                            EntityCollection dept = cr_dept.retrieveQuery();
                                                            if (dept != null && dept.Entities.Count == 1)
                                                            {
                                                                //Check if already exist in department mapping
                                                                crmretrieve cr_map = new crmretrieve(service, "new_departmentmapping");
                                                                cr_map.addCondition("new_helpname", ConditionOperator.Equal, _helpname.ToString());
                                                                cr_map.addLinkCondition("new_department", "new_department", "new_departmentid", "new_name", ConditionOperator.Equal, department[i]);
                                                                EntityCollection depmap = cr_map.retrieveQuery();

                                                                //if doesn't exist, create it
                                                                if (depmap == null || depmap.Entities.Count == 0)
                                                                {
                                                                    Entity newdepmap = new Entity("new_departmentmapping");
                                                                    newdepmap.Attributes["new_helpname"] = new EntityReference("new_helpname", _helpname);
                                                                    newdepmap.Attributes["new_department"] = new EntityReference("new_department", dept[0].Id);
                                                                    service.Create(newdepmap);
                                                                    departmentmappingcreated++;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                Console.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Department tidak dikenali");
                                                                log.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Department tidak dikenali");
                                                            }
                                                        }
                                                        else
                                                        {
                                                            Console.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" Departemen wajib diisi");
                                                            log.WriteLine((i + 1).ToString() + " [Department Mapping] - ERROR: Baris : \"" + line[i] + "\" Departemen wajib diisi");
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    Console.WriteLine((i + 1).ToString() + " [Helpname] - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Kolom duration, name, helpcategory, dan owner wajib diisi");
                                                    log.WriteLine((i + 1).ToString() + " [Helpname] - ERROR: Baris : \"" + line[i] + "\" tidak diproses. Kolom duration, name, helpcategory, dan owner wajib diisi");
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                Console.WriteLine((i + 1).ToString() + " [Helpname] - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom ipklincludable yang salah");
                                                log.WriteLine((i + 1).ToString() + " [Helpname] - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom ipklincludable yang salah");
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine((i + 1).ToString() + " [Helpname] - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom duration yang salah");
                                            log.WriteLine((i + 1).ToString() + " [Helpname] - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom duration yang salah");
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine((i + 1).ToString() + " [Helpname] - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom ipklincludable yang salah");
                                        log.WriteLine((i + 1).ToString() + " [Helpname] - ERROR: Baris: \"" + line[i] + "\" memiliki nilai pada kolom ipklincludable yang salah");
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine((i + 1).ToString() + " - ERROR: Record gagal dibuat untuk line berikut: " + line[i] + "\n" + ex.ToString());
                                    log.WriteLine((i + 1).ToString() + " - ERROR: Record gagal dibuat untuk line berikut: " + line[i] + "\n" + ex.ToString());
                                }
                            }
                        }
                        log.WriteLine();
                        log.WriteLine("Helpname Created: " + helpnamecreated.ToString());
                        log.WriteLine("Department Mapping Created: " + departmentmappingcreated.ToString());
                        log.WriteLine("--------- FINISHED ---------");
                        log.Close();
                        Console.WriteLine("Helpname Created: " + helpnamecreated.ToString());
                        Console.WriteLine("Department Mapping Created: " + departmentmappingcreated.ToString());
                        Console.WriteLine("--------- FINISHED ---------");
                        Console.ReadLine();
                        //tambah komen lagi

                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("===ERROR=== " + ex.ToString());
                        log.WriteLine("===ERROR=== " + ex.ToString());
                        log.WriteLine();
                        log.WriteLine();
                        log.Close();
                        Console.ReadLine();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        static IOrganizationService crmservice_conn()
        {
            Uri uriorg = new Uri(ConfigurationManager.AppSettings["CRMUrlOrg"].ToString());
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential.Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            credentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            credentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            OrganizationServiceProxy serviceproxy = new OrganizationServiceProxy(uriorg, null, credentials, null);
            IOrganizationService service = (IOrganizationService)serviceproxy;

            return service;
        }


    }
}
