﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace residential.batchHelpnameAndMapping
{
    public class crmretrieve
    {
        private QueryExpression main_qe;
        private FilterExpression main_fe;
        private String _entityname;
        private IOrganizationService _service;

        public crmretrieve(IOrganizationService service, string EntityName)
        {
            _service = service;
            _entityname = EntityName;
            main_qe = new QueryExpression(_entityname);
            main_fe = new FilterExpression();
        }

        public void addCondition(string attributeName, ConditionOperator conditionOperator, Object value)
        {
            ConditionExpression ce = new ConditionExpression();
            ce.AttributeName = attributeName;
            ce.Operator = conditionOperator;
            if (conditionOperator != ConditionOperator.Null && conditionOperator != ConditionOperator.NotNull)
                ce.Values.Add(value);
            main_fe.AddCondition(ce);
        }

        public void addLinkCondition(string fromAttributeName, string toEntityName, string toAttributeName, string attributeName, ConditionOperator conditionOperator, Object value)
        {
            LinkEntity le = new LinkEntity { LinkFromEntityName = _entityname, LinkFromAttributeName = fromAttributeName, LinkToEntityName = toEntityName, LinkToAttributeName = toAttributeName };
            FilterExpression fe = new FilterExpression();
            ConditionExpression ce = new ConditionExpression();
            ce.AttributeName = attributeName;
            ce.Operator = conditionOperator;
            if (conditionOperator != ConditionOperator.Null && conditionOperator != ConditionOperator.NotNull)
                ce.Values.Add(value);

            fe.AddCondition(ce);
            le.LinkCriteria.AddFilter(fe);
            main_qe.LinkEntities.Add(le);
        }

        public void addLink2Condition(string fromAttributeName1, string toEntityName1, string toAttributeName1, string fromAttributeName2, string toEntityName2, string toAttributeName2, string attributeName, ConditionOperator conditionOperator, Object value)
        {
            LinkEntity le1 = new LinkEntity { LinkFromEntityName = _entityname, LinkFromAttributeName = fromAttributeName1, LinkToEntityName = toEntityName1, LinkToAttributeName = toAttributeName1 };
            FilterExpression fe1 = new FilterExpression();
            ConditionExpression ce1 = new ConditionExpression();

            LinkEntity le2 = new LinkEntity { LinkFromEntityName = toEntityName1, LinkFromAttributeName = fromAttributeName2, LinkToEntityName = toEntityName2, LinkToAttributeName = toAttributeName2 };
            FilterExpression fe2 = new FilterExpression();
            ConditionExpression ce2 = new ConditionExpression();

            ce2.AttributeName = attributeName;
            ce2.Operator = conditionOperator;
            if (conditionOperator != ConditionOperator.Null && conditionOperator != ConditionOperator.NotNull)
                ce2.Values.Add(value);

            fe2.AddCondition(ce2);
            le2.LinkCriteria.AddFilter(fe2);
            le1.LinkEntities.Add(le2);

            main_qe.LinkEntities.Add(le1);
        }

        public EntityCollection retrieveQuery()
        {
            main_qe.Criteria.AddFilter(main_fe);
            return _service.RetrieveMultiple(main_qe);
        }

        public Entity getrecord_byattribute(String _searchattr, String _searchvalue, ColumnSet _columnset)
        {
            QueryByAttribute query = new QueryByAttribute(_entityname);
            query.ColumnSet = _columnset;
            query.Attributes.AddRange(new String[] { _searchattr });
            query.Values.AddRange(new Object[] { _searchvalue });
            EntityCollection entities = _service.RetrieveMultiple(query);
            Entity entity = null;
            if (entities != null)
                if (entities.Entities.Count > 0)
                    entity = entities.Entities[0];

            return entity;
        }
    }

    
}
