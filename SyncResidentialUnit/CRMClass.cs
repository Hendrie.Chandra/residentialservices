﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;


namespace SyncResidentialUnit
{
    class CRMClass
    {
        public static IOrganizationService CrmConnection(Uri uriorg, string userName, string password, string domain)
        {
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential.UserName = userName;
            credentials.Windows.ClientCredential.Password = password;
            credentials.Windows.ClientCredential.Domain = domain;


            OrganizationServiceProxy serviceProxy = new OrganizationServiceProxy(uriorg, null, credentials, null);
            IOrganizationService service;
            service = (IOrganizationService)serviceProxy;

            return service;
        }

        

    }
}
