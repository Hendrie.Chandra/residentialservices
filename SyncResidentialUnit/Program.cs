﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SyncResidentialUnit
{
    class Program
    {
        static void Main(string[] args)
        {            
            Uri uriorg = new Uri(ConfigurationManager.AppSettings["CRMUrlOrg"]);
            string userName = ConfigurationManager.AppSettings["CRMUsername"];
            string password = ConfigurationManager.AppSettings["CRMPassword"];
            string domain = ConfigurationManager.AppSettings["CRMDomain"];

            IOrganizationService service = CRMClass.CrmConnection(uriorg, userName, password, domain);            
            string connectionString = connectionString = ConfigurationManager.ConnectionStrings["rsdb"].ToString();
            string LogPath = ConfigurationManager.AppSettings["Log"].ToString();

            SyncUnitOwner(service, connectionString, LogPath);
            SyncUnitTenant(service, connectionString, LogPath);
        }

        static private void SyncUnitOwner(IOrganizationService _service, String _connectionstring, String _logpath)
        {
            SqlConnection con = new SqlConnection(_connectionstring);
            con.Open();
            string sql = "SELECT " +
	                        "a.UnitCode, a.UnitNo, a.OwnerPSCode, " +
	                        "b.UnitAreaCode, c.UnitAreaDesc, " +
	                        "b.UnitCategoryCode, d.UnitCategoryDesc, " +
	                        "b.UnitClusterCode, e.UnitClusterDesc " +
                        "FROM "+
	                        "TRUnitOwner a " + 
	                        "LEFT JOIN MSUnit b on a.OrgID = b.OrgID AND a.SiteID = b.SiteID AND a.UnitCode = b.UnitCode AND a.UnitNo = b.UnitNo " +
	                        "LEFT JOIN MSUnitArea c ON b.OrgID = c.OrgID AND b.SiteID = c.SiteID AND b.UnitAreaCode = c.UnitAreaCode " +
	                        "LEFT JOIN MSUnitCategory d ON b.OrgID = d.OrgID AND b.SiteID = d.SiteID AND b.UnitCategoryCode = d.UnitCategoryCode " +
	                        "LEFT JOIN MSUnitCluster e ON b.OrgID = e.OrgID AND b.SiteID = e.SiteID AND b.UnitClusterCode = e.UnitClusterCode " +
                        "WHERE " +
	                        "a.IsActive = 1 and a.SiteID = 5";
            SqlCommand cmd = new SqlCommand(sql, con);
            //cmd.CommandType = CommandType.Text;

            SqlDataReader reader = cmd.ExecuteReader();

            //define team name for lippo cikarang
            string fetchXmlTeam = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                      <entity name='team'>
                                        <attribute name='name' />
                                        <attribute name='businessunitid' />
                                        <attribute name='teamid' />
                                        <order attribute='name' descending='false' />
                                        <filter type='and'>
                                          <condition attribute='name' operator='eq' value='Lippo Cikarang' />
                                        </filter>
                                      </entity>
                                    </fetch>";

            EntityCollection teams = _service.RetrieveMultiple(new FetchExpression(fetchXmlTeam));
            Guid? teamID = null;
            if (teams.Entities.Count > 0)
                teamID = teams.Entities[0].Id;
            else
                return;

            if (reader.HasRows)
            {
                int i = 1;
                while (reader.Read())
                {
                    try
                    {
                        Console.WriteLine("Processing data " + i.ToString() + " Unit Code : " + reader["UnitCode"].ToString() + " Unit No : " + reader["UnitNo"].ToString() + " Owner PSCode : " + reader["OwnerPSCode"].ToString());
                        //cek apakah pscodenya ada?
                        string fetchXmlPersonal = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                          <entity name='contact'>
                                                            <attribute name='fullname' />
                                                            <attribute name='contactid' />
                                                            <attribute name='new_pscode' />
                                                            <filter type='and'>
                                                              <condition attribute='new_pscode' operator='eq' value='" + reader["OwnerPSCode"].ToString() + @"' />
                                                              <condition attribute='statecode' operator='eq' value='0' />
                                                            </filter>
                                                          </entity>
                                                        </fetch>";

                        EntityCollection personals = _service.RetrieveMultiple(new FetchExpression(fetchXmlPersonal));

                        if (personals.Entities.Count > 0)
                        {
                            Entity personal = personals.Entities[0];

                            //cek, apakah unit sudah ada?
                            string fetcXmlUnit = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                              <entity name='new_unit'>
                                                <attribute name='new_name' />
                                                <attribute name='new_unitno' />
                                                <attribute name='new_unitcode' />
                                                <attribute name='new_unitid' />
                                                <attribute name='new_unitowner' />
                                                <attribute name='new_unittenant' />
                                                <attribute name='ownerid' />
                                                <filter type='and'>
                                                  <condition attribute='new_name' operator='eq' value='" + reader["UnitCode"].ToString() + @"' />
                                                  <condition attribute='new_unitno' operator='eq' value='" + reader["UnitNo"].ToString() + @"' />
                                                  <condition attribute='ownerid' operator='eq' value='" + teamID + @"' />
                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                </filter>
                                              </entity>
                                            </fetch>";

                            EntityCollection units = _service.RetrieveMultiple(new FetchExpression(fetcXmlUnit));
                            if (units.Entities.Count == 0)
                            {
                                //insert unit here

                                //cek apakah unit code sudah ada di unit master
                                string fetchXmlUnitMaster = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_unitmaster'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_unitcode' />
                                                                <attribute name='new_unitmasterid' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_name' operator='eq' value='" + reader["UnitCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                EntityCollection unitmasters = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitMaster));

                                if (unitmasters.Entities.Count == 0)
                                {
                                    //jika belum ada, create master unit dan insert unit
                                    Entity unitmaster = new Entity("new_unitmaster");
                                    unitmaster.Attributes["new_name"] = reader["UnitCode"].ToString();
                                    unitmaster.Attributes["new_unitcode"] = reader["UnitCode"].ToString();
                                    Guid unitmasterGuid = _service.Create(unitmaster);

                                    if (unitmasterGuid != null)
                                    {
                                        //insert unit
                                        Entity unit = new Entity("new_unit");
                                        unit.Attributes["new_unitcode"] = new EntityReference("new_unitmaster", unitmasterGuid);
                                        unit.Attributes["new_unitno"] = reader["UnitNo"].ToString();
                                        unit.Attributes["new_name"] = reader["UnitCode"].ToString();
                                        unit.Attributes["new_unitowner"] = new EntityReference("contact", personal.Id);

                                        //cek unit area
                                        string fetchXmlUnitArea = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_area'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_areacode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_areacode' operator='eq' value='" + reader["UnitAreaCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                        EntityCollection unitareas = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitArea));
                                        if (unitareas.Entities.Count == 0)
                                        {
                                            //insert unit area
                                            if (reader["UnitAreaDesc"] != DBNull.Value)
                                            {
                                                Entity unitarea = new Entity("new_area");
                                                unitarea.Attributes["new_areacode"] = reader["UnitAreaCode"].ToString();
                                                unitarea.Attributes["new_name"] = reader["UnitAreaDesc"].ToString();
                                                Guid unitareaid = _service.Create(unitarea);

                                                unit.Attributes["new_areacode"] = new EntityReference("new_area", unitareaid);
                                            }

                                        } else
                                        {
                                            Entity unitarea = unitareas.Entities[0];
                                            unit.Attributes["new_areacode"] = new EntityReference("new_area", unitarea.Id);
                                        }

                                        //cek unit category
                                        string fetchXmlUnitCategory = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_category'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_categorycode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_categorycode' operator='eq' value='" + reader["UnitCategoryCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                        EntityCollection unitcategories = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitCategory));
                                        if (unitcategories.Entities.Count == 0)
                                        {
                                            //insert unit category
                                            if (reader["UnitCategoryDesc"] != DBNull.Value)
                                            {
                                                Entity unitcategory = new Entity("new_category");
                                                unitcategory.Attributes["new_categorycode"] = reader["UnitCategoryCode"].ToString();
                                                unitcategory.Attributes["new_name"] = reader["UnitCategoryDesc"].ToString();
                                                Guid unitcategoryid = _service.Create(unitcategory);

                                                unit.Attributes["new_categorycode"] = new EntityReference("new_category", unitcategoryid);
                                            }
                                        }
                                        else
                                        {
                                            Entity unitcategory = unitcategories.Entities[0];
                                            unit.Attributes["new_categorycode"] = new EntityReference("new_category", unitcategory.Id);
                                        }

                                        //cek unit cluster
                                        string fetchXmlUnitCluster = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_mastercluster'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_clustercode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_clustercode' operator='eq' value='" + reader["UnitClusterCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                        EntityCollection unitclusters = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitCluster));
                                        if (unitclusters.Entities.Count == 0)
                                        {
                                            //insert unit cluster
                                            if (reader["UnitClusterDesc"] != DBNull.Value)
                                            {
                                                Entity unitcluster = new Entity("new_mastercluster");
                                                unitcluster.Attributes["new_clustercode"] = reader["UnitClusterCode"].ToString();
                                                unitcluster.Attributes["new_name"] = reader["UnitClusterDesc"].ToString();
                                                Guid unitclusterid = _service.Create(unitcluster);

                                                unit.Attributes["new_clustercode"] = new EntityReference("new_mastercluster", unitclusterid);
                                            }
                                        }
                                        else
                                        {
                                            Entity unitcluster = unitclusters.Entities[0];
                                            unit.Attributes["new_clustercode"] = new EntityReference("new_mastercluster", unitcluster.Id);
                                        }

                                        Guid unitGuid = _service.Create(unit);

                                        var request = new AssignRequest
                                        {
                                            Assignee = new EntityReference("team", teamID.Value),
                                            Target = new EntityReference("new_unit", unitGuid)
                                        };
                                        _service.Execute(request);

                                        LogingClass.WriteSuccesLog("SyncResidentialUnit_success_log", "UnitCode : " + reader["UnitCode"].ToString() + " Unit No : " + reader["UnitNo"].ToString() + " PsCode: " + reader["OwnerpsCode"].ToString() + " Success");
                                        i++;
                                        Console.Clear();
                                    }
                                }
                                else
                                {
                                    //insert unit
                                    Entity masterunit = unitmasters.Entities[0];
                                    Entity unit = new Entity("new_unit");
                                    unit.Attributes["new_unitcode"] = new EntityReference("new_unitmaster", masterunit.Id);
                                    unit.Attributes["new_unitno"] = reader["UnitNo"].ToString();
                                    unit.Attributes["new_name"] = reader["UnitCode"].ToString();
                                    unit.Attributes["new_unitowner"] = new EntityReference("contact", personal.Id);

                                    //cek unit area
                                    string fetchXmlUnitArea = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_area'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_areacode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_areacode' operator='eq' value='" + reader["UnitAreaCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                    EntityCollection unitareas = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitArea));
                                    if (unitareas.Entities.Count == 0)
                                    {
                                        //insert unit area
                                        if (reader["UnitAreaDesc"] != DBNull.Value)
                                        {
                                            Entity unitarea = new Entity("new_area");
                                            unitarea.Attributes["new_areacode"] = reader["UnitAreaCode"].ToString();
                                            unitarea.Attributes["new_name"] = reader["UnitAreaDesc"].ToString();
                                            Guid unitareaid = _service.Create(unitarea);

                                            unit.Attributes["new_areacode"] = new EntityReference("new_area", unitareaid);
                                        }

                                    }
                                    else
                                    {
                                        Entity unitarea = unitareas.Entities[0];
                                        unit.Attributes["new_areacode"] = new EntityReference("new_area", unitarea.Id);
                                    }

                                    //cek unit category
                                    string fetchXmlUnitCategory = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_category'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_categorycode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_categorycode' operator='eq' value='" + reader["UnitCategoryCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                    EntityCollection unitcategories = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitCategory));
                                    if (unitcategories.Entities.Count == 0)
                                    {
                                        //insert unit category
                                        if (reader["UnitCategoryDesc"] != DBNull.Value)
                                        {
                                            Entity unitcategory = new Entity("new_category");
                                            unitcategory.Attributes["new_categorycode"] = reader["UnitCategoryCode"].ToString();
                                            unitcategory.Attributes["new_name"] = reader["UnitCategoryDesc"].ToString();
                                            Guid unitcategoryid = _service.Create(unitcategory);

                                            unit.Attributes["new_categorycode"] = new EntityReference("new_category", unitcategoryid);
                                        }
                                    }
                                    else
                                    {
                                        Entity unitcategory = unitcategories.Entities[0];
                                        unit.Attributes["new_categorycode"] = new EntityReference("new_category", unitcategory.Id);
                                    }

                                    //cek unit cluster
                                    string fetchXmlUnitCluster = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_mastercluster'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_clustercode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_clustercode' operator='eq' value='" + reader["UnitClusterCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                    EntityCollection unitclusters = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitCluster));
                                    if (unitclusters.Entities.Count == 0)
                                    {
                                        //insert unit cluster
                                        if (reader["UnitClusterDesc"] != DBNull.Value)
                                        {
                                            Entity unitcluster = new Entity("new_mastercluster");
                                            unitcluster.Attributes["new_clustercode"] = reader["UnitClusterCode"].ToString();
                                            unitcluster.Attributes["new_name"] = reader["UnitClusterDesc"].ToString();
                                            Guid unitclusterid = _service.Create(unitcluster);

                                            unit.Attributes["new_clustercode"] = new EntityReference("new_mastercluster", unitclusterid);
                                        }
                                    }
                                    else
                                    {
                                        Entity unitcluster = unitclusters.Entities[0];
                                        unit.Attributes["new_clustercode"] = new EntityReference("new_mastercluster", unitcluster.Id);
                                    }

                                    Guid unitGuid = _service.Create(unit);

                                    var request = new AssignRequest
                                    {
                                        Assignee = new EntityReference("team", teamID.Value),
                                        Target = new EntityReference("new_unit", unitGuid)
                                    };
                                    _service.Execute(request);

                                    LogingClass.WriteSuccesLog("SyncResidentialUnit_success_log", "UnitCode : " + reader["UnitCode"].ToString() + " Unit No : " + reader["UnitNo"].ToString() + " PsCode: " + reader["OwnerpsCode"].ToString() + " Success");
                                    i++;
                                    Console.Clear();
                                }
                            }
                            else
                            {
                                //unit sudah ada
                                //cek apakah unit ownernya ada?
                                Entity unit = units.Entities[0];
                                Entity updateunit = new Entity("new_unit");
                                updateunit.Id = unit.Id;
                                updateunit.Attributes["new_unitowner"] = new EntityReference("contact", personal.Id);
                                _service.Update(updateunit);
                                i++;
                                Console.Clear();
                            }
                        }
                        else
                        {
                            //data personal tidak ada
                            LogingClass.WriteSuccesLog("SyncResidentialUnit_personal_log", "UnitCode : " + reader["UnitCode"].ToString() + " Unit No : " + reader["UnitNo"].ToString() + " PsCode: " + reader["OwnerpsCode"].ToString() + " Personal tidak ada");
                            i++;
                            Console.Clear();
                        }
                    }
                    catch (Exception ex)
                    {
                        LogingClass.WriteErrorLog("SyncResidentialUnit_error_log", "Oops, an error occured with message : " + ex.Message);
                        i++;
                        Console.Clear();
                    }
                }
            }//end if reader
        }

        static private void SyncUnitTenant(IOrganizationService _service, String _connectionstring, String _logpath)
        {
            SqlConnection con = new SqlConnection(_connectionstring);
            con.Open();
            string sql = "SELECT " +
                            "a.UnitCode, a.UnitNo, a.TenantPSCode, " +
                            "b.UnitAreaCode, c.UnitAreaDesc, " +
                            "b.UnitCategoryCode, d.UnitCategoryDesc, " +
                            "b.UnitClusterCode, e.UnitClusterDesc " +
                        "FROM " +
                            "TRUnitTenant a " +
                            "LEFT JOIN MSUnit b on a.OrgID = b.OrgID AND a.SiteID = b.SiteID AND a.UnitCode = b.UnitCode AND a.UnitNo = b.UnitNo " +
                            "LEFT JOIN MSUnitArea c ON b.OrgID = c.OrgID AND b.SiteID = c.SiteID AND b.UnitAreaCode = c.UnitAreaCode " +
                            "LEFT JOIN MSUnitCategory d ON b.OrgID = d.OrgID AND b.SiteID = d.SiteID AND b.UnitCategoryCode = d.UnitCategoryCode " +
                            "LEFT JOIN MSUnitCluster e ON b.OrgID = e.OrgID AND b.SiteID = e.SiteID AND b.UnitClusterCode = e.UnitClusterCode " +
                        "WHERE " +
                            "a.IsActive = 1 and a.SiteID = 5";
            SqlCommand cmd = new SqlCommand(sql, con);
            //cmd.CommandType = CommandType.Text;

            SqlDataReader reader = cmd.ExecuteReader();

            //define team name for lippo cikarang
            string fetchXmlTeam = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                      <entity name='team'>
                                        <attribute name='name' />
                                        <attribute name='businessunitid' />
                                        <attribute name='teamid' />
                                        <order attribute='name' descending='false' />
                                        <filter type='and'>
                                          <condition attribute='name' operator='eq' value='Lippo Cikarang' />
                                        </filter>
                                      </entity>
                                    </fetch>";

            EntityCollection teams = _service.RetrieveMultiple(new FetchExpression(fetchXmlTeam));
            Guid? teamID = null;
            if (teams.Entities.Count > 0)
                teamID = teams.Entities[0].Id;
            else
                return;

            if (reader.HasRows)
            {
                int i = 1;
                while (reader.Read())
                {
                    try
                    {
                        Console.WriteLine("Processing data " + i.ToString() + " Unit Code : " + reader["UnitCode"].ToString() + " Unit No : " + reader["UnitNo"].ToString() + " Tenant PSCode : " + reader["TenantPSCode"].ToString());
                        //cek apakah pscodenya ada?
                        string fetchXmlPersonal = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                          <entity name='contact'>
                                                            <attribute name='fullname' />
                                                            <attribute name='contactid' />
                                                            <attribute name='new_pscode' />
                                                            <filter type='and'>
                                                              <condition attribute='new_pscode' operator='eq' value='" + reader["TenantPSCode"].ToString() + @"' />
                                                              <condition attribute='statecode' operator='eq' value='0' />
                                                            </filter>
                                                          </entity>
                                                        </fetch>";

                        EntityCollection personals = _service.RetrieveMultiple(new FetchExpression(fetchXmlPersonal));

                        if (personals.Entities.Count > 0)
                        {
                            Entity personal = personals.Entities[0];

                            //cek, apakah unit sudah ada?
                            string fetcXmlUnit = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                              <entity name='new_unit'>
                                                <attribute name='new_name' />
                                                <attribute name='new_unitno' />
                                                <attribute name='new_unitcode' />
                                                <attribute name='new_unitid' />
                                                <attribute name='new_unitowner' />
                                                <attribute name='new_unittenant' />
                                                <attribute name='ownerid' />
                                                <filter type='and'>
                                                  <condition attribute='new_name' operator='eq' value='" + reader["UnitCode"].ToString() + @"' />
                                                  <condition attribute='new_unitno' operator='eq' value='" + reader["UnitNo"].ToString() + @"' />
                                                  <condition attribute='ownerid' operator='eq' value='" + teamID + @"' />
                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                </filter>
                                              </entity>
                                            </fetch>";

                            EntityCollection units = _service.RetrieveMultiple(new FetchExpression(fetcXmlUnit));
                            if (units.Entities.Count == 0)
                            {
                                //insert unit here

                                //cek apakah unit code sudah ada di unit master
                                string fetchXmlUnitMaster = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_unitmaster'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_unitcode' />
                                                                <attribute name='new_unitmasterid' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_name' operator='eq' value='" + reader["UnitCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                EntityCollection unitmasters = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitMaster));

                                if (unitmasters.Entities.Count == 0)
                                {
                                    //jika belum ada, create master unit dan insert unit
                                    Entity unitmaster = new Entity("new_unitmaster");
                                    unitmaster.Attributes["new_name"] = reader["UnitCode"].ToString();
                                    unitmaster.Attributes["new_unitcode"] = reader["UnitCode"].ToString();
                                    Guid unitmasterGuid = _service.Create(unitmaster);

                                    if (unitmasterGuid != null)
                                    {
                                        //insert unit
                                        Entity unit = new Entity("new_unit");
                                        unit.Attributes["new_unitcode"] = new EntityReference("new_unitmaster", unitmasterGuid);
                                        unit.Attributes["new_unitno"] = reader["UnitNo"].ToString();
                                        unit.Attributes["new_name"] = reader["UnitCode"].ToString();
                                        unit.Attributes["new_unittenant"] = new EntityReference("contact", personal.Id);

                                        //cek unit area
                                        string fetchXmlUnitArea = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_area'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_areacode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_areacode' operator='eq' value='" + reader["UnitAreaCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                        EntityCollection unitareas = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitArea));
                                        if (unitareas.Entities.Count == 0)
                                        {
                                            //insert unit area
                                            if (reader["UnitAreaDesc"] != DBNull.Value)
                                            {
                                                Entity unitarea = new Entity("new_area");
                                                unitarea.Attributes["new_areacode"] = reader["UnitAreaCode"].ToString();
                                                unitarea.Attributes["new_name"] = reader["UnitAreaDesc"].ToString();
                                                Guid unitareaid = _service.Create(unitarea);

                                                unit.Attributes["new_areacode"] = new EntityReference("new_area", unitareaid);
                                            }

                                        }
                                        else
                                        {
                                            Entity unitarea = unitareas.Entities[0];
                                            unit.Attributes["new_areacode"] = new EntityReference("new_area", unitarea.Id);
                                        }

                                        //cek unit category
                                        string fetchXmlUnitCategory = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_category'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_categorycode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_categorycode' operator='eq' value='" + reader["UnitCategoryCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                        EntityCollection unitcategories = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitCategory));
                                        if (unitcategories.Entities.Count == 0)
                                        {
                                            //insert unit category
                                            if (reader["UnitCategoryDesc"] != DBNull.Value)
                                            {
                                                Entity unitcategory = new Entity("new_category");
                                                unitcategory.Attributes["new_categorycode"] = reader["UnitCategoryCode"].ToString();
                                                unitcategory.Attributes["new_name"] = reader["UnitCategoryDesc"].ToString();
                                                Guid unitcategoryid = _service.Create(unitcategory);

                                                unit.Attributes["new_categorycode"] = new EntityReference("new_category", unitcategoryid);
                                            }
                                        }
                                        else
                                        {
                                            Entity unitcategory = unitcategories.Entities[0];
                                            unit.Attributes["new_categorycode"] = new EntityReference("new_category", unitcategory.Id);
                                        }

                                        //cek unit cluster
                                        string fetchXmlUnitCluster = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_mastercluster'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_clustercode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_clustercode' operator='eq' value='" + reader["UnitClusterCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                        EntityCollection unitclusters = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitCluster));
                                        if (unitclusters.Entities.Count == 0)
                                        {
                                            //insert unit cluster
                                            if (reader["UnitClusterDesc"] != DBNull.Value)
                                            {
                                                Entity unitcluster = new Entity("new_mastercluster");
                                                unitcluster.Attributes["new_clustercode"] = reader["UnitClusterCode"].ToString();
                                                unitcluster.Attributes["new_name"] = reader["UnitClusterDesc"].ToString();
                                                Guid unitclusterid = _service.Create(unitcluster);

                                                unit.Attributes["new_clustercode"] = new EntityReference("new_mastercluster", unitclusterid);
                                            }
                                        }
                                        else
                                        {
                                            Entity unitcluster = unitclusters.Entities[0];
                                            unit.Attributes["new_clustercode"] = new EntityReference("new_mastercluster", unitcluster.Id);
                                        }

                                        Guid unitGuid = _service.Create(unit);

                                        var request = new AssignRequest
                                        {
                                            Assignee = new EntityReference("team", teamID.Value),
                                            Target = new EntityReference("new_unit", unitGuid)
                                        };
                                        _service.Execute(request);

                                        LogingClass.WriteSuccesLog("SyncResidentialUnit_success_log", "UnitCode : " + reader["UnitCode"].ToString() + " Unit No : " + reader["UnitNo"].ToString() + " PsCode: " + reader["TenantPSCode"].ToString() + " Success");
                                        i++;
                                        Console.Clear();
                                    }
                                }
                                else
                                {
                                    //insert unit
                                    Entity masterunit = unitmasters.Entities[0];
                                    Entity unit = new Entity("new_unit");
                                    unit.Attributes["new_unitcode"] = new EntityReference("new_unitmaster", masterunit.Id);
                                    unit.Attributes["new_unitno"] = reader["UnitNo"].ToString();
                                    unit.Attributes["new_name"] = reader["UnitCode"].ToString();
                                    unit.Attributes["new_unittenant"] = new EntityReference("contact", personal.Id);

                                    //cek unit area
                                    string fetchXmlUnitArea = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_area'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_areacode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_areacode' operator='eq' value='" + reader["UnitAreaCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                    EntityCollection unitareas = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitArea));
                                    if (unitareas.Entities.Count == 0)
                                    {
                                        //insert unit area
                                        if (reader["UnitAreaDesc"] != DBNull.Value)
                                        {
                                            Entity unitarea = new Entity("new_area");
                                            unitarea.Attributes["new_areacode"] = reader["UnitAreaCode"].ToString();
                                            unitarea.Attributes["new_name"] = reader["UnitAreaDesc"].ToString();
                                            Guid unitareaid = _service.Create(unitarea);

                                            unit.Attributes["new_areacode"] = new EntityReference("new_area", unitareaid);
                                        }

                                    }
                                    else
                                    {
                                        Entity unitarea = unitareas.Entities[0];
                                        unit.Attributes["new_areacode"] = new EntityReference("new_area", unitarea.Id);
                                    }

                                    //cek unit category
                                    string fetchXmlUnitCategory = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_category'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_categorycode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_categorycode' operator='eq' value='" + reader["UnitCategoryCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                    EntityCollection unitcategories = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitCategory));
                                    if (unitcategories.Entities.Count == 0)
                                    {
                                        //insert unit category
                                        if (reader["UnitCategoryDesc"] != DBNull.Value)
                                        {
                                            Entity unitcategory = new Entity("new_category");
                                            unitcategory.Attributes["new_categorycode"] = reader["UnitCategoryCode"].ToString();
                                            unitcategory.Attributes["new_name"] = reader["UnitCategoryDesc"].ToString();
                                            Guid unitcategoryid = _service.Create(unitcategory);

                                            unit.Attributes["new_categorycode"] = new EntityReference("new_category", unitcategoryid);
                                        }
                                    }
                                    else
                                    {
                                        Entity unitcategory = unitcategories.Entities[0];
                                        unit.Attributes["new_categorycode"] = new EntityReference("new_category", unitcategory.Id);
                                    }

                                    //cek unit cluster
                                    string fetchXmlUnitCluster = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                              <entity name='new_mastercluster'>
                                                                <attribute name='new_name' />
                                                                <attribute name='new_clustercode' />
                                                                <filter type='and'>
                                                                  <condition attribute='new_clustercode' operator='eq' value='" + reader["UnitClusterCode"].ToString() + @"' />
                                                                  <condition attribute='statecode' operator='eq' value='0' />
                                                                </filter>
                                                              </entity>
                                                            </fetch>";

                                    EntityCollection unitclusters = _service.RetrieveMultiple(new FetchExpression(fetchXmlUnitCluster));
                                    if (unitclusters.Entities.Count == 0)
                                    {
                                        //insert unit cluster
                                        if (reader["UnitClusterDesc"] != DBNull.Value)
                                        {
                                            Entity unitcluster = new Entity("new_mastercluster");
                                            unitcluster.Attributes["new_clustercode"] = reader["UnitClusterCode"].ToString();
                                            unitcluster.Attributes["new_name"] = reader["UnitClusterDesc"].ToString();
                                            Guid unitclusterid = _service.Create(unitcluster);

                                            unit.Attributes["new_clustercode"] = new EntityReference("new_mastercluster", unitclusterid);
                                        }
                                    }
                                    else
                                    {
                                        Entity unitcluster = unitclusters.Entities[0];
                                        unit.Attributes["new_clustercode"] = new EntityReference("new_mastercluster", unitcluster.Id);
                                    }

                                    Guid unitGuid = _service.Create(unit);

                                    var request = new AssignRequest
                                    {
                                        Assignee = new EntityReference("team", teamID.Value),
                                        Target = new EntityReference("new_unit", unitGuid)
                                    };
                                    _service.Execute(request);

                                    LogingClass.WriteSuccesLog("SyncResidentialUnit_success_log", "UnitCode : " + reader["UnitCode"].ToString() + " Unit No : " + reader["UnitNo"].ToString() + " PsCode: " + reader["TenantPSCode"].ToString() + " Success");
                                    i++;
                                    Console.Clear();
                                }
                            }
                            else
                            {
                                //unit sudah ada
                                Entity unit = units.Entities[0];
                                Entity updateunit = new Entity("new_unit");
                                updateunit.Id = unit.Id;
                                updateunit.Attributes["new_unittenant"] = new EntityReference("contact", personal.Id);
                                _service.Update(updateunit);
                                i++;
                                Console.Clear();
                            }
                        }
                        else
                        {
                            //data personal tidak ada
                            LogingClass.WriteSuccesLog("SyncResidentialUnit_personal_log", "UnitCode : " + reader["UnitCode"].ToString() + " Unit No : " + reader["UnitNo"].ToString() + " PsCode: " + reader["TenantPSCode"].ToString() + " Personal tidak ada");
                            i++;
                            Console.Clear();
                        }
                    }
                    catch (Exception ex)
                    {
                        LogingClass.WriteErrorLog("SyncResidentialUnit_error_log", "Oops, an error occured with message : " + ex.Message);
                        i++;
                        Console.Clear();
                    }
                }
            }//end if reader
        }

    }
}
