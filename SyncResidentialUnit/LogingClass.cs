﻿using System;
using System.Configuration;
using System.IO;
namespace SyncResidentialUnit
{
    class LogingClass
    {
        public static string CreateLogFile(string errorLogName)
        {
            string logPath = ConfigurationManager.AppSettings["Log"].ToString();
            string fullLogPath = logPath + errorLogName + ".txt";

            if (!File.Exists(fullLogPath))
            {
                File.Create(fullLogPath).Close();
            }

            return fullLogPath;
        }

        public static void WriteErrorLog(string errorlogName, string errorMessage) 
        {
            string logFile = LogingClass.CreateLogFile(errorlogName);
            StreamWriter st_witer = File.AppendText(logFile);
            st_witer.WriteLine(DateTime.Now.ToString() + ";" + errorMessage);
            st_witer.Close();
        }

        public static void WriteSuccesLog(string successlogName, string successMessage)
        {
            string logFile = LogingClass.CreateLogFile(successlogName);
            StreamWriter st_witer = File.AppendText(logFile);
            st_witer.WriteLine(DateTime.Now.ToString() + ";" + successMessage);
            st_witer.Close();
        }
    }
}

