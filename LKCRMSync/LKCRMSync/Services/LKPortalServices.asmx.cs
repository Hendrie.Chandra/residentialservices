﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using log4net;
using System.Data;
using LKCRMSync.Classes;
using System.ServiceModel;

namespace LKCRMSync.Services
{
    /// <summary>
    /// Summary description for LKPortalServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class LKPortalServices : System.Web.Services.WebService
    {

        [WebMethod]
        public string Get()
        {
            return "Hello World";
        }
    }
}
