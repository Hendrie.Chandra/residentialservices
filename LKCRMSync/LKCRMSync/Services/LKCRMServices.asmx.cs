﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using log4net;
using System.Data;
using LKCRMSync.Classes;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using System.ServiceModel.Description;
using System.Configuration;

namespace LKCRMSync.Services
{
    /// <summary>
    /// Summary description for LKCRMServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class LKCRMServices : System.Web.Services.WebService
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(LKCRMServices));  

        [WebMethod]
        public string SyncUnitPsCodeCRM()
        {
            log4net.Config.XmlConfigurator.Configure();
            string result = "Success";

            log.Info("LKCRMServices.SyncUnitPsCodeCRM - START");

            try
            {
                log.Debug("LKCRMServices.SyncUnitPsCodeCRM - START - clsLKCRMServices.SyncAlihHakUnit()");
                clsLKCRMServices.SyncAlihHakUnit();
                log.Debug("LKCRMServices.SyncUnitPsCodeCRM - END - clsLKCRMServices.SyncAlihHakUnit()");

                log.Debug("LKCRMServices.SyncUnitPsCodeCRM - START - clsLKCRMServices.GetChangeListFromTMDB()");
                DataTable dt = clsLKCRMServices.GetChangeListFromTMDB();
                log.Debug("LKCRMServices.SyncUnitPsCodeCRM - END - clsLKCRMServices.GetChangeListFromTMDB() Result = " + dt.Rows.Count + " Data");

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        CRMUnit.updateUnitRequest CRMUnitRequest = new CRMUnit.updateUnitRequest();

                        CRMUnitRequest.site = dt.Rows[i]["SiteID"].ToString();

                        CRMUnitRequest.unitcode = dt.Rows[i]["UnitCode"].ToString();

                        CRMUnitRequest.unitno = dt.Rows[i]["UnitNo"].ToString();

                        CRMUnitRequest.unitstatuscode = "Sold";

                        if (dt.Rows[i]["OwnerPSCode"].ToString() != "")
                        {
                            CRMUnitRequest.ownerpscode = dt.Rows[i]["OwnerPSCode"].ToString();
                        }

                        if (dt.Rows[i]["TenantPsCode"].ToString() != "")
                        {
                            CRMUnitRequest.tenantpscode = dt.Rows[i]["TenantPsCode"].ToString();
                        }

                        CRMUnit.UnitServiceClient Service = new CRMUnit.UnitServiceClient();

                        log.Debug("LKCRMServices.SyncUnitPsCodeCRM - START -  Service.updateUnit(CRMUnitRequest) SiteID=" + dt.Rows[i]["SiteID"].ToString() +
                                    ", UnitCode=" + dt.Rows[i]["UnitCode"].ToString() + ", UnitNo=" + dt.Rows[i]["UnitNo"].ToString() +
                                    ", OwnerPsCode=" + dt.Rows[i]["OwnerPsCode"].ToString() + ", TenantPsCode=" + dt.Rows[i]["TenantPsCode"].ToString());

                        var a = Service.updateUnit(CRMUnitRequest);

                        log.Debug("LKCRMServices.SyncUnitPsCodeCRM - END -  Service.updateUnit(CRMUnitRequest) SiteID=" + dt.Rows[i]["SiteID"].ToString() +
                                    ", UnitCode=" + dt.Rows[i]["UnitCode"].ToString() + ", UnitNo=" + dt.Rows[i]["UnitNo"].ToString() +
                                    ", OwnerPsCode=" + dt.Rows[i]["OwnerPsCode"].ToString() + ", TenantPsCode=" + dt.Rows[i]["TenantPsCode"].ToString() +
                                    ". Result= " + a.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                result = "Error";
                log.Error("LKCRMServices.SyncUnitPsCodeCRM - ERROR=" + ex.ToString());
            }

            log.Info("LKCRMServices.SyncUnitPsCodeCRM - END");

            return result;
        }

        [WebMethod]
        public string AutoCaseClose()
        {
            log4net.Config.XmlConfigurator.Configure();
            string result = "";

            log.Info("LKCRMServices.AutoCaseClose - START");

            try
            {
                IOrganizationService serviceadmin = clsLKCRMServices.CrmConnection();

                Int32 DaysToClosed = Int32.Parse(ConfigurationManager.AppSettings["DaysToClosed"].ToString());

                log.Debug("LKCRMServices.AutoCaseClose - START - GetProgressCompletedList");

                DataTable data = clsLKCRMServices.GetProgressCompletedList();

                log.Debug("LKCRMServices.AutoCaseClose - END   - GetProgressCompletedList");

                if (data != null)
                {
                    log.Info("LKCRMServices.AutoCaseClose - LOOP  - Data " + data.Rows.Count + " Rows");

                    foreach (DataRow dtRow in data.Rows)
                    {
                        if ((DateTime.Now - DateTime.Parse(dtRow["ModifiedDate"].ToString())).Days >= DaysToClosed)
                        {
                            String NomorCase = dtRow["CaseNumber"].ToString();
                            String CRMCaseID = dtRow["CRMCaseID"].ToString();
                            String CreatedDate = dtRow["CreatedDate"].ToString();

                            log.Debug("LKCRMServices.AutoCaseClose - START  - clsLKCRMServices.Update_Status_CaseClosed CaseNumber =" + NomorCase + " CRMCaseID=" + CRMCaseID);

                            result = clsLKCRMServices.Update_Status_CaseClosed(NomorCase, CRMCaseID);

                            log.Debug("LKCRMServices.AutoCaseClose - END    - clsLKCRMServices.Update_Status_CaseClosed CaseNumber =" + NomorCase + " CRMCaseID=" + CRMCaseID + " Result=" + result);
                        }
                        else
                        {
                            log.Info("LKCRMServices.AutoCaseClose - LOOP  - CaseNumber=" + dtRow["CaseNumber"].ToString() + " Below " + DaysToClosed + " Days");
                        }
                    }
                }
                else
                {
                    log.Info("LKCRMServices.AutoCaseClose - LOOP  - No Data");
                }

                log.Info("LKCRMServices.AutoCaseClose - END");
            }
            catch (Exception ex)
            {
                result = "Error";

                log.Error("LKCRMServices.AutoCaseClose - ERROR=" + ex.ToString());
            }

            return "Success";
        }

        [WebMethod]
        public string SyncPersonalsCRM()
        {
            log4net.Config.XmlConfigurator.Configure();
            string result = "Success";

            result = clsServiceBuilder.SyncPersonal();





            return result;
        }


    }
}
