﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using System.ServiceModel.Description;

namespace LKCRMSync.Classes
{
    class clsLKCRMServices
    {
        public static void SyncAlihHakUnit()
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMDB"].ToString()))
                {
                    try
                    {
                        conn.Open();
                        SqlCommand cmd = conn.CreateCommand();
                        cmd.CommandText = "spOwnerTenantSync";
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        public static DataTable GetChangeListFromTMDB()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMDB"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSelectChangeOwnerTenantList";
                    cmd.Parameters.Add(new SqlParameter("CheckDay", ConfigurationManager.AppSettings["CheckDay"].ToString()));
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(dt);
                    oAdapter.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public static IOrganizationService CrmConnection()
        {
            Uri uriorg = new Uri(ConfigurationManager.AppSettings["crmurlservice"].ToString());
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential.Domain = ConfigurationManager.AppSettings["crmdomain"].ToString();
            credentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings["crmuser"].ToString();
            credentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings["crmpassword"].ToString();
            OrganizationServiceProxy serviceproxy = new OrganizationServiceProxy(uriorg, null, credentials, null);

            IOrganizationService service;
            service = (IOrganizationService)serviceproxy;

            return service;
        }

        public static DataTable GetProgressCompletedList()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMDB"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SP_Retrieve_Case";
                    cmd.Parameters.Add(new SqlParameter("CaseStatusID", 6));
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(dt);
                    oAdapter.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public static string Update_Status_CaseClosed(String _NomorCase, String _CRMCaseID)
        {
            IOrganizationService crmservice = CrmConnection();

            try
            {
                //Update to CRM
                QueryByAttribute findcase = new QueryByAttribute("incident");
                findcase.ColumnSet = new ColumnSet(true);
                findcase.Attributes.AddRange("ticketnumber");
                findcase.Values.AddRange(_NomorCase);

                Entity updateCase = crmservice.Retrieve("incident", new Guid(_CRMCaseID), new ColumnSet(true));
                updateCase.Attributes["new_helpdeskstatus"] = new OptionSetValue(9);
                updateCase.Attributes["new_remarks"] = "Case Closed. No complaint from customer";
                updateCase.Attributes["customersatisfactioncode"] = new OptionSetValue(3);
                crmservice.Update(updateCase);

                //Update to TMDDB
                DataTable dt = new DataTable();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TMDB"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "SP_Update_Status_TRCase_CaseClosed";
                    cmd.Parameters.Add(new SqlParameter("CaseNumber", _NomorCase));
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(dt);
                    oAdapter.Dispose();

                    return "Success";
                }
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        public static Entity getrecord_byattribute(IOrganizationService _service, String _entityname, String _searchattr, String _searchvalue, String[] _columnset)
        {
            QueryByAttribute query = new QueryByAttribute(_entityname);
            query.ColumnSet = new ColumnSet(_columnset);
            query.Attributes.AddRange(new String[] { _searchattr });
            query.Values.AddRange(new Object[] { _searchvalue });
            EntityCollection entities = _service.RetrieveMultiple(query);
            Entity entity = null;
            if (entities != null)
                if (entities.Entities.Count > 0)
                    entity = entities.Entities[0];

            Console.WriteLine("Wwww " + entity.Attributes["new_helpdeskstatus"].ToString());
            return entity;
        }

        public static DataTable GetUpdatedPersonal()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Personals"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSyncCrmWithPersonal";
                    cmd.Parameters.Add(new SqlParameter("Days", ConfigurationManager.AppSettings["Days"].ToString()));
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(dt);
                    oAdapter.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public static DataTable GetUpdatedAddress()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["Personals"].ToString()))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "spSyncCrmWithAddress";
                    cmd.Parameters.Add(new SqlParameter("Days", ConfigurationManager.AppSettings["Days"].ToString()));
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter oAdapter = new SqlDataAdapter();
                    oAdapter.SelectCommand = cmd;

                    oAdapter.Fill(dt);
                    oAdapter.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

    }
}
