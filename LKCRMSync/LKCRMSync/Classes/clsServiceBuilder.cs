﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.ServiceModel;
using Microsoft.Xrm.Sdk;
using System.Collections;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.Data.SqlClient;
using System.Xml;
using System.Data.OleDb;
using System.Threading;
using Microsoft.Crm.Sdk.Messages;
using log4net;
using LKCRMSync.Classes;
using Xrm;


namespace LKCRMSync.Classes
{
    public class clsServiceBuilder
    {
        protected static readonly ILog log = LogManager.GetLogger(typeof(clsServiceBuilder));

        public static string SyncPersonal()
        {
            log4net.Config.XmlConfigurator.Configure();

            log.Info("clsServiceBuilder.SyncPersonal - START");

            log.Debug("SyncPersonal.GetUpdatedPersonal - START");

            DataTable dt = clsLKCRMServices.GetUpdatedPersonal();

            log.Debug("SyncPersonal.GetUpdatedPersonal - END   - Result: " + dt.Rows.Count + " Row(s)");

            string result = "";

            if (dt.Rows.Count > 0)
            {
                string stPsCode = "";
                string parent = "";
                string stName = "";
                string stMarsStatus = "";
                string stOcc = "";
                string stBirthPlace = "";
                string stFmTrans = "";
                string stNPWP = "";
                string stRemarks = "";
                string stParentPsCode = "";
                string stFamStat = "";
                string stBlood = "";
                string stRel = "";
                string stGrade = "";
                string stNation = "";
                string stRmodifUN = "";
                string stRinputUN = "";

                int stGender = 0;

                DateTime? stBirtdate = new DateTime();
                DateTime stRmodifTime = new DateTime();
                DateTime stRinputTime = new DateTime();

                string connectionstring = BuildConnectionString();

                bool isActive = false;

                foreach (DataRow dtRow in dt.Rows)
                {
                    try
                    {
                        stPsCode = dtRow["pscode"].ToString().TrimEnd();
                        parent = dtRow["parent"].ToString().TrimEnd();
                        stName = dtRow["name"].ToString().TrimEnd();
                        stParentPsCode = dtRow["parentPsCode"].ToString().TrimEnd();
                        stGrade = dtRow["Grade"].ToString().TrimEnd();

                        if (stPsCode.Equals(parent))
                        {
                            stParentPsCode = "";
                        }

                        if (dtRow["sex"].ToString().ToUpper().Trim() == "L")
                        {
                            stGender = 1;
                        }

                        stBirtdate = null;

                        if (dtRow["birthdate"] != DBNull.Value)
                        {
                            stBirtdate = DateTime.Parse(dtRow["birthdate"].ToString().TrimEnd());
                        }

                        stBirthPlace = dtRow["birthPlace"].ToString().TrimEnd();
                        stMarsStatus = dtRow["marCode"].ToString().TrimEnd();
                        stRel = dtRow["relCode"].ToString().TrimEnd();
                        stBlood = dtRow["bloodCode"].ToString().TrimEnd();
                        stFamStat = dtRow["FamilyStatus"].ToString().TrimEnd();
                        stFmTrans = dtRow["FPTransCode"].ToString().TrimEnd();
                        stOcc = dtRow["occId"].ToString().TrimEnd();
                        stNation = dtRow["nationId"].ToString().TrimEnd();

                        stRmodifTime = DateTime.Parse(dtRow["modifTime"].ToString().TrimEnd());
                        stRmodifUN = dtRow["modifUN"].ToString().TrimEnd();
                        stRinputTime = DateTime.Parse(dtRow["inputTime"].ToString().TrimEnd());
                        stRinputUN = dtRow["inputUN"].ToString().TrimEnd();
                        isActive = bool.Parse(dtRow["isactive"].ToString().Trim());

                        if (dtRow["NPWP"] != DBNull.Value)
                        {
                            stNPWP = dtRow["NPWP"].ToString().TrimEnd();
                        }

                        if (stGrade == "")
                        {
                            stGrade = "0";
                        }

                        insertLog_PersonalsInsert(stPsCode, stName, stParentPsCode, stGender, stBirtdate, stBirthPlace, stMarsStatus, stRel,
                            stBlood, stFamStat, stNPWP, stFmTrans, stGrade, stOcc, stNation, stRemarks, stRmodifTime, stRmodifUN, stRinputTime,
                            stRinputUN, isActive, connectionstring);
                    }
                    catch (Exception ex)
                    {
                        log.Error("LKCRMServices.SyncPersonal - ERROR - PsCode=" + stPsCode + " Result= " + ex.ToString());
                    }
                }

                result = "Success";
            }
            else
            {
                result = "No New Data Entry";
            }

            log.Info("clsServiceBuilder.SyncPersonal - END   - Result= " + result);

            return result;
        }

        private static string insertLog_PersonalsInsert(string _stPsCode, string _stName, string _strParentPsCode,
            int _stGender, DateTime? _stBirtdate, string _stBirthPlace, string _stMarsStatus, string _stRel, string _stBlood,
            string _stFamStat, string _stNPWP, string _stFmTrans, string _stGrade, string _stOcc, string _stNation,
            string _stRemarks, DateTime _stRmodifTime, string _stRmodifUN, DateTime _stRinputTime, string _stRinputUN, bool isactive, string ConnectionString)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();

                Xrm.XrmServiceContext crm;

                Microsoft.Xrm.Client.CrmConnection connection;

                connection = Microsoft.Xrm.Client.CrmConnection.Parse(ConnectionString);

                crm = new Xrm.XrmServiceContext(connection);

                Guid guid = new Guid();

                var CTrans = crm.new_transformSet.Where(a => a.new_TransCode.Equals(_stFmTrans)).FirstOrDefault();

                if (_stFmTrans != "")
                {
                    if (CTrans == null)
                    {
                        Xrm.new_transform insertTrans = new Xrm.new_transform
                        {
                            new_TransCode = _stFmTrans
                        };
                        crm.AddObject(insertTrans);
                        crm.SaveChanges();
                        CTrans = crm.new_transformSet.Where(a => a.new_TransCode.Equals(_stFmTrans)).FirstOrDefault();
                    }
                }
                else
                {
                    CTrans = crm.new_transformSet.Where(a => a.new_TransCode.Equals("00")).FirstOrDefault();
                }

                var Cocc = crm.new_occupationSet.Where(a => a.new_OccId.Equals(_stOcc)).FirstOrDefault();

                if (_stOcc != "")
                {
                    if (Cocc == null)
                    {
                        Xrm.new_occupation insertOcc = new Xrm.new_occupation
                        {
                            new_OccId = _stOcc
                        };
                        crm.AddObject(insertOcc);
                        crm.SaveChanges();
                        Cocc = crm.new_occupationSet.Where(a => a.new_OccId.Equals(_stOcc)).FirstOrDefault();
                    }
                }
                else
                {
                    Cocc = crm.new_occupationSet.Where(a => a.new_OccId.Equals("001")).FirstOrDefault();
                }

                var Cnation = crm.new_nationSet.Where(a => a.new_NationalityID.Equals(_stNation)).FirstOrDefault();

                if (_stNation != "")
                {
                    if (Cnation == null)
                    {
                        Xrm.new_nation insertNation = new Xrm.new_nation
                        {
                            new_NationalityID = _stNation
                        };
                        crm.AddObject(insertNation);
                        crm.SaveChanges();
                        Cnation = crm.new_nationSet.Where(a => a.new_NationalityID.Equals(_stNation)).FirstOrDefault();
                    }
                }
                else
                {
                    Cnation = crm.new_nationSet.Where(a => a.new_NationalityID.Equals("001")).FirstOrDefault();
                }

                int intMarStat = 0;
                
                if (_stMarsStatus == "0")
                { 
                    intMarStat = 3; 
                }
                else if (_stMarsStatus == "M")
                { 
                    intMarStat = 1; 
                }
                else if (_stMarsStatus == "S")
                { 
                    intMarStat = 2; 
                }
                else if (_stMarsStatus == "W")
                { 
                    intMarStat = 4; 
                }
                else
                {
                    intMarStat = 3;
                }

                if (_stBirtdate != null)
                {
                    if (_stBirtdate.Value.Year < 1900)
                    {
                        DateTime d = new DateTime(1900, 1, 1);
                        if (_stBirtdate != null && _stBirtdate <= d)
                        {
                            _stBirtdate = new DateTime(int.Parse("19 " + _stBirtdate.Value.Year.ToString().Substring(2, 2)), _stBirtdate.Value.Month, _stBirtdate.Value.Day);
                        }
                    }
                }

                var cekParentPs = crm.ContactSet.Where(
                    Par => Par.new_PsCode.Equals(_strParentPsCode)).FirstOrDefault();

                if (_strParentPsCode == _stPsCode || _strParentPsCode == "")
                {
                    cekParentPs = null;
                }
                else
                {
                    if (cekParentPs == null)
                    {
                        var ocu = crm.new_occupationSet.Where(a => a.new_OccId.Equals("001")).FirstOrDefault();
                        var nas = crm.new_nationSet.Where(a => a.new_NationalityID.Equals("001")).FirstOrDefault();
                        var conOri = crm.new_transformSet.Where(a => a.new_TransCode.Equals("00")).FirstOrDefault();

                        Xrm.Contact insertParent = new Xrm.Contact
                        {
                            new_PsCode = _strParentPsCode,
                            FirstName = "-",
                            new_NPWP = "-",
                            new_ReligionNew = 0,
                            BirthDate = new DateTime(1900, 1, 2),
                            new_BirthPlace = "-",
                            new_IsConnector = "1",
                            //default
                            new_Sex = true,
                            new_Occupation = ocu != null ? ocu.ToEntityReference() : null,
                            new_FMTransCode = conOri != null ? conOri.ToEntityReference() : null,
                            new_Nationality = nas != null ? nas.ToEntityReference() : null,
                            new_Active = false,

                            new_LKMaritalStatus = 0,
                            new_ParentPSCode = null,
                            new_BloodNew = 0,
                            new_FamilyStatusNew = 0,
                            new_Grade = 0,
                        };

                        crm.ClearChanges();
                        crm.AddObject(insertParent);
                        crm.SaveChanges();
                        cekParentPs = crm.ContactSet.Where(Par => Par.new_PsCode.Equals(_strParentPsCode)).FirstOrDefault();
                    }
                }

                if (_stNPWP == "")
                {
                    _stNPWP = "-";
                }
                if (_stName == "")
                {
                    _stName = "-";
                }
                if (_stBirthPlace == "")
                {
                    _stBirthPlace = "-";
                }
                if (_stGender != 0 && _stGender != 1)
                {
                    _stGender = 0;
                }

                var cekPS = crm.ContactSet.Where(
                    r => r.new_PsCode.Equals(_stPsCode)).FirstOrDefault();

                if (cekPS == null)
                {
                    Xrm.Contact insert = new Xrm.Contact
                    {
                        new_PsCode = _stPsCode,

                        new_LKMaritalStatus = intMarStat,
                        new_ReligionNew = Convert.ToInt32(_stRel),
                        FirstName = _stName,
                        new_ParentPSCode = cekParentPs != null ? cekParentPs.ToEntityReference() : null,
                        new_BloodNew = Convert.ToInt32(_stBlood),
                        BirthDate = _stBirtdate,
                        new_BirthPlace = _stBirthPlace,
                        new_FamilyStatusNew = Convert.ToInt32(_stFamStat),
                        new_FMTransCode = CTrans != null ? CTrans.ToEntityReference() : null,
                        new_Occupation = Cocc != null ? Cocc.ToEntityReference() : null,
                        new_Nationality = Cnation != null ? Cnation.ToEntityReference() : null,
                        new_NPWP = _stNPWP,
                        new_Sex = Convert.ToBoolean(_stGender),
                        new_Grade = Convert.ToInt32(_stGrade),
                        new_IsConnector = "1",
                        new_Active = isactive,
                    };
                    crm.ClearChanges();
                    crm.AddObject(insert);
                    crm.SaveChanges();
                }
                else if (cekPS != null)
                {
                    guid = cekPS.Id;

                    crm.ClearChanges();
                    Xrm.Contact update = new Xrm.Contact
                    {
                        Id = guid,

                        new_PsCode = _stPsCode,
                        new_LKMaritalStatus = intMarStat,
                        new_ReligionNew = Convert.ToInt32(_stRel),
                        FirstName = _stName,
                        new_ParentPSCode = cekParentPs != null ? cekParentPs.ToEntityReference() : null,
                        new_BloodNew = Convert.ToInt32(_stBlood),
                        BirthDate = _stBirtdate,
                        new_BirthPlace = _stBirthPlace,
                        new_FamilyStatusNew = Convert.ToInt32(_stFamStat),
                        new_FMTransCode = CTrans != null ? CTrans.ToEntityReference() : null,
                        new_Occupation = Cocc != null ? Cocc.ToEntityReference() : null,
                        new_Nationality = Cnation != null ? Cnation.ToEntityReference() : null,
                        new_NPWP = _stNPWP,
                        new_Sex = Convert.ToBoolean(_stGender),
                        new_Grade = Convert.ToInt32(_stGrade),
                        new_IsConnector = "1",
                        new_Active = isactive,
                    };
                    crm.Attach(update);
                    crm.UpdateObject(update);
                    crm.SaveChanges();
                }

                int state = 0;

                if (isactive == true)
                {
                    state = 0;
                }
                else
                {
                    state = 1;
                }
                cekPS = crm.ContactSet.Where(
                    r => r.new_PsCode.Equals(_stPsCode)).FirstOrDefault();

                DeactivateEntity(cekPS, state, ConnectionString);

                return "";
            }
            catch (Exception ex)
            {

                return ex.ToString();
            }
        }

        private static void DeactivateEntity(Microsoft.Xrm.Client.CrmEntity ety, int state, string ConnectionString)
        {
            try
            {
                if (ety != null)
                {
                    var connection = Microsoft.Xrm.Client.CrmConnection.Parse(ConnectionString);
                    Xrm.XrmServiceContext service = new Xrm.XrmServiceContext(connection);

                    SetStateRequest setStateRequest = new SetStateRequest();

                    setStateRequest.State = new OptionSetValue(state);

                    setStateRequest.Status = new OptionSetValue(state + 1);
                    setStateRequest.EntityMoniker = new EntityReference(ety.LogicalName, ety.Id);
                    service.Execute(setStateRequest);
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public static string SyncAddress()
        {
            log4net.Config.XmlConfigurator.Configure();

            log.Info("clsServiceBuilder.SyncAddress - START");

            log.Debug("SyncAddress.GetUpdatedAddress - START");

            DataTable dt = clsLKCRMServices.GetUpdatedAddress();

            log.Debug("SyncAddress.GetUpdatedAddress - END   - Result: " + dt.Rows.Count + " Row(s)");

            string result = "";

            if (dt.Rows.Count > 0)
            {
                string stPsCode = "";
                string stAddrType = "";
                string stAddress = "";
                string stPostCode = "";
                string stCity = "";
                string stCountry = "";
                string strRefId = "";
                string stRinputUN = "";
                string stRmodifUN = "";

                DateTime stRmodifTime = new DateTime();
                DateTime stRinputTime = new DateTime();

                string connectionstring = BuildConnectionString();

                foreach (DataRow dtRow in dt.Rows)
                {
                    try
                    {

                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
            {
                result = "No New Data Entry";
            }

            log.Info("clsServiceBuilder.SyncAddress - END   - Result= " + result);

            return result;
        }















        private static string BuildConnectionString()
        {
            string strServer = ConfigurationManager.AppSettings["server"].ToString();
            string strDomain = ConfigurationManager.AppSettings["crmdomain"].ToString();
            string strUsername = ConfigurationManager.AppSettings["username"].ToString();
            string strPass = ConfigurationManager.AppSettings["password"].ToString();

            string strConnectionCRM = "Server= " + strServer + "; Domain= " + strDomain + "; Username= " + strUsername + "; Password= " + strPass + "";

            return strConnectionCRM;
        }




    }
}