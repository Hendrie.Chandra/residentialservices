﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace LKCRMSync.CRMUnit {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="createUnitRequest", Namespace="http://schemas.datacontract.org/2004/07/residential.services.libs")]
    [System.SerializableAttribute()]
    public partial class createUnitRequest : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string areacodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string billingcutoffdateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string billingstartdateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string billingstatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string categorycodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string clustercodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string clusternameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string handoverdateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ownerpscodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string productcodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string siteField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string tenantpscodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string unitcodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string unitnoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string unitstatuscodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string areacode {
            get {
                return this.areacodeField;
            }
            set {
                if ((object.ReferenceEquals(this.areacodeField, value) != true)) {
                    this.areacodeField = value;
                    this.RaisePropertyChanged("areacode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string billingcutoffdate {
            get {
                return this.billingcutoffdateField;
            }
            set {
                if ((object.ReferenceEquals(this.billingcutoffdateField, value) != true)) {
                    this.billingcutoffdateField = value;
                    this.RaisePropertyChanged("billingcutoffdate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string billingstartdate {
            get {
                return this.billingstartdateField;
            }
            set {
                if ((object.ReferenceEquals(this.billingstartdateField, value) != true)) {
                    this.billingstartdateField = value;
                    this.RaisePropertyChanged("billingstartdate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string billingstatus {
            get {
                return this.billingstatusField;
            }
            set {
                if ((object.ReferenceEquals(this.billingstatusField, value) != true)) {
                    this.billingstatusField = value;
                    this.RaisePropertyChanged("billingstatus");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string categorycode {
            get {
                return this.categorycodeField;
            }
            set {
                if ((object.ReferenceEquals(this.categorycodeField, value) != true)) {
                    this.categorycodeField = value;
                    this.RaisePropertyChanged("categorycode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string clustercode {
            get {
                return this.clustercodeField;
            }
            set {
                if ((object.ReferenceEquals(this.clustercodeField, value) != true)) {
                    this.clustercodeField = value;
                    this.RaisePropertyChanged("clustercode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string clustername {
            get {
                return this.clusternameField;
            }
            set {
                if ((object.ReferenceEquals(this.clusternameField, value) != true)) {
                    this.clusternameField = value;
                    this.RaisePropertyChanged("clustername");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string handoverdate {
            get {
                return this.handoverdateField;
            }
            set {
                if ((object.ReferenceEquals(this.handoverdateField, value) != true)) {
                    this.handoverdateField = value;
                    this.RaisePropertyChanged("handoverdate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ownerpscode {
            get {
                return this.ownerpscodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ownerpscodeField, value) != true)) {
                    this.ownerpscodeField = value;
                    this.RaisePropertyChanged("ownerpscode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string productcode {
            get {
                return this.productcodeField;
            }
            set {
                if ((object.ReferenceEquals(this.productcodeField, value) != true)) {
                    this.productcodeField = value;
                    this.RaisePropertyChanged("productcode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string site {
            get {
                return this.siteField;
            }
            set {
                if ((object.ReferenceEquals(this.siteField, value) != true)) {
                    this.siteField = value;
                    this.RaisePropertyChanged("site");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string tenantpscode {
            get {
                return this.tenantpscodeField;
            }
            set {
                if ((object.ReferenceEquals(this.tenantpscodeField, value) != true)) {
                    this.tenantpscodeField = value;
                    this.RaisePropertyChanged("tenantpscode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string unitcode {
            get {
                return this.unitcodeField;
            }
            set {
                if ((object.ReferenceEquals(this.unitcodeField, value) != true)) {
                    this.unitcodeField = value;
                    this.RaisePropertyChanged("unitcode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string unitno {
            get {
                return this.unitnoField;
            }
            set {
                if ((object.ReferenceEquals(this.unitnoField, value) != true)) {
                    this.unitnoField = value;
                    this.RaisePropertyChanged("unitno");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string unitstatuscode {
            get {
                return this.unitstatuscodeField;
            }
            set {
                if ((object.ReferenceEquals(this.unitstatuscodeField, value) != true)) {
                    this.unitstatuscodeField = value;
                    this.RaisePropertyChanged("unitstatuscode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="responseBase", Namespace="http://schemas.datacontract.org/2004/07/residential.services.libs.Models")]
    [System.SerializableAttribute()]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(LKCRMSync.CRMUnit.updateUnitResponse))]
    [System.Runtime.Serialization.KnownTypeAttribute(typeof(LKCRMSync.CRMUnit.createUnitResponse))]
    public partial class responseBase : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string operationReasonField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private LKCRMSync.CRMUnit.resultStatus operationResultField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string operationReason {
            get {
                return this.operationReasonField;
            }
            set {
                if ((object.ReferenceEquals(this.operationReasonField, value) != true)) {
                    this.operationReasonField = value;
                    this.RaisePropertyChanged("operationReason");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public LKCRMSync.CRMUnit.resultStatus operationResult {
            get {
                return this.operationResultField;
            }
            set {
                if ((this.operationResultField.Equals(value) != true)) {
                    this.operationResultField = value;
                    this.RaisePropertyChanged("operationResult");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="updateUnitResponse", Namespace="http://schemas.datacontract.org/2004/07/residential.services.libs")]
    [System.SerializableAttribute()]
    public partial class updateUnitResponse : LKCRMSync.CRMUnit.responseBase {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string unitGuidField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string unitGuid {
            get {
                return this.unitGuidField;
            }
            set {
                if ((object.ReferenceEquals(this.unitGuidField, value) != true)) {
                    this.unitGuidField = value;
                    this.RaisePropertyChanged("unitGuid");
                }
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="createUnitResponse", Namespace="http://schemas.datacontract.org/2004/07/residential.services.libs")]
    [System.SerializableAttribute()]
    public partial class createUnitResponse : LKCRMSync.CRMUnit.responseBase {
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string unitGuidField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string unitGuid {
            get {
                return this.unitGuidField;
            }
            set {
                if ((object.ReferenceEquals(this.unitGuidField, value) != true)) {
                    this.unitGuidField = value;
                    this.RaisePropertyChanged("unitGuid");
                }
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="resultStatus", Namespace="http://schemas.datacontract.org/2004/07/residential.services.libs.Models")]
    public enum resultStatus : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Success = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Failure = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Error = 2,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="updateUnitRequest", Namespace="http://schemas.datacontract.org/2004/07/residential.services.libs")]
    [System.SerializableAttribute()]
    public partial class updateUnitRequest : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string areacodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string billingcutoffdateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string billingstartdateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string billingstatusField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string categorycodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string clustercodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string clusternameField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string handoverdateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ownerpscodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string productcodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string siteField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string tenantpscodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string unitcodeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string unitnoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string unitstatuscodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string areacode {
            get {
                return this.areacodeField;
            }
            set {
                if ((object.ReferenceEquals(this.areacodeField, value) != true)) {
                    this.areacodeField = value;
                    this.RaisePropertyChanged("areacode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string billingcutoffdate {
            get {
                return this.billingcutoffdateField;
            }
            set {
                if ((object.ReferenceEquals(this.billingcutoffdateField, value) != true)) {
                    this.billingcutoffdateField = value;
                    this.RaisePropertyChanged("billingcutoffdate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string billingstartdate {
            get {
                return this.billingstartdateField;
            }
            set {
                if ((object.ReferenceEquals(this.billingstartdateField, value) != true)) {
                    this.billingstartdateField = value;
                    this.RaisePropertyChanged("billingstartdate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string billingstatus {
            get {
                return this.billingstatusField;
            }
            set {
                if ((object.ReferenceEquals(this.billingstatusField, value) != true)) {
                    this.billingstatusField = value;
                    this.RaisePropertyChanged("billingstatus");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string categorycode {
            get {
                return this.categorycodeField;
            }
            set {
                if ((object.ReferenceEquals(this.categorycodeField, value) != true)) {
                    this.categorycodeField = value;
                    this.RaisePropertyChanged("categorycode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string clustercode {
            get {
                return this.clustercodeField;
            }
            set {
                if ((object.ReferenceEquals(this.clustercodeField, value) != true)) {
                    this.clustercodeField = value;
                    this.RaisePropertyChanged("clustercode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string clustername {
            get {
                return this.clusternameField;
            }
            set {
                if ((object.ReferenceEquals(this.clusternameField, value) != true)) {
                    this.clusternameField = value;
                    this.RaisePropertyChanged("clustername");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string handoverdate {
            get {
                return this.handoverdateField;
            }
            set {
                if ((object.ReferenceEquals(this.handoverdateField, value) != true)) {
                    this.handoverdateField = value;
                    this.RaisePropertyChanged("handoverdate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ownerpscode {
            get {
                return this.ownerpscodeField;
            }
            set {
                if ((object.ReferenceEquals(this.ownerpscodeField, value) != true)) {
                    this.ownerpscodeField = value;
                    this.RaisePropertyChanged("ownerpscode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string productcode {
            get {
                return this.productcodeField;
            }
            set {
                if ((object.ReferenceEquals(this.productcodeField, value) != true)) {
                    this.productcodeField = value;
                    this.RaisePropertyChanged("productcode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string site {
            get {
                return this.siteField;
            }
            set {
                if ((object.ReferenceEquals(this.siteField, value) != true)) {
                    this.siteField = value;
                    this.RaisePropertyChanged("site");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string tenantpscode {
            get {
                return this.tenantpscodeField;
            }
            set {
                if ((object.ReferenceEquals(this.tenantpscodeField, value) != true)) {
                    this.tenantpscodeField = value;
                    this.RaisePropertyChanged("tenantpscode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string unitcode {
            get {
                return this.unitcodeField;
            }
            set {
                if ((object.ReferenceEquals(this.unitcodeField, value) != true)) {
                    this.unitcodeField = value;
                    this.RaisePropertyChanged("unitcode");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string unitno {
            get {
                return this.unitnoField;
            }
            set {
                if ((object.ReferenceEquals(this.unitnoField, value) != true)) {
                    this.unitnoField = value;
                    this.RaisePropertyChanged("unitno");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string unitstatuscode {
            get {
                return this.unitstatuscodeField;
            }
            set {
                if ((object.ReferenceEquals(this.unitstatuscodeField, value) != true)) {
                    this.unitstatuscodeField = value;
                    this.RaisePropertyChanged("unitstatuscode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CRMUnit.IUnitService")]
    public interface IUnitService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUnitService/createUnit", ReplyAction="http://tempuri.org/IUnitService/createUnitResponse")]
        LKCRMSync.CRMUnit.createUnitResponse createUnit(LKCRMSync.CRMUnit.createUnitRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IUnitService/updateUnit", ReplyAction="http://tempuri.org/IUnitService/updateUnitResponse")]
        LKCRMSync.CRMUnit.updateUnitResponse updateUnit(LKCRMSync.CRMUnit.updateUnitRequest request);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IUnitServiceChannel : LKCRMSync.CRMUnit.IUnitService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class UnitServiceClient : System.ServiceModel.ClientBase<LKCRMSync.CRMUnit.IUnitService>, LKCRMSync.CRMUnit.IUnitService {
        
        public UnitServiceClient() {
        }
        
        public UnitServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public UnitServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public UnitServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public UnitServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public LKCRMSync.CRMUnit.createUnitResponse createUnit(LKCRMSync.CRMUnit.createUnitRequest request) {
            return base.Channel.createUnit(request);
        }
        
        public LKCRMSync.CRMUnit.updateUnitResponse updateUnit(LKCRMSync.CRMUnit.updateUnitRequest request) {
            return base.Channel.updateUnit(request);
        }
    }
}
