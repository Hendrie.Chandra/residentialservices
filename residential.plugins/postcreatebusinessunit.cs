﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;

namespace residential.plugins
{
    public class postcreatebusinessunit : IPlugin
    {
        public Int32 save_site(String _crmsiteid, String _sitecode, String _sitename, int _orgid, String _savedby, String connectionstring)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Site", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CRMSiteID", _crmsiteid);
            cmd.Parameters.AddWithValue("@SiteCode", _sitecode);
            cmd.Parameters.AddWithValue("@SiteName", _sitename);
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }       

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.InitiatingUserId);

                QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                q_configuration.Attributes.AddRange(new string[] { "new_name" });
                q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);
                String connectionstring = "";

                if (configurations != null)
                {
                    if (configurations.Entities.Count > 0)
                    {
                        Entity configuration = configurations.Entities[0];
                        connectionstring = configuration.Attributes["new_value"].ToString();
                    }
                }

                Entity businessunit = (Entity)context.InputParameters["Target"];

                try
                {                   
                    String SiteName = "";
                    if (businessunit.Attributes.Contains("name"))
                        SiteName = businessunit.Attributes["name"].ToString();

                    String SiteCode = "";                    
                    SiteName.Split(' ').ToList().ForEach(i => SiteCode += i[0]);

                    String CRMSiteID = businessunit.Id.ToString();                    

                    Entity user = adminservice.Retrieve("systemuser", context.UserId, new ColumnSet(new String[] { "domainname" }));
                    String SavedBy = user.Attributes["domainname"].ToString();

                    save_site(CRMSiteID, SiteCode,SiteName, 1, SavedBy, connectionstring);
                }
                catch (FaultException<OrganizationServiceFault> ex)
                {
                    throw new InvalidPluginExecutionException("An error occurred in the plug-in.", ex);
                }
            }

        }
    }
}
