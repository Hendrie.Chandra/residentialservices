﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;

namespace residential.plugins
{
    public class postcreateemailactivity : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));            

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService _service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                IOrganizationService __service = servicefactory.CreateOrganizationService(null);

                //EntityReference emailreference = (EntityReference)context.InputParameters["EntityMoniker"];
                //Entity entityemail = service.Retrieve(emailreference.LogicalName, emailreference.Id, new ColumnSet(new String[] { "statecode" }));
                //Entity helpname = (Entity)context.InputParameters["EntityMoniker"];
                //OptionSetValue sc = (OptionSetValue)entityemail.Attributes["statecode"];      
                Entity emailactivity = (Entity)context.InputParameters["Target"];

                try
                {
                    EntityReference moniker = new EntityReference();
                    moniker.LogicalName = "email";
                    moniker.Id = emailactivity.Id;

                    OrganizationRequest request = new OrganizationRequest() { RequestName = "SetState" };
                    request["EntityMoniker"] = moniker;
                    request["State"] = new OptionSetValue(0);
                    request["Status"] = new OptionSetValue(-1);
                    _service.Execute(request);

                    //SetStateRequest setState = new SetStateRequest();
                    //setState.EntityMoniker = new EntityReference();
                    //setState.EntityMoniker.Id = emailactivity.Id;
                    ////setState.EntityMoniker.Name = "email";
                    //setState.EntityMoniker.LogicalName = "email";
                    //setState.State = new OptionSetValue(0);
                    //setState.Status = new OptionSetValue(1);
                    //SetStateResponse setStateResponse = (SetStateResponse)service.Execute(setState);
                    if (emailactivity.Attributes["subject"] != null)
                    {
                        String emailsender = emailactivity.Attributes["sender"].ToString();
                        String emailrecipient = emailactivity.Attributes["torecipients"].ToString();

                        QueryByAttribute querybyattribute = new QueryByAttribute("new_email");
                        querybyattribute.ColumnSet = new ColumnSet("new_contact", "new_name"); // new_contact = Personal Name, new_name = Email Address
                        //  Attribute to query.
                        querybyattribute.Attributes.AddRange("new_name");
                        //  Value of queried attribute to return.
                        querybyattribute.Values.AddRange(emailsender);
                        //  Query passed to service proxy.
                        EntityCollection retrieved = service.RetrieveMultiple(querybyattribute);
                        if (retrieved != null && retrieved.Entities.Count > 0)
                        {
                            //if (sc.Value.ToString() == "1")
                            //{
                            //    emailactivity.Attributes["statecode"] = new OptionSetValue(0);
                            //    service.Update(emailactivity);
                            //}                        

                            EntityReference personal = (EntityReference)retrieved.Entities[0].Attributes["new_contact"];

                            Entity updateemail = new Entity("email");
                            updateemail.Id = emailactivity.Id;

                            //Entity fromparty = new Entity("activityparty");
                            //fromparty.Attributes["partyid"] = new EntityReference("contact", personal.Id);
                            //fromparty.Attributes["addressused"] = emailsender;
                            //updateemail.Attributes["from"] = new Entity[] { fromparty };

                            //EntityCollection ac = (EntityCollection)updateemail.Attributes["from"];
                            //EntityCollection ac = (EntityCollection)emailactivity.Attributes["from"];
                            //ac.Entities[0].Attributes.Add("partyid", new EntityReference("contact", personal.Id));

                            //Entity toparty = new Entity("activityparty");
                            //toparty.Attributes["partyid"] = new EntityReference("contact", personal.Id);
                            //updateemail.Attributes["to"] = new Entity[] { toparty };

                            //_service.Update(emailactivity);
                            //_service.Update(updateemail);
                            //_service.Create(updateemail);

                            request["EntityMoniker"] = moniker;
                            request["State"] = new OptionSetValue(1);
                            request["Status"] = new OptionSetValue(4);
                            _service.Execute(request);


                            //updateemail.Attributes["from"] = new EntityReference("contact", personal.Id);
                            //updateemail.Attributes["new_fromemail"] = emailsender;
                            //updateemail.Attributes["to"] = new EntityReference("contact", personal.Id);


                            //emailbaru.Attributes["from"] = new EntityReference("contact", personal.Id);

                            

                            //ac.Add("partyid", new EntityReference("contact", personal.Id));
                            //emailactivity.Attributes["from"] = ac;
                            //__service.Update(emailactivity);
                            //_service.Update(emailactivity);
                            //service.Create(emailbaru);                        
                        }
                        else
                        {
                            Entity loc = new Entity("new_location");
                            loc.Attributes["new_name"] = "Gagal";
                            service.Create(loc);
                        }
                    }
                }
                catch (FaultException<OrganizationServiceFault> ex)
                {
                    throw new InvalidPluginExecutionException("An error occurred in the plug-in.", ex);
                }
            }
        }
    }
}
