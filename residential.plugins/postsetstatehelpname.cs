﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;

namespace residential.plugins
{
    public class postsetstatehelpname : IPlugin
    {
        private Int32 find_helpid(Guid _helpguid, String _connstr)
        {
            SqlConnection conn = new SqlConnection(_connstr);
            SqlCommand cmd = new SqlCommand("SELECT HelpID FROM MSHelp WHERE CRMHelpID = '" + _helpguid + "'", conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();

            Int32 returnhelpid = 0;
            if (dt.Rows.Count > 0) returnhelpid = Int32.Parse(dt.Rows[0]["HelpID"].ToString());
            return returnhelpid;
        }
            
        public void Execute(IServiceProvider _serviceprovider)
        {            
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));
            
            if (context.InputParameters.Contains("EntityMoniker") && context.InputParameters["EntityMoniker"] is EntityReference)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.InitiatingUserId);

                QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                q_configuration.Attributes.AddRange(new string[] { "new_name" });
                q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);
                String connectionstring = "";

                if (configurations != null)
                {
                    if (configurations.Entities.Count > 0)
                    {
                        Entity configuration = configurations.Entities[0];
                        connectionstring = configuration.Attributes["new_value"].ToString();
                    }
                }

                EntityReference helpreference = (EntityReference)context.InputParameters["EntityMoniker"];
                Entity helpname = service.Retrieve(helpreference.LogicalName, helpreference.Id, new ColumnSet(new String[] { "statecode" }));
                //Entity helpname = (Entity)context.InputParameters["EntityMoniker"];
                OptionSetValue sc = (OptionSetValue)helpname.Attributes["statecode"];                
                bool statecode = true;
                if (sc.Value.ToString() == "1")
                    statecode = false;

                Entity user = adminservice.Retrieve("systemuser", context.UserId, new ColumnSet(new String[] { "domainname" }));
                String SavedBy = user.Attributes["domainname"].ToString();

                try
                {
                    SqlConnection conn = new SqlConnection(connectionstring);                    
                    SqlCommand cmd = new SqlCommand("SP_Save_Help", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@HelpID", find_helpid(helpname.Id, connectionstring));
                    cmd.Parameters.AddWithValue("@IsActive", statecode);
                    cmd.Parameters.AddWithValue("@SavedBy", SavedBy);
                    
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
                catch (FaultException<OrganizationServiceFault> ex)
                {
                    throw new InvalidPluginExecutionException("An error occurred in the plug-in.", ex);
                }
            }
        }
    }
}
