﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace residential.plugins
{
    public class postcreatehelpname : IPlugin
    {
        private Int32 save_message(String Message, String _connstr)
        {
            SqlConnection conn = new SqlConnection(_connstr);
            SqlCommand cmd = new SqlCommand("SaveMessage", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Message", Message);
            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        private Int32 save_help(Int32 _orgid, Int32 _siteid, String _crmhelpid, String _helpname, String _crmparenthelpid, String _crmhelpcategoryid,
            String _crmdepartmentid, Boolean _ipklincludable, Int16 _duration, Int32 _teamid, String _viewformname, Boolean _isactive, String _savedby, String _connstr)
        {
            SqlConnection conn = new SqlConnection(_connstr);
            SqlCommand cmd = new SqlCommand("SP_Save_Help", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CRMHelpID", _crmhelpid);
            cmd.Parameters.AddWithValue("@HelpName", _helpname);
            cmd.Parameters.AddWithValue("@CRMParentHelpID", _crmparenthelpid);
            cmd.Parameters.AddWithValue("@CRMHelpCategoryID", _crmhelpcategoryid);
            cmd.Parameters.AddWithValue("@CRMDepartmentID", _crmdepartmentid);
            cmd.Parameters.AddWithValue("@IPKLIncludable", _ipklincludable);
            cmd.Parameters.AddWithValue("@Duration", _duration);
            cmd.Parameters.AddWithValue("@TeamID", _teamid);
            cmd.Parameters.AddWithValue("@ViewFormName", _viewformname);
            cmd.Parameters.AddWithValue("@IsActive", _isactive);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        private DataTable retrieve_site(String _crmsiteid, String _connstr)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(_connstr);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Site", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CRMSiteID", _crmsiteid);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.InitiatingUserId);                

                QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                q_configuration.Attributes.AddRange(new string[] { "new_name" });
                q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);
                String connectionstring = "";

                if (configurations != null)
                {
                    if (configurations.Entities.Count > 0)
                    {
                        Entity configuration = configurations.Entities[0];
                        connectionstring = configuration.Attributes["new_value"].ToString();
                    }
                }

                Entity helpname = (Entity)context.InputParameters["Target"];

                //String Message = "(INSERT) Depth: " + context.Depth.ToString() + "; ID: " + helpname.Id.ToString();
                //if (helpname.Attributes.Contains("new_parent"))
                //    Message = Message + "; Parent: " + ((EntityReference)helpname.Attributes["new_parent"]).Id.ToString();
                //else
                //    Message = Message + "; Parent: None";

                //if (helpname.Attributes.Contains("new_helpcategory"))
                //    Message = Message + "; Help Category: " + ((EntityReference)helpname.Attributes["new_helpcategory"]).Id.ToString();
                //else
                //    Message = Message + "; Help Category: None";

                //if (helpname.Attributes.Contains("new_department"))
                //    Message = Message + "; Department: " + ((EntityReference)helpname.Attributes["new_department"]).Id.ToString();
                //else
                //    Message = Message + "; Department: None";
                //save_message(Message, connectionstring);

                if (helpname.Attributes.Contains("ownerid"))
                {
                    String crmsiteid = "";
                    EntityReference ownerref = (EntityReference)helpname.Attributes["ownerid"];
                    if (ownerref.LogicalName == "team")
                    {
                        Entity team = service.Retrieve("team", ownerref.Id, new ColumnSet(new String[] { "businessunitid" }));
                        if (team != null)
                        {
                            if (team.Attributes.Contains("businessunitid"))
                            { 
                                EntityReference businessunitref = (EntityReference)team.Attributes["businessunitid"];
                                crmsiteid = businessunitref.Id.ToString();
                            }                        
                        }                    
                    }

                    DataTable dt_site = retrieve_site(crmsiteid, connectionstring);
                    DataRow dr_site = dt_site.Rows[0];
                    Int32 siteid = (Int32)dr_site["SiteID"];

                    String Name = "";
                    if (helpname.Attributes.Contains("new_name"))
                        Name = helpname.Attributes["new_name"].ToString();

                    String crmparenthelpid = "";
                    if (helpname.Attributes.Contains("new_parent"))
                        crmparenthelpid = ((EntityReference)helpname.Attributes["new_parent"]).Id.ToString();

                    String crmhelpcategoryid = "";
                    if (helpname.Attributes.Contains("new_helpcategory"))
                        crmhelpcategoryid = ((EntityReference)helpname.Attributes["new_helpcategory"]).Id.ToString();

                    String crmdepartmentid = "";
                    if (helpname.Attributes.Contains("new_department"))
                        crmdepartmentid = ((EntityReference)helpname.Attributes["new_department"]).Id.ToString();

                    Boolean IPKLIncludable = false;
                    if (helpname.Contains("new_ipklincludable") && helpname.GetAttributeValue<bool>("new_ipklincludable"))
                        IPKLIncludable = true;

                    Int16 Duration = 0;
                    if (helpname.Attributes.Contains("new_duration"))
                        Duration = Int16.Parse(helpname.Attributes["new_duration"].ToString());

                    String viewformname = "";
                    if (helpname.Attributes.Contains("new_rsviewformname"))
                        viewformname = helpname.Attributes["new_rsviewformname"].ToString();

                    Entity user = adminservice.Retrieve("systemuser", context.UserId, new ColumnSet(new String[] { "domainname" }));
                    String SavedBy = user.Attributes["domainname"].ToString();

                    save_help(1, siteid, helpname.Id.ToString(), Name, crmparenthelpid, crmhelpcategoryid, crmdepartmentid, IPKLIncludable, Duration, 1, viewformname, true, SavedBy, connectionstring);
                }

                //try
                //{
                //    String Name = "";
                //    if (helpname.Attributes.Contains("new_name"))
                //        Name = helpname.Attributes["new_name"].ToString();

                //    Int32 ParentHelpID = 0;
                //    if (helpname.Attributes.Contains("new_parent"))
                //        ParentHelpID = find_helpid(((EntityReference)helpname["new_parent"]).Id, connectionstring);

                //    Int32 DepartmentID = 0;
                //    if (helpname.Attributes.Contains("new_department"))
                //        DepartmentID = find_department(((EntityReference)helpname["new_department"]).Id, connectionstring);

                //    Boolean IPKLIncludable = false;
                //    if (helpname.Contains("new_ipklincludable") && helpname.GetAttributeValue<bool>("new_ipklincludable"))                    
                //        IPKLIncludable = true;

                //    Int16 Duration = 0;
                //    if (helpname.Attributes.Contains("new_duration"))
                //        Duration = Int16.Parse(helpname.Attributes["new_duration"].ToString());

                //    Entity user = adminservice.Retrieve("systemuser", context.UserId, new ColumnSet(new String[] { "domainname" }));
                //    String SavedBy = user.Attributes["domainname"].ToString();
                    
                //    save_help(0, 1, 1, helpname.Id.ToString(), Name, ParentHelpID, DepartmentID, IPKLIncludable, Duration, 1, true, SavedBy, connectionstring);
                //}
                //catch (FaultException<OrganizationServiceFault> ex)
                //{
                //    throw new InvalidPluginExecutionException("An error occurred in the plug-in.", ex);
                //}                              
            }

        }
    }
}
