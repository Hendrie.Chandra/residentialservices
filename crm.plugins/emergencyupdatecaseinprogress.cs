﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace crm.plugins
{
    //penambahan 8 mei 2017
    //setiap case emergency inprogress, update case inprogress kirim email ke group email emergency
    public class emergencyupdatecaseinprogress : IPlugin
    {
        private static String connectionstring = "";
        private static String SavedBy = "";

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));
            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {

                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService systemservice = servicefactory.CreateOrganizationService(null);

                Entity newcase = (Entity)context.InputParameters["Target"];

                Entity PreUpdateCaseImage = (Entity)context.PreEntityImages["PreUpdateCaseImage"];

                Entity EntHelpCategory = service.Retrieve("new_helpcategory", ((EntityReference)PreUpdateCaseImage.Attributes["new_helpcategory"]).Id, new ColumnSet(new String[] { "new_name" }));
                
                String HelpCategory = EntHelpCategory.Attributes["new_name"].ToString();
                int HelpdeskStatus = ((OptionSetValue)PreUpdateCaseImage.Attributes["new_helpdeskstatus"]).Value;

                if (HelpCategory.ToUpper() == "INCIDENT" && HelpdeskStatus == 4)//inprogress
                {
                    // Retrieve initiating user
                    Entity user = service.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname", "fullname" }));
                    SavedBy = user.Attributes["domainname"].ToString();

                    QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                    q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                    q_configuration.Attributes.AddRange(new string[] { "new_name" });
                    q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                    EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);

                    if (configurations != null)
                    {
                        if (configurations.Entities.Count > 0)
                        {
                            Entity configuration = configurations.Entities[0];
                            connectionstring = configuration.Attributes["new_value"].ToString();
                        }
                    }

                    //execute plugin jika update remarks
                    if (newcase.Attributes.Contains("new_remarks"))
                    {
                        try
                        {
                            string lastRemarks = "";
                            lastRemarks = newcase.Attributes["new_remarks"].ToString();
                            SendEmailEmergency(adminservice, PreUpdateCaseImage, HelpdeskStatus, connectionstring, lastRemarks);

                        }
                        catch (Exception ex)
                        {
                            throw new InvalidPluginExecutionException(ex.Message);
                        }

                    }

                }
            }
        }


        static private void SendEmailEmergency(IOrganizationService _service, Entity _incident, Int32 _helpdeskstatus, String _connectionstring, string _lastRemarks)
        {
            Guid crmcaseid = _incident.Id;

            QueryByAttribute q_tickethistory = new QueryByAttribute("new_tickethistory");
            q_tickethistory.ColumnSet = new ColumnSet(new string[] { "new_caseid", "new_remarks", "new_helpdeskstatus", "new_updateddate" });
            q_tickethistory.Attributes.AddRange(new string[] { "new_caseid" });
            q_tickethistory.Values.Add(crmcaseid);
            EntityCollection tickethistories = _service.RetrieveMultiple(q_tickethistory);

            string updateremarkshistory = "";
            if (tickethistories.Entities.Count > 0)
            {
                foreach (Entity tickethistory in tickethistories.Entities)
                {
                    string remarkshistory = "";
                    DateTime? remarkshistorydate = null;
                    if (tickethistory.Contains("new_remarks"))
                        remarkshistory = tickethistory.Attributes["new_remarks"].ToString();
                    if (tickethistory.Contains("new_updateddate"))
                        remarkshistorydate = ((DateTime)tickethistory.Attributes["new_updateddate"]).ToLocalTime();

                    updateremarkshistory = updateremarkshistory + " [" + remarkshistorydate.ToString() + "] " + remarkshistory + " ";
                }
            }
            updateremarkshistory = updateremarkshistory + " [" + DateTime.Now.ToLocalTime() +"] " + _lastRemarks;
            //String EmailTemplateName = "Case No Pending Emergency - PIC";

            Entity mycase = _service.Retrieve("incident", _incident.Id, new ColumnSet(new string[] { "new_unit" }));
            Entity unit = _service.Retrieve("new_unit", ((EntityReference)mycase.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
            Entity businessunit = _service.Retrieve("businessunit", ((EntityReference)unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue", "name","new_emailtemplatenopendingpicemergency" }));

            String EmailTemplateName = "";
            EmailTemplateName = businessunit.Attributes["new_emailtemplatenopendingpicemergency"].ToString();

            QueryByAttribute templatequery = new QueryByAttribute("template");
            templatequery.ColumnSet = new ColumnSet(true);
            templatequery.Attributes.AddRange("title");
            templatequery.Values.AddRange(EmailTemplateName);
            EntityCollection emailtemplate = _service.RetrieveMultiple(templatequery);

            if (emailtemplate.Entities.Count > 0)
            {
                EntityCollection From = new EntityCollection();
                Entity QueueEmail = new Entity("activityparty");
                QueueEmail.Attributes.Add("partyid", new EntityReference("queue", ((EntityReference)businessunit.Attributes["new_emailqueue"]).Id));
                From.Entities.Add(QueueEmail);

                EntityCollection To = new EntityCollection();

                String Representative = "";

                //Retrieve Recipient
                List<MailAddress> recipients = new List<MailAddress>();

                //find pic head based on case
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Retrieve_Notification_User_Emergency", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", 1);       //OrgID hardcoded=1
                cmd.Parameters.AddWithValue("@SiteID", 5);
                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();
                adapter.Fill(dt);
                conn.Close();

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr_user in dt.Rows)
                    {

                        Entity activityPICHead = new Entity("activityparty");
                        activityPICHead.Attributes.Add("addressused", dr_user["EmailAddress"].ToString());
                        To.Entities.Add(activityPICHead);

                        Representative += dr_user["Fullname"].ToString() + ", ";

                        //MailAddress recipient = new MailAddress();
                        //recipient.DisplayName = dr_user["FullName"].ToString();
                        //recipient.EmailAddress = dr_user["EmailAddress"].ToString();

                        //recipients.Add(recipient);
                    }
                }

                if (To.Entities.Count > 0)
                {
                    InstantiateTemplateRequest instTemplateReq = new InstantiateTemplateRequest();
                    instTemplateReq.TemplateId = emailtemplate.Entities[0].Id;
                    instTemplateReq.ObjectId = mycase.Id;
                    instTemplateReq.ObjectType = mycase.LogicalName;

                    InstantiateTemplateResponse instTemplateResp = (InstantiateTemplateResponse)_service.Execute(instTemplateReq);

                    Entity email = new Entity("email");
                    email = instTemplateResp.EntityCollection[0];

                    //Email Signature
                    String SignatureLogo = "";
                    String SignatureAddress = "";

                    QueryByAttribute SignatureTemplate = new QueryByAttribute("new_residentialconfiguration");
                    SignatureTemplate.ColumnSet = new ColumnSet(new string[] { "new_value", "new_value2" });
                    SignatureTemplate.Attributes.AddRange(new string[] { "new_name" });
                    SignatureTemplate.Values.AddRange(businessunit.Attributes["name"].ToString() + " Signature");
                    EntityCollection SignatureTemplates = _service.RetrieveMultiple(SignatureTemplate);

                    if (SignatureTemplates != null && SignatureTemplates.Entities.Count > 0)
                    {
                        SignatureLogo = "<b>" + SignatureTemplates.Entities[0].Attributes["new_value"].ToString() + "</b>";
                        SignatureAddress = SignatureTemplates.Entities[0].Attributes["new_value2"].ToString();
                    }

                    String Subject = email.Attributes["subject"].ToString();
                    String MessageContent = email.Attributes["description"].ToString();

                    MessageContent = MessageContent.Replace("{Representative}", Representative);
                    MessageContent = MessageContent.Replace("{Signature_Logo}", SignatureLogo);
                    MessageContent = MessageContent.Replace("{Signature_Address}", SignatureAddress);
                    MessageContent = MessageContent.Replace("{Progress_Case}", updateremarkshistory);
                    MessageContent = MessageContent.Replace("\n", "<br />");

                    email.Attributes["regardingobjectid"] = new EntityReference(mycase.LogicalName, mycase.Id);
                    email.Attributes["from"] = From;
                    email.Attributes["to"] = To;
                    email.Attributes["subject"] = Subject;
                    email.Attributes["description"] = MessageContent;
                    Guid EmailID = _service.Create(email);

                    SendEmailRequest reqSendEmail = new SendEmailRequest();
                    reqSendEmail.EmailId = EmailID;
                    reqSendEmail.TrackingToken = "";
                    reqSendEmail.IssueSend = true;
                    SendEmailResponse res = (SendEmailResponse)_service.Execute(reqSendEmail);

                }
            }
        }


        //SendEmailToPICHead(adminservice, newcase, helpdeskstatus, connectionstring);



        private void SendEmailEmergency2(IOrganizationService _service, Entity _incident, Int32 _helpdeskstatus, String _connectionstring)
        {
            try
            {
                Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                Entity businessunit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue", "new_emailtemplatecancelledpic", "new_emailtemplateonhold", "new_emailtemplatenotsatisfied", "new_siteid" }));
                int siteid = (int)businessunit.Attributes["new_siteid"];

                //find pic head based on case
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Retrieve_Notification_User_Emergency", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", 1);       //OrgID hardcoded=1
                cmd.Parameters.AddWithValue("@SiteID", siteid);
                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();
                adapter.Fill(dt);
                conn.Close();

                if (dt.Rows.Count > 0)
                {
                    EntityCollection To = new EntityCollection();

                    foreach (DataRow dr in dt.Rows)
                    {
                        String PICHeadEmailAddress = dr["EmailAddress"].ToString();

                        if (!String.IsNullOrWhiteSpace(PICHeadEmailAddress) &&
                        Regex.IsMatch(PICHeadEmailAddress, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                        {
                            Entity activityPICEmailAddress = new Entity("activityparty");
                            activityPICEmailAddress.Attributes.Add("addressused", PICHeadEmailAddress);
                            To.Entities.Add(activityPICEmailAddress);
                        }
                    }

                    if (To.Entities.Count > 0)
                    {
                        String findTemplate = "";
                        findTemplate = businessunit.Attributes["new_emailtemplatenopendingpicemergency"].ToString();   

                        SendEmail(_service, _incident, findTemplate, To);
                    }
                }
            }
            catch (Exception ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void SendEmail(IOrganizationService _service, Entity _incident, String _EmailTemplateName, EntityCollection _ToAddress)
        {
            try
            {
                Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                Entity BusinessUnit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(true));

                //Find Template
                QueryByAttribute templatequery = new QueryByAttribute("template");
                templatequery.ColumnSet = new ColumnSet(true);
                templatequery.Attributes.AddRange("title");
                templatequery.Values.AddRange(_EmailTemplateName);
                EntityCollection retrieved = _service.RetrieveMultiple(templatequery);

                if (retrieved != null && retrieved.Entities.Count > 0)
                {
                    //retrieving email template
                    InstantiateTemplateRequest instTemplateReq = new InstantiateTemplateRequest();
                    instTemplateReq.TemplateId = retrieved.Entities[0].Id;
                    instTemplateReq.ObjectId = _incident.Id;
                    instTemplateReq.ObjectType = _incident.LogicalName;

                    InstantiateTemplateResponse instTemplateResp = (InstantiateTemplateResponse)_service.Execute(instTemplateReq);

                    //Initiating FROM
                    EntityCollection From = new EntityCollection();
                    Entity QueueEmail = new Entity("activityparty");
                    QueueEmail.Attributes.Add("partyid", new EntityReference("queue", ((EntityReference)BusinessUnit.Attributes["new_emailqueue"]).Id));
                    From.Entities.Add(QueueEmail);

                    //create new Email Activity
                    Entity email = new Entity("email");
                    email = instTemplateResp.EntityCollection[0];

                    //Email Signature
                    String SignatureLogo = "";
                    String SignatureAddress = "";
                    String CallCenter = "";
                    String Description = "";

                    QueryByAttribute SignatureTemplate = new QueryByAttribute("new_residentialconfiguration");
                    SignatureTemplate.ColumnSet = new ColumnSet(new string[] { "new_value", "new_value2" });
                    SignatureTemplate.Attributes.AddRange(new string[] { "new_name" });
                    SignatureTemplate.Values.AddRange(BusinessUnit.Attributes["name"].ToString() + " Signature");
                    EntityCollection SignatureTemplates = _service.RetrieveMultiple(SignatureTemplate);

                    if (SignatureTemplates != null && SignatureTemplates.Entities.Count > 0)
                    {
                        SignatureLogo = "<b>" + SignatureTemplates.Entities[0].Attributes["new_value"].ToString() + "</b>";
                        SignatureAddress = SignatureTemplates.Entities[0].Attributes["new_value2"].ToString();
                    }

                    //add by bili feb 2017
                    Entity helpcategory = _service.Retrieve("new_helpcategory", ((EntityReference)_incident.Attributes["new_helpcategory"]).Id, new ColumnSet(new String[] { "new_name" }));
                    if (BusinessUnit.Contains("address1_telephone1"))
                        CallCenter = BusinessUnit.Attributes["address1_telephone1"].ToString();
                    if (helpcategory != null)
                    {
                        if (_incident.Contains("new_description") && helpcategory.Attributes["new_name"].ToString().ToUpper() == "INCIDENT")
                            Description = _incident.Attributes["new_description"].ToString();
                    }


                    String Subject = email.Attributes["subject"].ToString();
                    String MessageContent = email.Attributes["description"].ToString();

                    //add by bili feb 2017
                    MessageContent = MessageContent.Replace("{Call_Center}", CallCenter);
                    MessageContent = MessageContent.Replace("{Emergency_Description}", Description);

                    MessageContent = MessageContent.Replace("{Signature_Logo}", SignatureLogo);
                    MessageContent = MessageContent.Replace("{Signature_Address}", SignatureAddress);
                    MessageContent = MessageContent.Replace("\n", "<br />");

                    int casestatus = ((OptionSetValue)_incident.Attributes["new_helpdeskstatus"]).Value;

                    //throw new InvalidPluginExecutionException("error " + casestatus.ToString());
                    if (casestatus == 2)
                    {
                        //find pic head based on case
                        SqlConnection conn = new SqlConnection(connectionstring);
                        SqlCommand cmd = new SqlCommand("SPPICHeadByCaseNumber_GET", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CaseNumber", _incident.Attributes["ticketnumber"].ToString());
                        conn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                        DataTable dt = new DataTable();
                        adapter.Fill(dt);
                        conn.Close();

                        string rep = "";

                        foreach (DataRow dr in dt.Rows)
                            rep += dr["FullName"].ToString() + ", ";

                        MessageContent = MessageContent.Replace("{Representative}", rep);

                        // get help duration                        
                        Entity Enthelp = _service.Retrieve("new_helpname", ((EntityReference)_incident.Attributes["new_helpname"]).Id, new ColumnSet(true));
                        string helpduration = Enthelp.Attributes["new_duration"].ToString();

                        MessageContent = MessageContent.Replace("{CaseDuration}", helpduration);
                    }

                    email.Attributes["regardingobjectid"] = new EntityReference(_incident.LogicalName, _incident.Id);
                    email.Attributes["from"] = From;
                    email.Attributes["to"] = _ToAddress;
                    email.Attributes["subject"] = Subject;
                    email.Attributes["description"] = MessageContent;
                    Guid EmailID = _service.Create(email);

                    SendEmailRequest reqSendEmail = new SendEmailRequest();
                    reqSendEmail.EmailId = EmailID;
                    reqSendEmail.TrackingToken = "";
                    reqSendEmail.IssueSend = true;

                    SendEmailResponse res = (SendEmailResponse)_service.Execute(reqSendEmail);
                }
            }
            catch (Exception ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void Save_PluginErrorLog(Exception _exc, String _initiatingUser, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
                cmd.Parameters.AddWithValue("@Message", _exc.Message);
                cmd.Parameters.AddWithValue("@Source", _exc.Source);
                cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
                if (_exc.InnerException != null)
                {
                    cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                    cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
                }
                cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
                cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Error occured in Plugin : Saving Error Log");
            }
        }

        //static private void Send_Email(Int32 _OrgID, Int32 _SiteID, String _CaseNumber, String _CRMCaseID, Int32 _CaseAging, out String _Log)
        //{
        //    //IOrganizationService crmservice = crmservice_conn();

        //    //String DBConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
        //    //SqlConnection conn = new SqlConnection(DBConnectionString);

        //    //String EmailTemplateName = ConfigurationManager.AppSettings["EmailTemplateName"].ToString();

        //    //Entity mycase = crmservice.Retrieve("incident", new Guid(_CRMCaseID), new ColumnSet(new string[] { "new_unit" }));
        //    //Entity unit = crmservice.Retrieve("new_unit", ((EntityReference)mycase.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
        //    //Entity businessunit = crmservice.Retrieve("businessunit", ((EntityReference)unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue", "name" }));

        //    //QueryByAttribute templatequery = new QueryByAttribute("template");
        //    //templatequery.ColumnSet = new ColumnSet(true);
        //    //templatequery.Attributes.AddRange("title");
        //    ////templatequery.Values.AddRange("Case Emergency Notification");
        //    //templatequery.Values.AddRange(EmailTemplateName);
        //    //EntityCollection emailtemplate = crmservice.RetrieveMultiple(templatequery);

        //    if (emailtemplate.Entities.Count > 0)
        //    {
        //        EntityCollection From = new EntityCollection();
        //        Entity QueueEmail = new Entity("activityparty");
        //        QueueEmail.Attributes.Add("partyid", new EntityReference("queue", ((EntityReference)businessunit.Attributes["new_emailqueue"]).Id));
        //        //QueueEmail.Attributes.Add("partyid", new EntityReference("queue", new Guid("8455D8CF-42E2-E311-B8F9-005056A34E26")));// TEST HARCODE id
        //        From.Entities.Add(QueueEmail);

        //        EntityCollection To = new EntityCollection();

        //        String Representative = "";

        //        //Retrieve Recipient
        //        List<MailAddress> recipients = new List<MailAddress>();

        //        DataTable notificationusers = Retrieve_Notification_User(_OrgID, _SiteID, _CaseAging);
        //        if (notificationusers.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr_user in notificationusers.Rows)
        //            {
        //                //update Mailsent Counter
        //                Update_Case_Emergency_Email_Counter(_CaseNumber);


        //                Entity activityPICHead = new Entity("activityparty");
        //                activityPICHead.Attributes.Add("addressused", dr_user["EmailAddress"].ToString());
        //                To.Entities.Add(activityPICHead);

        //                Representative += dr_user["Fullname"].ToString() + ", ";

        //                //MailAddress recipient = new MailAddress();
        //                //recipient.DisplayName = dr_user["FullName"].ToString();
        //                //recipient.EmailAddress = dr_user["EmailAddress"].ToString();

        //                //recipients.Add(recipient);
        //            }
        //        }

        //        if (To.Entities.Count > 0)
        //        {
        //            InstantiateTemplateRequest instTemplateReq = new InstantiateTemplateRequest();
        //            instTemplateReq.TemplateId = emailtemplate.Entities[0].Id;
        //            instTemplateReq.ObjectId = mycase.Id;
        //            instTemplateReq.ObjectType = mycase.LogicalName;

        //            InstantiateTemplateResponse instTemplateResp = (InstantiateTemplateResponse)crmservice.Execute(instTemplateReq);

        //            Entity email = new Entity("email");
        //            email = instTemplateResp.EntityCollection[0];

        //            //Email Signature
        //            String SignatureLogo = "";
        //            String SignatureAddress = "";

        //            QueryByAttribute SignatureTemplate = new QueryByAttribute("new_residentialconfiguration");
        //            SignatureTemplate.ColumnSet = new ColumnSet(new string[] { "new_value", "new_value2" });
        //            SignatureTemplate.Attributes.AddRange(new string[] { "new_name" });
        //            SignatureTemplate.Values.AddRange(businessunit.Attributes["name"].ToString() + " Signature");
        //            EntityCollection SignatureTemplates = crmservice.RetrieveMultiple(SignatureTemplate);

        //            if (SignatureTemplates != null && SignatureTemplates.Entities.Count > 0)
        //            {
        //                SignatureLogo = "<b>" + SignatureTemplates.Entities[0].Attributes["new_value"].ToString() + "</b>";
        //                SignatureAddress = SignatureTemplates.Entities[0].Attributes["new_value2"].ToString();
        //            }

        //            String Subject = email.Attributes["subject"].ToString();
        //            String MessageContent = email.Attributes["description"].ToString();

        //            MessageContent = MessageContent.Replace("{Representative}", Representative);
        //            MessageContent = MessageContent.Replace("{Signature_Logo}", SignatureLogo);
        //            MessageContent = MessageContent.Replace("{Signature_Address}", SignatureAddress);
        //            MessageContent = MessageContent.Replace("\n", "<br />");

        //            email.Attributes["regardingobjectid"] = new EntityReference(mycase.LogicalName, mycase.Id);
        //            email.Attributes["from"] = From;
        //            email.Attributes["to"] = To;
        //            email.Attributes["subject"] = Subject;
        //            email.Attributes["description"] = MessageContent;
        //            Guid EmailID = crmservice.Create(email);

        //            SendEmailRequest reqSendEmail = new SendEmailRequest();
        //            reqSendEmail.EmailId = EmailID;
        //            reqSendEmail.TrackingToken = "";
        //            reqSendEmail.IssueSend = true;
        //            SendEmailResponse res = (SendEmailResponse)crmservice.Execute(reqSendEmail);

        //            _Log = "Email sent successfully";
        //        }
        //        else
        //            _Log = "Invalid Email Address";
        //    }
        //    else
        //        _Log = "Email template not found";
        //}
    }

    class MailAddress
    {
        public String DisplayName { get; set; }
        public String EmailAddress { get; set; }
    }
}
