﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace crm.plugins
{
    public class postchangecaseorigin : IPlugin
    {
        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);

                Entity regardingactivity = (Entity)context.InputParameters["Target"];

                if (regardingactivity.Contains("regardingobjectid"))
                {
                    Entity regardingcase = service.Retrieve("incident", ((EntityReference)regardingactivity.Attributes["regardingobjectid"]).Id, new ColumnSet(true));

                    Entity updatecase = new Entity("incident");
                    updatecase.Id = regardingcase.Id;
                    updatecase.Attributes["caseorigincode"] = new OptionSetValue(2); //email                    
                    service.Update(updatecase);
                }
            }
        }
    }
}
