﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace crm.plugins
{
    public class mobilepushnotificationassigncase : IPlugin
    {
        private static String connectionstring = "";

        public DataTable Get_UserPIC_ByUser(string _userName, String _connectionstring)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Mobile_Get_PICUser_ByUserName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@UserName", _userName);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public void Save_PluginErrorLog(Exception _exc, String _initiatingUser, String _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
            cmd.Parameters.AddWithValue("@Message", _exc.Message);
            cmd.Parameters.AddWithValue("@Source", _exc.Source);
            cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
            if (_exc.InnerException != null)
            {
                cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
            }
            cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
            cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Save_MobileNotification(string _caseNumber, int _caseStatusID, int _userID, int _flagSent, string _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Mobile_Save_Notification", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);
            cmd.Parameters.AddWithValue("@CaseStatusID", _caseStatusID);
            cmd.Parameters.AddWithValue("@UserID", _userID);
            cmd.Parameters.AddWithValue("@FlagSent", _flagSent);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));
            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService systemservice = servicefactory.CreateOrganizationService(null);

                Entity newcase = (Entity)context.InputParameters["Target"];
                Entity PreUpdateCaseImage = (Entity)context.PreEntityImages["PreUpdateCaseImage"];

                if (newcase.Attributes.Contains("new_workedby"))
                {

                    string workby = newcase.Attributes["new_workedby"].ToString();
                    String CaseNumber = PreUpdateCaseImage.Attributes["ticketnumber"].ToString();
                    int HelpdeskStatus = ((OptionSetValue)newcase.Attributes["new_helpdeskstatus"]).Value;
                    //throw new InvalidPluginExecutionException("error " + workby );

                    // Retrieve TMDB Conn String
                    QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                    q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                    q_configuration.Attributes.AddRange(new string[] { "new_name" });
                    q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                    EntityCollection configurations = service.RetrieveMultiple(q_configuration);

                    if (configurations != null)
                    {
                        if (configurations.Entities.Count > 0)
                        {
                            Entity configuration = configurations.Entities[0];
                            connectionstring = configuration.Attributes["new_value"].ToString();
                        }
                    }

                    DataTable dt = Get_UserPIC_ByUser(workby, connectionstring);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            try
                            {
                                int picUserID = (int)row["UserID"];
                                Save_MobileNotification(CaseNumber, HelpdeskStatus, picUserID, 0, connectionstring);
                            }
                            catch (Exception ef)
                            {
                                Save_PluginErrorLog(ef, "mobileNotifUser", connectionstring);
                            }
                        }
                    }
                }
            }
        }
    }
}
