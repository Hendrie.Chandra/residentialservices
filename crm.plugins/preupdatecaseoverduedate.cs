﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;
using System.Data;
using System.ServiceModel;

namespace crm.plugins
{
    public class preupdatecaseoverduedate : IPlugin
    {
        private static String connectionstring = "";
        private static String SavedBy = "";

        private Boolean IsWeekend(DateTime _date)
        {
            return _date.DayOfWeek == DayOfWeek.Saturday || _date.DayOfWeek == DayOfWeek.Sunday;
        }

        private void Save_PluginErrorLog(InvalidPluginExecutionException _exc, String _initiatingUser, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
                cmd.Parameters.AddWithValue("@Message", _exc.Message);
                cmd.Parameters.AddWithValue("@Source", _exc.Source);
                cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
                cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
                cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
                cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                throw new InvalidPluginExecutionException("Error occured in Plugin");
            }
        }

        private void UpdateOverdueDateToTMDDB(String _CaseNumber, DateTime _OverdueDate, String _SavedBy, String _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Update_OverdueDate", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                try
                {
                    IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                    IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);

                    Entity entity = (Entity)context.InputParameters["Target"];
                    Entity PreUpdateCaseImage = (Entity)context.PreEntityImages["PreUpdateCase"];

                    Entity systemuser = adminservice.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname" }));
                    SavedBy = systemuser.Attributes["domainname"].ToString();

                    QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                    q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                    q_configuration.Attributes.AddRange(new string[] { "new_name" });
                    q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                    EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);                    

                    if (configurations != null)
                    {
                        if (configurations.Entities.Count > 0)
                        {
                            Entity configuration = configurations.Entities[0];
                            connectionstring = configuration.Attributes["new_value"].ToString();
                        }
                    }

                    Entity HelpName;
                    if (entity.Contains("new_helpname"))
                        HelpName = adminservice.Retrieve("new_helpname", ((EntityReference)entity.Attributes["new_helpname"]).Id, new ColumnSet(new String[] { "new_duration", "new_name" }));
                    else
                        HelpName = adminservice.Retrieve("new_helpname", ((EntityReference)PreUpdateCaseImage.Attributes["new_helpname"]).Id, new ColumnSet(new String[] { "new_duration", "new_name" }));

                    String HelpDeskName = HelpName.Attributes["new_name"].ToString();

                    QueryByAttribute findNoOverdueRequests = new QueryByAttribute("new_residentialconfiguration");
                    findNoOverdueRequests.ColumnSet = new ColumnSet(new String[] { "new_name", "new_value" });
                    findNoOverdueRequests.Attributes.AddRange("new_name");
                    findNoOverdueRequests.Values.AddRange("No Overdue");
                    EntityCollection NoOverdueReq = service.RetrieveMultiple(findNoOverdueRequests);

                    Boolean IsNoOverdue = false;
                    foreach (Entity ent in NoOverdueReq.Entities)
                    {
                        if (HelpDeskName == ent.Attributes["new_value"].ToString())
                            IsNoOverdue = true;
                    }

                    //throw new InvalidPluginExecutionException("AA");
                    
                    if (!IsNoOverdue)
                    {
                        if (entity.Contains("new_helpcategory") && !PreUpdateCaseImage.Contains("followupby"))
                        {
                            Entity helpcategory = service.Retrieve("new_helpcategory", ((EntityReference)entity.Attributes["new_helpcategory"]).Id, new ColumnSet(new String[] { "new_name" }));
                            String HelpCategory = helpcategory.Attributes["new_name"].ToString().ToUpper();
                            if (HelpCategory == "COMPLAINT")
                                entity.Attributes["new_helpdeskstatus"] = new OptionSetValue(2);
                            else if (HelpCategory == "INFORMATION")
                                entity.Attributes["new_helpdeskstatus"] = new OptionSetValue(4);
                        }

                        if (entity.Contains("new_helpdeskstatus") && (((OptionSetValue)entity.Attributes["new_helpdeskstatus"]).Value == 2 ||
                            ((OptionSetValue)entity.Attributes["new_helpdeskstatus"]).Value == 4) && !PreUpdateCaseImage.Contains("followupby"))
                        {
                            Int32 Duration = Int32.Parse(HelpName.Attributes["new_duration"].ToString());
                            DateTime StartDate = DateTime.Now;
                            DateTime CurrentDate = StartDate;
                            DateTime EndDate = DateTime.Now.AddDays(Duration);

                            ConditionExpression condition1 = new ConditionExpression();
                            condition1.AttributeName = "new_date";
                            condition1.Operator = ConditionOperator.GreaterEqual;
                            condition1.Values.Add(DateTime.Now.Date.ToUniversalTime());

                            FilterExpression filter1 = new FilterExpression();
                            filter1.Conditions.Add(condition1);

                            QueryExpression query = new QueryExpression("new_offdays");
                            query.ColumnSet = new ColumnSet(new String[] { "new_date", "new_remarks" });
                            query.Criteria.AddFilter(filter1);
                            EntityCollection OffDays = adminservice.RetrieveMultiple(query);

                            List<DateTime> ListOffDays = new List<DateTime>();

                            if (OffDays.Entities.Count > 0)
                            {
                                foreach (Entity ent in OffDays.Entities)
                                {
                                    ListOffDays.Add(DateTime.Parse(ent.Attributes["new_date"].ToString()).ToLocalTime());
                                }
                            }

                            for (int i = 1; i <= Duration; i++)
                            {
                                var IsOffDays = ListOffDays.FindAll(item => item.ToString("yyyy-MM-dd") == CurrentDate.ToString("yyyy-MM-dd"));
                                if (IsWeekend(CurrentDate))
                                {
                                    EndDate = EndDate.AddDays(1);
                                    Duration++;
                                }
                                else if (IsOffDays.Count > 0)
                                {
                                    EndDate = EndDate.AddDays(1);
                                    Duration++;
                                }
                                CurrentDate = CurrentDate.AddDays(1);
                            }

                            entity.Attributes.Add("followupby", EndDate);
                            
                            String CaseNumber = PreUpdateCaseImage.Attributes["ticketnumber"].ToString();                            

                            UpdateOverdueDateToTMDDB(CaseNumber, EndDate, SavedBy, connectionstring);
                        }
                    }
                }
                catch (InvalidPluginExecutionException ex)
                {
                    Save_PluginErrorLog(ex, SavedBy, connectionstring);
                }
            }
        }
    }
}
