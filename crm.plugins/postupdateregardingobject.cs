﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;

namespace crm.plugins
{
    public class postupdateregardingobject : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService _service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                IOrganizationService __service = servicefactory.CreateOrganizationService(null);

                Entity Target = (Entity)context.InputParameters["Target"];

                try
                {
                    if (Target.Contains("new_converttocaseparameters"))
                    {
                        string[] activities = Target.Attributes["new_converttocaseparameters"].ToString().Split(';');

                        Entity activity = __service.Retrieve(activities[1], new Guid(activities[0]), new ColumnSet("regardingobjectid"));
                        activity.Attributes["regardingobjectid"] = new EntityReference("incident", Target.Id);
                        __service.Update(activity);
                    }
                }
                catch (InvalidPluginExecutionException ex)
                {
                    throw new InvalidPluginExecutionException("An error occurred in the plug-in", ex);
                }
            }
        }
    }
}
