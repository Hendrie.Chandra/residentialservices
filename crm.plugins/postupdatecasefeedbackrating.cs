﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace crm.plugins
{
    /*add on 21 August 2017*/
    /*requestor : Adhi Sugiarto (Lippokarawaci)*/
    /*sumbit rating ke tmdb*/
    public class postupdatecasefeedbackrating : IPlugin
    {
        private static String connectionstring = "";
        private static String SavedBy = "";
        private static String caseNumber = "";
        private decimal? rating = null;
        private static string feedBackDescription = "";

        private void saveRatingToTMDB(string _casenumber,decimal? _rating, string _description, string _connectionstring, string _saveby)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_FeedBack_Rating", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);
                cmd.Parameters.AddWithValue("@Rating", _rating);
                cmd.Parameters.AddWithValue("@FeedBackDescription", _description);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void Save_PluginErrorLog(Exception _exc, String _initiatingUser, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
                cmd.Parameters.AddWithValue("@Message", _exc.Message);
                cmd.Parameters.AddWithValue("@Source", _exc.Source);
                cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
                if (_exc.InnerException != null)
                {
                    cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                    cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
                }
                cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
                cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Error occured in Plugin : Saving Error Log");
            }
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));
            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService systemservice = servicefactory.CreateOrganizationService(null);

                Entity newcase = (Entity)context.InputParameters["Target"];

                Entity PostUpdateCaseImage = (Entity)context.PostEntityImages["PostUpdateCaseImage"];

                rating = null;
                feedBackDescription = "";

                if (PostUpdateCaseImage.Contains("ticketnumber"))
                    caseNumber = PostUpdateCaseImage.Attributes["ticketnumber"].ToString();
                if (PostUpdateCaseImage.Contains("new_rating"))
                    rating = (decimal)PostUpdateCaseImage.Attributes["new_rating"];
                if (PostUpdateCaseImage.Contains("new_satisfactiondescription"))
                    feedBackDescription = PostUpdateCaseImage.Attributes["new_satisfactiondescription"].ToString();

                //execute plugin jika filed rating terisi
                if (newcase.Attributes.Contains("new_rating") || newcase.Attributes.Contains("new_satisfactiondescription"))
                {                    
                    try
                    {
                        // Retrieve initiating user
                        Entity user = service.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname", "fullname" }));
                        SavedBy = user.Attributes["domainname"].ToString();

                        QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                        q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                        q_configuration.Attributes.AddRange(new string[] { "new_name" });
                        q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                        EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);

                        if (configurations != null)
                        {
                            if (configurations.Entities.Count > 0)
                            {
                                Entity configuration = configurations.Entities[0];
                                connectionstring = configuration.Attributes["new_value"].ToString();

                                //if (rating.Equals(0) || rating.Equals(0.0))
                                //    rating = null;

                                if (!string.IsNullOrEmpty(caseNumber))
                                    saveRatingToTMDB(caseNumber, rating, feedBackDescription, connectionstring, SavedBy);
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        throw new InvalidPluginExecutionException(ex.Message);
                    }
                }
            }
        }
    }
}
