﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;
using System.Data;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace crm.plugins
{
    public class postcreatehelpname : IPlugin
    {
        
        private Int32 save_help(Int32 _helpid, Int32 _orgid, Int32 _siteid, String _crmhelpid, String _helpname, Int32 _parenthelpid, Int32 _helpcategoryid,
            Int32 _departmentid, bool _ipklincludable, Int16 _duration, Int32 _teamid, bool _isactive, String _savedby, String _connstr)
        {
            SqlConnection conn = new SqlConnection(_connstr);
            SqlCommand cmd = new SqlCommand("SP_Save_Help", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpID", _helpid);
            cmd.Parameters.AddWithValue("@OrgID", _orgid);
            cmd.Parameters.AddWithValue("@SiteID", _siteid);
            cmd.Parameters.AddWithValue("@CRMHelpID", _crmhelpid);
            cmd.Parameters.AddWithValue("@HelpName", _helpname);
            if (_parenthelpid != 0)
                cmd.Parameters.AddWithValue("@ParentHelpID", _parenthelpid);
            if (_helpcategoryid != 0)
                cmd.Parameters.AddWithValue("@HelpCategoryID", _helpcategoryid);
            if (_departmentid != 0)
                cmd.Parameters.AddWithValue("@DepartmentID", _departmentid);
            cmd.Parameters.AddWithValue("@IPKLIncludable", _ipklincludable);
            cmd.Parameters.AddWithValue("@Duration", _duration);
            cmd.Parameters.AddWithValue("@TeamID", _teamid);
            cmd.Parameters.AddWithValue("@IsActive", _isactive);
            cmd.Parameters.AddWithValue("@SavedBy", _savedby);

            conn.Open();
            Int32 returnvalue = cmd.ExecuteNonQuery();
            conn.Close();

            return returnvalue;
        }

        private Int32 find_department(Guid _departmentguid, String _connstr)
        {
            SqlConnection conn = new SqlConnection(_connstr);
            SqlCommand cmd = new SqlCommand("SELECT DepartmentID FROM MSDepartment WHERE CRMDepartmentID = '" + _departmentguid + "'", conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();

            return Int32.Parse(dt.Rows[0]["DepartmentID"].ToString());
        }

        private Int32 find_helpid(Guid _helpguid, String _connstr)
        {
            SqlConnection conn = new SqlConnection(_connstr);
            SqlCommand cmd = new SqlCommand("SELECT HelpID FROM MSHelp WHERE CRMHelpID = '" + _helpguid + "'", conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();
            
            Int32 returnhelpid = 0;            
            if (dt.Rows.Count > 0) returnhelpid = Int32.Parse(dt.Rows[0]["HelpID"].ToString());
            return returnhelpid;
        }

        private Int32 find_helpcategoryid(Guid _helpcategoryguid, String _connstr)
        {
            SqlConnection conn = new SqlConnection(_connstr);
            SqlCommand cmd = new SqlCommand("SELECT HelpCategoryID FROM MSHelpCategory WHERE CRMHelpCategoryID = '" + _helpcategoryguid + "'", conn);
            conn.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conn.Close();

            Int32 returnhelpcategoryid = 0;
            if (dt.Rows.Count > 0) returnhelpcategoryid = Int32.Parse(dt.Rows[0]["HelpCategoryID"].ToString());
            return returnhelpcategoryid;
        }
       
        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.InitiatingUserId);                

                QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                q_configuration.Attributes.AddRange(new string[] { "new_name" });
                q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);
                String connectionstring = "";

                if (configurations != null)
                {
                    if (configurations.Entities.Count > 0)
                    {
                        Entity configuration = configurations.Entities[0];
                        connectionstring = configuration.Attributes["new_value"].ToString();
                    }
                }

                Entity helpname = (Entity)context.InputParameters["Target"];

                try
                {
                    String Name = "";
                    if (helpname.Attributes.Contains("new_name"))
                        Name = helpname.Attributes["new_name"].ToString();

                    Int32 ParentHelpID = 0;
                    if (helpname.Attributes.Contains("new_parent"))
                        ParentHelpID = find_helpid(((EntityReference)helpname.Attributes["new_parent"]).Id, connectionstring);

                    Int32 HelpCategoryID = 0;
                    if (helpname.Attributes.Contains("new_helpcategory"))
                        HelpCategoryID = find_helpcategoryid(((EntityReference)helpname.Attributes["new_helpcategory"]).Id, connectionstring);

                    Int32 DepartmentID = 0;
                    if (helpname.Attributes.Contains("new_department"))
                        DepartmentID = find_department(((EntityReference)helpname.Attributes["new_department"]).Id, connectionstring);

                    Boolean IPKLIncludable = false;
                    if (helpname.Contains("new_ipklincludable") && helpname.GetAttributeValue<bool>("new_ipklincludable"))                    
                        IPKLIncludable = true;

                    Int16 Duration = 0;
                    if (helpname.Attributes.Contains("new_duration"))
                        Duration = Int16.Parse(helpname.Attributes["new_duration"].ToString());

                    Entity user = adminservice.Retrieve("systemuser", context.UserId, new ColumnSet(new String[] { "domainname" }));
                    String SavedBy = user.Attributes["domainname"].ToString();
                    
                    save_help(0, 1, 1, helpname.Id.ToString(), Name, ParentHelpID, HelpCategoryID, DepartmentID, IPKLIncludable, Duration, 1, true, SavedBy, connectionstring);
                }
                catch (FaultException<OrganizationServiceFault> ex)
                {
                    throw new InvalidPluginExecutionException("An error occurred in the plug-in.", ex);
                }                              
            }

        }
    }
}
