﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Crm.Sdk;
using Microsoft.Crm.Sdk.Messages;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using System.Xml;
using System.IO;
using System.Xml.Xsl;

namespace crm.plugins
{
    public class test_postcreatecase: IPlugin
    {
        private static String SavedBy = "";
        private static String connectionstring = "";

        private void SaveToTMDDB(Int32 _OrgID, String _CRMSiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, String _CRMHelpID,
            String _CRMLocationID, String _CRMSubLocationID, Int32 _CaseOriginID, String _PSName, String _PSCode, String _UnitCode, String _UnitNo,
            String _NamaPelapor, String _TeleponPelapor, String _EmailPelapor, String _Description, DateTime? _OverdueDate, String _SavedBy, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Case", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@CRMSiteID", _CRMSiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@CRMHelpID", _CRMHelpID);
                cmd.Parameters.AddWithValue("@CRMLocationID", _CRMLocationID);
                cmd.Parameters.AddWithValue("@CRMSubLocationID", _CRMSubLocationID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@PSName", _PSName);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@OverdueDate", _OverdueDate);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                //throw new InvalidPluginExecutionException(_CRMSiteID + " xx " + ex.Message);
   
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void CreateTicketHistoryToTMDDB(String _casenumber, Int32 _status, String _remarks, String _updatedby, String _savedby, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_TicketHistory", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _status);
                cmd.Parameters.AddWithValue("@FullName", _updatedby);
                cmd.Parameters.AddWithValue("@Remarks", _remarks);
                cmd.Parameters.AddWithValue("@SavedBy", _savedby);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                //throw new InvalidPluginExecutionException("2");

                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void sendEmailToCustomer(IOrganizationService _service, Entity _incident, Int32 _helpdeskstatus)
        {
            try
            {
                QueryByAttribute FindCustomerEmail = new QueryByAttribute("new_email");
                FindCustomerEmail.ColumnSet = new ColumnSet(new String[] { "new_name" });
                FindCustomerEmail.Attributes.AddRange(new string[] { "new_contact" });
                FindCustomerEmail.Values.AddRange(((EntityReference)_incident.Attributes["customerid"]).Id);
                EntityCollection CustomerEmailAddress = _service.RetrieveMultiple(FindCustomerEmail);

                if (_incident.Attributes.Contains("new_email") || CustomerEmailAddress.Entities.Count > 0)
                {
                    Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                    Entity businessunit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue", "new_emailtemplatepending", "new_emailtemplatenopending", "new_emailtemplatecancelledcustomer", "new_emailtemplateprogresscompleted", "new_emailtemplatecaseclosed", "new_emailtemplaterejected", "new_emailtemplateonhold" }));

                    String findTemplate = "";
                    switch (_helpdeskstatus)
                    {
                        case 1: //pending
                            findTemplate = businessunit.Attributes["new_emailtemplatepending"].ToString();
                            break;
                        case 2: //assigned to pic
                        case 4: //In proggress - for Information category
                            findTemplate = businessunit.Attributes["new_emailtemplatenopending"].ToString();
                            break;
                    }

                    EntityCollection To = new EntityCollection();

                    //get Owner email address from new_email
                    if (CustomerEmailAddress.Entities.Count > 0)
                    {
                        foreach (Entity emailToSend in CustomerEmailAddress.Entities)
                        {
                            String OwnerEmailAddress = emailToSend.Attributes["new_name"].ToString();
                            if (Regex.IsMatch(OwnerEmailAddress, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                            {
                                Entity OwnerEmail = new Entity("activityparty");
                                OwnerEmail.Attributes.Add("partyid", new EntityReference("contact", ((EntityReference)_incident.Attributes["customerid"]).Id));
                                OwnerEmail.Attributes.Add("addressused", OwnerEmailAddress);
                                To.Entities.Add(OwnerEmail);
                            }
                        }
                    }

                    //get Requestor email address
                    if (_incident.Contains("new_email") &&
                        Regex.IsMatch(_incident.Attributes["new_email"].ToString(), @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                    {
                        String RequestorEmailAddress = _incident.Attributes["new_email"].ToString();
                        Entity CustomerEmail = new Entity("activityparty");
                        CustomerEmail.Attributes.Add("addressused", RequestorEmailAddress);
                        To.Entities.Add(CustomerEmail);
                    }

                    if (To.Entities.Count > 0)
                        SendEmail(_service, _incident, findTemplate, To);
                }
            }
            catch (Exception ex)
            {
                //throw new InvalidPluginExecutionException("3");

                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void SendEmailToPICHead(IOrganizationService _service, Entity _incident, Int32 _helpdeskstatus, String _connectionstring)
        {
            try
            {
                String CaseNumber = _incident.Attributes["ticketnumber"].ToString();

                //find pic head based on case
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Retrieve_PICHead", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();
                adapter.Fill(dt);
                conn.Close();

                if (dt.Rows.Count > 0)
                {
                    EntityCollection To = new EntityCollection();

                    foreach (DataRow dr in dt.Rows)
                    {
                        String PICHeadEmailAddress = dr["EmailAddress"].ToString();

                        if (!String.IsNullOrWhiteSpace(PICHeadEmailAddress) &&
                        Regex.IsMatch(PICHeadEmailAddress, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                        {
                            Entity activityPICEmailAddress = new Entity("activityparty");
                            activityPICEmailAddress.Attributes.Add("addressused", PICHeadEmailAddress);
                            To.Entities.Add(activityPICEmailAddress);
                        }
                    }

                    if (To.Entities.Count > 0)
                    {
                        Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                        Entity businessunit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue", "new_emailtemplatenopendingpic", "new_emailtemplatecancelledpic", "new_emailtemplateonhold", "new_emailtemplatenotsatisfied" }));

                        String findTemplate = "";
                        switch (_helpdeskstatus)
                        {
                            case 2: //Assigned To PIC
                                findTemplate = businessunit.Attributes["new_emailtemplatenopendingpic"].ToString();
                                break;
                        }

                        SendEmail(_service, _incident, findTemplate, To);
                    }
                }
            }
            catch (Exception ex)
            {
                //throw new InvalidPluginExecutionException("4");

                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void SendEmail(IOrganizationService _service, Entity _incident, String _EmailTemplateName, EntityCollection _ToAddress)
        {
            try
            {
                Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                Entity BusinessUnit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(true));

                //Find Template
                QueryByAttribute templatequery = new QueryByAttribute("template");
                templatequery.ColumnSet = new ColumnSet(true);
                templatequery.Attributes.AddRange("title");
                templatequery.Values.AddRange(_EmailTemplateName);
                EntityCollection retrieved = _service.RetrieveMultiple(templatequery);

                if (retrieved != null && retrieved.Entities.Count > 0)
                {
                    //retrieving email template
                    InstantiateTemplateRequest instTemplateReq = new InstantiateTemplateRequest();
                    instTemplateReq.TemplateId = retrieved.Entities[0].Id;
                    instTemplateReq.ObjectId = _incident.Id;
                    instTemplateReq.ObjectType = _incident.LogicalName;

                    InstantiateTemplateResponse instTemplateResp = (InstantiateTemplateResponse)_service.Execute(instTemplateReq);

                    //Initiating FROM
                    EntityCollection From = new EntityCollection();
                    Entity QueueEmail = new Entity("activityparty");
                    QueueEmail.Attributes.Add("partyid", new EntityReference("queue", ((EntityReference)BusinessUnit.Attributes["new_emailqueue"]).Id));
                    From.Entities.Add(QueueEmail);

                    //create new Email Activity
                    Entity email = new Entity("email");
                    email = instTemplateResp.EntityCollection[0];

                    //Email Signature
                    String SignatureLogo = "";
                    String SignatureAddress = "";

                    QueryByAttribute SignatureTemplate = new QueryByAttribute("new_residentialconfiguration");
                    SignatureTemplate.ColumnSet = new ColumnSet(new string[] { "new_value", "new_value2" });
                    SignatureTemplate.Attributes.AddRange(new string[] { "new_name" });
                    SignatureTemplate.Values.AddRange(BusinessUnit.Attributes["name"].ToString() + " Signature");
                    EntityCollection SignatureTemplates = _service.RetrieveMultiple(SignatureTemplate);

                    if (SignatureTemplates != null && SignatureTemplates.Entities.Count > 0)
                    {
                        SignatureLogo = "<b>" + SignatureTemplates.Entities[0].Attributes["new_value"].ToString() + "</b>";
                        SignatureAddress = SignatureTemplates.Entities[0].Attributes["new_value2"].ToString();
                    }

                    String Subject = email.Attributes["subject"].ToString();
                    String MessageContent = email.Attributes["description"].ToString();

                    MessageContent = MessageContent.Replace("{Signature_Logo}", SignatureLogo);
                    MessageContent = MessageContent.Replace("{Signature_Address}", SignatureAddress);
                    MessageContent = MessageContent.Replace("\n", "<br />");

                    int casestatus = ((OptionSetValue)_incident.Attributes["new_helpdeskstatus"]).Value;
                    if (casestatus == 2)
                    {
                        //find pic head based on case
                        SqlConnection conn = new SqlConnection(connectionstring);
                        SqlCommand cmd = new SqlCommand("SP_Retrieve_PICHead", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CaseNumber", _incident.Attributes["ticketnumber"].ToString());
                        conn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                        DataTable dt = new DataTable();
                        adapter.Fill(dt);
                        conn.Close();

                        string rep = "";

                        foreach (DataRow dr in dt.Rows)
                            rep += dr["FullName"].ToString() + ", ";

                        MessageContent = MessageContent.Replace("{Representative}", rep);

                        // get help duration                        
                        Entity Enthelp = _service.Retrieve("new_helpname", ((EntityReference)_incident.Attributes["new_helpname"]).Id, new ColumnSet(true));
                        string helpduration = Enthelp.Attributes["new_duration"].ToString();

                        MessageContent = MessageContent.Replace("{CaseDuration}", helpduration);
                    }

                    email.Attributes["regardingobjectid"] = new EntityReference(_incident.LogicalName, _incident.Id);
                    email.Attributes["from"] = From;
                    email.Attributes["to"] = _ToAddress;
                    email.Attributes["subject"] = Subject;
                    email.Attributes["description"] = MessageContent;
                    Guid EmailID = _service.Create(email);

                    SendEmailRequest reqSendEmail = new SendEmailRequest();
                    reqSendEmail.EmailId = EmailID;
                    reqSendEmail.TrackingToken = "";
                    reqSendEmail.IssueSend = true;

                    SendEmailResponse res = (SendEmailResponse)_service.Execute(reqSendEmail);
                }
            }
            catch (Exception ex)
            {
                //throw new InvalidPluginExecutionException("5");

                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void Save_PluginErrorLog(Exception _exc, String _initiatingUser, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
                cmd.Parameters.AddWithValue("@Message", _exc.Message);
                cmd.Parameters.AddWithValue("@Source", _exc.Source);
                cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
                if (_exc.InnerException != null)
                {
                    cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                    cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
                }
                cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
                cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
                throw new InvalidPluginExecutionException(_exc.Message);
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Error occured in Plugin : Saving Error Log");
            }
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                try
                {
                    IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                    IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);
                    IOrganizationService systemservice = servicefactory.CreateOrganizationService(null);

                    Entity newcase = (Entity)context.InputParameters["Target"];
                    //throw new InvalidPluginExecutionException("6ca");

                    if (newcase.Attributes.Contains("new_helpcategory") && newcase.Attributes.Contains("new_helpname"))
                    {
                        // Retrieve initiating user
                        Entity user = service.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname", "fullname" }));
                        SavedBy = user.Attributes["domainname"].ToString();

                        // Retrieve TMDB Conn String
                        QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                        q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                        q_configuration.Attributes.AddRange(new string[] { "new_name" });
                        q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                        EntityCollection configurations = service.RetrieveMultiple(q_configuration);

                        if (configurations != null)
                        {
                            if (configurations.Entities.Count > 0)
                            {
                                Entity configuration = configurations.Entities[0];
                                connectionstring = configuration.Attributes["new_value"].ToString();
                            }
                        }

                        ///check whether the incident was created by Call Center
                        ///

                        QueryExpression query = new QueryExpression();
                        // Return distinct values
                        query.Distinct = true;
                        // Setup the query for the systemuser entity
                        query.EntityName = "systemuser";
                        // Specify the columns to retrieve
                        ColumnSet columns = new ColumnSet();
                        columns.AddColumns(new String[] { "fullname", "businessunitid", "title", "address1_telephone1", "systemuserid" });
                        query.ColumnSet = columns;
                        query.Criteria = new FilterExpression();
                        query.Criteria.FilterOperator = LogicalOperator.And;

                        // Create the systemuserid condition
                        ConditionExpression condition1 = new ConditionExpression();
                        condition1.AttributeName = "systemuserid";
                        condition1.Operator = ConditionOperator.Equal;
                        condition1.Values.Add(((EntityReference)newcase.Attributes["createdby"]).Id.ToString());

                        query.Criteria.AddCondition(condition1);

                        // Create the link from the systemuser to the systemuserroles entity
                        LinkEntity linkEntity1 = new LinkEntity();
                        linkEntity1.JoinOperator = JoinOperator.Natural;
                        linkEntity1.LinkFromEntityName = "systemuser";
                        linkEntity1.LinkFromAttributeName = "systemuserid";
                        linkEntity1.LinkToEntityName = "systemuserroles";
                        linkEntity1.LinkToAttributeName = "systemuserid";

                        // Create the link from the systemuserroles to the role entity
                        LinkEntity linkEntity2 = new LinkEntity();
                        linkEntity2.JoinOperator = JoinOperator.Natural;
                        linkEntity2.LinkFromEntityName = "systemuserroles";
                        linkEntity2.LinkFromAttributeName = "roleid";
                        linkEntity2.LinkToEntityName = "role";
                        linkEntity2.LinkToAttributeName = "roleid";
                        // You have specified the alias "aj". An alias is not supported in query expressions

                        linkEntity2.LinkCriteria = new FilterExpression();
                        linkEntity2.LinkCriteria.FilterOperator = LogicalOperator.And;

                        // Create the name condition
                        ConditionExpression condition2 = new ConditionExpression();
                        condition2.AttributeName = "name";
                        condition2.Operator = ConditionOperator.Equal;
                        condition2.Values.Add("Call Center");

                        linkEntity2.LinkCriteria.Conditions.Add(condition2);

                        linkEntity1.LinkEntities.Add(linkEntity2);

                        query.LinkEntities.Add(linkEntity1);

                        // Execute the query and return the result
                        EntityCollection isCallCenter = systemservice.RetrieveMultiple(query);
                        //throw new InvalidPluginExecutionException("6b");

                        if (isCallCenter != null && isCallCenter.Entities.Count > 0)
                        {
                            Entity unit = systemservice.Retrieve("new_unit", ((EntityReference)newcase.Attributes["new_unit"]).Id, new ColumnSet("ownerid"));

                            //share record to team
                            GrantAccessRequest grantAccess = new GrantAccessRequest();
                            grantAccess.PrincipalAccess = new PrincipalAccess();
                            grantAccess.PrincipalAccess.AccessMask = AccessRights.ReadAccess | AccessRights.WriteAccess | AccessRights.AppendAccess | AccessRights.AppendToAccess;
                            grantAccess.PrincipalAccess.Principal = (EntityReference)unit.Attributes["ownerid"];
                            grantAccess.Target = new EntityReference("incident", newcase.Id);

                            systemservice.Execute(grantAccess);
                        }

                        QueryByAttribute qba_crmresidential = new QueryByAttribute("new_residentialconfiguration");
                        qba_crmresidential.ColumnSet = new ColumnSet(true);
                        qba_crmresidential.Attributes.AddRange("new_name");
                        qba_crmresidential.Values.AddRange("CRM Residential Service Admin");
                        EntityCollection CRMResidentialUser = service.RetrieveMultiple(qba_crmresidential);
                        String userlogin = CRMResidentialUser[0].Attributes["new_value"].ToString();

                        Entity helpcategory = service.Retrieve("new_helpcategory", ((EntityReference)newcase.Attributes["new_helpcategory"]).Id, new ColumnSet(new String[] { "new_name" }));
                        int helpdeskstatus = ((OptionSetValue)newcase.Attributes["new_helpdeskstatus"]).Value;

                        if (user.Attributes["domainname"].ToString().ToUpper() != userlogin.ToUpper())
                        {
                            //save to TMDDB                            
                            Int32 OrgID = 1;

                            Entity Unit = service.Retrieve("new_unit", ((EntityReference)newcase.Attributes["new_unit"]).Id, new ColumnSet(new String[] { "new_name", "new_unitno", "owningbusinessunit" }));

                            String CRMSiteID = ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id.ToString();
                            String CRMCaseID = newcase.Id.ToString();
                            String CaseNumber = newcase.Attributes["ticketnumber"].ToString();
                            Int32 CaseStatusID = 1;
                            String CRMHelpID = ((EntityReference)newcase.Attributes["new_helpname"]).Id.ToString();

                            String CRMLocationID = null;
                            String CRMSubLocationID = null;
                            if (newcase.Attributes.Contains("new_location") && newcase.Attributes.Contains("new_sublocation"))
                            {
                                CRMLocationID = ((EntityReference)newcase.Attributes["new_location"]).Id.ToString();
                                CRMSubLocationID = ((EntityReference)newcase.Attributes["new_sublocation"]).Id.ToString();
                            }
                            Int32 CaseOriginID = ((OptionSetValue)newcase.Attributes["caseorigincode"]).Value;
                            Entity Owner = service.Retrieve("contact", ((EntityReference)newcase.Attributes["customerid"]).Id, new ColumnSet(new String[] { "new_pscode", "fullname" }));
                            String PSName = Owner.Attributes["fullname"].ToString();
                            String PSCode = Owner.Attributes["new_pscode"].ToString();

                            String UnitCode = Unit.Attributes["new_name"].ToString();
                            String UnitNo = Unit.Attributes["new_unitno"].ToString();

                            String NamaPelapor = "";
                            if (newcase.Attributes.Contains("new_name"))
                                NamaPelapor = newcase.Attributes["new_name"].ToString();

                            String TeleponPelapor = "";
                            if (newcase.Attributes.Contains("new_phone"))
                                TeleponPelapor = newcase.Attributes["new_phone"].ToString();

                            String EmailPelapor = "";
                            if (newcase.Attributes.Contains("new_email"))
                                EmailPelapor = newcase.Attributes["new_email"].ToString();

                            String DescriptionRequest = "";
                            if (newcase.Attributes.Contains("new_description"))
                                DescriptionRequest = newcase.Attributes["new_description"].ToString();

                            Nullable<DateTime> overdueDate = null;
                            if (newcase.Attributes.Contains("followupby"))
                                overdueDate = DateTime.Parse(newcase.Attributes["followupby"].ToString());

                            SaveToTMDDB(OrgID, CRMSiteID, CRMCaseID, CaseNumber, CaseStatusID, CRMHelpID, CRMLocationID, CRMSubLocationID,
                                CaseOriginID, PSName, PSCode, UnitCode, UnitNo, NamaPelapor, TeleponPelapor, EmailPelapor, DescriptionRequest, overdueDate, SavedBy, connectionstring);

                            String Remarks = "Case created";
                            Entity tickethistory = new Entity("new_tickethistory");
                            tickethistory.Attributes["new_caseid"] = new EntityReference("incident", newcase.Id);
                            Int32 HelpdeskStatus = 1;
                            if (helpcategory.Attributes["new_name"].ToString().ToUpper() == "INFORMATION")
                                HelpdeskStatus = 4;
                            else if (helpcategory.Attributes["new_name"].ToString().ToUpper() == "COMPLAINT")
                            {
                                HelpdeskStatus = 2;
                                SqlConnection conn = new SqlConnection(connectionstring);
                                SqlCommand cmd = new SqlCommand("SP_Retrieve_PICHead", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                                conn.Open();
                                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                                DataTable dt = new DataTable();
                                adapter.Fill(dt);
                                conn.Close();

                                if (dt.Rows.Count > 0)
                                    Remarks = "Assigned To PIC Head : " + dt.Rows[0]["FullName"].ToString();

                                //throw new InvalidPluginExecutionException("123");

                            }

                            tickethistory.Attributes["new_helpdeskstatus"] = new OptionSetValue(HelpdeskStatus);
                            tickethistory.Attributes["new_remarks"] = Remarks;

                            String FullName = user.Attributes["fullname"].ToString();

                            tickethistory.Attributes["new_updatedby"] = FullName;
                            tickethistory.Attributes["new_updateddate"] = DateTime.Now;
                            service.Create(tickethistory);

                            //throw new InvalidPluginExecutionException("6a");


                            CreateTicketHistoryToTMDDB(CaseNumber, HelpdeskStatus, Remarks, FullName, SavedBy, connectionstring);

                            if (helpcategory.Attributes["new_name"].ToString().ToUpper() == "COMPLAINT")
                            {
                                sendEmailToCustomer(adminservice, newcase, helpdeskstatus);
                                SendEmailToPICHead(adminservice, newcase, helpdeskstatus, connectionstring);
                            }
                            else
                                sendEmailToCustomer(adminservice, newcase, helpdeskstatus);
                        }
                    }
                }
                catch (Exception e)
                {
                    //throw new InvalidPluginExecutionException("6");

                    Save_PluginErrorLog(e, SavedBy, connectionstring);
                    //throw new InvalidPluginExecutionException("Error occured in Plugin : " + e.Message);
                }
            }
        }
    }
}
