﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;
using System.Data;

namespace crm.plugins
{
    public class postcreatelocation : IPlugin
    {
        void Save_Location_TMDDB(String _crmLocationID, String _locationName, String _savedBy, String connectionstring)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Location", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CRMLocationID", _crmLocationID);
            cmd.Parameters.AddWithValue("@LocationName", _locationName);
            cmd.Parameters.AddWithValue("@SavedBy", _savedBy);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.InitiatingUserId);

                QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                q_configuration.Attributes.AddRange(new string[] { "new_name" });
                q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);
                String connectionstring = "";

                if (configurations != null)
                {
                    if (configurations.Entities.Count > 0)
                    {
                        Entity configuration = configurations.Entities[0];
                        connectionstring = configuration.Attributes["new_value"].ToString();
                    }
                }

                Entity location = (Entity)context.InputParameters["Target"];

                Entity user = adminservice.Retrieve("systemuser", context.UserId, new ColumnSet(new String[] { "domainname" }));
                String SavedBy = user.Attributes["domainname"].ToString();

                Save_Location_TMDDB(location.Id.ToString(), location.Attributes["new_name"].ToString(), SavedBy, connectionstring);
            }
        }
    }
}
