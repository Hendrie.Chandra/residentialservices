﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Xrm.Sdk.Query;

namespace crm.plugins
{
	public class postcruddepartmentmapping : IPlugin
	{
        private void SaveMappingToDB(Guid _CRMDepartmentMappingID, Guid _CRMHelpID, Guid _CRMSubLocationID, Guid _CRMDepartmentID, String _SavedBy, String _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_DepartmentMapping", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CRMDepartmentMappingID", _CRMDepartmentMappingID);
            cmd.Parameters.AddWithValue("@CRMHelpID", _CRMHelpID);
            cmd.Parameters.AddWithValue("@CRMSubLocationID", _CRMSubLocationID);
            cmd.Parameters.AddWithValue("@CRMDepartmentID", _CRMDepartmentID);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);

                Entity entity = (Entity)context.InputParameters["Target"];

                QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                q_configuration.Attributes.AddRange(new string[] { "new_name" });
                q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);
                String connectionstring = "";

                if (configurations != null)
                {
                    if (configurations.Entities.Count > 0)
                    {
                        Entity configuration = configurations.Entities[0];
                        connectionstring = configuration.Attributes["new_value"].ToString();
                    }
                }

                Guid CRMDepartmentMappingID = Guid.Empty;

                if (context.MessageName.ToUpper() == "UPDATE")
                    CRMDepartmentMappingID = ((EntityReference)entity.Attributes["new_departmentmapping"]).Id;

                Guid HelpName = ((EntityReference)entity.Attributes["new_helpname"]).Id;
                Guid SubLocation = ((EntityReference)entity.Attributes["new_sublocation"]).Id;
                Guid Department = ((EntityReference)entity.Attributes["new_department"]).Id;

                Entity user = service.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname", "fullname" }));
                String SavedBy = user.Attributes["domainname"].ToString();

                SaveMappingToDB(CRMDepartmentMappingID, HelpName, SubLocation, Department, SavedBy, connectionstring);
            }
        }
	}
}
