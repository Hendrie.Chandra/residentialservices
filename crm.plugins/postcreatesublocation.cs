﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;
using System.Data;
using System.ServiceModel;

namespace crm.plugins
{
    public class postcreatesublocation : IPlugin
    {
        void Save_SubLocation_TMDDB(String _crmSubLocationID, String _locationName, String _crmLocationID, String _savedBy, String connectionstring)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_SubLocation", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CRMSubLocationID", _crmSubLocationID);
            cmd.Parameters.AddWithValue("@SubLocationName", _locationName);
            cmd.Parameters.AddWithValue("@CRMLocationID", _crmLocationID);
            cmd.Parameters.AddWithValue("@SavedBy", _savedBy);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.InitiatingUserId);

                QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                q_configuration.Attributes.AddRange(new string[] { "new_name" });
                q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);
                String connectionstring = "";

                if (configurations != null)
                {
                    if (configurations.Entities.Count > 0)
                    {
                        Entity configuration = configurations.Entities[0];
                        connectionstring = configuration.Attributes["new_value"].ToString();
                    }
                }

                try
                {
                    Entity sublocation = (Entity)context.InputParameters["Target"];

                    Entity user = adminservice.Retrieve("systemuser", context.UserId, new ColumnSet(new String[] { "domainname" }));
                    String SavedBy = user.Attributes["domainname"].ToString();
                    
                    Save_SubLocation_TMDDB(sublocation.Id.ToString(), sublocation.Attributes["new_name"].ToString(), ((EntityReference)sublocation.Attributes["new_location"]).Id.ToString(), SavedBy, connectionstring);
                }
                catch (Exception ex)
                {
                    throw new InvalidPluginExecutionException("An error occurred in the plug-in.", ex);
                }
            }
        }
    }
}
