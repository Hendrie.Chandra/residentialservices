﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Discovery;
using Microsoft.Crm.Sdk;
using Microsoft.Crm.Sdk.Messages;
using System.Net;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;

namespace crm.plugins
{
    public class mobilepushnotification : IPlugin
    {
        private static String connectionstring = "";
        private static String pushNotifApiUrl = "";

        public class PushNotificationRequest
        {
            public IList<int> UserID { get; set; }
            public string MsgPush { get; set; }
            public string CaseNumber { get; set; }
        }
        public class PushNotificationResponse
        {
            public string Code { get; set; }
        }

        public DataTable Get_UserPIC(int _orgID, string _CRMSiteID, String _CRMHelpID, String _connectionstring)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Mobile_Get_PICUser_ByDepertment", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _orgID);
            cmd.Parameters.AddWithValue("@CRMSiteID", _CRMSiteID);
            cmd.Parameters.AddWithValue("@CRMHelpID", _CRMHelpID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public void Save_PluginErrorLog(Exception _exc, String _initiatingUser, String _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
            cmd.Parameters.AddWithValue("@Message", _exc.Message);
            cmd.Parameters.AddWithValue("@Source", _exc.Source);
            cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
            if (_exc.InnerException != null)
            {
                cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
            }
            cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
            cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Save_MobileNotification(string _caseNumber, int _caseStatusID, int _userID, int _flagSent, string _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Mobile_Save_Notification", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _caseNumber);
            cmd.Parameters.AddWithValue("@CaseStatusID", _caseStatusID);
            cmd.Parameters.AddWithValue("@UserID", _userID);
            cmd.Parameters.AddWithValue("@FlagSent", _flagSent);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));
            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {

                IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);
                IOrganizationService systemservice = servicefactory.CreateOrganizationService(null);

                Entity newcase = (Entity)context.InputParameters["Target"];

                if (newcase.Attributes.Contains("new_helpcategory") && newcase.Attributes.Contains("new_helpname"))
                {
                    try
                    {
                        Entity EntHelpCategory = service.Retrieve("new_helpcategory", ((EntityReference)newcase.Attributes["new_helpcategory"]).Id, new ColumnSet(new String[] { "new_name" }));
                        String HelpCategory = EntHelpCategory.Attributes["new_name"].ToString();
                        int HelpdeskStatus = ((OptionSetValue)newcase.Attributes["new_helpdeskstatus"]).Value;

                        if (HelpCategory.ToUpper() == "COMPLAINT" || HelpCategory.ToUpper() == "INCIDENT")
                        {
                            // Retrieve TMDB Conn String
                            QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                            q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                            q_configuration.Attributes.AddRange(new string[] { "new_name" });
                            q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                            EntityCollection configurations = service.RetrieveMultiple(q_configuration);

                            if (configurations != null)
                            {
                                if (configurations.Entities.Count > 0)
                                {
                                    Entity configuration = configurations.Entities[0];
                                    connectionstring = configuration.Attributes["new_value"].ToString();
                                }
                            }

                            EntityReference _helpname_ref = (EntityReference)newcase.Attributes["new_helpname"];
                            string CRMHelpID = _helpname_ref.Id.ToString();


                            Entity Unit = service.Retrieve("new_unit", ((EntityReference)newcase.Attributes["new_unit"]).Id, new ColumnSet(new String[] { "new_name", "new_unitno", "owningbusinessunit" }));
                            String CRMSiteID = ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id.ToString();
                            String CaseNumber = newcase.Attributes["ticketnumber"].ToString();

                            DataTable dt = Get_UserPIC(1, CRMSiteID, CRMHelpID, connectionstring);
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow row in dt.Rows)
                                {
                                    try
                                    {
                                        int picUserID = (int)row["UserID"];
                                        Save_MobileNotification(CaseNumber, HelpdeskStatus, picUserID, 0, connectionstring);
                                    }
                                    catch (Exception ef)
                                    {
                                        Save_PluginErrorLog(ef, "mobileNotifUser", connectionstring);
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Save_PluginErrorLog(e, "mobileNotifUser", connectionstring);
                    }
                }//
            }
        }//end of function
    }
}
