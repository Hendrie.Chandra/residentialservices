﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.Data.SqlClient;
using System.Data;
using System.ServiceModel;

namespace crm.plugins
{
    public class precreatecase : IPlugin
    {
        private static String connectionstring = "";
        private static String SavedBy = "";

        private Boolean IsWeekend(DateTime _date)
        {
            return _date.DayOfWeek == DayOfWeek.Saturday || _date.DayOfWeek == DayOfWeek.Sunday;
        }

        private DateTime OverdueDate(int _Duration, IOrganizationService _service)
        {
            DateTime StartDate = DateTime.Now;
            DateTime CurrentDate = StartDate;
            DateTime EndDate = DateTime.Now.AddDays(_Duration);

            ConditionExpression condition1 = new ConditionExpression();
            condition1.AttributeName = "new_date";
            condition1.Operator = ConditionOperator.GreaterEqual;
            condition1.Values.Add(DateTime.Now.Date.ToUniversalTime());

            FilterExpression filter1 = new FilterExpression();
            filter1.Conditions.Add(condition1);

            QueryExpression query = new QueryExpression("new_offdays");
            query.ColumnSet = new ColumnSet(new String[] { "new_date", "new_remarks" });
            query.Criteria.AddFilter(filter1);
            EntityCollection OffDays = _service.RetrieveMultiple(query);

            List<DateTime> ListOffDays = new List<DateTime>();

            if (OffDays.Entities.Count > 0)
            {
                foreach (Entity ent in OffDays.Entities)
                {
                    ListOffDays.Add(DateTime.Parse(ent.Attributes["new_date"].ToString()).ToLocalTime());
                }
            }

            for (int i = 1; i <= _Duration; i++)
            {
                var IsOffDays = ListOffDays.FindAll(item => item.ToString("yyyy-MM-dd") == CurrentDate.ToString("yyyy-MM-dd"));
                if (IsWeekend(CurrentDate))
                {
                    EndDate = EndDate.AddDays(1);
                    _Duration++;
                }
                else if (IsOffDays.Count > 0)
                {
                    EndDate = EndDate.AddDays(1);
                    _Duration++;
                }
                CurrentDate = CurrentDate.AddDays(1);
            }

            return EndDate;            
        }

        private void Save_PluginErrorLog(FaultException<OrganizationServiceFault> _exc, String _initiatingUser, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
                cmd.Parameters.AddWithValue("@Message", _exc.Message);
                cmd.Parameters.AddWithValue("@Source", _exc.Source);
                cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
                cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
                cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
                cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (SqlException)
            {                
                throw new InvalidPluginExecutionException("Error occured in Plugin");
            }
        }

        private void Save_PluginErrorLogProc(FaultException<OrganizationServiceFault> _exc, String _initiatingUser, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
                cmd.Parameters.AddWithValue("@Message", _exc.Message);
                cmd.Parameters.AddWithValue("@Source", _exc.Source);
                cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
                cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
                cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
                cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                throw new InvalidPluginExecutionException("Error occured in Plugin");
            }
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));
            
            try
            {
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                    IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);

                    Entity newcase = (Entity)context.InputParameters["Target"];

                    String CaseNumber = newcase.Attributes["ticketnumber"].ToString();

                    Entity user = service.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname", "fullname" }));
                    SavedBy = user.Attributes["domainname"].ToString();

                    // retrieve Conn String configuration
                    QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                    q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                    q_configuration.Attributes.AddRange(new string[] { "new_name" });
                    q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                    EntityCollection configurations = service.RetrieveMultiple(q_configuration);

                    if (configurations != null)
                    {
                        if (configurations.Entities.Count > 0)
                        {
                            Entity configuration = configurations.Entities[0];
                            connectionstring = configuration.Attributes["new_value"].ToString();
                        }
                    }

                    Entity EntHelpCategory = service.Retrieve("new_helpcategory", ((EntityReference)newcase.Attributes["new_helpcategory"]).Id, new ColumnSet(new String[] { "new_name" }));
                    String HelpCategory = EntHelpCategory.Attributes["new_name"].ToString();

                    Entity EntHelpName = service.Retrieve("new_helpname", ((EntityReference)newcase.Attributes["new_helpname"]).Id, new ColumnSet(new String[] { "new_duration" }));
                    int Duration = int.Parse(EntHelpName.Attributes["new_duration"].ToString());
                    
                    switch (HelpCategory.ToUpper())
                    {
                        case "COMPLAINT":
                            //change status to Assigned To PIC
                            newcase.Attributes["new_helpdeskstatus"] = new OptionSetValue(2);
                            break;
                        
                        //add by bili march 2017
                        case "INCIDENT":
                            //change status to Assigned To PIC
                            newcase.Attributes["new_helpdeskstatus"] = new OptionSetValue(2);
                            break;

                        case "INFORMATION":
                            //change status to In Progress
                            newcase.Attributes["new_helpdeskstatus"] = new OptionSetValue(4);
                            break;
                    }
                    
                    //adding overdue date
                    DateTime overdueDate = OverdueDate(Duration, adminservice);                    
                    newcase.Attributes.Add("followupby", overdueDate);                                                           
                    
                    //add incident title                    
                    newcase.Attributes.Add("title", CaseNumber);
                    
                    //fill help group field                 
                    EntityReference HelpGroup = (EntityReference)service.Retrieve("new_helpname", ((EntityReference)newcase.Attributes["new_helpname"]).Id, new ColumnSet(new string[] { "new_parent" })).Attributes["new_parent"];
                    if (newcase.Attributes.Contains("new_helpgroup"))
                        newcase.Attributes["new_helpgroup"] = HelpGroup;
                    else
                        newcase.Attributes.Add("new_helpgroup", HelpGroup);
                }
            }
            catch (FaultException<OrganizationServiceFault> e)
            {                
                Save_PluginErrorLog(e, SavedBy, connectionstring);
                throw new InvalidPluginExecutionException("Error occured in Plugin");
            }
        }
    }
}
