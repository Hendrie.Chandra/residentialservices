﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Crm.Sdk.Messages;
using System.Text.RegularExpressions;

/*Update History
 * 1. Update By Bili    -11 juli 2017   -Tambah Status Baru CRM Customer can't be reachead   
 */

namespace crm.plugins
{
    public class postupdatecasetotickethistory : IPlugin
    {
        private static String connectionstring = "";
        private static String SavedBy = "";

        private Entity getrecord_byattribute(IOrganizationService _service, String _entityname, String _searchattr, String _searchvalue, String[] _columnset)
        {
            QueryByAttribute query = new QueryByAttribute(_entityname);
            query.ColumnSet = new ColumnSet(_columnset);
            query.Attributes.AddRange(new String[] { _searchattr });
            query.Values.AddRange(new Object[] { _searchvalue });
            EntityCollection entities = _service.RetrieveMultiple(query);
            Entity entity = null;
            if (entities != null)
                if (entities.Entities.Count > 0)
                    entity = entities.Entities[0];

            return entity;
        }

        private String getconfigurationvalue(IOrganizationService _service, String _configurationname)
        {
            String configurationvalue = "";
            Entity configuration = getrecord_byattribute(_service, "new_residentialconfiguration", "new_name", _configurationname, new String[] { "new_value" });
            if (configuration != null)
                if (configuration.Attributes.Contains("new_value"))
                    configurationvalue = configuration.Attributes["new_value"].ToString();

            return configurationvalue;
        }

        private EntityCollection getemailattachmentoncase(IOrganizationService _service, Guid _caseid)
        {
            EntityCollection emailattachments = new EntityCollection();

            QueryExpression query_attach = new QueryExpression()
            {
                EntityName = "annotation",
                ColumnSet = new ColumnSet(new String[] { "subject", "filename", "mimetype", "documentbody", "notetext" }),
                Criteria = new FilterExpression
                {
                    Conditions =
                            {
                                new ConditionExpression
                                {
                                    AttributeName = "objectid",
                                    Operator = ConditionOperator.Equal,
                                    Values = { _caseid }
                                },
                                new ConditionExpression
                                {
                                    AttributeName = "documentbody",
                                    Operator = ConditionOperator.NotNull
                                },
                                new ConditionExpression
                                {
                                    AttributeName = "objecttypecode",
                                    Operator = ConditionOperator.Equal,
                                    Values = { "incident" }                                
                                }
                            }
                }
            };

            emailattachments = _service.RetrieveMultiple(query_attach);

            return emailattachments;
        }

        private void SendEmail(IOrganizationService _service, Entity _incident, String _EmailTemplateName, EntityCollection _ToAddress)
        {
            try
            {
                Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                Entity BusinessUnit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(true));

                //Find Template
                QueryByAttribute templatequery = new QueryByAttribute("template");
                templatequery.ColumnSet = new ColumnSet(true);
                templatequery.Attributes.AddRange("title");
                templatequery.Values.AddRange(_EmailTemplateName);
                EntityCollection retrieved = _service.RetrieveMultiple(templatequery);

                if (retrieved != null && retrieved.Entities.Count > 0)
                {
                    //retrieving email template
                    InstantiateTemplateRequest instTemplateReq = new InstantiateTemplateRequest();
                    instTemplateReq.TemplateId = retrieved.Entities[0].Id;
                    instTemplateReq.ObjectId = _incident.Id;
                    instTemplateReq.ObjectType = _incident.LogicalName;

                    InstantiateTemplateResponse instTemplateResp = (InstantiateTemplateResponse)_service.Execute(instTemplateReq);

                    //Initiating FROM
                    EntityCollection From = new EntityCollection();
                    Entity QueueEmail = new Entity("activityparty");
                    QueueEmail.Attributes.Add("partyid", new EntityReference("queue", ((EntityReference)BusinessUnit.Attributes["new_emailqueue"]).Id));
                    From.Entities.Add(QueueEmail);

                    //create new Email Activity
                    Entity email = new Entity("email");
                    email = instTemplateResp.EntityCollection[0];

                    //Email Signature
                    String SignatureLogo = "";
                    String SignatureAddress = "";
                    string CallCenter = "";

                    QueryByAttribute SignatureTemplate = new QueryByAttribute("new_residentialconfiguration");
                    SignatureTemplate.ColumnSet = new ColumnSet(new string[] { "new_value", "new_value2" });
                    SignatureTemplate.Attributes.AddRange(new string[] { "new_name" });
                    SignatureTemplate.Values.AddRange(BusinessUnit.Attributes["name"].ToString() + " Signature");
                    EntityCollection SignatureTemplates = _service.RetrieveMultiple(SignatureTemplate);

                    if (SignatureTemplates != null && SignatureTemplates.Entities.Count > 0)
                    {
                        SignatureLogo = "<b>" + SignatureTemplates.Entities[0].Attributes["new_value"].ToString() + "</b>";
                        SignatureAddress = SignatureTemplates.Entities[0].Attributes["new_value2"].ToString();
                    }

                    //start add by bili
                    if (BusinessUnit.Contains("address1_telephone1"))
                        CallCenter = BusinessUnit.Attributes["address1_telephone1"].ToString();
                    //end add by bili

                    String Subject = email.Attributes["subject"].ToString();
                    String MessageContent = email.Attributes["description"].ToString();

                    MessageContent = MessageContent.Replace("{Call_Center}", CallCenter);// add by bili
                    MessageContent = MessageContent.Replace("{Signature_Logo}", SignatureLogo);
                    MessageContent = MessageContent.Replace("{Signature_Address}", SignatureAddress);
                    MessageContent = MessageContent.Replace("\n", "<br />");

                    int casestatus = ((OptionSetValue)_incident.Attributes["new_helpdeskstatus"]).Value;

                    // Start-- Adhitya Prabowo Update 20161227

                    if (casestatus == 6 || casestatus==11)
                    {

                        String SatisfactionURL = getconfigurationvalue(_service, "Satisfaction URL");

                        Entity SatisficationAccess = new Entity("new_satisfactionaccess");

                        SatisficationAccess.Attributes["new_name"] = SatisfactionURL;
                        SatisficationAccess.Attributes["new_case"] = new EntityReference("incident", _incident.Id);

                        SatisficationAccess.Attributes["new_isaccess"] = false;

                        Guid AccessChecking = _service.Create(SatisficationAccess);

                        String HTMLSatisfication = @"<b>
                        Silakan beri tanggapan untuk pelayanan yang diberikan : " +
                        "<a href=\"" + SatisfactionURL + "?checking=" + AccessChecking.ToString() + "&issatisfied=1&id=" + _incident.Id.ToString() + "\">Selesai</a> &nbsp;" +
                        "| &nbsp;<a href=\"" + SatisfactionURL + "?checking=" + AccessChecking.ToString() + "&issatisfied=0&id=" + _incident.Id.ToString() + "\">Tidak Selesai</a>" +
                        "</b>";

                        MessageContent = MessageContent.Replace("{Satisfaction_Link_bahasaindonesia}", HTMLSatisfication);


                        HTMLSatisfication = @"<b>
                        Please give your response for the services provided by us : " +
                        "<a href=\"" + SatisfactionURL + "?checking=" + AccessChecking.ToString() + "&issatisfied=1&id=" + _incident.Id.ToString() + "\">Finish</a> &nbsp;" +
                        "| &nbsp;<a href=\"" + SatisfactionURL + "?checking=" + AccessChecking.ToString() + "&issatisfied=0&id=" + _incident.Id.ToString() + "\">Not Finish</a>" +
                        "</b>";

                        MessageContent = MessageContent.Replace("{Satisfaction_Link_bahasainggris}", HTMLSatisfication);

                    }
                    else
                    {
                        MessageContent = MessageContent.Replace("{Satisfaction_Link_bahasainggris}", "");
                        MessageContent = MessageContent.Replace("{Satisfaction_Link_bahasaindonesia}", "");

                    }

                    // Adhitya Prabowo Update 20161227 --End

                    if (casestatus == 2)
                    {
                        //find pic head based on case
                        SqlConnection conn = new SqlConnection(connectionstring);
                        SqlCommand cmd = new SqlCommand("SP_Retrieve_PICHead", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CaseNumber", _incident.Attributes["ticketnumber"].ToString());
                        conn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                        DataTable dt = new DataTable();
                        adapter.Fill(dt);
                        conn.Close();

                        string rep = "";

                        foreach (DataRow dr in dt.Rows)
                            rep += dr["FullName"].ToString() + ", ";

                        MessageContent = MessageContent.Replace("{Representative}", rep);
                    }
                    else if (casestatus == 3 || casestatus == 8)
                    {
                        //find pic head based on case
                        SqlConnection conn = new SqlConnection(connectionstring);
                        SqlCommand cmd = new SqlCommand("SP_Retrieve_PICHead", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@CaseNumber", _incident.Attributes["ticketnumber"].ToString());
                        conn.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                        DataTable dt = new DataTable();
                        adapter.Fill(dt);
                        conn.Close();

                        string rep = "";

                        foreach (DataRow dr in dt.Rows)
                            rep += dr["FullName"].ToString() + ", ";

                        //find pic on case            
                        SqlCommand cmdpic = new SqlCommand("SP_Retrieve_PICCase", conn);
                        cmdpic.CommandType = CommandType.StoredProcedure;
                        cmdpic.Parameters.AddWithValue("@CaseNumber", _incident.Attributes["ticketnumber"].ToString());
                        conn.Open();
                        SqlDataAdapter adapterpic = new SqlDataAdapter(cmdpic);

                        DataTable dtpic = new DataTable();
                        adapterpic.Fill(dtpic);
                        conn.Close();

                        if (dtpic.Rows.Count > 0)
                            rep += dtpic.Rows[0]["FullName"].ToString();

                        MessageContent = MessageContent.Replace("{Representative}", rep);
                    }

                    email.Attributes["regardingobjectid"] = new EntityReference(_incident.LogicalName, _incident.Id);
                    email.Attributes["from"] = From;
                    email.Attributes["to"] = _ToAddress;
                    email.Attributes["subject"] = Subject;
                    email.Attributes["description"] = MessageContent;
                    Guid EmailID = _service.Create(email);

                    // Start-- Adhitya Prabowo Update Fixing 20161215
                    if (casestatus == 6)
                    {
                        EntityCollection emailattachments = getemailattachmentoncase(_service, _incident.Id);
                        foreach (Entity emailattachment in emailattachments.Entities)
                        {
                            Entity newemailattachment = new Entity("activitymimeattachment");
                            if (emailattachment.Attributes.Contains("subject"))
                                newemailattachment.Attributes["subject"] = emailattachment.Attributes["subject"];
                            if (emailattachment.Attributes.Contains("filename"))
                                newemailattachment.Attributes["filename"] = emailattachment.Attributes["filename"];
                            if (emailattachment.Attributes.Contains("mimetype"))
                                newemailattachment.Attributes["mimetype"] = emailattachment.Attributes["mimetype"];
                            if (emailattachment.Attributes.Contains("documentbody"))
                                newemailattachment.Attributes["body"] = emailattachment.Attributes["documentbody"];
                            newemailattachment.Attributes["objectid"] = new EntityReference("email", EmailID);
                            newemailattachment.Attributes["objecttypecode"] = "email";

                            Guid newemailattachmentid = _service.Create(newemailattachment);
                        }
                    }
                    // Adhitya Prabowo Update Fixing 20161215 --End


                    SendEmailRequest reqSendEmail = new SendEmailRequest();
                    reqSendEmail.EmailId = EmailID;
                    reqSendEmail.TrackingToken = "";
                    reqSendEmail.IssueSend = true;

                    SendEmailResponse res = (SendEmailResponse)_service.Execute(reqSendEmail);
                }
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void sendEmailToCustomer(IOrganizationService _service, Entity _incident, Int32 _helpdeskstatus)
        {
            try
            {
                QueryByAttribute FindCustomerEmail = new QueryByAttribute("new_email");
                FindCustomerEmail.ColumnSet = new ColumnSet(new String[] { "new_name" });
                FindCustomerEmail.Attributes.AddRange(new string[] { "new_contact" });
                FindCustomerEmail.Values.AddRange(((EntityReference)_incident.Attributes["customerid"]).Id);
                EntityCollection CustomerEmailAddress = _service.RetrieveMultiple(FindCustomerEmail);

                if (_incident.Attributes.Contains("new_email") || CustomerEmailAddress.Entities.Count > 0)
                {
                    Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                    Entity businessunit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue", "new_emailtemplatepending", "new_emailtemplatenopending", "new_emailtemplatecancelledcustomer", "new_emailtemplateprogresscompleted", "new_emailtemplatecaseclosed", "new_emailtemplaterejected", "new_emailtemplateonhold","new_emailtemplatenopendingemergency","new_emailtemplateprogresscompletedemergency","new_emailtemplatecannotbereached" }));// add by bili email emergency

                    String findTemplate = "";
                    //start add by bili
                    Entity EntHelpCategory = _service.Retrieve("new_helpcategory", ((EntityReference)_incident.Attributes["new_helpcategory"]).Id, new ColumnSet(new String[] { "new_name" }));
                    String HelpCategory = EntHelpCategory.Attributes["new_name"].ToString();
                    //end add by bili
                    switch (_helpdeskstatus)
                    {
                        case 1: //pending
                            findTemplate = businessunit.Attributes["new_emailtemplatepending"].ToString();
                            break;
                        case 2: //assigned to pic
                            if (HelpCategory.ToUpper() == "INCIDENT")
                                findTemplate = businessunit.Attributes["new_emailtemplatenopendingemergency"].ToString();
                            else
                                findTemplate = businessunit.Attributes["new_emailtemplatenopending"].ToString();
                            break;
                        case 3: //cancelled
                            findTemplate = businessunit.Attributes["new_emailtemplatecancelledcustomer"].ToString();
                            break;
                        case 6: //progress completed
                            if (HelpCategory.ToUpper() == "INCIDENT")
                                findTemplate = businessunit.Attributes["new_emailtemplateprogresscompletedemergency"].ToString();
                            else
                                findTemplate = businessunit.Attributes["new_emailtemplateprogresscompleted"].ToString();
                            break;
                        case 9: //case closed
                            findTemplate = businessunit.Attributes["new_emailtemplatecaseclosed"].ToString();
                            break;
                        case 10: //rejected
                            findTemplate = businessunit.Attributes["new_emailtemplaterejected"].ToString();
                            break;
                        case 11: //customer can't be reached
                            findTemplate = businessunit.Attributes["new_emailtemplatecannotbereached"].ToString();// add by bili 11 juli 2017
                            break;
                    }

                    EntityCollection To = new EntityCollection();

                    //get Owner email address from new_email
                    if (CustomerEmailAddress.Entities.Count > 0)
                    {
                        foreach (Entity emailToSend in CustomerEmailAddress.Entities)
                        {
                            String OwnerEmailAddress = emailToSend.Attributes["new_name"].ToString();
                            if (Regex.IsMatch(OwnerEmailAddress, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                            {
                                Entity OwnerEmail = new Entity("activityparty");
                                OwnerEmail.Attributes.Add("partyid", new EntityReference("contact", ((EntityReference)_incident.Attributes["customerid"]).Id));
                                OwnerEmail.Attributes.Add("addressused", OwnerEmailAddress);
                                To.Entities.Add(OwnerEmail);
                            }
                        }
                    }

                    //get Requestor email address
                    if (_incident.Contains("new_email") &&
                        Regex.IsMatch(_incident.Attributes["new_email"].ToString(), @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                    {
                        String RequestorEmailAddress = _incident.Attributes["new_email"].ToString();
                        Entity CustomerEmail = new Entity("activityparty");
                        CustomerEmail.Attributes.Add("addressused", RequestorEmailAddress);
                        To.Entities.Add(CustomerEmail);
                    }

                    if (To.Entities.Count > 0)
                        SendEmail(_service, _incident, findTemplate, To);
                }
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void SendEmailToPICHead(IOrganizationService _service, Entity _incident, Int32 _helpdeskstatus, String _connectionstring)
        {
            try
            {
                String CaseNumber = _incident.Attributes["ticketnumber"].ToString();

                //find pic head based on case
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Retrieve_PICHead", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();
                adapter.Fill(dt);
                conn.Close();

                if (dt.Rows.Count > 0)
                {
                    EntityCollection To = new EntityCollection();

                    foreach (DataRow dr in dt.Rows)
                    {
                        String PICHeadEmailAddress = dr["EmailAddress"].ToString();

                        if (!String.IsNullOrWhiteSpace(PICHeadEmailAddress) &&
                            Regex.IsMatch(PICHeadEmailAddress, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                        {
                            Entity activityPICEmailAddress = new Entity("activityparty");
                            activityPICEmailAddress.Attributes.Add("addressused", PICHeadEmailAddress);
                            To.Entities.Add(activityPICEmailAddress);
                        }
                    }

                    if (To.Entities.Count > 0)
                    {
                        Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                        Entity businessunit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue", "new_emailtemplatenopendingpic", "new_emailtemplatecancelledpic", "new_emailtemplateonhold", "new_emailtemplatenotsatisfied" }));

                        String findTemplate = "";
                        switch (_helpdeskstatus)
                        {
                            case 2: //Assigned To PIC
                                findTemplate = businessunit.Attributes["new_emailtemplatenopendingpic"].ToString();
                                break;
                            case 3: //cancelled
                                findTemplate = businessunit.Attributes["new_emailtemplatecancelledpic"].ToString();
                                break;
                            case 5: //on hold
                                findTemplate = businessunit.Attributes["new_emailtemplateonhold"].ToString();
                                break;
                            case 8: //not satisfied
                                findTemplate = businessunit.Attributes["new_emailtemplatenotsatisfied"].ToString();
                                break;
                        }

                        SendEmail(_service, _incident, findTemplate, To);
                    }
                }
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void SendEmailToPIC(IOrganizationService _service, Entity _incident, Int32 _helpdeskstatus, String _connectionstring)
        {
            try
            {
                //String WorkedByUserName = _postincident.Attributes["new_workedby"].ToString();
                String CaseNumber = _incident.Attributes["ticketnumber"].ToString();

                //find pic head
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd_pichead = new SqlCommand("SP_Retrieve_PICHead", conn);
                cmd_pichead.CommandType = CommandType.StoredProcedure;
                cmd_pichead.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                conn.Open();
                SqlDataAdapter adapter_pichead = new SqlDataAdapter(cmd_pichead);

                DataTable dt_pichead = new DataTable();
                adapter_pichead.Fill(dt_pichead);
                conn.Close();

                //find pic on case            
                SqlCommand cmd = new SqlCommand("SP_Retrieve_Case", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                conn.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);

                DataTable dt = new DataTable();
                adapter.Fill(dt);
                conn.Close();

                String WorkedBy = "";
                if (dt.Rows.Count > 0 || dt_pichead.Rows.Count > 0)
                {
                    Entity Unit = _service.Retrieve("new_unit", ((EntityReference)_incident.Attributes["new_unit"]).Id, new ColumnSet(new string[] { "owningbusinessunit" }));
                    Entity businessunit = _service.Retrieve("businessunit", ((EntityReference)Unit.Attributes["owningbusinessunit"]).Id, new ColumnSet(new String[] { "new_emailqueue", "new_emailtemplatecancelledpic", "new_emailtemplateonhold", "new_emailtemplatenotsatisfied" }));
                    String findTemplate = "";
                    switch (_helpdeskstatus)
                    {
                        case 2: //Assigned To PIC
                            findTemplate = businessunit.Attributes["new_emailtemplatenopendingpic"].ToString();
                            break;
                        case 3: //cancelled
                            findTemplate = businessunit.Attributes["new_emailtemplatecancelledpic"].ToString();
                            break;
                        case 5: //on hold
                            findTemplate = businessunit.Attributes["new_emailtemplateonhold"].ToString();
                            break;
                        case 8: //not satisfied
                            findTemplate = businessunit.Attributes["new_emailtemplatenotsatisfied"].ToString();
                            break;
                    }

                    EntityCollection To = new EntityCollection();

                    //insert pic head email address
                    if (dt_pichead.Rows.Count > 0 && !String.IsNullOrWhiteSpace(dt_pichead.Rows[0]["EmailAddress"].ToString()) &&
                        Regex.IsMatch(dt_pichead.Rows[0]["EmailAddress"].ToString(), @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                    {
                        Entity actvPicHead = new Entity("activityparty");
                        actvPicHead.Attributes.Add("addressused", dt_pichead.Rows[0]["EmailAddress"].ToString());
                        To.Entities.Add(actvPicHead);
                    }

                    //insert pic email address
                    WorkedBy = dt.Rows[0]["WorkedBy"].ToString();

                    SqlCommand cmd2 = new SqlCommand("SP_Retrieve_User", conn);
                    cmd2.CommandType = CommandType.StoredProcedure;
                    cmd2.Parameters.AddWithValue("@UserName", WorkedBy);
                    conn.Open();
                    SqlDataAdapter adapter2 = new SqlDataAdapter(cmd2);

                    DataTable dt2 = new DataTable();
                    adapter2.Fill(dt2);
                    conn.Close();

                    String PICEmailAddress = dt2.Rows[0]["EmailAddress"].ToString();

                    //check PIC's email
                    if (!String.IsNullOrWhiteSpace(PICEmailAddress) &&
                        Regex.IsMatch(PICEmailAddress, @"\A(?:[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?)\Z"))
                    {
                        Entity activityPICEmailAddress = new Entity("activityparty");
                        activityPICEmailAddress.Attributes.Add("addressused", PICEmailAddress);
                        To.Entities.Add(activityPICEmailAddress);
                    }

                    if (To.Entities.Count > 0)
                        SendEmail(_service, _incident, findTemplate, To);
                }
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void SaveToTMDDB(Int32 _OrgID, String _CRMSiteID, String _CRMCaseID, String _CaseNumber, Int32 _CaseStatusID, String _CRMHelpID,
            String _CRMLocationID, String _CRMSubLocationID, Int32 _CaseOriginID, String _PSCode, String _UnitCode, String _UnitNo,
            String _NamaPelapor, String _TeleponPelapor, String _EmailPelapor, String _Description, String _SavedBy, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_Case", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrgID", _OrgID);
                cmd.Parameters.AddWithValue("@CRMSiteID", _CRMSiteID);
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
                cmd.Parameters.AddWithValue("@CRMHelpID", _CRMHelpID);
                cmd.Parameters.AddWithValue("@CRMLocationID", _CRMLocationID);
                cmd.Parameters.AddWithValue("@CRMSubLocationID", _CRMSubLocationID);
                cmd.Parameters.AddWithValue("@CaseOriginID", _CaseOriginID);
                cmd.Parameters.AddWithValue("@PSCode", _PSCode);
                cmd.Parameters.AddWithValue("@UnitCode", _UnitCode);
                cmd.Parameters.AddWithValue("@UnitNo", _UnitNo);
                cmd.Parameters.AddWithValue("@NamaPelapor", _NamaPelapor);
                cmd.Parameters.AddWithValue("@TeleponPelapor", _TeleponPelapor);
                cmd.Parameters.AddWithValue("@EmailPelapor", _EmailPelapor);
                cmd.Parameters.AddWithValue("@Description", _Description);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void CreateTicketHistory(Guid _caseid, Int32 _status, String _remarks, String _updatedby, IOrganizationService _service)
        {
            try
            {
                Entity tickethistory = new Entity("new_tickethistory");
                tickethistory.Attributes["new_caseid"] = new EntityReference("incident", _caseid);
                tickethistory.Attributes["new_helpdeskstatus"] = new OptionSetValue(_status);
                tickethistory.Attributes["new_remarks"] = _remarks;
                tickethistory.Attributes["new_updatedby"] = _updatedby;
                tickethistory.Attributes["new_updateddate"] = DateTime.Now;
                _service.Create(tickethistory);
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void CreateTicketHistoryToTMDDB(String _casenumber, Int32 _status, String _remarks, String _updatedby, String _savedby, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_TicketHistory", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);
                cmd.Parameters.AddWithValue("@CaseStatusID", _status);
                cmd.Parameters.AddWithValue("@FullName", _updatedby);
                cmd.Parameters.AddWithValue("@Remarks", _remarks);
                cmd.Parameters.AddWithValue("@SavedBy", _savedby);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void CreateFeedbackLogToTMDDB(String _PostIncidentID, String _PostIncidentTicketNumber, Int32 _OptHelpdeskStatusValue, String _Remarks, String _FullName, String _SavedBy, String _userLogin, String _systemUserDomain, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_SaveToTestFeedbackLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PostIncidentID", _PostIncidentID);
                cmd.Parameters.AddWithValue("@PostIncidentTicketNumber", _PostIncidentTicketNumber);
                cmd.Parameters.AddWithValue("@OptHelpdeskStatusValue", _OptHelpdeskStatusValue);
                cmd.Parameters.AddWithValue("@Remarks", _Remarks);
                cmd.Parameters.AddWithValue("@FullName", _FullName);

                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
                cmd.Parameters.AddWithValue("@userLogin", _userLogin);
                cmd.Parameters.AddWithValue("@systemUserDomain", _systemUserDomain);
                cmd.Parameters.AddWithValue("@connectionString", _connectionstring);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }


        private void Save_Satisfaction(String _CaseNumber, Int32 _SatisfactionID, String _ConnString)
        {
            SqlConnection conn = new SqlConnection(_ConnString);
            SqlCommand cmd = new SqlCommand("SP_Save_Satisfaction", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@SatisfactionID", _SatisfactionID);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        private Boolean IsWeekend(DateTime _date)
        {
            return _date.DayOfWeek == DayOfWeek.Saturday || _date.DayOfWeek == DayOfWeek.Sunday;
        }

        private void UpdateHelpname(Guid _CRMCaseID, Guid _CRMHelpID, String _SavedBy, String _ConnString)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_ConnString);
                SqlCommand cmd = new SqlCommand("SP_Update_CaseDetail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CRMCaseID", _CRMCaseID);
                cmd.Parameters.AddWithValue("@CRMHelpID", _CRMHelpID);
                cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (InvalidPluginExecutionException ex)
            {
                Save_PluginErrorLog(ex, SavedBy, connectionstring);
            }
        }

        private void Save_PluginErrorLog(InvalidPluginExecutionException _exc, String _initiatingUser, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
                cmd.Parameters.AddWithValue("@Message", _exc.Message);
                cmd.Parameters.AddWithValue("@Source", _exc.Source);
                cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
                cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
                cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
                cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                throw new InvalidPluginExecutionException("Error occured in Plugin");
            }
        }

        //add by bili 7 maret 2017
        private void Save_PluginErrorLog(Exception _exc, String _initiatingUser, String _connectionstring)
        {
            try
            {
                SqlConnection conn = new SqlConnection(_connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
                cmd.Parameters.AddWithValue("@Message", _exc.Message);
                cmd.Parameters.AddWithValue("@Source", _exc.Source);
                cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
                if (_exc.InnerException != null)
                {
                    cmd.Parameters.AddWithValue("@InnerExceptionMessage", _exc.InnerException.Message);
                    cmd.Parameters.AddWithValue("@InnerExceptionStackTrace", _exc.InnerException.StackTrace);
                }
                cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
                cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e)
            {
                throw new InvalidPluginExecutionException("Error occured in Plugin : Saving Error Log");
            }
        }
        // end add by bili

        public void Execute(IServiceProvider _serviceprovider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
            {
                try
                {
                    IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                    IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);

                    QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                    q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                    q_configuration.Attributes.AddRange(new string[] { "new_name" });
                    q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                    EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);

                    if (configurations != null)
                    {
                        if (configurations.Entities.Count > 0)
                        {
                            Entity configuration = configurations.Entities[0];
                            connectionstring = configuration.Attributes["new_value"].ToString();
                        }
                    }

                    Entity postincident = (Entity)context.PostEntityImages["IncidentPostImage"];
                    Entity preincident = (Entity)context.PreEntityImages["IncidentPreImage"];

                    Entity user = service.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname", "fullname" }));
                    String FullName = user.Attributes["fullname"].ToString();
                    SavedBy = user.Attributes["domainname"].ToString();
                    String CaseNumber = postincident.Attributes["ticketnumber"].ToString();

                    OptionSetValue Opthelpdeskstatus = (OptionSetValue)postincident.Attributes["new_helpdeskstatus"];

                    //Send email if case updated from different status
                    if (((OptionSetValue)preincident.Attributes["new_helpdeskstatus"]).Value != Opthelpdeskstatus.Value)
                    {
                        Int32 helpdeskstatus = Opthelpdeskstatus.Value;
                        switch (helpdeskstatus)
                        {
                            //case 1: //Pending
                            //    sendEmailToCustomer(adminservice, postincident, preincident, helpdeskstatus);
                            //    break;
                            case 2: //Assigned To PIC - No Pending
                                SendEmailToPICHead(adminservice, postincident, helpdeskstatus, connectionstring);
                                sendEmailToCustomer(adminservice, postincident, helpdeskstatus);
                                break;
                            case 3: //Cancelled
                                sendEmailToCustomer(adminservice, postincident, helpdeskstatus);
                                SendEmailToPIC(adminservice, postincident, helpdeskstatus, connectionstring);
                                break;
                            case 5: //On Hold
                            case 6: //Progress Complete
                            case 9: //Case Closed
                            case 10: //Rejected
                            case 11: //customer connot be reached   {add by bili 11 juli 2017}
                                sendEmailToCustomer(adminservice, postincident, helpdeskstatus);
                                break;
                            case 8: //Not Satisfied
                                SendEmailToPIC(adminservice, postincident, helpdeskstatus, connectionstring);
                                break;
                        }
                    }

                    //Update case detail in Residential DB if any changed (now : helpname only)                
                    UpdateHelpname(postincident.Id, ((EntityReference)postincident.Attributes["new_helpname"]).Id, SavedBy, connectionstring);

                    //Create Ticket history
                    Entity systemuser = adminservice.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname" }));

                    QueryByAttribute qba_crmresidential = new QueryByAttribute("new_residentialconfiguration");
                    qba_crmresidential.ColumnSet = new ColumnSet(true);
                    qba_crmresidential.Attributes.AddRange("new_name");
                    qba_crmresidential.Values.AddRange("CRM Residential Service Admin");
                    EntityCollection CRMResidentialUser = service.RetrieveMultiple(qba_crmresidential);
                    String userlogin = CRMResidentialUser[0].Attributes["new_value"].ToString();

                    //Prevent creating Ticket history fromResidential Service login account
                    if (systemuser.Attributes["domainname"].ToString().ToUpper() != userlogin.ToUpper())
                    {
                        String Remarks = "";

                        if (postincident.Attributes.Contains("new_remarks") && !String.IsNullOrEmpty(postincident.Attributes["new_remarks"].ToString()))
                            Remarks = postincident.Attributes["new_remarks"].ToString();

                        //set remarks if helpdesk status changed from Pending to Assigned To PIC
                        if (Opthelpdeskstatus.Value == 2)
                        {
                            SqlConnection conn = new SqlConnection(connectionstring);
                            SqlCommand cmd = new SqlCommand("SP_Retrieve_PICHead", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@CaseNumber", CaseNumber);
                            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                            DataTable dt = new DataTable();
                            adapter.Fill(dt);

                            if (((OptionSetValue)preincident.Attributes["new_helpdeskstatus"]).Value == 1)
                                Remarks = "Assigned To PIC Head : " + ((dt.Rows.Count > 0) ? dt.Rows[0]["FullName"].ToString() : "");
                        }

                        CreateTicketHistory(postincident.Id, Opthelpdeskstatus.Value, Remarks, FullName, service);

                        CreateTicketHistoryToTMDDB(postincident.Attributes["ticketnumber"].ToString(), Opthelpdeskstatus.Value, Remarks, FullName, SavedBy, connectionstring);
                        //Satisfaction
                        //if (helpdeskstatus == 7)
                        //{
                        //    Int32 SatisfactionID = ((OptionSetValue)postincident.Attributes["customersatisfactioncode"]).Value;
                        //    Save_Satisfaction(CaseNumber, SatisfactionID, connectionstring);
                        //}
                    }
                }
                catch (Exception e)//InvalidPluginExecutionException
                {
                    Save_PluginErrorLog(e, SavedBy, connectionstring);
                    throw new InvalidPluginExecutionException("Error occured in Plugin. " +e.Message);
                }
            }
        }
    }
}
