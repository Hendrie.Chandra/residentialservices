﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel.Description;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Client;
using System.IO;

namespace residential.batchAutoNotification
{
    class Program
    {
        class MailAddress
        {
            public String DisplayName { get; set; }
            public String EmailAddress { get; set; }
        }

        static void Main(string[] args)
        {
            String PathResultLog = ConfigurationManager.AppSettings["PathResultLog"].ToString();
            if (!Directory.Exists(PathResultLog))
                Directory.CreateDirectory(PathResultLog);

            String LogData = "";

            try
            {
                //email berjenjang khusus cikarang, dijalankan setiap 5 menit
                DataTable cases = Retrieve_Emergency_InProgress(1, 5);

                if (cases.Rows.Count > 0)
                {
                    String Log = "";
                    foreach (DataRow dr in cases.Rows)
                    {
                        String LogNotification;

                        Int32 OrgID = (Int32)dr["OrgID"];
                        Int32 SiteID = (Int32)dr["SiteID"];
                        String CaseNumber = dr["CaseNumber"].ToString();
                        Int32 CaseAging = (Int32)dr["CaseAging"];

                        //kirim email disini
                        //batch dijalankan setiap 5 menit
                        //template email >> Case Emergency Notification
                        //Send_Email(OrgID, SiteID, CaseNumber, CRMCaseID, CaseAging, out LogNotification);

                        String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                        DBclass dbclass = new DBclass(constring);
                        DataTable dt = dbclass.retrieve_case_detail(CaseNumber);

                        string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                        string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();
                        string emailFrom = dt.Rows[0]["EmailFrom"].ToString();
                        string displayEmailFrom = dt.Rows[0]["DisplayEmailFrom"].ToString();
                        int helpdeskStatus = (int)dt.Rows[0]["CaseStatusID"];

                        EmailClass.sendEmailEmergencyNotif(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus, CaseAging, out LogNotification);


                        Log = "Case number : " + CaseNumber + " | Details : " + LogNotification + " | Time : " + DateTime.Now.ToString();
                        LogData += Log + "\r\n";
                        Console.WriteLine(Log);
                    }
                }
                else
                {
                    LogData += "No Case Overdue \r\n";
                    Console.WriteLine("No Case Overdue");
                }
                LogData += "Batch Auto Notification has been Completed | " + DateTime.Now.ToString() + "\r\n";
                LogData += CleanFileTemp(PathResultLog);
                WriteLog(LogData, PathResultLog + "logBatchAutoNotification - " + DateTime.Now.ToString("dd-MM-yyyy") + ".txt");

                Console.WriteLine("Done");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                LogData = ex.Message;
                WriteLog(LogData, PathResultLog + "logBatchAutoNotification - " + DateTime.Now.ToString("dd-MM-yyyy") + ".txt");
            }
            //Console.Write("Press any key to exit...");
            //Console.ReadKey();
        }

        static private void WriteLog(string linesLog, String PathResultLog)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(PathResultLog);
            file.WriteLine(linesLog);
            file.Close();
        }

        static string CleanFileTemp(string path)
        {
            String log = "";
            DateTime minus7 = DateTime.Now.AddDays(-7);
            string file = path + "logBatchAutoNotification - " + minus7.ToString("dd-MM-yyyy") + ".txt";

            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
                log += file + " will be deleted";
            }
            else
            {
                log += file + " not found";
            }

            return log;
        }

        static DataTable Retrieve_Emergency_InProgress(Int32 _OrgID, Int32 _SiteID)
        {
            string DBConnectionString = ConfigurationManager.ConnectionStrings["residentialdev"].ConnectionString;
            SqlConnection conn = new SqlConnection(DBConnectionString);
            SqlCommand cmd = new SqlCommand("SP_Get_Case_Emergency_InProgress", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        static DataTable Retrieve_Notification_User(Int32 _OrgID, Int32 _SiteID, Int32 _CaseAging)
        {
            string DBConnectionString = ConfigurationManager.ConnectionStrings["residentialdev"].ConnectionString;
            SqlConnection conn = new SqlConnection(DBConnectionString);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Notification_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CaseAging", _CaseAging);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
    }
}
