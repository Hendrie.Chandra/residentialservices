﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace residential.batchAutoNotification
{
    public class EmailClass
    {
        private static emailHtml createEmailBodyEmergencyNotif(string emailTemplate, DataTable dt, Int32 CaseAging)
        {
            String DBConnectionString = ConfigurationManager.ConnectionStrings["residentialdev"].ConnectionString;
            SqlConnection conn = new SqlConnection(DBConnectionString);

            emailHtml response = new emailHtml();
            string pathTemplate = ConfigurationManager.AppSettings["emailTemplateLocation"].ToString() + emailTemplate;
            string body = string.Empty;
            
            using (StreamReader reader = new StreamReader(pathTemplate))
            {
                body = reader.ReadToEnd();
            }

            //get pic head by case number
            List<emailTo> listMailTo = new List<emailTo>();
            DBclass dbclass = new DBclass(DBConnectionString);
           
            string picname = "";
            DataTable notificationusers = Retrieve_Notification_User(1, (int)dt.Rows[0]["SiteID"], CaseAging);
            if (notificationusers.Rows.Count > 0)
            {
                foreach (DataRow dr_user in notificationusers.Rows)
                {
                    emailTo to = new emailTo();
                    to.emailAddress = dr_user["EmailAddress"].ToString();
                    picname = picname + dr_user["Fullname"].ToString() + " ,";
                    listMailTo.Add(to);
                }
            }     


            body = body.Replace("{{representative}}", picname);

            body = body.Replace("{{unitcode}}", dt.Rows[0]["UnitCode"].ToString());
            body = body.Replace("{{unitnumber}}", dt.Rows[0]["UnitNo"].ToString());
            body = body.Replace("{{helpcategory}}", dt.Rows[0]["HelpCategoryName"].ToString());
            body = body.Replace("{{helpgroup}}", dt.Rows[0]["HelpGroup"].ToString());
            body = body.Replace("{{helpname}}", dt.Rows[0]["HelpName"].ToString());
            body = body.Replace("{{location}}", dt.Rows[0]["LocationName"].ToString());
            body = body.Replace("{{sublocation}}", dt.Rows[0]["SublocationName"].ToString());
            body = body.Replace("{{casenumber}}", dt.Rows[0]["CaseNumber"].ToString());
            body = body.Replace("{{descrtiption}}", dt.Rows[0]["Description"].ToString());          //pic
            body = body.Replace("{{duration}}", dt.Rows[0]["Duration"].ToString());              //pic
            body = body.Replace("{{callcenter}}", dt.Rows[0]["CallCenter"].ToString());
            body = body.Replace("{{signaturelogo}}", dt.Rows[0]["SignatureName"].ToString());
            body = body.Replace("{{signatureaddress}}", dt.Rows[0]["SignatureAddress"].ToString());

            var resultEmail = listMailTo.Where(p => p.emailAddress != "").GroupBy(p => p.emailAddress).Select(r => r.FirstOrDefault());
            response.emailTo = resultEmail.ToList();
            response.emailTilte = "Emergency Notification - " + dt.Rows[0]["CaseNumber"].ToString();
            response.emailBody = body;

            return response;
        }

        public static void sendEmailEmergencyNotif(string emailServiceUrl, string emailCategory, DataTable dt, string emailFrom, string displayEmail, int helpdeskStatus, Int32 caseAging, out string _Log)
        {
            try
            {                
                lkservice.LKEmailService emailService = new lkservice.LKEmailService();
                emailService.Url = emailServiceUrl;
                string mailCategory = emailCategory;
                string emailID = emailService.getEmailIDForMultipleAttachment(mailCategory);

                int helpCategoryID = (int)dt.Rows[0]["HelpCategoryID"];
                int siteID = (int)dt.Rows[0]["SiteID"];
                string emailTemplate = "Case Emergency Notification.html";


                emailHtml emailHtmlResponse = new emailHtml();
                emailHtmlResponse = createEmailBodyEmergencyNotif(emailTemplate, dt,caseAging);

                List<emailTo> listEmailTo = emailHtmlResponse.emailTo;
                if (listEmailTo.Count > 0)
                {
                    string listStringEmailto = "";
                    foreach (emailTo e in listEmailTo)
                    {
                        listStringEmailto = listStringEmailto + e.emailAddress + ";";
                    }
                    string to = listStringEmailto.Substring(0, (listStringEmailto.Length - 1));

                    emailService.sendMailAttachment_Inserted(to, string.Empty, string.Empty, emailHtmlResponse.emailTilte,
                        emailHtmlResponse.emailBody, string.Empty, emailCategory, emailFrom, displayEmail, emailID);

                    //save log
                    String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                    DBclass dbclass = new DBclass(constring);
                    dbclass.save_email_request_log(dt.Rows[0]["CaseNumber"].ToString(), helpdeskStatus, to, emailHtmlResponse.emailTilte + " " + emailHtmlResponse.emailBody);
                    _Log = "Email sent successfully";
                }else
                    _Log = "Email not Found";

            }catch(Exception e){
                _Log = "Error send email " + e.Message;
            }                    
        }

        //batch
        static private void Retrieve_PICHead(String _CaseNumber, out String _PICHeadEmailAddress, out String _PICHeadName, SqlConnection _Connection)
        {
            SqlCommand cmd_pichead = new SqlCommand("SP_Retrieve_PICHead", _Connection);
            cmd_pichead.CommandType = CommandType.StoredProcedure;
            cmd_pichead.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            _Connection.Open();
            SqlDataAdapter adp_pichead = new SqlDataAdapter(cmd_pichead);

            DataTable dt_pichead = new DataTable();
            adp_pichead.Fill(dt_pichead);
            _Connection.Close();

            _PICHeadEmailAddress = "";
            _PICHeadName = "";

            if (dt_pichead.Rows.Count > 0)
            {
                _PICHeadEmailAddress = dt_pichead.Rows[0]["EmailAddress"].ToString();
                _PICHeadName = dt_pichead.Rows[0]["FullName"].ToString();
            }
        }

        static private void Retrieve_PIC(String _CaseNumber, out String _PICEmailAddress, out String _PICName, SqlConnection _Connection)
        {
            SqlCommand cmd_case = new SqlCommand("SP_Retrieve_Case", _Connection);
            cmd_case.CommandType = CommandType.StoredProcedure;
            cmd_case.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            _Connection.Open();
            SqlDataAdapter adp_case = new SqlDataAdapter(cmd_case);

            DataTable dt_case = new DataTable();
            adp_case.Fill(dt_case);
            _Connection.Close();

            _PICEmailAddress = "";
            _PICName = "";

            if (dt_case.Rows.Count > 0)
            {
                String PICUserName = dt_case.Rows[0]["WorkedBy"].ToString();

                SqlCommand cmd_pic = new SqlCommand("SP_Retrieve_User", _Connection);
                cmd_pic.CommandType = CommandType.StoredProcedure;
                cmd_pic.Parameters.AddWithValue("@UserName", PICUserName);
                _Connection.Open();
                SqlDataAdapter adp_pic = new SqlDataAdapter(cmd_pic);

                DataTable dt_pic = new DataTable();
                adp_pic.Fill(dt_pic);
                _Connection.Close();

                if (dt_pic.Rows.Count > 0)
                {
                    _PICEmailAddress = dt_pic.Rows[0]["EmailAddress"].ToString();
                    _PICName = dt_pic.Rows[0]["FullName"].ToString();
                }
            }
        }

        static DataTable Retrieve_Notification_User(Int32 _OrgID, Int32 _SiteID, Int32 _CaseAging)
        {
            string DBConnectionString = ConfigurationManager.ConnectionStrings["residentialdev"].ConnectionString;
            SqlConnection conn = new SqlConnection(DBConnectionString);
            SqlCommand cmd = new SqlCommand("SP_Retrieve_Notification_User", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@OrgID", _OrgID);
            cmd.Parameters.AddWithValue("@SiteID", _SiteID);
            cmd.Parameters.AddWithValue("@CaseAging", _CaseAging);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

    }

    public class emailHtml
    {
        public List<emailTo> emailTo { get; set; }
        public string emailBody { get; set; }
        public string emailTilte { get; set; }
    }

    public class emailTo
    {
        public string emailAddress { get; set; }
    }

}