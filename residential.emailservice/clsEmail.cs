﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace residential.emailservice
{
    public class clsEmail
    {
        private static emailHtml createEmailBodyToCustomer(string emailTemplate, DataTable dt, string satisfactionAccessID)
        {
            emailHtml response = new emailHtml();

            string pathTemplate = HttpContext.Current.Server.MapPath("~/EmailTemplate/" + emailTemplate);
            string body = string.Empty;

            using (StreamReader reader = new StreamReader(pathTemplate))
            {
                body = reader.ReadToEnd();
            }

            body = body.Replace("{{customername}}", dt.Rows[0]["NamaPelapor"].ToString());
            body = body.Replace("{{unitcode}}", dt.Rows[0]["UnitCode"].ToString());
            body = body.Replace("{{unitnumber}}", dt.Rows[0]["UnitNo"].ToString());
            body = body.Replace("{{helpcategory}}", dt.Rows[0]["HelpCategoryName"].ToString());
            body = body.Replace("{{helpgroup}}", dt.Rows[0]["HelpGroup"].ToString());
            body = body.Replace("{{helpname}}", dt.Rows[0]["HelpName"].ToString());
            body = body.Replace("{{location}}", dt.Rows[0]["LocationName"].ToString());
            body = body.Replace("{{sublocation}}", dt.Rows[0]["SublocationName"].ToString());
            body = body.Replace("{{casenumber}}", dt.Rows[0]["CaseNumber"].ToString());
            body = body.Replace("{{descrtiption}}", dt.Rows[0]["Description"].ToString());          //pic
            body = body.Replace("{{duration}}", dt.Rows[0]["Duration"].ToString());              //pic
            body = body.Replace("{{callcenter}}", dt.Rows[0]["CallCenter"].ToString());
            body = body.Replace("{{signaturelogo}}", dt.Rows[0]["SignatureName"].ToString());
            body = body.Replace("{{signatureaddress}}", dt.Rows[0]["SignatureAddress"].ToString());

            string SatisfactionURL = ConfigurationManager.AppSettings["SatisfactionUrl"].ToString();
            String HTMLSatisficationIndonesia = @"<b>Silakan beri tanggapan untuk pelayanan yang diberikan : " +
            "<a href=\"" + SatisfactionURL + "?checking=" + dt.Rows[0]["CaseNumber"].ToString() + "&issatisfied=1&id=" + satisfactionAccessID + "\">Selesai</a> &nbsp;" +
            "| &nbsp;<a href=\"" + SatisfactionURL + "?checking=" + dt.Rows[0]["CaseNumber"].ToString() + "&issatisfied=0&id=" + satisfactionAccessID + "\">Tidak Selesai</a>" +
            "</b>";

            string HTMLSatisficationInggris = @"<b>Please give your response for the services provided by us : " +
            "<a href=\"" + SatisfactionURL + "?checking=" + dt.Rows[0]["CaseNumber"].ToString() + "&issatisfied=1&id=" + satisfactionAccessID + "\">Finish</a> &nbsp;" +
            "| &nbsp;<a href=\"" + SatisfactionURL + "?checking=" + dt.Rows[0]["CaseNumber"].ToString() + "&issatisfied=0&id=" + satisfactionAccessID + "\">Not Finish</a>" +
            "</b>";

            body = body.Replace("{{satisfaction_link_indonesia}}", HTMLSatisficationIndonesia);
            body = body.Replace("{{satisfaction_link_inggris}}", HTMLSatisficationInggris);

            //return body;
            emailTo to = new emailTo();
            to.emailAddress = dt.Rows[0]["EmailPelapor"].ToString();

            List<emailTo> listMailTo = new List<emailTo>();
            listMailTo.Add(to);

            response.emailTo = listMailTo;
            response.emailTilte = "Case status for " + dt.Rows[0]["CaseNumber"].ToString() + " is " + dt.Rows[0]["CaseStatusName"].ToString();
            response.emailBody = body;

            return response;
        }

        private static emailHtml createEmailBodyToPICHead(string emailTemplate, DataTable dt)
        {
            emailHtml response = new emailHtml();

            string pathTemplate = HttpContext.Current.Server.MapPath("~/EmailTemplate/" + emailTemplate);
            string body = string.Empty;

            using (StreamReader reader = new StreamReader(pathTemplate))
            {
                body = reader.ReadToEnd();
            }

            //get pic head by case number
            List<emailTo> listMailTo = new List<emailTo>();
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dtPic = dbclass.retrieve_pichead_bycase(dt.Rows[0]["CaseNumber"].ToString());     /*SPPICHeadByCaseNumber_GET*/
            string picname = "";
            if (dtPic.Rows.Count > 0)
            {
                foreach (DataRow dr in dtPic.Rows)
                {
                    emailTo to = new emailTo();
                    to.emailAddress = dr["EmailAddress"].ToString();
                    picname = picname + dr["FullName"].ToString() + " ,";
                    listMailTo.Add(to);
                }
            }
            body = body.Replace("{{representative}}", picname);

            body = body.Replace("{{unitcode}}", dt.Rows[0]["UnitCode"].ToString());
            body = body.Replace("{{unitnumber}}", dt.Rows[0]["UnitNo"].ToString());
            body = body.Replace("{{helpcategory}}", dt.Rows[0]["HelpCategoryName"].ToString());
            body = body.Replace("{{helpgroup}}", dt.Rows[0]["HelpGroup"].ToString());
            body = body.Replace("{{helpname}}", dt.Rows[0]["HelpName"].ToString());
            body = body.Replace("{{location}}", dt.Rows[0]["LocationName"].ToString());
            body = body.Replace("{{sublocation}}", dt.Rows[0]["SublocationName"].ToString());
            body = body.Replace("{{casenumber}}", dt.Rows[0]["CaseNumber"].ToString());
            body = body.Replace("{{descrtiption}}", dt.Rows[0]["Description"].ToString());          //pic
            body = body.Replace("{{duration}}", dt.Rows[0]["Duration"].ToString());              //pic
            body = body.Replace("{{callcenter}}", dt.Rows[0]["CallCenter"].ToString());
            body = body.Replace("{{signaturelogo}}", dt.Rows[0]["SignatureName"].ToString());
            body = body.Replace("{{signatureaddress}}", dt.Rows[0]["SignatureAddress"].ToString());

            //return body;
            response.emailTo = listMailTo;
            response.emailTilte = "Case status for " + dt.Rows[0]["CaseNumber"].ToString() + " is " + dt.Rows[0]["CaseStatusName"].ToString();
            response.emailBody = body;

            return response;
        }

        private static emailHtml createEmailBodyToPIC(string emailTemplate, DataTable dt)
        {
            emailHtml response = new emailHtml();

            string pathTemplate = HttpContext.Current.Server.MapPath("~/EmailTemplate/" + emailTemplate);
            string body = string.Empty;

            using (StreamReader reader = new StreamReader(pathTemplate))
            {
                body = reader.ReadToEnd();
            }

            //get pic head by case number
            List<emailTo> listMailTo = new List<emailTo>();
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);

            DataTable dt_pichead = dbclass.retrieve_pic_head(dt.Rows[0]["CaseNumber"].ToString());      /*SP_Retrieve_PICHead*/
            DataTable dt_case = dbclass.retrieve_Case(dt.Rows[0]["CaseNumber"].ToString());             /*SP_Retrieve_Case*/

            String WorkedBy = "";
            string picname = "";
            if (dt_case.Rows.Count > 0 || dt_pichead.Rows.Count > 0)
            {
                emailTo to = new emailTo();
                to.emailAddress = dt_pichead.Rows[0]["EmailAddress"].ToString();
                picname = picname + dt_pichead.Rows[0]["FullName"].ToString() + " ,";
                listMailTo.Add(to);
                //insert pic email address
                WorkedBy = dt_case.Rows[0]["WorkedBy"].ToString();

                DataTable dt_pic = dbclass.retrieve_user(WorkedBy);
                if (dt_pic.Rows.Count > 0)
                {
                    to.emailAddress = dt_pic.Rows[0]["EmailAddress"].ToString();
                    picname = picname + dt_pic.Rows[0]["FullName"].ToString();
                    listMailTo.Add(to);
                }

            }

            body = body.Replace("{{representative}}", picname);

            body = body.Replace("{{unitcode}}", dt.Rows[0]["UnitCode"].ToString());
            body = body.Replace("{{unitnumber}}", dt.Rows[0]["UnitNo"].ToString());
            body = body.Replace("{{helpcategory}}", dt.Rows[0]["HelpCategoryName"].ToString());
            body = body.Replace("{{helpgroup}}", dt.Rows[0]["HelpGroup"].ToString());
            body = body.Replace("{{helpname}}", dt.Rows[0]["HelpName"].ToString());
            body = body.Replace("{{location}}", dt.Rows[0]["LocationName"].ToString());
            body = body.Replace("{{sublocation}}", dt.Rows[0]["SublocationName"].ToString());
            body = body.Replace("{{casenumber}}", dt.Rows[0]["CaseNumber"].ToString());
            body = body.Replace("{{descrtiption}}", dt.Rows[0]["Description"].ToString());          //pic
            body = body.Replace("{{duration}}", dt.Rows[0]["Duration"].ToString());              //pic
            body = body.Replace("{{callcenter}}", dt.Rows[0]["CallCenter"].ToString());
            body = body.Replace("{{signaturelogo}}", dt.Rows[0]["SignatureName"].ToString());
            body = body.Replace("{{signatureaddress}}", dt.Rows[0]["SignatureAddress"].ToString());

            //return body;
            response.emailTo = listMailTo;
            response.emailTilte = "Case status for " + dt.Rows[0]["CaseNumber"].ToString() + " is " + dt.Rows[0]["CaseStatusName"].ToString();
            response.emailBody = body;

            return response;
        }

        public static void sendEmailToCustomer(string emailServiceUrl, string emailCategory, DataTable dt, string emailFrom, string displayEmail, int helpdeskStatus)
        {
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            string SatisfactionURL = ConfigurationManager.AppSettings["SatisfactionUrl"].ToString();
            lkservice.LKEmailService emailService = new lkservice.LKEmailService();
            emailService.Url = emailServiceUrl;
            string mailCategory = emailCategory;

            string emailID = emailService.getEmailIDForMultipleAttachment(mailCategory);

            int helpCategoryID = (int)dt.Rows[0]["HelpCategoryID"];
            int siteID = (int)dt.Rows[0]["SiteID"];
            string caseNumber = dt.Rows[0]["CaseNumber"].ToString();
            string emailTemplate = "";
            string satisfactionAccessID = "";
            switch (helpdeskStatus)
            {
                case 1: //pending
                    emailTemplate = "Case Created Notification - Pending.html";
                    break;
                case 2: //assigned to pic
                    if (helpCategoryID == 4 && siteID == 5)    //incident cikarang pake template khusus
                        emailTemplate = "Case Created Notification Emergency - No Pending.html";
                    else
                        emailTemplate = "Case Created Notification - No Pending.html";
                    break;
                case 3: //cancelled
                    emailTemplate = "Case Cancelled Notification - Customer.html";
                    break;
                case 5: //OnHold
                    emailTemplate = "Case On Hold Notification.html";
                    break;
                case 6: //progress completed
                    emailTemplate = "Case Updated Progress Completed Notification.html";
                    satisfactionAccessID = RandomStringNumber(5);
                    dbclass.save_satisfaction_access(satisfactionAccessID, caseNumber, SatisfactionURL);
                    break;
                case 9: //case closed
                    emailTemplate = "Case Closed Notification.html";
                    break;
                case 10: //rejected
                    emailTemplate = "Case Rejected Notification.html";
                    break;
            }

            emailHtml emailHtmlResponse = new emailHtml();
            emailHtmlResponse = createEmailBodyToCustomer(emailTemplate, dt, satisfactionAccessID);

            List<emailTo> listEmailTo = emailHtmlResponse.emailTo;
            if (listEmailTo.Count > 0)
            {
                string listStringEmailto = "";
                foreach (emailTo e in listEmailTo)
                {
                    listStringEmailto = listStringEmailto + e.emailAddress + ";";
                }
                string to = listStringEmailto.Substring(0, (listStringEmailto.Length - 1));

                emailService.sendMailAttachment_Inserted(to, string.Empty, string.Empty, emailHtmlResponse.emailTilte,
                    emailHtmlResponse.emailBody, string.Empty, emailCategory, emailFrom, displayEmail, emailID);

                //save log
                dbclass.save_email_request_log(dt.Rows[0]["CaseNumber"].ToString(), helpdeskStatus, to, emailHtmlResponse.emailTilte + " " + emailHtmlResponse.emailBody);
            }

        }

        public static void sendEmailToPICHead(string emailServiceUrl, string emailCategory, DataTable dt, string emailFrom, string displayEmail, int helpdeskStatus)
        {
            lkservice.LKEmailService emailService = new lkservice.LKEmailService();
            emailService.Url = emailServiceUrl;
            string mailCategory = emailCategory;
            string emailID = emailService.getEmailIDForMultipleAttachment(mailCategory);

            int helpCategoryID = (int)dt.Rows[0]["HelpCategoryID"];
            int siteID = (int)dt.Rows[0]["SiteID"];
            string emailTemplate = "";
            switch (helpdeskStatus)
            {
                case 2: //Assigned To PIC
                    if (helpCategoryID == 4 && siteID == 5)    //incident cikarang pake template khusus
                        emailTemplate = "Case No Pending Emergency - PIC.html";
                    else
                        emailTemplate = "Case No Pending - PIC.html";
                    break;
            }

            emailHtml emailHtmlResponse = new emailHtml();
            emailHtmlResponse = createEmailBodyToPICHead(emailTemplate, dt);

            List<emailTo> listEmailTo = emailHtmlResponse.emailTo;
            if (listEmailTo.Count > 0)
            {
                string listStringEmailto = "";
                foreach (emailTo e in listEmailTo)
                {
                    listStringEmailto = listStringEmailto + e.emailAddress + ";";
                }
                string to = listStringEmailto.Substring(0, (listStringEmailto.Length - 1));

                emailService.sendMailAttachment_Inserted(to, string.Empty, string.Empty, emailHtmlResponse.emailTilte,
                    emailHtmlResponse.emailBody, string.Empty, emailCategory, emailFrom, displayEmail, emailID);

                //save log
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                dbclass.save_email_request_log(dt.Rows[0]["CaseNumber"].ToString(), helpdeskStatus, to, emailHtmlResponse.emailTilte + " " + emailHtmlResponse.emailBody);
            }

        }

        public static void sendEmailToPIC(string emailServiceUrl, string emailCategory, DataTable dt, string emailFrom, string displayEmail, int helpdeskStatus)
        {
            lkservice.LKEmailService emailService = new lkservice.LKEmailService();
            emailService.Url = emailServiceUrl;
            string mailCategory = emailCategory;
            string emailID = emailService.getEmailIDForMultipleAttachment(mailCategory);

            string emailTemplate = "";
            switch (helpdeskStatus)
            {
                case 2: //Assigned To PIC
                    emailTemplate = "Case No Pending - PIC.html";
                    break;
                case 3: //cancelled
                    emailTemplate = "Case Cancelled Notification - PIC.html";
                    break;
                case 5: //on hold
                    emailTemplate = "Case On Hold Notification.html";
                    break;
                case 8: //not satisfied
                    emailTemplate = "Case Not Satisfied Notification.html";
                    break;
            }

            emailHtml emailHtmlResponse = new emailHtml();
            emailHtmlResponse = createEmailBodyToPIC(emailTemplate, dt);

            List<emailTo> listEmailTo = emailHtmlResponse.emailTo;
            if (listEmailTo.Count > 0)
            {
                string listStringEmailto = "";
                foreach (emailTo e in listEmailTo)
                {
                    listStringEmailto = listStringEmailto + e.emailAddress + ";";
                }
                string to = listStringEmailto.Substring(0, (listStringEmailto.Length - 1));

                emailService.sendMailAttachment_Inserted(to, string.Empty, string.Empty, emailHtmlResponse.emailTilte,
                    emailHtmlResponse.emailBody, string.Empty, emailCategory, emailFrom, displayEmail, emailID);

                //save log
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                DBclass dbclass = new DBclass(constring);
                dbclass.save_email_request_log(dt.Rows[0]["CaseNumber"].ToString(), helpdeskStatus, to, emailHtmlResponse.emailTilte + " " + emailHtmlResponse.emailBody);
            }
        }

        private static string RandomStringNumber(int size)
        {
            string Alphabet = "0123456789";
            Random rand = new Random();
            char[] chars = new char[size];
            for (int i = 0; i < size; i++)
            {
                chars[i] = Alphabet[rand.Next(Alphabet.Length)];
            }
            return new string(chars);
        }
    }

    public class emailHtml
    {
        public List<emailTo> emailTo { get; set; }
        public string emailBody { get; set; }
        public string emailTilte { get; set; }
    }

    public class emailTo
    {
        public string emailAddress { get; set; }
    }

    //public class emailTo
    //{
    //    public List<emailAddress> listEmailTo { get; set; }
    //}


    //public class emailAddress
    //{
    //    //string emailAddress { get; set; }
    //}

    //public class emailCustomer
    //{
    //    public List<emailAddress> listEmailCustomer { get; set; }
    //}

    //public class emailPIC
    //{
    //    public List<emailAddress> listEmailPic { get; set; }
    //}
}
