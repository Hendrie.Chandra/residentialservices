﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ServiceModel.Description;
using System.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using System.Data.SqlClient;
using System.Data;

namespace residential.batchAutoCaseClosed
{
    class Program
    {
        static void Main(string[] args)
        {            
            String PathResultLog = ConfigurationManager.AppSettings["PathResultLog"].ToString();
            String linesLog = "";          

            Int32 DaysToClosed = Int32.Parse(ConfigurationManager.AppSettings["DaysToClosed"].ToString());

            DataTable dtget_TrxCase_Progress_Completed = get_TrxCase_Progress_Completed();                       

            if (dtget_TrxCase_Progress_Completed != null)
            {
                String Log = "";
                foreach (DataRow dtRow in dtget_TrxCase_Progress_Completed.Rows)
                {
                    if ((DateTime.Now - DateTime.Parse(dtRow["ModifiedDate"].ToString())).Days != DaysToClosed)
                    {
                        String NomorCase = dtRow["CaseNumber"].ToString();
                        String CreatedDate = dtRow["CreatedDate"].ToString();


                        //update status menjadi case closed disini
                        Update_Status_CaseClosed(NomorCase);

                        Log = "Update NomorCase = " + NomorCase + " Dengan Status " + "" + " Menjadi Case Closed Karena aging Sudah Mencapai  " + "" + " Hari Dari Case Yang Dibuat Tanggal = " + CreatedDate;
                        linesLog += Log + "\r\n";
                        Console.WriteLine(Log);
                    }
                }                
            }
            else
            {
                linesLog += "Tidak ada case yang akan di Closed \r\n";
                Console.WriteLine("Tidak ada case yang akan di Closed");
            }
            linesLog += "Done running bacth AutoCompleted at " + DateTime.Now.ToString("HH:mm:ss tt") + "\r\n";
            linesLog += cleanFileTemp(PathResultLog);
            writeLog(linesLog, PathResultLog + "logBatchAutoCaseClosed - " + DateTime.Now.ToString("dd-MM-yyyy") + ".txt");

            Console.WriteLine("Done");
            //Console.Write("Press any key to exit...");
            //Console.ReadKey();
        }

        static private void writeLog(string linesLog, String PathResultLog)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(PathResultLog);
            file.WriteLine(linesLog);
            file.Close();
        }

        static string cleanFileTemp(string path)
        {
            String log = "";
            DateTime minus7 = DateTime.Now.AddDays(-7);
            string file = path + "logBatchAutoCaseClosed - " + minus7.ToString("dd-MM-yyyy") + ".txt";

            if (System.IO.File.Exists(file))
            {
                System.IO.File.Delete(file);
                log += file + " will be deleted";
            }
            else
            {
                log += file + " not found";
            }

            return log;
        }

        static DataTable get_TrxCase_Progress_Completed()
        {
            string DBConnectionString = ConfigurationManager.ConnectionStrings["residentialdev"].ConnectionString;

            SqlConnection conn = new SqlConnection(DBConnectionString);
            conn.Open();
            SqlCommand com = new SqlCommand("SP_Retrieve_Case", conn);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CaseStatusID", 6);
            SqlDataAdapter sda = new SqlDataAdapter(com);
            DataTable dta = new DataTable();
            sda.Fill(dta);
            conn.Close();

            return dta;
        }

        static private void Update_Status_CaseClosed(String _NomorCase)
        {     
            //Update to TMDDB
            string DBConnectionString = ConfigurationManager.ConnectionStrings["residentialdev"].ConnectionString;
            SqlConnection conn = new SqlConnection(DBConnectionString);
            conn.Open();
            SqlCommand com = new SqlCommand("SP_Update_Status_TRCase_CaseClosed", conn);//sp ini modified untuk update ticket history, karena tidak ada crm
            com.Parameters.AddWithValue("@CaseNumber", _NomorCase);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter sda = new SqlDataAdapter(com);
            DataTable dta = new DataTable();
            sda.Fill(dta);
            conn.Close();

            //setalah update status menjadi close, kirim email ke customer dari plugin
            ////kirim email disini 
            String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
            DBclass dbclass = new DBclass(constring);
            DataTable dt = dbclass.retrieve_case_detail(_NomorCase);
            if (dt.Rows.Count > 0)
            {
                string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                string emailFrom = dt.Rows[0]["EmailFrom"].ToString();
                string displayEmailFrom = dt.Rows[0]["DisplayEmailFrom"].ToString();
                int helpdeskStatus = (int)dt.Rows[0]["CaseStatusID"];

                EmailClass.sendEmailToCustomerCaseClose(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
            }
        }
    }
}
