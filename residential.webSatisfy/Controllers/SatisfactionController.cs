﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel.Description;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net.Http.Headers;
using residential.webSatisfy.Models;
using System.Data;

namespace residential.webSatisfy.Controllers
{
    public class SatisfactionController : Controller
    {

        public IOrganizationService crmservice_conn()
        {
            Uri uriorg = new Uri(ConfigurationManager.AppSettings["CRMUrlOrg"].ToString());
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential.Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            credentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            credentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            OrganizationServiceProxy serviceproxy = new OrganizationServiceProxy(uriorg, null, credentials, null);
            IOrganizationService service;
            service = (IOrganizationService)serviceproxy;

            return service;
        }

        private Entity getrecord_byattribute(IOrganizationService _service, String _entityname, String _searchattr, String _searchvalue, String[] _columnset)
        {
            QueryByAttribute query = new QueryByAttribute(_entityname);
            query.ColumnSet = new ColumnSet(_columnset);
            query.Attributes.AddRange(new String[] { _searchattr });
            query.Values.AddRange(new Object[] { _searchvalue });
            EntityCollection entities = _service.RetrieveMultiple(query);
            Entity entity = null;
            if (entities != null)
                if (entities.Entities.Count > 0)
                    entity = entities.Entities[0];

            return entity;
        }

        // GET: Satisfaction
        public ActionResult Index()
        {
            List<String> Response = new List<String>();
            ViewBag.HtmlStr = "";
            ViewBag.HtmlResponse = "";

            //test
            //string aa = "0";
            //if (aa == "0")
            //{
            //    bool issatisfied = true;
            //    if (!issatisfied)
            //    {
            //        ViewBag.HtmlStr = "<p>Berikan tanggapan jika Anda tidak puas dengan pelayanan kami: </p><textarea id=\"SatisfactionDescription\" name=\"SatisfactionDescription\" cols=\"200\" rows=\"10\"></textarea><br/><br/><input type=\"button\" name=\"SubmitDescription\" id=\"SubmitDescription\" class=\"btn btn-primary\" value=\"Submit\">";
            //        ViewBag.HtmlResponse = "<p><b>Terima kasih atas respon anda, kami akan menindaklanjuti masalah ini.</b></p>";
            //    }
            //    else
            //    {
            //        //ViewBag.HtmlStr = "<p><b>Terima kasih atas kepuasan anda.</b></p>";
            //        ViewBag.HtmlStr = "<p>Please submit your rating and feedback for us</p><div align=\"center\"><input id=\"inputRating\" type=\"number\" class=\"rating\" min=0 max=5 step=0.5 data-glyphicon=\"false\" data-star-captions=\"{}\" data-default-caption=\"{rating} Stars\" data-size=\"xl\"></div><input id=\"RatingHiddenValue\" name=\"RatingHiddenValue\" type=\"hidden\" value=\"1\" /><br/><div class=\"row\"><div class=\"col-md-12 col-sm-12 col-xs-12\"><textarea id=\"SatisfactionDescription\" name=\"SatisfactionDescription\" cols=\"200\" rows=\"10\"></textarea></div></div><br/><br /><input type=\"button\" name=\"SubmitRating\" id=\"SubmitRating\" class=\"btn btn-primary\" value=\"Submit\">";
            //        ViewBag.HtmlResponse = "<p><b>Terima kasih atas kepuasan anda !!</b></p>";
            //    }

            //    return View();
            //}

            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                ClassDB db = new ClassDB(constring);

                if (Request["checking"] != null && Request["issatisfied"] != null && Request["id"] != null)
                {
                    string Case_id = Request["checking"].ToString();                                                       //casenumber
                    string SatisficationAccess_id = Request["id"].ToString();                                              //unik random number
                    Boolean issatisfied = Convert.ToBoolean(Convert.ToInt16(Request["issatisfied"].ToString()));           //finis:1 not finish:0

                    if (!string.IsNullOrEmpty(SatisficationAccess_id))
                    {
                        //panggil sp untuk dapatkan satisfactiondata
                        DataTable dt = db.retrieve_satisfaction_access(SatisficationAccess_id);
                        if (dt.Rows.Count<=0)
                            return ViewBag.HtmlStr = "<p><b>Satisfaction token tidak valid !!</b></p>";                                              

                        bool isAccess = (bool)dt.Rows[0]["IsAccess"];


                        if (!isAccess && (int)dt.Rows[0]["CaseStatusID"] == 6)
                        {
                            //jika belum, maka update isaccess menajdi true
                            //update sp disini
                            string returnupdate = db.update_satisfaction_access(SatisficationAccess_id, true, issatisfied);

                            if (!issatisfied)
                            {
                                ViewBag.HtmlStr = "<p>Berikan tanggapan jika Anda tidak puas dengan pelayanan kami: </p><textarea id=\"SatisfactionDescription\" name=\"SatisfactionDescription\" cols=\"200\" rows=\"10\"></textarea><br/><br/><input type=\"button\" name=\"SubmitDescription\" id=\"SubmitDescription\" class=\"btn btn-primary\" value=\"Submit\">";
                                ViewBag.HtmlResponse = "<p><b>Terima kasih atas respon anda, kami akan menindaklanjuti masalah ini.</b></p>";
                            }
                            else
                            {
                                //ViewBag.HtmlStr = "<p><b>Terima kasih atas kepuasan anda.</b></p>";
                                //ViewBag.HtmlStr = "<p>Please submit your rating for us</p><div align=\"left\" style=\"margin-left: 350px\"><input id=\"inputRating\" type=\"number\" class=\"rating\" min=0 max=5 step=0.5 data-glyphicon=\"false\" data-star-captions=\"{}\" data-default-caption=\"{rating} Stars\" data-size=\"xl\"></div><input id=\"RatingHiddenValue\" name=\"RatingHiddenValue\" type=\"hidden\" value=\"1\" /><br /><input type=\"button\" name=\"SubmitRating\" id=\"SubmitRating\" class=\"btn btn-primary\" value=\"Submit\">";
                                //ViewBag.HtmlStr = "<p>Please submit your rating and feedback for us</p><div align=\"center\"><input id=\"inputRating\" type=\"number\" class=\"rating\" min=0 max=5 step=0.5 data-glyphicon=\"false\" data-star-captions=\"{}\" data-default-caption=\"{rating} Stars\" data-size=\"xl\"></div><input id=\"RatingHiddenValue\" name=\"RatingHiddenValue\" type=\"hidden\" value=\"1\" /><br/><div class=\"row\"><div class=\"col-md-12 col-sm-12 col-xs-12\"><textarea id=\"SatisfactionDescription\" name=\"SatisfactionDescription\" cols=\"200\" rows=\"10\"></textarea></div></div><br/><br /><input type=\"button\" name=\"SubmitRating\" id=\"SubmitRating\" class=\"btn btn-primary\" value=\"Submit\">";

                                ViewBag.HtmlStr = "<p>Please submit your rating and feedback for us</p><div align=\"center\"><input id=\"inputRating\" type=\"number\" class=\"rating\" min=0 max=5 step=1 data-glyphicon=\"false\" data-star-captions=\"{}\" data-default-caption=\"{rating} Stars\" data-size=\"xl\"></div><input id=\"RatingHiddenValue\" name=\"RatingHiddenValue\" type=\"hidden\" value=\"1\" /><br/><div class=\"row\"><div class=\"col-md-12 col-sm-12 col-xs-12\"><textarea id=\"SatisfactionDescription\" name=\"SatisfactionDescription\" cols=\"200\" rows=\"10\"></textarea></div></div><br/><br /><input type=\"button\" name=\"SubmitRating\" id=\"SubmitRating\" class=\"btn btn-primary\" value=\"Submit\">";
                                ViewBag.HtmlResponse = "<p><b>Terima kasih atas kepuasan anda !!</b></p>";
                            }
                        }
                        else
                        {
                            ViewBag.HtmlStr = "<p><b>Akses anda telah berakhir !!</b></p>";
                        }
                    }
                    else
                    {
                        ViewBag.HtmlStr = "<p><b>Akses anda telah berakhir !!</b></p>";
                    }
                }
                else
                {
                    ViewBag.HtmlStr = "<p><b>Data anda tidak lengkap !!</b></p>";
                }
            }
            catch (Exception ex)
            {
                string errorMessage = ex.Message;
                string innerException = ex.InnerException.ToString();

                if (ex.InnerException != null)
                    ViewBag.HtmlStr = "<p><b>" + errorMessage + " : " + innerException + "</b></p>";
                else
                    ViewBag.HtmlStr = "<p><b>" + errorMessage + "</b></p>";

            }

            return View();
        }

        // Tidak puas/Not finish
        [HttpPost]
        public ActionResult InsertSatisfactionDescription(SatisfactionDescriptionModels descRequest)
        {
            SatisfactionResponseModels response = new SatisfactionResponseModels();

            try
            {
                //update case status menjadi 8
                //update disini
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                string satisfactionUser = ConfigurationManager.AppSettings["satisfactionUser"].ToString();
                string fullName = ConfigurationManager.AppSettings["satisfactionUserFullName"].ToString();
                ClassDB db = new ClassDB(constring);

                string returnvalue = db.saveRatingToTMDB(descRequest.CaseID, null, descRequest.SatisfactionDescription);
                string returnupdate = db.save_tickethistory(descRequest.CaseID, 8, fullName, descRequest.SatisfactionDescription, satisfactionUser, null);

                ////kirim email disini                    
                DataTable dt = db.retrieve_case_detail(descRequest.CaseID);
                if (dt.Rows.Count > 0)
                {
                    string mailServiceUrl = ConfigurationManager.AppSettings["MailService"].ToString();
                    string mailCategory = ConfigurationManager.AppSettings["MailCategory"].ToString();

                    string emailFrom = dt.Rows[0]["EmailFrom"].ToString();
                    string displayEmailFrom = dt.Rows[0]["DisplayEmailFrom"].ToString();
                    int helpdeskStatus = (int)dt.Rows[0]["CaseStatusID"];

                    clsEmail.sendEmailToPIC(mailServiceUrl, mailCategory, dt, emailFrom, displayEmailFrom, helpdeskStatus);
                }

                return Json(new { success = true, responseText = "Berhasil! Case ID: " + descRequest.CaseID + ", Description: " + descRequest.SatisfactionDescription }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.inexcp = ex.InnerException.ToString();

                return Json(new { success = false, responseText = "Gagal! Case ID: " + descRequest.CaseID + ", Description: " + descRequest.SatisfactionDescription }, JsonRequestBehavior.AllowGet);
            }
        }

        // Puas/Satisfy
        [HttpPost]
        public ActionResult InsertSatisfactionRating(SatisfactionModels ratingRequest)
        {
            SatisfactionResponseModels response = new SatisfactionResponseModels();
            
            decimal des = 0;
            try
            {
                String constring = ConfigurationManager.ConnectionStrings["residentialdev"].ToString();
                string satisfactionUser = ConfigurationManager.AppSettings["satisfactionUser"].ToString();
                string fullName = ConfigurationManager.AppSettings["satisfactionUserFullName"].ToString();
                ClassDB db = new ClassDB(constring);

                string returnvalue = db.saveRatingToTMDB(ratingRequest.CaseID, ratingRequest.SatisfactionRating, ratingRequest.SatisfactionDescription);
                string returnupdate = db.save_tickethistory(ratingRequest.CaseID, 9, fullName, ratingRequest.SatisfactionDescription, satisfactionUser, null);

                return Json(new { success = true, responseText = "Berhasil! Case ID: " + ratingRequest.CaseID + ", Rating: " + ratingRequest.SatisfactionRating }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                response.message = ex.Message;
                response.inexcp = ex.InnerException.ToString();

                return Json(new { success = false, responseText = "Gagal! Case ID: " + ratingRequest.CaseID + ", Rating: " + ratingRequest.SatisfactionRating + ", Message: " + response.message + ", Inner Exception: " + response.inexcp }, JsonRequestBehavior.AllowGet);
            }

        }
    }
}