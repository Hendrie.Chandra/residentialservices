﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace residential.webSatisfy.Models
{
    public class ClassDB
    {
        private String connectionstring;

        public ClassDB(String _connectionstring)
        {
            connectionstring = _connectionstring;
        }

        public string save_tickethistory(String _CaseNumber, Int32 _CaseStatusID, String _FullName, String _Remarks, String _SavedBy, String _Attachment)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_TicketHistory", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@CaseStatusID", _CaseStatusID);
            cmd.Parameters.AddWithValue("@FullName", _FullName);
            cmd.Parameters.AddWithValue("@Remarks", _Remarks);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);
            cmd.Parameters.AddWithValue("@Attachment", _Attachment);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return returnparameter.Value.ToString();
        }

        public string saveRatingToTMDB(string _casenumber, decimal? _rating, string _description)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("SP_Save_FeedBack_Rating", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);
                cmd.Parameters.AddWithValue("@Rating", _rating);
                cmd.Parameters.AddWithValue("@FeedBackDescription", _description);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public DataTable retrieve_satisfaction_access(string _SatisfactionAccessID)
        {
            DataTable dt = new DataTable();

            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_satisfaction_access", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SatisfactionAccessID", _SatisfactionAccessID);

            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }

        public string update_satisfaction_access(String _SatisfactionAccessID, bool _IsAccess, bool _IsSatisfied)
        {
            try
            {
                SqlConnection conn = new SqlConnection(connectionstring);
                SqlCommand cmd = new SqlCommand("splk_update_satisfaction_access", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SatisfactionAccessID", _SatisfactionAccessID);
                cmd.Parameters.AddWithValue("@IsAccess", _IsAccess);
                cmd.Parameters.AddWithValue("@IsSatisfied", _IsSatisfied);

                SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
                returnparameter.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(returnparameter);

                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

                return returnparameter.Value.ToString();
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }
        }

        public DataTable retrieve_case_detail(string _CaseNumber)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_retrieve_case_detail", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
    }
}