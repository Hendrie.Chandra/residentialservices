﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace residential.webSatisfy.Models
{
    public class DBClass
    {
        private String connectionstring;

        public DBClass(String _connectionstring)
        {
            connectionstring = _connectionstring;
        }

        public string saveRatingToTMDB(string _casenumber, decimal? _rating, string _description, string _connectionstring, string _saveby)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_FeedBack_Rating", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _casenumber);
            cmd.Parameters.AddWithValue("@Rating", _rating);
            cmd.Parameters.AddWithValue("@FeedBackDescription", _description);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return returnparameter.Value.ToString();
        }
        public string save_satisfaction_access(string _SatisfactionAccessID, string _CaseNumber, string _SatisfactionUrl ,string _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("splk_insert_satisfaction_access", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@SatisfactionAccessID", _SatisfactionAccessID);
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@SatisfactionUrl", _SatisfactionUrl);

            SqlParameter returnparameter = new SqlParameter("@returnvalue", SqlDbType.Int);
            returnparameter.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnparameter);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();

            return returnparameter.Value.ToString();
        }

        public DataTable retrieve_resident_card(string _CaseNumber, string _SatisfactionUrl, bool _IsAccess, bool _IsSatisfied, DateTime _CreatedOn)
        {
            SqlConnection conn = new SqlConnection(connectionstring);
            SqlCommand cmd = new SqlCommand("splk_insert_satisfaction_access", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CaseNumber", _CaseNumber);
            cmd.Parameters.AddWithValue("@SatisfactionUrl", _SatisfactionUrl);
            cmd.Parameters.AddWithValue("@IsAccess", _IsAccess);
            cmd.Parameters.AddWithValue("@IsSatisfied", _IsSatisfied);
            cmd.Parameters.AddWithValue("@CreatedOn", _CreatedOn);
            conn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            conn.Close();

            return dt;
        }
    }
}