﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.webSatisfy.Models
{
    public class SatisfactionModels
    {
        public string SatisfactionAccessID { get; set; }
        public string CaseID { get; set; }
        public decimal SatisfactionRating { get; set; }
        public string SatisfactionDescription { get; set; } //add by bili 7 April 2017
    }

    public class SatisfactionDescriptionModels
    {
        public string SatisfactionAccessID { get; set; }
        public string CaseID { get; set; }
        public string SatisfactionDescription { get; set; }
    }

}