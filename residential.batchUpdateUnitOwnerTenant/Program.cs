﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using System.Configuration;
using System.ServiceModel.Description;
using Microsoft.Xrm.Sdk.Client;
using System.Windows.Forms;
using System.IO;
using Microsoft.Xrm.Sdk.Query;

namespace residential.updateUnitOwnerTenant
{
    static class Program
    {
        static IOrganizationService crmservice_conn()
        {
            Uri uriorg = new Uri(ConfigurationManager.AppSettings["CRMUrlOrg"].ToString());
            ClientCredentials credentials = new ClientCredentials();
            credentials.Windows.ClientCredential.Domain = ConfigurationManager.AppSettings["CRMDomain"].ToString();
            credentials.Windows.ClientCredential.UserName = ConfigurationManager.AppSettings["CRMUsername"].ToString();
            credentials.Windows.ClientCredential.Password = ConfigurationManager.AppSettings["CRMPassword"].ToString();
            OrganizationServiceProxy serviceproxy = new OrganizationServiceProxy(uriorg, null, credentials, null);
            IOrganizationService service;
            service = (IOrganizationService)serviceproxy;

            return service;
        }

        [STAThread]
        static void Main(string[] args)
        {
            IOrganizationService service = crmservice_conn();

            string filepath;

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "CSV Files|*.csv";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                filepath = dialog.FileName.ToString();

                List<string> line = new List<string>();
                List<string> roadcode = new List<string>();
                List<string> kavno = new List<string>();
                List<string> pscode = new List<string>();

                String logpath = ConfigurationManager.AppSettings["PathResultLog"].ToString();
                StreamWriter log;

                if (!File.Exists(logpath))
                {
                    log = new StreamWriter(logpath);
                }
                else
                {
                    log = new StreamWriter(logpath, true);
                }

                log.WriteLine("-----------------------" + DateTime.Now + "-----------------------");

                try
                {
                    using (StreamReader readFile = new StreamReader(filepath))
                    {
                        string baris;
                        int count = 0;
                        while ((baris = readFile.ReadLine()) != null)
                        {
                            String[] kolom = baris.Split(';');
                            if (kolom.Length == 3)
                            {
                                line.Add(baris);
                                roadcode.Add(kolom.ElementAt<string>(0).Replace(" ", ""));
                                kavno.Add(kolom.ElementAt<string>(1).Replace(" ", ""));
                                pscode.Add(kolom.ElementAt<string>(2).Replace(" ", ""));
                                count++;
                            }
                            else
                            {
                                Console.WriteLine("ERROR: Baris: \"" + baris + "\" memiliki jumlah elemen yang salah");
                                log.WriteLine("ERROR: Baris: \"" + baris + "\" memiliki jumlah elemen yang salah");
                            }
                        }

                        if (count > 0)
                        {
                            line.RemoveAt(0);
                            roadcode.RemoveAt(0);
                            kavno.RemoveAt(0);
                            pscode.RemoveAt(0);
                            count--;
                        }

                        for (int i = 0; i < count; i++)
                        {
                            try
                            {
                                if (roadcode[i].Length > 0 && kavno[i].Length > 0 && pscode[i].Length > 0)
                                {
                                    QueryExpression unitquery = new QueryExpression("new_unit");
                                    
                                    FilterExpression unitquery_filter1 = new FilterExpression();

                                    ConditionExpression unitquery_cond1 = new ConditionExpression();
                                    unitquery_cond1.AttributeName = "new_name";
                                    unitquery_cond1.Operator = ConditionOperator.Equal;
                                    unitquery_cond1.Values.Add(roadcode[i]);

                                    ConditionExpression unitquery_cond2 = new ConditionExpression();
                                    unitquery_cond2.AttributeName = "statecode";
                                    unitquery_cond2.Operator = ConditionOperator.Equal;
                                    unitquery_cond2.Values.Add(0);

                                    ConditionExpression unitquery_cond3 = new ConditionExpression();
                                    unitquery_cond3.AttributeName = "new_unitno";
                                    unitquery_cond3.Operator = ConditionOperator.Equal;
                                    unitquery_cond3.Values.Add(kavno[i]);

                                    unitquery_filter1.AddCondition(unitquery_cond1);
                                    unitquery_filter1.AddCondition(unitquery_cond2);
                                    unitquery_filter1.AddCondition(unitquery_cond3);

                                    unitquery.Criteria.AddFilter(unitquery_filter1);

                                    LinkEntity unitquery_link1 = new LinkEntity("new_unit", "new_unitmaster", "new_unitcode", "new_unitmasterid", JoinOperator.Inner);

                                    FilterExpression unitquery_link1_filter1 = new FilterExpression();
                                    
                                    ConditionExpression unitquery_link1_filter1_cond1 = new ConditionExpression();
                                    unitquery_link1_filter1_cond1.AttributeName = "new_name";
                                    unitquery_link1_filter1_cond1.Operator = ConditionOperator.Equal;
                                    unitquery_link1_filter1_cond1.Values.Add(roadcode[i]);

                                    ConditionExpression unitquery_link1_filter1_cond2 = new ConditionExpression();
                                    unitquery_link1_filter1_cond2.AttributeName = "new_unitcode";
                                    unitquery_link1_filter1_cond2.Operator = ConditionOperator.Equal;
                                    unitquery_link1_filter1_cond2.Values.Add(roadcode[i]);

                                    ConditionExpression unitquery_link1_filter1_cond3 = new ConditionExpression();
                                    unitquery_link1_filter1_cond3.AttributeName = "statecode";
                                    unitquery_link1_filter1_cond3.Operator = ConditionOperator.Equal;
                                    unitquery_link1_filter1_cond3.Values.Add(0);

                                    unitquery_link1_filter1.Conditions.Add(unitquery_link1_filter1_cond1);
                                    unitquery_link1_filter1.Conditions.Add(unitquery_link1_filter1_cond2);
                                    unitquery_link1_filter1.Conditions.Add(unitquery_link1_filter1_cond3);

                                    unitquery_link1.LinkCriteria.AddFilter(unitquery_link1_filter1);

                                    unitquery.LinkEntities.Add(unitquery_link1);

                                    EntityCollection unit = service.RetrieveMultiple(unitquery);

                                    if (unit != null && unit.Entities.Count == 1)
                                    {
                                        //Retrieve Owner
                                        QueryExpression ownerquery = new QueryExpression("contact");

                                        FilterExpression ownerquery_filter1 = new FilterExpression();

                                        ConditionExpression ownerquery_filter1_cond1 = new ConditionExpression();
                                        ownerquery_filter1_cond1.AttributeName = "new_pscode";
                                        ownerquery_filter1_cond1.Operator = ConditionOperator.Equal;
                                        ownerquery_filter1_cond1.Values.Add(pscode[i]);

                                        ConditionExpression ownerquery_filter1_cond2 = new ConditionExpression();
                                        ownerquery_filter1_cond2.AttributeName = "statecode";
                                        ownerquery_filter1_cond2.Operator = ConditionOperator.Equal;
                                        ownerquery_filter1_cond2.Values.Add(0);

                                        EntityCollection owner = service.RetrieveMultiple(ownerquery);

                                        if (owner != null && owner.Entities.Count == 1)
                                        {
                                            unit[0].Attributes["new_unitowner"] = new EntityReference("contact", owner[0].Id);
                                            service.Update(unit[0]);

                                            Console.WriteLine((i + 1).ToString() + " - Record unit " + roadcode[i] + " - " + kavno[i] + " telah berhasil diupdate untuk line berikut: " + line[i]);
                                            log.WriteLine((i + 1).ToString() + " - Record unit " + roadcode[i] + " - " + kavno[i] + " telah berhasil diupdate untuk line berikut: " + line[i]);
                                        }
                                        else
                                        {
                                            Console.WriteLine((i + 1).ToString() + " - ERROR: Tidak ditemukan atau terdapat duplikat untuk data personal dengan pscode " + pscode[i] + " untuk baris: \"" + line[i] + "\"");
                                            log.WriteLine((i + 1).ToString() + " - ERROR: Tidak ditemukan atau terdapat duplikat untuk data personal dengan pscode " + pscode[i] + " untuk baris: \"" + line[i] + "\"");
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine((i + 1).ToString() + " - ERROR: Tidak ditemukan atau terdapat duplikat untuk data unit " + roadcode[i] + " - " + kavno[i] + " untuk baris: \"" + line[i] + "\"");
                                        log.WriteLine((i + 1).ToString() + " - ERROR: Tidak ditemukan atau terdapat duplikat untuk data unit " + roadcode[i] + " - " + kavno[i] + " untuk baris: \"" + line[i] + "\"");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine((i + 1).ToString() + " - ERROR: Baris: \"" + line[i] + "\" memiliki data yang tidak lengkap");
                                    log.WriteLine((i + 1).ToString() + " - ERROR: Baris: \"" + line[i] + "\" memiliki data yang tidak lengkap");
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine((i + 1).ToString() + " - ERROR: Record gagal diproses untuk line berikut: " + line[i] + "\n" + ex.ToString());
                                log.WriteLine((i + 1).ToString() + " - ERROR: Record gagal diproses untuk line berikut: " + line[i] + "\n" + ex.ToString());
                            }
                        }
                    }
                    log.WriteLine();
                    log.WriteLine();
                    log.Close();
                    Console.ReadLine();

                }
                catch (Exception ex)
                {
                    Console.WriteLine("===ERROR=== " + ex.ToString());
                    log.WriteLine("===ERROR=== " + ex.ToString());
                    log.WriteLine();
                    log.WriteLine();
                    log.Close();
                    Console.ReadLine();
                }
            }
        }
    }
}
