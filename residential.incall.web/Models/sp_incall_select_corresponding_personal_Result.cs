//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace residential.incall.web.Models
{
    using System;
    
    public partial class sp_incall_select_corresponding_personal_Result
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string psCode { get; set; }
        public string StatusOwner { get; set; }
        public string UnitCode { get; set; }
        public string UnitNo { get; set; }
        public string sex { get; set; }
        public Nullable<System.DateTime> birthDate { get; set; }
        public string BillAddress { get; set; }
        public string BillCity { get; set; }
        public string email { get; set; }
    }
}
