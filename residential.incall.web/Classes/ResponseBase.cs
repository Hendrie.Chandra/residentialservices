﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace residential.incall.web.Classes
{
    public class ResponseBase
    {
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
    }
}