﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using residential.incall.web.Models;
using residential.incall.web.Classes;

namespace residential.incall.web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Incall()
        {
            string callerNumber = "";
            if (Request.Url.Segments.Count() < 4)
                return View("PageNotFound");
                

            callerNumber = Request.Url.Segments[3];

            if (!string.IsNullOrEmpty(callerNumber))
            {

            }
            else
            {
                //page not found
                return View("PageNotFound");
            }

            return View();
        }

        public ActionResult PageNotFound()
        {
            return View();
        }

        public JsonResult getCorrespondingPersonal(string callerNumber)
        {
            var result = new object();
            try
            {
                TMDBEntities db = new TMDBEntities();                
                result = db.sp_incall_select_corresponding_personal(callerNumber).ToList();
                return Json(new { Response = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { Response = ex.Message.ToString() }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Response = result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return View();
        }

        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";

        //    return View();
        //}

        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}