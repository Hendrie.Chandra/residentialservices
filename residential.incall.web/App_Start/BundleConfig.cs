﻿using System.Web;
using System.Web.Optimization;

namespace residential.incall.web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //add by bili
            bundles.Add(new StyleBundle("~/assets/vendor/bootstrap/css").Include(
                      "~/assets/vendor/bootstrap/css/bootstrap.min.css"));

            bundles.Add(new StyleBundle("~/assets/css").Include(
                      "~/assets/css/scrolling-nav.css",
                      "~/assets/font-awesome/css/font-awesome.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/template").Include(
                      "~/assets/vendor/jquery/jquery.min.js",
                      "~/assets/vendor/bootstrap/js/bootstrap.bundle.min.js",
                      "~/assets/vendor/jquery-easing/jquery.easing.min.js",
                      "~/assets/js/scrolling-nav.js",
                      "~/assets/js/incall-site.js"
                      ));



        }
    }
}
