﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Data;

namespace residential.DB_CRM_Synchronize
{
    public class Post_CRUD_DepartmentMapping : IPlugin
    {
        private void SaveMappingToDB(String _CRMDepartmentMappingID, String _CRMHelpID, String _CRMSubLocationID, String _CRMDepartmentID, String _SavedBy, String _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_DepartmentMapping", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CRMDepartmentMappingID", _CRMDepartmentMappingID);
            cmd.Parameters.AddWithValue("@CRMHelpID", _CRMHelpID);
            cmd.Parameters.AddWithValue("@CRMSubLocationID", _CRMSubLocationID);
            cmd.Parameters.AddWithValue("@CRMDepartmentID", _CRMDepartmentID);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        private void Save_PluginErrorLog(Exception _exc, String _initiatingUser, String _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
            cmd.Parameters.AddWithValue("@Message", _exc.Message);
            cmd.Parameters.AddWithValue("@Source", _exc.Source);
            cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
            cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
            cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            String connectionstring = "";
            String SavedBy = "";
            try
            {
                IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));

                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = servicefactory.CreateOrganizationService(context.InitiatingUserId);
                    IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.UserId);

                    Entity entity = new Entity();

                    if (String.Equals(context.MessageName.ToUpper(), "CREATE"))
                        entity = (Entity)context.InputParameters["Target"];
                    else if (String.Equals(context.MessageName.ToUpper(), "UPDATE"))
                        entity = (Entity)context.PostEntityImages["DepartmentMappingImage"];

                    QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                    q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                    q_configuration.Attributes.AddRange(new string[] { "new_name" });
                    q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                    EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);                    

                    if (configurations != null)
                    {
                        if (configurations.Entities.Count > 0)
                        {
                            Entity configuration = configurations.Entities[0];
                            connectionstring = configuration.Attributes["new_value"].ToString();
                        }
                    }

                    String CRMDepartmentMappingID = entity.Id.ToString();
                    String CRMHelpNameID = ((EntityReference)entity.Attributes["new_helpname"]).Id.ToString();
                    String CRMSubLocationID = null;
                    if (entity.Contains("new_sublocation"))
                        CRMSubLocationID = ((EntityReference)entity.Attributes["new_sublocation"]).Id.ToString();
                    String CRMDepartmentID = ((EntityReference)entity.Attributes["new_department"]).Id.ToString();

                    Entity user = service.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname", "fullname" }));
                    SavedBy = user.Attributes["domainname"].ToString();

                    SaveMappingToDB(CRMDepartmentMappingID, CRMHelpNameID, CRMSubLocationID, CRMDepartmentID, SavedBy, connectionstring);
                }
            }
            catch (FaultException<OrganizationServiceFault> e)
            {
                Save_PluginErrorLog(e, SavedBy, connectionstring);
                throw new InvalidPluginExecutionException("Error occured in Plugin");
            }
        }
    }
}
