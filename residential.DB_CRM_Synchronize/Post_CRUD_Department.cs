﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System.ServiceModel;
using System.Data.SqlClient;
using System.Data;

namespace residential.DB_CRM_Synchronize
{
    public class Post_CRUD_Department : IPlugin
    {
        private void SaveDepartmentToDB(String _CRMDepartmentID, String _CRMSiteID, String _DepartmentCode, String _DepartmentName, String _SavedBy, String _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_Department", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CRMDepartmentID", _CRMDepartmentID);
            cmd.Parameters.AddWithValue("@CRMSiteID", _CRMSiteID);
            cmd.Parameters.AddWithValue("@DepartmentCode", _DepartmentCode);
            cmd.Parameters.AddWithValue("@DepartmentName", _DepartmentName);
            cmd.Parameters.AddWithValue("@SavedBy", _SavedBy);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        private void Save_PluginErrorLog(Exception _exc, String _initiatingUser, String _connectionstring)
        {
            SqlConnection conn = new SqlConnection(_connectionstring);
            SqlCommand cmd = new SqlCommand("SP_Save_PluginErrorLog", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@HelpLink", _exc.HelpLink);
            cmd.Parameters.AddWithValue("@Message", _exc.Message);
            cmd.Parameters.AddWithValue("@Source", _exc.Source);
            cmd.Parameters.AddWithValue("@StackTrace", _exc.StackTrace);
            cmd.Parameters.AddWithValue("@TargetSite", _exc.TargetSite.ToString());
            cmd.Parameters.AddWithValue("@InitiatingUser", _initiatingUser);

            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void Execute(IServiceProvider _serviceprovider)
        {
            String connectionstring = "";
            String SavedBy = "";
            try
            {
                IPluginExecutionContext context = (IPluginExecutionContext)_serviceprovider.GetService(typeof(IPluginExecutionContext));
                if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is Entity)
                {
                    IOrganizationServiceFactory servicefactory = (IOrganizationServiceFactory)_serviceprovider.GetService(typeof(IOrganizationServiceFactory));
                    IOrganizationService service = servicefactory.CreateOrganizationService(context.UserId);
                    IOrganizationService adminservice = servicefactory.CreateOrganizationService(context.InitiatingUserId);

                    Entity entity = new Entity();

                    if (String.Equals(context.MessageName, "CREATE", StringComparison.OrdinalIgnoreCase))
                        entity = (Entity)context.InputParameters["Target"];
                    else if (String.Equals(context.MessageName, "UPDATE", StringComparison.OrdinalIgnoreCase))
                        entity = (Entity)context.PostEntityImages["DepartmentImage"];

                    QueryByAttribute q_configuration = new QueryByAttribute("new_residentialconfiguration");
                    q_configuration.ColumnSet = new ColumnSet(new string[] { "new_value" });
                    q_configuration.Attributes.AddRange(new string[] { "new_name" });
                    q_configuration.Values.AddRange(new object[] { "Residential ConnectionString" });
                    EntityCollection configurations = adminservice.RetrieveMultiple(q_configuration);                    

                    if (configurations != null)
                    {
                        if (configurations.Entities.Count > 0)
                        {
                            Entity configuration = configurations.Entities[0];
                            connectionstring = configuration.Attributes["new_value"].ToString();
                        }
                    }

                    String BusinessUnitID = ((EntityReference)entity.Attributes["owningbusinessunit"]).Id.ToString();

                    Entity user = service.Retrieve("systemuser", context.InitiatingUserId, new ColumnSet(new String[] { "domainname", "fullname" }));
                    SavedBy = user.Attributes["domainname"].ToString();

                    SaveDepartmentToDB(entity.Id.ToString(), BusinessUnitID, entity.Attributes["new_departmentcode"].ToString(),
                        entity.Attributes["new_name"].ToString(), SavedBy, connectionstring);
                }                
            }
            catch (Exception e)
            {
                Save_PluginErrorLog(e, SavedBy, connectionstring);
                throw new InvalidPluginExecutionException("Error occured in Plugin");
            }
        }
    }
}
